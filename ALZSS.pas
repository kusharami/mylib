unit ALZSS;

interface

uses
 SysUtils, HexUnit, Classes, Compression;

const
 (* LZSS CONSTANTS *)
 US_READBITS  = US_PREPARE;
 US_GETMODE   = US_CUSTOM;
 US_WRITEMODE = US_CUSTOM + 1;
 US_COPYMODE  = US_CUSTOM + 2;
 US_BIGMODE   = US_CUSTOM + 4;
 US_GETOFFSET = US_CUSTOM + 5;
 US_COPY      = US_CUSTOM + 6;
 US_WRITE     = US_CUSTOM + 7;

type
 TALZSS_PackStream = class(TCustomLZ77_PackStream)
  private
   FlagsPtrOffset: Integer;
   FlagIndex:      Byte;
   CopyShift:      Byte;
   CopyMask:       Byte;
  protected
   function WriteHeader: Integer; override;
   procedure WriteState; override;
  public
   constructor Create(Dest: TStream; RingShift: Byte);
   procedure Reset; override;
 end;

 TALZSS_UnpackStream = class(TCustomRingedUnpackStream)
  private
   Flags:      Word;
   RemainCopy: SmallInt;
   RingMask:   Word;
   CopyShift:  Byte;
   CopyMask:   Byte;
  protected
   function ReadHeader: Integer; override;
   function DecompressStep: Boolean; override;
  public
   constructor Create(Source: TStream);
 end;

implementation

{ TALZSS_PackStream }

constructor TALZSS_PackStream.Create(Dest: TStream; RingShift: Byte);
var
 MM: Integer;
begin
 CopyShift := 16 - RingShift;
 CopyMask := (1 shl CopyShift) - 1;
 MM := 3 + CopyMask;
 if RingShift >= 12 then Inc(MM, 255);
 inherited Create(Dest, MM, 0, RingShift, False, 0, $FFFFFF);
 FMinOut := 4;
end;

procedure TALZSS_PackStream.Reset;
begin
 inherited Reset;
 FlagIndex := 8;
 FlagsPtrOffset := -1;
end;

function TALZSS_PackStream.WriteHeader: Integer;
var
 X: LongWord;
begin
 X := (FRingShift shl 4) or (FTotalIn shl 8) or 7;
 Result := FStream.Write(X, 4);
end;

procedure TALZSS_PackStream.WriteState;
var
 FlagsPtr: PByte;
begin
 inherited;
 if FlagsPtrOffset >= 0 then
 begin
  FlagsPtr := FBuffer;
  Inc(FlagsPtr, FlagsPtrOffset);
 end;
 if FlagIndex = 8 then
 begin
  FlagsPtrOffset := FTotalOut;
  FlagsPtr := FOutput;
  FlagsPtr^ := 0;
  FlagIndex := 0;
  Inc(FOutput);
  Inc(FTotalOut);
  Dec(FRemainOut);
 end;
 if FMatchLength >= 3 then
 begin
  FlagsPtr^ := FlagsPtr^ or (1 shl FlagIndex);
  PWord(FOutput)^ := (((FRingPosition - FMatchLength) - FSearchPosition) shl CopyShift);
  if (FRingShift >= 12) and (FMatchLength >= CopyMask + 3) then
  begin
   PWord(FOutput)^ := PWord(FOutput)^ or CopyMask; 
   Inc(FOutput, 2);
   FOutput^ := FMatchLength - (CopyMask + 3);
   Inc(FOutput);
   Inc(FTotalOut, 3);
   Dec(FRemainOut, 3);
  end else
  begin
   PWord(FOutput)^ := PWord(FOutput)^ or ((FMatchLength - 3) and CopyMask);
   Inc(FOutput, 2);
   Inc(FTotalOut, 2);
   Dec(FRemainOut, 2);
  end;
 end else
 begin
  FOutput^ := FSrcPtr^;
  Inc(FOutput);
  Inc(FTotalOut);
  Dec(FRemainOut);
 end;
 Inc(FlagIndex);
end;

{ TALZSS_UnpackStream }

constructor TALZSS_UnpackStream.Create(Source: TStream);
begin
 inherited Create(Source, 0, 0);
end;

function TALZSS_UnpackStream.DecompressStep: Boolean;
begin
 Result := False;
 case FState of
  US_READBITS:
  begin
   if FRemainIn <= 0 then Exit;
   Flags := $0100 or InputByte;
   FState := US_GETMODE;
  end;
  US_GETMODE:
  begin
   if Flags and 1 = 0 then
    FState := US_WRITEMODE else
    FState := US_GETOFFSET;
   Flags := Flags shr 1;
  end;
  US_WRITEMODE:
  begin
   if FRemainIn <= 0 then Exit;
   OutputByte(InputByte);
   if Flags = 1 then
    FState := US_READBITS else
    FState := US_GETMODE;
  end;
  US_COPYMODE:
  begin
   if FRemainIn <= 0 then Exit;
   RemainCopy := RemainCopy or (InputByte shl 8);
   FRingCopyOffset := (FRingPosition - RemainCopy shr CopyShift) and RingMask;
   RemainCopy := RemainCopy and CopyMask + 3;
   if (CopyShift < 5) and (RemainCopy = CopyMask + 3) then
    FState := US_BIGMODE else
    FState := US_COPY;
  end;
  US_BIGMODE:
  begin
   if FRemainIn <= 0 then Exit;
   Inc(RemainCopy, InputByte);
   FState := US_COPY;
  end;
  US_GETOFFSET:
  begin
   if FRemainIn <= 0 then Exit;
   RemainCopy := InputByte;
   FState := US_COPYMODE;
  end;
  US_COPY:
  begin
   Dec(RemainCopy, RingCopy(RemainCopy));
   if RemainCopy = 0 then
   begin
    if Flags = 1 then
     FState := US_READBITS else
     FState := US_GETMODE;
   end;
  end;
  else Exit; (* decompression finished or error *)
 end;
 Result := True;
end;

function TALZSS_UnpackStream.ReadHeader: Integer;
var
 X: LongWord;
begin
 Result := FStream.Read(X, 4);
 if (X and 15) <> 7 then Result := -1 else
 begin
  FOutputSize := X shr 8;
  FRingShift := (X shr 4) and 15;
  RingMask := (1 shl FRingShift) - 1;
  CopyShift := 16 - FRingShift;
  CopyMask := (1 shl CopyShift) - 1;
 end;
end;

end.
