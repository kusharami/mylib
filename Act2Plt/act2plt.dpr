program act2plt;

{$APPTYPE CONSOLE}

uses
  Classes, SysUtils;

Type TRGB = Packed Record R: Byte; G: Byte; B: Byte end;

Var
 Dest: TStringList; Source: TFileStream; Palette: Array[Byte] of TRGB;
 I: Integer;

begin
 Writeln('ACT2PAL BY DJINN');
 Writeln('USAGE: act2pal.exe actfile.act pltfile.plt');
 If ParamCount = 2 then
 begin
  Source := TFileStream.Create(ParamStr(1), fmOpenRead);
  Source.Read(Palette, SizeOf(Palette));
  Source.Free;
  Dest := TStringList.Create;
  Dest.Add('[Main]');
  For I := 0 to 255 do With Palette[I] do
   Dest.Add(IntToHex(I, 2) + '=' +
            IntToHex(B, 2) +
            IntToHex(G, 2) +
            IntToHex(R, 2));
  Dest.SaveToFile(ParamStr(2));
  Dest.Free;
 end;
end.
