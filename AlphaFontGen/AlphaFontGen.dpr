program AlphaFontGen;

{$APPTYPE CONSOLE}

uses
  Types,
  Classes,
  SysUtils,
  Windows,
  Graphics,
  TBXGraphics,
  DIB;

function MultiByteToWideChar(CodePage, Flags: Integer; MBStr: PChar;
  MBCount: Integer; WCStr: PWideChar; WCCount: Integer): Integer; stdcall;
  external 'kernel32.dll' name 'MultiByteToWideChar';

function GetLocaleInfo(Locale: Longint; LCType: Longint; lpLCData: PChar; cchData: Integer): Integer; stdcall;
  external 'kernel32.dll' name 'GetLocaleInfoA';

function GetThreadLocale: Longint; stdcall;
  external 'kernel32.dll' name 'GetThreadLocale';

function LCIDToCodePage(ALcid: LongWord): Integer;
const
  CP_ACP = 0;                                // system default code page
  LOCALE_IDEFAULTANSICODEPAGE = $00001004;   // default ansi code page
var
  ResultCode: Integer;
  Buffer: array [0..6] of Char;
begin
  GetLocaleInfo(ALcid, LOCALE_IDEFAULTANSICODEPAGE, Buffer, SizeOf(Buffer));
  Val(Buffer, Result, ResultCode);
  if ResultCode <> 0 then
    Result := CP_ACP;
end;

Var
 FontName: TFontName; FS: TFontStyles; CodePage: Integer;
 I, W, H, FSize: Integer; FileName, S: String; WS: WideString; SP: PChar;
 WP: PWideChar;

Function CharSize(Const Canvas: TCanvas; Char: WideChar): TSize;
Var TM: TTextMetricW;
begin
 Result.CX := 0; Result.CY := 0;
 Windows.GetTextExtentPoint32W(Canvas.Handle, Addr(Char), 1, Result);
 Inc(Result.CX);
end;

Procedure DrawChar(Const Canvas: TCanvas; X, Y: Integer; Char: WideChar);
begin
 Windows.TextOutW(Canvas.Handle, X, Y, Addr(Char), 1);
end;

Var XX, YY: Integer; CSize: TSize; PixelData: TPixelData32; Pic: TDIB;

begin
 Writeln('USAGE:');
 Writeln('AlphaFontGen FileName.png Width Height FontName FontSize FontStyle CodePage');
 If ParamCount >= 3 then
 begin
  W := StrToInt(ParamStr(2));
  H := StrToInt(ParamStr(3));
  FontName := ParamStr(4);
  If FontName = '' then FontName := 'MS Sans Serif';
  try FSize := StrToInt(ParamStr(5))
  except on E: TObject do FSize := 8 end;
  try I := StrToInt(ParamStr(6))
  except on E: TObject do I := 0 end;
  FS := [];
  If I and 1 = 1 then Include(FS, fsBold);
  If I and 2 = 1 then Include(FS, fsItalic);
  If I and 4 = 1 then Include(FS, fsUnderline);
  If I and 8 = 1 then Include(FS, fsStrikeOut);
  try CodePage := StrToInt(ParamStr(7))
  except on E: TObject do CodePage := LCIDToCodePage(GetThreadLocale) end;
  FileName := ParamStr(1);
  With TDIB.Create do
  begin
   PixelFormat := MakeDIBPixelFormat(8, 8, 8);
   BitCount := 24;
   Width := W;
   Height := H;
   Pic := TDIB.Create;
   Pic.PixelFormat := PixelFormat;
   Pic.BitCount := 8;
   Pic.Width := W;
   Pic.Height := H;
   With Pic do For I := 0 to 255 do With ColorTable[I] do
   begin
    rgbRed := I;
    rgbGreen := I;
    rgbBlue := I;
    rgbReserved := 0;
   end;
   Pic.UpdatePalette;
   SetLength(S, 256);
   SetLength(WS, 256);
   SP := Addr(S[1]);
   For I := 0 to 255 do
   begin
    SP^ := Char(I);
    Inc(SP);
   end;
   WP := Addr(WS[1]);
   MultiByteToWideChar(CodePage, 0, Addr(S[1]), 256, WP, 256);
   With Canvas do
   begin
    Brush.Color := 0;
    Brush.Style := bsClear;
    Font.Color := clWhite;
    Font.Name := FontName;
    Font.Height := FSize;
    Font.Style := FS;
    FillRect(ClipRect);
    XX := 0; YY := 0;
    With TStringList.Create do
    begin
     Add('[symbol_coords]');
     Add(Format('height=%d', [FSize]));
     For I := 0 to 255 do
     begin
      CSize := CharSize(Canvas, WP^);
      If CSize.CX + XX >= W then
      begin
       XX := 0;
       Inc(YY, FSize);
      end;
      DrawChar(Canvas, XX, YY, WP^);
      Inc(WP);
      Add(Format('%3.3d = %4d, %4d, %4d, %4d', [I, XX, YY, XX + CSize.CX, YY + CSize.CY]));
      Inc(XX, CSize.CX);
     end;
     SaveToFile(ChangeFileExt(FileName, '.ini'));
     Free;
    end;
    Pic.Canvas.CopyRect(ClipRect, Canvas, ClipRect);
    Pic.SaveToFile(ChangeFileExt(FileName, '.bmp'));
    With Pic do For I := 0 to 255 do With ColorTable[I] do
    begin
     rgbRed := 255;
     rgbGreen := 255;
     rgbBlue := 255;
     rgbReserved := I;
    end;
    Pic.UpdatePalette;
    Pic.BitCount := 32;
    With TPNGBitmap.Create do
    begin
     Transparent := True;
     TDIB32(Addr(DIB)^) := TDIB32.Create;
     DIB.SetSize(W, H);
     PixelData.Bits := Pic.TopPBits;
     PixelData.ContentRect := Bounds(0, 0, W, H);
     PixelData.RowStride := -W;
     DIB.CopyFrom(PixelData, 0, 0, PixelData.ContentRect);
     SaveToFile(ChangeFileExt(FileName, '.png'));
     Free;
    end;
    Pic.Free;
   end;
   Free;
  end;
 end;
end.
