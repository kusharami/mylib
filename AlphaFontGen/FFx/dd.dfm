object Form1: TForm1
  Left = 305
  Top = 54
  AutoScroll = False
  Caption = 'Form1'
  ClientHeight = 673
  ClientWidth = 877
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 736
    Top = 112
    Width = 10
    Height = 13
    Caption = 'X:'
  end
  object Label2: TLabel
    Left = 784
    Top = 112
    Width = 10
    Height = 13
    Caption = 'Y:'
  end
  object Label3: TLabel
    Left = 736
    Top = 136
    Width = 11
    Height = 13
    Caption = 'D:'
  end
  object Label4: TLabel
    Left = 784
    Top = 136
    Width = 10
    Height = 13
    Caption = 'S:'
    Visible = False
  end
  object LBlock: TLabel
    Left = 8
    Top = 232
    Width = 28
    Height = 13
    Caption = #1041#1083#1086#1082':'
  end
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 721
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 736
    Top = 8
    Width = 65
    Height = 17
    Caption = #1054#1073#1079#1086#1088'...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 808
    Top = 8
    Width = 65
    Height = 17
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
    TabOrder = 2
    OnClick = Button2Click
  end
  object List: TListBox
    Left = 392
    Top = 32
    Width = 337
    Height = 193
    ItemHeight = 13
    TabOrder = 3
    OnClick = ListClick
  end
  object Text: TMemo
    Left = 8
    Top = 32
    Width = 377
    Height = 193
    Lines.Strings = (
      'Text')
    TabOrder = 4
    OnChange = TextChange
  end
  object FontAdr: TEdit
    Left = 768
    Top = 176
    Width = 17
    Height = 21
    TabOrder = 5
    Text = 'ff4fnt.bmp'
    Visible = False
  end
  object FontTxt: TEdit
    Left = 784
    Top = 176
    Width = 17
    Height = 21
    TabOrder = 6
    Text = 'ff4txt.txt'
    Visible = False
  end
  object Button3: TButton
    Left = 800
    Top = 176
    Width = 17
    Height = 25
    Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
    TabOrder = 7
    Visible = False
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 736
    Top = 160
    Width = 65
    Height = 17
    Caption = #1055#1086#1082#1072#1079#1072#1090#1100
    TabOrder = 8
    OnClick = Button4Click
  end
  object DXDraw1: TDXDraw
    Left = 8
    Top = 256
    Width = 865
    Height = 457
    AutoInitialize = True
    AutoSize = True
    Color = clBtnFace
    Display.FixedBitCount = True
    Display.FixedRatio = True
    Display.FixedSize = False
    Options = [doAllowReboot, doWaitVBlank, doCenter, doDirectX7Mode, doHardware, doSelectDriver]
    SurfaceHeight = 457
    SurfaceWidth = 865
    TabOrder = 9
  end
  object ChAvatar: TCheckBox
    Left = 736
    Top = 96
    Width = 81
    Height = 17
    Caption = #1040#1074#1072#1090#1072#1088#1099
    Checked = True
    State = cbChecked
    TabOrder = 10
    OnClick = Button4Click
  end
  object cX: TEdit
    Left = 752
    Top = 112
    Width = 25
    Height = 21
    MaxLength = 3
    TabOrder = 11
    Text = '12'
    OnKeyPress = cXKeyPress
  end
  object cY: TEdit
    Left = 800
    Top = 112
    Width = 25
    Height = 21
    MaxLength = 3
    TabOrder = 12
    Text = '8'
    OnKeyPress = cYKeyPress
  end
  object cD: TEdit
    Left = 752
    Top = 136
    Width = 25
    Height = 21
    MaxLength = 3
    TabOrder = 13
    Text = '12'
    OnKeyPress = cDKeyPress
  end
  object cS: TEdit
    Left = 800
    Top = 136
    Width = 25
    Height = 21
    MaxLength = 1
    TabOrder = 14
    Text = '4'
    Visible = False
    OnKeyPress = cSKeyPress
  end
  object ChBG: TCheckBox
    Left = 736
    Top = 48
    Width = 65
    Height = 17
    Caption = 'BG'
    Checked = True
    State = cbChecked
    TabOrder = 15
    OnClick = Button4Click
  end
  object ChAuto: TCheckBox
    Left = 736
    Top = 32
    Width = 113
    Height = 17
    Caption = #1040#1074#1090#1086#1086#1073#1085#1086#1074#1083#1077#1085#1080#1077
    Checked = True
    State = cbChecked
    TabOrder = 16
  end
  object ChReplace: TCheckBox
    Left = 736
    Top = 64
    Width = 121
    Height = 17
    Caption = #1047#1072#1084#1077#1085#1072' '#1087#1086' '#1090#1072#1073#1083#1080#1094#1077
    Checked = True
    State = cbChecked
    TabOrder = 17
    OnClick = Button4Click
  end
  object ChByte: TCheckBox
    Left = 736
    Top = 80
    Width = 97
    Height = 17
    Caption = #1059#1073#1088#1072#1090#1100' '#1073#1072#1081#1090#1099
    Checked = True
    State = cbChecked
    TabOrder = 18
  end
  object Button5: TButton
    Left = 808
    Top = 160
    Width = 65
    Height = 17
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 19
    OnClick = Button5Click
  end
  object EGoTo: TEdit
    Left = 112
    Top = 232
    Width = 49
    Height = 21
    TabOrder = 20
  end
  object Button6: TButton
    Left = 168
    Top = 232
    Width = 41
    Height = 17
    Caption = 'GoTo'
    TabOrder = 21
    OnClick = Button6Click
  end
  object Edit3: TEdit
    Left = 216
    Top = 232
    Width = 169
    Height = 21
    TabOrder = 22
  end
  object Button7: TButton
    Left = 392
    Top = 232
    Width = 65
    Height = 17
    Caption = #1053#1072#1081#1090#1080
    TabOrder = 23
  end
  object Edit4: TEdit
    Left = 464
    Top = 232
    Width = 177
    Height = 21
    TabOrder = 24
  end
  object Button8: TButton
    Left = 648
    Top = 232
    Width = 81
    Height = 17
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100
    TabOrder = 25
  end
  object Button9: TButton
    Left = 736
    Top = 232
    Width = 137
    Height = 17
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1074#1089#1105
    TabOrder = 26
  end
  object OpenDialog1: TOpenDialog
    Left = 736
    Top = 176
  end
end
