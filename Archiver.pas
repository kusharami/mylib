unit Archiver;

interface

Uses
 Windows, WcxHeadEx, SysUtils, Classes,  NodeLst, MyUtils, MyClasses;

Type
 TArchiveItem = Class;
 TLoadFromFile = Class;
 TLoadFromStream = Class;
 TArchiveItemClass = Class of TArchiveItem;
 TLoadFromFileClass = Class of TLoadFromFile;
 TLoadFromStreamClass = Class of TLoadFromStream;
 TArcProgressEvent = procedure(Sender: TObject; Item: TArchiveItem;
       const BytesCopied: Int64; var ProgressResult: Integer) of object;

 TArchiveSearchRec = Record
  Item: TArchiveItem;
  NextItem: TArchiveItem; 
  FindAttr: Integer;
  FindFlgs: Cardinal;
  FlagFind: Boolean;
  Path: WideString;
  Mask: WideString;
  FSF: Boolean;
 end;

 TCustomArchive = class(TNodeList)
  private
    FFileName: WideString;
    FStream: TStream;
    FFromFile: Boolean;
    FFreeFileStream: Boolean;
    FStreamFree: Boolean;
    FReadOnly: Boolean;
    FRestoreStreamPosition: Boolean;
    FLoaded: Boolean;
    FAutoUpdate: Boolean;
    FUpdated: Boolean;
    FFlags: Cardinal;
    FArchiveTime: Integer;
    FUpdateResult: Integer;
    FItemClass: TArchiveItemClass;
    FLoadFromFileClass: TLoadFromFileClass;
    FLoadFromStreamClass: TLoadFromStreamClass;
    FCopyBufferSize: Integer;
    FCopyBuffer: Pointer;
    FOnProgress: TArcProgressEvent;
    FLastError: Integer;
    FCustomParameters: WideString;
    FCanAdd: Boolean;
    FCanDelete: Boolean;
    function GetFileItem(Index: Integer): TArchiveItem;
    function FindFile(Name: WideString): TArchiveItem;
    function FindByFolder(Path: WideString): TArchiveItem;
    procedure SetAutoUpdate(Value: Boolean);
    procedure SetFlags(Value: Cardinal);
    procedure SetArchiveTime(Value: Integer);
  protected
    procedure Initialize; override;
    procedure AfterSetFlags; virtual;
    procedure AfterSetTime; virtual;
    function DoProgress(Item: TArchiveItem;
                  const BytesCopied: Int64): Integer; virtual;
    function Load(Source: TStream): Integer; virtual; abstract;
    function Save(Source, Dest: TStream): Integer; virtual; abstract;
    procedure ClearData; override;
    function GetDataFileName: WideString; virtual;
  public
    property ArchiveName: WideString
        read FFileName
       write FFileName; 
    property FromFile: Boolean
        read FFromFile;
    property AutoUpdate: Boolean
        read FAutoUpdate
       write SetAutoUpdate;
    property Updated: Boolean
        read FUpdated
       write FUpdated;
    property Loaded: Boolean
        read FLoaded
       write FLoaded;
    property RestoreStreamPosition: Boolean
        read FRestoreStreamPosition
       write FRestoreStreamPosition;
    property CanDelete: Boolean
        read FCanDelete
       write FCanDelete;
    property CanAdd: Boolean
        read FCanAdd
       write FCanAdd;
    property Flags: Cardinal
        read FFlags
       write SetFlags;
    property ReadOnly: Boolean
        read FReadOnly;
    property ArchiveTime: Integer
        read FArchiveTime
       write SetArchiveTime;
    property LastError: Integer
        read FLastError
       write FLastError; 
    property UpdateResult: Integer
        read FUpdateResult
       write FUpdateResult;
    property ItemClass: TArchiveItemClass
        read FItemClass
       write FItemClass;
    property LoadFromFileClass: TLoadFromFileClass
        read FLoadFromFileClass
       write FLoadFromFileClass;
    property LoadFromStreamClass: TLoadFromStreamClass
        read FLoadFromStreamClass
       write FLoadFromStreamClass;
    property CustomParameters: WideString
        read FCustomParameters
       write FCustomParameters;
    property FileItems[Index: Integer]: TArchiveItem
        read GetFileItem;
    property Files[Name: WideString]: TArchiveItem
        read FindFile;
    property FolderFiles[Path: WideString]: TArchiveItem
        read FindByFolder;
    property OnProgress: TArcProgressEvent
        read FOnProgress
       write FOnProgress;

    function InitStream: TStream; virtual;
    procedure DoneStream(var Stream: TStream); virtual;
    function AddItem: TArchiveItem;
    constructor Create(Stream: TStream; Size: Int64 = -1;
                            Position: Int64 = -1); overload;
    constructor Create(const FileName: WideString); overload;
    destructor Destroy; override;
    function Extract(Item: TArchiveItem; Dest: TStream): Integer; virtual;
    function ExtractFile(const FileName,
                               DestName: WideString): Integer; overload;
    function ExtractFile(const FileName: WideString;
                               Dest: TStream): Integer; overload;
    function ExtractFile(Item: TArchiveItem;
                         const DestName: WideString): Integer; overload;
    function AddFile(const FileName, DestName: WideString;
                     DeleteAfterUse: Boolean = False): TArchiveItem; overload;
    function AddFile(Source: TStream;
                     const DestName: WideString;
                     FreeAfterUse: Boolean = False): TArchiveItem; overload;
    procedure AddFromStream(Source: TStream;
                           Dest: TArchiveItem;
                           FreeAfterUse: Boolean = False);
    procedure AddFromFile(const FileName: WideString;
                         Dest: TArchiveItem;
                         DeleteAfterUse: Boolean = False);
    procedure LoadArchive; virtual;
    procedure UpdateFile; virtual;
    procedure SaveToFile(const FileName: WideString);
    procedure SaveToStream(Dest: TStream); virtual;
    procedure DisposeAdders(ApplyChanges: Boolean = False);
    function ArcFindFirst(const Pattern: WideString; Attr: Integer;
                        var SR: TArchiveSearchRec;
                       FindSubFolders: Boolean = False): Boolean;
    function ArcFindFirstFlg(const Pattern: WideString; Attr: Integer;
                        var SR: TArchiveSearchRec;
                        Flags: LongWord;
                       FindSubFolders: Boolean = False): Boolean;
    function ArcFindNext(var SR: TArchiveSearchRec): Boolean;
    function ArcDeleteFile(Item: TArchiveItem): Boolean; overload;
    function ArcDeleteFile(const FileName: WideString): Boolean; overload;
    function FindFolderFile(Path: WideString;
                 Root: TArchiveItem = NIL;
                 SearchInSubFolders: Boolean = True;                 
                 Converted: Boolean = False): TArchiveItem;
    procedure ApplyParameters; virtual;
 end;

 TArchiveClass = class of TCustomArchive; 

 TFileModifier = Class
  private
    FOwner: TArchiveItem;
    FApply: Boolean;
    FDisposeAfterUse: Boolean;
    FNewPosition: Int64;
    FNewFileSize: Int64;
    FNewPackSize: Int64;
    FNewFlags: Cardinal;
    FLoadFlags: Integer;
    FAdditionalData: Array of Integer;
    function GetFileSize: Int64;
  protected
    function DoGetFileSize: Int64; virtual;
  public
    property Owner: TArchiveItem
      read FOwner;
    property NewFileSize: Int64
        read GetFileSize
       write FNewFileSize;
    property NewPosition: Int64
        read FNewPosition
       write FNewPosition;
    property NewPackSize: Int64
        read FNewPackSize
       write FNewPackSize;
    property NewFlags: Cardinal
        read FNewFlags
       write FNewFlags;
    property LoadFlags: Integer
        read FLoadFlags
       write FLoadFlags;
    property DisposeAfterUse: Boolean
        read FDisposeAfterUse
       write FDisposeAfterUse;
    constructor Create(Owner: TArchiveItem; DisposeAfterUse: Boolean = False);
    destructor Destroy; override;
    function Process(Output: TStream): Integer; virtual;
    procedure ApplyChanges; virtual;
    procedure Dispose; virtual;
 end;

 TLoadFromFile = Class(TFileModifier)
  private
    FFileSizeGot: Boolean;
    FSourceFileName: WideString;
  protected
    function DoGetFileSize: Int64; override;
  public
    property SourceFileName: WideString
        read FSourceFileName
       write FSourceFileName;
    destructor Destroy; override;
    procedure Dispose; override;
    function Process(Output: TStream): Integer; override;
 end;

 TLoadFromStream = Class(TFileModifier)
  private
   FSourceStream: TStream;
  protected
   function DoGetFileSize: Int64; override;
  public
   property SourceStream: TStream
       read FSourceStream
      write FSourceStream;
   procedure Dispose; override;
   function Process(Output: TStream): Integer; override;
 end;
 
 TArchiveItem = Class(TNode)
  protected
    FOwner: TCustomArchive;
    FFileName: WideString;
    FFileAttr: Integer;
    FFileTime: Integer;
    FPosition: Int64;
    FFileSize: Int64;
    FPackSize: Int64;
    FNoPathFileName: WideString;
    FFlags: Cardinal;
    FModifier: TFileModifier;
    FTotalBytesCopied: Int64;
    function GetNewFileSize: Int64;
    function GetAnsiFileName: AnsiString;
    procedure SetAnsiFileName(const Value: AnsiString);
    procedure SetFileName(const Value: WideString);
  public
    property Owner: TCustomArchive
        read FOwner;
    property FileName: WideString
        read FFileName
       write SetFileName;
    property NoPathFileName: WideString
        read FNoPathFileName;
    property AnsiFileName: AnsiString
        read GetAnsiFileName
       write SetAnsiFileName;
    property FileAttr: Integer
        read FFileAttr
       write FFileAttr;
    property FileTime: Integer
        read FFileTime
       write FFileTime;
    property Position: Int64
        read FPosition
       write FPosition;
    property FileSize: Int64
        read FFileSize
       write FFileSize;
    property PackSize: Int64
        read FPackSize
       write FPackSize;
    property Flags: Cardinal
        read FFlags
       write FFlags;
    property TotalBytesCopied: Int64
        read FTotalBytesCopied
       write FTotalBytesCopied;
    property Modifier: TFileModifier
        read FModifier
       write FModifier;
    property NewFileSize: Int64
        read GetNewFileSize;

    constructor Create(Owner: TCustomArchive);
    destructor Destroy; override;
    function CopyData(Input, Output: TStream; Size: Int64): Integer; virtual;
    function Decompress(Input, Output: TStream): Integer; virtual;
    function Compress(Input, Output: TStream): Integer; virtual;
    function ProcessFile(Input, Output: TStream): Integer;
 end;

 EArchiveError = class(Exception)
  constructor BadArguments(const MethodName: AnsiString);
  constructor NoStream(const MethodName: AnsiString);
 end;

procedure WriteFileError;

const
 LOAD_FLAG_SET = 1 shl 31;

var
 AcceptedParameters: WideString;

implementation

procedure WriteFileError;
begin
 raise EArchiveError.Create('Error writing file.');
end;

//-------- Local functions --------//

function Find(var SR: TArchiveSearchRec): Boolean;
var S, S2: WideString; L: Integer;
begin
 With SR do
 begin
  L := Length(Path);
  While Item <> NIL do With Item do
  begin
   If (FFileAttr and FindAttr <> 0) and
      (not FlagFind or (FFlags = FindFlgs)) and (Length(FileName) > L) then
   begin
    S := WideUpperCase(FFileName);
    If (L = 0) or CompareMem(Pointer(Path), Pointer(S), L shl 1) then
    begin
     Delete(S, 1, L);
     S2 := WideExtractFileName(S);
     If FSF or (S = S2) then
     begin
      If WideMatchesMask(S2, Mask, True) then
      begin
       Result := True;
       Exit;
      end;
     end;
    end;
   end;
   Item := NextItem;
   if Item <> nil then
    NextItem := Pointer(Item.Next) else
    NextItem := nil;
  end;
 end;
 Finalize(SR.Path);
 Finalize(SR.Mask);
 FillChar(SR, SizeOf(SR), 0);
 Result := False;
end;

//----- TCustomArchive -----//

procedure TCustomArchive.ApplyParameters;
begin
 // do nothing
end;

function TCustomArchive.AddFile(const FileName, DestName: WideString;
                          DeleteAfterUse: Boolean = False): TArchiveItem;
begin
 If (FileName <> '') and (DestName <> '') then
 begin
  Result := Files[DestName];
  If Result = NIL then
  begin
   Result := AddItem;
   Result.FileName := DestName;
  end;
  AddFromFile(FFileName, Result, DeleteAfterUse);
 end else
  raise EArchiveError.BadArguments('AddFile');
end;

function TCustomArchive.AddFile(Source: TStream;
                          const DestName: WideString;
                          FreeAfterUse: Boolean = False): TArchiveItem;
begin
 If Assigned(Source) and (DestName <> '') then
 begin
  Result := Files[DestName];
  If Result = NIL then
  begin
   Result := AddItem;
   Result.FileName := DestName;
  end;
  AddFromStream(Source, Result, FreeAfterUse);
 end else
  raise EArchiveError.BadArguments('AddFile (overload #2)');
end;

procedure TCustomArchive.AddFromFile(const FileName: WideString;
                              Dest: TArchiveItem;
                              DeleteAfterUse: Boolean = False);
Var Adder: TLoadFromFile;
begin
 If (FileName <> '') and WideFileExists(FileName) and Assigned(Dest) then
 begin
  Adder := FLoadFromFileClass.Create(Dest, DeleteAfterUse);
  Adder.FSourceFileName := FileName;
  Dest.FModifier := Adder;
  FUpdated := False;
  if FAutoUpdate then UpdateFile;
 end else
  raise EArchiveError.BadArguments('AddFromFile');
end;

procedure TCustomArchive.AddFromStream(Source: TStream; Dest: TArchiveItem;
                                FreeAfterUse: Boolean = False);
Var Adder: TLoadFromStream;
begin
 If Assigned(Source) and Assigned(Dest) then
 begin
  Adder := FLoadFromStreamClass.Create(Dest, FreeAfterUse);
  Adder.FSourceStream := Source;
  Dest.FModifier := Adder;
  FUpdated := False;
  If FAutoUpdate then UpdateFile;
 end else
  raise EArchiveError.BadArguments('AddFromStream');
end;

function TCustomArchive.AddItem: TArchiveItem;
begin
 Result := FItemClass.Create(Self);
 Result.FFileTime := FArchiveTime;
 AddCreated(Result, True);
 FUpdated := False;
end;

procedure TCustomArchive.AfterSetFlags;
begin
//Do nothing
end;

procedure TCustomArchive.AfterSetTime;
begin
//Do nothing
end;

function TCustomArchive.ArcFindFirst(const Pattern: WideString; Attr: Integer;
         var SR: TArchiveSearchRec;
         FindSubFolders: Boolean = False): Boolean;
begin
 With SR do
 begin
  FSF := FindSubFolders;
  Path := WideUpperCase(WideExtractFilePath(Pattern));
  Mask := WideUpperCase(WideExtractFileName(Pattern));
  Item := TArchiveItem(RootNode);
  if Item <> nil then
   NextItem := Pointer(Item.Next) else
   NextItem := nil;
  FlagFind := False;
  FindAttr := Attr;
  Result := Find(SR);
 end;
end;

function TCustomArchive.ArcFindFirstFlg(const Pattern: WideString; Attr: Integer;
         var SR: TArchiveSearchRec;
         Flags: LongWord;
         FindSubFolders: Boolean = False): Boolean;
begin
 With SR do
 begin
  FSF := FindSubFolders;
  Path := WideUpperCase(WideExtractFilePath(Pattern));
  Mask := WideUpperCase(WideExtractFileName(Pattern));
  Item := TArchiveItem(RootNode);
  if Item <> nil then
   NextItem := Pointer(Item.Next) else
   NextItem := nil;  
  FindFlgs := Flags;
  FlagFind := True;
  FindAttr := Attr;
  Result := Find(SR);
 end;
end;


function TCustomArchive.ArcFindNext(var SR: TArchiveSearchRec): Boolean;
begin
 Result := False;
 with SR do
 begin
  Item := NextItem;
  if Item <> nil then
  begin
   NextItem := Pointer(Item.Next);
   Result := Find(SR);
  end else NextItem := nil;
 end;
end;

procedure TCustomArchive.ClearData;
begin
 DisposeAdders(True);
end;

constructor TCustomArchive.Create(const FileName: WideString);
begin
 FFileName := FileName;
 inherited Create;
 FReadOnly := CheckFileForWrite(FileName) = cfLocked;
 FFromFile := True;
 FArchiveTime := WideFileAge(FFileName);
end;

constructor TCustomArchive.Create(Stream: TStream; Size: Int64 = -1;
                            Position: Int64 = -1);
begin
 inherited Create;
 If (Size < 0) and (Position < 0) then
 begin
  FStream := Stream;
  FStreamFree := False;
 end Else
 begin
  FStream := TSubStream.Create(Stream, Size, Position);
  FStreamFree := True;
 end;
 FReadOnly := True;
 FFromFile := False;
end;

function TCustomArchive.ArcDeleteFile(const FileName: WideString): Boolean;
Var Item: TArchiveItem;
begin
 If FileName <> '' then
 begin
  Item := Files[FileName];
  Result := ArcDeleteFile(Item);
 end else Result := False;
end;

function TCustomArchive.ArcDeleteFile(Item: TArchiveItem): Boolean;
begin
 If Assigned(Item) then
 begin
  Remove(Item);
  FUpdated := False;
  if FAutoUpdate then UpdateFile;
  Result := True;
 end Else Result := False;
end;

procedure TCustomArchive.DisposeAdders(ApplyChanges: Boolean = False);
Var N: TNode;
begin
 N := RootNode;
 While N <> NIL do if N is TArchiveItem then With TArchiveItem(N) do
 begin
  If (FFileAttr and faDirectory = 0) and Assigned(FModifier) then
  begin
   FModifier.FApply := ApplyChanges;
   FreeAndNIL(FModifier);
  end;
  N := Next;
 end;
 N := RootNode;
 While N <> NIL do if N is TArchiveItem then With TArchiveItem(N) do
 begin
  If (FFileAttr and faDirectory <> 0) and Assigned(FModifier) then
  begin
   FModifier.FApply := ApplyChanges;
   FreeAndNIL(FModifier);
  end;
  N := Next;
 end;
end;

destructor TCustomArchive.Destroy;
begin
 inherited;
 If FStreamFree and Assigned(FStream) then FreeAndNIL(FStream);
 If FCopyBuffer <> NIL then FreeMem(FCopyBuffer);
end;

Procedure TCustomArchive.DoneStream(var Stream: TStream);
begin
 If FFreeFileStream then
 begin
  FFreeFileStream := False;
  if Assigned(Stream) then
  begin
   with Stream as THandleStream do
    FileClose(Handle);
   FreeAndNIL(Stream);
  end;
 end;
end;

function TCustomArchive.DoProgress(Item: TArchiveItem;
                          const BytesCopied: Int64): Integer;
begin
 Result := E_OK;
 Inc(Item.FTotalBytesCopied, BytesCopied);
 If Assigned(FOnProgress) then
  FOnProgress(Self, Item, BytesCopied, Result);
end;

function TCustomArchive.Extract(Item: TArchiveItem; Dest: TStream): Integer;
var
 Source: TStream;
begin
 if Assigned(Item) and Assigned(Dest) then
 begin
  Source := InitStream;
  try
   if Source <> nil then
   begin
    with Item do
    begin
     Source.Position :={ StreamPosition + }Position;
     Result := Decompress(Source, Dest);
    end;
   end else Result := E_ERROR;
  finally
   DoneStream(Source);
  end;
 end else
  raise EArchiveError.BadArguments('Extract');
end;

function TCustomArchive.ExtractFile(Item: TArchiveItem;
                              const DestName: WideString): Integer;
var
 F: TStream;
 H: THandle;
begin
 Result := E_ERROR;
 If Assigned(Item) and (DestName <> '') then
 try
  H := CreateFileW(PWideChar(DestName), GENERIC_READ or GENERIC_WRITE,
       0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
  if H <> INVALID_HANDLE_VALUE then
  try
   F := THandleStream.Create(H);
   try
    Result := Extract(Item, F);
   finally
    F.Free;
   end;
  finally
   FileClose(H);
  end;
 except
 end else
  raise EArchiveError.BadArguments('ExtractFile');
end;

function TCustomArchive.ExtractFile(const FileName: WideString;
                                          Dest: TStream): Integer;
begin
 If (FileName <> '') and Assigned(Dest) then
  Result := Extract(Files[FileName], Dest) else
  raise EArchiveError.BadArguments('ExtractFile (overload #2)');
end;

function TCustomArchive.ExtractFile(const FileName, DestName: WideString): Integer;
begin
 If (FileName <> '') and (DestName <> '') then
  Result := ExtractFile(Files[FileName], DestName) else
  raise EArchiveError.BadArguments('ExtractFile (overload #3)'); 
end;

function TCustomArchive.FindByFolder(Path: WideString): TArchiveItem;
Var N: TNode; Item: TArchiveItem;
begin
 N := RootNode; Result := NIL;
 Path := WideIncludeTrailingPathDelimiter(WideUpperCase(Path));
 While N <> NIL do if N is TArchiveItem then
 begin
  Item := TArchiveItem(N);
  if (Path = '') or (Pos(Path, WideUpperCase(Item.FFileName)) = 1) then
  begin
   Result := Item;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TCustomArchive.FindFile(Name: WideString): TArchiveItem;
Var N: TNode; Item: TArchiveItem;
begin
 Result := NIL;
 if Name = '' then Exit;
 N := RootNode;
 Name := WideUpperCase(Name);
 While N <> NIL do if N is TArchiveItem then
 begin
  Item := TArchiveItem(N);
  If (Item <> NIL) and (Name = WideUpperCase(Item.FFileName)) then
  begin
   Result := Item;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TCustomArchive.FindFolderFile(Path: WideString;
                 Root: TArchiveItem = NIL;
                 SearchInSubFolders: Boolean = True;
                 Converted: Boolean = False): TArchiveItem;
var
 N: TNode;
 S: WideString;
 L: Integer;
begin
 if Root = NIL then
  N := RootNode else
  N := Root;
 if not Converted then
  Path := WideIncludeTrailingPathDelimiter(WideUpperCase(Path));
 L := Length(Path);
 while N <> NIL do if N is TArchiveItem then
 begin
  Result := TArchiveItem(N);
  S := WideUpperCase(Result.FFileName);
  if (Path = '') or ((Length(S) > L) and
     CompareMem(Pointer(Path), Pointer(S), L shl 1)) then
  begin
   if SearchInSubFolders then Exit else
   begin
    System.Delete(S, 1, L);
    if S = WideExtractFileName(S) then Exit;
   end;
  end;
  N := N.Next;
 end;
 Result := NIL;
end;

function TCustomArchive.GetDataFileName: WideString;
begin
 Result := FFileName;
end;

function TCustomArchive.GetFileItem(Index: Integer): TArchiveItem;
begin
 Result := TArchiveItem(Nodes[Index]);
end;

procedure TCustomArchive.Initialize;
begin
 FArchiveTime := DateTimeToFileDate(Now);
 FLoaded := False;
 FUpdated := False;
 FNodeClass := TArchiveItem;
 FItemClass := TArchiveItem;
 FLoadFromFileClass := TLoadFromFile;
 FLoadFromStreamClass := TLoadFromStream;
 FCopyBufferSize := 32768;
 If FCopyBuffer <> NIL then
  ReallocMem(FCopyBuffer, FCopyBufferSize);
 FCanDelete := True;
 FCanAdd := True;
 FCustomParameters := AcceptedParameters;
end;

function TCustomArchive.InitStream: TStream;
var
 H: THandle;
 FName: WideString;
begin
 Result := nil;
 FFreeFileStream := False;
 if FFromFile then
 try
  FName := GetDataFileName;
  H := CreateFileW(PWideChar(FName), GENERIC_READ,
                   FILE_SHARE_READ, nil, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL, 0);
  if H <> INVALID_HANDLE_VALUE then
  Result := THandleStream.Create(H);
  FFreeFileStream := True;
 except
 end else
 begin
  Result := FStream;
  If Result <> nil then
   Result.Position := 0;
 end;
end;

procedure TCustomArchive.LoadArchive;
var Source: TStream;
begin
 Source := InitStream;
 try
  if Source <> NIL then
  begin
   try
    Clear;
    FLastError := Load(Source);
    if FLastError <> E_OK then
     raise EArchiveError.Create('Load error');
    if FRestoreStreamPosition then
     Source.Position := 0;
   finally
    DoneStream(Source);
   end;
  end else
   raise EArchiveError.NoStream('LoadArchive');
  FLoaded := True;
  FUpdated := True;
 except
  Clear;
  FLoaded := False;
  FUpdated := False;
  raise;
 end;
end;

procedure TCustomArchive.SaveToFile(const FileName: WideString);
var
 F: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ or GENERIC_WRITE,
      0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  F := THandleStream.Create(H);
  try
   SaveToStream(F);
  finally
   F.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure TCustomArchive.SaveToStream(Dest: TStream);
var Source: TStream;
begin
 Source := InitStream;
 try
  If (Source <> NIL) or not FLoaded then
  begin
   ApplyParameters;
   FLastError := Save(Source, Dest);
   if FLastError <> E_OK then
    raise EArchiveError.Create('Save error');
   DisposeAdders(True);
  end else
   raise EArchiveError.NoStream('SaveToStream');
 finally
  DoneStream(Source);
 end;
end;

procedure TCustomArchive.SetArchiveTime(Value: Integer);
begin
 If FArchiveTime <> Value then
 begin
  FArchiveTime := Value;
  AfterSetTime;
 end;
end;

procedure TCustomArchive.SetAutoUpdate(Value: Boolean);
begin
 FAutoUpdate := Value;
end;

procedure TCustomArchive.SetFlags(Value: Cardinal);
begin
 If FFlags <> Value then
 begin
  FFlags := Value;
  AfterSetFlags;
 end;
end;

procedure TCustomArchive.UpdateFile;
var
 F: TStream;
 H: THandle;
 BakName, FName: WideString;
 Source: TStream;
begin
 if not FReadOnly and FFromFile then
 begin
  If not FUpdated then
  begin
   BakName := GetDataFileName + '.backup';
   Source := InitStream;
   try
    if (Source <> nil) or not FLoaded then
    try
     H := CreateFileW(PWideChar(BakName), GENERIC_READ or GENERIC_WRITE,
          0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
     if H <> INVALID_HANDLE_VALUE then
     try
      F := THandleStream.Create(H);
      try
       ApplyParameters;
       FLastError := Save(Source, F);
       if FLastError <> E_OK then
        raise EArchiveError.Create('Save error');
      finally
       F.Free;
      end;
     finally
      FileClose(H);
     end;
     FLoaded := True;
     FUpdated := True;
     DisposeAdders(True);
    except
     if WideFileExists(BakName) then DeleteFileW(Pointer(BakName));
     FUpdated := False;
     DisposeAdders;
     raise;
    end else
    begin
     if WideFileExists(BakName) then DeleteFileW(Pointer(BakName));
     raise EArchiveError.NoStream('UpdateFile');
    end;
   finally
    DoneStream(Source);
   end;
   if FUpdated then
   begin
    FName := GetDataFileName;
    DeleteFileW(Pointer(FName));
    MoveFileW(Pointer(BakName), Pointer(FName));
   end else
    DeleteFileW(Pointer(BakName));
  end;
 end else
  raise EArchiveError.Create('Archive data is read only ' +
                             'and cannot be updated via UpdateFile');
end;

//----- TArchiveItem -----//

function TArchiveItem.Compress(Input, Output: TStream): Integer;
begin
 If Assigned(FModifier) then
 begin
  Result := CopyData(Input, Output, FModifier.NewFileSize);
  FModifier.FNewPackSize := FModifier.FNewFileSize;
  FModifier.FNewFlags := Flags;
 end else Result := CopyData(Input, Output, FFileSize);
end;

function TArchiveItem.CopyData(Input, Output: TStream;
                               Size: Int64): Integer;
var FSZ: Int64;
begin
 Result := E_ERROR;
 If Assigned(Input) and Assigned(Output) and Assigned(FOwner) then
 With FOwner do If (FCopyBufferSize > 0) then
 begin
  if FCopyBuffer = nil then GetMem(FCopyBuffer, FCopyBufferSize);
  Result := E_OK;
  FSZ := Size;
  FTotalBytesCopied := 0;
  try
   while FSZ > 0 do
   begin
    If FSZ >= FCopyBufferSize then
    begin
     Input.ReadBuffer(FCopyBuffer^, FCopyBufferSize);
     Output.WriteBuffer(FCopyBuffer^, FCopyBufferSize);
     Dec(FSZ, FCopyBufferSize);
     Result := DoProgress(Self, FCopyBufferSize);
    end Else
    begin
     Input.ReadBuffer(FCopyBuffer^, FSZ);
     Output.WriteBuffer(FCopyBuffer^, FSZ);
     Result := DoProgress(Self, FSZ);     
     FSZ := 0;
    end;
    If Result = E_EABORTED then
     Break;
   end;
  except
   Result := E_ERROR;  
  end;
 end;
end;

constructor TArchiveItem.Create(Owner: TCustomArchive);
begin
 FOwner := Owner;
 FFileName := '';
 FFileAttr := faArchive;
 FFileTime := 0;
 FPosition := 0;
 FFileSize := 0;
 FPackSize := 0;
 FFlags := 0;
 FModifier := NIL;
end;

function TArchiveItem.Decompress(Input, Output: TStream): Integer;
begin
 Result := CopyData(Input, Output, FFileSize);
end;

destructor TArchiveItem.Destroy;
begin
 Finalize(FFileName);
 FModifier.Free;
 inherited;
end;

function TArchiveItem.GetAnsiFileName: AnsiString;
begin
 Result := AnsiString(FFileName);
end;

function TArchiveItem.GetNewFileSize: Int64;
begin
 If Assigned(FModifier) then
  Result := FModifier.NewFileSize Else
  Result := FFileSize;
end;

function TArchiveItem.ProcessFile(Input, Output: TStream): Integer;
begin
 Result := E_ERROR;
 If Assigned(Output) and Assigned(FOwner) then
 begin
  if Assigned(FModifier) then
  begin
   FModifier.FNewPosition := Output.Position;
   Result := FModifier.Process(Output);
  end else if Assigned(Input) then //Copy from original file
  begin
   FModifier := TFileModifier.Create(Self);
   FModifier.FNewPosition := Output.Position;
   Input.Position := Position;
   Result := CopyData(Input, Output, FPackSize); //Returns error value
  end;
 end;
end;

procedure TArchiveItem.SetAnsiFileName(const Value: AnsiString);
begin
 FileName := WideString(Value);
end;

procedure TArchiveItem.SetFileName(const Value: WideString);
begin
 FFileName := Value;
 FNoPathFileName := WideExtractFileName(Value);
end;

//----- TFileModifier -----//

constructor TFileModifier.Create(Owner: TArchiveItem;
                                 DisposeAfterUse: Boolean = False);
begin
 FOwner := Owner;
 FDisposeAfterUse := DisposeAfterUse;
 FNewPosition := Owner.Position;
 FNewFileSize := Owner.FileSize;
 FNewPackSize := Owner.PackSize;
 FNewFlags := Owner.FFlags;
end;

procedure TFileModifier.ApplyChanges;
begin
 With FOwner do
 begin
  FPosition := FNewPosition;
  FFileSize := NewFileSize;
  FPackSize := FNewPackSize;
  FFlags := FNewFlags;
 end;
end;

destructor TFileModifier.Destroy;
begin
 If Assigned(FOwner) and FApply then With FOwner do
 If FFileAttr and faDirectory = 0 then ApplyChanges;
 If FDisposeAfterUse and FApply then Dispose;
 Finalize(FAdditionalData);
 inherited;
end;

function TFileModifier.GetFileSize: Int64;
begin
 FNewFileSize := DoGetFileSize;
 Result := FNewFileSize;
end;

function TFileModifier.DoGetFileSize: Int64;
begin
 Result := FNewFileSize;
end;

function TFileModifier.Process(Output: TStream): Integer;
begin
 Result := E_OK;
end;

procedure TFileModifier.Dispose;
begin
//Do nothing
end;

//----- TLoadFromFile -----//

destructor TLoadFromFile.Destroy;
begin
 Finalize(FSourceFileName);
 inherited;
end;

procedure TLoadFromFile.Dispose;
begin
 If FSourceFileName <> '' then
 begin
  If FOwner.FFileAttr and faDirectory = 0 then
   DeleteFileW(Pointer(FSourceFileName)) Else
   WideRemoveDir(FSourceFileName);
 end;
end;

function TLoadFromFile.Process(Output: TStream): Integer;
var
 F: TStream;
 H: THandle;
begin
 Result := E_ERROR;
 If Assigned(FOwner) and Assigned(Output) and
   (FOwner.FFileAttr and faDirectory = 0) then
 begin
  try
   H := CreateFileW(PWideChar(FSourceFileName), GENERIC_READ,
                    FILE_SHARE_READ, nil, OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL, 0);
   if H <> INVALID_HANDLE_VALUE then
   try
    F := THandleStream.Create(H);
    try
     Result := FOwner.Compress(F, Output); // Returns error value
    finally
     F.Free;
    end;
   finally
    FileClose(H);
   end;
  except
   Result := E_ECREATE;
  end;
 end;
end;

function TLoadFromFile.DoGetFileSize: Int64;
Var SR: TSearchRecW;
begin
 If not FFileSizeGot and (WideFindFirst(FSourceFileName,
    faArchive or faHidden or faReadOnly, SR) = 0) then
 begin
  FFileSizeGot := True;
  Result := SR.Size;
  WideFindClose(SR);
 end Else Result := FNewFileSize;
end;

//----- TLoadFromStream -----//

function TLoadFromStream.DoGetFileSize: Int64;
begin
 If Assigned(FSourceStream) then
  Result := FSourceStream.Size Else
  Result := 0;
end;

procedure TLoadFromStream.Dispose;
begin
 FSourceStream.Free;
end;

function TLoadFromStream.Process(Output: TStream): Integer;
begin
 If Assigned(FOwner) and Assigned(Output) then
 begin
  try
   FSourceStream.Seek(0, soFromBeginning);
   Result := FOwner.Compress(FSourceStream, Output); // Returns error value
  except
   Result := E_ERROR;
  end;
 end else
  raise EArchiveError.BadArguments('Process');
end;

{ EArchiveError }

constructor EArchiveError.BadArguments(const MethodName: AnsiString);
begin
 CreateFmt('Error in %s.%s: Bad arguments', [ClassName, MethodName]);
end;

constructor EArchiveError.NoStream(const MethodName: AnsiString);
begin
 CreateFmt('Stream initialization error in %s.%s', [ClassName, MethodName]);
end;

end.
