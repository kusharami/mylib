unit BitmapEx;

interface

uses Windows, SysUtils, Classes, HexUnit, BmpImg, NodeLst, MyClasses;

const
 CONVERTER_CODE_SIZE = 256;

 CF_RGB = 'RGB888';
 CF_BGRZ = 'BGRZ8888';
 CF_BGRA = 'BGRA8888';
 CF_RGBA = 'RGBA8888';
 CF_RGBZ = 'RGBZ8888';
 CF_ARGB = 'ARGB8888';
 CF_MCGA = 'RZGZB62626';
 CF_GRAY = 'GRAY8';
 CFLG_NORMAL     = 0;
 CFLG_RGBQUAD    = 1;
 CFLG_GRAYSCALE  = 2;
 CFLG_ALPHA      = 1 shl 2;
 CFLG_BYTESWAP_SHIFT = 3;
 CFLG_BYTESWAP   = 1 shl CFLG_BYTESWAP_SHIFT;
 CFLG_8888       = 1 shl 4;

 // First 5 bits is an Image Bits Count minus 1
 IMG_BITS_REVERSE   = 1 shl 6; // if set then read bits from right to left
 IMG_PIXEL_BSWAP    = 1 shl 6; // if set then read pixel bytes from right to left
 IMG_PLANAR         = 1 shl 7; // if set then image have more than one plane
 IMG_PLANE_REVERSE  = 1 shl 8; // if set then read planes from last to first
 IMG_PLANE_VECTOR   = 1 shl 9; // if set then the image line made from planes
 IMG_FLIPS_SHIFT    = 10;
 IMG_BYTES_REVERSE  = 1 shl (IMG_FLIPS_SHIFT + 0); // if set then line bytes read from right to left
 IMG_Y_FLIP         = 1 shl (IMG_FLIPS_SHIFT + 1); // if set then lines read from last to first
 IMG_X_FLIP         = IMG_BYTES_REVERSE;
 IMG_X_FLIP4        = IMG_BYTES_REVERSE or IMG_BITS_REVERSE;
 IMG_BITS_SHIFT     = 1 shl 12; // sets when DRAW_X_FLIP
// IMG_ANGLE45        = 1 shl 13;
 IMG_COMPRESSED     = 1 shl 13; // may be compressed in linear mode only
                               // only transparent pixels are compressed
 PLANE_FORMAT_SHIFT = 14; //2 bits
 IMG_PLANE_1BIT     = 0 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_2BIT     = 1 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_4BIT     = 2 shl PLANE_FORMAT_SHIFT;
 IMG_PLANE_8BIT     = 3 shl PLANE_FORMAT_SHIFT;
 IMG_FORMAT_MASK = (1 shl PLANE_FORMAT_SHIFT) - 1;
 //Internal draw flags
 DRAW_BITS          = 1;
 DRAW_PIXEL_MODIFY_SHIFT = 1;
 DRAW_PIXEL_MODIFY  = 1 shl DRAW_PIXEL_MODIFY_SHIFT;
 DRAW_VALUE_CONVERT = 1 shl 2;
 DRAW_SRC_COMPOSITE = 1 shl 3;
 DRAW_DST_COMPOSITE = 1 shl 4;
 DRAW_SRC_PROC      = 1 shl 5;
 DRAW_DST_PROC      = 1 shl 6;
 DRAW_COMPRESSED    = 1 shl 7;
 //User draw flags
 DRAW_FLIPS_SHIFT   = 8;
 DRAW_X_FLIP        = 1 shl (DRAW_FLIPS_SHIFT + 0);
 DRAW_Y_FLIP        = 1 shl (DRAW_FLIPS_SHIFT + 1);
 DRAW_XY_FLIP       = DRAW_X_FLIP or DRAW_Y_FLIP;
 DRAW_TRANSP_SHIFT  = 10;
 DRAW_TRANSPARENT   = 1 shl DRAW_TRANSP_SHIFT; // transparent if equal
 DRAW_TRANSP_AE     = 2 shl DRAW_TRANSP_SHIFT; // transparent if above or equal
 DRAW_TRANSP_BE     = 3 shl DRAW_TRANSP_SHIFT; // transparent if below or equal
 DRAW_TRANSP_MASK   = 3 shl DRAW_TRANSP_SHIFT;
 DRAW_ALPHA_BLEND   = 1 shl 12;
 DRAW_NO_CONVERT    = 1 shl 13; // Move values from source to dest without any conversion
 //Converter flags
 SRC_IMGFMT_SHIFT = 16;
 SRC_INDEXED      = 0 shl SRC_IMGFMT_SHIFT;
 SRC_GRAYSCALE    = 1 shl SRC_IMGFMT_SHIFT;
 SRC_RGB          = 2 shl SRC_IMGFMT_SHIFT;
 DST_IMGFMT_SHIFT = 16 + 2;
 DST_INDEXED      = 0 shl DST_IMGFMT_SHIFT;
 DST_GRAYSCALE    = 1 shl DST_IMGFMT_SHIFT;
 DST_RGB          = 2 shl DST_IMGFMT_SHIFT;


type
 TColorFormat = class
  private
    FColorBits: LongInt;   //0
    FUsedBits: LongInt;    //1
    FColorSize: LongInt;   //2
    FFlags: LongWord;      //3

    FBlueMask: LongWord;   //4
    FGreenMask: LongWord;  //5
    FRedMask: LongWord;    //6
    FAlphaMask: LongWord;  //7

    FBlueShift: Byte;      //8
    FGreenShift: Byte;
    FRedShift: Byte;
    FAlphaShift: Byte;

    FBlueRShift: Byte;     //9
    FGreenRShift: Byte;
    FRedRShift: Byte;
    FAlphaRShift: Byte;

    FBlueCount: Byte;      //10
    FGreenCount: Byte;
    FRedCount: Byte;
    FAlphaCount: Byte;
    function GetAlpha: Boolean;
    function GetByteSwap: Boolean;
    procedure SetAlpha(Value: Boolean);
    procedure SetByteSwap(Value: Boolean);
    function GetChannelsCount: Integer;
    function GetFormatString: AnsiString;
  public
    procedure SetFormat(const Format: AnsiString); overload;        
  public
    property ColorSize: LongInt read FColorSize write FColorSize;
    property UsedBits:  LongInt read FUsedBits write FUsedBits;
    property ColorBits: LongInt read FColorBits;
    property Flags:  LongWord read FFlags write FFlags;
    property Alpha: Boolean read GetAlpha write SetAlpha;
    property ByteSwap: Boolean read GetByteSwap write SetByteSwap;
    property ChannelsCount: Integer read GetChannelsCount;
    
    property BlueMask: LongWord read FBlueMask;
    property GreenMask: LongWord read FGreenMask;
    property RedMask: LongWord read FRedMask;
    property AlphaMask: LongWord read FAlphaMask;

    property BlueCount: Byte read FBlueCount;
    property GreenCount: Byte read FGreenCount;
    property RedCount: Byte read FRedCount;
    property AlphaCount: Byte read FAlphaCount;

    property BlueShift: Byte read FBlueShift;
    property GreenShift: Byte read FGreenShift;
    property RedShift: Byte read FRedShift;
    property AlphaShift: Byte read FAlphaShift;

    property BlueByteShift: Byte read FBlueRShift;
    property GreenByteShift: Byte read FGreenRShift;
    property RedByteShift: Byte read FRedRShift;
    property AlphaByteShift: Byte read FAlphaRShift;

    property FormatString: AnsiString read GetFormatString write SetFormat;

    constructor Create; overload;
    constructor Create(const Format: AnsiString); overload;
    constructor CreateFromMask(RBitMask, GBitMask, BBitMask, ABitMask: LongWord);

    procedure SetFormat(RBitMask, GBitMask, BBitMask, ABitMask: LongWord); overload;
    procedure Assign(ClrFmt: TColorFormat);
    procedure AssignMask(ClrFmt: TColorFormat);

    function IsGray: Boolean;
    function ValueToRGBQuad(Value: LongWord): LongWord;  
    function ToRGBQuad(const Source): LongWord;
    function FromRGBQuad(Source: LongWord): LongWord;
    function Compress(Source, Dest: Pointer;
             Count: Integer; SkipValue: LongWord): Integer;
    procedure Decompress(Source, Dest: Pointer);
    procedure CompressToRGBQuad(Source, Dest: Pointer; Count: Integer; SkipValue: LongWord);
    procedure DecompressToRGBQuad(Source, Dest: Pointer);
    function ConvertValue(DestFormat: TColorFormat; const Source): LongWord; overload;
    function ConvertValue(const DestFormat: AnsiString; const Source): LongWord; overload;
    procedure ConvertToRGBQuad(Source, Dest: Pointer; Count: Integer);
    procedure ConvertFromRGBQuad(Source, Dest: Pointer; Count: Integer);
    procedure ConvertTo(DestFormat: TColorFormat; Source, Dest: Pointer;
       Count: Integer); overload;
    procedure ConvertTo(DestFormat: AnsiString;
                        Source, Dest: Pointer; Count: Integer); overload;
    procedure SetColor(Dest: Pointer; Index: Integer; RGBQuad: LongWord); overload;
    procedure SetColor(var Dest; RGBQuad: LongWord); overload;    
    function SameAs(const CFormat: TColorFormat): Boolean;
 end;

 TColorTable = class(TBaseSectionedList)
  private
    FColorFormat: TColorFormat;
    FColorData: Pointer;
    FColorsCount: Integer;
    FTransparentIndex: Integer;
    procedure SetColorsCount(Value: Integer);
    function GetTableSize: Integer;
    function GetColor(X: Integer): LongWord;
    procedure SetColor(X: Integer; Value: LongWord);
    function GetRGBQuad(X: Integer): LongWord;
    procedure SetRGBQuad(X: Integer; Value: LongWord);
    function GetFormatString: AnsiString;
    procedure SetFormatString(const Value: AnsiString);
    procedure SetColorFormat(Value: TColorFormat);
    function GetColorPtr(X: Integer): Pointer;
  protected
    procedure Initialize; override;

    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); override;
    procedure FillHeader(var Header: TSectionHeader); override;
    function CalculateChecksum: TChecksum; override;
    function CalculateDataSize: LongInt; override;
  public
    property ColorFormatString: AnsiString read GetFormatString write SetFormatString;
    property ColorFormat: TColorFormat read FColorFormat write SetColorFormat;
    property ColorData: Pointer read FColorData;
    property ColorsCount: Integer read FColorsCount write SetColorsCount;
    property TransparentIndex: Integer read FTransparentIndex write FTransparentIndex;
    property TableSize: Integer read GetTableSize;
    property RGBZ[X: Integer]: LongWord read GetColor write SetColor;
    property BGRA[X: Integer]: LongWord read GetRGBQuad write SetRGBQuad;
    property ColorPtr[X: Integer]: Pointer read GetColorPtr;

    procedure Reset(AColorFmt: TColorFormat; ACount: Integer = -1); overload;
    procedure Reset(const AColorFmt: AnsiString; ACount: Integer = -1); overload;
    procedure LoadActFromStream(Stream: TStream);
    procedure SaveActToStream(Stream: TStream);
    procedure Assign(Source: TNode); override;
    destructor Destroy; override;
 end;

 TColorTableList = class(TSectionedList)
  private
    function GetColTblItem(Index: Integer): TColorTable;
  protected
    procedure Initialize; override;
  public
    property Items[Index: Integer]: TColorTable read GetColTblItem; default;

    function AddColorTable(const AName: WideString; AColorFmt: TColorFormat;
                           ACount: Integer = 256): TColorTable; overload;
    function AddColorTable(const AName: WideString;
                           const AColorFmt: AnsiString;
                           ACount: Integer = 256): TColorTable; overload;
 end;

 EColorTableFormatError = class(Exception);

 TCompositeFlags = array of Word;

 TImageFormat =
 (ifIndexed,   // For indexed color image data.
  ifRGB);      // Full color image data. Can include alpha channel.

 TBitmapConverter = class;

 TBitmapContainer = class
  private
    function GetImageSize: Integer;
    function GetBitsCountTotal: Integer;
    function GetPaletteSize: Integer;
    function GetPaletteCount: Integer;
    function GetFlagsList: TCompositeFlags;
    procedure SetFlagsList(const Value: TCompositeFlags);
  protected
    FImageData:        Pointer;
    FColorTable:       Pointer;
    FColorFormat:      TColorFormat;
    FLeft:             Integer;
    FTop:              Integer;
    FWidth:            Word;
    FWidthRemainder:   Word;
    FHeight:           Word;
    FImageFlags:       Word;
    FTransparentColor: LongWord;
    FPixelModifier:    LongWord;
    FConvertFrom:      Pointer;
    FConvertTo:        Pointer;
    FCompressedSize:   Integer;
    FCompressed:       Boolean;
    FImageFormat:      TImageFormat;
  public
    CompositeFlags: TCompositeFlags;
    property PixelModifier: LongWord read FPixelModifier write FPixelModifier;
    property PixelConvertFrom: Pointer read FConvertFrom write FConvertFrom;
    property PixelConvertTo: Pointer read FConvertTo write FConvertTo;
    property Compressed: Boolean read FCompressed write FCompressed;
    property CompressedSize: Integer read FCompressedSize write FCompressedSize;
    property ImageData: Pointer read FImageData write FImageData;
    property ColorTable: Pointer read FColorTable write FColorTable;
    property ColorFormat: TColorFormat read FColorFormat write FColorFormat;
    property TransparentColor: LongWord read FTransparentColor write FTransparentColor;
    property Left: Integer read FLeft write FLeft;
    property Top: Integer read FTop write FTop;
    property Width: Word read FWidth write FWidth;
    property WidthRemainder: Word read FWidthRemainder write FWidthRemainder;
    property Height: Word read FHeight write FHeight;
    property ImageFormat: TImageFormat read FImageFormat write FImageFormat;
    property ImageFlags: Word read FImageFlags write FImageFlags;
    property FlagsList: TCompositeFlags read GetFlagsList write SetFlagsList;
    property BitsCount: Integer read GetBitsCountTotal;
    property ImageSize: Integer read GetImageSize;
    property PaletteSize: Integer read GetPaletteSize;
    property PaletteCount: Integer read GetPaletteCount;

    function SameAs(Image: TBitmapContainer): Boolean;
    function CompareImage(Image: TBitmapContainer): Boolean;
    procedure SetFlags(const Flags: array of Word);
    procedure SetInfo(Width, Height: Integer;
            Fmt: TImageFormat; ClrFmt: TColorFormat = nil; Rmndr: Integer = 0);
    procedure Draw(Dest: TBitmapContainer; X, Y: Integer; Flags: Integer = 0);
              overload;
    procedure Draw(Dest: TBitmapContainer; X, Y: Integer; Cvt: TBitmapConverter);
              overload;
    procedure FillData(ColorValue: LongWord);
 end;

 PConvertInfo = ^TConvertInfo;
 TValueCvtProc = function (Value: LongWord; Src, Dst: PConvertInfo): LongWord;
 TGetLongInt = function: LongInt;
 TConvertInfo = packed record
  Pntr: PByte;
  W: Word;
  H: Word;
  Flags: Word;
  InitialBitPos: Byte;
  BitPos: Byte;
  Prev: PConvertInfo;
  Next: PConvertInfo;
  ClrTbl: Pointer;
  ClrFmt: TColorFormat;
  PixelModifier: LongInt;
  UseProcs: Byte;
  BitsCount: Byte;  
  case Byte of
   0: (RowStride: LongInt;          //0
       TransparentColor: LongWord;  //1
       ReadProc: TGetLongInt;       //2
       ADstConvert: TGetLongInt;    //3
       ColorConvert: TGetLongInt);  //4
   1: (WordRowStride: SmallInt;     //0
       InitCounter: Word;
       CmprDataPtr: PByte;          //1
       WriteProc: TProcedure;       //2
       ValueConvert: TGetLongInt;   //3
       SkipProc: TProcedure;        //4
       DstReadProc: TGetLongInt;    //5
       Counter1: Byte;              //6
       Counter2: Byte);
 end;

 TConvertProc = procedure(Src, Dst: PConvertInfo);

 TBitmapConverter = class
  private
   FSrcInfo: PConvertInfo;
   FDstInfo: PConvertInfo;
   FConvert: TConvertProc;
   FProgramSize: Integer;
  public
   property SrcInfo: PConvertInfo read FSrcInfo;
   property DstInfo: PConvertInfo read FDstInfo;
   property Convert: TConvertProc read FConvert;
   property PrgSize: Integer read FProgramSize;
   constructor Create; overload;
   constructor Create(const SrcFlags, DstFlags: array of Word;
                   DrawFlags: Integer = 0;
                   ConvertFrom: Pointer = nil;
                   ConvertTo: Pointer = nil); overload;
   procedure Reset(const SrcFlags, DstFlags: array of Word;
                   DrawFlags: Integer = 0;
                   ConvertFrom: Pointer = nil;
                   ConvertTo: Pointer = nil);
   destructor Destroy; override;
 end;

 TCustomImageFile = class;

 TImageFileClass = class of TCustomImageFile;

 PFrameInfo = ^TFrameInfo;
 TFrameInfo = packed record
  fiImage: Pointer;
  fiPalette: Pointer;
  fiImgCmprSize: LongInt;
  fiPalCmprSize: LongInt;
  fiDelay: LongWord;
  fiNext: PFrameInfo;
 end;

 TCustomImage = class(TBitmapContainer)
  private
   FImageFileClass: TImageFileClass;
   FResultData: Cardinal;
   FColorsUsed: LongInt;
   FFramesCount: Integer;
   FLast: PFrameInfo;
   FCurrent: PFrameInfo;
   procedure SetCurrentFrame(Value: PFrameInfo);
  public
 //  FInternalImage: Pointer;
 //  FInternalPalette: Pointer;
   FirstFrame: TFrameInfo;
   FInternalClrFmt: TColorFormat;
   property FramesCount: Integer read FFramesCount;
   property ColorsUsed: LongInt read FColorsUsed write FColorsUsed;
   property ResultData: Cardinal read FResultData;
   property CurrentFrame: PFrameInfo read FCurrent write SetCurrentFrame;
   property ImageFileClass: TImageFileClass read FImageFileClass write FImageFileClass;
   procedure Reallocate(Width, Height: Integer;
                        Fmt: TImageFormat;
                        const Flags: array of Word;
                        ClrFmt: TColorFormat = nil;
                        ClrTbl: Pointer = nil;
                        Rmndr: Integer = -1);

   procedure Compress(Frame: PFrameInfo);
   procedure Decompress(Frame: PFrameInfo);
   procedure CompressPalette(Frame: PFrameInfo; SkipColor: LongWord; Cnt: Integer = 0);
   procedure Clear;
   function AddFrame: PFrameInfo;
   constructor Create(FileClass: TImageFileClass = nil);
   destructor Destroy; override;
   procedure LoadFromStream(Stream: TStream);
   procedure SaveToStream(Stream: TStream);
   procedure LoadFromFile(const FileName: String);
   procedure SaveToFile(const FileName: String);
 end;

 TCustomImageFile = class
  protected
   FImageInfo: TCustomImage;
   FResultData: Cardinal;
  public
   constructor Create(AImageData: TCustomImage);
   function GetResultData: Cardinal; virtual;
   procedure LoadFromStream(Stream: TStream); virtual; abstract;
   procedure SaveToStream(Stream: TStream); virtual; abstract;
 end;

 TBitmapExFile = class(TCustomImageFile)
  public
   procedure LoadFromStream(Stream: TStream); override;
   procedure SaveToStream(Stream: TStream); override;
 end;

 TBitmapFile = class(TCustomImageFile)
  public
   procedure LoadFromStream(Stream: TStream); override;
   procedure SaveToStream(Stream: TStream); override;
 end;

 TZSoftPCX = class(TCustomImageFile)
  public
   procedure LoadFromStream(Stream: TStream); override;
   procedure SaveToStream(Stream: TStream); override;
 end;

 EImageHolderError = class(Exception);
 EImageExError = class(Exception);
 EColorFormatError = class(Exception);

var
 DefaultColorFormat: TColorFormat = nil;
 DefaultColorTable: Pointer = nil;
 BGRZ_ColorFormat: TColorFormat;
 BGRA_ColorFormat: TColorFormat;
 RGB_ColorFormat: TColorFormat;
 RGBZ_ColorFormat: TColorFormat;
 GrayScaleFormat: TColorFormat;
 MMX_Present: Boolean;

type
 TPlanarFmt = (pfMatrix, pfVector);

 TColorFormatRec = packed record
  colFlags:     LongWord;
  colSize:      Byte;
  colReserved1: Byte;
  colBits:      Word;
  colBlueMask:  LongWord;
  colGreenMask: LongWord;
  colRedMask:   LongWord;
  colAlphaMask: LongWord;
 end;
 
 TBitmapExFileHeader = packed record
  exMagic:            array[0..3] of AnsiChar;
  exHeaderSize:       LongInt;
  exImageSize:        LongInt;
  exLeft:             LongInt;
  exTop:              LongInt;
  exWidth:            Word;
  exWidthRemainder:   Word;
  exHeight:           Word;
  exImageFlags:       Word;
  exCompositeCount:   Byte;
  exImageFormat:      TImageFormat;
  exExtraFlags:       Word;
  exTransparentColor: LongWord;
  exColorsCount:      LongInt;
  exColorFormat:      TColorFormatRec;
 end;

 TMilaDataHeader = packed record
  dhHeaderID: array[0..3] of AnsiChar;
  dhHeaderSize: LongInt;
 end;

 TMilaFrameHeader = packed record
  fhPaletteSize: LongInt;
  fhImageSize:   LongInt;  
  fhDelay:       LongWord;
 end;

const
 BMPEX_MAGIC = $414C494D;
 MILA_FRAME = $4D52464D;
 MILA_END = $444E454D;

function Linear(BitsCount: Integer; BytesReverse: Boolean = False): Word;
function Planar(BCnt, PlaneBCnt: Integer; Fmt: TPlanarFmt = pfMatrix;
                BytesReverse: Boolean = False): Word;
function GetPlaneBitsCount(Flags: Word): Integer;
function GetImageLineSize(Width, Rmndr, Flags: Word): Integer;
function FlagsGetBitsCount(const Flags: array of Word): Integer;

procedure FileFormatError;
procedure ImageFormatError;

const
 CLST_SIGN =  Ord('C') or
             (Ord('L') shl 8) or
             (Ord('S') shl 16) or
             (Ord('T') shl 24);
 CTBL_SIGN =  Ord('C') or
             (Ord('T') shl 8) or
             (Ord('B') shl 16) or
             (Ord('L') shl 24);

type
  TPicHeader = packed record
                 bhHead: TBitmapHeader;
                 bhPalette: TRGBQuads;
                end;

procedure FillPicHeader(AWidth, AHeight: Integer;
                        var APicHeader: TPicHeader;
                        var APicData: TBitmapContainer);

procedure DrawPic(DC: HDC; const DestRect: TRect;
                   const Header: TPicHeader; Data: Pointer);
                                           
implementation

procedure DrawPic(DC: HDC; const DestRect: TRect;
                   const Header: TPicHeader; Data: Pointer);
begin
 GdiFlush;
 StretchDIBits(DC,
               DestRect.Left, DestRect.Top,
               DestRect.Right - DestRect.Left,
               DestRect.Bottom - DestRect.Top,
               0, 0,
               Header.bhHead.bhInfoHeader.biWidth,
               Header.bhHead.bhInfoHeader.biHeight,
               Data,
               TBitmapInfo(Addr(Header.bhHead.bhInfoHeader)^),
               DIB_RGB_COLORS, SRCCOPY);
end;

procedure FillPicHeader(AWidth, AHeight: Integer;
                        var APicHeader: TPicHeader;
                        var APicData: TBitmapContainer);
var
 WidthBytes: Integer;
 P: Pointer;
begin
 WidthBytes := FillBitmapHeader(@APicHeader, AWidth, AHeight, 8);
 P := APicData.ImageData;
 ReallocMem(P, WidthBytes * AHeight);
 APicData.Width := AWidth;
 APicData.WidthRemainder := WidthBytes - AWidth;
 APicData.Height := AHeight;
 APicData.ImageData := P;
end;

function FlagsGetBitsCount(const Flags: array of Word): Integer;
var
 I: Integer;
begin
 Result := 0;
 for I := 0 to Length(Flags) - 1 do
  Inc(Result, (Flags[I] and 31) + 1);
end;

type
 TMainHeader = packed record
  mhSignature: LongWord;
  mhHeaderSize: LongInt;
 end;

 TColorTableHeader = packed record
  cthColorFmt: TColorFormatRec;
  cthTransparentIndex: LongInt;
  cthColorCnt: Word;
  cthNameLength: Word;
 end;

type
 PConvertInitInfo = ^TConvertInitInfo;
 TConvertInitInfo = packed record
  DrawFlags: Word;
  SrcFlags:  Word;
  DstFlags:  Word;
  ConvertFrom: Pointer;
  ConvertTo: Pointer;
 end;
(*
 TConvertInitProc = function(Info: PConvertInitInfo; Dest: Pointer): LongInt;
 PConvertProgRec = ^TConvertProgRec;
 TConvertProgRec = packed record
  Init:          TConvertInitProc;
  Convert:       TConvertProc;
 end; *)

 TChannelType = (ctDummy, ctAlpha, ctRed, ctGreen, ctBlue, ctGray);

procedure ColorTableFormatError;
begin
 raise EColorTableFormatError.Create('Color table data format error.');
end;

function HaveMMX: Boolean;
asm
 push ebx
 xor eax,eax
 cpuid
 cmp eax,1
 jnl @NotFail
 xor eax,eax
 pop ebx
 ret
@NotFail:
 xor edx,edx
 mov eax,1
 cpuid
 bt edx,23
 setc al
 pop ebx
end;

function AddInfo(Current: PConvertInfo; Flags, DrawFlags, DW, WR, W, H: Word): PConvertInfo;
var
 I: Integer;
begin
 New(Result);
 if Current <> nil then
 begin
  Result.UseProcs := Current.UseProcs;
  Current.Next := Result;
 end;
 if DrawFlags and DRAW_Y_FLIP <> 0 then
  Flags := Flags xor IMG_Y_FLIP;
 if (DrawFlags and DRAW_COMPRESSED <> 0) then
  Flags := (Flags and not (IMG_PLANAR or IMG_BYTES_REVERSE or IMG_BITS_SHIFT))
          or IMG_COMPRESSED else
 if DrawFlags and DRAW_X_FLIP <> 0 then
  Flags := Flags xor (IMG_BYTES_REVERSE or IMG_BITS_SHIFT);
 Result.Pntr := nil;
 LongRec(Result.RowStride).Lo := DW;
 LongRec(Result.RowStride).Hi := WR;
 Result.W := W;
 Result.H := H;
 Result.Prev := Current;
 Result.Next := nil;
 if Flags and IMG_PLANAR <> 0 then
 begin
  if DrawFlags and DRAW_X_FLIP <> 0 then
   Flags := Flags xor IMG_BITS_REVERSE;
  Result.Flags := Flags;
  Current := Result;
  for I := 1 to GetPlanarLineSize(1,
                1 shl (Flags shr PLANE_FORMAT_SHIFT),
                Flags and 31 + 1) - 1 do
  begin
   New(Result);
   Current.Next := Result;
   LongRec(Result.RowStride).Lo := DW;
   LongRec(Result.RowStride).Hi := WR;
   Result.Pntr := nil;
   Result.UseProcs := 1;
   Result.W := W;
   Result.H := H;
   Result.Flags := Flags;
   Result.Prev := Current;
   Result.Next := nil;
   Current := Result;
  end;
 end else
 begin
  if (Flags and IMG_COMPRESSED = 0) and
     (DrawFlags and DRAW_X_FLIP <> 0) and (Flags and 31 < 4) then
    Flags := Flags xor IMG_BITS_REVERSE;
  Result.Flags := Flags;
 end;
end;

procedure ClearInfo(Root: PConvertInfo);
var
 N, Prv: PConvertInfo;
begin
 if Root <> nil then
  Prv := Root.Prev else
  Prv := nil;
 while Root <> nil do
 begin
  N := Root;
  Root := N.Next;
  Dispose(N);
 end;
 while Prv <> nil do
 begin
  N := Prv;
  Prv := N.Prev;
  Dispose(N);
 end;
end;

function Linear(BitsCount: Integer; BytesReverse: Boolean): Word;
begin
 Result := (Abs(BitsCount) - 1) and 31;
 if BitsCount < 0 then
  Result := Result or IMG_BITS_REVERSE or IMG_PIXEL_BSWAP;
 if BytesReverse then
  Result := Result or IMG_BYTES_REVERSE;
end;

function Planar(BCnt, PlaneBCnt: Integer;
                Fmt: TPlanarFmt; BytesReverse: Boolean): Word;
begin
 Result := ((Abs(BCnt) - 1) and 31) or IMG_PLANAR;
 if PlaneBCnt < 0 then
  Result := Result or IMG_BITS_REVERSE;
 if Fmt <> pfMatrix then
  Result := Result or IMG_PLANE_VECTOR;
 if BCnt < 0 then
  Result := Result or IMG_PLANE_REVERSE;
 case Abs(PlaneBCnt) of
  1:   Result := Result or IMG_PLANE_1BIT;
  2:   Result := Result or IMG_PLANE_2BIT;
  3,4: Result := Result or IMG_PLANE_4BIT;
  else Result := Result or IMG_PLANE_8BIT;
 end;
 if BytesReverse then
  Result := Result or IMG_BYTES_REVERSE;
end;

function GetPlaneBitsCount(Flags: Word): Integer;
begin
 Result := 1 shl (Flags shr PLANE_FORMAT_SHIFT);
end;

function GetImageLineSize(Width, Rmndr, Flags: Word): Integer;
var
 BCnt: Integer;
begin
 BCnt := (Flags and 31) + 1;
 if Flags and IMG_PLANAR <> 0 then
  Result := GetPlanarLineSizeRmndr(Width, Rmndr, GetPlaneBitsCount(Flags), BCnt) else
  Result := BmpImg.GetLineSize(Width, BCnt) + Rmndr;
end;

function PaletteConvert: Longword;//(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
// ebp = dst info
asm
end;
{begin
 Result := Value;
end;}

function RGBQuadToIndexed: LongWord;
asm
end;

function IndexedToRGBQuad: Longword;//(Value: LongInt; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
asm
 mov ecx,[ebx + TConvertInfo.ClrFmt]
 mov edx,[ecx + TColorFormat.FColorSize]
 mul edx
 mov edx,[ebx + TConvertInfo.ClrTbl]
 add edx,eax
 mov eax,ecx
 call TColorFormat.ToRGBQuad
end;

function IndexedToRGB: Longword;//(Value: LongInt; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
// ebp = dst info
asm
 mov ecx,[ebx + TConvertInfo.ClrFmt]
 mov edx,[ecx + TColorFormat.FColorSize]
 mul edx
 mov edx,[ebx + TConvertInfo.ClrTbl]
 add edx,eax
 mov eax,ecx
 call TColorFormat.ToRGBQuad
 mov edx,eax
 mov eax,[ebp + TConvertInfo.ClrFmt]
 call TColorFormat.FromRGBQuad
end;
{begin
 with Src.SrcClr^, ColorFormat do
  Result := Src.DstClr.ColorFormat.FromRGBQuad
  (
   ToRGBQuad(PByteArray(ColorTable)[Value * FColorSize])
  );
end;}

function RGBtoIndexed: Longword;//(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
// ebp = dst info
asm
end;
{begin
 Result := Value;
end;}

function RGBQuadToRGB: Longword;//(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebp = dst info
asm
 mov edx,eax
 mov eax,[ebp + TConvertInfo.ClrFmt]
 call TColorFormat.FromRGBQuad
end;

function RGBtoRGBQuad: Longword;//(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
asm
 mov edx,eax
 mov eax,[ebx + TConvertInfo.ClrFmt]
 call TColorFormat.ValueToRGBQuad
end;

function RGBConvert: Longword;//(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
// ebp = dst info
asm
 mov edx,eax
 mov eax,[ebx + TConvertInfo.ClrFmt]
 call TColorFormat.ValueToRGBQuad
 mov edx,eax
 mov eax,[ebp + TConvertInfo.ClrFmt]
 call TColorFormat.FromRGBQuad
end;
{begin
 Result := Src.DstClr.ColorFormat.FromRGBQuad
 (
  Src.SrcClr.ColorFormat.ValueToRGBQuad(Value)
 );
end;}

function AlphaBlend: Longword; //(Value: LongWord; Src, Dst: PConvertInfo): LongWord;
// eax = value
// ebx = src info
// ebp = dst info
asm
 mov ecx,[ebx + TConvertInfo.ColorConvert]
 test ecx,ecx
 jz @@NoSrcConvert
 call ecx
@@NoSrcConvert:
 push eax // save source color
 shr eax,24 // get alpha value
 xor ecx,ecx
 xor edx,edx
 test al,al // if alpha value is zero then store get destination value and exit
 jz @@OnlyDestinationRead
 inc eax // real alpha value
 movd mm0,eax // mm0 = alpha value
 call dword ptr [TConvertInfo.DstReadProc + ebp]
 mov edx,dword ptr [TConvertInfo.Next + ebp]
 test edx,edx
 jz @@nonext
 xchg eax,edx
 call @@drl
@@nonext:
 mov ecx,[ebx + TConvertInfo.ADstConvert]
 test ecx,ecx
 jz @@Blend
 xchg ebx,ebp
 call ecx
 xchg ebp,ebx
@@Blend:
 pop edx
 movd mm1,edx // mm1 = source color
 movd mm2,eax // mm2 = destination color
 punpcklwd mm0,mm0 // 000000FF -> 00FF00FF (filling mm0 with alpha value)
 punpcklwd mm0,mm0 // 00FF00FF -> 00FF00FF00FF00FF
 punpcklbw mm1,mm1 // unpack 32 bit src color value to 64 bit
 psrlw mm1,8 // clear redundant bytes
 punpcklbw mm2,mm2 // unpack 32 bit dst color value to 64 bit
 psrlw mm2,8 // clear redundant bytes
 psubw mm1,mm2 // mm1 = src colors - dst colors
 pmullw mm0,mm1 // mm0 = alpha value * composition of colors
 psrlw mm0,8 // div 256
 paddb mm0,mm2 // mm0 = mm0 + dst colors
 packuswb mm0,mm0
 or edx,eax // merging alpha values
 and edx,$FF000000 // eax = only new alpha value
 movd eax,mm0
 and eax,$00FFFFFF
 or eax,edx // set alpha value
 emms
 mov ecx,[ebp + TConvertInfo.ColorConvert]
 test ecx,ecx
 jz @@NoDstConvert
 call ecx
@@NoDstConvert:
 ret
@@OnlyDestinationRead:
 pop eax
 call dword ptr [TConvertInfo.DstReadProc + ebp]
 mov edx,dword ptr [TConvertInfo.Next + ebp]
 test edx,edx
 jnz @@odrl
 ret
@@odrl:
 xchg eax,edx
 jmp @@drl
@@DestinationReadLoop:
 call dword ptr [TConvertInfo.DstReadProc + ebp]
 shl eax,cl
 or edx,eax
 mov eax,dword ptr [TConvertInfo.Next + ebp]
 test eax,eax
 jz @@rc
@@drl:
  add cl,byte ptr [TConvertInfo.BitsCount + ebp]
  mov ebp,eax
  call @@DestinationReadLoop
  mov ebp,dword ptr [TConvertInfo.Prev + ebp]
 @@rc:
end;

procedure InitBitsPositions(Info: PConvertInfo);
asm
 push esi
 mov esi,eax //ESI = Src
@LoopStart:
 xor edx,edx
 mov cx,[TConvertInfo.Flags + esi]
 test cx,IMG_PLANAR
 jz @@NoPlanar
 shr cx,PLANE_FORMAT_SHIFT
 mov dl,1
 shl dl,cl
 jmp @@gr1
@@NoPlanar:
 mov dl,cl
 and dl,31
 inc dl
 cmp dl,3
 jne @@gr1
 inc dl
@@gr1:
 cmp dl,5
 jb @@NoFullByte
 add dl,7
 shr dl,3
 shl dl,3
 mov [TConvertInfo.BitsCount + esi],dl 
 jmp @@grNext
@@NoFullByte:
 mov [TConvertInfo.BitsCount + esi],dl
 xor eax,eax
 test word ptr [TConvertInfo.Flags + esi],IMG_BITS_SHIFT
 jz @@EvenWidth
 mov al,byte ptr [edx + PixelsInByteM - 1]
// mov al,8
// div dl
// dec al
 mov ah,byte ptr [TConvertInfo.W + esi]
 test ah,al
 jnz @@OddWidth
 xor al,al
 jmp @@EvenWidth
@@OddWidth:
 mov dl,ah
 or ah,al
 inc ah
 sub ah,dl
 and al,ah // get width bit offset
 mov dl,byte ptr [TConvertInfo.BitsCount + esi]
 mul dl
@@EvenWidth:
 mov dl,al
 test word ptr [TConvertInfo.Flags + esi],IMG_BITS_REVERSE
 jnz @@gr2
 mov dl,8
 sub dl,al
@@gr2:
 mov [TConvertInfo.InitialBitPos + esi],dl
@@grNext:
 mov esi,[TConvertInfo.Next + esi] //Check next
 test esi,esi
 jnz @LoopStart
 pop esi
end;

procedure InitProcs(SrcInfo, DstInfo: PConvertInfo);
asm
 push ebx
 push esi
 push edx // save Dst
 test byte ptr [TConvertInfo.UseProcs + eax],1
 jz @@NoProcs1
 mov esi,eax //ESI = Src
 call @GetR
@@NoProcs1:
 pop esi //ESI = Dst
 test byte ptr [TConvertInfo.UseProcs + esi],1
 jz @@NoProcs2
 call @GetW
@@NoProcs2: 
 pop esi
 pop ebx
 ret
//
@GetR:
 xor eax,eax
 mov cx,[TConvertInfo.Flags + esi]
 test cx,IMG_PLANAR
 jz @@grNoPlanar
 shr cx,PLANE_FORMAT_SHIFT
 mov al,1
 shl al,cl
 jmp @@grgr1
@@grNoPlanar:
 mov al,cl
 and al,31
 inc al
@@grgr1:
 cmp al,5
 jae @@grgrFullByter
 mov edx,offset @BitsReaders_TBL
 dec al
 cmp al,3
 jb @@grgr_finish
 dec al //if bits count >= 3
 jmp @@grgr_finish
@@grgrFullByter:
 mov edx,offset @BytesReaders_TBL
 add al,7
 shr al,3
 dec eax
@@grgr_finish:
 xor ecx,ecx
 test word ptr [TConvertInfo.Flags + esi],IMG_COMPRESSED
 jz @@NoESI
 inc ecx
 mov edx,[edx + 4]
 shl eax,3 // (0,1,2,3) * 8
 jmp @@IsESI
@@NoESI:
 mov edx,[edx]
 shl eax,4 // (0,1,2,3) * 16
@@IsESI:
 add eax,edx
 xor edx,edx 
 test cl,cl
 jnz @@NoBytesReverse
 test word ptr [TConvertInfo.Flags + esi],IMG_BYTES_REVERSE
 setnz dl
 lea eax,[eax + edx * 8]
@@NoBytesReverse:
 test word ptr [TConvertInfo.Flags + esi],IMG_BITS_REVERSE or IMG_PIXEL_BSWAP
 setnz dl
 mov eax,[eax + edx * 4] //get procedure address
 mov [TConvertInfo.ReadProc + esi],eax //set procedure address
@@gr_next:
 mov esi,[TConvertInfo.Next + esi] //Check next
 test esi,esi
 jnz @GetR
 ret
@GetW:
 xor eax,eax
 mov cx,[TConvertInfo.Flags + esi]
 test cx,IMG_PLANAR
 jz @@NoPlanar
 shr cx,PLANE_FORMAT_SHIFT
 mov al,1
 shl al,cl
 jmp @@gr1
@@NoPlanar:
 mov al,cl
 and al,31
 inc al
@@gr1:
 cmp al,5
 jae @@grFullByter
 mov ebx,offset @BitsW_TBL
 dec al
 cmp al,3
 jb @@gr_finish
 dec al //if bits count >= 3
 jmp @@gr_finish
@@grFullByter:
 mov ebx,offset @BytesW_TBL
 add al,7
 shr al,3
 dec eax
@@gr_finish:
 xor ecx,ecx
 test word ptr [TConvertInfo.Flags + esi],IMG_COMPRESSED
 jz @@NoEdi
 inc ecx
 mov edx,[ebx + 4] 
 shl eax,3 // (0,1,2,3) * 8
 jmp @@IsEDI
@@NoEDI:
 mov edx,[ebx]
 shl eax,4 // (0,1,2,3) * 16
@@IsEDI:
 push eax
 add eax,edx
 xor edx,edx
 test word ptr [TConvertInfo.Flags + esi],IMG_BYTES_REVERSE
 setnz dl
 push edx
 lea eax,[eax + edx * 8]
 test word ptr [TConvertInfo.Flags + esi],IMG_BITS_REVERSE or IMG_PIXEL_BSWAP
 setnz dl
 mov eax,[eax + edx * 4] //get procedure address
 mov [TConvertInfo.WriteProc + esi],eax //set procedure address
 test cl,cl
 jnz @@Compressed
 pop eax
 mov ecx,[ebx + 8] // Skippers table offset
 lea ecx,[ecx + eax * 8]
 pop eax
 add ecx,eax
 mov ecx,[ecx + edx * 4]
 mov [TConvertInfo.SkipProc + esi],ecx //set procedure address
 shr eax,1
 mov ecx,[ebx + 12] // Destination readers table offset
 add ecx,eax
 mov ecx,[ecx + edx * 4]
 mov [TConvertInfo.DstReadProc + esi],ecx //set procedure address
@@grw_next:
 mov esi,[TConvertInfo.Next + esi] //Check next
 test esi,esi
 jnz @GetW
 ret
@@Compressed:
 pop eax
 pop eax
 jmp @@grw_next
//
@BitsReaders_TBL:
 dd @BitsReaders
 dd @BitsReadersESI
@BytesReaders_TBL:
 dd @BytesReaders
 dd @BytesReadersESI
@BitsW_TBL:
 dd @BitsWriters
 dd @BitsWritersEDI
 dd @BitsSkippers
 dd @DstBitsReaders
@BytesW_TBL:
 dd @BytesWriters
 dd @BytesWritersEDI
 dd @BytesSkippers
 dd @DstBytesReaders
@BitsReaders:
 dd @Read1_LR_FW
 dd @Read1_RL_FW
 dd @Read1_LR_BW
 dd @Read1_RL_BW
 dd @Read2_LR_FW
 dd @Read2_RL_FW
 dd @Read2_LR_BW
 dd @Read2_RL_BW
 dd @Read4_LR_FW
 dd @Read4_RL_FW
 dd @Read4_LR_BW
 dd @Read4_RL_BW
@BitsReadersESI:
 dd @Read1_LR_ESI
 dd @Read1_RL_ESI
 dd @Read2_LR_ESI
 dd @Read2_RL_ESI
 dd @Read4_LR_ESI
 dd @Read4_RL_ESI
@DstBitsReaders:
 dd @wRead1_LR
 dd @wRead1_RL
 dd @wRead2_LR
 dd @wRead2_RL
 dd @wRead4_LR
 dd @wRead4_RL
@BitsWriters:
 dd @Write1_LR_FW
 dd @Write1_RL_FW
 dd @Write1_LR_BW
 dd @Write1_RL_BW
 dd @Write2_LR_FW
 dd @Write2_RL_FW
 dd @Write2_LR_BW
 dd @Write2_RL_BW
 dd @Write4_LR_FW
 dd @Write4_RL_FW
 dd @Write4_LR_BW
 dd @Write4_RL_BW
@BitsWritersEDI:
 dd @Write1_LR_EDI
 dd @Write1_RL_EDI
 dd @Write2_LR_EDI
 dd @Write2_RL_EDI
 dd @Write4_LR_EDI
 dd @Write4_RL_EDI
@BitsSkippers:
 dd @w1lrfw_skip
 dd @w1rlfw_skip
 dd @w1lrbw_skip
 dd @w1rlbw_skip
 dd @w2lrfw_skip
 dd @w2rlfw_skip
 dd @w2lrbw_skip
 dd @w2rlbw_skip
 dd @w4lrfw_skip
 dd @w4rlfw_skip
 dd @w4lrbw_skip
 dd @w4rlbw_skip
@BytesReaders:
 dd @Read8_FW
 dd @SwapRead8_FW
 dd @Read8_BW
 dd @SwapRead8_BW
 dd @Read16_FW
 dd @SwapRead16_FW
 dd @Read16_BW
 dd @SwapRead16_BW
 dd @Read24_FW
 dd @SwapRead24_FW
 dd @Read24_BW
 dd @SwapRead24_BW
 dd @Read32_FW
 dd @SwapRead32_FW
 dd @Read32_BW
 dd @SwapRead32_BW
@BytesReadersESI:
 dd @Read8_ESI
 dd @SwapRead8_ESI
 dd @Read16_ESI
 dd @SwapRead16_ESI
 dd @Read24_ESI
 dd @SwapRead24_ESI
 dd @Read32_ESI
 dd @SwapRead32_ESI
@DstBytesReaders:
 dd @wRead8
 dd @wSwapRead8
 dd @wRead16
 dd @wSwapRead16
 dd @wRead24
 dd @wSwapRead24
 dd @wRead32
 dd @wSwapRead32
@BytesWriters:
 dd @Write8_FW
 dd @SwapWrite8_FW
 dd @Write8_BW
 dd @SwapWrite8_BW
 dd @Write16_FW
 dd @SwapWrite16_FW
 dd @Write16_BW
 dd @SwapWrite16_BW
 dd @Write24_FW
 dd @SwapWrite24_FW
 dd @Write24_BW
 dd @SwapWrite24_BW
 dd @Write32_FW
 dd @SwapWrite32_FW
 dd @Write32_BW
 dd @SwapWrite32_BW
@BytesWritersEDI:
 dd @Write8_EDI
 dd @Write8_EDI
 dd @Write16_EDI
 dd @SwapWrite16_EDI
 dd @Write24_EDI
 dd @SwapWrite24_EDI
 dd @Write32_EDI
 dd @SwapWrite32_EDI
@BytesSkippers:
 dd @w8fw_skip
 dd @w8fw_skip
 dd @w8bw_skip
 dd @w8bw_skip
 dd @w16fw_skip
 dd @sw16fw_skip
 dd @w16bw_skip
 dd @sw16bw_skip
 dd @w24fw_skip
 dd @sw24fw_skip
 dd @w24bw_skip
 dd @sw24bw_skip
 dd @w32fw_skip
 dd @sw32fw_skip
 dd @w32bw_skip
 dd @sw32bw_skip
//*----- Read Left to Right -----*//
@Read1_LR_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 dec cl
 shr al,cl
 and al,1
 test cl,cl
 jnz @@lr1esi
  mov cl,8
  inc esi
 @@lr1esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read1_LR_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 dec cl
 shr al,cl
 and al,1
 test cl,cl
 jnz @@lr1fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@lr1fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read1_LR_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 dec cl
 shr al,cl
 and al,1
 test cl,cl
 jnz @@lr1bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@lr1bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read2_LR_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 sub cl,2
 shr al,cl
 and al,3
 test cl,cl
 jnz @@lr2esi
  mov cl,8
  inc esi
 @@lr2esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read2_LR_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 sub cl,2
 shr al,cl
 and al,3
 test cl,cl
 jnz @@lr2fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@lr2fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read2_LR_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 sub cl,2
 shr al,cl
 and al,3
 test cl,cl
 jnz @@lr2bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@lr2bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read4_LR_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 sub cl,4
 shr al,cl
 and al,15
 test cl,cl
 jnz @@lr4esi
  mov cl,8
  inc esi
 @@lr4esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read4_LR_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 sub cl,4
 shr al,cl
 and al,15
 test cl,cl
 jnz @@lr4fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@lr4fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read4_LR_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 sub cl,4
 shr al,cl
 and al,15
 test cl,cl
 jnz @@lr4bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@lr4bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
//*----- Read Right to Left -----*//
@Read1_RL_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 shr al,cl
 and al,1
 inc cl
 test cl,8
 jz @@rl1esi
  xor cl,cl
  inc esi
 @@rl1esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read1_RL_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,1
 inc cl
 test cl,8
 jz @@rl1fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@rl1fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read1_RL_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,1
 inc cl
 test cl,8
 jz @@rl1bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@rl1bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read2_RL_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 shr al,cl
 and al,3
 add cl,2
 test cl,8
 jz @@rl2esi
  xor cl,cl
  inc esi
 @@rl2esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read2_RL_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,3
 add cl,2
 test cl,8
 jz @@rl2fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@rl2fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read2_RL_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,3
 add cl,2
 test cl,8
 jz @@rl2bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@rl2bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read4_RL_ESI:
 xor eax,eax
 mov al,byte ptr [esi]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebx]
 shr al,cl
 and al,15
 add cl,4
 test cl,8
 jz @@rl4esi
  xor cl,cl
  inc esi
 @@rl4esi:
 mov byte ptr [offset TConvertInfo.BitPos + ebx],cl
 ret
@Read4_RL_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,15
 add cl,4
 test cl,8
 jz @@rl4fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebx]
 @@rl4fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
@Read4_RL_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //save counter
 shr al,cl
 and al,15
 add cl,4
 test cl,8
 jz @@rl4bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebx]
 @@rl4bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebx] //restore counter
 ret
//*----- Read Bytes -----*//
@Read8_FW:
@SwapRead8_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 inc dword ptr [TConvertInfo.Pntr + ebx]
 ret
@Read8_BW:
@SwapRead8_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 dec dword ptr [TConvertInfo.Pntr + ebx]
 ret
@Read16_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,word ptr [eax]
 add dword ptr [TConvertInfo.Pntr + ebx],2
 ret
@Read16_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,word ptr [eax]
 sub dword ptr [TConvertInfo.Pntr + ebx],2
 ret
@SwapRead16_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,word ptr [eax]
 xchg al,ah
 add dword ptr [TConvertInfo.Pntr + ebx],2
 ret
@SwapRead16_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,word ptr [eax]
 xchg al,ah
 sub dword ptr [TConvertInfo.Pntr + ebx],2
 ret
@Read24_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 and eax,$FFFFFF
 add dword ptr [TConvertInfo.Pntr + ebx],3
 ret
@Read24_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 and eax,$FFFFFF
 sub dword ptr [TConvertInfo.Pntr + ebx],3
 ret
@SwapRead24_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 bswap eax
 shr eax,8
 add dword ptr [TConvertInfo.Pntr + ebx],3
 ret
@SwapRead24_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 bswap eax
 shr eax,8
 sub dword ptr [TConvertInfo.Pntr + ebx],3
 ret
@Read32_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 add dword ptr [TConvertInfo.Pntr + ebx],4
 ret
@Read32_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 sub dword ptr [TConvertInfo.Pntr + ebx],4
 ret
@SwapRead32_FW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 bswap eax
 add dword ptr [TConvertInfo.Pntr + ebx],4
 ret
@SwapRead32_BW:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 mov eax,[eax]
 bswap eax
 sub dword ptr [TConvertInfo.Pntr + ebx],4
 ret
//*----- Read Bytes use ESI-----*//
@Read8_ESI:
@SwapRead8_ESI:
 xor eax,eax
 lodsb
 ret
@Read16_ESI:
 xor eax,eax
 lodsw
 ret
@SwapRead16_ESI:
 xor eax,eax
 lodsw
 xchg al,ah
 ret
@Read24_ESI:
 mov eax,[esi]
 and eax,$FFFFFF
 add esi,3
 ret
@SwapRead24_ESI:
 mov eax,[esi]
 bswap eax
 shr eax,8
 add esi,3
 ret
@Read32_ESI:
 lodsd
 ret
@SwapRead32_ESI:
 lodsd
 bswap eax
 ret
//*----- Destination Readers ----- *//
@wRead1_LR:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 dec cl
 shr al,cl
 and al,1
 inc cl
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@wRead2_LR:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,2
 shr al,cl
 and al,3
 add cl,2
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@wRead4_LR:
 mov eax,dword ptr [TConvertInfo.Pntr + ebx]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,4
 shr al,cl
 and al,15
 add cl,4
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
//*----- Read Right to Left -----*//
@wRead1_RL:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shr al,cl
 and al,1
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@wRead2_RL:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shr al,cl
 and al,3
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@wRead4_RL:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,byte ptr [eax]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shr al,cl
 and al,15
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
//*----- Read destination Bytes -----*//
@wRead8:
@wSwapRead8:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,byte ptr [eax]
 ret
@wRead16:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,word ptr [eax]
 ret
@wSwapRead16:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 movzx eax,word ptr [eax]
 xchg al,ah
 ret
@wRead24:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 mov eax,[eax]
 and eax,$FFFFFF
 ret
@wSwapRead24:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 mov eax,[eax]
 bswap eax
 shr eax,8
 ret
@wRead32:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 mov eax,[eax]
 ret
@wSwapRead32:
 mov eax,dword ptr [TConvertInfo.Pntr + ebp]
 mov eax,[eax]
 bswap eax
 ret
//*----- Write Bits Left to Right -----*//
@Write1_LR_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 dec cl
 shl al,cl
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr1edi
  mov cl,8
  inc edi
 @@wlr1edi:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write1_LR_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 dec cl
 shl al,cl
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr1fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr1fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w1lrfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 dec cl
 test cl,cl
 jnz @@wlr1fws
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr1fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write1_LR_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 dec cl
 shl al,cl
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr1bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr1bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w1lrbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 dec cl
 test cl,cl
 jnz @@wlr1bws
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr1bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write2_LR_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,2
 shl al,cl
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr2edi
  mov cl,8
  inc edi
 @@wlr2edi:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 ret
@Write2_LR_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,2
 shl al,cl
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr2fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr2fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w2lrfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,2
 test cl,cl
 jnz @@wlr2fws
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr2fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write2_LR_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,2
 shl al,cl
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr2bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr2bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w2lrbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,2
 test cl,cl
 jnz @@wlr2bws
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr2bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write4_LR_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,4
 shl al,cl
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr4edi
  mov cl,8
  inc edi
 @@wlr4edi:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write4_LR_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,4
 shl al,cl
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr4fw
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr4fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w4lrfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,4
 test cl,cl
 jnz @@wlr4fws
  mov cl,8
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr4fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write4_LR_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 sub cl,4
 shl al,cl
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 test cl,cl
 jnz @@wlr4bw
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr4bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w4lrbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 sub cl,4
 test cl,cl
 jnz @@wlr4bws
  mov cl,8
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wlr4bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
//*----- Write Bits Right to Left -----*//
@Write1_RL_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 shl al,cl //set pixel position
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 inc cl
 test cl,8
 jz @@wrl1edi
  xor cl,cl
  inc edi
 @@wrl1edi:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write1_RL_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 inc cl
 test cl,8
 jz @@wrl1fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl1fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w1rlfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 inc cl
 test cl,8
 jz @@wrl1fws
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl1fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write1_RL_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,1
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 inc cl
 test cl,8
 jz @@wrl1bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl1bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w1rlbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 inc cl
 test cl,8
 jz @@wrl1bws
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl1bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write2_RL_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 shl al,cl //set pixel position
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,2
 test cl,8
 jz @@wrl2edi
  xor cl,cl
  inc edi
 @@wrl2edi:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write2_RL_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,2
 test cl,8
 jz @@wrl2fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl2fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w2rlfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 add cl,2
 test cl,8
 jz @@wrl2fws
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl2fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write2_RL_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,3
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,2
 test cl,8
 jz @@wrl2bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl2bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w2rlbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 add cl,2
 test cl,8
 jz @@wrl2bws
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl2bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write4_RL_EDI:
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,4
 test cl,8
 jz @@wrl4edi
  xor cl,cl
  inc edi
 @@wrl4edi:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl //restore counter
 ret
@Write4_RL_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,4
 test cl,8
 jz @@wrl4fw
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl4fw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w4rlfw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 add cl,4
 test cl,8
 jz @@wrl4fws
  xor cl,cl
  inc dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl4fws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
@Write4_RL_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //save counter
 shl al,cl //set pixel position
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 add cl,4
 test cl,8
 jz @@wrl4bw
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl4bw:
 xchg cl,byte ptr [offset TConvertInfo.BitPos + ebp] //restore counter
 ret
@w4rlbw_skip:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov cl,byte ptr [offset TConvertInfo.BitPos + ebp]
 add cl,4
 test cl,8
 jz @@wrl4bws
  xor cl,cl
  dec dword ptr [TConvertInfo.Pntr + ebp]
 @@wrl4bws:
 mov byte ptr [offset TConvertInfo.BitPos + ebp],cl
 ret
//*----- Write Bytes -----*//
@Write8_EDI:
 stosb
 ret
@Write8_FW:
@SwapWrite8_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],al
@w8fw_skip:
 inc dword ptr [TConvertInfo.Pntr + ebp]
 ret
@Write8_BW:
@SwapWrite8_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],al
@w8bw_skip:
 dec dword ptr [TConvertInfo.Pntr + ebp]
 ret
@Write16_EDI:
 stosw
 ret
@Write16_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],ax
@w16fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],2
 ret
@Write16_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],ax
@w16bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],2
 ret
@SwapWrite16_EDI:
 xchg al,ah
 stosw
 ret
@SwapWrite16_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg al,ah
 mov [edi],ax
@sw16fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],2
 ret
@SwapWrite16_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 xchg al,ah
 mov [edi],ax
@sw16bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],2
 ret
@Write24_EDI:
 stosw
 shr eax,16
 stosb
 ret
@Write24_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],ax
 shr eax,16
 mov [edi+2],al
@w24fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],3
 ret
@Write24_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],ax
 shr eax,16
 mov [edi+2],al
@w24bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],3
 ret
@SwapWrite24_EDI:
 bswap eax
 shr eax,8
 stosw
 shr eax,16
 stosb
 ret
@SwapWrite24_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi+2],al
 shr eax,8
 xchg al,ah
 mov [edi],ax
@sw24fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],3
 ret
@SwapWrite24_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi+2],al
 shr eax,8
 xchg al,ah
 mov [edi],ax
@sw24bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],3
 ret
@Write32_EDI:
 stosd
 ret
@Write32_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],eax
@w32fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],4
 ret
@Write32_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 mov [edi],eax
@w32bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],4
 ret
@SwapWrite32_EDI:
 bswap eax
 stosd
 ret 
@SwapWrite32_FW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 bswap eax
 mov [edi],eax
@sw32fw_skip:
 add dword ptr [TConvertInfo.Pntr + ebp],4
 ret
@SwapWrite32_BW:
 mov edi,dword ptr [TConvertInfo.Pntr + ebp]
 bswap eax
 mov [edi],eax
@sw32bw_skip:
 sub dword ptr [TConvertInfo.Pntr + ebp],4
end;

var
 ConverterProtected: Boolean = True;
 OldProtect: Cardinal;

//procedure ImageConvertCode;
function BuildConverter(Info: PConvertInitInfo; Dest: Pointer): LongInt;
const
 INS_CLD = $FC;
 INS_STD = $FD;
 INC_ESI = $46;
 INC_EDI = $47;
 DEC_ESI = $4E;
 DEC_EDI = $4F;
 ADD_ESI = $C6; //offset + 1
 ADD_EDI = $C7; //offset + 1
 SUB_ESI = $EE; //offset + 1
 SUB_EDI = $EF; //offset + 1
 ADD_DL  = $C2; //offset + 1
 ADD_DH  = $C6; //offset + 1
 SUB_DL  = $EA; //offset + 1
 SUB_DH  = $EE; //offset + 1
 SHL_ECX = $E1C1;
 SHR_ECX = $E9C1;
 EDX2EAX = $D089;
 PUSH_ECX = $51;
 POP_ECX = $59;
 XOR_EAXEAX = $C031;
 INS_JAE = $73;
 INS_JBE = $76;
 C_GetPixelBits1 = 0;
 C_GetPixelBits2 = 4;
 C_CL_Increment1 = 8;
 C_CL_Decrement1 = 12;
 C_EsiInc1 = 16;
 C_EsiInc2 = 20;
 C_EsiInc3 = 24;
 C_EsiInc4 = 28;
asm
(*
 dd @Init
 dd @Convert
@BitsReaderTbl:
 dd @ReadBitsLR
 dd @ReadBitsRL
@BitsWriterTbl:
 dd @WriteBitsLR
 dd @WriteBitsRL
@ReaderTbl:
 dd @Read8
 dd @Read16
 dd @Read24
 dd @Read32
@SwapReaderTbl:
 dd @SwapRead8
 dd @SwapRead16
 dd @SwapRead24
 dd @SwapRead32
@WriterTbl:
 dd @Write8
 dd @Write16
 dd @Write24
 dd @Write32
@SwapWriterTbl:
 dd @SwapWrite8
 dd @SwapWrite16
 dd @SwapWrite24
 dd @SwapWrite32
@QuickCopyTbl:
 dd @QuickCopy8
 dd @QuickCopy16
 dd @QuickCopy24 
 dd @QuickCopy32
@ValueCvtCallPtr:
 dd @ValueConvert
@ReadProcCallPtr:
 dd @ReadProcCall
@WriteProcCallPtr:
 dd @WriteProcCall
//@ReadWriteCallPtr:
// dd @ReadWriteRecurse
@ReadPlanesPtr:
 dd @ReadPlanes
@WritePlanesPtr:
 dd @WritePlanes
@ConvertStartPtr:
 dd @ConvertStart
@ESILoadPtr:
 dd @ESIload
@EDILoadPtr:
 dd @EDIload
@MainLoopStartPtr:
 dd @MainLoopStart
@CheckTransparentPtr:
 dd @CheckTransparent
@CheckTransLRBitsPtr:
 dd @CheckTransparentBitsLR
@CheckTransRLBitsPtr:
 dd @CheckTransparentBitsRL
@CheckTransProcPtr:
 dd @CheckTransparentProc
@CheckTransRecursePtr:
 dd @CheckTransparentRecurse
@PixelModifierPtr:
 dd @PixelModifier
@ConvertLoopEndPtr:
 dd @ConvertLoopEnd
@SrcNextLinePtr:
 dd @SrcNextLine
@SrcNextLineBitsPtr:
 dd @SrcNextLineBits
@SrcNextLineListPtr:
 dd @SrcNextLineList
@DstNextLinePtr:
 dd @DstNextLine
@DstNextLineBitsPtr:
 dd @DstNextLineBits
@DstNextLineListPtr:
 dd @DstNextLineList
@NextLineListPtr:
 dd @NextLineList
@NextLineListBitsPtr:
 dd @NextLineListBits
@MainLoopEndPtr:
 dd @YConvertLoopEnd
@MainLoopEndPtr2:
 dd @YConvertLoopEnd2
@ConvertEndPtr:
 dd @ConvertEnd
 dd @Convert   // @TransferStart //for size
@TransCheckTbl:
 dd @CheckTransparentPtr
 dd @CTOffset1+1
 dd @CheckTransLRBitsPtr
 dd @CTOffset2+1
 dd @CheckTransRLBitsPtr
 dd @CTOffset3+1
 dd @CheckTransProcPtr
 dd @CTOffset4+1
 dd @CheckTransRecursePtr
 dd @CTOffset5+1
@Init:
 cmp byte ptr [Converting],0
 jnz @Init //loop while previous converting not finished
 mov byte ptr [Converting],1
 push esi
 push edi
 push ebx
 mov ebx,eax //EBX = Info
 mov eax,offset OldProtect
 push eax
 push PAGE_EXECUTE_READWRITE
 mov eax,offset @VariableCodeEnd + 1
 mov edx,offset @VariableCodeStart
 sub eax,edx
 push eax
 push edx
 call Windows.VirtualProtect *)

(** NEW **)
 push esi
 push edi
 push ebx
 push edx    // save Dest
 mov ebx,eax // EBX = Info
 mov edi,edx // EDI = Dest

 mov al,byte ptr [ConverterProtected]
 test al,al
 jz @@NoProtect
 mov byte ptr [ConverterProtected],0
 mov eax,offset OldProtect
 push eax
 push PAGE_EXECUTE_READWRITE
 mov eax,offset @VariableCodeEnd
 mov edx,offset @VariableCodeStart
 sub eax,edx
 push eax
 push edx
 call Windows.VirtualProtect
@@NoProtect:
(** NEW **)

//Code blocks write
 mov esi,offset @ConvertStartPtr
// mov edi,offset @Convert
 call @CodeBlockCopy

 test word ptr [TConvertInitInfo.DrawFlags + ebx],DRAW_SRC_COMPOSITE or DRAW_SRC_PROC
 jnz @@NoEsiLoad
 mov esi,offset @ESIloadPtr
 call @CodeBlockCopy
@@NoEsiLoad:
 test word ptr [TConvertInitInfo.DrawFlags + ebx],DRAW_DST_COMPOSITE or DRAW_DST_PROC
 jnz @@NoEdiLoad
 mov esi,offset @EDIloadPtr
 call @CodeBlockCopy
@@NoEdiLoad:

 push edi //save y loop start position

 mov esi,offset @MainLoopStartPtr
 call @CodeBlockCopy

 push edi //save x loop start position

//Write reader code
 mov cx,word ptr [TConvertInitInfo.DrawFlags + ebx]
 mov dh,byte ptr [TConvertInitInfo.SrcFlags + ebx]
 and dh,31
 mov dl,byte ptr [TConvertInitInfo.DstFlags + ebx]
 and dl,31
 xor eax,eax // clear AH
 test cx,DRAW_SRC_COMPOSITE or DRAW_SRC_PROC
 jnz @@NoXorEaxEax2
 cmp dh,23
 jae @@NoXorEaxEax2

 (*** NEW ***)
 mov eax,dword ptr [TConvertInitInfo.ConvertFrom + ebx]
 or eax,dword ptr [TConvertInitInfo.ConvertTo + ebx]
 setnz ah
 shl ah,DRAW_PIXEL_MODIFY_SHIFT
 push ecx
 push eax
 or cl,ah
 (*** NEW ***)

 test cx,DRAW_VALUE_CONVERT or DRAW_PIXEL_MODIFY or DRAW_TRANSP_MASK // DRAW_TRANSPARENT
 jnz @@XorEaxEax
 mov ah,dh
 add ah,8
 shr ah,3
 mov al,dl
 add al,8
 shr al,3
 cmp ah,al // if SrcBytes >= DstBytes then skip eax clear
 jae @@NoXorEaxEax
@@XorEaxEax:
 mov word ptr [edi],XOR_EAXEAX
 inc edi
 inc edi
@@NoXorEaxEax:

(*** NEW ***)
 pop eax
 pop ecx
 (*** NEW ***)

@@NoXorEaxEax2:

 cmp dh,3
 setbe al
 or cl,al
 cmp dl,3
 setbe al
 or cl,al
 mov word ptr [TConvertInitInfo.DrawFlags + ebx],cx

 (*** NEW ***)
 or cl,ah // DRAW_PIXEL_MODIFY
 (*** NEW ***)

 test cx,DRAW_BITS or DRAW_SRC_PROC or DRAW_DST_PROC or DRAW_SRC_COMPOSITE or DRAW_DST_COMPOSITE or DRAW_VALUE_CONVERT or DRAW_PIXEL_MODIFY or DRAW_TRANSP_MASK // DRAW_TRANSPARENT
 jnz @@NoQuickCopy
 add dh,8
 shr dh,3  // dh = source pixel bytes count
 add dl,8
 shr dl,3  // dl = target pixel bytes count
 cmp dh,dl
 jne @@NoQuickCopy
 test word ptr [TConvertInitInfo.SrcFlags + ebx],IMG_PIXEL_BSWAP
 setnz al
 test word ptr [TConvertInitInfo.DstFlags + ebx],IMG_PIXEL_BSWAP
 setnz ah
 cmp al,ah
 jne @@NoQuickCopy 
 test word ptr [TConvertInitInfo.SrcFlags + ebx],IMG_BYTES_REVERSE
 setnz al
 test word ptr [TConvertInitInfo.DstFlags + ebx],IMG_BYTES_REVERSE
 setnz ah
 cmp al,ah
 jne @@NoQuickCopy
 movzx edx,dl // edx = pixel bytes count
 lea esi,[@QuickCopyTbl + edx * 4 - 4]
 pop edx // clear saved x loop start address
 test al,al
 jz @NextLineProgCodeBlockCopy
 mov byte ptr [edi],INS_STD
 inc edi
 call @CodeBlockCopy
 mov byte ptr [edi],INS_CLD
 inc edi
 jmp @NextLineProg
@@NoQuickCopy:

 test cx,DRAW_VALUE_CONVERT or DRAW_BITS or DRAW_SRC_COMPOSITE or DRAW_DST_COMPOSITE or DRAW_SRC_PROC or DRAW_DST_PROC
 jz @@NoPushECX1
 mov byte ptr [edi],PUSH_ECX
 inc edi
@@NoPushECX1:
 test cx,DRAW_SRC_PROC
 jz @@NoProc1
 mov esi,offset @ReadProcCallPtr
 jmp @Next3
@@NoProc1:
 test cx,DRAW_SRC_COMPOSITE
 jz @LinearRead
@@src_composite:
 test cx,DRAW_DST_COMPOSITE
 jz @@rcp
@@dst_composite:
 test cx,DRAW_VALUE_CONVERT or DRAW_SRC_PROC or DRAW_DST_PROC or DRAW_PIXEL_MODIFY or DRAW_TRANSP_MASK //DRAW_TRANSPARENT
 jnz @@rcp
{ test cx,DRAW_BITS
 jnz @@nopushhh
 mov byte ptr [edi],PUSH_ECX
 inc edi
@@nopushhh:}
 mov esi,offset @ReadPlanesPtr
 call @CodeBlockCopy
 mov esi,offset @WritePlanesPtr2
 jmp @Next4
@@rcp:
 test cx,DRAW_VALUE_CONVERT or DRAW_BITS or DRAW_SRC_COMPOSITE or DRAW_DST_COMPOSITE or DRAW_SRC_PROC or DRAW_DST_PROC
 jnz @@NoPushECX2 // Already pushed?
 mov byte ptr [edi],PUSH_ECX
 inc edi
@@NoPushECX2:
 mov esi,offset @ReadPlanesPtr
 call @CodeBlockCopy
 mov word ptr [edi],EDX2EAX
 inc edi
 inc edi
 jmp @Next3b

@LinearRead:
 xor edx,edx
 call @SetReadWriteCode
@Next3:
 call @CodeBlockCopy
@Next3b:

//Write transparent check code
 mov cx,word ptr [offset TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_TRANSP_MASK
 jz @@NoTransCheck
 mov eax,3
 test cx,DRAW_DST_PROC
 jnz @@ntcfb2
 test cx,DRAW_DST_COMPOSITE
 jz @@ndstpr
@@ndrcmps:
 inc eax
 jmp @@ntcfb2
@@ndstpr:
 movzx eax,byte ptr [TConvertInitInfo.DstFlags + ebx]
 and al,31
 cmp al,3
 ja @@ntcfb //if Bits count >= 5 write full byte readers/writers
 inc ah
 test word ptr [TConvertInitInfo.DstFlags + ebx],IMG_BITS_REVERSE
 jz @@ntcfb
 inc ah
@@ntcfb:
 movzx eax,ah // eax = destination bits count
@@ntcfb2:
 lea esi,[@TransCheckTbl + eax * 8]
 push esi
 mov esi,[esi]
 call @CodeBlockCopy
 mov cx,word ptr [TConvertInitInfo.DrawFlags + ebx]
 and cx,DRAW_TRANSP_MASK
 shr ecx,DRAW_TRANSP_SHIFT
 dec ecx
 jz @@NoTransCheck
 dec ecx
 jz @@SetTranspJAE
 mov byte ptr [edi - 2],INS_JBE
 jmp @@NoTransCheck
@@SetTranspJAE:
 mov byte ptr [edi - 2],INS_JAE
@@NoTransCheck:

 mov esi,dword ptr [offset TConvertInitInfo.ConvertFrom + ebx]
 test esi,esi
 jz @@NoPixelConvertFrom
 call @CodeBlockCopy
 jmp @@NoPixelModify
@@NoPixelConvertFrom:

 test word ptr [offset TConvertInitInfo.DrawFlags + ebx],DRAW_PIXEL_MODIFY
 jz @@NoPixelModify
 mov esi,offset @PixelModifierPtr
 call @CodeBlockCopy
@@NoPixelModify:

//Write value convert code
 test word ptr [offset TConvertInitInfo.DrawFlags + ebx],DRAW_VALUE_CONVERT
 jz @@NoValueConvert
 mov esi,offset @ValueCvtCallPtr
 call @CodeBlockCopy
@@NoValueConvert:

 mov esi,dword ptr [offset TConvertInitInfo.ConvertTo + ebx]
 test esi,esi
 jz @@NoPixelConvertTo
 call @CodeBlockCopy
@@NoPixelConvertTo:

//Write writer code
 mov cx,word ptr [offset TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_DST_PROC
 jz @@NoProc2
 mov esi,offset @WriteProcCallPtr
 jmp @Next4
@@NoProc2:
 test cx,DRAW_DST_COMPOSITE
 jz @LinearWrite
@@dst_composite2:
 mov esi,offset @WritePlanesPtr
 jmp @Next4

@WriteBitsLRPtr:
 dd @WriteBitsLR+6
 dd @WriteBitsRL
@WriteBitsRLPtr:
 dd @WriteBitsRL+3
 dd @WriteBitsRLEnd

@LinearWrite:
 mov edx,1
 call @SetReadWriteCode
 mov cx,word ptr [offset TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_TRANSP_MASK
 jz @Next4
 test cx,DRAW_VALUE_CONVERT
 jnz @Next4
 cmp esi,offset @BitsWriterTbl // WriteBitsLR
 jne @@lwc2
 mov esi,[esi]
 mov ecx,[esi+4]
 push ecx
 mov cx,[esi+4+4]
 push ecx
 mov ecx,[esi]
 mov dword ptr [esi+6],ecx
 mov esi,offset @WriteBitsLRPtr
 call @WriteCodeCopy
 mov esi,offset @BitsWriterTbl
 mov esi,[esi]
 mov ecx,[esi+6]
 mov dword ptr [esi],ecx
 pop ecx
 mov word ptr [esi+4+4],cx
 pop ecx
 mov dword ptr [esi+4],ecx
 jmp @SkipNext4
@@lwc2:
 cmp esi,offset @BitsWriterTbl + 4 // WriteBitsRL
 jne @Next4
 mov esi,[esi]
 mov ecx,[esi+4]
 push ecx
 mov ecx,[esi]
 mov dword ptr [esi+3],ecx
 mov esi,offset @WriteBitsRLPtr
 call @WriteCodeCopy
 mov esi,offset @BitsWriterTbl + 4
 mov esi,[esi]
 mov ecx,[esi+3]
 mov dword ptr [esi],ecx
 pop ecx
 mov dword ptr [esi+4],ecx
 jmp @SkipNext4
@Next4:
 call @WriteCodeCopy //eax = block size; edx = skip offset
@SkipNext4:

 mov cx,word ptr [TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_TRANSP_MASK
 jz @notransp
 sub eax,edx
 test cx,DRAW_VALUE_CONVERT
 jz @@notrvc
 add edx,[offset @ValueCvtCallPtr + 4]
 sub edx,[offset @ValueCvtCallPtr]
@@notrvc:
 mov esi,dword ptr [TConvertInitInfo.ConvertFrom + ebx]
 test esi,esi
 jz @@nocvtfrom
 add edx,[esi + 4]
 sub edx,[esi]
 jmp @@nopixmod
@@nocvtfrom:
 test cx,DRAW_PIXEL_MODIFY
 jz @@nopixmod
 add edx,[offset @PixelModifierPtr + 4]
 sub edx,[offset @PixelModifierPtr]
@@nopixmod:
 mov esi,dword ptr [TConvertInitInfo.ConvertTo + ebx]
 test esi,esi
 jz @@nocvtto
 add edx,[esi + 4]
 sub edx,[esi]
@@nocvtto:
 pop esi
 mov ecx,[esi]
 add edx,[ecx+4]
 mov esi,[esi+4]
 sub edx,esi
 mov esi,edi
 sub esi,eax
 sub esi,edx
 dec edx
 mov byte ptr [esi],dl
@notransp:

 test word ptr [offset TConvertInitInfo.DrawFlags + ebx],DRAW_SRC_COMPOSITE or DRAW_DST_COMPOSITE or DRAW_SRC_PROC or DRAW_DST_PROC or DRAW_VALUE_CONVERT or DRAW_BITS
 jz @@NoPopECX
@@PopECX:
 mov byte ptr [edi],POP_ECX
 inc edi
@@NoPopECX:

//Write end of X loop
 mov eax,edi
 inc eax
 inc eax
 pop ecx //restore x loop start position
 sub eax,ecx
 neg al //al = -((edi + 2) - ecx)
 mov byte ptr [@ConvertLoopEnd + 1],al //set loop link
 mov esi,offset @ConvertLoopEndPtr
@NextLineProgCodeBlockCopy:
 call @CodeBlockCopy

@NextLineProg:
//Write src next line code
 mov cx,[TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_SRC_COMPOSITE or DRAW_SRC_PROC
 jnz @@NextLineProc1
 mov esi,offset @SrcNextLinePtr
 mov cl,byte ptr [TConvertInitInfo.SrcFlags + ebx]
 and cl,31
 cmp cl,7
 jae @@snlfb
 call @CodeBlockCopy
 mov esi,offset @SrcNextLineBitsPtr
@@snlfb:
 call @CodeBlockCopy
 jmp @@nlpcopy1
@@NextLineProc1:
 mov esi,offset @SrcNextLineListPtr
 call @CodeBlockCopy
 xor eax,eax
 mov cl,byte ptr [TConvertInitInfo.SrcFlags + ebx]
 and cl,31
 cmp cl,7
 setb al
 lea esi,[offset @NextLineListPtr + eax * 4]
 call @CodeBlockCopy
@@nlpcopy1:

//Write dst next line code
 mov cx,[TConvertInitInfo.DrawFlags + ebx]
 test cx,DRAW_DST_COMPOSITE or DRAW_DST_PROC
 jnz @@NextLineProc2
 mov esi,offset @DstNextLinePtr
 mov cl,byte ptr [TConvertInitInfo.DstFlags + ebx]
 and cl,31
 cmp cl,7
 jae @@dnlfb
 call @CodeBlockCopy
 mov esi,offset @DstNextLineBitsPtr
@@dnlfb:
 call @CodeBlockCopy
 jmp @@nlpcopy2
@@NextLineProc2:
 mov esi,offset @DstNextLineListPtr
 call @CodeBlockCopy
 xor eax,eax
 mov cl,byte ptr [TConvertInitInfo.DstFlags + ebx]
 and cl,31
 cmp cl,7
 setb al
 lea esi,[offset @NextLineListPtr + eax * 4]
 call @CodeBlockCopy
@@nlpcopy2:

//Write end of Y loop
 mov eax,edi
 inc eax
 inc eax
 inc eax
 pop ecx //restore loop start position
 sub eax,ecx
 neg eax //al = -((edi + 2) - ecx)
 cmp eax,-128
 jge @@ShortJump
 sub eax,5
 mov dword ptr [@ConvertEnd - 4],eax
 mov esi,offset @MainLoopEndPtr2
 jmp @@EndYLoop
@@ShortJump:
 mov byte ptr [@YConvertLoopEndEnd - 1],al //set loop link
 mov esi,offset @MainLoopEndPtr
@@EndYLoop:
 call @CodeBlockCopy

//write ending code
 mov esi,offset @ConvertEndPtr
 call @CodeBlockCopy
(*
//Protect start
 sub esp,4
 mov eax,esp
 push eax
 push OldProtect
 mov eax,offset @VariableCodeEnd + 1
 mov edx,offset @VariableCodeStart
 sub eax,edx
 push eax
 push edx
 call Windows.VirtualProtect
 add esp,4
//Protect end
*)
 (*** NEW ***)
 pop eax // restore Dest
 sub edi,eax
 (*** NEW ***)

// sub edi,offset @Convert //offset @TransferStart //get code size
 mov eax,edi //Result = code size
 pop ebx
 pop edi
 pop esi
 ret
///////////// End of converter building code //////////////////
@BitsReaderTbl:
 dd @ReadBitsLR
 dd @ReadBitsRL
@BitsWriterTbl:
 dd @WriteBitsLR
 dd @WriteBitsRL
@ReaderTbl:
 dd @Read8
 dd @Read16
 dd @Read24
 dd @Read32
@SwapReaderTbl:
 dd @SwapRead8
 dd @SwapRead16
 dd @SwapRead24
 dd @SwapRead32
@WriterTbl:
 dd @Write8
 dd @Write16
 dd @Write24
 dd @Write32
@SwapWriterTbl:
 dd @SwapWrite8
 dd @SwapWrite16
 dd @SwapWrite24
 dd @SwapWrite32
@QuickCopyTbl:
 dd @QuickCopy8
 dd @QuickCopy16
 dd @QuickCopy24 
 dd @QuickCopy32
@ValueCvtCallPtr:
 dd @ValueConvert
@ReadProcCallPtr:
 dd @ReadProcCall
@WriteProcCallPtr:
 dd @WriteProcCall
//@ReadWriteCallPtr:
// dd @ReadWriteRecurse
@ReadPlanesPtr:
 dd @ReadPlanes
@WritePlanesPtr:
 dd @WritePlanes
@WritePlanesPtr2:
 dd @WritePlanes2
@ConvertStartPtr:
 dd @ConvertStart
@ESILoadPtr:
 dd @ESIload
@EDILoadPtr:
 dd @EDIload
@MainLoopStartPtr:
 dd @MainLoopStart
@CheckTransparentPtr:
 dd @CheckTransparent
@CheckTransLRBitsPtr:
 dd @CheckTransparentBitsLR
@CheckTransRLBitsPtr:
 dd @CheckTransparentBitsRL
@CheckTransProcPtr:
 dd @CheckTransparentProc
@CheckTransRecursePtr:
 dd @CheckTransparentRecurse
@PixelModifierPtr:
 dd @PixelModifier
@ConvertLoopEndPtr:
 dd @ConvertLoopEnd
@SrcNextLinePtr:
 dd @SrcNextLine
@SrcNextLineBitsPtr:
 dd @SrcNextLineBits
@SrcNextLineListPtr:
 dd @SrcNextLineList
@DstNextLinePtr:
 dd @DstNextLine
@DstNextLineBitsPtr:
 dd @DstNextLineBits
@DstNextLineListPtr:
 dd @DstNextLineList
@NextLineListPtr:
 dd @NextLineList
@NextLineListBitsPtr:
 dd @NextLineListBits
@MainLoopEndPtr:
 dd @YConvertLoopEnd
@MainLoopEndPtr2:
 dd @YConvertLoopEnd2
@ConvertEndPtr:
 dd @ConvertEnd
 dd @VariableCodeEnd //for size
@TransCheckTbl:
 dd @CheckTransparentPtr
 dd @CTOffset1+1
 dd @CheckTransLRBitsPtr
 dd @CTOffset2+1
 dd @CheckTransRLBitsPtr
 dd @CTOffset3+1
 dd @CheckTransProcPtr
 dd @CTOffset4+1
 dd @CheckTransRecursePtr
 dd @CTOffset5+1
@CodeBlockCopy: //esi - ptr pos; edi - dest
 mov ecx,[esi+4] // get end ptr
 mov esi,[esi]   // get source ptr
 sub ecx,esi     // count source size
 mov eax,ecx     // return block size
 jz @@sbc
 rep movsb
@@sbc:
 ret
@WriteCodeCopy: //esi - ptr pos; edi - dest
 mov ecx,[esi+4] // get end ptr
 mov esi,[esi]   // get source ptr
 mov edx,[esi]   // get skip ptr
 add esi,4
 sub edx,esi // return skip position
 sub ecx,esi // count source size
 mov eax,ecx // return block size
 jz @@wcc
 rep movsb
@@wcc:
 ret
@ReaderIncrementTbl:
 dd @EsiAdd1
 dd @EsiAdd2
 dd @EsiAdd3
@SwapReaderIncTbl:
 dd @EsiAdd4
 dd @EsiAdd5
 dd @EsiAdd6
@WriterIncrementTbl:
 dd @EdiAdd1
 dd @EdiAdd2
 dd @EdiAdd3
@SwapWriterIncTbl:
 dd @EdiAdd4
 dd @EdiAdd5
 dd @EdiAdd6
@RWTbl:
 dd @ReaderTbl
 dd @SwapReaderTbl
 dd @WriterTbl
 dd @SwapWriterTbl
 dd @ReaderIncrementTbl
 dd @SwapReaderIncTbl
 dd @WriterIncrementTbl
 dd @SwapWriterIncTbl
@RWModifiers:
 dd @GetPixelBits1 + 1 //0
 dd @GetPixelBits2 + 1 //4
 dd @CL_Increment1 + 2 //8
 dd @CL_Decrement1 + 2 //12
 dd @EsiInc1           //16
 dd @EsiInc2           //20
 dd @EsiInc3           //24
 dd @EsiInc4           //28
 dd @GetPixelBits3 + 1 //0
 dd @GetPixelBits4 + 1 //4
 dd @CL_Increment2 + 2 //8
 dd @CL_Decrement2 + 2 //12
 dd @EdiInc1           //16
 dd @EdiInc2           //20
 dd @EdiInc3           //24
 dd @EdiInc4           //28
@SetReadWriteCode:
 movzx eax,byte ptr [TConvertInitInfo.SrcFlags + ebx + edx * 2]
 and al,31
 inc al //al = Source bits count

 push ebx
 mov cl,INC_ESI
 add cl,dl
 mov ch,ADD_ESI
 add ch,dl
 test word ptr [TConvertInitInfo.SrcFlags + ebx + edx * 2],IMG_BYTES_REVERSE
 jz @lr1
 add cl,DEC_ESI - INC_ESI //if reversed bytes then decrement
 add ch,SUB_ESI - ADD_ESI //if reversed bytes then subtract
@lr1:
 shl ecx,8
 cmp al,5
 jae @FullByter //if Bits count >= 5 write full byte readers/writers
//1-4 bit pixel readers/writers
 mov cl,al
 mov ah,1
 shl ah,cl
 dec ah //get pixel mask
 cmp al,3
 jb @ilbcwret
 mov al,4 //if bits count >= 3
@ilbcwret:
 lea esi,[@BitsReaderTbl + edx * 8]
 test word ptr [TConvertInitInfo.SrcFlags + ebx + edx * 2],IMG_BITS_REVERSE
 jz @Next2
 mov ebx,edx
 shl ebx,5
 lea ebx,[@RWModifiers + ebx]
 mov edx,[ebx + C_GetPixelBits2]
 mov byte ptr [edx],ah //set pixel mask
 mov edx,[ebx + C_CL_Increment1]
 mov byte ptr [edx],al //set bits increment
 mov edx,[ebx + C_EsiInc2]
 mov byte ptr [edx],ch
 add esi,4 //Set pixels in byte reverse reader
 jmp @srwc
@Next2:
 mov ebx,edx
 shl ebx,5
 lea ebx,[@RWModifiers + ebx]
 mov edx,[ebx + C_GetPixelBits1]
 mov byte ptr [edx],ah //set pixel mask
 mov edx,[ebx + C_CL_Decrement1]
 cmp edx,offset @CL_Decrement2 + 2
 jne @nocldec3
 mov byte ptr [@CL_Decrement3 + 2],al
@nocldec3:
 mov byte ptr [edx],al //set bits decrement
 mov edx,[ebx + C_EsiInc1]
 mov byte ptr [edx],ch
 jmp @srwc
@FullByter:
 add al,7
 shr al,3 //get pixel size
 dec eax
 lea esi,[@RWTbl + edx * 8]
 test word ptr [TConvertInitInfo.SrcFlags + ebx + edx * 2],IMG_PIXEL_BSWAP
 mov ebx,edx
 setnz dl
 and edx,1
 test al,al
 jnz @MoreThan8
 mov esi,dword ptr [esi + edx * 4] //get reader/writer proc
 shl ebx,5
 lea ebx,[@RWModifiers + ebx]
 mov edx,[ebx + C_EsiInc3]
 mov byte ptr [edx],ch
 mov edx,[ebx + C_EsiInc4]
 mov byte ptr [edx],ch
 jmp @srwc
@MoreThan8:
 mov ebx,dword ptr [esi + 16 + edx * 4] //RWIncTbl
 mov esi,dword ptr [esi + edx * 4]
 lea esi,[esi + eax * 4] //get reader/writer proc
 dec eax
 mov eax,[eax * 4 + ebx]
 bswap ecx
 mov [eax + 1],ch
@srwc:
 pop ebx
 ret
/////////
@VariableCodeStart:
@ReadBitsLR:
 mov cl,[offset TConvertInfo.BitPos + ebx]
 @CL_Decrement1:
 sub cl,4 //variable
 mov al,byte ptr [esi]
 shr al,cl
 @GetPixelBits1:
 and al,15 //variable get X bit pixel
 test cl,cl
 jnz @@rbe
  mov cl,8
  @EsiInc1:
  inc esi //variable instruction
 @@rbe:
 mov [offset TConvertInfo.BitPos + ebx],cl
@ReadBitsRL:
 mov cl,[offset TConvertInfo.BitPos + ebx]
 mov al,byte ptr [esi]
 shr al,cl
 @GetPixelBits2:
 and al,15 //variable get X bit pixel
 @CL_Increment1:
 add cl,4 // variable
 test cl,8
 jz @@rre
  xor cl,cl
  @EsiInc2:
  inc esi //variable instruction
 @@rre:
 mov [offset TConvertInfo.BitPos + ebx],cl
@WriteBitsLR:
// dd -1
 dd @wblr_skip
 mov cl,[offset TConvertInfo.BitPos + ebp]
 @CL_Decrement2:
 sub cl,4 //variable
 shl al,cl //set pixel position
 @GetPixelBits3:
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
@wblr_skip:
 test cl,cl
 jnz @@wbe
  mov cl,8
  @EdiInc1:
  inc edi
 @@wbe:
 mov [offset TConvertInfo.BitPos + ebp],cl
@WriteBitsRL:
// dd -1
 dd @CL_Increment2
 mov cl,[offset TConvertInfo.BitPos + ebp]
 shl al,cl //set pixel position
 @GetPixelBits4:
 mov ah,15
 shl ah,cl
 and al,ah
 not ah //get pixel mask
 and [edi],ah //clear pixel bits
 or [edi],al //write pixel
 @CL_Increment2:
 add cl,4 //variable
 test cl,8
 jz @@wre
  xor cl,cl
  @EdiInc2:
  inc edi
 @@wre:
 mov [offset TConvertInfo.BitPos + ebp],cl
@WriteBitsRLEnd:
@Read8:
 mov al,byte ptr [esi]
 @EsiInc3:
 inc esi //variable instruction
@Read16:
 mov ax,word ptr [esi]
 @EsiAdd1:
 add esi,2 //variable instruction
@Read24:
 mov eax,[esi]
 and eax,$FFFFFF
 @EsiAdd2:
 add esi,3 //variable instruction
@Read32:
 mov eax,[esi]
 @EsiAdd3:
 add esi,4 //variable instruction
@SwapRead8:
 mov al,byte ptr [esi]
 @EsiInc4:
 inc esi //variable instruction
@SwapRead16:
 mov ax,word ptr [esi]
 xchg al,ah
 @EsiAdd4:
 add esi,2 //variable instruction
@SwapRead24:
 mov eax,[esi]
 bswap eax
 shr eax,8
 @EsiAdd5:
 add esi,3 //variable instruction
@SwapRead32:
 mov eax,[esi]
 bswap eax
 @EsiAdd6:
 add esi,4 //variable instruction
@Write8:
// dd -1
 dd @EdiInc3
 mov [edi],al
 @EdiInc3:
 inc edi
@Write16:
// dd -1
 dd @EdiAdd1
 mov [edi],ax
 @EdiAdd1:
 add edi,2
@Write24:
// dd -1
 dd @EdiAdd2
 mov [edi],ax
 shr eax,8
 mov [edi+2],ah
 @EdiAdd2:
 add edi,3
@Write32:
// dd -1
 dd @EdiAdd3
 mov [edi],eax
 @EdiAdd3:
 add edi,4
@SwapWrite8:
// dd -1
 dd @EdiInc4
 mov [edi],al
 @EdiInc4:
 inc edi
@SwapWrite16:
// dd -1
 dd @EdiAdd4
 xchg al,ah
 mov [edi],ax
 @EdiAdd4:
 add edi,2
@SwapWrite24:
// dd -1
 dd @EdiAdd5
 mov [edi+2],al
 shr eax,8
 xchg al,ah
 mov [edi],ax
 @EdiAdd5:
 add edi,3
@SwapWrite32:
// dd -1
 dd @EdiAdd6
 bswap eax
 mov [edi],eax
 @EdiAdd6:
 add edi,4
@QuickCopy8:
 rep movsb
@QuickCopy16:
 rep movsw
@QuickCopy24:
 rep movsw
 movzx ecx,word ptr [offset TConvertInfo.W + ebx]
 rep movsb
@QuickCopy32:
 rep movsd
@ValueConvert:
// mov edx,ebx
// mov ecx,ebp
 call dword ptr [TConvertInfo.ValueConvert + ebp]
@ReadProcCall:
 call dword ptr [TConvertInfo.ReadProc + ebx]
@WriteProcCall:
// dd -1
 dd @WriteProcCallEnd
 call dword ptr [TConvertInfo.WriteProc + ebp]
 @WriteProcCallEnd:
@ReadPlanes:
 xor ecx,ecx
 xor edx,edx
 push ebx
@@rc_loop:
 call dword ptr [TConvertInfo.ReadProc + ebx]
 shl eax,cl
 or edx,eax
 add cl,byte ptr [TConvertInfo.BitsCount + ebx]
 mov ebx,dword ptr [TConvertInfo.Next + ebx]
 test ebx,ebx
 jnz @@rc_loop
 pop ebx
// mov eax,edx  //eax - pixel value
//////////////////
@WritePlanes:
 dd @WriteCallOffset
@WritePlanesStart:
 xor ecx,ecx
 mov edx,eax  //edx - pixel value
 push ebp
@@wc_loop:
 mov eax,edx
 shr eax,cl
 call dword ptr [TConvertInfo.WriteProc + ebp]
 add cl,byte ptr [TConvertInfo.BitsCount + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wc_loop
 pop ebp
 @WriteCallOffset:
//////////////
@WritePlanes2:
 dd @WriteCallOffset2
@WritePlanesStart2:
 xor ecx,ecx
 push ebp
@@wc_loop2:
 mov eax,edx
 shr eax,cl
 call dword ptr [TConvertInfo.WriteProc + ebp]
 add cl,byte ptr [TConvertInfo.BitsCount + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wc_loop2
 pop ebp
 @WriteCallOffset2:
@ConvertStart:
 push esi
 push edi
 push ebx
 push ebp
 mov ebx,eax // SrcInfo
 mov ebp,edx // DstInfo
 xor ecx,ecx
 mov cx,word ptr [offset TConvertInfo.H + ebx]
@ESIload:
 mov esi,[offset TConvertInfo.Pntr + ebx]
@EDIload:
 mov edi,[offset TConvertInfo.Pntr + ebp]
@MainLoopStart:
 push ecx // save height
 mov cx,word ptr [offset TConvertInfo.W + ebx]
@CheckTransparent:
 cmp eax,dword ptr [TConvertInfo.TransparentColor + ebx]
@CToffset1:
 je @CheckTransEnd //variable
@CheckTransparentBitsLR:
 mov cl,[offset TConvertInfo.BitPos + ebp]
 @CL_Decrement3:
 sub cl,4 //variable
 cmp eax,dword ptr [TConvertInfo.TransparentColor + ebx]
@CToffset2:
 je @CheckTransEnd //variable
@CheckTransparentBitsRL:
 mov cl,[offset TConvertInfo.BitPos + ebp]
 cmp eax,dword ptr[TConvertInfo.TransparentColor + ebx]
@CToffset3:
 je @CheckTransEnd //variable
@CheckTransparentProc:
 cmp eax,dword ptr[TConvertInfo.TransparentColor + ebx]
 jne @CheckTransProcEnd
 call dword ptr [TConvertInfo.SkipProc + ebp]
@CTOffset4:
 jmp @CTOffset4
@CheckTransProcEnd:
@CheckTransparentRecurse:
 cmp eax,dword ptr[TConvertInfo.TransparentColor + ebx]
 jne @CheckTransEnd
// call @WriteSkip
 push ebp
@@wcs_loop:
 call dword ptr [TConvertInfo.SkipProc + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wcs_loop
 pop ebp
@CTOffset5:
 jmp @CTOffset5
@CheckTransEnd:
@PixelModifier:
 add eax,[offset TConvertInfo.PixelModifier + ebx]
@ConvertLoopEnd:
 loop @ConvertLoopEnd
@SrcNextLine:
 add esi,[offset TConvertInfo.RowStride + ebx]
@SrcNextLineBits:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + ebx]
 mov byte ptr [TConvertInfo.BitPos + ebx],cl
@SrcNextLineList:
 mov eax,ebx // DstInfo
@DstNextLine:
 add edi,[offset TConvertInfo.RowStride + ebp]
@DstNextLineBits:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + ebp]
 mov byte ptr [TConvertInfo.BitPos + ebp],cl
@DstNextLineList:
 mov eax,ebp // DstInfo
@NextLineList:
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @NextLineList
@NextLineListBits:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + eax]
 mov byte ptr [TConvertInfo.BitPos + eax],cl
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @NextLineListBits
@YConvertLoopEnd:
 pop ecx
 loop @YConvertLoopEnd
@YConvertLoopEndEnd:
@YConvertLoopEnd2:
 pop ecx
 dec ecx
 jnz @VariableCodeStart
@ConvertEnd:
 pop ebp
 pop ebx
 pop edi
 pop esi
 ret
(* --------- *)
(*
@Convert: //Place holder code start
 push esi
 push edi
 push ebx
 push ebp
 mov ebx,eax // SrcInfo
 mov ebp,edx // DstInfo
 xor ecx,ecx
 mov cx,word ptr [offset TConvertInfo.H + ebx]
 mov esi,[offset TConvertInfo.Pntr + ebx]
 mov edi,[offset TConvertInfo.Pntr + ebp]
@@YLoopStart:
 push ecx // save height
 mov cx,word ptr [offset TConvertInfo.W + ebx]
@@LoopStart:
 push ecx // save width
 xor eax,eax
     //Reader block start
 xor ecx,ecx
 xor edx,edx
 push ebx
@@XX:
 call dword ptr [TConvertInfo.ReadProc + ebx]
 shl eax,cl
 or edx,eax
 add cl,byte ptr [TConvertInfo.BitsCount + ebx]
 mov ebx,dword ptr [TConvertInfo.Next + ebx]
 test ebx,ebx
 jnz @@XX
 pop ebx
 mov eax,edx  //eax - pixel value
     //Reader block end

     // check transparent color start
 cmp eax,dword ptr[TConvertInfo.TransparentColor + ebx]
 jne @@Continue
 push ebp
@@wcs_loopxx:
 call dword ptr [TConvertInfo.SkipProc + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wcs_loopxx
 pop ebp
 jmp @@LoopEnd
@@Continue:
     // check transparent color end

     // pixel modifier block
 add eax,[offset TConvertInfo.PixelModifier + ebx]

     //convert value block start
// mov edx,ebx
// mov ecx,ebp
 call dword ptr [TConvertInfo.ValueConvert + ebp]
     //convert value block end

     //Writer block start
 xor ecx,ecx
 mov edx,eax  //edx - pixel value
 push ebp
@@wc_loopxx:
 mov eax,edx
 shr eax,cl
 call dword ptr [TConvertInfo.WriteProc + ebp]
 add cl,byte ptr [TConvertInfo.BitsCount + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wc_loopxx
 pop ebp
      //Writer block end
@@LoopEnd:
 pop ecx
      //Final block start
 loop @@LoopStart

      //Next line src block
 mov eax,ebx
@@nll1:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + eax]
 mov byte ptr [TConvertInfo.BitPos + eax],cl
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @@nll1
      //Next line dst block
 mov eax,ebp
@@nll2:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + eax]
 mov byte ptr [TConvertInfo.BitPos + eax],cl
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @@nll2
      //Block End
 pop ecx
 loop @@YLoopStart

 pop ebp
 pop ebx
 pop edi
 pop esi
     //Final block end  *)
@VariableCodeEnd:
end;

procedure Decompress(SrcInfo, DstInfo: PConvertInfo);
asm
 push esi
 push edi
 push ebx
 push ebp
 mov ebx,eax // SrcInfo
 mov ebp,edx // DstInfo
 xor ecx,ecx
 mov cx,word ptr [offset TConvertInfo.H + ebx]
@@YLoopStart:
 push ecx // save height
@DecompressInit:
 mov esi,dword ptr [TConvertInfo.Pntr + ebx]
 movsx eax,word ptr [TConvertInfo.WordRowStride + ebx]
 add dword ptr [TConvertInfo.Pntr + ebx],eax
 add esi,[esi] //get scanline pointer
 and byte ptr [TConvertInfo.UseProcs + ebx],15 //clear flags
 mov cx,word ptr [TConvertInfo.InitCounter + ebx]
 xor eax,eax
 test ecx,ecx
 jz @@StopSkip
@@InitialSkip:
 lodsw
@@CheckStop:
 test ax,ax // if both are zero then stop
 jnz @@PreInitialReadMode
 or byte ptr [TConvertInfo.UseProcs + ebx],$40 //set stop mode
 jmp @@StopSkip
@@PreInitialReadMode: 
 xor edx,edx
 mov dl,al
 sub ecx,edx
 jg @@InitialReadMode
 neg ecx
 mov al,cl
 or byte ptr [TConvertInfo.UseProcs + ebx],$80 //set skip mode
 jmp @@StopSkip
@@InitialReadMode:
 test ah,ah // if copy count = 0
 jz @@InitialSkip
 mov dl,ah //high bytes of edx are empty, no need to clear
@@InitialRead:
 call dword ptr [TConvertInfo.ReadProc + ebx]
 dec edx
 dec ecx
 jz @@StopSkip2
 test edx,edx
 jnz @@InitialRead
 jmp @@InitialSkip
@@ddSetSkipCount:
 mov word ptr [TConvertInfo.Counter1 + ebx],ax
 test al,al
 jz @@ddReadCount
 or byte ptr [TConvertInfo.UseProcs + ebx],$80 // skip mode
 jmp @@ddReadValue  
@@StopSkip2:
 mov al,dl
@@StopSkip:
 mov word ptr [TConvertInfo.Counter1 + ebx],ax
// xor ecx,ecx
 mov cx,word ptr [offset TConvertInfo.W + ebx]
@@LoopStart:
 push ecx   // save width
@DecompressValue:
 mov cl,byte ptr [TConvertInfo.UseProcs + ebx]
 test cl,$40 //stop mode
 jnz @Skip
 mov al,byte ptr [TConvertInfo.Counter1 + ebx]
 test al,al
 jnz @@ddReadValue
 // if not skip mode
 test cl,$80
 jz @@ddReadSkipCount
@@ddReadCount:
 mov al,byte ptr [TConvertInfo.Counter2 + ebx] // copy count
 test al,al
 jz @@ddReadSkipCount
 mov byte ptr [TConvertInfo.Counter1 + ebx],al
 and byte ptr [TConvertInfo.UseProcs + ebx],15 // read mode
 mov cl,byte ptr [TConvertInfo.InitialBitPos + ebx]
 mov byte ptr [TConvertInfo.BitPos + ebx],cl 
 jmp @@ddReadValue
@@ddReadValue:
 dec byte ptr [TConvertInfo.Counter1 + ebx]
 test byte ptr [TConvertInfo.UseProcs + ebx],$80
 jnz @Skip // change for skip label
 call dword ptr [TConvertInfo.ReadProc + ebx]
@@ColorWrite:
 add eax,dword ptr [TConvertInfo.PixelModifier + ebx]
 test byte ptr [TConvertInfo.UseProcs + ebp],2
 jz @@NoConvert
 call dword ptr [TConvertInfo.ValueConvert + ebp]
@@NoConvert:
 xor ecx,ecx
 mov edx,eax  //edx - pixel value
 push ebp
@@wc_loop:
 mov eax,edx
 shr eax,cl
 call dword ptr [TConvertInfo.WriteProc + ebp]
 add cl,byte ptr [TConvertInfo.BitsCount + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wc_loop
 pop ebp
 pop ecx
 loop @@LoopStart
 jmp @@LoopEnd
@Skip:
 test byte ptr [TConvertInfo.UseProcs + ebp],4
 jnz @@TransparentColor
 push ebp
@@wcs_loop:
 call dword ptr [TConvertInfo.SkipProc + ebp]
 mov ebp,dword ptr [TConvertInfo.Next + ebp]
 test ebp,ebp
 jnz @@wcs_loop
 pop ebp
 pop ecx
 loop @@LoopStart
 jmp @@LoopEnd
@@TransparentColor:
 mov eax,dword ptr [TConvertInfo.TransparentColor + ebx]
 jmp @@ColorWrite
@@ddReadSkipCount:
 lodsw // read counts
 test ax,ax // if both are zero then stop
 jnz @@ddSetSkipCount
@@ddStopMode:
 or byte ptr [TConvertInfo.UseProcs + ebx],$40 // stop mode
 jmp @Skip 
@@LoopEnd:
 mov eax,ebp
@@nll:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + eax]
 mov byte ptr [TConvertInfo.BitPos + eax],cl
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @@nll
 pop ecx
 dec ecx
 jnz @@YLoopStart
 pop ebp
 pop ebx
 pop edi
 pop esi
end;

procedure Compress(SrcInfo, DstInfo: PConvertInfo);
asm
 push esi
 push edi
 push ebx
 push ebp
 mov ebx,eax // SrcInfo
 mov ebp,edx // DstInfo
 xor ecx,ecx
 mov cx,word ptr [offset TConvertInfo.H + ebx]
 mov edi,dword ptr [offset TConvertInfo.CmprDataPtr + ebp]
 jmp @@YLoopStart
@@CountSkips:
 inc word ptr [TConvertInfo.Counter1 + ebp]
 xor cl,cl
 mov byte ptr [TConvertInfo.InitCounter + ebp],cl
 pop ecx
 loop @@LoopStart
 jmp @@LoopEnd
@@YLoopStart:
 push ecx // save height
 mov edx,dword ptr [TConvertInfo.Pntr + ebp]
 movsx eax,word ptr [TConvertInfo.WordRowStride + ebp]
 add dword ptr [TConvertInfo.Pntr + ebp],eax
 mov eax,edi
 sub eax,edx
 mov [edx],eax //set scanline pointer
 xor ecx,ecx
 mov byte ptr [TConvertInfo.InitCounter + ebp],cl //clear flags
 mov word ptr [TConvertInfo.Counter1 + ebp],cx
 mov cx,word ptr [offset TConvertInfo.W + ebx]
@@LoopStart:
 push ecx  // save width
 xor ecx,ecx
 xor edx,edx
 push ebx
@@XX:
 call dword ptr [TConvertInfo.ReadProc + ebx]
 shl eax,cl
 or edx,eax
 add cl,byte ptr [TConvertInfo.BitsCount + ebx]
 mov ebx,dword ptr [TConvertInfo.Next + ebx]
 test ebx,ebx
 jnz @@XX
 pop ebx
 cmp edx,dword ptr [TConvertInfo.TransparentColor + ebx]
 je @@CountSkips
 test byte ptr [TConvertInfo.InitCounter + ebp],1
 jnz @@Continue
 mov eax,255
// xor ecx,ecx // unneccesary 
 mov cx,word ptr [TConvertInfo.Counter1 + ebp]
@@SkipFill:
 cmp ecx,eax
 jle @@FillEnd
 sub ecx,eax
 stosw // count + 0 bytes
 jmp @@SkipFill
@@FillEnd:
 mov [edi],cx
 inc edi
 mov esi,edi // esi = counter ptr
 inc edi
 inc byte ptr [TConvertInfo.InitCounter + ebp] //clear flags
 xor ecx,ecx
 mov word ptr [TConvertInfo.Counter1 + ebp],cx // clear skip counter
 mov cl,byte ptr [TConvertInfo.InitialBitPos + ebp]
 mov byte ptr [TConvertInfo.BitPos + ebp],cl
@@Continue:
 inc byte ptr [esi] // increase byte counter
 jnz @@NoOverflow
 mov byte ptr [esi],255
 mov word ptr [edi],$0100 // zero skip, one byte out
 inc edi
 mov esi,edi  // esi = counter ptr
 inc edi
 mov cl,byte ptr [TConvertInfo.InitialBitPos + ebp]
 mov byte ptr [TConvertInfo.BitPos + ebp],cl 
@@NoOverflow:
 mov eax,edx
 add eax,dword ptr [TConvertInfo.PixelModifier + ebx]
 test byte ptr [TConvertInfo.UseProcs + ebp],2
 jz @@NoConvert
 call dword ptr [TConvertInfo.ValueConvert + ebp]
@@NoConvert:
 call dword ptr [TConvertInfo.WriteProc + ebp]
 pop ecx
 loop @@LoopStart
//

@@LoopEnd: // cx is zero
 mov [edi],cx // write line terminator
 inc edi
 inc edi
 mov eax,ebx
@@nll:
 mov cl,byte ptr [TConvertInfo.InitialBitPos + eax]
 mov byte ptr [TConvertInfo.BitPos + eax],cl
 mov ecx,dword ptr [TConvertInfo.RowStride + eax]
 add dword ptr [TConvertInfo.Pntr + eax],ecx
 mov eax,dword ptr [TConvertInfo.Next + eax]
 test eax,eax
 jnz @@nll
 pop ecx
 dec ecx
 jnz @@YLoopStart
 mov dword ptr [offset TConvertInfo.CmprDataPtr + ebp],edi 
 pop ebp
 pop ebx
 pop edi
 pop esi
end;

function GetFromRgbQuadConverter(Fmt: TImageFormat): Pointer;
begin
 if Fmt = ifIndexed then
  Result := @RGBQuadToIndexed else
  Result := @RGBQuadToRGB;
end;

function GetToRgbQuadConverter(Fmt: TImageFormat): Pointer;
begin
 if Fmt = ifIndexed then
  Result := @IndexedToRGBQuad else
  Result := @RGBtoRGBQuad;
end;

function GetValueConverter(SrcFmt, DstFmt: TImageFormat): Pointer;
begin
 case SrcFmt of
  ifIndexed:
  case DstFmt of
   ifIndexed:   Result := @PaletteConvert;
   else         Result := @IndexedToRGB;
  end;
  else
  case DstFmt of
   ifIndexed:   Result := @RGBtoIndexed;
   else         Result := @RGBConvert;
  end;
 end;
end;

function FillInfo(Info: PConvertInfo; X, Y: Integer): Integer;
var
 BCnt, Sz, B, FullSz: Integer;
begin
 BCnt := Info.BitsCount;
 if Info.Flags and IMG_BYTES_REVERSE = 0 then
 begin
  if Info.Flags and IMG_BITS_SHIFT <> 0 then
   Inc(Info.Pntr, LongRec(Info.RowStride).Hi);
  Inc(Info.Pntr, GetXOffset(X, BCnt));
 end else
 begin
  if Info.Flags and IMG_BITS_SHIFT = 0 then
   Inc(Info.Pntr, LongRec(Info.RowStride).Hi);
  Inc(Info.Pntr, GetXOffset(Info.W - 1 - X, BCnt));
 end;
 if BCnt < 5 then
 begin
  if BCnt = 3 then Inc(BCnt);
  Result := PixelsInByteM[BCnt - 1]; //8 div BCnt - 1;
  if Info.Flags and IMG_BITS_REVERSE = 0 then
  begin
   B := (8 - Info.InitialBitPos) div BCnt;
   Sz := ((B + X) and Result) * BCnt;
   Dec(Info.InitialBitPos, Sz);
  end else
  begin
   B := Info.InitialBitPos div BCnt;
   Sz := ((B + X) and Result) * BCnt;
   Inc(Info.InitialBitPos, Sz)
  end;

  B := Integer((Sz + BCnt <> 8) and ((B + X + Word(Info.RowStride)) and Result <> 0));
  Info.BitPos := Info.InitialBitPos;
 end else B := 0;

 B := BmpImg.GetLineSize(Word(Info.RowStride), BCnt) - B; 
 Sz := BmpImg.GetLineSize(Info.W, BCnt) + LongRec(Info.RowStride).Hi;
 if (Info.Flags and IMG_PLANAR <> 0) and
    (Info.Flags and IMG_PLANE_VECTOR <> 0) then
 begin
  Result := Sz;

//  B := GetPlanarLineSize(Word(Info.RowStride), BCnt, Sz) - B;
  Sz := GetPlanarLineSizeRmndr(Info.W, LongRec(Info.RowStride).Hi, BCnt, Info.Flags and 31 + 1);
  FullSz := Info.H * Sz;
 end else
 begin

  FullSz := Info.H * Sz;
  Result := FullSz;
 end;
 if Info.Flags and IMG_Y_FLIP <> 0 then
 begin
  Inc(Info.Pntr, FullSz - (Y + 1) * Sz);
  if Info.Flags and IMG_BYTES_REVERSE <> 0 then
   Info.RowStride := -(Sz - B) else
   Info.RowStride := -(Sz + B)
 end else
 begin
  Inc(Info.Pntr, Y * Sz);
  if Info.Flags and IMG_BYTES_REVERSE <> 0 then
   Info.RowStride := Sz + B else
   Info.RowStride := Sz - B;
 end;
end; //FillInfo

procedure FillInfoLoop(Root: PConvertInfo; Ptr: PByte; X, Y: Integer);
var
 Info, Save: PConvertInfo;
 SavePtr: PByte;
 I, YY: Integer;
 Vector: Boolean;
begin
 Info := Root;
 while Info <> nil do
 begin
  if Info.Flags and IMG_PLANAR <> 0 then
  begin
   Vector := Info.Flags and IMG_PLANE_VECTOR <> 0;
   SavePtr := Ptr;
   YY := GetPlanarLineSize(1, Info.BitsCount, Info.Flags and 31 + 1);
   if Info.Flags and IMG_PLANE_REVERSE <> 0 then
   begin
    for I := 0 to YY - 2 do Info := Info.Next;
    Save := Info.Next;
    for I := 0 to YY - 1 do
    begin
     Info.Pntr := Ptr;
     Inc(Ptr, FillInfo(Info, X, Y));
     Info := Info.Prev;
    end;
    Info := Save;
   end else
   for I := 0 to YY - 1 do
   begin
    Info.Pntr := Ptr;
    Inc(Ptr, FillInfo(Info, X, Y));
    Info := Info.Next;
   end;
   if Vector then
    Inc(Ptr, (Integer(Ptr) - Integer(SavePtr)) * (Root.H - 1));
  end else
  begin
   Info.Pntr := Ptr;
   if Info.Flags and IMG_COMPRESSED <> 0 then
   begin
    if Info.Flags and IMG_Y_FLIP <> 0 then
    begin
     Info.WordRowStride := -4;
     Inc(Info.Pntr, ((Info.H - 1) - Y) * 4);
    end else
    begin
     Info.WordRowStride := 4;
     Inc(Info.Pntr, Y * 4);
    end;
    Info.InitCounter := X;
    Exit;
   end else
   begin
    Inc(Ptr, FillInfo(Info, X, Y));
    Info := Info.Next;
   end;
  end;
 end;
end; //FillInfoLoop

//const
// Ch: Array[0..3] of TChannelType = (ctBlue, ctGreen, ctRed, ctAlpha);

procedure ArgumentsError;
begin
 raise EImageHolderError.Create('Bad arguments.');
end;

procedure FileFormatError;
begin
 raise EImageExError.Create('File format error.');
end;

procedure BitmapFileFormatError;
begin
 raise EImageExError.Create('BMP file format error.');
end;

procedure PCX_FileFormatError;
begin
 raise EImageExError.Create('PCX file format error.');
end;

procedure ColorFormatError;
begin
 raise EColorFormatError.Create('Color format error.');
end;

procedure ImageFormatError;
begin
 raise EImageHolderError.Create('Image format error.');
end;

procedure DestImageFormatError;
begin
 raise EImageHolderError.Create('Destination image format error.');
end;

{ TColorFormat }

procedure TColorFormat.Assign(ClrFmt: TColorFormat);
begin
 if ClrFmt <> Self then
 begin
  if ClrFmt = nil then
   SetFormat(0, 0, 0, 0) else
   Move(ClrFmt.FColorBits, FColorBits, 11 * 4);
 end;
end;

procedure TColorFormat.AssignMask(ClrFmt: TColorFormat);
begin
 if ClrFmt <> Self then
 begin
  if ClrFmt = nil then
   SetFormat(0, 0, 0, 0) else
   SetFormat(ClrFmt.FRedMask, ClrFmt.FGreenMask, ClrFmt.FBlueMask, ClrFmt.FAlphaMask);
 end;
end;

function TColorFormat.Compress(Source, Dest: Pointer;
                            Count: Integer; SkipValue: LongWord): Integer;
var
 SB: PByte absolute Source;
 SW: PWord absolute Source;
 SD: PLongWord absolute Source;
 DB: PByte;
 Save: Pointer;
 Idx, Cnt: Integer;
begin
 Idx := 0;
 DB := Dest;
 case FColorSize of
  1:
  repeat
   while (Count > 0) and (SB^ = Byte(SkipValue)) do
   begin
    Inc(SB);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SB;
   Cnt := 0;
   while (Count > 0) and (SB^ <> Byte(SkipValue)) do
   begin
    Inc(SB);
    Inc(Cnt);
    Dec(Count);
   end;
   if Cnt >= 256 then
   begin
    Move(Save^, Dest^, 256);
    Result := 0;
    Exit;
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);
   Inc(DB);
   Move(Save^, DB^, Cnt);
   Inc(DB, Cnt);   
  until False;
  2:
  repeat
   while (Count > 0) and (SW^ = Word(SkipValue)) do
   begin
    Inc(SW);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SW;
   Cnt := 0;
   while (Count > 0) and (SW^ <> Word(SkipValue)) do
   begin
    Inc(SW);
    Inc(Cnt);
    Dec(Count);
   end;
   if Cnt >= 256 then
   begin
    Move(Save^, Dest^, 256 * 2);
    Result := 0;
    Exit;
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);
   Cnt := Cnt shl 1;   
   Inc(DB);
   Move(Save^, DB^, Cnt);
   Inc(DB, Cnt);
  until False;
  3:
  begin
   SkipValue := SkipValue and $FFFFFF;
   repeat
    while (Count > 0) and (SD^ and $FFFFFF = SkipValue) do
    begin
     Inc(SB, 3);
     Inc(Idx);
     Dec(Count);
    end;
    Save := SB;
    Cnt := 0;
    while (Count > 0) and (SD^ and $FFFFFF <> SkipValue) do
    begin
     Inc(SB, 3);
     Inc(Cnt);
     Dec(Count);
    end;
    if Cnt >= 256 then
    begin
     Move(Save^, Dest^, 256 * 3);
     Result := 0;
     Exit;
    end;
    DB^ := Cnt;
    Inc(DB);
    if Cnt = 0 then Break;
    DB^ := Idx;
    Inc(Idx, Cnt);
    Cnt := Cnt * 3;      
    Inc(DB);
    Move(Save^, DB^, Cnt);
    Inc(DB, Cnt);
   until False;
  end;
  else
  repeat
   while (Count > 0) and (SD^ = SkipValue) do
   begin
    Inc(SD);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SD;
   Cnt := 0;
   while (Count > 0) and (SD^ <> SkipValue) do
   begin
    Inc(SD);
    Inc(Cnt);
    Dec(Count);
   end;
   if Cnt >= 256 then
   begin
    Move(Save^, Dest^, 256 * 4);
    Result := 0;
    Exit;
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);
   Cnt := Cnt shl 2;   
   Inc(DB);
   Move(Save^, DB^, Cnt);
   Inc(DB, Cnt);
  until False;
 end;
 Result := Integer(DB) - Integer(Dest);
end;

procedure TColorFormat.CompressToRGBQuad(Source, Dest: Pointer;
                                Count: Integer; SkipValue: LongWord);
var
 SB: PByte absolute Source;
 SW: PWord absolute Source;
 SD: PLongWord absolute Source;
 DB: PByte absolute Dest;
 DD: PLongword absolute Dest;
 Save: PByte;
 Idx, Cnt: Integer;
begin
 Idx := 0;
 case FColorSize of
  1:
  repeat
   while (Count > 0) and (SB^ = Byte(SkipValue)) do
   begin
    Inc(SB);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SB;
   Cnt := 0;
   while (Count > 0) and (SB^ <> Byte(SkipValue)) do
   begin
    Inc(SB);
    Inc(Cnt);
    Dec(Count);
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);
   Inc(DB);
   while Cnt > 0 do
   begin
    DD^ := ValueToRGBQuad(Save^);
    Inc(Save);
    Inc(DD);
    Dec(Cnt);
   end;
  until False;
  2:
  repeat
   while (Count > 0) and (SW^ = Word(SkipValue)) do
   begin
    Inc(SW);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SB;
   Cnt := 0;
   while (Count > 0) and (SW^ <> Word(SkipValue)) do
   begin
    Inc(SW);
    Inc(Cnt);
    Dec(Count);
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);   
   Inc(DB);
   while Cnt > 0 do
   begin
    DD^ := ToRGBQuad(Save^);
    Inc(Save, 2);
    Inc(DD);
    Dec(Cnt);
   end;
  until False;
  3:
  begin
   SkipValue := SkipValue and $FFFFFF;
   repeat
    while (Count > 0) and (SD^ and $FFFFFF = SkipValue) do
    begin
     Inc(SB, 3);
     Inc(Idx);
     Dec(Count);
    end;
    Save := SB;
    Cnt := 0;
    while (Count > 0) and (SD^ and $FFFFFF <> SkipValue) do
    begin
     Inc(SB, 3);
     Inc(Cnt);
     Dec(Count);
    end;
    DB^ := Cnt;
    Inc(DB);
    if Cnt = 0 then Break;
    DB^ := Idx;
    Inc(Idx, Cnt);
    Inc(DB);
    while Cnt > 0 do
    begin
     DD^ := ToRGBQuad(Save^);
     Inc(Save, 3);
     Inc(DD);
     Dec(Cnt);
    end;
   until False;
  end;
  else
  if FFlags and 3 = CFLG_RGBQUAD then
  repeat
   while (Count > 0) and (SD^ = SkipValue) do
   begin
    Inc(SD);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SB;
   Cnt := 0;
   while (Count > 0) and (SD^ <> SkipValue) do
   begin
    Inc(SD);
    Inc(Cnt);
    Dec(Count);
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);
   Cnt := Cnt shl 2;   
   Inc(DB);
   Move(Save^, DB^, Cnt);
   Inc(DB, Cnt);
  until False else
  repeat
   while (Count > 0) and (SD^ = SkipValue) do
   begin
    Inc(SD);
    Inc(Idx);
    Dec(Count);
   end;
   Save := SB;
   Cnt := 0;
   while (Count > 0) and (SD^ <> SkipValue) do
   begin
    Inc(SD);
    Inc(Cnt);
    Dec(Count);
   end;
   DB^ := Cnt;
   Inc(DB);
   if Cnt = 0 then Break;
   DB^ := Idx;
   Inc(Idx, Cnt);   
   Inc(DB);
   while Cnt > 0 do
   begin
    DD^ := ToRGBQuad(Save^);
    Inc(Save, 4);
    Inc(DD);
    Dec(Cnt);
   end;
  until False    
 end;
end;

procedure TColorFormat.ConvertFromRGBQuad(Source, Dest: Pointer;
  Count: Integer);
var
 SB: ^LongWord absolute Source;
 DB: PByte absolute Dest;
 DW: PWord absolute Dest;
 DD: PLongWord absolute Dest;
 I: Integer; Value: LongWord;
begin
 if FFlags and 3 <> CFLG_RGBQUAD then
 begin
  case FColorSize of
   1:
   for I := 0 to Count - 1 do
   begin
    DB^ := FromRGBQuad(SB^);
    Inc(SB);
    Inc(DB);
   end;
   2:
   for I := 0 to Count - 1 do
   begin
    DW^ := FromRGBQuad(SB^);
    Inc(SB);
    Inc(DW);
   end;
   3:
   for I := 0 to Count - 1 do
   begin
    Value := FromRGBQuad(SB^);
    DW^ := Value;
    LongRec(DD^).Bytes[2] := Value shr 16;
    Inc(SB);
    Inc(DB, 3);
   end;
   4:
   for I := 0 to Count - 1 do
   begin
    DD^ := FromRGBQuad(SB^);
    Inc(SB);
    Inc(DD);
   end;
  end;
 end else
 if Source <> Dest then
  Move(Source^, Dest^, Count * SizeOf(TRGBQuad));
end;

procedure TColorFormat.ConvertTo(DestFormat: TColorFormat; Source,
  Dest: Pointer; Count: Integer);
var
 RGBQuads: Pointer;
begin
 if Count > 0 then
 begin
  if not SameAs(DestFormat) then
  begin
   if DestFormat.FColorSize = SizeOf(TRGBQuad) then
   begin
    ConvertToRGBQuad(Source, Dest, Count);
    DestFormat.ConvertFromRGBQuad(Dest, Dest, Count);
   end else
   begin
    GetMem(RGBQuads, Count * SizeOf(TRGBQuad));
    try
     ConvertToRGBQuad(Source, RGBQuads, Count);
     DestFormat.ConvertFromRGBQuad(RGBQuads, Dest, Count);
    finally
     FreeMem(RGBQuads);
    end;
   end;
  end else Move(Source^, Dest^, Count * FColorSize);
 end;
end;

procedure TColorFormat.ConvertTo(DestFormat: AnsiString;
  Source, Dest: Pointer; Count: Integer);
var
 ClFmt: TColorFormat;
begin
 ClFmt := TColorFormat.Create(DestFormat);
 try
  ConvertTo(ClFmt, Source, Dest, Count);
 finally
  ClFmt.Free;
 end;
end;

procedure TColorFormat.ConvertToRGBQuad(Source, Dest: Pointer; Count: Integer);
var
 SP: PByte absolute Source;
 DP: PCardinal absolute Dest;
 I: Integer;
begin
 if FFlags and 3 <> CFLG_RGBQUAD then
 begin
  for I := 0 to Count - 1 do
  begin
   DP^ := ToRGBQuad(SP^);
   Inc(SP, FColorSize);
   Inc(DP);
  end;
 end else
  Move(Source^, Dest^, Count * SizeOf(TRGBQuad));
end;

constructor TColorFormat.Create;
begin
 if DefaultColorFormat = nil then
  DefaultColorFormat := BGRZ_ColorFormat;
 Assign(DefaultColorFormat);
end;

constructor TColorFormat.Create(const Format: AnsiString);
begin
 SetFormat(Format);
end;

constructor TColorFormat.CreateFromMask(RBitMask, GBitMask,
  BBitMask, ABitMask: LongWord);
begin
 SetFormat(RBitMask, GBitMask, BBitMask, ABitMask);
end;

procedure TColorFormat.Decompress(Source, Dest: Pointer);
var
 SP: PByte absolute Source;
 I, Cnt: Integer;
begin
 repeat
  Cnt := SP^;
  if Cnt = 0 then Break;
  Cnt := Cnt * FColorSize;
  Inc(SP);
  I := SP^;
  Inc(SP);
  Move(SP^, Pointer(Integer(Dest) + (I * FColorSize))^, Cnt);
  Inc(SP, Cnt);
 until False;
end;

procedure TColorFormat.DecompressToRGBQuad(Source, Dest: Pointer);
type
 PDWordArray = ^TDWordArray;
 TDWordArray = array[Byte] of LongWord;

var
 SP: PByte absolute Source;
 I, Cnt: Integer;
 DestPalette: PDWordArray absolute Dest;
 DD: PLongword;
begin
 if FFlags and 3 <> CFLG_RGBQUAD then
 repeat
  Cnt := SP^;
  if Cnt = 0 then Break;
  Inc(SP);
  I := SP^;
  Inc(SP);
  DD := Addr(DestPalette[I]);
  while Cnt > 0 do
  begin
   DD^ := ToRGBQuad(SP^);
   Inc(DD);
   Inc(SP, FColorSize);
   Dec(Cnt);
  end;
 until False else
 repeat
  Cnt := SP^;
  if Cnt = 0 then Break;
  Cnt := Cnt * SizeOf(TRGBQuad);
  Inc(SP);
  I := SP^;
  Inc(SP);
  Move(SP^, DestPalette[I], Cnt);
  Inc(SP, Cnt);
 until False;
end;

const
 Threshold: array[0..3] of Word = (28, 151, 77, 0);

function TColorFormat.FromRGBQuad(Source: LongWord): LongWord;
asm
 mov ecx,dword ptr [eax + offset TColorFormat.FFlags]
 and ecx,3
 jz @@FullColor  // if FFlags and 3 = CFLG_NORMAL
 dec ecx
 jnz @@GrayScale // if FFlags and 3 = CFLG_GRAYSCALE
 mov eax,edx     // if FFlags and 3 = CFLG_RGBQUAD
 ret
@@GrayScale:
 test MMX_Present,1
 jz @@NoMMX
 push ebx
 mov ebx,eax
 movd mm0,edx
 punpcklbw mm0,mm0
 psrlw mm0,8
 pmullw mm0,Threshold
 movq mm1,mm0
 psllq mm1,16
 paddusw mm0,mm1
 psllq mm1,16
 paddusw mm0,mm1
 psrlq mm0,40
 movd eax,mm0
 emms
 movzx eax,al
 mov cl,byte ptr [ebx + offset TColorFormat.FRedRShift]
 shr eax,cl
 mov cl,byte ptr [ebx + offset TColorFormat.FRedShift]
 shl eax,cl
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_ALPHA
 jz @@NoAlpha1
 shr edx,24 // Get Alpha value
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaRShift]
 shr edx,cl
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaShift]
 shl edx,cl
 or eax,edx
@@NoAlpha1:
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jnz @@Rotate
 pop ebx
 ret
@@NoMMX:
 push ebx
 push eax
 xor eax,eax
 mov al,dl
 mov cl,28
 mul cl          // Blue * 28 +
 mov bx,ax
 mov al,dh
 mov cl,151
 mul cl          // Green * 151 +
 add bx,ax
 bswap edx
 mov al,dh
 mov cl,77       // Red * 77
 mul cl
 add ax,bx
 shr eax,8 // div 256
 pop ebx
 mov cl,byte ptr [ebx + offset TColorFormat.FRedRShift]
 shr eax,cl
 mov cl,byte ptr [ebx + offset TColorFormat.FRedShift]
 shl eax,cl
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_ALPHA
 jz @@NoAlpha2
 movzx edx,dl // edx = Alpha value
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaRShift]
 shr edx,cl
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaShift]
 shl edx,cl
 or eax,edx
@@NoAlpha2:
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jz @@NoRotate2
@@Rotate:
 mov edx,[ebx + offset TColorFormat.FColorSize]
 pop ebx
 dec edx
 jz @@Exit2
 dec edx
 jz @@rtt2
 dec edx
 jz @@rtt3
 bswap eax
@@Exit2:
 ret
@@NoRotate2:
 pop ebx
 ret
@@FullColor:
 push ebx
 push esi
 mov esi,eax
 mov cl,byte ptr [esi + offset TColorFormat.FBlueRShift]
 shr dl,cl
 mov cl,byte ptr [esi + offset TColorFormat.FGreenRShift]
 shr dh,cl
 bswap edx
 mov cl,byte ptr [esi + offset TColorFormat.FRedRShift]
 shr dh,cl
 xor ebx,ebx
 test byte ptr [esi + offset TColorFormat.FFlags],CFLG_ALPHA
 jz @@NoAlpha3
 mov cl,byte ptr [esi + offset TColorFormat.FAlphaRShift]
 shr dl,cl
 movzx ebx,dl
 mov cl,byte ptr [esi + offset TColorFormat.FAlphaShift]
 shl ebx,cl
@@NoAlpha3:
 movzx eax,dh
 mov cl,byte ptr [esi + offset TColorFormat.FRedShift]
 shl eax,cl
 or eax,ebx
 bswap edx
 movzx ebx,dh
 mov cl,byte ptr [esi + offset TColorFormat.FGreenShift]
 shl ebx,cl
 or eax,ebx
 movzx ebx,dl
 mov cl,byte ptr [esi + offset TColorFormat.FBlueShift]
 shl ebx,cl
 or eax,ebx
 test byte ptr [esi + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jz @@NoRotate1
 mov edx,[esi + offset TColorFormat.FColorSize]
 pop esi
 pop ebx
 dec edx
 jz @@Exit // if ColorSize = 1
 dec edx
 jz @@rtt2 // if ColorSize = 2
 dec edx
 jz @@rtt3 // if ColorSize = 3
 bswap eax // if ColorSize >= 4
@@Exit:
 ret
@@rtt3:
 bswap eax
 shr eax,8
 ret
@@rtt2:
 xchg al,ah
 ret
@@NoRotate1:
 pop esi
 pop ebx
end;  {
begin
 if Flags and 3 = CFLG_GRAYSCALE then
 begin
  Result := (Byte((Source.rgbRed * 77) shr 8 +
  (Source.rgbGreen * 151) shr 8 +
  (Source.rgbBlue * 28) shr 8) shr FAlphaRShift) shl FAlphaShift;
 end else
 begin
  Result := (Source.rgbRed shr FRedRShift) shl FRedShift;
  Result := Result or (Source.rgbGreen shr FGreenRShift) shl FGreenShift;
  Result := Result or (Source.rgbBlue shr FBlueRShift) shl FBlueShift;
  if Flags and CFLG_ALPHA <> 0 then
  Result := Result or (Source.rgbReserved shr FAlphaRShift) shl FAlphaShift;
 end;
end;          }

function TColorFormat.GetAlpha: Boolean;
begin
 Result := FFlags and CFLG_ALPHA <> 0;
end;

function TColorFormat.GetByteSwap: Boolean;
begin
 Result := FFlags and CFLG_BYTESWAP <> 0;
end;

function TColorFormat.GetChannelsCount: Integer;
function CountZero(UsedBits, Mask: Integer): Integer;
var
 Cnt: Integer;
begin
 Result := 0;
 while UsedBits > 0 do
 begin
  if Mask and 1 = 0 then
  begin
   Inc(Result);
   Mask := Mask shr 1;
   Dec(UsedBits);
   Cnt := 7;
   while (Cnt > 0) and (UsedBits > 0) and (Mask and 1 = 0) do
   begin
    Mask := Mask shr 1;
    Dec(UsedBits);
    Dec(Cnt);
   end;
  end else
  begin
   Mask := Mask shr 1;
   Dec(UsedBits);
  end;
 end;
end;
begin
 if FFlags and CFLG_GRAYSCALE <> 0 then
  Result := Byte(FRedCount > 0) +
            Byte(FAlphaCount > 0) +
            CountZero(FUsedBits, FRedMask or FAlphaMask) else
  Result := Byte(FRedCount > 0) +
            Byte(FGreenCount > 0) +
            Byte(FBlueCount > 0) +
            Byte(FAlphaCount > 0) +
            CountZero(FUsedBits, FRedMask or FGreenMask or
                                 FBlueMask or FAlphaMask);
end;

function TColorFormat.SameAs(const CFormat: TColorFormat): Boolean;
begin
 Result := (CFormat <> nil) and ((Self = CFormat) or
   CompareMem(@FColorBits, Addr(CFormat.FColorBits), 11 * 4));
end;

procedure TColorFormat.SetAlpha(Value: Boolean);
begin
 if Value then
  FFlags := FFlags or CFLG_ALPHA else
  FFlags := FFlags and not CFLG_ALPHA;
end;

procedure TColorFormat.SetByteSwap(Value: Boolean);
begin
 if FFlags and 3 <> CFLG_GRAYSCALE then
 begin
  if Value then FFlags := (FFlags and not 3) or CFLG_BYTESWAP else
  begin
   FFlags := FFlags and not CFLG_BYTESWAP;
   if (FFlags and CFLG_8888 <> 0) and
      (FBlueShift = 0) and (FGreenShift = 8) and
      (FRedShift = 16) and (FAlphaShift = 24) then
    FFlags := (FFlags and not 3) or CFLG_RGBQUAD else
    FFlags := FFlags and not 3;
  end;
 end;
end;

procedure TColorFormat.SetFormat(const Format: AnsiString);
const
 C_GRAY = Ord('G') +
          Ord('R') shl 8 +
          Ord('A') shl 16 +
          Ord('Y') shl 24;
var
 I, Count, chBitsCount, Increment: Integer; //P: ^TChannelType;
 SF, SC: PAnsiChar; GRAY: array[0..3] of AnsiChar;
{ IsRGBQuad: Boolean;} chType: TChannelType;
begin
 Count := Length(Format) shr 1;
 if Count <= 0 then ColorFormatError;
 if Count >= 2 then
  Move(Pointer(Format)^, GRAY, 4);
 GRAY[0] := UpCase(GRAY[0]);
 GRAY[1] := UpCase(GRAY[1]);
 GRAY[2] := UpCase(GRAY[2]);
 GRAY[3] := UpCase(GRAY[3]);
 FRedMask := 0;
 FGreenMask := 0;
 FBlueMask := 0;
 FAlphaMask := 0;
 FRedShift := 0;
 FGreenShift := 0;
 FBlueShift := 0;
 FAlphaShift := 0;
 FRedCount := 0;
 FGreenCount := 0;
 FBlueCount := 0;
 FAlphaCount := 0;
 if LongWord(GRAY) = C_GRAY then
 begin
  if (Length(Format) < 5) or not (Format[5] in ['1'..'8']) then
   ColorFormatError;
  FRedCount := Ord(Format[5]) - Ord('0');
  FRedShift := 0;
  FUsedBits := FRedCount;
  FRedMask := (1 shl FRedCount) - 1;
  FGreenMask := FRedMask;
  FGreenCount := FRedCount;
  FBlueMask := FRedMask;
  FBlueCount := FRedCount;
  FColorBits := FRedCount;
  if (Length(Format) >= 6) and (Format[6] in ['1'..'8']) then
  begin
   FAlphaCount := Ord(Format[6]) - Ord('0');
   FAlphaShift := FRedCount;
   Inc(FUsedBits, FAlphaCount);
   FAlphaMask := ((1 shl FAlphaCount) - 1) shl FAlphaShift;
   if (FUsedBits > 8) and (Length(Format) >= 7) and (Format[7] = '*') then
    FFlags := CFLG_GRAYSCALE or CFLG_ALPHA or CFLG_BYTESWAP else
    FFlags := CFLG_GRAYSCALE or CFLG_ALPHA;
  end else
   FFlags := CFLG_GRAYSCALE;
  FColorSize := (FUsedBits + 7) shr 3;
  FRedRShift := 8 - FRedCount;
  FGreenRShift := 8 - FGreenCount;
  FBlueRShift := 8 - FBlueCount;
  FAlphaRShift := 8 - FAlphaCount;
  Exit;
 end;
 if Format[Length(Format)] = '*' then
  FFlags := CFLG_BYTESWAP else
  FFlags := 0;
// IsRGBQuad := FFlags = 0;
 SF := Pointer(Format);
 SC := Pointer(Integer(SF) + 1);
 Increment := 1;
 case SF^ of
  '1'..'8':
  case SC^ of
   '1'..'8': //888RGB
   begin
    SC := SF;
    Inc(SF, Count);
   end;
   else //8R8G8B
   begin
    Inc(Increment);
    SF := SC;
    SC := Pointer(Format);
   end;
  end;
  else
  case SC^ of
   '1'..'8': Inc(Increment); // R8G8B8 
   else Inc(SC, Count - 1); //RGB888
  end;
 end;
// P := Addr(Ch);
 FUsedBits := 0;
 FColorBits := 0;
// IsRGBQuad := IsRGBQuad and (Count = 4);
 for I := 0 to Count - 1 do
 begin
  if not (SC^ in ['1'..'8']) then
   ColorFormatError;
  chBitsCount := Ord(SC^) - Ord('0');
  chType := ctDummy;
  case SF^ of
   'A','a':
   begin
    chType := ctAlpha;
    FAlphaShift := FUsedBits;
    FAlphaMask := (1 shl chBitsCount - 1) shl FUsedBits;
    FAlphaCount := chBitsCount;
    FFlags := FFlags or CFLG_ALPHA;
   end;
   'B','b':
   begin
    chType := ctBlue;
    FBlueShift := FUsedBits;
    FBlueMask := (1 shl chBitsCount - 1) shl FUsedBits;
    FBlueCount := chBitsCount;
   end;
   'G','g':
   begin
    chType := ctGreen;
    FGreenShift := FUsedBits;
    FGreenMask := (1 shl chBitsCount - 1) shl FUsedBits;
    FGreenCount := chBitsCount;
   end;
   'R','r':
   begin
    chType := ctRed;
    FRedShift := FUsedBits;
    FRedMask := (1 shl chBitsCount - 1) shl FUsedBits;
    FRedCount := chBitsCount;
   end;
   'W','w': //GRAY
   begin
    chType := ctGray;
    FRedShift := FUsedBits;
    FRedMask := (1 shl chBitsCount - 1) shl FUsedBits;
    FRedCount := chBitsCount;
    FGreenShift := FRedShift;
    FGreenMask := FRedMask;
    FGreenCount := chBitsCount;
    FBlueShift := FRedShift;
    FBlueMask := FRedMask;
    FBlueCount := chBitsCount;
   end;
   'Z','z':
   begin
    if FAlphaCount = 0 then
    begin
     FAlphaShift := FUsedBits;
     FAlphaMask := (1 shl chBitsCount - 1) shl FUsedBits;
     FAlphaCount := chBitsCount;
    end;
   end else
    ColorFormatError;
  end;
  if chType in [ctRed, ctGreen, ctBlue, ctGray] then
   Inc(FColorBits, chBitsCount);
  Inc(FUsedBits, chBitsCount);
  Inc(SF, Increment);
  Inc(SC, Increment);
 end;

 if (FRedMask = FBlueMask) and (FGreenMask = FBlueMask) then
  FFlags := FFlags or CFLG_GRAYSCALE else
 begin
  if (FRedCount = 8) and (FGreenCount = 8) and
     (FBlueCount = 8) and (FAlphaCount = 8) and
     (FRedShift and 7 = 0) and (FGreenShift and 7 = 0) and
     (FBlueShift and 7 = 0) and (FAlphaShift and 7 = 0) then
   FFlags := FFlags or CFLG_8888;

  if FUsedBits <= 8 then
   FFlags := FFlags and not CFLG_BYTESWAP;

  if (FFlags and CFLG_BYTESWAP <> 0) and
     (FFlags and CFLG_8888 <> 0) then
  asm
   mov ecx,Self
   mov eax,dword ptr [ecx + offset TColorFormat.FBlueShift]
   bswap eax
   mov dword ptr [ecx + offset TColorFormat.FBlueShift],eax
 //  mov eax,dword ptr [ecx + offset TColorFormat.FBlueCount]
 //  bswap eax               // They are same! Swap is unnecessary
 //  mov dword ptr [ecx + offset TColorFormat.FBlueCount],eax
   mov eax,[ecx + offset TColorFormat.FBlueMask]
   xchg eax,[ecx + offset TColorFormat.FAlphaMask]
   mov [ecx + offset TColorFormat.FBlueMask],eax
   mov eax,[ecx + offset TColorFormat.FGreenMask]
   xchg eax,[ecx + offset TColorFormat.FRedMask]
   mov [ecx + offset TColorFormat.FGreenMask],eax
   and dword ptr [ecx + offset TColorFormat.FFlags],not CFLG_BYTESWAP
  end;

  if (FFlags and CFLG_8888 <> 0) and
     (FBlueShift = 0) and (FGreenShift = 8) and
     (FRedShift = 16) and (FAlphaShift = 24) then
   FFlags := FFlags or CFLG_RGBQUAD;
 end;
 FColorSize := (FUsedBits + 7) shr 3;
 FRedRShift := 8 - FRedCount;
 FGreenRShift := 8 - FGreenCount;
 FBlueRShift := 8 - FBlueCount;
 FAlphaRShift := 8 - FAlphaCount;
end;

procedure TColorFormat.SetColor(Dest: Pointer; Index: Integer;
  RGBQuad: LongWord);
var
 Value: LongWord;
 PB: PByte absolute Dest;
 PW: PWord absolute PB;
 PD: PLongWord absolute PW;
begin
 Inc(PB, Index * ColorSize);
 Value := FromRGBQuad(RGBQuad);
 case ColorSize of
  4: PD^ := Value;
  3:
  begin
   PW^ := Value;
   Inc(PW);
   PB^ := Value shr 16;
  end;
  2: PW^ := Value;
  else PB^ := Value;
 end;
end;

procedure TColorFormat.SetColor(var Dest; RGBQuad: LongWord);
var
 B: Byte absolute Dest;
 W: Word absolute Dest;
 D: LongWord absolute Dest;
 PB: PByte;
 Value: LongWord;
begin
 Value := FromRGBQuad(RGBQuad);
 case ColorSize of
  4: D := Value;
  3:
  begin
   W := Value;
   PB := @Dest;
   Inc(PB, 2);
   PB^ := Value shr 16;
  end;
  2: W := Value;
  else B := Value;
 end;
end;

procedure TColorFormat.SetFormat(RBitMask,GBitMask,BBitMask,ABitMask: LongWord);
var
 Position: Integer;

 function GetBitCount(B: LongWord): Integer;
 var
  I: LongWord;
 begin
  Position := 0;
  Result := 0;
  if B = 0 then Exit;
  
  I := 1;
  while (I <> 0) and (B and I = 0) do
  begin
   I := I shl 1;
   Inc(Position);
  end;

  while B and I <> 0 do
  begin
   I := I shl 1;
   Inc(Result);
  end;
 end;

begin
 FRedMask := RBitMask;
 FGreenMask := GBitMask;
 FBlueMask := BBitMask;
 FAlphaMask := ABitMask;
 FRedCount := GetBitCount(RBitMask);
 FRedShift := Position;
 FGreenCount := GetBitCount(GBitMask);
 FGreenShift := Position;
 FBlueCount := GetBitCount(BBitMask);
 FBlueShift := Position;
 FAlphaCount := GetBitCount(ABitMask);
 FAlphaShift := Position;
 if (FRedShift = FGreenShift) and (FBlueShift = FGreenShift) then
 begin
  FFlags := CFLG_GRAYSCALE;
  FColorBits := FRedCount;
 end else
 begin
  FColorBits := FRedCount + FGreenCount + FBlueCount;
  FFlags := CFLG_NORMAL;
  if (FRedCount = 8) and (FGreenCount = 8) and
     (FBlueCount = 8) and (FAlphaCount = 8) and
     (FRedShift and 7 = 0) and (FGreenShift and 7 = 0) and
     (FBlueShift and 7 = 0) and (FAlphaShift and 7 = 0) then
  begin
   if (FBlueShift = 0) and (FGreenShift = 8) and
      (FRedShift = 16) and (FAlphaShift = 24) then
    FFlags := CFLG_RGBQUAD;
   FFlags := FFlags or CFLG_8888;
  end;
 end;
 FUsedBits := Max(Max(FRedShift + FRedCount, FGreenShift + FGreenCount),
                  Max(FBlueShift + FBlueCount, FAlphaShift + FAlphaCount));
 FColorSize := (FUsedBits + 7) shr 3;
 if FAlphaCount > 0 then
  FFlags := FFlags or CFLG_ALPHA;
 FRedRShift := 8 - FRedCount;
 FGreenRShift := 8 - FGreenCount;
 FBlueRShift := 8 - FBlueCount;
 FAlphaRShift := 8 - FAlphaCount;
end;

procedure torgbqGrayWithAlpha; forward;
procedure torgbqGrayscale; forward;
procedure torgbqRGB; forward;

function TColorFormat.ToRGBQuad(const Source): LongWord;
asm
 mov ecx,dword ptr [eax + offset TColorFormat.FFlags]
 and ecx,3
 jz @@Normal     // if FFlags and 3 = CFLG_NORMAL
 dec ecx
 jnz @@GrayScale // if FFlags and 3 = CFLG_GRAYSCALE
 mov eax,[edx]   // if FFlags and 3 = CFLG_RGBQUAD
 ret
@@GrayScale:
 push ebx
 call @@ReadValue // returns value in edx, object data in ebx
 jmp torgbqGrayscale;
@@Normal:
 push ebx
 call @@ReadValue // returns value in edx, object data in ebx
 jmp torgbqRGB
@@ReadValue:
 mov ebx,eax
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jz @@NoSwap
 mov ecx,dword ptr [ebx + offset TColorFormat.FColorSize]
 dec ecx
 jz @@rtt1   // if ColorSize = 1
 dec ecx
 jz @@rtt2   // if ColorSize = 2
 dec ecx
 jz @@rtt3   // if ColorSize = 3
@@rtt4:
 mov edx,[edx]
 bswap edx   // if ColorSize >= 4
 ret
@@rtt3:
 mov edx,[edx]
 bswap edx
 shr edx,8
 ret
@@rtt2:
 mov dx,[edx]
 xchg dl,dh
 ret
@@rtt1:
 mov dl,[edx]
 ret
@@NoSwap:
 mov ecx,dword ptr [ebx + offset TColorFormat.FColorSize]
 dec ecx
 jz @@rtt1     // if ColorSize = 1
 dec ecx
 jz @@nsrtt2   // if ColorSize = 2
 mov edx,[edx] // if ColorSize >= 3
 ret
@@nsrtt2:
 mov dx,[edx]
end;{
var
 Value8: Byte absolute Source;
 Value16: Word absolute Source;
 Value24: TRGB absolute Source;
 Value32: LongWord absolute Source;
 Value: LongWord;
begin
 if FFlags and 3 = CFLG_RGBQUAD then
 begin
  Result := Value32;
  Exit;
 end;
 if FFlags and 3 = CFLG_GRAYSCALE then
 begin
  if (FColorSize <= 1) or (FFlags and CFLG_BYTESWAP = 0) then
  with TRGBQuad(Result) do
  begin
   rgbRed := ConvertColorValue(FRedCount, (Value16 and FRedMask) shr FRedShift);
   rgbGreen := rgbRed;
   rgbBlue := rgbRed;
   rgbReserved := ConvertColorValue(FAlphaCount, (Value16 and FAlphaMask) shr FAlphaShift);
  end else
  with TRGBQuad(Result) do
  begin
   Value := SwapWord(Value16);
   rgbRed := ConvertColorValue(FRedCount, (Value and FRedMask) shr FRedShift);
   rgbGreen := rgbRed;
   rgbBlue := rgbRed;
   rgbReserved := ConvertColorValue(FAlphaCount, (Value and FAlphaMask) shr FAlphaShift);
  end;
  Exit;
 end else
 if FFlags and CFLG_BYTESWAP = 0 then
 case FColorSize of
  2:   Value := Value16;
  3:
  begin
   LongRec(Value).Lo := Value16;
   LongRec(Value).Bytes[2] := Value24.B;
  end;
  4:   Value := Value32;
  else Value := Value8;
 end else
 case FColorSize of
  2:   Value := SwapWord(Value16);
  3:
  begin
   LongRec(Value).Bytes[0] := Value24.B;
   Word(Addr(LongRec(Value).Bytes[1])^) := SwapWord(Value16);
  end;
  4:   Value := SwapInt(Value32);
  else Value := Value8;
 end;
 with TRGBQuad(Result) do
 begin
  rgbRed := ConvertColorValue(FRedCount, (Value and FRedMask) shr FRedShift);
  rgbGreen := ConvertColorValue(FGreenCount,(Value and FGreenMask)shr FGreenShift);
  rgbBlue := ConvertColorValue(FBlueCount,(Value and FBlueMask) shr FBlueShift);
  if FFlags and CFLG_ALPHA <> 0 then
   rgbReserved := ConvertColorValue(FAlphaCount,
                  (Value and FAlphaMask)shr FAlphaShift) else
   rgbReserved := 255;
 end;
end;     }

procedure torgbqRGB;
asm
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_ALPHA
 jnz @@RGBwithAlpha
 mov ax,$FF00 // alpha = 255
 jmp @@SkipAlpha
@@RGBwithAlpha:
 push edx // save value
 and edx,dword ptr [ebx + offset TColorFormat.FAlphaMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FAlphaCount]
 call ConvertColorValue
 pop edx // restore value
 shl eax,8 
@@SkipAlpha:
 push edx // save value
 and edx,dword ptr [ebx + offset TColorFormat.FRedMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FRedShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FRedCount]
 call ConvertColorValue
 pop edx // restore value
 shl eax,8
 push edx // save value
 and edx,dword ptr [ebx + offset TColorFormat.FGreenMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FGreenShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FGreenCount]
 call ConvertColorValue
 pop edx // restore value
 shl eax,8
 and edx,dword ptr [ebx + offset TColorFormat.FBlueMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FBlueShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FBlueCount]
 call ConvertColorValue
 pop ebx
end;

procedure torgbqGrayWithAlpha;
asm
 mov ah,al // red = result
 and edx,dword ptr [ebx + offset TColorFormat.FAlphaMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FAlphaShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FAlphaCount]
 call ConvertColorValue
 mov dl,ah // dl = red
 bswap eax
 mov al,dl // green = red
 mov ah,dl // blue  = red
 pop ebx
end;

procedure torgbqGrayScale;
asm
 push edx // save value
 and edx,dword ptr [ebx + offset TColorFormat.FRedMask]
 mov cl,byte ptr [ebx + offset TColorFormat.FRedShift]
 shr edx,cl
 mov al,byte ptr [ebx + offset TColorFormat.FRedCount]
 call ConvertColorValue
 pop edx
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_ALPHA
 jnz torgbqGrayWithAlpha
 mov ah,al        // red   = result
 shl eax,8        // green = red
 mov al,ah        // blue  = green
 or eax,$FF000000 // alpha = 255
 pop ebx
end;

function TColorFormat.ValueToRGBQuad(Value: LongWord): LongWord;
asm
 mov ecx,dword ptr [eax + offset TColorFormat.FFlags]
 and ecx,3
 jz @@Normal     // if FFlags and 3 = CFLG_NORMAL
 dec ecx
 jnz @@GrayScale // if FFlags and 3 = CFLG_GRAYSCALE
 mov eax,edx     // if FFlags and 3 = CFLG_RGBQUAD
 ret
@@GrayScale:
 push ebx
 mov ebx,eax
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jz torgbqGrayScale
 call @@ReadValue // returns value in edx, object data in ebx
 jmp torgbqGrayScale
@@Normal:
 push ebx
 mov ebx,eax
 test byte ptr [ebx + offset TColorFormat.FFlags],CFLG_BYTESWAP
 jz torgbqRGB
 call @@ReadValue // returns value in edx, object data in ebx
 jmp torgbqRGB
@@ReadValue:
 mov ecx,dword ptr [ebx + offset TColorFormat.FColorSize]
 dec ecx
 jz @@rtt1   // if ColorSize = 1
 dec ecx
 jz @@rtt2   // if ColorSize = 2
 dec ecx
 jz @@rtt3   // if ColorSize = 3
@@rtt4:
 bswap edx   // if ColorSize >= 4
 ret
@@rtt3:
 bswap edx
 shr edx,8
 ret
@@rtt2:
 xchg dl,dh
 ret
@@rtt1:
 mov dl,[edx]
end;

const
 STR_GRAY: AnsiString = 'GRAY';

function TColorFormat.GetFormatString: AnsiString;
const
 OrderChar: array[0..3] of AnsiChar = ('B', 'G', 'R', 'Z');
 SUnknownFormat: AnsiString = 'Unknown format';
var
 ShiftPtr: PByte;
 ShiftMinPtr: PByte;
 I, J, Diff, Bits: Integer;
 Order: array[0..3] of Byte;
begin
 if (FColorSize > 4) or (FUsedBits > 32) then
 begin
  Result := SUnknownFormat;
  Exit;
 end;
 Result := '';
 Bits := 0;
 if Flags and 3 = CFLG_GRAYSCALE then
 begin
  if (FRedCount = 0) or (FRedCount > 8) then
  begin
   Result := SUnknownFormat;
   Exit;
  end;
  if (FRedShift > 0) or
     (FUsedBits > FRedCount) or
     (FColorSize * 8 > FRedCount) or
     (Flags and CFLG_ALPHA <> 0) then
  begin
   if FAlphaShift < FRedShift then
   begin
    if FAlphaShift + FAlphaCount > FRedShift then
    begin
     Result := SUnknownFormat;
     Exit;
    end else
    begin
     if FAlphaCount > 8 then
     begin
      Result := SUnknownFormat;
      Exit;
     end;
     if FAlphaCount > 0 then
     begin
      Diff := FAlphaShift;
      Inc(Bits, Diff);
      while Diff > 0 do
      begin
       Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
       Dec(Diff, 8);
      end;
      Result := Result + 'A' + AnsiChar(FAlphaCount + $30);
      Inc(Bits, FAlphaCount);

      Diff := FRedShift - (FAlphaShift + FAlphaCount);
     end else
      Diff := FRedShift;

     Inc(Bits, Diff);
     while Diff > 0 do
     begin
      Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
      Dec(Diff, 8);
     end;
     Result := Result + 'W' + AnsiChar(Min(FRedCount, 8) + $30);
     Inc(Bits, FRedCount);
    end;
   end else
   begin
    if (FAlphaCount > 0) and (FRedShift + FRedCount > FAlphaShift) then
    begin
     Result := SUnknownFormat;
     Exit;
    end else
    begin
     Diff := FRedShift;
     Inc(Bits, Diff);
     while Diff > 0 do
     begin
      Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
      Dec(Diff, 8);
     end;
     Result := Result + 'W' + AnsiChar(FRedCount + $30);
     Inc(Bits, FRedCount);

     if FAlphaCount > 8 then
     begin
      Result := SUnknownFormat;
      Exit;
     end;
     if FAlphaCount > 0 then
     begin
      Diff := FAlphaShift - (FRedShift + FRedCount);
      Inc(Bits, Diff);
      while Diff > 0 do
      begin
       Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
       Dec(Diff, 8);
      end;
      Result := Result + 'A' + AnsiChar(FAlphaCount + $30);
      Inc(Bits, FAlphaCount);
     end;
    end;
   end;
  end else
  begin
   Result := STR_GRAY + AnsiChar(Min(FRedCount, 8) + $30);
   Exit;
  end;
 end else
 begin
  if (FRedCount > 8) or
     (FGreenCount > 8) or
     (FBlueCount > 8) or
     (FAlphaCount > 8) then
  begin
   Result := SUnknownFormat;
   Exit;
  end;
  FillChar(Order, 4, 255);
  I := 0;
  repeat
   ShiftPtr := @FBlueShift;
   ShiftMinPtr := nil;
   for J := 0 to 3 do
   begin
    if ((I = 0) or (ShiftPtr^ > PByteArray(@FBlueShift)[Order[I - 1]])) and
       (PByteArray(@FBlueCount)[J] > 0) and
       ((ShiftMinPtr = nil) or (ShiftPtr^ < ShiftMinPtr^)) then
     ShiftMinPtr := ShiftPtr;
    Inc(ShiftPtr);
   end;
   
   if ShiftMinPtr = nil then
    Break;

   J := Integer(ShiftMinPtr) - Integer(@FBlueShift);
   Order[I] := J;

   if I = 0 then
    Diff := ShiftMinPtr^ else
    Diff := (ShiftMinPtr^ - PByteArray(@FBlueShift)[Order[I - 1]]) -
          PByteArray(@FBlueCount)[Order[I - 1]];

   if Diff < 0 then
   begin
    Result := SUnknownFormat;
    Exit;
   end;

   Inc(Bits, Diff);

   while Diff > 0 do
   begin
    Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
    Dec(Diff, 8);
   end;

   if (J = 3) and (Flags and CFLG_ALPHA <> 0) then
    Result := Result + 'A' else
    Result := Result + OrderChar[J];

   J := PByteArray(@FBlueCount)[J];
   Result := Result + AnsiChar(J + $30);
   Inc(Bits, J);
   Inc(I);
  until False;
 end;

 if Bits > 32 then
 begin
  Result := SUnknownFormat;
  Exit;
 end;
 
 Diff := Max(FColorSize * 8, FUsedBits) - Bits;
 while Diff > 0 do
 begin
  Result := Result + 'Z' + AnsiChar(Min(Diff, 8) + $30);
  Dec(Diff, 8);
 end;

 if Flags and CFLG_BYTESWAP <> 0 then
  Result := Result + '*';
end;

function TColorFormat.ConvertValue(DestFormat: TColorFormat;
  const Source): LongWord;
begin
 Result := DestFormat.FromRGBQuad(ToRGBQuad(Source));
end;

function TColorFormat.ConvertValue(const DestFormat: AnsiString;
  const Source): LongWord;
begin
 with TColorFormat.Create(DestFormat) do
 try
  Result := FromRGBQuad(Self.ToRGBQuad(Source));
 finally
  Free;
 end;
end;

function TColorFormat.IsGray: Boolean;
begin
 Result := FFlags and 3 = CFLG_GRAYSCALE;
end;

{ TBitmapContainer }

function TBitmapContainer.CompareImage(Image: TBitmapContainer): Boolean;
var
 L, BCnt: Integer;
begin
 BCnt := BitsCount;
 L := Length(CompositeFlags);
 Result := (FWidth = Image.FWidth) and
           (FHeight = Image.FHeight) and
           (FImageFlags = Image.FImageFlags) and
           (FImageFormat = Image.FImageFormat) and
           (L = Length(Image.CompositeFlags)) and
           ((L = 0) or CompareMem(Pointer(CompositeFlags),
                       Pointer(Image.CompositeFlags), L shl 1)) and
           ((FImageData = Image.FImageData) or
            ((FImageData <> nil) and (Image.FImageData <> nil) and
             CompareMem(FImageData, Image.FImageData, ImageSize))) and
           ((FColorFormat = Image.FColorFormat) or
        ((FColorFormat <> nil) and FColorFormat.SameAs(Image.FColorFormat))) and
           ((FColorTable = Image.FColorTable) or
            ((FColorTable <> nil) and (Image.FColorTable <> nil) and
             CompareMem(FColorTable, Image.FColorTable, (1 shl BCnt) *
             FColorFormat.ColorSize)));
end;

procedure TBitmapContainer.Draw(Dest: TBitmapContainer; X, Y, Flags: Integer);
var
 InitInfo: TConvertInitInfo;
 SrcInfo, DstInfo, Info: PConvertInfo;
 I, Width, Height, SrcX, SrcY, SrcImgFlags, DstImgFlags, DestFlags: Integer;
 ValueCvt: Pointer;
 PixelModifier: LongWord;
begin
 Width := FWidth;
 Height := FHeight;
 Inc(X, FLeft);
 Inc(Y, FTop);

 if X < 0 then
 begin
  SrcX := -X;
  X := 0;
  Dec(Width, SrcX)
 end else SrcX := 0;

 I := Dest.FWidth - X;
 if I <= 0 then Exit;
 if Width > I then Width := I;

 if Y < 0 then
 begin
  SrcY := -Y;
  Y := 0;
  Dec(Height, SrcY)
 end else SrcY := 0;

 I := Dest.FHeight - Y;
 if I <= 0 then Exit;
 if Height > I then Height := I;

 PixelModifier := Byte(Flags);
 LongRec(Flags).Bytes[0] := 0;
 if Dest.FCompressed or (FColorFormat = nil) or
    (FColorFormat.FFlags and CFLG_ALPHA = 0) then
  Flags := Flags and not DRAW_ALPHA_BLEND;
 ValueCvt := nil;
 if Flags and DRAW_ALPHA_BLEND <> 0 then
 begin
  if (FImageFormat = ifIndexed) or (FColorFormat.FFlags and CFLG_RGBQUAD = 0) then
   ValueCvt := GetToRgbQuadConverter(FImageFormat);
  Flags := Flags or DRAW_DST_PROC or DRAW_VALUE_CONVERT;
 end else
 if Flags and DRAW_NO_CONVERT = 0 then
 begin
  ValueCvt := GetValueConverter(FImageFormat, Dest.FImageFormat);
  if ValueCvt <> nil then
   if (Dest.FImageFormat = ifIndexed) or
      (FImageFormat = ifIndexed) or
      not FColorFormat.SameAs(Dest.FColorFormat) then
    Flags := Flags or DRAW_VALUE_CONVERT;
 end;
 if (Length(CompositeFlags) > 0) or
    (FImageFlags and IMG_PLANAR <> 0) then
  Flags := Flags or DRAW_SRC_COMPOSITE;
 if (Length(Dest.CompositeFlags) > 0) or
    (Dest.FImageFlags and IMG_PLANAR <> 0) then
  Flags := Flags or DRAW_DST_COMPOSITE;

 if PixelModifier > 0 then
  Flags := Flags or DRAW_PIXEL_MODIFY;
 SrcInfo := nil;
 DstInfo := nil;
 try
  SrcImgFlags := FImageFlags;
  DstImgFlags := Dest.FImageFlags;
  if FCompressed then
  begin
   if Flags and DRAW_X_FLIP <> 0 then
   begin
    DestFlags := DRAW_X_FLIP;
    Flags := Flags xor DRAW_X_FLIP;
   end else
    DestFlags := 0;
   Flags := Flags or DRAW_COMPRESSED;
   SrcImgFlags :=
    (SrcImgFlags and not 31) or ((BitsCount - 1) and 31);
  end else
  if Dest.FCompressed then
  begin
   Inc(Dest.FLeft, X);
   Inc(Dest.FTop, Y);
   Dest.FWidth := Width;
   Dest.FWidthRemainder := 0;
   Dest.FHeight := Height;
   X := 0;
   Y := 0;   
   DestFlags := DRAW_COMPRESSED;
   DstImgFlags :=
    (DstImgFlags and not 31) or ((Dest.BitsCount - 1) and 31);
  end else
   DestFlags := 0;
  SrcInfo := AddInfo(nil, SrcImgFlags, Flags, Width, FWidthRemainder, FWidth, FHeight);
  InitInfo.DrawFlags := Flags;
  InitInfo.SrcFlags := SrcInfo.Flags;
  InitInfo.DstFlags := Dest.FImageFlags;
  InitInfo.ConvertFrom := FConvertFrom;
  InitInfo.ConvertTo := Dest.FConvertTo;
  Info := SrcInfo;
  while SrcInfo.Prev <> nil do SrcInfo := SrcInfo.Prev;
  SrcInfo.UseProcs :=
   Byte((Flags and (DRAW_SRC_PROC or DRAW_SRC_COMPOSITE) <> 0) or
             FCompressed or Dest.FCompressed);
  if not FCompressed then
   for I := 0 to Length(CompositeFlags) - 1 do
    Info := AddInfo(Info, CompositeFlags[I], Flags,
               Width, FWidthRemainder, FWidth, FHeight);

  DstInfo := AddInfo(nil, DstImgFlags, DestFlags, Width,
             Dest.FWidthRemainder, Dest.FWidth, Dest.FHeight);
  Info := DstInfo;
  while DstInfo.Prev <> nil do DstInfo := DstInfo.Prev;
  DstInfo.UseProcs :=
   Byte((Flags and (DRAW_ALPHA_BLEND or DRAW_DST_PROC or DRAW_DST_COMPOSITE) <> 0) or
            FCompressed or Dest.FCompressed);
  if not Dest.FCompressed then
   for I := 0 to Length(Dest.CompositeFlags) - 1 do
    Info := AddInfo(Info, Dest.CompositeFlags[I], DestFlags, Width,
             Dest.FWidthRemainder, Dest.FWidth, Dest.FHeight);
  InitBitsPositions(SrcInfo);
  InitBitsPositions(DstInfo);
  InitProcs(SrcInfo, DstInfo);
  if DestFlags and DRAW_X_FLIP <> 0 then // DRAW_X_FLIP for compressed source
  begin
   Inc(SrcX, X);
   X := Dest.FWidth - (X + Width);
  end;
  FillInfoLoop(SrcInfo, FImageData, SrcX, SrcY);
  FillInfoLoop(DstInfo, Dest.FImageData, X, Y);
  SrcInfo.ClrTbl := Self.FColorTable;
  SrcInfo.ClrFmt := Self.FColorFormat;
  DstInfo.ClrTbl := Dest.FColorTable;
  DstInfo.ClrFmt := Dest.FColorFormat;
  SrcInfo.W := Width;
  SrcInfo.H := Height;
  if PixelModifier = 0 then
   SrcInfo.PixelModifier := FPixelModifier else
   SrcInfo.PixelModifier := PixelModifier;
  DstInfo.PixelModifier := Dest.FPixelModifier;
  SrcInfo.TransparentColor := FTransparentColor;
  if Flags and DRAW_ALPHA_BLEND <> 0 then
  begin
   DstInfo.ValueConvert := @AlphaBlend;
   if Flags and DRAW_NO_CONVERT = 0 then
   begin
    SrcInfo.ColorConvert := ValueCvt;
    if (Dest.FImageFormat = ifIndexed) or
       (Dest.FColorFormat.FFlags and CFLG_RGBQUAD = 0) then
    begin
     DstInfo.ColorConvert := GetFromRgbQuadConverter(Dest.FImageFormat);
     SrcInfo.ADstConvert := GetToRgbQuadConverter(Dest.FImageFormat);
    end else
    begin
     DstInfo.ColorConvert := nil;
     SrcInfo.ADstConvert := nil;
    end;
   end else
   begin
    SrcInfo.ColorConvert := nil;
    DstInfo.ColorConvert := nil;
    SrcInfo.ADstConvert := nil;
   end;
  end else
   DstInfo.ValueConvert := ValueCvt;
  if FCompressed then
  begin
   if Flags and DRAW_VALUE_CONVERT <> 0 then
    DstInfo.UseProcs := DstInfo.UseProcs or 2;
   if Flags and DRAW_TRANSP_MASK = 0 then
    DstInfo.UseProcs := DstInfo.UseProcs or 4;
   Decompress(SrcInfo, DstInfo);
  end else
  if Dest.FCompressed then
  begin
   if Flags and DRAW_VALUE_CONVERT <> 0 then
    DstInfo.UseProcs := DstInfo.UseProcs or 2;
   DstInfo.CmprDataPtr := Pointer(Integer(Dest.FImageData) + Height * 4);
   Compress(SrcInfo, DstInfo);
   Dest.FCompressedSize :=
     Integer(DstInfo.CmprDataPtr) - Integer(Dest.FImageData);
  end else
  asm
   sub esp,CONVERTER_CODE_SIZE
   lea eax,[ebp + offset InitInfo]
   mov edx,esp
   call BuildConverter
   mov eax,SrcInfo
   mov edx,DstInfo
   call esp
   add esp,CONVERTER_CODE_SIZE
//   PConvertProgRec(@ImageConvertCode).Init(@InitInfo);
//   PConvertProgRec(@ImageConvertCode).Convert(SrcInfo, DstInfo);
  end;
 finally
  ClearInfo(DstInfo);
  ClearInfo(SrcInfo);
 end;
end;

procedure TBitmapContainer.Draw(Dest: TBitmapContainer; X, Y: Integer;
  Cvt: TBitmapConverter);
var
 Info: PConvertInfo;
 Width, Height, SrcX, SrcY: Integer;
 {SBC, DBC, }I: Integer;
begin
 if Cvt = nil then Exit;
 Width := FWidth;
 Height := FHeight;
 Inc(X, FLeft);
 Inc(Y, FTop);

 if X < 0 then
 begin
  SrcX := -X;
  X := 0;
  Dec(Width, SrcX)
 end else SrcX := 0;

 I := Dest.FWidth - X;
 if I <= 0 then Exit;
 if Width > I then Width := I;

 if Y < 0 then
 begin
  SrcY := -Y;
  Y := 0;
  Dec(Height, SrcY)
 end else SrcY := 0;

 I := Dest.FHeight - Y;
 if I <= 0 then Exit;
 if Height > I then Height := I;

 Info := Cvt.SrcInfo;
 while Info <> nil do
 begin
  LongRec(Info.RowStride).Lo := Width;
  LongRec(Info.RowStride).Hi := FWidthRemainder;
  Info.W := FWidth;
  Info.H := FHeight;
  Info := Info.Next;
 end;
 Info := Cvt.DstInfo;
// if (Info.Next = nil) and
//    (Info.Flags and IMG_COMPRESSED <> 0) then
 if Dest.FCompressed then
 begin
  Inc(Dest.FLeft, X);
  Inc(Dest.FTop, Y);
  Dest.FWidth := Width;
  Dest.FWidthRemainder := 0;
  Dest.FHeight := Height;
  X := 0;
  Y := 0;
  Info.CmprDataPtr := Pointer(Integer(Dest.FImageData) + Height * 4);
 end;
 while Info <> nil do
 begin
  LongRec(Info.RowStride).Lo := Width;
  LongRec(Info.RowStride).Hi := Dest.FWidthRemainder;
  Info.W := Dest.FWidth;
  Info.H := Dest.FHeight;
  Info := Info.Next;
 end;
 with Cvt do
 begin
  InitBitsPositions(SrcInfo);
  InitBitsPositions(DstInfo);
  SrcInfo.TransparentColor := FTransparentColor;
  if DstInfo.UseProcs and 8 <> 0 then // DRAW_X_FLIP for compressed source
  begin
   Inc(SrcX, X);
   X := Dest.FWidth - (X + Width);
  end;
  FillInfoLoop(SrcInfo, FImageData, SrcX, SrcY);
  FillInfoLoop(DstInfo, Dest.FImageData, X, Y);
  SrcInfo.ClrTbl := Self.FColorTable;
  SrcInfo.ClrFmt := Self.FColorFormat;
  DstInfo.ClrTbl := Dest.FColorTable;
  DstInfo.ClrFmt := Dest.FColorFormat;
  SrcInfo.W := Width;
  SrcInfo.H := Height;
  Convert(SrcInfo, DstInfo);
  if Dest.FCompressed then Dest.FCompressedSize :=
    Integer(DstInfo.CmprDataPtr) - Integer(Dest.FImageData);
 end;
end;

procedure TBitmapContainer.FillData(ColorValue: LongWord);
var
 Buf: Pointer;
 Cnt: Integer;
begin
 if FImageData <> nil then
 begin
  if ColorValue > 0 then
  begin
   Cnt := FWidth * FHeight;
   GetMem(Buf, Cnt * 4);
   try
    Fill32(ColorValue, Buf^, Cnt);
    with TBitmapContainer.Create do
    try
     FImageFlags := 32 - 1;
     FImageData := Buf;
     FWidth := Self.FWidth;
     FHeight := Self.FHeight;
     Draw(Self, 0, 0, DRAW_NO_CONVERT);
    finally
     Free;
    end;
   finally
    FreeMem(Buf);
   end;
  end else
   FillChar(FImageData^, ImageSize, 0);
 end;
end;

function TBitmapContainer.GetBitsCountTotal: Integer;
var
 I: Integer;
begin
 Result := (FImageFlags and 31) + 1;
 for I := 0 to Length(CompositeFlags) - 1 do
  Inc(Result, (CompositeFlags[I] and 31) + 1);
// FTotalBits := Result;
end;

function TBitmapContainer.GetFlagsList: TCompositeFlags;
var
 L: Integer;
begin
 L := Length(CompositeFlags);
 SetLength(Result, 1 + L);
 Result[0] := FImageFlags;
 if L > 0 then Move(Pointer(CompositeFlags)^, Result[1], L * 2);
end;

function TBitmapContainer.GetImageSize: Integer;
var
 I: Integer;
begin
 Result := GetImageLineSize(FWidth, FWidthRemainder, FImageFlags);
 for I := 0 to Length(CompositeFlags) - 1 do
  Inc(Result, GetImageLineSize(FWidth, FWidthRemainder, CompositeFlags[I]));
 Result := Result * FHeight;
end;

function TBitmapContainer.GetPaletteCount: Integer;
begin
 Result := 1 shl BitsCount;
end;

function TBitmapContainer.GetPaletteSize: Integer;
begin
 Result := (1 shl BitsCount) * FColorFormat.ColorSize;
end;

function TBitmapContainer.SameAs(Image: TBitmapContainer): Boolean;
var
 L{, BCnt}: Integer;
begin
// BCnt := BitsCount;
 L := Length(CompositeFlags);
 Result := (FWidth = Image.FWidth) and
           (FWidthRemainder = Image.FWidthRemainder) and
           (FHeight = Image.FHeight) and
           (FImageFlags = Image.FImageFlags) and
           (FImageFormat = Image.FImageFormat) and
           (L = Length(Image.CompositeFlags)) and
           ((L = 0) or CompareMem(Pointer(CompositeFlags),
                       Pointer(Image.CompositeFlags), L shl 1)) and
         {  ((FImageData = Image.FImageData) or
            ((FImageData <> nil) and (Image.FImageData <> nil) and
             CompareMem(FImageData, Image.FImageData, ImageSize))) and   }
           ((FColorFormat = Image.FColorFormat) or
        ((FColorFormat <> nil) and FColorFormat.SameAs(Image.FColorFormat))) {and
           ((FColorTable = Image.FColorTable) or
            ((FColorTable <> nil) and (Image.FColorTable <> nil) and
             CompareMem(FColorTable, Image.FColorTable, (1 shl BCnt) *
             FColorFormat.ColorSize)))};
end;

procedure TBitmapContainer.SetFlags(const Flags: array of Word);
var
 Cnt: Integer;
begin
 Cnt := Length(Flags);
 if Cnt > 0 then
 begin
  FImageFlags := Flags[0];
  Dec(Cnt);
  SetLength(CompositeFlags, Cnt);
  if Cnt > 0 then
   Move(Flags[1], Pointer(CompositeFlags)^, Cnt shl 1);
 end else
 begin
  FImageFlags := 0;
  Finalize(CompositeFlags);
 end;
end;

procedure TBitmapContainer.SetFlagsList(const Value: TCompositeFlags);
begin
 SetFlags(Value);
end;

procedure TBitmapContainer.SetInfo(Width, Height: Integer;
          Fmt: TImageFormat; ClrFmt: TColorFormat; Rmndr: Integer);
begin
 if Width >= 0 then
  FWidth := Width;
 if Rmndr >= 0 then
  FWidthRemainder := Rmndr;
 if Height >= 0 then
  FHeight := Height;
 FImageFormat := Fmt;
 if ClrFmt <> nil then
  FColorFormat := ClrFmt;
end;

{ TCustomImage }

function TCustomImage.AddFrame: PFrameInfo;
begin
 New(Result);
 FLast.fiNext := Result;
 FLast := Result;
 if FirstFrame.fiNext = nil then
  FirstFrame.fiNext := Result;
 FillChar(Result^, SizeOf(TFrameInfo), 0);
 Inc(FFramesCount); 
end;

procedure TCustomImage.Clear;
var
 Node, DNode: PFrameInfo;
begin
 if FirstFrame.fiPalette <> nil then
  FreeMem(FirstFrame.fiPalette);
 if FirstFrame.fiImage <> nil then
  FreeMem(FirstFrame.fiImage);
 Node := FirstFrame.fiNext;
 FillChar(FirstFrame, SizeOf(TFrameInfo), 0);
 while Node <> nil do
 begin
  if Node.fiPalette <> nil then
   FreeMem(Node.fiPalette);
  if Node.fiImage <> nil then
   FreeMem(Node.fiImage);
  DNode := Node;
  Node := Node.fiNext;
  Dispose(DNode);
 end;
 FLast := @FirstFrame;
 FFramesCount := 1;
 FCurrent := FLast;
end;

procedure TCustomImage.Compress(Frame: PFrameInfo);
var
 Info, SrcInfo, DstInfo: PConvertInfo;
 I: Integer; Buffer: Pointer;
begin
 if (Frame.fiImage <> nil) and (Frame.fiImgCmprSize <= 0) then
 begin
  SrcInfo := nil;
  DstInfo := nil;
  try
   SrcInfo := AddInfo(nil, FImageFlags, DRAW_TRANSPARENT,
                      FWidth, FWidthRemainder, FWidth, FHeight);
   Info := SrcInfo;
   while SrcInfo.Prev <> nil do SrcInfo := SrcInfo.Prev;
   SrcInfo.UseProcs := 1;

   for I := 0 to Length(CompositeFlags) - 1 do
    Info := AddInfo(Info, CompositeFlags[I], DRAW_TRANSPARENT,
               FWidth, FWidthRemainder, FWidth, FHeight);

   DstInfo := AddInfo(DstInfo, ((BitsCount - 1) and 31) or
    (FImageFlags and not 31), DRAW_COMPRESSED,
      FWidth, 0, FWidth, FHeight);

   DstInfo.UseProcs := 1;

   InitBitsPositions(SrcInfo);
   InitBitsPositions(DstInfo);
   InitProcs(SrcInfo, DstInfo);
   FillInfoLoop(SrcInfo, Frame.fiImage, 0, 0);
   GetMem(Buffer, (ImageSize * 2) + FHeight * 4);
   try
    FillInfoLoop(DstInfo, Buffer, 0, 0);
    SrcInfo.TransparentColor := FTransparentColor;
    DstInfo.CmprDataPtr := Pointer(Integer(Buffer) + FHeight * 4);
    BitmapEx.Compress(SrcInfo, DstInfo);
    Frame.fiImgCmprSize := Integer(DstInfo.CmprDataPtr) - Integer(Buffer);
    FreeMem(Frame.fiImage);    
    ReallocMem(Buffer, Frame.fiImgCmprSize);
    if Frame.fiImage = FImageData then
    begin
     FImageData := Buffer;
     FCompressedSize := Frame.fiImgCmprSize;
     FCompressed := True;
    end;
    Frame.fiImage := Buffer;
   except
    FreeMem(Buffer);
    raise;
   end;
  finally
   ClearInfo(SrcInfo);
   ClearInfo(DstInfo);
  end;
 end
end;

procedure TCustomImage.CompressPalette(Frame: PFrameInfo; SkipColor: LongWord; Cnt: Integer);
var
 Buffer: Pointer;
begin
 if (Frame.fiPalette <> nil) and (Frame.fiPalCmprSize <= 0) then
 begin
  if Cnt <= 0 then
   Cnt := PaletteCount;
  GetMem(Buffer, Cnt * FInternalClrFmt.FColorSize + Cnt);
  try
   Frame.fiPalCmprSize :=
    FInternalClrFmt.Compress(Frame.fiPalette, Buffer, Cnt, SkipColor);
   if Frame.fiPalCmprSize > 0 then
   begin
    FreeMem(Frame.fiPalette);   
    ReallocMem(Buffer, Frame.fiPalCmprSize);
    Frame.fiPalette := Buffer;
   end else
    FreeMem(Buffer);    
  except
   FreeMem(Buffer);
   raise;
  end;
 end;
end;

constructor TCustomImage.Create(FileClass: TImageFileClass);
begin
 FImageFileClass := FileClass;
// FInternalImage := nil;
// FInternalPalette := nil;
 FColorTable := DefaultColorTable;
 FInternalClrFmt := TColorFormat.Create;

 FColorFormat := FInternalClrFmt;
 FLast := @FirstFrame;
 FFramesCount := 1;
end;

procedure TCustomImage.Decompress(Frame: PFrameInfo);
begin
 Frame.fiImgCmprSize := 0;
 //
end;

destructor TCustomImage.Destroy;
begin
 Clear;
 FInternalClrFmt.Free;
end;

procedure TCustomImage.LoadFromFile(const FileName: String);
var
 Stream: TStream;
begin
 Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TCustomImage.LoadFromStream(Stream: TStream);
begin
 with FImageFileClass.Create(Self) do
 try
  LoadFromStream(Stream);
  Self.FResultData := GetResultData;
 finally
  Free;
 end;
end;

procedure TCustomImage.Reallocate(Width, Height: Integer;
  Fmt: TImageFormat; const Flags: array of Word; ClrFmt: TColorFormat;
  ClrTbl: Pointer; Rmndr: Integer);
var
 Temp: TBitmapContainer;
 ImageSize, BCnt, CvtFlags, PaletteSize: Integer;
begin
 Temp := TBitmapContainer.Create;
 try
  if ClrFmt = nil then
   ClrFmt := FColorFormat;
  if Width < 0 then
   Width := FWidth;
  if Rmndr < 0 then
   Rmndr := FWidthRemainder;  
  if Height < 0 then
   Height := FHeight;
  Temp.SetInfo(Width, Height, Fmt, ClrFmt, Rmndr);
  Temp.SetFlags(Flags);
  BCnt := Temp.BitsCount;
  if (BCnt < 1) or ((Fmt <> ifRGB) and (BCnt > 8)) or (BCnt > 32) then
   ImageFormatError;
  ImageSize := Temp.ImageSize;
  if Fmt = ifIndexed then
  begin
   PaletteSize := (1 shl BCnt) * ClrFmt.FColorSize;
   GetMem(Temp.FColorTable, PaletteSize);
   FillChar(Temp.FColorTable^, PaletteSize, 0);
   if ClrTbl <> nil then
    Move(ClrTbl^, Temp.FColorTable^, PaletteSize) else
   if FColorTable <> nil then
    FColorFormat.ConvertTo(ClrFmt, FColorTable, Temp.FColorTable,
                           Min(1 shl BCnt, 1 shl BitsCount)) else
  end;
  if ImageSize > 0 then
  begin
   GetMem(Temp.FImageData, ImageSize);
   try
    FillChar(Temp.FImageData^, ImageSize, 0);
    if FImageData <> nil then
    begin
     if (ClrTbl = nil) and not ((FImageFormat = ifIndexed) and
        (Temp.FImageFormat = ifIndexed) and
        (BitsCount > BCnt)) then
      CvtFlags := DRAW_NO_CONVERT else
      CvtFlags := 0;
     Draw(Temp, -FLeft, -FTop, CvtFlags);
    end;
   except
    if Temp.FColorTable <> nil then FreeMem(Temp.FColorTable);
    if Temp.FImageData  <> nil then FreeMem(Temp.FImageData);
    raise;
   end;
  end;
  FInternalClrFmt.Assign(ClrFmt);
//  if FInternalImage <> nil then FreeMem(FInternalImage);
//  if FInternalPalette <> nil then FreeMem(FInternalPalette);
  Clear;
  FirstFrame.fiImage := Temp.FImageData;
  FirstFrame.fiPalette := Temp.FColorTable;
  FImageData := Temp.FImageData;
  FColorTable := Temp.FColorTable;
  SetInfo(Width, Height, Fmt, FInternalClrFmt, Rmndr);
  SetFlags(Flags);
 finally
  Temp.Free;
 end;
end;

procedure LoadRGB(Stream: TStream; Buf: Pointer; W, H, {WB,} LSZ, BCnt: Integer); stdcall;
var
 Y, X, I{, SeekRest}: Integer;
 Mask: Byte; P: PByte;
begin
// SeekRest := WB - LSZ;
 P := Buf;
 if H < 0 then
 begin
  Inc(P, (-H - 1) * LSZ);
  LSZ := -LSZ;
 end;
 X := Abs(LSZ);
 for Y := 1 to Abs(H) do
 begin
  Stream.ReadBuffer(P^, X);
  if BCnt <= 4 then
  begin
   Inc(P, X - 1);
   Mask := 1 shl BCnt - 1;
   for I := 1 to W and (8 div BCnt - 1) do
   begin
    P^ := P^ and not Mask;
    Mask := Mask shl BCnt;
   end;
   Dec(P, X - 1);
  end;
//  Stream.Seek(SeekRest, soFromCurrent);
  Inc(P, LSZ);
 end;
end;

procedure TCustomImage.SaveToFile(const FileName: String);
var
 Stream: TStream;
begin
 Stream := TFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TCustomImage.SaveToStream(Stream: TStream);
begin
 with FImageFileClass.Create(Self) do
 try
  SaveToStream(Stream);
  FResultData := GetResultData;
 finally
  Free;
 end;
end;

procedure TCustomImage.SetCurrentFrame(Value: PFrameInfo);
begin
 FCurrent := Value;
 if Value <> nil then
 begin
  if Value.fiImage = nil then
  begin
   FImageData := FirstFrame.fiImage;
   FCompressedSize := FirstFrame.fiImgCmprSize;
  end else
  begin
   FImageData := Value.fiImage;
   FCompressedSize := Value.fiImgCmprSize;
  end;
  FColorTable := Value.fiPalette;
  if FColorTable = nil then
   FColorTable := FirstFrame.fiPalette;
  FCompressed := FCompressedSize > 0;
 end;
end;

{ TBitmapFile }

procedure TBitmapFile.LoadFromStream(Stream: TStream);
var
 BF: TBitmapFileHeader;
 V5: TBitmapV5Header;
 V4: TBitmapV4Header absolute V5;
 BI: TBitmapInfoHeader absolute V5;
 BC: TBitmapCoreHeader absolute V5;
 I, LineSize, ImgSize, PalCount: Integer;
 ImageBuffer: Pointer;
 OS2: Boolean;
 ColorFmt: TColorFormat;
 Localpf: packed record RBitMask, GBitMask, BBitMask: LongWord end;
 AColorTable: Pointer;

 procedure LoadRLE4;
 var
  RLE: Pointer;
 begin
  GetMem(RLE, BF.bfSize);
  try
   Stream.ReadBuffer(RLE^, BF.bfSize);
   DecodeRLE4(RLE^, ImageBuffer^, LineSize);
  finally
   FreeMem(RLE);
  end;
 end;

 procedure LoadRLE8;
 var
  RLE: Pointer;
 begin
  GetMem(RLE, BF.bfSize);
  try
   Stream.ReadBuffer(RLE^, BF.bfSize);
   DecodeRLE8(RLE^, ImageBuffer^, LineSize);
  finally
   FreeMem(RLE);
  end;
 end;

begin
 with FImageInfo do
 begin
  { Read file header }
  I := Stream.Read(BF, SizeOf(TBitmapFileHeader));
  if (I <> SizeOf(TBitmapFileHeader)) or
     (BF.bfType <> Ord('B') + Ord('M') shl 8) then
   BitmapFileFormatError;
  Self.FResultData := BF.bfSize; 
  Dec(BF.bfSize, I);

  {  Read header size }
  I := Stream.Read(BI.biSize, 4);
  Dec(BF.bfSize, BI.biSize);

  if I > 0 then
  begin
   if I <> 4 then
    BitmapFileFormatError;

   { Check DIB type }
   OS2 := False;
   case BI.biSize of
    SizeOf(TBitmapCoreHeader):
    begin
     { OS/2 type }
     Stream.ReadBuffer(Addr(BC.bcWidth)^, SizeOf(TBitmapCoreHeader) - 4);
     BI.biClrUsed := 0;
     BI.biCompression := BI_RGB;
     BI.biBitCount := BC.bcBitCount;
     BI.biPlanes := BC.bcPlanes;
     BI.biHeight := BC.bcHeight;
     BI.biWidth := BC.bcWidth;
     OS2 := True;
    end;
    SizeOf(TBitmapInfoHeader):
    begin
     { Windows type }
     Stream.ReadBuffer(Addr(BI.biWidth)^, SizeOf(TBitmapInfoHeader) - 4);
     if BI.biCompression = BI_BITFIELDS then
     begin
      Stream.ReadBuffer(Localpf, SizeOf(Localpf));
      Dec(BF.bfSize, SizeOf(Localpf));
      V5.bV5RedMask := Localpf.RBitMask;
      V5.bV5GreenMask := Localpf.GBitMask;
      V5.bV5BlueMask := Localpf.BBitMask;
      V5.bV5AlphaMask := 0;
     end;
    end;
    SizeOf(TBitmapV4Header) - 52:
    begin
     { Version 3 type ??? }    
     Stream.ReadBuffer(Addr(V4.bV4Width)^, (SizeOf(TBitmapV4Header) - 52) - 4);
    end;
    SizeOf(TBitmapV4Header):
    begin
     { Version 4 type }
     Stream.ReadBuffer(Addr(V4.bV4Width)^, SizeOf(TBitmapV4Header) - 4);
    end;
    SizeOf(TBitmapV5Header):
    begin
     { Version 5 type }
     Stream.ReadBuffer(Addr(V5.bV5Width)^, SizeOf(TBitmapV5Header) - 4);
    end;
    else BitmapFileFormatError;
   end;
   if (BI.biWidth < 0) or (BI.biPlanes <> 1) or
      not (BI.biBitCount in [1, 4, 8, 16, 24, 32]) then
    BitmapFileFormatError;
   AColorTable := nil;
   ImageBuffer := nil;
   if (BI.biSize >= SizeOf(TBitmapV4Header)) or
      (BI.biBitCount = 32) then
    ColorFmt := TColorFormat.Create(CF_BGRA) else
    ColorFmt := TColorFormat.Create(CF_BGRZ);
   try
    if BI.biCompression = BI_BITFIELDS then
    begin
     if BI.biBitCount <= 8 then
      BitmapFileFormatError;
     ColorFmt.SetFormat(V5.bV5RedMask, V5.bV5GreenMask, V5.bV5BlueMask,
                                       V5.bV5AlphaMask);
     if BI.biBitCount = 32 then
     begin
      ColorFmt.FColorSize := 4;
      ColorFmt.FUsedBits := 32;
     end;
    end else
    if BI.biBitCount = 16 then
    begin
     ColorFmt.SetFormat('BGR555');
    end else
    if BI.biBitCount = 24 then
     ColorFmt.SetFormat('BGR888');
    try
     { Read palette data }
     PalCount := BI.biClrUsed;
     if (PalCount = 0) and (BI.biBitCount <= 8) then
      PalCount := 1 shl BI.biBitCount;

     if OS2 then
     begin
      { OS/2 type }
      ColorFmt.SetFormat('BGR888');
      PalCount := SizeOf(TRGBTriple) * PalCount;
      if BI.biBitCount > 8 then
       ImgSize := PalCount else
       ImgSize := Max((1 shl BI.biBitCount) * 3, PalCount);
     end else
     begin
      { Windows type }
      PalCount := SizeOf(TRGBQuad) * PalCount;
      if BI.biBitCount > 8 then
       ImgSize := PalCount else
       ImgSize := Max((1 shl BI.biBitCount) * 4, PalCount);
     end;
     if ImgSize > 0 then
     begin
      GetMem(AColorTable, ImgSize);
      FillChar(AColorTable^, ImgSize, 0);
      Stream.ReadBuffer(AColorTable^, PalCount);
      Dec(BF.bfSize, PalCount);
     end;

     if BI.biCompression in [BI_BITFIELDS, BI_RGB] then
     begin
      LineSize := GetWidthBytes(BI.biWidth, BI.biBitCount);
      I := LineSize - BmpImg.GetLineSize(BI.biWidth, BI.biBitCount);
     end else
     begin
      LineSize := BmpImg.GetLineSize(BI.biWidth, BI.biBitCount);
      I := 0;
     end;
     ImgSize := LineSize * Abs(BI.biHeight);
     GetMem(ImageBuffer, ImgSize);
     case BI.biCompression of
      BI_BITFIELDS,
      BI_RGB:  LoadRGB(Stream, ImageBuffer, BI.biWidth, -BI.biHeight,
            LineSize, BI.biBitCount);
      BI_RLE4: LoadRLE4;
      BI_RLE8: LoadRLE8;
      else BitmapFileFormatError;
     end;
    except
     if ImageBuffer <> nil then FreeMem(ImageBuffer);
     if AColorTable <> nil then FreeMem(AColorTable);
     raise;
    end;
    FWidth := BI.biWidth;
    FLeft := 0;
    FTop := 0;
    FWidthRemainder := I;
    FHeight := Abs(BI.biHeight);
    if BI.biBitCount <= 8 then
     FImageFormat := ifIndexed else
     FImageFormat := ifRGB;
    Finalize(CompositeFlags);
    FImageFlags := BI.biBitCount - 1;
    FInternalClrFmt.Assign(ColorFmt);
    Clear;
    FirstFrame.fiImage := ImageBuffer;
    FirstFrame.fiPalette := AColorTable;
    FImageData := ImageBuffer;
    FColorTable := AColorTable;
    FColorFormat := FInternalClrFmt;
   finally
    ColorFmt.Free;
   end;
  end else
  begin
   FWidth := 0;
   FHeight := 0;
   FImageData := nil;
   Clear;
  end;
 end;
end;

procedure TBitmapFile.SaveToStream(Stream: TStream);
var
 HeaderV4: TBitmapHeaderV4;
 Header: TBitmapHeader absolute HeaderV4;
 Buffer: Pointer;
 WidthBytes, Size, CvtFlags: Integer;
 BCnt, Y: Integer;
 Color: PCardinal; V4: Boolean;
 DestClFmt: TColorFormat;
 SaveHolder: TBitmapContainer;
 FRM: PFrameInfo;
begin
 with FImageInfo do
 begin
  BCnt := BitsCount;
  case BCnt of
   1:    Y := 1;
   2..4: Y := 4;
   5..8: Y := 8;
   else  Y := BCnt;
  end;
  if Y < 1 then ImageFormatError;
  DestClFmt := nil;
  V4 := False;
  if (FImageFormat = ifRGB) and
     not (FColorFormat.FFlags and 3 <> CFLG_GRAYSCALE) and
     ((BCnt > 8) or (FColorFormat.FFlags and CFLG_ALPHA <> 0)) then
  with FColorFormat do
  begin
   if FFlags and CFLG_ALPHA = 0 then
   begin
    case FColorSize of
     1:
     begin
      DestClFmt := TColorFormat.Create('BGR555');
      Y := 16;
     end;
     2:
     begin
      DestClFmt := TColorFormat.Create('BGR555');
      if not FColorFormat.SameAs(DestClFmt) then
      begin
       DestClFmt.AssignMask(FColorFormat);
       V4 := True;
      end;
      Y := 16;
     end;
     3:
     begin
      DestClFmt := TColorFormat.Create('BGRZ8888');
      if not FColorFormat.SameAs(DestClFmt) then
      begin
       DestClFmt.AssignMask(FColorFormat);
       V4 := True;
      end;
      DestClFmt.FColorSize := 4;
      DestClFmt.FUsedBits := 32;
      Y := 32;
     end;
     4:
     begin
      DestClFmt := TColorFormat.Create('BGRZ8888');
      if not FColorFormat.SameAs(DestClFmt) then
      begin
       DestClFmt.AssignMask(FColorFormat);
       V4 := True;
      end;
      Y := 32;
     end;
     else ImageFormatError;
    end;
   end else
   begin
    DestClFmt := TColorFormat.Create('BGRA8888');
    if (Y >= 16) and not FColorFormat.SameAs(DestClFmt) then
     DestClFmt.AssignMask(FColorFormat);
    V4 := True;
    Y := FColorSize * 8;
   end;
  end else
  if FColorFormat.FFlags and CFLG_ALPHA <> 0 then
  begin
   DestClFmt := TColorFormat.Create(CF_BGRA);
   V4 := True;
  end;
  try
   if V4 then
    FillBitmapHeaderV4(@HeaderV4, Width, Height, Y) else
    FillBitmapHeader(@Header, Width, Height, Y);
   if DestClFmt <> nil then
   begin
    HeaderV4.bhInfoHeader.bV4RedMask := DestClFmt.FRedMask;
    HeaderV4.bhInfoHeader.bV4GreenMask := DestClFmt.FGreenMask;
    HeaderV4.bhInfoHeader.bV4BlueMask := DestClFmt.FBlueMask;
    HeaderV4.bhInfoHeader.bV4AlphaMask := DestClFmt.FAlphaMask;
   end;
   Stream.WriteBuffer(HeaderV4, HeaderV4.bhInfoHeader.bV4Size + SizeOf(TBitmapFileHeader));
   with Header.bhInfoHeader do
   begin
    if biBitCount <= 8 then
    begin
     biClrUsed := 1 shl BCnt;
     biClrImportant := biClrUsed;
     Size := biClrUsed * 4;
     if FImageFormat = ifRGB then
     begin
      GetMem(Buffer, Size);
      try
       Color := Buffer;
       for Y := 0 to biClrUsed - 1 do
       begin
        Color^ := FColorFormat.ValueToRGBQuad(Y);
        Inc(Color);
       end;
       Stream.WriteBuffer(Buffer^, Size);
      finally
       FreeMem(Buffer);
      end;
     end else
     begin
      if (FCurrent <> nil) and (FCurrent.fiPalCmprSize > 0) and
         (FCurrent.fiPalette <> nil) then
      begin
       GetMem(Buffer, Size);
       try
        if FCurrent <> @FirstFrame then
         FColorFormat.ConvertToRGBQuad(FirstFrame.fiPalette, Buffer, biClrUsed) else
         FillChar(Buffer^, Size, 0);
        FColorFormat.DecompressToRGBQuad(FCurrent.fiPalette, Buffer);
        Stream.WriteBuffer(Buffer^, Size);
       finally
        FreeMem(Buffer);
       end;
      end else
      if FColorFormat.FFlags and 3 <> CFLG_RGBQUAD then
      begin
       GetMem(Buffer, Size);
       try
        FColorFormat.ConvertToRGBQuad(FColorTable, Buffer, biClrUsed);
        Stream.WriteBuffer(Buffer^, Size);
       finally
        FreeMem(Buffer);
       end;
      end else
       Stream.WriteBuffer(FColorTable^, Size);
     end;
    end;
    GetMem(Buffer, biSizeImage);
    try
     SaveHolder := TBitmapContainer.Create;
     try
      SaveHolder.FImageData := Buffer;
      WidthBytes := GetWidthBytes(biWidth, biBitCount);
      Size := BmpImg.GetLineSize(biWidth, biBitCount);
      SaveHolder.FImageFlags := (biBitCount - 1) or IMG_Y_FLIP;
      SaveHolder.FWidth := biWidth;
      SaveHolder.FWidthRemainder := WidthBytes - Size;
      SaveHolder.FHeight := biHeight;
      if biBitCount <= 8 then
      begin
       CvtFlags := DRAW_NO_CONVERT;
       SaveHolder.FImageFormat := ifIndexed;
       SaveHolder.FColorFormat := FColorFormat;
      end else
      begin
       CvtFlags := 0;
       SaveHolder.FImageFormat := ifRGB;
       SaveHolder.FColorFormat := DestClFmt;
      end;
      if not SaveHolder.SameAs(FImageInfo) then
      begin
       if (FCurrent <> nil) and (FCurrent <> @FirstFrame) and
          (FImageData <> nil) and
          (FImageData = FCurrent.fiImage) then
       begin
        FRM := FCurrent;
        SetCurrentFrame(@FirstFrame); // Draw key frame
        Draw(SaveHolder, -FLeft, -FTop, CvtFlags);
        SetCurrentFrame(FRM);
        CvtFlags := CvtFlags or DRAW_TRANSPARENT;
       end;
       Draw(SaveHolder, -FLeft, -FTop, CvtFlags);
       Stream.WriteBuffer(Buffer^, WidthBytes * biHeight);
      end else
       Stream.WriteBuffer(FImageData^, WidthBytes * biHeight);
     finally
      SaveHolder.Free;
     end;
    finally
     FreeMem(Buffer);
    end;
   end;
  finally
   DestClFmt.Free;
  end;
 end;
end;

{ TZSoftPCX }

procedure TZSoftPCX.LoadFromStream(Stream: TStream);
var
 Header: TZSoftPCX_Header;
 I, BitCount, Cnt, ImgSize, PlaneLineSize, LineSize, W, H: Integer;
 ImageBuffer: Pointer; ImFlags: Word;
 ColorFmt: TColorFormat;
 AColorTable: Pointer; ImFmt: TImageFormat;
 B: Byte; PB: PByte; Skip: Integer;
 S: AnsiString;
begin
 with FImageInfo do
 begin
  I := Stream.Read(Header, SizeOf(TZSoftPCX_Header));
  with Header do
  begin
   BitCount := pcxPlaneCount * pcxBitCount;
   case pcxBitCount of
    1..2: if BitCount > 8 then PCX_FileFormatError;
    else
    if pcxBitCount <= 8 then
    case pcxPlaneCount of
     1: ;
     2: BitCount := 16;
     3: BitCount := 24;
     4: BitCount := 32;
     else PCX_FileFormatError;
    end else PCX_FileFormatError;
   end;
   if (I <> SizeOf(TZSoftPCX_Header)) or
      (pcxFormatID <> $0A) or not (pcxVersion in [0..5]) or
     ((pcxVersion <> 5) and not (BitCount in [1..4])) or
      not (BitCount in [1..32]) then
    PCX_FileFormatError;
   W := (pcxRight + 1) - pcxLeft;
   H := (pcxBottom + 1) - pcxTop;
   AColorTable := nil;
   ImageBuffer := nil;
   ColorFmt := TColorFormat.Create('RGB888');
   try
    if pcxPlaneCount > 1 then
    begin
     if W < 0 then
      ImFlags := Planar(BitCount, -pcxBitCount, pfVector, True) else
      ImFlags := Planar(BitCount, pcxBitCount, pfVector, False);
    end else
    if BitCount < 5 then
    begin
     if W < 0 then
      ImFlags := Linear(-BitCount, True) else
      ImFlags := BitCount - 1;
    end else
     ImFlags := Linear(BitCount, W < 0);
    PlaneLineSize := BmpImg.GetLineSize(Abs(W), pcxBitCount);
    if pcxWidthBytes < PlaneLineSize then
     PCX_FileFormatError;
    Skip := pcxWidthBytes - PlaneLineSize;     
    LineSize := GetImageLineSize(Abs(W), Skip, ImFlags);
    ImgSize := LineSize * Abs(H);
    try
     GetMem(ImageBuffer, ImgSize);
     if pcxCompressed then
     begin
      PB := ImageBuffer;
      I := 0;
      while ImgSize > 0 do
      begin
       Stream.Read(B, 1);
       if B >= $C0 then
       begin
        Cnt := B and $3F;
        Stream.Read(B, 1);
       end else
        Cnt := 1;
       Inc(I, Cnt);
       Dec(ImgSize, Cnt);
       if (ImgSize < 0) or (I > pcxWidthBytes) then
        PCX_FileFormatError;
       if I = pcxWidthBytes then I := 0;

       FillChar(PB^, Cnt, B);
       Inc(PB, Cnt);
      end;
     end else
      LoadRGB(Stream, ImageBuffer, Abs(W), H,
              pcxWidthBytes, BitCount);
     if BitCount <= 8 then
     begin
      Cnt := (1 shl BitCount) * SizeOf(TRGBTriple);
      GetMem(AColorTable, Cnt);
      FillChar(AColorTable^, Cnt, 0);
      if pcxPaletteInfo = 2 then
      begin
       ImFmt := ifRGB;
       ColorFmt.SetFormat(STR_GRAY + AnsiChar(BitCount + Byte('0')));
      end else
      begin
       ImFmt := ifIndexed;
       Move(pcxEgaPalette, AColorTable^, Min(16 * 3, Cnt));
       if pcxVersion = 5 then
       begin
        if Stream.Read(B, 1) = 1 then
        begin
         case B of
          $0C: ;
          $0A: ColorFmt.SetFormat(CF_MCGA);
          else PCX_FileFormatError;
         end;
         if Stream.Read(AColorTable^, Cnt) <> Cnt then
          PCX_FileFormatError;
        end;
       end;
      end;
     end else
     begin
      ImFmt := ifRGB;
      case pcxPlaneCount of
       2:
       case pcxBitCount of
        3..5:
        begin
         S := 'RGB555';
         B := pcxBitCount + Byte('0');
         for I := 4 to 6 do
          S[I] := AnsiChar(B);
         ColorFmt.SetFormat(S);
        end;
        6: ColorFmt.SetFormat('RGB565');
        else PCX_FileFormatError;
       end;
       3:
       if pcxBitCount < 8 then // default is RGB888
       begin
        S := 'RZGZB62626';
        B := pcxBitCount + Byte('0');
        for I := 0 to 2 do
         S[I shl 1 + 6] := AnsiChar(B);
        B := (8 - pcxBitCount) + Byte('0');
        for I := 0 to 1 do
         S[I shl 1 + 7] := AnsiChar(B);
        ColorFmt.SetFormat(S);
       end;
       4:
       if pcxBitCount = 8 then ColorFmt.SetFormat('RGBA8888') else
       begin
        S := 'RZGZBZA6262626';
        B := pcxBitCount + Byte('0');
        for I := 0 to 3 do
         S[I shl 1 + 8] := AnsiChar(B);
        B := (8 - pcxBitCount) + Byte('0');
        for I := 0 to 2 do
         S[I shl 1 + 9] := AnsiChar(B);
        ColorFmt.SetFormat(S);
       end;
      end;
     end;
    except
     if ImageBuffer <> nil then FreeMem(ImageBuffer);
     if AColorTable <> nil then FreeMem(AColorTable);
     raise;
    end;
    FLeft := Min(pcxLeft, pcxRight);
    FTop := Min(pcxTop, pcxBottom);
    FWidth := Abs(W);
    FWidthRemainder := Skip;
    FHeight := Abs(H);
    FImageFormat := ImFmt;
    Finalize(CompositeFlags);
    FImageFlags := ImFlags;
    FInternalClrFmt.Assign(ColorFmt);
    Clear;
    FirstFrame.fiImage := ImageBuffer;
    FirstFrame.fiPalette := AColorTable;
    FImageData := ImageBuffer;
    FColorTable := AColorTable;
    FColorFormat := FInternalClrFmt;
   finally
    ColorFmt.Free;
   end;
  end;
 end;
end;

procedure TZSoftPCX.SaveToStream(Stream: TStream);
begin
//

end;

{ TCustomImageFile }

constructor TCustomImageFile.Create(AImageData: TCustomImage);
begin
 FImageInfo := AImageData;
end;

function TCustomImageFile.GetResultData: Cardinal;
begin
 Result := FResultData;
end;

{ TBitmapConverter }

constructor TBitmapConverter.Create(const SrcFlags, DstFlags: array of Word;
        DrawFlags: Integer; ConvertFrom, ConvertTo: Pointer);
begin
 Reset(SrcFlags, DstFlags, DrawFlags, ConvertFrom, ConvertTo);
end;

constructor TBitmapConverter.Create;
begin
 // do nothing
end;

destructor TBitmapConverter.Destroy;
begin
 if FProgramSize > 0 then
  FreeMem(@FConvert);
 ClearInfo(FDstInfo);
 ClearInfo(FSrcInfo);
 inherited;
end;

procedure TBitmapConverter.Reset(const SrcFlags, DstFlags: array of Word;
  DrawFlags: Integer; ConvertFrom, ConvertTo: Pointer);
var
 InitInfo: TConvertInitInfo;
 Info: PConvertInfo;
 Len, I, DestFlags: Integer;
 CodeBuf: array[0..CONVERTER_CODE_SIZE - 1] of Byte;
begin
 if FProgramSize > 0 then
  FreeMem(@FConvert);
 ClearInfo(FDstInfo);
 ClearInfo(FSrcInfo);
 LongRec(DrawFlags).Bytes[0] := LongRec(DrawFlags).Bytes[0] and $FE;
 if Length(SrcFlags) > 0 then
  InitInfo.SrcFlags := SrcFlags[0] else
  InitInfo.SrcFlags := 0;
 if Length(DstFlags) > 0 then
  InitInfo.DstFlags := DstFlags[0] else
  InitInfo.DstFlags := 0;
 InitInfo.ConvertFrom := ConvertFrom;
 InitInfo.ConvertTo := ConvertTo; 

 if (Length(SrcFlags) > 1) or
    (InitInfo.SrcFlags and IMG_PLANAR <> 0) then
  DrawFlags := DrawFlags or DRAW_SRC_COMPOSITE;
 if (Length(DstFlags) > 1) or
    (InitInfo.DstFlags and IMG_PLANAR <> 0) then
  DrawFlags := DrawFlags or DRAW_DST_COMPOSITE;

 if InitInfo.SrcFlags and IMG_COMPRESSED <> 0 then
 begin
  if DrawFlags and DRAW_X_FLIP <> 0 then
  begin
   DestFlags := DRAW_X_FLIP;
   DrawFlags := DrawFlags xor DRAW_X_FLIP;
  end else
   DestFlags := 0;;
  DrawFlags := DrawFlags or DRAW_COMPRESSED;
  Len := 0;
  for I := 0 to Length(SrcFlags) - 1 do
   Inc(Len, SrcFlags[I] and 31 + 1);
  InitInfo.SrcFlags := (InitInfo.SrcFlags and not 31) or ((Len - 1) and 31);
 end else
 if InitInfo.DstFlags and IMG_COMPRESSED <> 0 then
 begin
  DestFlags := DRAW_COMPRESSED;
  Len := 0;
  for I := 0 to Length(DstFlags) - 1 do
   Inc(Len, DstFlags[I] and 31 + 1);
  InitInfo.DstFlags := (InitInfo.DstFlags and not 31) or ((Len - 1) and 31);
 end else
  DestFlags := 0;

 FSrcInfo := AddInfo(nil, InitInfo.SrcFlags, DrawFlags, 0, 0, 0, 0);
 InitInfo.SrcFlags := FSrcInfo.Flags;
 Info := FSrcInfo;
 while FSrcInfo.Prev <> nil do FSrcInfo := FSrcInfo.Prev;
 FSrcInfo.PixelModifier := 0;
 FSrcInfo.UseProcs :=
      Ord((DrawFlags and (DRAW_SRC_PROC or DRAW_SRC_COMPOSITE) <> 0) or
      ((DrawFlags or DestFlags) and DRAW_COMPRESSED <> 0));
 if InitInfo.SrcFlags and IMG_COMPRESSED = 0 then      
 for I := 1 to Length(SrcFlags) - 1 do
  Info := AddInfo(Info, SrcFlags[I], DrawFlags, 0, 0, 0, 0);

 FDstInfo := AddInfo(nil, InitInfo.DstFlags, DestFlags, 0, 0, 0, 0);
 FDstInfo.PixelModifier := 0;
 Info := FDstInfo;
 while FDstInfo.Prev <> nil do FDstInfo := FDstInfo.Prev;
 FDstInfo.UseProcs :=
  Ord((DrawFlags and (DRAW_ALPHA_BLEND or DRAW_DST_PROC or DRAW_DST_COMPOSITE) <> 0) or
  ((DrawFlags or DestFlags) and DRAW_COMPRESSED <> 0));
 if InitInfo.DstFlags and IMG_COMPRESSED = 0 then
 for I := 1 to Length(DstFlags) - 1 do
  Info := AddInfo(Info, DstFlags[I], DestFlags, 0, 0, 0, 0);

 if DrawFlags and DRAW_ALPHA_BLEND <> 0 then
 begin
  DrawFlags := DrawFlags or DRAW_DST_PROC or DRAW_VALUE_CONVERT;
  if DrawFlags and DRAW_NO_CONVERT = 0 then
  begin
   FSrcInfo.ColorConvert := GetToRgbQuadConverter
   (
    TImageFormat((DrawFlags shr SRC_IMGFMT_SHIFT) and 3)
   );
   FDstInfo.ColorConvert := GetFromRgbQuadConverter
   (
    TImageFormat((DrawFlags shr DST_IMGFMT_SHIFT) and 3)
   );
   FSrcInfo.ADstConvert := GetToRgbQuadConverter
   (
    TImageFormat((DrawFlags shr DST_IMGFMT_SHIFT) and 3)
   )
  end else
  begin
   FSrcInfo.ColorConvert := nil;
   FDstInfo.ColorConvert := nil;
   FSrcInfo.ADstConvert := nil;
  end;
  FDstInfo.ValueConvert := @AlphaBlend;
 end else
  FDstInfo.ValueConvert := GetValueConverter
  (
   TImageFormat((DrawFlags shr SRC_IMGFMT_SHIFT) and 3),
   TImageFormat((DrawFlags shr DST_IMGFMT_SHIFT) and 3)
  );

 InitProcs(FSrcInfo, FDstInfo);
 if (FSrcInfo.Flags and IMG_COMPRESSED <> 0) then
 begin
  if DrawFlags and DRAW_VALUE_CONVERT <> 0 then
   FDstInfo.UseProcs := FDstInfo.UseProcs or 2;
  if DrawFlags and DRAW_TRANSPARENT = 0 then
   FDstInfo.UseProcs := FDstInfo.UseProcs or 4;
  if DestFlags <> 0 then
   FDstInfo.UseProcs := FDstInfo.UseProcs or 8; // DRAW_X_FLIP
  @FConvert := @Decompress;
  FProgramSize := 0;
 end else
 if(FDstInfo.Flags and IMG_COMPRESSED <> 0) then
 begin
  if DrawFlags and DRAW_VALUE_CONVERT <> 0 then
   FDstInfo.UseProcs := FDstInfo.UseProcs or 2;
  @FConvert := @Compress;
  FProgramSize := 0;
 end else
 begin
  InitInfo.DrawFlags := DrawFlags;

  //Len := PConvertProgRec(@ImageConvertCode).Init(@InitInfo);
  Len := BuildConverter(@InitInfo, @CodeBuf);
  FProgramSize := Len;
  if Len > 0 then
  begin
   GetMem(@FConvert, Len);
//   Move(Addr(PConvertProgRec(@ImageConvertCode).Convert)^, Pointer(@FConvert)^, Len);
   Move(CodeBuf, Pointer(@FConvert)^, Len);
  end;
 end;
end;

{ TBitmapExFile }

procedure TBitmapExFile.LoadFromStream(Stream: TStream);
var
 Header: TBitmapExFileHeader;
 Frame: TFrameInfo;
 ImgSize, BCnt: Integer;

 function LoadFrame: Integer;
 var
  DataHeader: TMilaDataHeader;
  FrameHeader: TMilaFrameHeader;
  ImSize, Cnt, I: Integer;
 begin
  Result := -1;
  Stream.ReadBuffer(DataHeader, SizeOf(TMilaDataHeader));
  if LongWord(DataHeader.dhHeaderID) = MILA_FRAME then
  begin
   if DataHeader.dhHeaderSize <> SizeOf(TMilaFrameHeader) then Exit;
   Stream.ReadBuffer(FrameHeader, SizeOf(TMilaFrameHeader));
   Frame.fiDelay := FrameHeader.fhDelay;
   Frame.fiPalette := nil;
   Frame.fiPalCmprSize := FrameHeader.fhPaletteSize;
   Frame.fiImage := nil;
   Frame.fiImgCmprSize := FrameHeader.fhImageSize;
   if (Header.exImageFormat = ifIndexed) and
      (FrameHeader.fhPaletteSize >= 0) then
   begin
    if FrameHeader.fhPaletteSize > 0 then
    begin
     Cnt := FrameHeader.fhPaletteSize;
     GetMem(Frame.fiPalette, Cnt);
    end else
    begin
     if Header.exColorsCount = 0 then
      Cnt := 1 shl BCnt else
      Cnt := Min(256, Header.exColorsCount);
     Cnt := Cnt * Header.exColorFormat.colSize;
     I := (1 shl BCnt) * Header.exColorFormat.colSize;
     GetMem(Frame.fiPalette, I);
     if I > Cnt then FillChar(Frame.fiPalette^, I, 0);
    end;
    Stream.ReadBuffer(Frame.fiPalette^, Cnt);
   end;
   if Frame.fiImgCmprSize >= 0 then
   begin
    if Frame.fiImgCmprSize > 0 then
     ImSize := Frame.fiImgCmprSize else
     ImSize := ImgSize;
    GetMem(Frame.fiImage, ImSize);
    Stream.ReadBuffer(Frame.fiImage^, ImSize);
   end;
   Result := 1;
  end else if LongWord(DataHeader.dhHeaderID) = MILA_END then
   Result := 0;
 end;
var
 I, Cnt: Integer;
 ImageBuffer, AColorTable: Pointer;
 ACompositeFlags: array of Word;
begin
 Stream.ReadBuffer(Header, SizeOf(TBitmapExFileHeader));
 if (LongWord(Header.exMagic) <> BMPEX_MAGIC) or
    (Header.exHeaderSize <> SizeOf(TBitmapExFileHeader)) then
  FileFormatError;
 SetLength(ACompositeFlags, Header.exCompositeCount);
 Stream.ReadBuffer(Pointer(ACompositeFlags)^, Header.exCompositeCount * 2);
 ImgSize := GetImageLineSize(Header.exWidth, Header.exWidthRemainder, Header.exImageFlags);
 BCnt := (Header.exImageFlags and 31) + 1;
 for I := 0 to Length(ACompositeFlags) - 1 do
 begin
  Inc(ImgSize, GetImageLineSize(Header.exWidth, Header.exWidthRemainder, ACompositeFlags[I]));
  Inc(BCnt, (ACompositeFlags[I] and 31) + 1);
 end;
 if BCnt > 32 then
  FileFormatError;
 if Header.exExtraFlags and 2 <> 0 then
 begin
  ImgSize := ImgSize * Header.exHeight;
  if ImgSize <> Header.exImageSize then
   FileFormatError;
  with FImageInfo do
  begin
   Clear;
   FInternalClrFmt.SetFormat(Header.exColorFormat.colRedMask,
                              Header.exColorFormat.colGreenMask,
                              Header.exColorFormat.colBlueMask,
                              Header.exColorFormat.colAlphaMask);
   FInternalClrFmt.Flags := Header.exColorFormat.colFlags;
   FInternalClrFmt.ColorSize := Header.exColorFormat.colSize;
   FInternalClrFmt.UsedBits := Header.exColorFormat.colBits;
   FImageData := nil;
   FColorTable := nil;
   FColorFormat := FInternalClrFmt;
   FWidth := Header.exWidth;
   FWidthRemainder := Header.exWidthRemainder;
   FHeight := Header.exHeight;
   FImageFlags := Header.exImageFlags;
   Finalize(CompositeFlags);
   if Header.exCompositeCount > 0 then
   begin
    SetLength(CompositeFlags, Header.exCompositeCount);
    Move(ACompositeFlags[0], CompositeFlags[0], Header.exCompositeCount * 2);
   end;
   FImageFormat := Header.exImageFormat;
   FLeft := Header.exLeft;
   FTop := Header.exTop;
   FTransparentColor := Header.exTransparentColor;
   FCompressedSize := Header.exImageSize;
//   FTotalBits := BCnt;
  end;
  Frame.fiImage := nil;
  Frame.fiPalette := nil;
  Frame.fiNext := nil;
  try
   case LoadFrame of
    0: Exit;
    1:
    begin
     Move(Frame, FImageInfo.FirstFrame, SizeOf(TFrameInfo));
     FImageInfo.CurrentFrame := Addr(FImageInfo.FirstFrame);
    end;
    else FileFormatError;
   end;
   repeat
    case LoadFrame of
     0: Exit;
     1: Move(Frame, FImageInfo.AddFrame^, SizeOf(TFrameInfo));
     else FileFormatError;
    end; 
   until False;
  except
   if Frame.fiImage <> nil then FreeMem(Frame.fiImage);
   if Frame.fiPalette <> nil then FreeMem(Frame.fiPalette);
   raise;
  end;
 end else
 begin
  if Header.exExtraFlags and 1 <> 0 then
   ImgSize := Header.exImageSize else
   ImgSize := ImgSize * Header.exHeight;   
  ImageBuffer := nil;
  AColorTable := nil;
  try
   if Header.exImageFormat = ifIndexed then
   begin
    if Header.exColorsCount = 0 then
     Cnt := 1 shl BCnt else
     Cnt := Min(256, Header.exColorsCount);
    Cnt := Cnt * Header.exColorFormat.colSize;
    I := (1 shl BCnt) * Header.exColorFormat.colSize;
    GetMem(AColorTable, I);
    if I > Cnt then FillChar(AColorTable^, I, 0);
    Stream.ReadBuffer(AColorTable^, Cnt);
   end;
   GetMem(ImageBuffer, Max(Header.exImageSize, ImgSize));
   Stream.ReadBuffer(ImageBuffer^, Header.exImageSize);
   with FImageInfo do
   begin
    FInternalClrFmt.SetFormat(Header.exColorFormat.colRedMask,
                              Header.exColorFormat.colGreenMask,
                              Header.exColorFormat.colBlueMask,
                              Header.exColorFormat.colAlphaMask);
    FInternalClrFmt.Flags := Header.exColorFormat.colFlags;
    FInternalClrFmt.ColorSize := Header.exColorFormat.colSize;
    FInternalClrFmt.UsedBits := Header.exColorFormat.colBits;
    FCompressed := Header.exExtraFlags and 1 <> 0;
    Clear;
    FirstFrame.fiImage := ImageBuffer;
    FirstFrame.fiPalette := AColorTable;
    if FCompressed then
     FirstFrame.fiImgCmprSize := Header.exImageSize;
    FImageData := ImageBuffer;
    FColorTable := AColorTable;
    FColorFormat := FInternalClrFmt;
    FWidth := Header.exWidth;
    FWidthRemainder := Header.exWidthRemainder;
    FHeight := Header.exHeight;
    FImageFlags := Header.exImageFlags;
    Finalize(CompositeFlags);
    if Header.exCompositeCount > 0 then
    begin
     SetLength(CompositeFlags, Header.exCompositeCount);
     Move(ACompositeFlags[0], CompositeFlags[0], Header.exCompositeCount * 2);
    end;
    FImageFormat := Header.exImageFormat;
    FLeft := Header.exLeft;
    FTop := Header.exTop;
    FTransparentColor := Header.exTransparentColor;
    FCompressedSize := Header.exImageSize;
//    FTotalBits := BCnt;
   end;
  except
   if ImageBuffer <> nil then FreeMem(ImageBuffer);
   if AColorTable <> nil then FreeMem(AColorTable);
   raise;
  end;
 end;
end;

procedure TBitmapExFile.SaveToStream(Stream: TStream);
var
 Header: TBitmapExFileHeader;
 Cnt, ImgSize: Integer;
 Frame: PFrameInfo;
 DataHeader: TMilaDataHeader;
 FrameHeader: TMilaFrameHeader;
begin
 with FImageInfo do
 begin
  LongWord(Header.exMagic) := BMPEX_MAGIC;
  Header.exHeaderSize := SizeOf(TBitmapExFileHeader);
 { if FCompressed then
  begin
   Header.exImageSize := FCompressedSize;
   Header.exExtraFlags := 1;
  end else
  begin   }
   Header.exImageSize := ImageSize;
   Header.exExtraFlags := 2;
 // end;
  Header.exLeft := FLeft;
  Header.exTop := FTop;
  Header.exWidth := FWidth;
  Header.exWidthRemainder := FWidthRemainder;
  Header.exHeight := FHeight;
  Header.exImageFlags := FImageFlags;
  Header.exCompositeCount := Length(CompositeFlags);
  Header.exImageFormat := FImageFormat;
  Header.exTransparentColor := FTransparentColor;
  Header.exColorsCount := FColorsUsed;
  Header.exColorFormat.colFlags := FInternalClrFmt.FFlags;
  Header.exColorFormat.colSize := FInternalClrFmt.FColorSize;
  Header.exColorFormat.colReserved1 := 0;
  Header.exColorFormat.colBits := FInternalClrFmt.FUsedBits;
  Header.exColorFormat.colBlueMask := FInternalClrFmt.FBlueMask;
  Header.exColorFormat.colGreenMask := FInternalClrFmt.FGreenMask;
  Header.exColorFormat.colRedMask := FInternalClrFmt.FRedMask;
  Header.exColorFormat.colAlphaMask := FInternalClrFmt.FAlphaMask;
  Stream.WriteBuffer(Header, SizeOf(TBitmapExFileHeader));
  if Header.exCompositeCount > 0 then
   Stream.WriteBuffer(Pointer(CompositeFlags)^, Header.exCompositeCount * 2);
  Frame := @FirstFrame;
  while Frame <> nil do
  begin
   LongWord(DataHeader.dhHeaderID) := MILA_FRAME;
   DataHeader.dhHeaderSize := SizeOf(TMilaFrameHeader);
   Stream.WriteBuffer(DataHeader, SizeOf(TMilaDataHeader));
   if (FImageFormat = ifIndexed) and (Frame.fiPalette <> nil) then
   begin
    if Frame.fiPalCmprSize > 0 then
    begin
     FrameHeader.fhPaletteSize := Frame.fiPalCmprSize;
     Cnt := Frame.fiPalCmprSize;
    end else
    begin
     if FColorsUsed = 0 then
      Cnt := PaletteCount * Header.exColorFormat.colSize else
      Cnt := Min(256, FColorsUsed) * Header.exColorFormat.colSize;
     FrameHeader.fhPaletteSize := 0;
    end;
   end else
   begin
    FrameHeader.fhPaletteSize := -1;
    Cnt := 0;
   end;
   if Frame.fiImage <> nil then
   begin
    if Frame.fiImgCmprSize > 0 then
    begin
     FrameHeader.fhImageSize := Frame.fiPalCmprSize;
     ImgSize := Frame.fiPalCmprSize;
    end else
    begin
     FrameHeader.fhImageSize := 0;
     ImgSize := Header.exImageSize;
    end;
   end else
   begin
    FrameHeader.fhImageSize := -1;
    ImgSize := 0;
   end;
   FrameHeader.fhDelay := Frame.fiDelay;
   Stream.WriteBuffer(FrameHeader, SizeOf(TMilaFrameHeader));
   if Cnt > 0 then
    Stream.WriteBuffer(FirstFrame.fiPalette^, Cnt);
   if ImgSize > 0 then
    Stream.WriteBuffer(FirstFrame.fiImage^, ImgSize);
   Frame := Frame.fiNext;
  end;
  LongWord(DataHeader.dhHeaderID) := MILA_END;
  DataHeader.dhHeaderSize := 0;
  Stream.WriteBuffer(DataHeader, SizeOf(TMilaDataHeader));
 end;
end;

{ TColorTable }

procedure TColorTable.Assign(Source: TNode);
var
 Src: TColorTable absolute Source;
begin
 if Source <> Self then
 begin
  inherited;
  FTransparentIndex := Src.FTransparentIndex;
  SetColorFormat(Src.FColorFormat);
  ColorsCount := Src.ColorsCount;
  Move(Src.FColorData^, FColorData^, TableSize);
 end;
end;

function TColorTable.CalculateChecksum: TChecksum;
begin
 Result :=CalcCheckSum(FColorData^, TableSize) +
           inherited CalculateChecksum;
end;

function TColorTable.CalculateDataSize: LongInt;
begin
 Result := TableSize + inherited CalculateDataSize;
end;

destructor TColorTable.Destroy;
begin
 FColorFormat.Free;
 FreeMem(FColorData);
end;

procedure TColorTable.FillHeader(var Header: TSectionHeader);
begin
 with TColorTableHeader(Addr(Header.dh)^) do
 begin
  cthColorFmt.colFlags     := FColorFormat.Flags;
  cthColorFmt.colSize      := FColorFormat.ColorSize;
  cthColorFmt.colReserved1 := 0;
  cthColorFmt.colBits      := FColorFormat.UsedBits;
  cthColorFmt.colRedMask   := FColorFormat.RedMask;
  cthColorFmt.colGreenMask := FColorFormat.GreenMask;
  cthColorFmt.colBlueMask  := FColorFormat.BlueMask;
  cthColorFmt.colAlphaMask := FColorFormat.AlphaMask;
  cthColorCnt := FColorsCount;
  cthTransparentIndex := FTransparentIndex;
  cthNameLength := ByteSwap16(Min(Length(FName), High(Word)));
 end;
end;

function TColorTable.GetColor(X: Integer): LongWord;
begin
 if (FColorData <> nil) and (X >= 0) and (X < FColorsCount) then
  Result := RGBZ_ColorFormat.FromRGBQuad(FColorFormat.ToRGBQuad(
       Pointer(Integer(FColorData) + X * FColorFormat.ColorSize)^)) else
  Result := 0;
end;

function TColorTable.GetColorPtr(X: Integer): Pointer;
begin
 if (FColorData <> nil) and (X >= 0) and (X < FColorsCount) then
  Result := Pointer(Integer(FColorData) + X * FColorFormat.ColorSize) else
  Result := nil;
end;

function TColorTable.GetFormatString: AnsiString;
begin
 Result := FColorFormat.FormatString;
end;

function TColorTable.GetRGBQuad(X: Integer): LongWord;
begin
 if (FColorData <> nil) and (X >= 0) and (X < FColorsCount) then
  Result := FColorFormat.ToRGBQuad(Pointer(Integer(FColorData) +
                                   X * FColorFormat.ColorSize)^) else
  Result := 0;
end;

function TColorTable.GetTableSize: Integer;
begin
 Result := FColorFormat.ColorSize * FColorsCount;
end;

procedure TColorTable.Initialize;
begin
 FColorFormat := TColorFormat.Create;
 FTransparentIndex := -1;
 FSignature := CTBL_SIGN;
 FHeaderSize := SizeOf(TColorTableHeader);
 FMaxNameLength := High(Word);
end;
         {
procedure TColorTable.LoadFromStream(Stream: TStream);
var
 MainHeader: TMainHeader;
 Header: TColorTableHeader;
begin
 Stream.ReadBuffer(MainHeader, SizeOf(TMainHeader));
 if (MainHeader.mhSignature <> CTBL_SIGN) or
    (MainHeader.mhHeaderSize <> SizeOf(TColorTableHeader)) then
  ColorTableFormatError;
 Stream.ReadBuffer(Header, SizeOf(TColorTableHeader));
 ColorsCount := 0; 
 FColorFormat.SetFormat(Header.cthColorFmt.colRedMask,
                        Header.cthColorFmt.colGreenMask,
                        Header.cthColorFmt.colBlueMask,
                        Header.cthColorFmt.colAlphaMask);
 FColorFormat.Flags := Header.cthColorFmt.colFlags;
 FColorFormat.ColorSize := Header.cthColorFmt.colSize;
 FColorFormat.UsedBits := Header.cthColorFmt.colBits;
 FTransparentIndex := Header.cthTransparentIndex;
 ColorsCount := Header.cthColorCnt;
 SetLength(FTableName, Header.cthNameLength);
 Stream.ReadBuffer(Pointer(FTableName)^, Length(FTableName) shl 1);
 Stream.ReadBuffer(FColorData^, TableSize);
end;         }

procedure TColorTable.LoadActFromStream(Stream: TStream);
var
 RGB: array [0..256 * 3 - 1] of Byte;
 Rec: LongRec;
begin
 ColorsCount := 0;
 Stream.ReadBuffer(RGB, SizeOf(RGB));
 if Stream.Read(Rec, 4) = 4 then
 begin
  ColorsCount := ByteSwap16(Rec.Lo);
  FTransparentIndex := SmallInt(ByteSwap16(Rec.Hi));
 end else
 begin
  ColorsCount := 256;
  FTransparentIndex := -1;
 end;
 RGB_ColorFormat.ConvertTo(FColorFormat, @RGB, FColorData, FColorsCount);                          
end;

procedure TColorTable.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 Rec: TProgressRec;
begin
 inherited;
 
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_READ_DATA;
 Read(Rec, FColorData^, TableSize);
end;

function TColorTable.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result then
  with TColorTableHeader(Addr(Header.dh)^) do
  begin
   ColorsCount := 0;
   FColorFormat.SetFormat(cthColorFmt.colRedMask,
                          cthColorFmt.colGreenMask,
                          cthColorFmt.colBlueMask,
                          cthColorFmt.colAlphaMask);
   FColorFormat.Flags := cthColorFmt.colFlags;
   FColorFormat.ColorSize := cthColorFmt.colSize;
   FColorFormat.UsedBits := cthColorFmt.colBits;
   FTransparentIndex := cthTransparentIndex;
   ColorsCount := cthColorCnt;
   cthNameLength := ByteSwap16(cthNameLength);
   SetLength(FName, cthNameLength);
  end;
end;

procedure TColorTable.Reset(const AColorFmt: AnsiString; ACount: Integer);
var
 OldFormat: TColorFormat;
 SZ: Integer;
 Buf, Temp: Pointer;
begin
 OldFormat := TColorFormat.Create;
 try
  OldFormat.Assign(FColorFormat);
  FColorFormat.SetFormat(AColorFmt);
  if ACount < 0 then ACount := FColorsCount;
  SZ := FColorFormat.FColorSize * ACount;
  GetMem(Buf, SZ);
  try
   FillChar(Buf^, SZ, 0);
   OldFormat.ConvertTo(FColorFormat, FColorData, Buf, Min(ACount, FColorsCount));
   Temp := Buf;
   Buf := FColorData;
   FColorData := Temp;
   FColorsCount := ACount;
  finally
   FreeMem(Buf);
  end;
 finally
  OldFormat.Free;
 end;
end;

procedure TColorTable.Reset(AColorFmt: TColorFormat; ACount: Integer);
var
 OldFormat: TColorFormat;
 SZ: Integer;
 Buf, Temp: Pointer;
begin
 OldFormat := TColorFormat.Create;
 try
  OldFormat.Assign(FColorFormat);
  FColorFormat.Assign(AColorFmt);
  if ACount < 0 then ACount := FColorsCount;
  SZ := FColorFormat.FColorSize * ACount;
  GetMem(Buf, SZ);
  try
   FillChar(Buf^, SZ, 0);
   OldFormat.ConvertTo(FColorFormat, FColorData, Buf, Min(ACount, FColorsCount));
   Temp := Buf;
   Buf := FColorData;
   FColorData := Temp;
   FColorsCount := ACount;
  finally
   FreeMem(Buf);
  end;
 finally
  OldFormat.Free;
 end;
end;
                  {
procedure TColorTable.SaveToStream(Stream: TStream);
var
 MainHeader: TMainHeader;
 Header: TColorTableHeader;
begin
 MainHeader.mhSignature := CTBL_SIGN;
 MainHeader.mhHeaderSize := SizeOf(TColorTableHeader);
 Stream.WriteBuffer(MainHeader, SizeOf(TMainHeader));
 Header.cthColorFmt.colFlags     := FColorFormat.Flags;
 Header.cthColorFmt.colSize      := FColorFormat.ColorSize;
 Header.cthColorFmt.colReserved1 := 0;
 Header.cthColorFmt.colBits      := FColorFormat.UsedBits;
 Header.cthColorFmt.colRedMask   := FColorFormat.RedMask;
 Header.cthColorFmt.colGreenMask := FColorFormat.GreenMask;
 Header.cthColorFmt.colBlueMask  := FColorFormat.BlueMask;
 Header.cthColorFmt.colAlphaMask := FColorFormat.AlphaMask;
 Header.cthColorCnt := FColorsCount;
 Header.cthTransparentIndex := FTransparentIndex;
 Header.cthReserved := 0;
 Header.cthNameLength := Min(Length(FTableName), 255);
 Stream.WriteBuffer(Header, SizeOf(TColorTableHeader));
 Stream.WriteBuffer(Pointer(FTableName)^, Header.cthNameLength shl 1);
 Stream.WriteBuffer(FColorData^, TableSize);
end;          }

procedure TColorTable.SaveActToStream(Stream: TStream);
var
 RGB: array [0..256 * 3 - 1] of Byte;
 Rec: LongRec;
begin
 FillChar(RGB, SizeOf(RGB), 0);
 FColorFormat.ConvertTo(RGB_ColorFormat, FColorData, @RGB, FColorsCount);
 Rec.Lo := ByteSwap16(FColorsCount);
 Rec.Hi := ByteSwap16(FTransparentIndex);
 Stream.WriteBuffer(RGB, SizeOf(RGB));
 Stream.WriteBuffer(Rec, 4);
end;

procedure TColorTable.SetColor(X: Integer; Value: LongWord);
begin
 if (FColorData <> nil) and (X >= 0) and (X < FColorsCount) then
  FColorFormat.SetColor(FColorData, X, RGBZ_ColorFormat.ValueToRGBQuad(Value));
end;

procedure TColorTable.SetColorFormat(Value: TColorFormat);
var
 Buf: Pointer;
 TSZ, Cnt: Integer;
begin
 if Value = nil then
  ColorFormat.SetFormat(0, 0, 0, 0) else
 if not Value.SameAs(ColorFormat) then
 begin
  Cnt := ColorsCount;
  TSZ := Cnt * Value.ColorSize;
  GetMem(Buf, TSZ);
  try
   FColorFormat.ConvertTo(Value, FColorData, Buf, Cnt);
   FColorFormat.Assign(Value);
   ColorsCount := 0;
   ColorsCount := Cnt;
   Move(Buf^, FColorData^, TSZ);
  finally
   FreeMem(Buf);
  end;
 end;
end;

procedure TColorTable.SetColorsCount(Value: Integer);
var
 Buf, Temp: Pointer;
 SZ: Integer;
begin
 if FColorsCount <> Value then
 begin
  Value := Min(Max(0, Value), High(Word));
  SZ := Value * FColorFormat.FColorSize;
  GetMem(Buf, SZ);
  try
   FillChar(Buf^, SZ, 0);
   Move(FColorData^, Buf^, Min(Value, FColorsCount) * FColorFormat.FColorSize);
   Temp := Buf;
   Buf := FColorData;
   FColorData := Temp;
   FColorsCount := Value;
  finally
   FreeMem(Buf);
  end;
 end;
end;

procedure TColorTable.SetFormatString(const Value: AnsiString);
var
 NewFmt: TColorFormat;
 Buf: Pointer;
 TSZ, Cnt: Integer;
begin
 NewFmt := TColorFormat.Create;
 try
  NewFmt.FormatString := Value;
  Cnt := ColorsCount;
  TSZ := Cnt * NewFmt.ColorSize;
  GetMem(Buf, TSZ);
  try
   FColorFormat.ConvertTo(NewFmt, FColorData, Buf, Cnt);
   FColorFormat.Assign(NewFmt);
   ColorsCount := 0;
   ColorsCount := Cnt;
   Move(Buf^, FColorData^, TSZ);
  finally
   FreeMem(Buf);
  end;
 finally
  NewFmt.Free;
 end;
end;

procedure TColorTable.SetRGBQuad(X: Integer; Value: LongWord);
begin
 if (FColorData <> nil) and (X >= 0) and (X < FColorsCount) then
  FColorFormat.SetColor(FColorData, X, Value);
end;

procedure TColorTable.WriteData(Stream: TStream; var Header: TSectionHeader);
var
 Rec: TProgressRec;
begin
 inherited;
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_WRITE_DATA;

 Write(Rec, FColorData^, TableSize);
end;

{ TColorTableList }

function TColorTableList.AddColorTable(const AName: WideString;
  const AColorFmt: AnsiString; ACount: Integer): TColorTable;
begin
 Result := AddNode as TColorTable;
 Result.Name := AName;
 Result.ColorFormat.SetFormat(AColorFmt);
 Result.ColorsCount := ACount;
end;

function TColorTableList.AddColorTable(const AName: WideString;
  AColorFmt: TColorFormat; ACount: Integer): TColorTable;
begin
 Result := AddNode as TColorTable;
 Result.Name := AName;
 Result.ColorFormat.Assign(AColorFmt);
 Result.ColorsCount := ACount;
end;

function TColorTableList.GetColTblItem(Index: Integer): TColorTable;
begin
 Result := Nodes[Index] as TColorTable;
end;

procedure TColorTableList.Initialize;
begin
 inherited;
 FAssignableClass := TColorTableList;
 FNodeClass := TColorTable;
 FSignature := CLST_SIGN;
end;
    {
procedure TColorTableList.LoadFromStream(Stream: TStream);
var
 I: Integer;
 MainHeader: TMainHeader;
 Header: TColorTableListHeader;
begin
 Clear;
 Stream.ReadBuffer(MainHeader, SizeOf(TMainHeader));
 if (MainHeader.mhSignature <> CLST_SIGN) or
    (MainHeader.mhHeaderSize <> SizeOf(TColorTableListHeader)) then
  ColorTableFormatError;
 Stream.ReadBuffer(Header, SizeOf(TColorTableListHeader));
 for I := 0 to Header.ctlCount - 1 do
  (AddNode as TColorTable).LoadFromStream(Stream);
end;        }

{procedure TColorTableList.SaveToStream(Stream: TStream);
var
 I: Integer;
 MainHeader: TMainHeader;
 Header: TColorTableListHeader;
begin
 MainHeader.mhSignature := CLST_SIGN;
 MainHeader.mhHeaderSize := SizeOf(TColorTableListHeader);
 Stream.WriteBuffer(MainHeader, SizeOf(TMainHeader));
 Header.ctlCount := Count;
 Stream.WriteBuffer(Header, SizeOf(TColorTableListHeader));
 for I := 0 to Count - 1 do Items[I].SaveToStream(Stream);
end;
 }

initialization
 MMX_Present := HaveMMX;
 BGRZ_ColorFormat := TColorFormat.Create(CF_BGRZ);
 BGRA_ColorFormat := TColorFormat.Create(CF_BGRA);
 RGB_ColorFormat := TColorFormat.Create(CF_RGB);
 RGBZ_ColorFormat := TColorFormat.Create(CF_RGBZ);
 GrayScaleFormat := TColorFormat.Create(CF_GRAY);
finalization
 GrayScaleFormat.Free;
 RGBZ_ColorFormat.Free;
 RGB_ColorFormat.Free;
 BGRA_ColorFormat.Free;
 BGRZ_ColorFormat.Free;
end.
