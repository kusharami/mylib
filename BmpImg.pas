unit BmpImg;

interface

uses
 Windows, MyUtils;

type
 PRGBQuads = ^TRGBQuads;
 TRGBQuads = array[Byte] of TRGBQuad;

 TRGB = packed record
  R: Byte;
  G: Byte;
  B: Byte;
 end;

 PRGBList = ^TRGBList;
 TRGBList = array[Byte] of TRGB;

 PBitmapHeader = ^TBitmapHeader;
 TBitmapHeader = packed record
  bhMainHeader: TBitmapFileHeader;
  bhInfoHeader: TBitmapInfoHeader;
 end;

 PBitmapHeaderV4 = ^TBitmapHeaderV4;
 TBitmapHeaderV4 = packed record
  bhMainHeader: TBitmapFileHeader;
  bhInfoHeader: TBitmapV4Header;
 end;

 PZSoftPCX_Header = ^TZSoftPCX_Header;
 TZSoftPCX_Header = packed record
  pcxFormatID:    Byte;       // Always $0A
  pcxVersion:     Byte;       // 0-5
  pcxCompressed:  Boolean;
  pcxBitCount:    Byte;
  pcxLeft:        SmallInt;
  pcxTop:         SmallInt;
  pcxRight:       SmallInt;
  pcxBottom:      SmallInt;
  pcxHorDPI:      Word;
  pcxVertDPI:     Word;
  pcxEgaPalette:  array[0..15] of TRGB;
  pcxReserved1:   Byte;
  pcxPlaneCount:  Byte;
  pcxWidthBytes:  Word;
  pcxPaletteInfo: Word;
  pcxScrWidth:    Word;
  pcxScrHeight:   Word;
  pcxReserved2:   array[0..53] of Byte;
 end;

 TOrder = (fromLeftToRight, fromRightToLeft);

 PMaskArray = ^TMaskArray;
 TMaskArray = array[0..7] of Byte;

const
 PixelsInByte: array[0..7] of Byte =
 (8 div 1, 8 div 2, 8 div 4, 8 div 4, 1, 1, 1, 1);
 PixelsInByteM: array[0..7] of Byte =
 (8 div 1 - 1, 8 div 2 - 1, 8 div 4 - 1, 8 div 4 - 1, 0, 0, 0, 0);

 TwoBitLevels: array[0..3] of Byte =
 (0,
  1 * (255 div (1 shl 2 - 1)),
  2 * (255 div (1 shl 2 - 1)),
  3 * (255 div (1 shl 2 - 1)));

 ThreeBitLevels: array[0..7] of Byte =
 (0,
 (1 * (255 div (1 shl 3 - 1))) or (1 shr 1),
 (2 * (255 div (1 shl 3 - 1))) or (2 shr 1),
 (3 * (255 div (1 shl 3 - 1))) or (3 shr 1),
 (4 * (255 div (1 shl 3 - 1))) or (4 shr 1),
 (5 * (255 div (1 shl 3 - 1))) or (5 shr 1),
 (6 * (255 div (1 shl 3 - 1))) or (6 shr 1),
 (7 * (255 div (1 shl 3 - 1))) or (7 shr 1));

const
 LRBM: TMaskArray =
 (
  1 shl 7,
  1 shl 6,
  1 shl 5,
  1 shl 4,
  1 shl 3,
  1 shl 2,
  1 shl 1,
  1
 );
 RLBM: TMaskArray =
 (
  1,
  1 shl 1,
  1 shl 2,
  1 shl 3,
  1 shl 4,
  1 shl 5,
  1 shl 6,
  1 shl 7
 ); 

procedure CGAtoBGRA(Source: PRGBList; Dest: PRGBQuads;
                    Count: Integer = 256);
function GetLineSize(Width: Integer; BitCount: Byte): Integer;
function GetXOffset(X: Integer; BitCount: Byte): Integer;
function GetEgaLineSize(Width: Integer): Integer;
function GetPlanarLineSize(Width: Integer; PlaneBCnt, BCnt: Byte): Integer;
function GetPlanarLineSizeRmndr(Width, Rmndr: Integer; PlaneBCnt, BCnt: Byte): Integer;
function GetWidthBytes(Width: Integer; BitCount: Byte): Integer;
function GetBitmapSize(Width, Height: Integer; BitCount: Byte): Integer;
function FillBitmapHeader(Dest: PBitmapHeader;
  Width, Height: Integer; BitCount: Byte): Integer;
function FillBitmapHeaderV4(Dest: PBitmapHeaderV4;
  Width, Height: Integer; BitCount: Byte): Integer;
function GetPaletteSize(BitCount: Byte): Integer;
procedure RAWtoBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer; BitCount: Byte; RowStride: Integer = 0);
procedure EGAtoBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer);
procedure PlanarToBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer; BitCount: Byte;
  PlaneOrder: TOrder = fromLeftToRight;
  BitsOrder: TOrder = fromLeftToRight);
procedure FontToBMP(var Source, Dest; CW, CH, IW, IH, Cnt: Integer);
procedure DecodeRLE4(var Source, Dest; WidthBytes: Integer);
procedure DecodeRLE8(var Source, Dest; WidthBytes: Integer);
function ConvertColorValue(SrcBits: Byte; Value: LongWord): Byte;
procedure GetVisibleRect(var Rect: TRect; Buf: Pointer; Width, Height, CellSize: Integer);
procedure GetValue32Rect(var Rect: TRect; Buf: Pointer;
           Width, Height: Integer; EmptyValue: LongWord);


type
 THSLTriple = record
  H: Double;
  S: Double;
  L: Double;
 end;

 TXYZTriple = record
  X: Double;
  Y: Double;
  Z: Double;
 end;

 TLabTriple = record
  L: Double;
  A: Double;
  B: Double;
 end;

function RGB2XYZ(const RGB: TRGBQuad): TXYZTriple;
function RGB2LAB(const RGB: TRGBQuad): TLabTriple;
function XYZ2LAB(const XYZ: TXYZTriple): TLabTriple;

function RGB2HSL(const RGB: TRGBQuad): THSLTriple;
function HSL2RGB(const HSL: THSLTriple): TRGBQuad;

function LabCompare(const Value1, Value2: TLabTriple): Double;

implementation

uses Math;

function RGB2LAB(const RGB: TRGBQuad): TLabTriple;
begin
 Result := XYZ2LAB(RGB2XYZ(RGB));
end;

function LabCompare(const Value1, Value2: TLabTriple): Double;
begin
 Result := Sqrt(Power(Value1.L - Value2.L, 2) +
                Power(Value1.A - Value2.A, 2) +
                Power(Value1.B - Value2.B, 2));
end;

function v2v(const Value: Double): Double;
begin
 if Value > 0.04045 then
  Result := Power((Value + 0.055 ) / 1.055, 2.4) else
  Result := Value / 12.92;
 Result := Result * 100.0;
end;

function RGB2XYZ(const RGB: TRGBQuad): TXYZTriple;
var
 vR, vG, vB: Double;
begin
 with RGB, Result do
 begin
  vR := v2v(rgbRed / 255.0);
  vG := v2v(rgbGreen / 255.0);
  vB := v2v(rgbBlue / 255.0);

  X := vR * 0.4124 + vG * 0.3576 + vB * 0.1805;
  Y := vR * 0.2126 + vG * 0.7152 + vB * 0.0722;
  Z := vR * 0.0193 + vG * 0.1192 + vB * 0.9505;
 end;
end;

function v2v_XYZ(const Value: Double): Double;
begin
 if Value > 0.008856 then
  Result := Power(Value, 1.0 / 3.0) else
  Result := (7.787 * Value) + (16.0 / 116.0);
end;

function XYZ2LAB(const XYZ: TXYZTriple): TLabTriple;
var
 vX, vY, vZ: Double;
begin
 with XYZ, Result do
 begin
  vX := v2v_XYZ(X / 95.047);
  vY := v2v_XYZ(Y / 100.000);
  vZ := v2v_XYZ(Z / 108.883);

  L := (116.0 * vY) - 16.0;
  A := 500.0 * (vX - vY);
  B := 200.0 * (vY - vZ);
 end;
end;

function RGB2HSL(const RGB: TRGBQuad): THSLTriple;
var
 vMin, vMax, delMax, delR, delG, delB, vR, vG, vB: Double;
begin
 with RGB, Result do
 begin
  vR := rgbRed / 255.0;
  vG := rgbGreen / 255.0;
  vB := rgbBlue / 255.0;

  vMin := Min(vR, Min(vG, vB));
  vMax := Max(vR, Max(vG, vB));

  delMax := vMax - vMin;

  L := (vMax + vMin) / 2.0;

  if delMax = 0.0 then
  begin
   H := 0.0;
   S := 0.0;
  end else
  begin
   if L < 0.5 then
    S := delMax / (vMax + vMin) else
    S := delMax / (2.0 - vMax - vMin);

   delR := (((vMax - vR) / 6.0) + (delMax / 2.0)) / delMax;
   delG := (((vMax - vG) / 6.0) + (delMax / 2.0)) / delMax;
   delB := (((vMax - vB) / 6.0) + (delMax / 2.0)) / delMax;

   if vR = vMax then
    H := delB - delG else
   if vG = vMax then
    H := (1.0 / 3.0) + delR - delB else
   if vB = vMax then
    H := (2.0 / 3.0) + delG - delR;

   if H < 0.0 then
    H := H + 1.0 else
   if H > 1.0 then
    H := H - 1.0;
  end;
 end;
end;

function HueToColor(const v1, v2: Double; vH: Double): Double;
begin
 if vH < 0.0 then
  vH := vH + 1.0 else
 if vH > 1.0 then
  vH := vH - 1.0;

 if 6.0 * vH < 1.0 then
  Result := v1 + (v2 - v1) * 6.0 * vH else
 if 2.0 * vH < 1.0 then
  Result := v2 else
 if 3.0 * vH < 2.0 then
  Result := v1 + (v2 - v1) * ((2.0 / 3.0) - vH) * 6.0 else
 Result := v1;
end;

function HSL2RGB(const HSL: THSLTriple): TRGBQuad;
var
 v1, v2: Double;
begin
 with HSL, Result do
 begin
  rgbReserved := 255;

  if S = 0.0 then
  begin
   rgbRed   := Round(L * 255.0);
   rgbGreen := rgbRed;
   rgbBlue  := rgbBlue;
  end else
  begin
   if L < 0.5 then
    v2 := L * (1.0 + S) else
    v2 := (L + S) - (S * L);

   v1 := 2.0 * L - v2;

   rgbRed   := Round(255.0 * HueToColor(v1, v2, H + (1.0 / 3.0)));
   rgbGreen := Round(255.0 * HueToColor(v1, v2, H));
   rgbBlue  := Round(255.0 * HueToColor(v1, v2, H - (1.0 / 3.0)));
  end;

 end;
end;

procedure DecodeRLE4(var Source, Dest; WidthBytes: Integer);
var
 B1, B2, C: Byte;
 Dst, Src, P: PByte;
 X, Y, i: Integer;
begin
 Src := @Source;
 X := 0;
 Y := 0;
 while True do
 begin
  B1 := Src^;
  Inc(Src);
  B2 := Src^;
  Inc(Src);

  if B1 = 0 then
  begin
   case B2 of
    0: {  End of line  }
    begin
     X := 0;
     Inc(Y);
    end;
    1: Break; {  End of bitmap  }
    2: {  Difference of coordinates  }
    begin
     Inc(X, B1);
     Inc(Y, B2);
     Inc(Src, 2);
    end;
    else {  Absolute mode  }
    begin
     Dst := Pointer(Integer(@Dest) + Y * WidthBytes);

     C := 0;
     for I := 0 to B2 - 1 do
     begin
      if I and 1 = 0 then
      begin
       C := Src^;
       Inc(Src);
      end else
       C := C shl 4;

      P := Pointer(Integer(Dst) + X shr 1);
      if X and 1 = 0 then
       P^ := (P^ and $0F) or (C and $F0)
      else
       P^ := (P^ and $F0) or ((C and $F0) shr 4);

      Inc(X);
     end;
    end;
   end;
  end else
  begin
   {  Encoding mode  }
   Dst := Pointer(Integer(@Dest) + Y * WidthBytes);

   for i:=0 to B1-1 do
   begin
    P := Pointer(Integer(Dst) + X shr 1);
    if X and 1 = 0 then
     P^ := (P^ and $0F) or (B2 and $F0)
    else
     P^ := (P^ and $F0) or ((B2 and $F0) shr 4);

    Inc(X);

    // Swap nibble
    B2 := (B2 shr 4) or (B2 shl 4);
   end;
  end;

  {  Word alignment  }
  Inc(Src, Longint(Src) and 1);
 end;
end;

procedure DecodeRLE8(var Source, Dest; WidthBytes: Integer);
var
 B1, B2: Byte;
 Dst, Src: PByte;
 X, Y: Integer;
begin
 Src := @Source;
 Dst := @Dest;
 X := 0;
 Y := 0;

 while True do
 begin
  B1 := Src^;
  Inc(Src);
  B2 := Src^;
  Inc(Src);

  if B1=0 then
  begin
   case B2 of
    0: {  End of line  }
    begin
     X := 0;
     Inc(Y);
     Dst := Pointer(Integer(@Dest) + Y * WidthBytes);
    end;
    1: Break; {  End of bitmap  }
    2: {  Difference of coordinates  }
    begin
     Inc(X, B1);
     Inc(Y, B2);
     Inc(Src, 2);
     Dst := Pointer(Integer(@Dest) + Y * WidthBytes + X);
    end;
    else {  Absolute mode  }
    begin
     Move(Src^, Dst^, B2);
     Inc(Dst, B2);
     Inc(Src, B2);
    end;
   end;
  end else {  Encoding mode  }
  begin
   FillChar(Dst^, B1, B2);
   Inc(Dst, B1);
  end;

  {  Word alignment  }
  Inc(Src, Longint(Src) and 1);
 end;
end;
                                      
function ConvertColorValue(SrcBits: Byte; Value: LongWord): Byte;
asm
 mov cl,8
 sub cl,al
 jz @@8bits
 jns @@LessThan8bits
// More than 8 bits
 neg cl
 shr edx,cl
@@8bits:
 mov al,dl
 ret
@@LessThan8bits:
 mov ch,al
 sub ch,cl
 js @@LessThan4bits
 mov al,dl
 shl dl,cl
 mov cl,ch
 shr al,cl
 or al,dl
 ret
@@LessThan4bits:
 dec al
 jz @@1bits // if SrcBits = 1 then
 dec al
 jz @@2bits
 dec al
 jz @@3bits
 xor al,al // return zero
 ret
@@1bits:
 not dl
 inc dl
 mov al,dl // Result = (not Value) + 1
 ret
@@2bits:
 mov al,byte ptr TwoBitLevels[edx]
 ret
@@3bits:
 mov al,byte ptr ThreeBitLevels[edx]
end;
{var
 S, S2: Integer;
 Value8: Byte absolute Value;
begin
 S := (8 - SrcBits);
 if S < 0 then Result := Value shr -S else
 if S = 0 then Result := Value else
 begin
  S2 := SrcBits - S;
  if S2 >= 0 then
   Result := (Value8 shl S) or (Value8 shr S2) else
  case SrcBits of
   1:   Result := (not Value8) + 1;
   2:   Result := TwoBitLevels[Value];
   3:   Result := ThreeBitLevels[Value];
   else Result := 0;
  end;
 end;
end;   }

procedure CGAtoBGRA(Source: PRGBList; Dest: PRGBQuads;
                    Count: Integer = 256);
var
 I: Integer;
begin
 if Count <= 256 then
  for I := 0 to Count - 1 do
   with Source[I], Dest[I] do
   begin
    rgbRed := ConvertColorValue(6, R);
    rgbGreen := ConvertColorValue(6, G);
    rgbBlue := ConvertColorValue(6, B);
    rgbReserved := 255;
   end;
end;

function GetLineSize(Width: Integer; BitCount: Byte): Integer;
begin
 case BitCount of
     1: Result := (Width + 7) shr 3;
     2: Result := (Width + 3) shr 2;
  3, 4: Result := (Width + 1) shr 1;
  5..8: Result := Width;
  else  Result := Width * ((BitCount + 7) shr 3);
 end;
end;

function GetXOffset(X: Integer; BitCount: Byte): Integer;
begin
 case BitCount of
     1: Result := X shr 3;
     2: Result := X shr 2;
  3, 4: Result := X shr 1;
  5..8: Result := X;
  else  Result := X * ((BitCount + 7) shr 3);
 end;
end;

function GetEgaLineSize(Width: Integer): Integer;
begin
 Result := ((Width + 7) shr 3) shl 2;
end;

function GetPlanarLineSize(Width: Integer; PlaneBCnt, BCnt: Byte): Integer;
begin
 case PlaneBCnt of
  1:
  begin
//   PlaneBCnt := 1;
   Result := (Width + 7) shr 3;
  end;
  2:
  begin
//   PlaneBCnt := 2;
   Result := (Width + 3) shr 2;
  end;
  3, 4:
  begin
   PlaneBCnt := 4;
   Result := (Width + 1) shr 1;
  end;
  else
  begin
   PlaneBCnt := 8;
   Result := Width;
  end;
 end;
 Result := ((BCnt + (PlaneBCnt - 1)) div PlaneBCnt) * Result;
 {if BCnt and (PlaneBCnt - 1) <> 0 then
  Result := (BCnt div PlaneBCnt + PlaneBCnt) * Result else
  Result := (BCnt div PlaneBCnt) * Result    }
end;

function GetPlanarLineSizeRmndr(Width, Rmndr: Integer; PlaneBCnt, BCnt: Byte): Integer;
begin
 case PlaneBCnt of
  1:
  begin
//   PlaneBCnt := 1;
   Result := (Width + 7) shr 3;
  end;
  2:
  begin
//   PlaneBCnt := 2;
   Result := (Width + 3) shr 2;
  end;
  3, 4:
  begin
   PlaneBCnt := 4;
   Result := (Width + 1) shr 1;
  end;
  else
  begin
   PlaneBCnt := 8;
   Result := Width;
  end;
 end;
 Result := ((BCnt + (PlaneBCnt - 1)) div PlaneBCnt) * (Result + Rmndr); 
 {
 if BCnt and (PlaneBCnt - 1) <> 0 then
  Result := (BCnt div PlaneBCnt + PlaneBCnt) * (Result + Rmndr) else
  Result := (BCnt div PlaneBCnt) * (Result + Rmndr);}
end;

function GetWidthBytes(Width: Integer; BitCount: Byte): Integer;
begin
 Result := (((Width * BitCount + 31) shr 5) * 4);
end;

function GetBitmapSize(Width, Height: Integer; BitCount: Byte): Integer;
begin
 Result := SizeOf(TBitmapFileHeader) + SizeOf(TBitmapInfoHeader) +
           GetPaletteSize(BitCount) + GetWidthBytes(Width, BitCount) * Height;
end;

function FillBitmapHeader(Dest: PBitmapHeader;
  Width, Height: Integer; BitCount: Byte): Integer;
begin
 Result := GetWidthBytes(Width, BitCount);
 with Dest.bhInfoHeader do
 begin
  biSize := SizeOf(TBitmapInfoHeader);
  biWidth := Width;
  biHeight := Height;
  biPlanes := 1;
  biBitCount := BitCount;
  biCompression := 0;
  biSizeImage := Result * Height;
  biXPelsPerMeter := $B12;
  biYPelsPerMeter := $B12;
  biClrUsed := 0;
  biClrImportant := 0;
 end;
 with Dest.bhMainHeader do
 begin
  bfType := Ord('B') + Ord('M') shl 8;
  bfOffBits := SizeOf(TBitmapFileHeader) +
               SizeOf(TBitmapInfoHeader) + GetPaletteSize(BitCount);
  bfSize := bfOffBits + Dest.bhInfoHeader.biSizeImage;
  bfReserved1 := 0;
  bfReserved2 := 0;
 end;
end;

function FillBitmapHeaderV4(Dest: PBitmapHeaderV4;
  Width, Height: Integer; BitCount: Byte): Integer;
begin
 Result := GetWidthBytes(Width, BitCount);
 with Dest.bhInfoHeader do
 begin
  bV4Size := SizeOf(TBitmapV4Header);
  bV4Width := Width;
  bV4Height := Height;
  bV4Planes := 1;
  bV4BitCount := BitCount;
  bV4SizeImage := Result * Height;
  bV4XPelsPerMeter := $B12;
  bV4YPelsPerMeter := $B12;
  bV4ClrUsed := 0;
  bV4ClrImportant := 0;
  case BitCount of
   16:
   begin
    bV4V4Compression := BI_BITFIELDS;
    bV4AlphaMask := 0;
    bV4RedMask := ((1 shl 5) - 1) shl 10;
    bV4GreenMask := ((1 shl 5) - 1) shl 5;
    bV4BlueMask := ((1 shl 5) - 1) shl 0;
   end;
   24:
   begin
    bV4V4Compression := BI_BITFIELDS;
    bV4AlphaMask := 0;
    bV4RedMask := ((1 shl 8) - 1) shl 16;
    bV4GreenMask := ((1 shl 8) - 1) shl 8;
    bV4BlueMask := ((1 shl 8) - 1) shl 0;
   end;
   32:
   begin
    bV4V4Compression := BI_BITFIELDS;
    bV4AlphaMask := Cardinal(((1 shl 8) - 1) shl 24);
    bV4RedMask := ((1 shl 8) - 1) shl 16;
    bV4GreenMask := ((1 shl 8) - 1) shl 8;
    bV4BlueMask := ((1 shl 8) - 1) shl 0;
   end;
   else
   begin
    bV4V4Compression := 0;
    bV4RedMask := 0;
    bV4GreenMask := 0;
    bV4BlueMask := 0;
    bV4AlphaMask := 0;
   end;
  end;
  bV4CSType := LCS_DEVICE_RGB;
  FillChar(bV4Endpoints, SizeOf(TCIEXYZTriple), 0);
  bV4GammaRed := 0;
  bV4GammaGreen := 0;
  bV4GammaBlue := 0;
 end;
 with Dest.bhMainHeader do
 begin
  bfType := Ord('B') + Ord('M') shl 8;
  bfOffBits := SizeOf(TBitmapFileHeader) +
               SizeOf(TBitmapV4Header) + GetPaletteSize(BitCount);
  bfSize := bfOffBits + Dest.bhInfoHeader.bV4SizeImage;
  bfReserved1 := 0;
  bfReserved2 := 0;
 end;
end;

function GetPaletteSize(BitCount: Byte): Integer;
begin
 if BitCount <= 8 then
  Result := (1 shl BitCount) * 4 else
  Result := 0;
end;

procedure RAWtoBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer; BitCount: Byte; RowStride: Integer);
var
 LineSize, WidthBytes, PaletteSize, Y: Integer;
 Src, Dst: PByte;
begin
 LineSize := GetLineSize(Width, BitCount);
 if RowStride = 0 then RowStride := LineSize;
 WidthBytes := GetWidthBytes(Width, BitCount);
 FillBitmapHeader(@Dest, Width, Height, BitCount);
 Dst := @Dest;
 Inc(Dst, SizeOf(TBitmapHeader));
 PaletteSize := GetPaletteSize(BitCount);
 Move(Palette^, Dst^, PaletteSize);
 Inc(Dst, PaletteSize);
 Inc(Dst, (Height - 1) * WidthBytes);
 Src := @Source;
 for Y := 1 to Height do
 begin
  Move(Src^, Dst^, LineSize);
  Inc(Src, RowStride);
  Dec(Dst, WidthBytes);
 end;
end;

procedure EGAtoBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer);
var
 LineSize, WidthBytes, X, Y: Integer;
 Dst, P, P1, P2, P3, P4: PByte;
begin
 LineSize := GetEgaLineSize(Width) div 4;
 WidthBytes := GetWidthBytes(Width, 4);
 FillBitmapHeader(@Dest, Width, Height, 4);
 Dst := @Dest;
 Inc(Dst, SizeOf(TBitmapHeader));
 Move(Palette^, Dst^, 16 * 4);
 Inc(Dst, 16 * 4);
 Inc(Dst, (Height - 1) * WidthBytes);
 P1 := @Source;
 P2 := Pointer(Integer(P1) + LineSize);
 P3 := Pointer(Integer(P2) + LineSize);
 P4 := Pointer(Integer(P3) + LineSize);
 for Y := 1 to Height do
 begin
  P := Dst;
  for X := 1 to LineSize do
  begin
   P^ := (((P4^ shr 7) and 1) shl 7) or
         (((P3^ shr 7) and 1) shl 6) or
         (((P2^ shr 7) and 1) shl 5) or
         (((P1^ shr 7) and 1) shl 4) or
         (((P4^ shr 6) and 1) shl 3) or
         (((P3^ shr 6) and 1) shl 2) or
         (((P2^ shr 6) and 1) shl 1) or
          ((P1^ shr 6) and 1);
   Inc(P);
   P^ := (((P4^ shr 5) and 1) shl 7) or
         (((P3^ shr 5) and 1) shl 6) or
         (((P2^ shr 5) and 1) shl 5) or
         (((P1^ shr 5) and 1) shl 4) or
         (((P4^ shr 4) and 1) shl 3) or
         (((P3^ shr 4) and 1) shl 2) or
         (((P2^ shr 4) and 1) shl 1) or
          ((P1^ shr 4) and 1);
   Inc(P);
   P^ := (((P4^ shr 3) and 1) shl 7) or
         (((P3^ shr 3) and 1) shl 6) or
         (((P2^ shr 3) and 1) shl 5) or
         (((P1^ shr 3) and 1) shl 4) or
         (((P4^ shr 2) and 1) shl 3) or
         (((P3^ shr 2) and 1) shl 2) or
         (((P2^ shr 2) and 1) shl 1) or
          ((P1^ shr 2) and 1);
   Inc(P);
   P^ := (((P4^ shr 1) and 1) shl 7) or
         (((P3^ shr 1) and 1) shl 6) or
         (((P2^ shr 1) and 1) shl 5) or
         (((P1^ shr 1) and 1) shl 4) or
         (( P4^        and 1) shl 3) or
         (( P3^        and 1) shl 2) or
         (( P2^        and 1) shl 1) or
          ( P1^        and 1);
   Inc(P);
   Inc(P1);
   Inc(P2);
   Inc(P3);
   Inc(P4);
  end;
  P1 := P4;
  P2 := Pointer(Integer(P1) + LineSize);
  P3 := Pointer(Integer(P2) + LineSize);
  P4 := Pointer(Integer(P3) + LineSize);
  Dec(Dst, WidthBytes);
 end;
end;

procedure PlanarToBMP(var Source, Dest; Palette: PRGBQuads;
  Width, Height: Integer; BitCount: Byte; PlaneOrder, BitsOrder: TOrder);
var
 LineSize, PlaneSize, WidthBytes, I, X, Y: Integer;
 DBC, PBC: Byte;
 Src, P, Dst: PByte;
 Planes: array of PByte;
 Mask: PMaskArray;
begin
 LineSize := (Width + 7) shr 3;
 WidthBytes := GetWidthBytes(Width, BitCount);
 FillBitmapHeader(@Dest, Width, Height, BitCount);
 Dst := @Dest;
 Inc(Dst, SizeOf(TBitmapHeader));
 X := GetPaletteSize(BitCount);
 Move(Palette^, Dst^, X);
 Inc(Dst, X);
 Inc(Dst, (Height - 1) * WidthBytes);
 Src := @Source;
 SetLength(Planes, BitCount);
 PlaneSize := LineSize * Height;
 if PlaneOrder = fromLeftToRight then
 for I := 0 to BitCount - 1 do
 begin
  Planes[I] := Src;
  Inc(Src, PlaneSize);
 end else
 for I := BitCount - 1 downto 0 do
 begin
  Planes[I] := Src;
  Inc(Src, PlaneSize);
 end;
 PBC := 0;
 DBC := 0;
 if BitsOrder = fromLeftToRight then
  Mask := @LRBM else
  Mask := @RLBM;
 for Y := 1 to Height do
 begin
  P := Dst;
  for X := 1 to Width do
  begin
   for I := 0 to BitCount - 1 do
   begin
    if DBC = 0 then P^ := 0;
    if Planes[I]^ and Mask[PBC] <> 0 then
     P^ := P^ or LRBM[DBC];
    Inc(DBC);
    if DBC = 8 then
    begin
     Inc(P);
     DBC := 0;
    end;
    if PBC = 7 then Inc(Planes[I]);
   end;
   PBC := (PBC + 1) and 7;
  end;
  if PBC > 0 then
   for I := 0 to BitCount - 1 do
    Inc(Planes[I]);
  Dec(Dst, WidthBytes);
 end;
end;

procedure FontToBMP(var Source, Dest; CW, CH, IW, IH, Cnt: Integer);
var
 Src, Dst, P: PByte;
 PAL: PInt64 absolute Dst;
 WidthBytes, LineSize, X, Y, W, H, TY: Integer;
begin
 IW := IW * CW;
 IH := IH * CH;
 FillBitmapHeader(@Dest, IW, IH, 1);
 Dst := @Dest;
 Inc(Dst, SizeOf(TBitmapHeader));
 PAL^ := $00FFFFFF00000000;
 Inc(PAL);
 W := IW div CW;
 H := IH div CH;
 CW := CW div 8;
 LineSize := GetWidthBytes(IW, 1);
 WidthBytes := LineSize * CH;
 FillChar(Dst^, LineSize * IH, 0);
 Inc(Dst, (IH - 1) * LineSize);
 Src := @Source;
 if Cnt > 0 then
 for Y := 0 to H - 1 do
 begin
  for X := 0 to W - 1 do
  begin
   P := Dst;
   Inc(P, X * CW);
   for TY := 0 to CH - 1 do
   begin
    Move(Src^, P^, CW);
    Inc(Src, CW);
    Dec(P, LineSize);
   end;
   Dec(Cnt);
   if Cnt = 0 then Break;
  end;
  if Cnt = 0 then Break;
  Dec(Dst, WidthBytes);
 end;
end;

procedure GetVisibleRect(var Rect: TRect; Buf: Pointer; Width, Height, CellSize: Integer);
var
 PB: PByte;
 RowStride, WW, HH: Integer;
begin
 if (Width > 0) and (Height > 0) and (CellSize > 0) then
 begin
  PB := Buf;
  HH := Height;
  RowStride := Width * CellSize;
  while (HH > 0) and ZeroMemCheck(PB, RowStride) do
  begin
   Dec(HH);
   Inc(PB, RowStride);
  end;
  Rect.Top := Height - HH;
  if HH > 0 then
  begin
   Inc(PB, (HH - 1) * RowStride);
   HH := Height;
   while (HH > 0) and ZeroMemCheck(PB, RowStride) do
   begin
    Dec(HH);
    Dec(PB, RowStride);
   end;
   Rect.Bottom := HH;
  end else
   Rect.Bottom := Height;
  PB := Buf;
  WW := RowStride;
  while (WW > 0) and ZeroMemVCheck(PB, RowStride, Height) do
  begin
   Dec(WW);
   Inc(PB);
  end;
  Rect.Left := (RowStride - WW) div CellSize;
  if WW > 0 then
  begin
   Inc(PB, WW - 1);
   WW := RowStride;
   while (WW > 0) and ZeroMemVCheck(PB, RowStride, Height) do
   begin
    Dec(WW);
    Dec(PB);
   end;
   Rect.Right := (WW + CellSize - 1) div CellSize;
  end else
   Rect.Right := Width;
 end else
  FillChar(Rect, SizeOf(TRect), 0);
end;

procedure GetValue32Rect(var Rect: TRect; Buf: Pointer;
           Width, Height: Integer; EmptyValue: LongWord);
var
 PD: PLongWord;
 RowStride, WW, HH: Integer;
begin
 if (Width > 0) and (Height > 0)  then
 begin
  PD := Buf;
  HH := Height;
  RowStride := Width;
  while (HH > 0) and (Value32MemCheck(EmptyValue, PD, RowStride) = RowStride) do
  begin
   Dec(HH);
   Inc(PD, RowStride);
  end;
  Rect.Top := Height - HH;
  if HH > 0 then
  begin
   Inc(PD, (HH - 1) * RowStride);
   HH := Height;
   while (HH > 0) and (Value32MemCheck(EmptyValue, PD, RowStride) = RowStride) do
   begin
    Dec(HH);
    Dec(PD, RowStride);
   end;
   Rect.Bottom := HH;
  end else
   Rect.Bottom := Height;
  PD := Buf;
  WW := RowStride;
  while (WW > 0) and Value32MemVCheck(PD, RowStride, Height, EmptyValue) do
  begin
   Dec(WW);
   Inc(PD);
  end;
  Rect.Left := RowStride - WW;
  if WW > 0 then
  begin
   Inc(PD, WW - 1);
   WW := RowStride;
   while (WW > 0) and Value32MemVCheck(PD, RowStride, Height, EmptyValue) do
   begin
    Dec(WW);
    Dec(PD);
   end;
   Rect.Right := WW;
  end else
   Rect.Right := Width;
 end else
  FillChar(Rect, SizeOf(TRect), 0);
end;

end.
