unit ColorReduction;

interface

uses
  Windows, Sysutils, Classes;

Type
  PNode = ^TNode;
  TNode = packed record
    IsLeaf: Boolean;
    PixelCount: Integer;
    RedSum: Integer;
    GreenSum: Integer;
    BlueSum: Integer;
    AlphaSum: Integer;
    Children: array [0..7] of PNode;
    Next: PNode;
  end;
  TColorQuantizer = class
  private
    FTree: PNode;
    FLeafCount: Integer;
    FReducibleNodes: array [0..8] of PNode;
    FMaxColors: Integer;
    FOutputMaxColors: Integer;
    FColorBits: Integer;
    procedure AddColor(var Node: PNode; const Color: TRGBQuad; Level: Integer);
    function CreateNode(Level: Integer): PNode;
    procedure DeleteTree(var Tree: PNode);
    procedure GetPaletteColors(Tree: PNode; Palette: PRGBQuad;
      var Index: Integer; Sum: PInteger);
    procedure ReduceTree;
  public
    property MaxColors: Integer
        read FOutputMaxColors;
    property ColorsCount: Integer
        read FLeafCount;

    procedure Clear;
    constructor Create(MaxColors: Integer = 256; ColorBits: Integer = 8);
    destructor Destroy; override;
    procedure Reset(MaxColors, ColorBits: Integer);
    procedure ProcessBuffer(const Buffer; Count: Integer);
    procedure FillColorTable(var Palette);
  end;

implementation

{ TColorQuantizer }

procedure TColorQuantizer.AddColor(var Node: PNode; const Color: TRGBQuad;
                                   Level: Integer);
var Shift, ML: Integer;
begin
 // If the node doesn't exist, create it.
 if Node = NIL then
  Node := CreateNode(Level);
 with Node^, Color do
 if IsLeaf then // Update color information if it's a leaf node.
 begin
  Inc(PixelCount);
  Inc(RedSum, rgbRed);
  Inc(GreenSum, rgbGreen);
  Inc(BlueSum, rgbBlue);
  Inc(AlphaSum, rgbReserved);
 end else // Recurse a level deeper if the node is not a leaf.
 begin
  Shift := 7 - Level; ML := 1 shl Shift;
  AddColor(Node.Children[(((rgbRed and ML) shr Shift) shl 2) or
                       (((rgbGreen and ML) shr Shift) shl 1) or
                         ((rgbBlue and ML) shr Shift)], Color, Level + 1);
 end;
end;

procedure TColorQuantizer.Clear;
begin
 DeleteTree(FTree);
end;

constructor TColorQuantizer.Create(MaxColors, ColorBits: Integer);
begin
 Reset(MaxColors, ColorBits);
end;

function TColorQuantizer.CreateNode(Level: Integer): PNode;
begin
 New(Result);
 with Result^ do
 begin
  IsLeaf := Level = FColorBits;
  if IsLeaf then Inc(FLeafCount) else
  begin
   Next := FReducibleNodes[Level];
   FReducibleNodes[Level] := Result;
  end;
 end;
end;

procedure TColorQuantizer.DeleteTree(var Tree: PNode);
var I: Integer;
begin
 if Tree = NIL then Exit;
 With Tree^ do for I := 0 to 7 do
  DeleteTree(Children[I]);
 Dispose(Tree);
 Tree := NIL;
end;

destructor TColorQuantizer.Destroy;
begin
 DeleteTree(FTree);
 inherited;
end;

procedure TColorQuantizer.FillColorTable(var Palette);
var
 PRGB: PRGBQuad; Index: Integer;
 Sum: array [0..15] of Integer;
 Temp: array [0..15] of TRGBQuad;
 J, K, NR, NG, NB, NA, NS, A, B: Integer;
begin
 Index := 0;
 if FOutputMaxColors < 16 then
 begin
  GetPaletteColors(FTree, @Temp, Index, @Sum);
  if FLeafCount > FOutputMaxColors then
  begin
   PRGB := Addr(Palette);
   for J := 0 to FOutputMaxColors - 1 do
   begin
    A := (J * FLeafCount) div FOutputMaxColors;
    B := ((J + 1) * FLeafCount) div FOutputMaxColors;
    NR := 0;
    NG := 0;
    NB := 0;
    NA := 0;
    NS := 0;
    for K := A to B - 1 do with Temp[K] do
    begin
     Inc(NR, rgbRed * Sum[K]);
     Inc(NG, rgbGreen * Sum[K]);
     Inc(NB, rgbBlue * Sum[K]);
     Inc(NA, rgbReserved * Sum[K]);
     Inc(NS, Sum[K]);
    end;
    with PRGB^ do
    begin
     rgbRed := NR div NS;
     rgbGreen := NG div NS;
     rgbBlue := NB div NS;
     rgbReserved := NA div NS;
    end;
    Inc(PRGB);
   end;
  end else
   Move(Temp, Palette, FLeafCount * SizeOf(TRGBQuad));
 end else
  GetPaletteColors(FTree, @Palette, Index, NIL);
end;

procedure TColorQuantizer.GetPaletteColors(Tree: PNode; Palette: PRGBQuad;
  var Index: Integer; Sum: PInteger);
var I: Integer;
begin
 if (Tree <> NIL) and (Palette <> NIL) then
 begin
  with Tree^ do
   if IsLeaf then
   with Palette^ do
   begin
    rgbRed := RedSum div PixelCount;
    rgbGreen := GreenSum div PixelCount;
    rgbBlue := BlueSum div PixelCount;
    rgbReserved := AlphaSum div PixelCount;
    if Sum <> NIL then
    begin
     Inc(Sum, Index);
     Sum^ := PixelCount;
    end;
    Inc(Index);
   end else
    for I := 0 to 7 do
     GetPaletteColors(Children[I], Palette, Index, Sum);
 end;
end;

procedure TColorQuantizer.ProcessBuffer(const Buffer; Count: Integer);
var I: Integer; P: PRGBQuad;
begin
 P := @Buffer;
 for I := 0 to Count - 1 do
 begin
  AddColor(FTree, P^, 0);
  while FLeafCount > FMaxColors do
   ReduceTree;
  Inc(P);
 end;
end;

procedure TColorQuantizer.ReduceTree;
var I: Integer; P: ^PNode; Node, Child: PNode; R, G, B, A, ChildCount: Integer;
begin
 // Find the deepest level containing at least one reducible node.
 I := FColorBits - 1;
 P := Addr(FReducibleNodes);
 Inc(P, I);
 while (I > 0) and (P^ = NIL) do
 begin
  Dec(I);
  Dec(P);
 end;
 // Reduce the node most recently added to the list at level I.
 Node := P^;
 P^ := Node.Next;
 R := 0;
 G := 0;
 B := 0;
 A := 0;
 ChildCount := 0;
 with Node^ do
 begin
  for I := 0 to 7 do
  begin
   Child := Children[I];
   if Child <> NIL then
   begin
    with Child^ do
    begin
     Inc(R, RedSum);
     Inc(G, GreenSum);
     Inc(B, BlueSum);
     Inc(A, AlphaSum);
     Inc(Node.PixelCount, PixelCount);
    end;
    Dispose(Child);
    Children[I] := NIL;
    Inc(ChildCount);
   end;
  end;
  IsLeaf := True;
  RedSum := R;
  GreenSum := G;
  BlueSum := B;
  AlphaSum := A;
 end;
 Dec(FLeafCount, ChildCount - 1);
end;

procedure TColorQuantizer.Reset(MaxColors, ColorBits: Integer);
begin
 if ColorBits > 8 then
  ColorBits := 8 else
 if ColorBits < 1 then
  ColorBits := 1;
 FColorBits := ColorBits;
 DeleteTree(FTree);
 FLeafCount := 0;
 FillChar(FReducibleNodes, SizeOf(FReducibleNodes), 0);
 FMaxColors := MaxColors;
 FOutputMaxColors := MaxColors;
end;

end.
