unit Compression;

interface

uses
 SysUtils, Classes, HexUnit;

const
 PackBufSize = 8192;

 (* TCustomPackStream states *)
 PS_ERROR    = -1;
 PS_FINISHED =  0;
 PS_PREPARE  =  1;
 PS_CUSTOM   =  2;
 PS_BUFWRITE = MaxInt;
 (* TCustomPackStreamML states *)
 PS_ML_INIT       = PS_PREPARE;
 PS_ML_FILL_LIMIT = PS_CUSTOM;
 PS_ML_SEARCH     = PS_CUSTOM + 1;
 PS_ML_WRITE      = PS_CUSTOM + 2;
 (* TCustomLZ_PackStream constants *)
 RING_SHIFT = 12;
 (* TCustomUnpackStream states *)
 US_ERROR    = -1;
 US_FINISHED =  0;
 US_PREPARE  =  1;
 US_CUSTOM   =  2;

 (* Compression utils *)

type
 TCustomPackUnpackStream = class(TStream)
  protected
   FInput:          PByte;
   FOutput:         PByte;
   FRemainIn:       Integer;
   FRemainOut:      Integer;
   FTotalIn:        Integer;
   FTotalOut:       Integer;
   FStream:         TStream;
   FStartPosition:  Int64;
   FStreamPosition: Int64;
   FBuffer:         Pointer;
   FBufferSize:     Integer;
   function PackGetRate: Single;
   function UnpackGetRate: Single;
  private
   FOnProgress:     TNotifyEvent;
  protected
   property OnProgress: TNotifyEvent read FOnProgress write FOnProgress;

   procedure SetStream(AStream: TStream); virtual;
   procedure Progress(Sender: TObject); virtual;
   procedure Reset; virtual; abstract;
  public
   property WorkStream: TStream read FStream;
   property SourcePosition: Int64 read FStartPosition;

   constructor Create(AStream: TStream; BufSize: Integer = PackBufSize);
   destructor Destroy; override;
 end;

 TCustomPackStream = class(TCustomPackUnpackStream)
  private
   FWriteLimit: Integer;
  protected
   FState:        Integer;
   FMinOut:       Word;
   FErrorMessage: String;
   FBytesWritten: Integer;
   FFinalWrite:   Boolean;
   function  Compress: Integer; virtual; abstract;
   procedure FinishCompression; virtual; abstract;
   function  WriteHeader: Integer; virtual;
   function  GetSize: Int64; override;
   procedure GrowBuffer(Size: Integer);
   procedure OutputByte(Value: Byte);
   procedure OutputWord(Value: Word);      
  public
   property ErrorMessage: String read FErrorMessage;
   property State: Integer read FState write FState;
   property CompressedSize: Integer read FTotalOut;
   property BytesWritten: Integer read FBytesWritten;
   property CompressionRate: Single read PackGetRate;
   property OnProgress;

   constructor Create(Dest: TStream; AWriteLimit: Integer = 0);
   destructor  Destroy; override;
   procedure   Reset; override;
   function    Seek(Offset: Integer; Origin: Word): Integer; override;
   function    Read(var Buffer; Count: Integer): Integer; override;
   function    Write(const Buffer; Count: Integer): Integer; override;
   procedure   SetSize(NewSize: Integer); override;
   procedure   Finalize;
 end;

 TCustomPackStreamML = class(TCustomPackStream)
  private
   FMatchLimit:   Integer;
   FStringBuf:    array of Byte;
  protected
   FSrcPtr:       PByte;
   FMatchLength:  Integer;
   FRemainInsert: Integer;
   function  Compress: Integer; override;
   procedure SearchState; virtual; abstract;
   procedure WriteState; virtual; abstract;
   function  UserState: Boolean; virtual;
   procedure FinishCompression; override;
  public
   constructor Create(Dest: TStream; AMatchLimit: Integer;
                                     AWriteLimit: Integer = 0);
   procedure Reset; override;
 end;

 TCustomLZ77_PackStream = class(TCustomPackStreamML)
  protected
   FRingPosition: Integer;
   RING_MAX: Integer;
   FSearchPosition: Integer;
   FSearchLimit: Integer;
   FStartRingPos: Integer;
   FPrevByte: Byte;
   FRingSaveByte: Byte;
   FMin2: Boolean;
   FMatchMax: Integer;
   RING_SIZE: Integer;
   FRingShift: Byte;   
   FRingBuffer: array of Byte;
   FHashTable: array of Byte;
   FBackStep: array of Word;
   FPairLinks: array[Word] of Word;
   procedure SearchState; override;
   procedure WriteState; override;
  public
   constructor Create(Dest: TStream; AMatchMax: Word;
                                     ARingPos: Word = $FEE;
                                     ARingShift: Byte = RING_SHIFT;
                                     AMin2: Boolean = False;
                                     ASearchLimit: Word = 0;
                                     AWriteLimit: Integer = 0);
   procedure Reset; override;
 end;

 TCustomRLE_PackStream = class(TCustomPackStreamML)
  protected
   procedure SearchState; override;
 end;

 TCustomUnpackStream = class(TCustomPackUnpackStream)
  protected
   FState:        Integer;
   FOutputSize:   Integer;
   FErrorMessage: String;
   FTotalRemain:  Integer;
   FDefaultOutputSize: Integer;
   function  DecompressStep: Boolean; virtual; abstract;
   function  ReadHeader: Integer; virtual;
   function  GetSize: Int64; override;
   procedure SetStream(AStream: TStream); override;
   function  InputByte: Byte;
   procedure OutputByte(Value: Byte);
   function  CopyBlock(Size: Integer): Integer;
   function  FillBlock(Value: Byte; Count: Integer): Integer;
  private
   FNoBuffer: Boolean;
   function  Decompress: Integer;
   procedure SetNoBuffer(Value: Boolean);
  public
   property NoBuffer: Boolean read FNoBuffer write SetNoBuffer;
   property ErrorMessage: String read FErrorMessage;
   property CompressedSize: Integer read FTotalIn;
   property CompressionRate: Single read UnpackGetRate;
   property OnProgress;

   function  Read(var Buffer; Count: Integer): Integer; override;
   function  Write(const Buffer; Count: Integer): Integer; override;
   function  Seek(Offset: Integer; Origin: Word): Integer; override;
   procedure SetSize(NewSize: Integer); override;
   procedure Reset; override;
   procedure SetPos(Offset: Integer); virtual;

   destructor Destroy; override;   
 end;

 TRingBuffer = array of Byte;

 TCustomRingedUnpackStream = class(TCustomUnpackStream)
  protected
   FRingPosition:   Integer;
   FRingCopyOffset: Integer;
   FStartRingPos:   Integer;
   FRingBuffer:     TRingBuffer;
   FRingSize:       Integer;
   FRingShift:      Byte;
   FBufFiller:      Byte;
   function  RingCopy(Size: Integer): Integer;
   procedure OutputByte(Value: Byte);
  public
   property RingBuffer: TRingBuffer read FRingBuffer;

   constructor Create(Source: TStream; RingPos: Integer = 0;
                      RingShift: Byte = RING_SHIFT);
   procedure Reset; override;
   procedure SetPos(Offset: Integer); override;   
 end;

 TUnpackStreamClass = class of TCustomUnpackStream;
 TPackStreamClass = class of TCustomPackStream;

 (* Huffman compression utils *)

 TBitStream = packed record
  Bits: Cardinal;
  Size: Integer;
 end;

 TBitStreams = array of TBitStream;

 TDirection = (diNone, diLeft, diRight);

 PHuffTreeNode = ^THuffTreeNode;
 THuffTreeNode = packed record
  Direction: TDirection;
  Value:     Integer;
  Count:     Cardinal;
  Position:  Integer;
  Left:      PHuffTreeNode;
  Right:     PHuffTreeNode;
  Next:      PHuffTreeNode;
 end;

 TBranches = array of packed record
  Level: Integer;
  Node:  PHuffTreeNode;
 end;

 THuffmanTree = class
  private
   FRoot:           PHuffTreeNode;
   FCount:          Integer;
   FLeavesCount:    Integer;
   FBitStreams:     TBitStreams;
   FBranches:       TBranches;
   FTotalBitsCount: Int64;
   function AddNode: PHuffTreeNode;
   function Build: Boolean;
   function GetTotalBytes: Integer;
  public
   property Trunk: PHuffTreeNode read FRoot;
   property Branches: TBranches read FBranches;
   property BitStreams: TBitStreams read FBitStreams;
   property TotalBitsCount: Int64 read FTotalBitsCount;
   property TotalBytesCount: Integer read GetTotalBytes;
   property LeavesCount: Integer read FLeavesCount;

   constructor Create(const FreqList: array of Cardinal);
   destructor  Destroy; override;
 end;

(* Exception classes *)

 ECompressionError = class(Exception);
 EDecompressionError = class(Exception);

const
 SInvalidStreamOperation: PChar = 'Invalid stream operation.';
 SHeaderIsInvalid: PChar = 'Header is invalid.'; 

implementation

{ TCustomPackUnpackStream }

constructor TCustomPackUnpackStream.Create(AStream: TStream;
  BufSize: Integer);
begin
 FBufferSize := BufSize;
 SetStream(AStream);
 Reset;
 if (FBufferSize > 0) and (FBuffer = nil) then
  GetMem(FBuffer, FBufferSize);
end;

destructor TCustomPackUnpackStream.Destroy;
begin
 if FBuffer <> NIL then
  FreeMem(FBuffer);
 inherited;
end;

function TCustomPackUnpackStream.PackGetRate: Single;
var
 Stream: TCustomPackUnpackStream;
 TotalIn: Integer;
begin
 Result := 0.0;
 Stream := Self;
 TotalIn := FTotalIn;
 while Stream.FStream is TCustomPackStream do
  Stream := TCustomPackStream(Stream.FStream);
 with Stream do
 begin
  if TotalIn > 0 then
   Result := (1.0 - (FTotalOut / TotalIn)) * 100.0;
 end;
end;

procedure TCustomPackUnpackStream.Progress(Sender: TObject);
begin
 if Assigned(FOnProgress) then FOnProgress(Sender);
end;

procedure TCustomPackUnpackStream.SetStream(AStream: TStream);
begin
 FStream := AStream;
 FStartPosition := AStream.Position;
 FStreamPosition := FStartPosition;
end;

function TCustomPackUnpackStream.UnpackGetRate: Single;
var
 Stream: TCustomPackUnpackStream;
 TotalOut: Integer;
begin
 Result := 0.0;
 Stream := Self;
 TotalOut := FTotalOut;
 while Stream.FStream is TCustomUnpackStream do
  Stream := TCustomUnpackStream(Stream.FStream);
 with Stream do
 begin
  if TotalOut > 0 then
   Result := (1.0 - (FTotalIn / TotalOut)) * 100.0;
 end;
end;

{ TCustomPackStream }

constructor TCustomPackStream.Create(Dest: TStream; AWriteLimit: Integer);
begin
 FWriteLimit := AWriteLimit;
 inherited Create(Dest, 0);
end;

destructor TCustomPackStream.Destroy;
begin
 try
  try
   Finalize;
  finally
   inherited;
  end;
 except
  on E: Exception do
  begin
   FState := PS_ERROR;
   FErrorMessage := E.Message;
   Progress(Self);
  end;
 end;
end;

procedure TCustomPackStream.Finalize;
var
 B, Count: Integer;
 P: PByte;
begin
 if (FState <> PS_FINISHED) and
    (FState <> PS_ERROR) then
 begin
  FFinalWrite := True;
  FinishCompression;
  FStream.Position := FStartPosition;
  Count := FTotalOut;
  Inc(FTotalOut, WriteHeader);
  FState := PS_BUFWRITE;
  FBytesWritten := 0;
  Progress(Self);
  P := FBuffer;
  for B := 1 to Count div $8000 do
  begin
   FStream.WriteBuffer(P^, $8000);
   Inc(P, $8000);
   Inc(FBytesWritten, $8000);
   Progress(Self);
  end;
  Count := Count and $7FFF;
  if Count > 0 then
  begin
   FStream.WriteBuffer(P^, Count);
   Inc(FBytesWritten, Count);
   Progress(Self);   
  end;
  if FStream is TCustomPackStream then
   TCustomPackStream(FStream).Finalize;
  FState := PS_FINISHED;
  Progress(Self);
  FBufferSize := 0;
  FreeMem(FBuffer);
  FBuffer := nil;
 end;
end;

function TCustomPackStream.GetSize: Int64;
begin
 Result := FTotalIn;
end;

procedure TCustomPackStream.GrowBuffer(Size: Integer);
var
 L: Integer;
begin
 if FRemainOut < FMinOut then
 begin
  L := Integer(FOutput) - Integer(FBuffer);
  Inc(FBufferSize, Size + FMinOut);
  ReallocMem(FBuffer, FBufferSize);
  FOutput := FBuffer;
  Inc(FOutput, L);
  FRemainOut := Size + FMinOut;
  Progress(Self);  
 end;
end;

procedure TCustomPackStream.OutputByte(Value: Byte);
begin
 FOutput^ := Value;
 Inc(FOutput);
 Inc(FTotalOut);
 Dec(FRemainOut);
end;

procedure TCustomPackStream.OutputWord(Value: Word);
begin
 PWord(FOutput)^ := Value;
 Inc(FOutput, 2);
 Inc(FTotalOut, 2);
 Dec(FRemainOut, 2);
end;

function TCustomPackStream.Read(var Buffer; Count: Integer): Integer;
begin
 FState := PS_ERROR;
 FErrorMessage := SInvalidStreamOperation;
 raise ECompressionError.Create(FErrorMessage);
end;

procedure TCustomPackStream.Reset;
begin
 FRemainOut := 0;
 if FBufferSize > 0 then
 begin
  FreeMem(FBuffer);
  FBufferSize := 0;
 end;
 FTotalIn := 0;
 FTotalOut := 0;
 FState := PS_PREPARE;
 FMinOut := 1;
 FFinalWrite := False;
end;

function TCustomPackStream.Seek(Offset: Integer; Origin: Word): Integer;
begin
 case Origin of
  soFromBeginning: Result := Offset;
  soFromCurrent,
  soFromEnd: Result := FTotalIn + Offset;
  else Result := FTotalIn;
 end;
 if Result < 0 then
 begin
 // unable to set position lower then zero
  FState := PS_ERROR;
  FErrorMessage := SInvalidStreamOperation;
  raise ECompressionError.Create(FErrorMessage);
 end else
 if (Result = 0) and (FTotalIn > 0) then
  Reset (* Restarting compression *) else
  SetSize(Result);
end;

procedure TCustomPackStream.SetSize(NewSize: Integer);
var
 Count, SZ: Integer;
 Buffer: array[0..2047] of Byte;
begin
 if NewSize = FTotalIn then Exit else
 if NewSize > FTotalIn then
 begin
  FillChar(Buffer, SizeOf(Buffer), 0);
  Count := NewSize - FTotalIn;
  while Count > 0 do
  begin
   if Count >= SizeOf(Buffer) then
    SZ := SizeOf(Buffer) else
    SZ := Count;
   WriteBuffer(Buffer, SZ);
   Dec(Count, SZ);
  end;
 end else
 begin
  FState := PS_ERROR;
  FErrorMessage := SInvalidStreamOperation;
  raise ECompressionError.Create(FErrorMessage);
 end;
end;

function TCustomPackStream.Write(const Buffer; Count: Integer): Integer;
begin
 if (FState = PS_FINISHED) or (FState = PS_ERROR) then
 begin
  Result := 0;
  Exit;
 end;
 FInput := @Buffer;
 if FWriteLimit > 0 then
 begin
  if FTotalIn + Count > FWriteLimit then
   Count := FWriteLimit - FTotalIn;
 end;
 FRemainIn := Count;
 Result := 0;
 try
  while FRemainIn > 0 do
  begin
   GrowBuffer(PackBufSize - 1);
   Inc(Result, Compress);
  end;
 except
  on E: Exception do
  begin
   FState := PS_ERROR;
   FErrorMessage := E.Message;
   Progress(Self);
   raise;
  end;
 end;
end;

function TCustomPackStream.WriteHeader: Integer;
begin
 Result := 0;
end;

{ TCustomPackStreamML }

function TCustomPackStreamML.Compress: Integer;
var
 RemainSave, ML: Integer;
begin
 RemainSave := FRemainIn;
 repeat
  case FState of
   PS_ML_INIT:
   begin
    if FRemainIn < FMatchLimit then
    begin
     if FRemainInsert > 0 then
     begin
      Move(FStringBuf[FMatchLength], FStringBuf[0], FRemainInsert);
      FMatchLength := FRemainInsert;
      ML := FMatchLimit - FRemainInsert;
      if ML > FRemainIn then ML := FRemainIn;
      FSrcPtr := Addr(FStringBuf[FRemainInsert]);
      if ML > 0 then
      begin
       Move(FInput^, FSrcPtr^, ML);
       Inc(FSrcPtr, ML);
       Inc(FMatchLength, ML);
       Inc(FInput, ML);
       Inc(FTotalIn, ML);
       Dec(FRemainIn, ML);
      end;
     end else
     begin
      if FRemainIn <= 0 then Break; 
      FMatchLength := FRemainIn;
      FRemainIn := 0;
      FSrcPtr := Pointer(FStringBuf);
      Move(FInput^, FSrcPtr^, FMatchLength);
      Inc(FSrcPtr, FMatchLength);
      Inc(FInput, FMatchLength);
      Inc(FTotalIn, FMatchLength);
     end;
     if (FMatchLength < FMatchLimit) and not FFinalWrite then
     begin
      FState := PS_ML_FILL_LIMIT;
      Break;
     end;
    end else if FRemainInsert > 0 then
    begin
     Move(FStringBuf[FMatchLength], FStringBuf[0], FRemainInsert);
     ML := FMatchLimit - FRemainInsert;
     FMatchLength := FRemainInsert;
     if ML > 0 then
     begin
      Move(FInput^, FStringBuf[FRemainInsert], ML);
      Inc(FInput, ML);
      Inc(FTotalIn, ML);
      Dec(FRemainIn, ML);
      Inc(FMatchLength, ML);
     end;
    end else
    begin
     FMatchLength := FMatchLimit;
     Move(FInput^, FStringBuf[0], FMatchLength);
     Inc(FInput, FMatchLength);
     Inc(FTotalIn, FMatchLength);
     Dec(FRemainIn, FMatchLength);
    end;
    FSrcPtr := Pointer(FStringBuf);
    FRemainInsert := FMatchLength;
    FState := PS_ML_SEARCH;
   end;
   PS_ML_FILL_LIMIT:
   begin
    if FMatchLength + FRemainIn >= FMatchLimit then
    begin
     ML := FMatchLimit - FMatchLength;
     if ML > 0 then
     begin
      Move(FInput^, FSrcPtr^, ML);
      Inc(FInput, ML);
      Inc(FTotalIn, ML);
      Dec(FRemainIn, ML);
     end;
     FMatchLength := FMatchLimit;
    end else if FRemainIn > 0 then
    begin
     Inc(FMatchLength, FRemainIn);
     Move(FInput^, FSrcPtr^, FRemainIn);
     Inc(FInput, FRemainIn);
     Inc(FTotalIn, FRemainIn);
     if not FFinalWrite then
     begin
      Inc(FSrcPtr, FRemainIn);
      FRemainIn := 0;
      Break;
     end else FRemainIn := 0;
    end else if not FFinalWrite then Break;
    FRemainInsert := FMatchLength;
    FSrcPtr := Pointer(FStringBuf);
    FState := PS_ML_SEARCH;
   end;
   PS_ML_SEARCH:
   begin
    FState := PS_ML_WRITE;
    SearchState;
   end;
   PS_ML_WRITE:
   begin
    if FRemainOut < FMinOut then Break;
    FState := PS_ML_INIT;
    WriteState;
    Dec(FRemainInsert, FMatchLength);    
   end;
   else if not UserState then Break;
  end;
 until False;
 Result := RemainSave - FRemainIn;
end;

constructor TCustomPackStreamML.Create(Dest: TStream; AMatchLimit,
  AWriteLimit: Integer);
begin
 FMatchLimit := AMatchLimit;
 SetLength(FStringBuf, FMatchLimit);
 inherited Create(Dest, AWriteLimit);
end;

procedure TCustomPackStreamML.FinishCompression;
begin
 if FTotalIn > 0 then
 begin
  GrowBuffer(63);
  Compress;
  while FRemainInsert > 0 do
  begin
   GrowBuffer(63);
   Compress;
  end;
 end;
end;

procedure TCustomPackStreamML.Reset;
begin
 inherited;
 FMatchLength := 0;
 FRemainInsert := 0;
end;

function TCustomPackStreamML.UserState: Boolean;
begin
 Result := False;
end;

{ TCustomLZ77_PackStream }

constructor TCustomLZ77_PackStream.Create(Dest: TStream;
  AMatchMax, ARingPos: Word; ARingShift: Byte; AMin2: Boolean;
  ASearchLimit: Word; AWriteLimit: Integer);
begin
 FStartRingPos := ARingPos;
 FRingShift := ARingShift and 15;
 FMin2 := AMin2;
 FSearchLimit := 1 shl FRingShift;
 if (ASearchLimit > 0) and (ASearchLimit < FSearchLimit) then
  FSearchLimit := ASearchLimit;
 FMatchMax := AMatchMax;
 inherited Create(Dest, AMatchMax + 1, AWriteLimit);
end;

procedure TCustomLZ77_PackStream.Reset;
begin
 RING_SIZE := 1 shl FRingShift;
 RING_MAX := RING_SIZE - 1;
 System.Finalize(FRingBuffer);
 System.Finalize(FHashTable);
 System.Finalize(FBackStep);
 SetLength(FRingBuffer, RING_SIZE);
 SetLength(FHashTable, RING_SIZE);
 SetLength(FBackStep, RING_SIZE);
 FRingPosition := FStartRingPos and RING_MAX;
 FillWord(FPairLinks[0], 65536, $FFFF);
 FillWord(FBackStep[0], RING_SIZE, RING_SIZE);
 inherited;
end;

procedure TCustomLZ77_PackStream.SearchState;
var
 Src, SrcSave: PByte;
 B1, Hash, Hash2: Byte;
 PosSave, SrcLen, Limit, Index, MaxLen, Len, MinLimit: Integer;
begin
 Src := FSrcPtr;
 SrcLen := FRemainInsert;
 FMatchLength := 0;
 FRingSaveByte := FRingBuffer[(FRingPosition + 1) and RING_MAX];
 if SrcLen > 1 then
 begin
  B1 := Src^;
  SrcSave := Src;
  Inc(SrcSave);
  Index := (SrcSave^ shl 8) or B1;
  PosSave := FPairLinks[Index];
  if PosSave < RING_SIZE then
  begin
   FSearchPosition := PosSave;
   PosSave := (FRingPosition - PosSave) and RING_MAX;
   if PosSave = 0 then
    FBackStep[FRingPosition] := RING_SIZE else
    FBackStep[FRingPosition] := PosSave;
  end else
  begin
   FSearchPosition := RING_SIZE;
   FBackStep[FRingPosition] := RING_SIZE;
  end;
  FPairLinks[Index] := FRingPosition;
  FHashTable[(FRingPosition - 1) and RING_MAX] :=
    FPrevByte * 7 + B1 * 5 + SrcSave^ * 3;
  FPrevByte := B1;
  B1 := SrcSave^;
  if (FSearchPosition < RING_SIZE) and (FMin2 or (SrcLen >= 3)) then
  begin
   if SrcLen >= 3 then
    Hash := FPrevByte * 7 + B1 * 5 + PByte(Integer(Src) + 2)^ * 3 else
    Hash := 0;
   if SrcLen >= 6 then
    Hash2 := PByte(Integer(Src) + 3)^ * 7 +
             PByte(Integer(Src) + 4)^ * 5 +
             PByte(Integer(Src) + 5)^ * 3 else
    Hash2 := 0;
   PosSave := FSearchPosition;
   if SrcLen < FMatchMax then
    MaxLen := SrcLen else
    MaxLen := FMatchMax;
   Limit := RING_SIZE;
   MinLimit := Limit - FSearchLimit;
   Dec(Limit, (FRingPosition - PosSave) and RING_MAX);
   repeat
    if FMin2 then
    begin
     while (Limit >= MinLimit) and
     (((FMatchLength >= 3) and (FHashTable[PosSave] <> Hash)) or
 ((FMatchLength >= 6) and (FHashTable[(PosSave + 3) and RING_MAX] <> Hash2))) do
     begin
      Len := FBackStep[PosSave];
      Dec(Limit, Len);
      PosSave := (PosSave - Len) and RING_MAX;
     end;
    end else
    begin
     while (Limit >= MinLimit) and ((FHashTable[PosSave] <> Hash) or
 ((FMatchLength >= 6) and (FHashTable[(PosSave + 3) and RING_MAX] <> Hash2))) do
     begin
      Len := FBackStep[PosSave];
      Dec(Limit, Len);
      PosSave := (PosSave - Len) and RING_MAX;
     end;
    end;
    if Limit < MinLimit then Break;
    SrcSave := Src;
    Len := 0;
    while (Len < MaxLen) and
          (SrcSave^ = FRingBuffer[(PosSave + Len) and RING_MAX]) do
    begin
     FRingBuffer[(FRingPosition + Len) and RING_MAX] := SrcSave^;
     Inc(SrcSave);
     Inc(Len);
    end;
    if Len > FMatchLength then
    begin
     FSearchPosition := PosSave;
     FMatchLength := Len;
     if Len > MinLimit then MinLimit := Len;
    end;
    if Len = MaxLen then Break;
    Len := FBackStep[PosSave];
    Dec(Limit, Len);
    PosSave := (PosSave - Len) and RING_MAX;
   until False;
  end;
 end;
end;

procedure TCustomLZ77_PackStream.WriteState;
var
 Src: PByte;
 I, Index, PosSave: Integer;
 B1: Byte;
begin
 if FMatchLength >= 3 - Ord(FMin2) then
 begin
  Src := FSrcPtr;
  FPrevByte := Src^;
  Inc(Src);
  B1 := Src^;
  Inc(Src);
  for I := 2 to FMatchLength do
  begin
   FHashTable[FRingPosition] := FPrevByte * 7 + B1 * 5 + Src^ * 3;
   FRingPosition := (FRingPosition + 1) and RING_MAX;
   Index := (Src^ shl 8) or B1;
   PosSave := FPairLinks[Index];
   if PosSave < RING_SIZE then
   begin
    PosSave := (FRingPosition - PosSave) and RING_MAX;
    if PosSave = 0 then
     FBackStep[FRingPosition] := RING_SIZE else
     FBackStep[FRingPosition] := PosSave;
   end else
    FBackStep[FRingPosition] := RING_SIZE;
   FPairLinks[Index] := FRingPosition;
   FPrevByte := B1;
   B1 := Src^;
   Inc(Src);
  end;
  FRingPosition := (FRingPosition + 1) and RING_MAX;
 end else
 begin
  FRingBuffer[FRingPosition] := FSrcPtr^;
  FRingPosition := (FRingPosition + 1) and RING_MAX;
  FRingBuffer[FRingPosition] := FRingSaveByte;
  FMatchLength := 1;
 end;
end;

{ TCustomRLE_PackStream }

procedure TCustomRLE_PackStream.SearchState;
var
 SrcLen: Integer;
 Src: PByte;
 B: Byte;
begin
 Src := FSrcPtr;
 SrcLen := FRemainInsert;
 B := Src^;
 repeat
  Inc(Src);
  Dec(SrcLen);
 until (SrcLen = 0) or (Src^ <> B);
 FMatchLength := FRemainInsert - SrcLen;
end;

{ TCustomUnpackStream }

function TCustomUnpackStream.CopyBlock(Size: Integer): Integer;
begin
 if FRemainOut > Size then
   Result := Size else
   Result := FRemainOut;
  if Result > FRemainIn then Result := FRemainIn;
  if Result > FTotalRemain then Result := FTotalRemain;
 if Result <= 0 then Exit;
 Move(FInput^, FOutput^, Result);
 Inc(FInput, Result);
 Inc(FOutput, Result);
 Inc(FTotalIn, Result);
 Inc(FTotalOut, Result);
 Dec(FRemainIn, Result);
 Dec(FRemainOut, Result);
 Dec(FTotalRemain, Result);
end;

function TCustomUnpackStream.Decompress: Integer;
var
 RemainSave: Integer;
begin
 RemainSave := FRemainOut;
 while (FTotalRemain > 0) and (FRemainOut > 0) and DecompressStep do;
 if FTotalRemain <= 0 then FState := US_FINISHED;
 Result := RemainSave - FRemainOut;
end;

destructor TCustomUnpackStream.Destroy;
begin
 FStream.Position := FStartPosition + CompressedSize;
 inherited;
end;

function TCustomUnpackStream.FillBlock(Value: Byte;
  Count: Integer): Integer;
begin
 if FRemainOut > Count then
  Result := Count else
  Result := FRemainOut;
 if Result > FTotalRemain then Result := FTotalRemain;
 if Result <= 0 then Exit;
 FillChar(FOutput^, Result, Value);
 Inc(FOutput, Result);
 Inc(FTotalOut, Result);
 Dec(FTotalRemain, Result);
 Dec(FRemainOut, Result);
end;

function TCustomUnpackStream.GetSize: Int64;
begin
 Result := FOutputSize;
end;

function TCustomUnpackStream.InputByte: Byte;
begin
 Result := FInput^;
 Inc(FInput);
 Inc(FTotalIn);
 Dec(FRemainIn);
end;

procedure TCustomUnpackStream.OutputByte(Value: Byte);
begin
 FOutput^ := Value;
 Inc(FOutput);
 Inc(FTotalOut);
 Dec(FTotalRemain);
 Dec(FRemainOut);
end;

function TCustomUnpackStream.Read(var Buffer; Count: Integer): Integer;
begin
 FOutput := @Buffer;
 FRemainOut := Count;
 Result := 0;
 while (FRemainOut > 0) and (FState <> US_FINISHED) and (FState <> US_ERROR) do
 begin
  if FRemainIn = 0 then
  begin
   if FNoBuffer then
   begin
    FInput := TCustomMemoryStream(FStream).Memory;
    FRemainIn := FStream.Size - FStreamPosition;
    Inc(FInput, FStreamPosition);
   end else
   begin
    FStream.Position := FStreamPosition;
    FRemainIn := FStream.Read(FBuffer^, FBufferSize);
    FInput := FBuffer;
   end;
   if FRemainIn = 0 then Exit;
   Inc(FStreamPosition, FRemainIn);
   Progress(Self);
  end;
  Inc(Result, Decompress);
 end;
end;

function TCustomUnpackStream.ReadHeader: Integer;
begin
 if FDefaultOutputSize <= 0 then
  FOutputSize := MaxInt else
  FOutputSize := FDefaultOutputSize;
 Result := 0;
end;

procedure TCustomUnpackStream.Reset;
begin
 FState := US_PREPARE;
 FStream.Position := FStartPosition;
 FTotalIn := ReadHeader;
 if FTotalIn < 0 then raise EDecompressionError.Create(SHeaderIsInvalid);
 FTotalRemain := FOutputSize;
 FStreamPosition := FStartPosition + FTotalIn;
 FTotalOut := 0;
 FRemainIn := 0;
 if FTotalRemain = 0 then FState := US_FINISHED; 
end;

function TCustomUnpackStream.Seek(Offset: Integer; Origin: Word): Integer;
begin
 case Origin of
  soFromBeginning: Result := Offset;
  soFromCurrent: Result := FTotalOut + Offset;
  soFromEnd: Result := FOutputSize + Offset;
  else Result := FTotalOut;
 end;
 if (Result < 0) or (Result > FOutputSize) then
 begin
 // unable to set position lower then zero or
 // greater then decompressed data size
  FState := US_ERROR;
  FErrorMessage := SInvalidStreamOperation;
  raise EDecompressionError.Create(FErrorMessage);
 end else
 if Result > FTotalOut then SetPos(Result) else
 if Result < FTotalOut then
 begin
  Reset; (* Restarting decompression *)
  SetPos(Result);
 end;
end;

procedure TCustomUnpackStream.SetNoBuffer(Value: Boolean);
begin
 if Value <> FNoBuffer then
 begin
  FNoBuffer := Value;
  Dec(FStreamPosition, FRemainIn);
  if not Value then
  begin
   if FBufferSize <= 0 then
   begin
    GetMem(FBuffer, PackBufSize);
    FBufferSize := PackBufSize;
   end;
  end else
  begin
   if (FBufferSize > 0) and (FBuffer <> nil) then
   begin
    FreeMem(FBuffer);
    FBuffer := nil;
   end;
   FBufferSize := 0;
  end;
  FRemainIn := 0;
 end;
end;

procedure TCustomUnpackStream.SetPos(Offset: Integer);
var
 Remain, L: Integer;
 Buf: array[0..1023] of Byte;
begin
 Remain := Offset - FTotalOut;
 while Remain > 0 do
 begin
  if Remain >= SizeOf(Buf) then
   L := SizeOf(Buf) else
   L := Remain;
  ReadBuffer(Buf[0], L);
  Dec(Remain, L);
 end;
end;

procedure TCustomUnpackStream.SetSize(NewSize: Integer);
begin
 FDefaultOutputSize := NewSize;
 Reset;
end;

procedure TCustomUnpackStream.SetStream(AStream: TStream);
begin
 inherited;
 FRemainIn := 0;
 SetNoBuffer(AStream is TCustomMemoryStream);
end;

function TCustomUnpackStream.Write(const Buffer; Count: Integer): Integer;
begin
 FState := US_ERROR;
 FErrorMessage := SInvalidStreamOperation;
 raise EDecompressionError.Create(FErrorMessage);
end;

{ TCustomRingedUnpackStream }

constructor TCustomRingedUnpackStream.Create(Source: TStream;
  RingPos: Integer; RingShift: Byte);
begin
 FRingShift := RingShift;
 FStartRingPos := RingPos;
 inherited Create(Source);
end;

procedure TCustomRingedUnpackStream.OutputByte(Value: Byte);
begin
 FOutput^ := Value;
 Inc(FOutput);
 Inc(FTotalOut);
 Dec(FTotalRemain);
 Dec(FRemainOut);
 FRingBuffer[FRingPosition] := Value;
 FRingPosition := (FRingPosition + 1) and (FRingSize - 1);
end;

procedure TCustomRingedUnpackStream.Reset;
begin
 inherited;
 FRingSize := 1 shl FRingShift;
 SetLength(FRingBuffer, FRingSize);
 FillChar(FRingBuffer[0], FRingSize, FBufFiller);
 FRingPosition := FStartRingPos and (FRingSize - 1);
end;

function TCustomRingedUnpackStream.RingCopy(Size: Integer): Integer;
var
 RING_MAX, Z: Integer;
 B: Byte;
begin
 if FRemainOut > Size then
  Result := Size else
  Result := FRemainOut;
 if Result > FTotalRemain then
  Result := FTotalRemain;
 if Result <= 0 then Exit;
 RING_MAX := FRingSize - 1;
 for Z := 0 to Result - 1 do
 begin
  B := FRingBuffer[FRingCopyOffset];
  FRingBuffer[FRingPosition] := B;
  FOutput^ := B;
  Inc(FOutput);
  FRingCopyOffset := (FRingCopyOffset + 1) and RING_MAX;
  FRingPosition := (FRingPosition + 1) and RING_MAX;
 end;
 Inc(FTotalOut, Result);
 Dec(FRemainOut, Result);
 Dec(FTotalRemain, Result);
end;

procedure TCustomRingedUnpackStream.SetPos(Offset: Integer);
var
 Remain, L: Integer;
begin
 Remain := Offset - FTotalOut;
 L := FRingSize - FRingPosition;
 while Remain > 0 do
 begin
  if L > Remain then L := Remain;
  ReadBuffer(FRingBuffer[FRingPosition], L);
  Dec(Remain, L);
  L := FRingSize;
 end;
end;

{ THuffmanTree }

function THuffmanTree.AddNode: PHuffTreeNode;
begin
 New(Result);
 FillChar(Result^, SizeOf(THuffTreeNode), 0);
 Result.Value := -1;
 Result.Next := FRoot;
 FRoot := Result;
 Inc(FCount);
end;

function THuffmanTree.Build: Boolean;
(* THuffmanTree.Build local functions *)
 function FindMin: PHuffTreeNode;
 var
  I: Cardinal;
  P, N: PHuffTreeNode;
 begin
  I := High(Cardinal);
  N := FRoot;
  P := NIL;
  while N <> NIL do with N^ do
  begin
   if I > Count then
   begin
    I := Count;
    P := N;
   end;
   N := Next;
  end;
  N := FRoot;
  if N = P then
  begin
   FRoot := N.Next;
   P.Next := NIL;
   Dec(FCount);
   Result := P;
   Exit;
  end;
  while N <> NIL do
  begin
   if P = N.Next then
   begin
    N.Next := P.Next;
    P.Next := NIL;
    Dec(FCount);
    Result := P;
    Exit;
   end;
   N := N.Next;
  end;
  Result := NIL;
 end;
(* End of THuffmanTree.Build local functions *)
 var
  A, B: PHuffTreeNode;
(* THuffmanTree.Build *)
begin
 A := FindMin;
 B := FindMin;
 if B = NIL then
 begin
  FRoot := A;
  Result := False;
 end else
 begin
  Result := FRoot <> NIL;
  with AddNode^ do
  begin
   Left := B;
   Right := A;
   Left.Direction := diLeft;
   Right.Direction := diRight;
   Value := -1;
   Count := A.Count + B.Count;
  end;
 end;
end;

constructor THuffmanTree.Create(const FreqList: array of Cardinal);
(* THuffmanTree.Create local functions *)
 procedure FillBitStream(Node: PHuffTreeNode; SZ: Integer; B: Cardinal);
 begin
  if Node = NIL then Exit;
  with Node^ do
  begin
   if Value >= 0 then with FBitStreams[Value] do
   begin
    Bits := B;
    Size := SZ;
    Inc(FTotalBitsCount, Int64(SZ) * Int64(FreqList[Value]));
   end;
   if (Left <> NIL) and (Right <> NIL) then
   begin
    FillBitStream(Left, SZ + 1, B + B);
    FillBitStream(Right, SZ + 1, (B + B) or 1);
   end;
  end;
 end; (* FillBitStream *)

 procedure FillList(N: PHuffTreeNode; LV: Integer);
 var
  L: Integer;
 begin
  if N <> NIL then with N^ do if (Left <> NIL) and (Right <> NIL) then
  begin
   L := Length(FBranches);
   SetLength(FBranches, L + 1);
   with FBranches[L] do
   begin
    Node := N;
    Level := LV;
    FillList(Left, LV + 1);
    FillList(Right, LV + 1);
   end;
  end;
 end; (* FillList *)

 procedure SortList(L, R: Integer);
 var
  I, J, X, Y: Integer;
  TempNode: PHuffTreeNode;
 begin
  I := L;
  J := R;
  X := FBranches[(L + R) shr 1].Level;
  repeat
   while FBranches[I].Level < X do Inc(I);
   while X < FBranches[J].Level do Dec(J);
   if I <= J then
   begin
    if FBranches[I].Level <> FBranches[J].Level then
    begin
     with FBranches[I] do
     begin
      Y := Level;
      TempNode := Node;
     end;
     FBranches[I] := FBranches[J];
     with FBranches[J] do
     begin
      Level := Y;
      Node := TempNode;
     end;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then SortList(L, J);
  if I < R then SortList(I, R);
 end; (* SortList *)
(* End of local functions *)

 var
  I: Integer;
(* THuffmanTree.Create *)
begin
 FRoot := NIL;
 FCount := 0;
 for I := 0 to Length(FreqList) - 1 do
  if FreqList[I] > 0 then
   with AddNode^ do
   begin
    Value := I;
    Count := FreqList[I];
   end;
 FLeavesCount := FCount;
 if FCount > 0 then
 begin
  while Build do;
  SetLength(FBitStreams, Length(FreqList));
//  FillChar(FBitStreams[0], Length(FreqList) * SizeOf(TBitStream), 0);
  FTotalBitsCount := 0;
  FillBitStream(FRoot, 0, 0);
  FillList(FRoot, 1);
  SortList(0, Length(FBranches) - 1);
  for I := 0 to Length(FBranches) - 1 do
   with FBranches[I].Node^ do
   begin
    Left.Position := I;
    Right.Position := I;
   end;
  FRoot.Position := -1;
 end;
end;

destructor THuffmanTree.Destroy;
(* Local functions *)
 procedure FreeNodes(Node: PHuffTreeNode);
 begin
  if Node <> NIL then
  begin
   FreeNodes(Node.Left);
   FreeNodes(Node.Right);
   Dispose(Node.Left);
   Dispose(Node.Right);
  end;
 end; (* FreeNodes *)

(* End of local functions *)
 var
  N: PHuffTreeNode;
(* THuffmanTree.Destroy *)
begin
 while FRoot <> NIL do
 begin
  with FRoot^ do
  begin
   N := Next;
   FreeNodes(Left);
   Dispose(Left);
   FreeNodes(Right);
   Dispose(Right);
  end;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCount := 0;
 Finalize(FBranches);
 Finalize(FBitStreams);
 inherited;
end;

function THuffmanTree.GetTotalBytes: Integer;
begin
 Result := (FTotalBitsCount shr 3) + Integer(Byte(FTotalBitsCount) and 7 > 0);
end;

end.
