object MainForm: TMainForm
  Left = 212
  Top = 188
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Doom 3 Font Data Editor'
  ClientHeight = 453
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox: TListBox
    Left = 0
    Top = 0
    Width = 121
    Height = 165
    Style = lbOwnerDrawFixed
    ItemHeight = 16
    TabOrder = 0
    OnClick = ListBoxClick
  end
  object FileNameEdit: TLabeledEdit
    Left = 128
    Top = 24
    Width = 281
    Height = 21
    EditLabel.Width = 48
    EditLabel.Height = 13
    EditLabel.Caption = 'File name:'
    TabOrder = 1
  end
  object P1Edit: TLabeledEdit
    Left = 128
    Top = 64
    Width = 65
    Height = 21
    EditLabel.Width = 34
    EditLabel.Height = 13
    EditLabel.Caption = 'Height:'
    TabOrder = 2
  end
  object P2Edit: TLabeledEdit
    Left = 200
    Top = 64
    Width = 65
    Height = 21
    EditLabel.Width = 22
    EditLabel.Height = 13
    EditLabel.Caption = 'Top:'
    TabOrder = 3
  end
  object P3Edit: TLabeledEdit
    Left = 272
    Top = 64
    Width = 65
    Height = 21
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.Caption = 'Bottom:'
    TabOrder = 4
  end
  object P4Edit: TLabeledEdit
    Left = 344
    Top = 64
    Width = 65
    Height = 21
    EditLabel.Width = 27
    EditLabel.Height = 13
    EditLabel.Caption = 'Pitch:'
    TabOrder = 5
  end
  object P5Edit: TLabeledEdit
    Left = 128
    Top = 104
    Width = 65
    Height = 21
    EditLabel.Width = 32
    EditLabel.Height = 13
    EditLabel.Caption = 'X skip:'
    TabOrder = 6
  end
  object P6Edit: TLabeledEdit
    Left = 200
    Top = 104
    Width = 65
    Height = 21
    EditLabel.Width = 60
    EditLabel.Height = 13
    EditLabel.Caption = 'Image width:'
    TabOrder = 7
  end
  object P7Edit: TLabeledEdit
    Left = 272
    Top = 104
    Width = 65
    Height = 21
    EditLabel.Width = 64
    EditLabel.Height = 13
    EditLabel.Caption = 'Image height:'
    TabOrder = 8
  end
  object P8Edit: TLabeledEdit
    Left = 344
    Top = 104
    Width = 65
    Height = 21
    EditLabel.Width = 39
    EditLabel.Height = 13
    EditLabel.Caption = 'X offset:'
    TabOrder = 9
  end
  object P9Edit: TLabeledEdit
    Left = 128
    Top = 144
    Width = 65
    Height = 21
    EditLabel.Width = 39
    EditLabel.Height = 13
    EditLabel.Caption = 'Y offset:'
    TabOrder = 10
  end
  object P10Edit: TLabeledEdit
    Left = 200
    Top = 144
    Width = 65
    Height = 21
    EditLabel.Width = 50
    EditLabel.Height = 13
    EditLabel.Caption = 'X + Width:'
    TabOrder = 11
  end
  object P11Edit: TLabeledEdit
    Left = 272
    Top = 144
    Width = 65
    Height = 21
    EditLabel.Width = 53
    EditLabel.Height = 13
    EditLabel.Caption = 'Y + Height:'
    TabOrder = 12
  end
  object P12Edit: TLabeledEdit
    Left = 344
    Top = 144
    Width = 65
    Height = 21
    EditLabel.Width = 30
    EditLabel.Height = 13
    EditLabel.Caption = 'Glyph:'
    TabOrder = 13
  end
  object ImPanel: TPanel
    Left = 0
    Top = 168
    Width = 411
    Height = 285
    Align = alBottom
    BevelOuter = bvLowered
    BorderStyle = bsSingle
    Color = 4981760
    TabOrder = 14
    object Image: TImage
      Left = 1
      Top = 1
      Width = 405
      Height = 279
      Align = alClient
      Proportional = True
      Stretch = True
    end
  end
  object MainMenu: TMainMenu
    AutoHotkeys = maManual
    Left = 8
    Top = 48
    object FileMenu: TMenuItem
      Caption = 'File'
      object OpenItem: TMenuItem
        Action = OpenAction
      end
      object SaveItem: TMenuItem
        Caption = 'Save...'
        ShortCut = 16467
        OnClick = SaveItemClick
      end
      object ExitItem: TMenuItem
        Caption = 'Exit'
        OnClick = ExitItemClick
      end
    end
    object Edit1: TMenuItem
      Caption = 'Edit'
      object SaveData1: TMenuItem
        Action = SaveDataAction
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Fix1: TMenuItem
        Action = FixAction
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '.dat'
    Filter = 'Font Data (*.dat)|*.dat'
    Left = 8
    Top = 16
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '.dat'
    Filter = 'Font Data (*.dat)|*.dat'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 40
    Top = 16
  end
  object ActionList: TActionList
    Left = 40
    Top = 48
    object OpenAction: TAction
      Caption = 'Open...'
      ShortCut = 16463
      OnExecute = OpenItemClick
    end
    object SaveDataAction: TAction
      Caption = 'Save Data'
      ShortCut = 113
      OnExecute = SaveDataActionExecute
    end
    object FixAction: TAction
      Caption = 'Fix'
      ShortCut = 123
      OnExecute = FixActionExecute
    end
  end
end
