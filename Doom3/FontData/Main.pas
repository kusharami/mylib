unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Buttons, StdCtrls, ExtCtrls, Menus, ActnList, TBXGraphics;

type
  TMainForm = class(TForm)
    ListBox: TListBox;
    FileNameEdit: TLabeledEdit;
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    OpenItem: TMenuItem;
    SaveItem: TMenuItem;
    ExitItem: TMenuItem;
    P1Edit: TLabeledEdit;
    P2Edit: TLabeledEdit;
    P3Edit: TLabeledEdit;
    P4Edit: TLabeledEdit;
    P5Edit: TLabeledEdit;
    P6Edit: TLabeledEdit;
    P7Edit: TLabeledEdit;
    P8Edit: TLabeledEdit;
    P9Edit: TLabeledEdit;
    P10Edit: TLabeledEdit;
    P11Edit: TLabeledEdit;
    P12Edit: TLabeledEdit;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    ActionList: TActionList;
    OpenAction: TAction;
    Image: TImage;
    ImPanel: TPanel;
    Edit1: TMenuItem;
    SaveDataAction: TAction;
    SaveData1: TMenuItem;
    FixAction: TAction;
    N1: TMenuItem;
    Fix1: TMenuItem;
    procedure ExitItemClick(Sender: TObject);
    procedure SaveItemClick(Sender: TObject);
    procedure OpenItemClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure SaveDataActionExecute(Sender: TObject);
    procedure FixActionExecute(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure DrawGlyph(X, Y, W, H, Top: Integer);
  end;

Type
 TFontItem = Packed Record
  Height: Integer;
  Top: Integer;
  Bottom: Integer;
  Pitch: Integer;
  xSkip: Integer;
  ImageWidth: Integer;
  ImageHeight: Integer;
  S: Single;
  T: Single;
  S2: Single;
  T2: Single;
  Glyph: Integer; //Zero
  Name: Array[0..31] of Char;
 end;

var
 MainForm: TMainForm;
 FontData: Array[Byte] of TFontItem;
 MainFileName: String;
 TrailData: Array[0..67] of Byte;{ =
 ($00, $00, $80, $40, $63, $68, $72, $73, $77, $66, $74, $65, $00,
  $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC,
  $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC,
  $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC,
  $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC, $CC,
  $CC, $CC, $CC);

                            }
implementation

uses targa, HexUnit;

{$R *.dfm}

Var
 TGA: TGA_Header;

procedure TMainForm.ExitItemClick(Sender: TObject);
begin
 Close;
end;

procedure TMainForm.SaveItemClick(Sender: TObject);
Var F: File; R: Integer;
begin
 If SaveDialog.Execute then
 begin
  AssignFile(F, SaveDialog.FileName);
  Rewrite(F, 1);
  BlockWrite(F, FontData, SizeOf(FontData), R);
  BlockWrite(F, TrailData, SizeOf(TrailData), R);
  CloseFile(F);
 end;
end;

procedure TMainForm.OpenItemClick(Sender: TObject);
Var F: File; R: Integer;
begin
 If OpenDialog.Execute then
 begin
  AssignFile(F, OpenDialog.FileName);
  Reset(F, 1);
  BlockRead(F, FontData, SizeOf(FontData), R);
  BlockRead(F, TrailData, SizeOf(TrailData), R);
  CloseFile(F);
  MainFileName := OpenDialog.FileName;
  ListBoxClick(Sender);
 end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
Var I: Integer;
begin
 With Image.Canvas do
 begin
  Brush.Color := $4C0400;
  Brush.Style := bsSolid;
  FillRect(ClipRect);
 end;
 For I := 0 to 255 do
  ListBox.Items.Add(IntToHex(I, 2));
 ListBox.ItemIndex := 0;
 ListBoxClick(Sender);
end;

Procedure TMainForm.DrawGlyph(X, Y, W, H, Top: Integer);
Var
 PNG: TPNGBitmap; PixelData: TPixelData32;
 I: Integer; Bmp: TBitmap;
begin
 With Image.Canvas do
 begin
  Brush.Color := $4C0400;
  Brush.Style := bsSolid;
  FillRect(ClipRect);
 end;
 With TGA do If Data <> NIL then
 begin
  PNG := TPNGBitmap.Create;
  TDIB32(Addr(PNG.DIB)^) := TDIB32.Create;
  PNG.Transparent := True;
  PNG.DIB.SetSize(W, H);
  PixelData.Bits := Addr(TGA.Data^);
  Inc(Integer(PixelData.Bits), (Integer(TGA.Width) shl 2) * (Integer(TGA.Height) - 1));
  PixelData.ContentRect := Bounds(0, 0, TGA.Width, TGA.Height);
  PixelData.RowStride := -TGA.Width;
  PNG.DIB.CopyFrom(PixelData, 0, 0, Bounds(X, Y, W, H));
  Bmp := TBitmap.Create;
  Bmp.PixelFormat := pf24Bit;
  Bmp.Width := W;
  Bmp.Height := H;
  With Bmp.Canvas do
  begin
   Brush.Color := $4C0400;
   Brush.Style := bsSolid;
   FillRect(ClipRect);
   Draw(0, 0, PNG);
  end;
  I := Image.Height div 2;
//  Image.Canvas.Draw((Image.Width - W) div 2, I - Top, PNG);
  Image.Canvas.StretchDraw(Bounds((Image.Width - W shl 1) shr 1,
  I - Top shl 1, W shl 1, H shl 1), Bmp);
  Bmp.Free;
  With Image.Canvas do
  begin
   Pen.Color := clWhite;
   Pen.Width := 1;
   MoveTo(0, I);
   LineTo(Image.Width, I);
  end;
  PNG.Free;
 end;
end;

function TranslateChar(const Str: string; FromChar, ToChar: Char): string;
var
  I, L: Integer; P: ^Char;
begin
 Result := Str;
 L := Length(Result);
 If L > 0 then
 begin
  P := Addr(Result[1]);
  For I := 1 to L do
  begin
   If P^ = FromChar then P^ := ToChar;
   Inc(P);
  end;
 end;
end;

procedure TMainForm.ListBoxClick(Sender: TObject);
Var FName: String;
Label FreeTGA;
begin
 With FontData[ListBox.ItemIndex] do
 begin
  FName := ExtractFileName(TranslateChar(Name, '/', '\'));
  FileNameEdit.Text := FName;
  P1Edit.Text := IntToStr(Height);
  P2Edit.Text := IntToStr(Top);
  P3Edit.Text := IntToStr(Bottom);
  P4Edit.Text := IntToStr(Pitch);
  P5Edit.Text := IntToStr(xSkip);
  P6Edit.Text := IntToStr(ImageWidth);
  P7Edit.Text := IntToStr(ImageHeight);
  P8Edit.Text := FloatToStr(S * 256);
  P9Edit.Text := FloatToStr(T * 256);
  P10Edit.Text := FloatToStr(S2 * 256);
  P11Edit.Text := FloatToStr(T2 * 256);
  P12Edit.Text := IntToHex(Glyph, 8);
  FName := ExtractFilePath(MainFileName) + FName;
  If FileExists(FName) then
  begin
   If TGA.Data <> NIL then FreeMem(TGA.Data);
   TGA := LoadTGA(FName);
   If TGA.BPP <> 32 then
   begin FreeTGA:
    FreeMem(TGA.Data);
    FillChar(TGA, SizeOf(TGA), 0);
   end;
  end Else Goto FreeTGA;
  DrawGlyph(Trunc(S * 256), Trunc(T * 256), ImageWidth, ImageHeight, Top);
 end;
end;

procedure TMainForm.SaveDataActionExecute(Sender: TObject);
Var FName: String;
Label FreeTGA;
begin
 With FontData[ListBox.ItemIndex] do
 begin
  FName := 'fonts/' + FileNameEdit.Text;
  If Length(FName) > 31 then SetLength(FName, 31);
  FillChar(Name, SizeOf(Name), 0);
  Move(FName[1], Name, Length(FName));
  FName := ExtractFileName(TranslateChar(Name, '/', '\'));
  try
   Height := StrToInt(P1Edit.Text);
   Top := StrToInt(P2Edit.Text);
   Bottom := StrToInt(P3Edit.Text);
   Pitch := StrToInt(P4Edit.Text);
   xSkip := StrToInt(P5Edit.Text);
   ImageWidth := StrToInt(P6Edit.Text);
   ImageHeight := StrToInt(P7Edit.Text);
   S := StrToFloat(P8Edit.Text) / 256;
   T := StrToFloat(P9Edit.Text) / 256;
   S2 := StrToFloat(P10Edit.Text) / 256;
   T2 := StrToFloat(P11Edit.Text) / 256;
   Glyph := HexToInt(P12Edit.Text);
  except
   on E: Exception do ShowMessage('Error!');
  end;
  FName := ExtractFilePath(MainFileName) + FName;
  If FileExists(FName) then
  begin
   If TGA.Data <> NIL then FreeMem(TGA.Data);
   TGA := LoadTGA(FName);
   If TGA.BPP <> 32 then
   begin FreeTGA:
    FreeMem(TGA.Data);
    FillChar(TGA, SizeOf(TGA), 0);
   end;
  end Else Goto FreeTGA;
  DrawGlyph(Trunc(S * 256), Trunc(T * 256), ImageWidth, ImageHeight, Top);
 end;
end;

procedure TMainForm.FixActionExecute(Sender: TObject);
Var I: Integer; X1, Y1, X2, Y2: Single;
begin
 For I := 0 to 255 do With FontData[I] do
 begin
  X1 := S * 256;
  Y1 := T * 256;
  X2 := X1 + ImageWidth;
  Y2 := Y1 + ImageHeight;
  S2 := X2 / 256;
  T2 := Y2 / 256;
 end;
 ListBoxClick(Sender);
 ShowMessage('Fixed!');
end;

end.
