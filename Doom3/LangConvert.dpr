program LangConvert;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes;

Var LastToken: String = '';

Function GetToken(Var P: PChar; Var Line, Cursor: Integer): String;
Var D: PChar;
begin
 Result := '';
 If P = NIL then Exit;
 Case P^ of
  #0: Exit;
  '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':':
  begin
   Inc(Cursor);
   Result := P^;
   Inc(P);
  end;
  Else
  begin
   If P^ = #13 then Inc(P);
   If P^ = #10 then
   begin
    Inc(Line);
    Inc(P);
    Cursor := 0;
   end;
   While P^ in [' ', #9, #13, #10] do
   begin
    Inc(Cursor);
    Inc(P);
   end;
   If P^ in ['(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'] then
   begin
    Inc(Cursor);
    Result := P^;
    Inc(P);
   end Else If P^ = '/' then
   begin
    Inc(Cursor);
    Inc(P);
    Case P^ of
     '/':
     begin
      While not (P^ in [#0, #13, #10]) do
      begin
       Inc(Cursor);
       Inc(P);
      end;
      Result := GetToken(P, Line, Cursor);
     end;
     '*':
     begin
      Repeat
       If P^ = '*' then
       begin
        Inc(Cursor);
        Inc(P);
       end;
       If P^ = '/' then
       begin
        Inc(Cursor);
        Inc(P);
        Result := GetToken(P, Line, Cursor);
        Break;
       end;
       If P^ = #13 then Inc(P);
       If P^ = #10 then
       begin
        Inc(Line);
        Inc(P);
        Cursor := 0;
       end Else
       begin
        Inc(Cursor);
        Inc(P);
       end;
      Until False;
     end;
     Else Result := '/';
    end;
   end Else If P^ = '"' then
   begin
    Repeat
     If P^ = '\' then
     begin
      D := Addr(P^);
      Inc(D);
      If D^ = '"' then
      begin
       Result := Result + '\';
       Inc(Cursor);
       Inc(P);
      end;
     end;
     Result := Result + P^;
     Inc(Cursor);
     Inc(P);
    Until P^ in ['"', #0, #13, #10];
    If P^ = '"' then
    begin
     Result := Result + '"';
     Inc(Cursor);
     Inc(P);
    end;
   end Else If P^ <> #0 then
   begin
    Repeat
     Result := Result + UpCase(P^);
     Inc(Cursor);
     Inc(P);
    Until P^ in [#0..#32, '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'];
   end;
  end;
 end;
 LastToken := Result;
end;

Var
 F: TMemoryStream; P, PP: PChar; S, SS: String; A, B: Integer;
 StrList: TStringList; N: Boolean;

begin
 F := TMemoryStream.Create;
 F.LoadFromFile(ParamStr(1));
 SetLength(S, F.Size);
 Move(F.Memory^, S[1], F.Size);
 F.Free;
 S := S + #0;
 P := Addr(S[1]);
 If GetToken(P, A, B) <> '{' then Exit;
 N := True;
 SS := '';
 While (GetToken(P, A, B) <> '') and (LastToken <> '}') do
 begin
  If N then
  begin
   LastToken := AnsiDequotedStr(LastToken, '"');
   If Pos('#str_', LastToken) = 1 then
   begin
    Delete(LastToken, 1, 5);
    SS := SS + '@' + LastToken + #13#10;
    N := False;
   end Else
   begin
    Write(LastToken); Readln;
    Exit;
   end;
  end Else
  begin
   If Length(LastToken) >= 2 then
   begin
    If LastToken[1] = '"' then Delete(LastToken, 1, 1);
    If LastToken[Length(LastToken)] = '"' then
     Delete(LastToken, Length(LastToken), 1);
    While True do
    begin
     A := Pos('\"', LastToken);
     If A <= 0 then Break;
     Delete(LastToken, A, 1);
    end;
    While True do
    begin
     A := Pos('\n', LastToken);
     If A <= 0 then Break;
     LastToken[A] := #13;
     LastToken[A + 1] := #10;
    end;
    SS := SS + LastToken + #13#10#13#10;
    N := True;
   end Else
   begin
    Write(LastToken); Readln;
    Exit;
   end;
  end;
 end;
 StrList := TStringList.Create;
 StrList.Text := SS;
 StrList.SaveToFile(ParamStr(2));
 StrList.Free;
end.
