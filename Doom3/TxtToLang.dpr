program TxtToLang;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes;

{Var LastToken: String = '';

Function GetToken(Var P: PChar; Var Line, Cursor: Integer): String;
Var D: PChar;
begin
 Result := '';
 If P = NIL then Exit;
 Case P^ of
  #0: Exit;}
//  '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':':
{  begin
   Inc(Cursor);
   Result := P^;
   Inc(P);
  end;
  Else
  begin
   If P^ = #13 then Inc(P);
   If P^ = #10 then
   begin
    Inc(Line);
    Inc(P);
    Cursor := 0;
   end;
   While P^ in [' ', #9, #13, #10] do
   begin
    Inc(Cursor);
    Inc(P);
   end;
 }//  If P^ in ['(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'] then
{   begin
    Inc(Cursor);
    Result := P^;
    Inc(P);
   end Else If P^ = '/' then
   begin
    Inc(Cursor);
    Inc(P);
    Case P^ of
     '/':
     begin
      While not (P^ in [#0, #13, #10]) do
      begin
       Inc(Cursor);
       Inc(P);
      end;
      Result := GetToken(P, Line, Cursor);
     end;
     '*':
     begin
      Repeat
       If P^ = '*' then
       begin
        Inc(Cursor);
        Inc(P);
       end;
       If P^ = '/' then
       begin
        Inc(Cursor);
        Inc(P);
        Result := GetToken(P, Line, Cursor);
        Break;
       end;
       If P^ = #13 then Inc(P);
       If P^ = #10 then
       begin
        Inc(Line);
        Inc(P);
        Cursor := 0;
       end Else
       begin
        Inc(Cursor);
        Inc(P);
       end;
      Until False;
     end;
     Else Result := '/';
    end;
   end Else If P^ = '"' then
   begin
    Repeat
     If P^ = '\' then
     begin
      D := Addr(P^);
      Inc(D);
      If D^ = '"' then
      begin
       Result := Result + '\';
       Inc(Cursor);
       Inc(P);
      end;
     end;
     Result := Result + P^;
     Inc(Cursor);
     Inc(P);
    Until P^ in ['"', #0, #13, #10];
    If P^ = '"' then
    begin
     Result := Result + '"';
     Inc(Cursor);
     Inc(P);
    end;
   end Else If P^ <> #0 then
   begin
    Repeat
     Result := Result + UpCase(P^);
     Inc(Cursor);
     Inc(P);}
//    Until P^ in [#0..#32, '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'];
{   end;
  end;
 end;
 LastToken := Result;
end;

Var
 F: TMemoryStream; P, PP: PChar; S, SS: String; A, B: Integer;
 StrList: TStringList; N: Boolean;
 }

Function FixStr(S: String): String;
Var I, L: Integer; C: ^Char;
begin
 Result := '';
 If S <> '' then
 begin
  C := Addr(S[1]);
  L := Length(S);
  For I := 1 to L do
  begin
   If C^ = '"' then
    Result := Result + '\"' Else
    Result := Result + C^;
   Inc(C); 
  end;
 end;
end;

Var
 List: TStringList; I: Integer;
 OutList: TStringList; S, SS, SD: String;
Label XY;
begin
 List := TStringList.Create;
 List.LoadFromFile(ParamStr(1));
 OutList := TStringList.Create;
 OutList.Add('// string table');
 OutList.Add('// turkish');
 OutList.Add('//');
 OutList.Add('');
 OutList.Add('{');
 I := 0;
 While I < List.Count do
 begin
  S := List[I];
  If (S <> '') and (S[1] = '@') then
  begin
   SS := #9'"#str_' + Copy(S, 2, Length(S) - 1) + '"'#9'"';
   Inc(I);
   Repeat
    S := FixStr(List[I]);
    Inc(I);
    If I = List.Count then Goto XY;
    If List[I] = '' then
    begin
     If I + 1 = List.Count then Goto XY;
     SD := List[I + 1];
     If (SD <> '') and (SD[1] = '@') then
     begin
      XY:
      SS := SS + S + '"'#9;
      Inc(I);
      OutList.Add(SS);
      SS := '';
      Break;
     end Else SS := SS + S + '\n';
    end Else SS := SS + S + '\n';
   Until False;
  end Else Break;
 end;
 OutList.Add('}');
 OutList.SaveToFile(ParamStr(2));
 OutList.Free;
 List.Free;
{ F := TMemoryStream.Create;
 F.LoadFromFile(ParamStr(1));
 SetLength(S, F.Size);
 Move(F.Memory^, S[1], F.Size);
 F.Free;
 S := S + #0;
 P := Addr(S[1]);
 If GetToken(P, A, B) <> '{' then Exit;
 N := True;
 SS := '';}
// While (GetToken(P, A, B) <> '') and (LastToken <> '}') do
{ begin
  If N then
  begin
   LastToken := AnsiDequotedStr(LastToken, '"');
   If Pos('#str_', LastToken) = 1 then
   begin
    Delete(LastToken, 1, 5);
    SS := SS + '@' + LastToken + #13#10;
    N := False;
   end Else
   begin
    Write(LastToken); Readln;
    Exit;
   end;
  end Else
  begin
   If Length(LastToken) >= 2 then
   begin
    If LastToken[1] = '"' then Delete(LastToken, 1, 1);
    If LastToken[Length(LastToken)] = '"' then
     Delete(LastToken, Length(LastToken), 1);
    While True do
    begin
     A := Pos('\"', LastToken);
     If A <= 0 then Break;
     Delete(LastToken, A, 1);
    end;
    While True do
    begin
     A := Pos('\n', LastToken);
     If A <= 0 then Break;
     LastToken[A] := #13;
     LastToken[A + 1] := #10;
    end;
    SS := SS + LastToken + #13#10#13#10;
    N := True;
   end Else
   begin
    Write(LastToken); Readln;
    Exit;
   end;
  end;
 end;
 StrList := TStringList.Create;
 StrList.Text := SS;
 StrList.SaveToFile(ParamStr(2));
 StrList.Free;}
end.
