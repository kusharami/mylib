program bin2asm;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, HexUnit;

var
 Input: TFileStream;
 Output: TStringList;
 I, J: Integer;
 B: Byte;
 S, Prefix, HexFormat: String;
begin
 If ParamCount < 2 then
 begin
  Writeln('USAGE: BIN2ASM srcfile.bin dstfile.asm [byte_prefix] [hex_format]');
 end Else
 begin
  try
   Input := TFileStream.Create(ParamStr(1), fmOpenRead);
   Output := TStringList.Create;
   J := Input.Size;
   I := 0;
   if ParamCount > 2 then
    Prefix := ParamStr(3) + ' ' else
    Prefix := 'db ';
   if ParamCount > 3 then
    HexFormat := ParamStr(4) else
    HexFormat := '0%2.2xh';
   S := Prefix;
   While J > 0 do
   begin
    Input.Read(B, 1);
    Dec(J);
    Inc(I);
    If (I = 16) or (J <= 0) then
    begin
     S := S + Format(HexFormat, [B]);
     Output.Add(S);
     I := 0;
     S := Prefix;
    end Else S := S + Format(HexFormat, [B]) + ',';
   end;
   Output.SaveToFile(ParamStr(2));
   Output.Free;
   Input.Free;
  except
   Writeln('File open error.');
  end;
 end;
end.
