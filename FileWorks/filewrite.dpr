program filewrite;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, HexUnit;

Var Input, Output: TFileStream; InPos, OutPos, SZ, Mode: Integer;
begin
 If ParamCount < 4 then
 begin
  Writeln('USAGE: FILEWRITE [INPUT] [OUTPUT] [INPUTPOS] [OUTPUTPOS] [SIZE]');
 end Else
 begin
  try
   Input := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyNone);
   InPos := GetLDW(ParamStr(3));
   OutPos := GetLDW(ParamStr(4));
   If ParamCount > 4 then SZ := GetLDW(ParamStr(5)) Else SZ := Input.Size - InPos;
   If not HexError and not IntError and (OutPos >= 0) and (InPos >= 0) and (SZ >= 0) then
   begin
    Input.Position := InPos;
    If FileExists(ParamStr(2)) then Mode := fmOpenWrite or fmShareDenyNone Else Mode := fmCreate;
    try
     Output := TFileStream.Create(ParamStr(2), Mode);
     Output.Position := OutPos;
     Output.CopyFrom(Input, SZ);
     Output.Free;
    except
     Writeln('File save error.');
    end;
   end Else Writeln('Position error.');
   Input.Free;
  except
   Writeln('File open error.');
  end;
 end;
end.
