unit im_main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, Menus, VirtualTrees, ToolWin, ComCtrls, Grids,
  MPHexEditor;

type
  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    ItemList: TVirtualStringTree;
    File1: TMenuItem;
    ToolBar1: TToolBar;
    HexEditor: TMPHexEditor;
    procedure FormCreate(Sender: TObject);
  private
    Procedure SetPT(X: String);
    Function GetPT: String;
  public
    property ProgramTitle: String read GetPT write SetPT;
  end;

var
  MainForm: TMainForm;

resourcestring
 Title = 'IPS Manager';  

implementation

{$R *.dfm}            

Procedure TMainForm.SetPT(X: String);
begin
 Application.Title := X;
 Caption := X;
end;

Function TMainForm.GetPT: String;
begin
 Result := Application.Title;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
 ProgramTitle := Title;
 HexEditor.LoadFromFile('im_main.pas');
end;

end.
