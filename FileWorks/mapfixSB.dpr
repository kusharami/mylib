program mapfixSB;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, MyUtils;

Var Input: TMemoryStream; W, H, X, Y, Increment, Modify: Integer; M: ^Byte;
begin
 If ParamCount < 4 then
 begin
  Writeln('USAGE: MAPFIXSB [OPTION] [INPUT] [OUTPUT] [PARAM1] [PARAM2]');
  Writeln('   -i: increase map cells by PARAM1');
  Writeln('   -m: increase PARAM1 cells by PARAM2');
  Writeln('   -iw: increase map cells by PARAM1 (map without header)');
  Writeln('   -mw: increase PARAM1 cells by PARAM2 (map without header)');
 end Else
 begin
  try
   Input := TMemoryStream.Create;
   Input.LoadFromFile(ParamStr(2));
   M := Input.Memory;
   If ParamStr(1) = '-iw' then
   begin
    Y := Input.Size;
    Increment := DecHexToInt(ParamStr(4));
    While Y > 0 do
    begin
     Inc(M^, Increment);
     Inc(M);
     Dec(Y);
    end;
   end Else If ParamStr(1) = '-mw' then
   begin
    Y := Input.Size;
    Increment := DecHexToInt(ParamStr(4));
    Modify := DecHexToInt(ParamStr(5));
    While Y > 0 do
    begin
     If M^ = Modify then Inc(M^, Increment);
     Inc(M);
     Dec(Y);
    end;
   end Else
   begin
    Input.Read(W, 1);
    Input.Read(H, 1);
    Inc(M, 2);
    If ParamStr(1) = '-i' then
    begin
     Increment := DecHexToInt(ParamStr(4));
     For Y := 0 to H - 1 do For X := 0 to W - 1 do
     begin
      Inc(M^, Increment);
      Inc(M);
     end;
    end Else If ParamStr(1) = '-m' then
    begin
     Increment := DecHexToInt(ParamStr(4));
     Modify := DecHexToInt(ParamStr(5));
     For Y := 0 to H - 1 do For X := 0 to W - 1 do
     begin
      If M^ = Modify then Inc(M^, Increment);
      Inc(M);
     end;
    end;
   end;
   Input.SaveToFile(ParamStr(3));
   Input.Free;
  except
   Writeln('File open error.');
  end;
 end;
end.
