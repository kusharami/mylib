program numwrite;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, HexUnit;

Var Output: TFileStream; OutPos, SZ, Number, Mode: Integer;
begin
 If ParamCount < 4 then
 begin
  Writeln('USAGE: NUMWRITE [NUMBER] [OUTPUTFILE] [OUTPUTPOS] [NUMSIZE]');
 end Else
 begin
  Number := GetLDW(ParamStr(1));
  OutPos := GetLDW(ParamStr(3));
  SZ := GetLDW(ParamStr(4));
  If SZ > 4 then SZ := 4;
  If not HexError and not IntError and (OutPos >= 0) and (SZ >= 0) then
  begin
   If FileExists(ParamStr(2)) then
    Mode := fmOpenWrite Else
    Mode := fmCreate;
   try
    Output := TFileStream.Create(ParamStr(2), Mode);
    Output.Position := OutPos;
    Output.Write(Number, SZ);
    Output.Free;
   except
    Writeln('File save error.');
   end;
  end Else Writeln('Position error.');
 end;
end.
