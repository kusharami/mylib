program genact;

{$APPTYPE CONSOLE}

Type
 TRGB = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
 end;

Var F: File; I, ColCount: Integer; Red, Green, Blue, Back: Boolean; Col: TRGB;

begin
 If ParamCount >= 2 then
 begin
  If ParamStr(2) = 'RGB' then
  begin
   Red := True;
   Green := True;
   Blue := True;
  end Else If ParamStr(2) = 'R' then
  begin
   Red := True;
   Green := False;
   Blue := False;
  end Else If ParamStr(2) = 'G' then
  begin
   Red := False;
   Green := True;
   Blue := False;
  end Else If ParamStr(2) = 'B' then
  begin
   Red := False;
   Green := False;
   Blue := True;
  end Else If ParamStr(2) = 'RG' then
  begin
   Red := True;
   Green := True;
   Blue := False;
  end Else If ParamStr(2) = 'RB' then
  begin
   Red := True;
   Green := False;
   Blue := True;
  end Else If ParamStr(2) = 'GB' then
  begin
   Red := False;
   Green := True;
   Blue := True;
  end Else
  begin
   Red := False;
   Green := False;
   Blue := False;
  end;
  Back := ParamStr(3) = '1';
  IF ParamStr(4) = '1' then ColCount := 15 Else ColCount := 255;
  AssignFile(F, ParamStr(1));
  Rewrite(F, 1);
  If Back then For I := ColCount downto 0 do
  begin
   With Col do
   begin
    If Red then R := I Else R := 0;
    If Green then G := I Else G := 0;
    If Blue then B := I Else B := 0;
    If ColCount = 15 then
    begin
     R := R or (R shl 4);
     G := G or (G shl 4);
     B := B or (B shl 4);
    end;
   end;
   BlockWrite(F, Col, 3);
  end Else For I := 0 to ColCount do
  begin
   With Col do
   begin
    If Red then R := I Else R := 0;
    If Green then G := I Else G := 0;
    If Blue then B := I Else B := 0;
    If ColCount = 15 then
    begin
     R := R or (R shl 4);
     G := G or (G shl 4);
     B := B or (B shl 4);
    end;    
   end;
   BlockWrite(F, Col, 3);
  end;
  If ColCount = 15 then
  begin
   With Col do
   begin
    R := 0;
    G := 0;
    B := 0;
   end;
   For I := 16 to 255 do BlockWrite(F, Col, 3);
  end;
  I := Integer($FFFF1000);
  BlockWrite(F, I, 4);
  CloseFile(F);
 end;
end.
 