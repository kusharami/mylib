unit GfxList;

interface

Uses DIB, Classes;

Type
 PGfxItem = ^TGfxItem;
 TGfxItem = Record
  Data: Array of Byte;
  Bit8: Boolean;
  Image: TDIB;
  Next: PGfxItem;
 end;
 TGfxList = Class
  FRoot, FCur: PGfxItem;
  FCount: Integer;
  Procedure SaveToFile(Const FileName: String);
  Procedure LoadFromFile(Const FileName: String);
  Function Get(Index: Integer): PGfxItem;
  Constructor Create;
  Destructor Destroy; override;
  Function Add: PGfxItem;
  Procedure Clear;
  Procedure Remove(Item: PGfxItem);
  property Count: Integer read FCount;
  Property Items[Index: Integer]: PGfxItem read Get;
 end;

implementation

Function TGfxList.Get(Index: Integer): PGfxItem;
Var I: Integer; N: PGfxItem;
begin
 I := 0; N := FRoot; Result := NIL;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Constructor TGfxList.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TGfxList.Destroy;
begin
 Clear;
 Inherited Destroy;
end;

Function TGfxList.Add: PGfxItem;
begin
 New(Result);
 If FRoot = NIL then FRoot := Result Else FCur^.Next := Result;
 FCur := Result;
 FillChar(Result^, SizeOf(TGfxItem), 0);
 With Result^ do
 begin
  Image := TDIB.Create;
  Image.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
  Image.BitCount := 8;
 end;
 Inc(FCount);
end;

Procedure TGfxList.Clear;
Var N: PGfxItem;
begin
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  FRoot^.Image.Free;
  SetLength(FRoot^.Data, 0);
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

Procedure TGfxList.Remove(Item: PGfxItem);
Var P, PR: PGfxItem;
begin
 P := FRoot; PR := NIL;
 While P <> NIL do
 begin
  If P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 If P = NIL then Exit;
 With P^ do
 begin
  If FRoot = P then
  begin
   If FRoot = FCur then FCur := Next;
   FRoot := Next;
  end Else If Next = NIL then
  begin
   PR^.Next := NIL;
   FCur := PR;
  end Else PR^.Next := P^.Next;
  P^.Image.Free;
  Dispose(P);
  Dec(FCount);
 end;
end;

Type
 TChar4 = Array[0..3] of Char;
Const
 Char4: TChar4 = 'GFX'#0;

Procedure TGfxList.SaveToFile(Const FileName: String);
Var Stream: TMemoryStream; N: PGfxItem; I, Y: Integer;
begin
 Stream := TMemoryStream.Create;
 Stream.Write(Char4, 4);
 Stream.Write(FCount, 4);
 N := FRoot;
 While N <> NIL do With N^ do
 begin
  I := Length(Data);
  Stream.Write(I, 4);
  Stream.Write(Data[0], I);
  I := Image.Width;
  I := I + Integer(Bit8) shl 31;
  Stream.Write(I, 4);
  I := Image.Height;
  Stream.Write(I, 4);
  With Image do
  begin
   If Bit8 then
    Stream.Write(ColorTable, 256 * 4) Else
    Stream.Write(ColorTable, 16 * 4);
   For Y := 0 to Height - 1 do
    Stream.Write(ScanLine[Y]^, Width);
  end;
  N := Next;
 end;
 Stream.SaveToFile(FileName);
 Stream.Free;
end;

Procedure TGfxList.LoadFromFile(Const FileName: String);
Var Stream: TMemoryStream; C, I, X, Y: Integer; Ch: TChar4;
begin
 Stream := TMemoryStream.Create;
 Stream.LoadFromFile(FileName);
 Stream.Read(Ch, 4);
 If Ch = Char4 then
 begin
  Clear;
  Stream.Read(C, 4);
  For I := 0 to C - 1 do With Add^ do
  begin
   Stream.Read(X, 4);
   SetLength(Data, X);
   Stream.Read(Data[0], X);
   Stream.Read(X, 4);
   Image.Width := X and $7FFFFFFF;
   Bit8 := Boolean(X shr 31);
   Stream.Read(X, 4);
   Image.Height := X;
   With Image do
   begin
    If Bit8 then Stream.Read(ColorTable, 256 * 4) Else
    begin
     Stream.Read(ColorTable, 16 * 4);
     For X := 16 to 255 do ColorTable[X] := ColorTable[0];
    end;
    For Y := 0 to Height - 1 do
     Stream.Read(ScanLine[Y]^, Width);
    UpdatePalette;
   end;
  end;
 end;
 Stream.Free;
end;

end.
