unit HexUnit;

interface

(*   Copyright!  *)
(* Made by Jinni *)
(*  Do not sell! *)


//***Numbers convertion utilities***//
function IntCheckOut(S: String): Boolean;
//Returns TRUE if S contains the valid integer value
function HexCheckOut(S: String): Boolean;
//Returns TRUE if S contains the valid integer value or hXX style hex value
function UnsCheckOut(S: String): Boolean;
//Returns TRUE if S contains the valud unsigned value or hXX style hex value
function StrToInt(const S: String): Integer;
//Converts decimal string to 32 bit integer value
function HexCharToInt(C: Char): Integer;
function HexToInt(const S: String): Integer;
//Converts hex-string to 32 bit integer value
function DecHexToInt(S: String): Integer;
//Converts decimal or hXX style hex-string to 32 bit integer value
function HexToCardinal(const S: String): Cardinal;
//Converts hex-string to 32 bit unsigned value
function GHexToInt(S: String): Integer;
//Converts hex-string of 0XXh style to integer value
function ByteSwap16(Value: Word): Word;
//Swaps bytes in 16 bit value
function ByteSwap32(Value: LongInt): LongInt; overload;
function ByteSwap32(Value: LongWord): LongWord; overload;
//Swaps bytes in 32 bit value
function ByteSwap64(const Value: Int64): Int64;
//Swaps bytes in 64 bit value
function IntToDelphiHex(const Value: Int64; Digits: Byte): String;
function IntTo0xHex(const Value: Int64; Digits: Byte): String;
//Converts 64 bit value to DELPHI format hex-string (ex: $135ABC)
procedure FillWord(var Dest; Count: LongInt; Value: Word);
procedure Fill16(Value: Word; var Dest; Count: LongInt);
procedure Fill32(Value: LongWord; var Dest; Count: LongInt);

function Min(A, B: Integer): Integer;
function Max(A, B: Integer): Integer;
function UMin(A, B: Cardinal): Cardinal;
function UMax(A, B: Cardinal): Cardinal;
function Aligned(Value, Alignment: LongInt): LongInt;

procedure Exchg(var A, B: LongInt);
procedure ExchgP(A, B: Pointer);

type
 TIntegerList = array of LongInt;

var
 HexError: Boolean = False;
 IntError: Boolean = False;

procedure _Shl64_ESI_EDX;
procedure _Shl64_EBX_EAX;
procedure _Shr64to32;

implementation

uses SysUtils;

function IntToDelphiHex(const Value: Int64; Digits: Byte): String;
begin
 Result := '$' + IntToHex(Value, Digits);
end;

function IntTo0xHex(const Value: Int64; Digits: Byte): String;
begin
 Result := '0x' + IntToHex(Value, Digits);
end;

function StrToInt(const S: String): Integer;
var
 E: Integer;
begin
 Val(S, Result, E);
 IntError := E <> 0;
end;

function DecHexToInt(S: String): Integer;
begin
 IntError := True;
 if S = '' then
 begin
  Result := 0;
  Exit;
 end;
 case S[1] of
  'h', 'H':
   begin
    Delete(S, 1, 1);
    if S = '' then
    begin
     Result := 0;
     HexError := True;
     IntError := False;
    end else
    begin
     Result := HexToInt(S);
     IntError := False;
    end;
  end;
  else Result := StrToInt(S);
 end;
end;

function GHexToInt(S: String): Integer;
var
 I, LS, J: Integer; PS: PChar; H: Char;
begin
 HexError := True;
 Result := 0;
 LS := Length(S);
 if (LS <= 0) then Exit;
 if Upcase(S[LS]) <> 'H' then
 begin
  HexError := False;
  Result := StrToInt(S);
  Exit;
 end else
 begin
  Dec(LS);
  SetLength(S, LS);
  if (LS > 8) and (S[1] = '0') then Delete(S, 1, 1);
  LS := Length(S);
 end;
 if LS > 8 then Exit;
 HexError := False;
 PS := Pointer(S);
 I := LS - 1;
 J := 0;
 While I >= 0 do
 begin
  H := UpCase(PS^);
  case H of
    '0'..'9': J := Word(H) - 48;
    'A'..'F': J := Word(H) - 55;
    else
    begin
     HexError := True;
     Result := 0;
     Exit;
    end;
  end;
  Inc(Result, J shl (I shl 2));
  Inc(PS);
  Dec(I);
 end;
end;

function HexCharToInt(C: Char): Integer;
begin
 C := UpCase(C);
 case C of
  '0'..'9': Result := Ord(C) - 48;
  'A'..'F': Result := Ord(C) - 55;
  else Result := -1;
 end
end;

function HexToInt(const S: String): Integer;
var
 I, LS, J: Integer; PS: PChar; H: Char;
begin
 HexError := True;
 Result := 0;
 LS := Length(S);
 if (LS <= 0) or (LS > 8) then Exit;
 HexError := False;
 PS := Pointer(S);
 I := LS - 1;
 J := 0;
 While I >= 0 do
 begin
  H := UpCase(PS^);
  case H of
    '0'..'9': J := Ord(H) - 48;
    'A'..'F': J := Ord(H) - 55;
    else
    begin
      HexError := True;
      Result := 0;
      Exit;
    end;
  end;
  Inc(Result, J shl (I shl 2));
  Inc(PS);
  Dec(I);
 end;
end;

function HexToCardinal(const S: String): Cardinal;
var
 I, LS: Integer; PS: PChar; H: Char; J: Cardinal;
begin
 HexError := True;
 Result := 0;
 LS := Length(S);
 if (LS <= 0) or (LS > 8) then Exit;
 HexError := False;
 PS := Pointer(S);
 I := LS - 1;
 J := 0;
 While I >= 0 do
 begin
  H := UpCase(PS^);
  case H of
    '0'..'9': J := Ord(H) - 48;
    'A'..'F': J := Ord(H) - 55;
    else
    begin
     HexError := True;
     Result := 0;
     Exit;
    end;
  end;
  Inc(Result, J shl (I shl 2));
  Inc(PS);
  Dec(I);
 end;
end;

function IntCheckOut(S: String): Boolean;
var I: Integer;
begin
 if S <> '' then
 begin
  Result := True;
  if S[1] = '-' then Delete(S, 1, 1);
  if (Length(S) > 10) or (S = '') then
  begin
   Result := False;
   Exit;
  end;
  for I := 1 to Length(S) do
    case S[I] of
      '0'..'9': ;
      else
      begin
        Result := False;
        Exit;
      end;
    end;
 end else Result := False;
end;

function HexCheckOut(S: String): Boolean;
var I: Integer;
begin
 Result := False;
 if S = '' then Exit;
 Result := True;
 case S[1] of
   'h', 'H':
   begin
    Delete(S, 1, 1);
    if (Length(S) > 8) or (S = '') then
    begin
     Result := False;
     Exit;
    end;
    for I := 1 to Length(S) do
     case S[I] of
       '0'..'9',
       'A'..'F',
       'a'..'f': ;
       else
       begin
         Result := False;
         Exit;
       end;
     end;
   end;
   else
   begin
    if S[1] = '-' then
     Delete(S, 1, 1);
    if (Length(S) > 10) or (S = '') then
    begin
     Result := False;
     Exit;
    end;
    for I := 1 to Length(S) do
      case S[I] of
        '0'..'9': ;
        else
        begin
          Result := False;
          Exit;
        end;
      end;
   end;
 end;
end;

function UnsCheckOut(S: String): Boolean;
var I: integer;
begin
 Result := False;
 if S = '' then Exit;
 Result := True;
 case S[1] of
   'h', 'H':
   begin
    Delete(S, 1, 1);
    if (Length(S) > 8) or (S = '') then
    begin
     Result := False;
     Exit;
    end;
    for I := 1 to Length(S) do
      case S[I] of
        '0'..'9', 'A'..'F', 'a'..'f': ;
        else
        begin
          Result := False;
          Exit;
        end;
      end;
   end;
   else
   begin
    if (Length(S) > 10) or (S = '') then
    begin
     Result := False;
     Exit;
    end;
    for I := 1 to Length(S) do
      case S[I] of
        '0'..'9': ;
        else
        begin
          Result := False;
          Exit;
        end;
      end;
   end;
 end;
end;

function ByteSwap32(Value: LongInt): LongInt;
asm
 bswap  eax
end;

function ByteSwap32(Value: LongWord): LongWord;
asm
 bswap  eax
end;

function ByteSwap16(Value: Word): Word;
asm
 xchg   al,ah
end;

function ByteSwap64(const Value: Int64): Int64;
type
 TInt64 = packed record
  A: LongInt;
  B: LongInt;
 end;
var
 Val: TInt64 absolute Value;
 Res: TInt64 absolute Result;
begin
 Res.A := ByteSwap32(Val.B);
 Res.B := ByteSwap32(Val.A);
end;

procedure FillWord(var Dest; Count: LongInt; Value: Word);
asm
 PUSH   EDI

 MOV    EDI,EAX

 MOV    EAX,ECX
 SHL    EAX,16
 MOV    AX,CX

 MOV    ECX,EDX
 SAR    ECX,1
 JS     @@exit

 REP    STOSD

 MOV    ECX,EDX
 AND    ECX,1
 REP    STOSW

@@exit:
 POP    EDI
end;

function Min(A, B: Integer): Integer;
begin
 if A < B then
  Result := A else
  Result := B;
end;

function Max(A, B: Integer): Integer;
begin
 if A > B then
  Result := A else
  Result := B;
end;

function UMin(A, B: LongWord): LongWord;
begin
 if A < B then
  Result := A else
  Result := B;
end;

function UMax(A, B: LongWord): LongWord;
begin
 if A > B then
  Result := A else
  Result := B;
end;

function Aligned(Value, Alignment: LongInt): LongInt;
asm
 cmp  edx,1
 jle  @@Exit
 mov  ecx,edx
 dec  ecx
 add  eax,ecx
 push eax
 mov  ecx,edx
 cdq
 idiv ecx
 pop eax
 sub eax,edx
@@Exit:
end;
{begin
 Result := Value + (Alignment - 1);
 Dec(Result, Result mod Alignment);
end;  }

procedure _Shl64_ESI_EDX;
asm
        cmp cl, 32
        jl  @__llshl@below32
        cmp cl, 64
        jl  @__llshl@below64
        xor edx, edx
        xor esi, esi
        ret

@__llshl@below64:
        mov edx, esi
        shl edx, cl
        xor esi, esi
        ret

@__llshl@below32:
        shld  edx, esi, cl
        shl esi, cl
        ret
end;

procedure _Shl64_EBX_EAX;
asm
        cmp cl, 32
        jl  @__llshl@below32
        cmp cl, 64
        jl  @__llshl@below64
        xor eax, eax
        xor ebx, ebx
        ret

@__llshl@below64:
        mov eax, ebx
        shl eax, cl
        xor ebx, ebx
        ret

@__llshl@below32:
        shld  eax, ebx, cl
        shl ebx, cl
        ret
end;

procedure _Shr64to32;
asm
        cmp cl, 32
        jl  @__llushr@below32
        cmp cl, 64
        jl  @__llushr@below64
        xor eax, eax
        ret

@__llushr@below64:
        mov eax, edx
        shr eax, cl
        ret

@__llushr@below32:
        shrd  eax, edx, cl
        ret
end;

procedure Exchg(var A, B: LongInt);
asm
 mov ecx,[eax]
 xchg ecx,[edx]
 mov [eax],ecx
end;

procedure ExchgP(A, B: Pointer);
asm
 mov ecx,[eax]
 xchg ecx,[edx]
 mov [eax],ecx
end;


procedure Fill16(Value: Word; var Dest; Count: LongInt);
asm
{       AX      Value                 }
{       EDX     Destination pointer   }
{       ECX     Repeat count          }

        PUSH    EDI
        MOV     EDI,EDX
        REP     STOSW
        POP     EDI
end;

procedure Fill32(Value: LongWord; var Dest; Count: LongInt);
asm
{       AX      Value                 }
{       EDX     Destination pointer   }
{       ECX     Repeat count          }

        PUSH    EDI
        MOV     EDI,EDX
        REP     STOSD
        POP     EDI
end;

end.
