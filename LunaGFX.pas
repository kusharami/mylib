unit LunaGFX;

interface

uses
 Windows, SysUtils, Classes, HexUnit, NodeLst, MyClasses, MyClassesEx,
 PlugAdapters, PlugInterface, ZLibEx, BitmapEx, bmpimg;

type
// TColorFormat = class;
 TInfoList = class(TNodeListEx)
  private
    FInfoStream: TMemStream;
  protected
    procedure Initialize; override;
  public
    property InfoStream: TMemStream read FInfoStream;
    destructor Destroy; override;
 end;

 TConvertersList = array[Boolean] of TBitmapConverter;

 TLunaGFX = class(TNode)
  private
    FContainer: TCustomImage;
    FConverters: TConvertersList;
{    FLeft: Integer;
    FTop: Integer;
    FWidth: Integer;
    FLineSize: Integer;
    FHeight: Integer;
    FBitCount: Integer;
    FPixelReadFormat: TPixelReadFormat;
    FData: Pointer;
    FColorFormat: TColorFormat;
    FColorTable: Pointer;
    FPaletteSize: Integer;
    FImageSize: Integer;    }
    FPixelSize: Integer;
    FInfoStream: TMemStream;
  //  FColorsUsed: Integer;
    FChannels: PChannel;
    procedure SetInfoSize(Value: Integer);
    function GetWidth: Integer;
    function GetHeight: Integer;
    function GetBitCount: Integer;
    function GetColorSize: Integer;
    function GetInfo: Pointer;
    function GetInfoSize: Integer;
    function GetChannelsPtr: PChannel;
    function GetChannelsCount: Integer;
    procedure UpdateColorTable;
    function GetColorFormat: TColorFormat;
    function GetColorTable: Pointer;
    function GetData: Pointer;
    function GetLeft: Integer;
    function GetLineSize: Integer;
    function GetTop: Integer;
    procedure SetLeft(Value: Integer);
    procedure SetTop(Value: Integer);
    function GetImageSize: Integer;
    function GetPaletteSize: Integer;
  protected
    procedure Initialize; override;
  public
    property Container: TCustomImage read FContainer;
    property Converters: TConvertersList read FConverters write FConverters;
    property Left: Integer read GetLeft write SetLeft;
    property Top: Integer read GetTop write SetTop;
    property Width: Integer read GetWidth;
    property LineSize: Integer read GetLineSize;
    property Height: Integer read GetHeight;
    property BitCount: Integer read GetBitCount;
//    property ColorsUsed: Integer read FColorsUsed write FColorsUsed;
    property ColorSize: Integer read GetColorSize;
    property ImgBuf: Pointer read GetData;
    property ImageSize: Integer read GetImageSize;
    property InfoStream: TMemStream read FInfoStream;
    property Info: Pointer read GetInfo;
    property InfoSize: Integer read GetInfoSize write SetInfoSize;
    property ColorTable: Pointer read GetColorTable;
    property PaletteSize: Integer read GetPaletteSize;
    property ChannelsPtr: PChannel read GetChannelsPtr;
    property ChannelsCount: Integer read GetChannelsCount;
//    property PixelReadFormat: TPixelReadFormat read FPixelReadFormat
//                                              write FPixelReadFormat;
    property ColorFormat: TColorFormat read GetColorFormat;

    procedure Assign(Source: TNode); override;
    procedure SetConverter(const DstFlags: array of Word; DrawFlags: Integer = 0);
    destructor Destroy; override;
    procedure GetDIBColorTable(Dest: Pointer; Count: Integer = -1);
    procedure SetColorFormat(Data: PChannel; Count: Integer);
    procedure SetSize(AWidth, AHeight, ABitCount: Integer);
 end;

 TLunaGFXList = class;

 TLunaGFXSet = class(TInfoList)
  private
    FName: WideString;
    FChildList: TLunaGFXList;
    FMasterColorTable: PRGBQuads;
    FLayersMode: Boolean;
    FNoBackground: Boolean;
    FLayersPrepared: Boolean;
    FMasterWidth: Word;
    FMasterHeight: Word;
    function GetImage(Index: Integer): TLunaGFX;
  protected
    procedure Initialize; override;
  public
    property Name: WideString read FName write FName;
    property ChildList: TLunaGFXList read FChildList;
    property MasterColorTable: PRGBQuads read FMasterColorTable;
    property MasterWidth: Word read FMasterWidth write FMasterWidth;
    property MasterHeight: Word read FMasterHeight write FMasterHeight;    
    property LayersMode: Boolean read FLayersMode write FLayersMode;

    property Images[Index: Integer]: TLunaGFX read GetImage;

    procedure PrepareLayers;
    function AddImage: TLunaGFX;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;
    destructor Destroy; override;
    procedure Assign(Source: TNode); override;
 end;

 TOnProgressEvent = procedure(Sender: TLunaGFXList; Value: Integer) of Object;

 TLunaGFXList = class(TInfoList)
  private
    FOnProgressMax: TOnProgressEvent;
    FOnProgress: TOnProgressEvent;
    FCompressionLevel: TZCompressionLevel;
    function GetSet(Index: Integer): TLunaGFXSet;
  protected
    procedure Initialize; override;
    procedure DoProgress(Value: Integer); virtual;
    procedure DoProgressMax(Value: Integer); virtual;
  public
    property Sets[Index: Integer]: TLunaGFXSet read GetSet;
    property OnProgress: TOnProgressEvent read FOnProgress
                                         write FOnProgress;
    property OnProgressMax: TOnProgressEvent read FOnProgressMax
                                            write FOnProgressMax;
    property CompressionLevel: TZCompressionLevel read FCompressionLevel
                                                write FCompressionLevel;
    function AddSet: TLunaGFXSet;
    procedure LoadFromStream(Stream: TStream); override;
    procedure ChildLoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream); override;
    procedure ChildSaveToStream(Stream: TStream);
 end;

 TLunaGFXAdapter = class(TInterfacedObject, ILunaGFX)
  private
    FSource: TLunaGFX;
    FFreeAfterUse: Boolean;
    function GetPtr: Pointer; stdcall;
    procedure SetInfoSize(Value: Integer); stdcall;
    procedure SetPixelReadFormat(Value: TPixelReadFormat); stdcall;
    function GetWidth: Integer; stdcall;
    function GetHeight: Integer; stdcall;
    function GetBitCount: Integer; stdcall;
    function GetInfo: Pointer; stdcall;
    function GetPixelReadFormat: TPixelReadFormat; stdcall;
    function GetImgBuf: Pointer; stdcall;
    function GetImageSize: Integer; stdcall;
    function GetColorTable: Pointer; stdcall;
    function GetPaletteSize: Integer; stdcall;
    procedure SetColorFormat(Data: PChannel; Count: Integer); stdcall;
    procedure GetDIBColorTable(Dest: Pointer; Count: Integer); stdcall;
    function GetColorSize: Integer; stdcall;
    function GetInfoSize: Integer; stdcall;
    function GetLineSize: Integer; stdcall;
    procedure SetSize(AWidth, AHeight, ABitCount: Integer); stdcall;
    function GetInfoStream: IStream32; stdcall;
    function GetColorsUsed: Integer; stdcall;
    procedure SetColorsUsed(Value: Integer); stdcall;
    function GetLeft: Integer; stdcall;
    procedure SetLeft(Value: Integer); stdcall;
    function GetTop: Integer; stdcall;
    procedure SetTop(Value: Integer); stdcall;
    function GetFirstFrame: PlugInterface.PFrameInfo; stdcall;
    function GetFramesCount: Integer; stdcall;
    procedure CompressImage(Frame: PlugInterface.PFrameInfo); stdcall;
    procedure CompressPalette(Frame: PlugInterface.PFrameInfo; SkipColor: LongWord; Cnt: Integer); stdcall;
    function AddFrame(ImgSz, PalSz: LongInt): PlugInterface.PFrameInfo; stdcall;    
    function GetColorFormat: IColorFormat; stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create(Source: TLunaGFX; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

 TLunaGFXSetAdapter = class(TInterfacedObject, ILunaGFXSet)
  private
    FSource: TLunaGFXSet;
    FFreeAfterUse: Boolean;
    function GetCount: Integer; stdcall;
    procedure Clear; stdcall;
    function GetImage(Index: Integer): ILunaGFX; stdcall;
    function AddImage: ILunaGFX; stdcall;
    procedure RemoveImage(const Image: ILunaGFX); stdcall;
    function GetPtr: Pointer; stdcall;
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    function GetName: PWideChar; stdcall;
    procedure SetName(Value: PWideChar); stdcall;
    function GetInfoStream: IStream32; stdcall;
    function GetChildList: ILunaGFXList; stdcall;
    procedure FreeObject; stdcall;
    procedure SetLayersMode(Value: LongBool); stdcall;
    procedure SetNoBackground(Value: LongBool); stdcall;
    function GetLayersMode: LongBool; stdcall;
    function GetNoBackground: LongBool; stdcall;
    procedure SetMasterWidth(Value: Integer); stdcall;
    procedure SetMasterHeight(Value: Integer); stdcall;
    function GetMasterWidth: Integer; stdcall;
    function GetMasterHeight: Integer; stdcall;
  public
    constructor Create(Source: TLunaGFXSet; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

 TLunaGFXListAdapter = class(TInterfacedObject, ILunaGFXList)
  private
    FSource: TLunaGFXList;
    FFreeAfterUse: Boolean;
    function GetSet(Index: Integer): ILunaGFXSet; stdcall;
    function AddSet: ILunaGFXSet; stdcall;
    procedure RemoveSet(const ASet: ILunaGFXSet); stdcall;
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    function GetCount: Integer; stdcall;
    procedure Clear; stdcall;
    function GetPtr: Pointer; stdcall;
    function GetInfoStream: IStream32; stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create(Source: TLunaGFXList; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

 TColorFormatAdapter = class(TInterfacedObject, IColorFormat)
  private
    FSource: TColorFormat;
    FFreeAfterUse: Boolean;
    function GetColorSize: LongInt; stdcall;
    function GetColorBits: LongInt; stdcall;
    function GetUsedBits: LongInt; stdcall;
    function GetFlags: LongWord; stdcall;
    procedure SetFlags(Value: LongWord); stdcall;    
    procedure ConvertToRGBQuad(Source, Dest: Pointer; Count: Integer);
      stdcall;
    procedure ConvertFromRQBQuad(Source, Dest: Pointer; Count: Integer);
      stdcall;
    function GetPtr: Pointer; stdcall;
    procedure ConvertTo(const DestFormat: IColorFormat; Source,
      Dest: Pointer; Count: Integer); stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create(Source: TColorFormat; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

 ELunaGFXListError = class(Exception);
 ELunaGFXSetError = class(Exception);
 ELunaGFXError = class(Exception);
 EColorFormatError = class(Exception);

const
 GFX_HEAD: TGfxHead = 'LUNA_GFX';
 GFX_SET: TGfxHead = 'LUNA_SET';
 RGBChannels: array[0..3] of TChannel =
 ((chType: ctRed; chBitsCount: 8),
  (chType: ctGreen; chBitsCount: 8),
  (chType: ctBlue; chBitsCount: 8),
  (chType: ctDummy; chBitsCount: 8));

function GetDIBbitCount(BCnt: Integer): Integer;
function GetColorFormatStr(Data: PChannel; Count: Integer): AnsiString;

implementation

uses TntClasses;

type
 TLunaImgInfo = packed record
  liLeft: LongInt;
  liTop: LongInt;
  liWidth: Word;
  liWidthRemainder: Word;
  liHeight: Word;
  liImgFmt: TImageFormat;
  liCompositeCount: Byte;
  liFlags1: Byte; // bits count (6 bits) and plane bits count (2 bit)
  liColorsUsed: LongInt;
  liFlags2: Byte; // image flags
  liChannelsCnt: LongInt;
 end;

function GetDIBbitCount(BCnt: Integer): Integer;
begin
 case BCnt of
  1: Result := 1;
  2..4: Result := 4;
  9..16: Result := 16;
  17..24: Result := 24;
  25..32: Result := 32;
  else Result := 8;
 end;
end;

function GetColorFormatStr(Data: PChannel; Count: Integer): AnsiString;
var
 I: Integer;
begin
 SetLength(Result, Count * 2);
 for I := 1 to Count do
 begin
  with Data^ do
  begin
   Result[I] := ChannelNames[chType];
   if chBitsCount > 8 then chBitsCount := 8;
   Result[I + Count] := AnsiChar(Byte('0') + chBitsCount);
  end;
  Inc(Data);
 end;
end;

procedure BadDataError;
begin
 raise ELunaGFXSetError.Create('Bad data.');
end;

procedure TLunaGFX.Assign(Source: TNode);
var
 Src: TLunaGFX absolute Source;
 Flags: TCompositeFlags;
begin
 inherited;
//  Self.SetColorFormat(ChannelsPtr, ChannelsCount);
 FContainer.ImageData := nil;
 FContainer.ColorTable := nil;
 Flags := Src.FContainer.FlagsList;
 FContainer.Reallocate(Src.FContainer.Width, Src.FContainer.Height,
          Src.FContainer.ImageFormat, Flags, nil, nil,
          Src.FContainer.WidthRemainder);
 FContainer.ColorsUsed := Src.FContainer.ColorsUsed;
 FPixelSize := Src.FPixelSize;
//  Self.SetSize(FWidth, FHeight, FBitCount);
 Move(Src.FContainer.FirstFrame.fiImage^, FContainer.FirstFrame.fiImage^, ImageSize);
 SetInfoSize(Src.InfoSize);
 Move(Src.Info^, Info^, InfoSize);
 Move(Src.FContainer.FirstFrame.fiPalette^, FContainer.FirstFrame.fiPalette^,
      Src.FContainer.PaletteSize);
 FreeAndNIL(FConverters[False]);
 FreeAndNIL(FConverters[True]);
end;

destructor TLunaGFX.Destroy;
begin
 if FChannels <> nil then FreeMem(FChannels);
 FConverters[True].Free;
 FConverters[False].Free;
 FContainer.Free;
 FInfoStream.Free;
 inherited;
end;

function TLunaGFX.GetBitCount: Integer;
begin
 if FContainer.FirstFrame.fiImage <> NIL then
  Result := FContainer.BitsCount else
  Result := 0;
end;

function TLunaGFX.GetChannelsCount: Integer;
begin
 if FContainer.FInternalClrFmt <> NIL then
  Result := FContainer.FInternalClrFmt.ChannelsCount else
  Result := 0;
end;

function TLunaGFX.GetChannelsPtr: PChannel;
var
 I, Cnt, Mask: Integer;
begin
 if FContainer.FInternalClrFmt <> NIL then
 with FContainer.FInternalClrFmt do
 begin
  if FChannels <> nil then FreeMem(FChannels);
  Cnt := ChannelsCount;
  GetMem(FChannels, Cnt * SizeOf(TChannel));
  Result := FChannels;
  FillChar(Result^, Cnt * SizeOf(TChannel), 0);
  Mask := 1;
  for I := 0 to Cnt - 1 do
  begin
   if AlphaMask and Mask <> 0 then
   begin
    if Result.chType = ctDummy then
     Inc(Result);
    if Flags and CFLG_ALPHA <> 0 then
     Result.chType := ctAlpha else
     Result.chType := ctDummy;
    Result.chBitsCount := AlphaCount;
    Mask := Mask shl AlphaCount;
   end else
   if RedMask and Mask <> 0 then
   begin
    if Result.chType = ctDummy then
     Inc(Result);
    if Flags and CFLG_GRAYSCALE <> 0 then
     Result.chType := ctGray else
     Result.chType := ctRed;
    Result.chBitsCount := RedCount;
    Mask := Mask shl RedCount;
   end else
   if (GreenMask and Mask <> 0) and (Flags and CFLG_GRAYSCALE = 0) then
   begin
    if Result.chType = ctDummy then
     Inc(Result);
    Result.chType := ctGreen;
    Result.chBitsCount := GreenCount;
    Mask := Mask shl GreenCount;
   end else
   if (BlueMask and Mask <> 0) and (Flags and CFLG_GRAYSCALE = 0) then
   begin
    if Result.chType = ctDummy then
     Inc(Result);
    Result.chType := ctBlue;
    Result.chBitsCount := BlueCount;
    Mask := Mask shl BlueCount;
   end else
   begin
    Result.chType := ctDummy;
    Inc(Result.chBitsCount);
    Mask := Mask shl 1;
    if Result.chBitsCount < 8 then
     Continue;
   end;
   Inc(Result);
  end;
  Result := FChannels;
 end else
  Result := NIL;
end;

function TLunaGFX.GetColorFormat: TColorFormat;
begin
 Result := FContainer.FInternalClrFmt;
end;

function TLunaGFX.GetColorSize: Integer;
begin
 if FContainer.FInternalClrFmt = NIL then
  Result := 0 else
  Result := FContainer.FInternalClrFmt.ColorSize;
end;

function TLunaGFX.GetColorTable: Pointer;
begin
 Result := FContainer.FirstFrame.fiPalette;
end;

function TLunaGFX.GetData: Pointer;
begin
 Result := FContainer.FirstFrame.fiImage;
end;

procedure TLunaGFX.GetDIBColorTable(Dest: Pointer; Count: Integer);
var
 BCnt: Byte;
 Y: Integer;
 Color: PLongWord;
begin
 Assert(Assigned(FContainer.ColorFormat), 'FColorFormat is not assigned.');

 BCnt := BitCount;
 if BCnt in [1, 2, 4, 8] then
 begin
  if Count < 0 then Count := 1 shl BCnt;
  if Dest <> NIL then
  begin
   Count := Min(Count, 1 shl BCnt);
   if Count > 0 then
   begin
    if FContainer.ImageFormat = ifRGB then
    begin
     Color := Dest;
     with FContainer.ColorFormat do
      for Y := 0 to Count - 1 do
      begin
       Color^ := ValueToRGBQuad(Y);
       Inc(Color);
      end;
    end else
    FContainer.ColorFormat.ConvertToRGBQuad(FContainer.FirstFrame.fiPalette, Dest, Count);
   end;
  end;
 end else
  ELunaGFXError.Create('Palette does not exist.');
end;

function TLunaGFX.GetHeight: Integer;
begin
 if FContainer.FirstFrame.fiImage <> NIL then
  Result := FContainer.Height else
  Result := 0;
end;

function TLunaGFX.GetImageSize: Integer;
begin
 if FContainer.Compressed then
  Result := FContainer.CompressedSize else
  Result := FContainer.ImageSize;
end;

function TLunaGFX.GetInfo: Pointer;
begin
 Assert(Assigned(FInfoStream), 'FInfoStream is not assigned.');

 Result := FInfoStream.Memory;
end;

function TLunaGFX.GetInfoSize: Integer;
begin
 Assert(Assigned(FInfoStream), 'FInfoStream is not assigned.');

 Result := FInfoStream.Size;
end;

function TLunaGFX.GetLeft: Integer;
begin
 Result := FContainer.Left;
end;

function TLunaGFX.GetLineSize: Integer;
begin
 Result := GetImageLineSize(FContainer.Width,
                            FContainer.WidthRemainder,
                            FContainer.ImageFlags);
end;

function TLunaGFX.GetPaletteSize: Integer;
begin
 Result := FContainer.PaletteSize;
end;

function TLunaGFX.GetTop: Integer;
begin
 Result := FContainer.Left;
end;

function TLunaGFX.GetWidth: Integer;
begin
 if FContainer.FirstFrame.fiImage <> NIL then
  Result := FContainer.Width else
  Result := 0;
end;

procedure TLunaGFX.Initialize;
begin
 inherited;
 FContainer := TCustomImage.Create;
// NodeClass := TLunaGFX;
// SetColorFormat(@BGRAChannels, 4);
// FPixelReadFormat := prLeftToRight;
 FInfoStream := TMemStream.Create;
// FAssignableClass := TLunaGFX;
end;

procedure TLunaGFX.SetColorFormat(Data: PChannel; Count: Integer);
begin
 FContainer.FInternalClrFmt.SetFormat(GetColorFormatStr(Data, Count));
 UpdateColorTable;
end;

procedure TLunaGFX.SetConverter(const DstFlags: array of Word; DrawFlags: Integer);
var
 Flags: TCompositeFlags;
 I, SrcBCnt, DestBCnt: Integer;
begin
 FConverters[True].Free;
 FConverters[False].Free;
 DestBCnt := 0;
 for I := 0 to Length(DstFlags) - 1 do
  Inc(DestBCnt, (DstFlags[I] and 31) + 1);
 Flags := FContainer.FlagsList;
 DrawFlags := DrawFlags or (Integer(Container.ImageFormat) shl SRC_IMGFMT_SHIFT);
 SrcBCnt := Container.BitsCount;
 if Container.ImageFormat = ifRGB then
 begin
  DrawFlags := DrawFlags or DST_RGB;
  if SrcBCnt <> DestBCnt then
   DrawFlags := DrawFlags or DRAW_VALUE_CONVERT;
 end else
  if DestBCnt < SrcBCnt then
   DrawFlags := DrawFlags or DRAW_PIXEL_MODIFY;
 FConverters[False] := TBitmapConverter.Create(Flags, DstFlags, DrawFlags);
 FConverters[True] := TBitmapConverter.Create([IMG_COMPRESSED or
            ((SrcBCnt - 1) and 31)], DstFlags, DrawFlags or DRAW_TRANSPARENT);
end;

procedure TLunaGFX.SetInfoSize(Value: Integer);
begin
 Assert(Assigned(FInfoStream), 'FInfoStream is not assigned.');

 if (Value >= 0) and (Value <> InfoSize) then FInfoStream.Size := Value;
end;

procedure TLunaGFX.SetLeft(Value: Integer);
begin
 FContainer.Left := Value;
end;

procedure TLunaGFX.SetSize(AWidth, AHeight, ABitCount: Integer);
var
 UpdateCT: Boolean;
 LSZ, ImgSz, BCnt: Integer;
begin
 BCnt := FContainer.BitsCount;
 if not ABitCount in [0, 1, 2, 4, 8, 16, 24, 32] then
  raise ELunaGFXError.Create('Unknown BitCount.');
 if (AWidth <> Width) or (AHeight <> Height) or (ABitCount <> BCnt) then
 begin
  UpdateCT := ABitCount <> BCnt;
  FContainer.Width := AWidth;
  FContainer.Height := AHeight;
  FContainer.ImageFlags := ABitCount - 1;
  if ABitCount <= 8 then
   FContainer.ImageFormat := ifIndexed else
   FContainer.ImageFormat := ifRGB;
  Finalize(FContainer.CompositeFlags);
  if (AWidth > 0) and (AHeight > 0) and (ABitCount > 0) then
  begin
   if ABitCount < 8 then
   begin
    LSZ := (8 div ABitCount);
    FPixelSize := 0;
    if AWidth mod LSZ > 0 then
     LSZ := ((AWidth div LSZ) * LSZ + LSZ) div LSZ else
     LSZ := AWidth div LSZ;
   end Else
   begin
    FPixelSize := ABitCount shr 3;
    LSZ := AWidth * FPixelSize;
   end;
   ImgSz := LSZ * AHeight;
   with FContainer do
   begin
    if FirstFrame.fiImage <> NIL then FreeMem(FirstFrame.fiImage);
    GetMem(FirstFrame.fiImage, ImgSz);
    FillChar(FirstFrame.fiImage^, ImgSz, 0);
    if ABitCount < 24 then
     ColorsUsed := 1 shl ABitCount else
     ColorsUsed := 1 shl 24;
   end;
  end else with FContainer do
  begin
   if FirstFrame.fiImage <> NIL then
   begin
    FreeMem(FirstFrame.fiImage);
    FirstFrame.fiImage := NIL;
   end;
   FPixelSize := 0;
  end;
  FContainer.ImageData := FContainer.FirstFrame.fiImage;
  FContainer.ColorTable := FContainer.FirstFrame.fiPalette;
  FContainer.ColorFormat := FContainer.FInternalClrFmt;
  FreeAndNIL(FConverters[True]);
  FreeAndNIL(FConverters[False]);
  if UpdateCT then UpdateColorTable;
 end;
end;

procedure TLunaGFX.SetTop(Value: Integer);
begin
 FContainer.Top := Value;
end;

procedure TLunaGFX.UpdateColorTable;
var
 OldCols: Pointer;
 OldSize: Integer;
 BCnt, PaletteSize: Integer;
begin
 OldCols := FContainer.FirstFrame.fiPalette;
 OldSize := FContainer.PaletteSize;
 BCnt := FContainer.BitsCount;
 if BCnt <= 8 then
 begin
  PaletteSize := (1 shl BCnt) * ColorSize;
  GetMem(FContainer.FirstFrame.fiPalette, PaletteSize);
  if OldSize > PaletteSize then
   OldSize := PaletteSize;
  FillChar(FContainer.FirstFrame.fiPalette^, PaletteSize, 0);
  if OldCols <> nil then
   Move(OldCols^, FContainer.FirstFrame.fiPalette^, OldSize);
 end else FContainer.FirstFrame.fiPalette := NIL;
 if FContainer.ColorTable = OldCols then
  FContainer.ColorTable := FContainer.FirstFrame.fiPalette;
 if OldCols <> NIL then FreeMem(OldCols);
end;

function TLunaGFXSet.AddImage: TLunaGFX;
begin
 FLayersPrepared := False;
 Result := TLunaGFX.Create;
 AddCreated(Result, True);
end;

procedure TLunaGFXSet.Assign(Source: TNode);
begin
 inherited;
 FLayersPrepared := False;
 FName := TLunaGFXSet(Source).FName;
 FChildList.Assign(TLunaGFXSet(Source).FChildList);
end;

procedure TLunaGFXSet.PrepareLayers;
var
 I, X, Flg, Cnt: Integer;
 Ptr: PLongWord;
 ColorIndex: array of Byte;
 Temp: PRGBQuads;
begin
 if not FLayersPrepared then
 begin
  New(Temp);
  try
   if FMasterColorTable <> nil then
    Dispose(FMasterColorTable);
   New(FMasterColorTable);
   SetLength(ColorIndex, Count);
   Cnt := 0;
   for I := 0 to Count - 1 do
    with TLunaGFX(Nodes[I]), FContainer do
    begin
     if (ImageFormat <> ifIndexed) or FInternalClrFmt.Alpha then
     begin
      Cnt := 257;
      Break;
     end;
     FInternalClrFmt.ConvertToRGBQuad(FirstFrame.fiPalette, Temp, ColorsUsed);
     Flg := Cnt;
     Ptr := Pointer(FMasterColorTable);
     for X := 0 to Cnt - ColorsUsed do
     begin
      if CompareMem(Ptr, Temp, ColorsUsed * 4) then
      begin
       Flg := X;
       Break;
      end;
      Inc(Ptr);
     end;
     ColorIndex[I] := Flg;
     if Flg = Cnt then
     begin
      Inc(Cnt, ColorsUsed);
      if Cnt <= 256 then
       Move(Temp^, FMasterColorTable[Flg], ColorsUsed * 4) else
       Break;
     end;
    end;
   if Cnt <= 256 then
   begin
    for I := 0 to Count - 1 do
     with TLunaGFX(Nodes[I]), FContainer do
     begin
      X := ColorIndex[I];
      if (I > 0) or FNoBackground then
      begin
       if X > 0 then
        Flg := DRAW_TRANSPARENT or DRAW_PIXEL_MODIFY else
        Flg := DRAW_TRANSPARENT;
      end else
       Flg := 0;
      SetConverter([(8 - 1) or IMG_Y_FLIP], Flg);
      FConverters[False].SrcInfo.PixelModifier := X;
      FConverters[True].SrcInfo.PixelModifier := X;
     end;
   end else
   begin
    Dispose(FMasterColorTable);
    FMasterColorTable := nil;
    for I := 0 to Count - 1 do
     with TLunaGFX(Nodes[I]), FContainer do
     begin
      if (I > 0) or FNoBackground then
      begin
       if FInternalClrFmt.Alpha then
        Flg := DRAW_ALPHA_BLEND or DST_RGB or DRAW_VALUE_CONVERT else
        Flg := DRAW_TRANSPARENT or DST_RGB or DRAW_VALUE_CONVERT;
      end else
       Flg := DST_RGB or DRAW_VALUE_CONVERT;
      SetConverter([(32 - 1) or IMG_Y_FLIP], Flg);
     end;
   end;
   FLayersPrepared := True;
  finally
   Dispose(Temp);
  end;
 end;
end;

destructor TLunaGFXSet.Destroy;
begin
 if FMasterColorTable <> nil then Dispose(FMasterColorTable);
 FChildList.Free;
 Finalize(FName);
 inherited;
end;

function TLunaGFXSet.GetImage(Index: Integer): TLunaGFX;
begin
 Result := TLunaGFX(Nodes[Index]);
end;

procedure TLunaGFXSet.Initialize;
begin
 inherited;
 FNodeClass := TLunaGFX;
 FAssignableClass := TLunaGFXSet;
 FChildList := TLunaGFXList.Create;
end;

procedure TLunaGFXSet.LoadFromStream(Stream: TStream);
var
 Cnt, I, W, H, X, BCnt, ClUsed: Integer; HEAD: TGfxHead; Error: Boolean;
 Temp: PChannel; LoadChild: Boolean;
 ImgInfo: TLunaImgInfo;
 ClrInfo: TColorFormatRec;
 Flags: array of Word;
 Mode2: Boolean;
begin
 Stream.ReadBuffer(HEAD, SizeOf(HEAD));
 LoadChild := False;
 if HEAD = GFX_SET then with Stream do
 begin
  ReadBuffer(Cnt, 4);
  FInfoStream.Size := Cnt;
  if Cnt > 0 then ReadBuffer(FInfoStream.Memory^, Cnt);
  if (Read(Cnt, 4) = 4) and (Cnt >= 0) then
  begin
   SetLength(FName, Cnt);
   Cnt := Cnt shl 1;
   Mode2 := False;
  end else
  begin
   ReadBuffer(Cnt, 4);
   SetLength(FName, Cnt);
   Cnt := Cnt shl 1;
   Mode2 := True;
  end;
  if (Read(Pointer(FName)^, Cnt) = Cnt) and
     (Read(Cnt, 4) = 4) then
  begin
   if Mode2 then
   begin
    FLayersMode := Cnt and 1 <> 0;
    ReadBuffer(FMasterWidth, 2);
    ReadBuffer(FMasterHeight, 2);
    ReadBuffer(Cnt, 4);
   end else
   begin
    FLayersMode := False;
    FMasterWidth := 0;
    FMasterHeight := 0;
   end;
   LoadChild := Cnt and $80000000 <> 0;
   for I := 0 to Cnt and $FFFFFF - 1 do with AddImage do
   begin
    Error := True;
    if Read(ImgInfo, SizeOf(TLunaImgInfo)) = SizeOf(TLunaImgInfo) then
    {if (Read(FLeft, 4) = 4) and (Read(FTop, 4) = 4) and
       (Read(W, 4) = 4) and (Read(H, 4) = 4) and (Read(BC, 1) = 1) and
       (Read(ClUsed, 4) = 4) and
       (Read(FPixelReadFormat, 1) = 1) and (Read(CCnt, 4) = 4) and (CCnt > 0) then}
    begin
     BCnt := ImgInfo.liFlags1 and 63;
     SetLength(Flags, ImgInfo.liCompositeCount + 1);
     if (BCnt > 0) and (BCnt <= 32) and ((ImgInfo.liCompositeCount = 0) or
        (Read(Flags[1], ImgInfo.liCompositeCount * 2) =
                       ImgInfo.liCompositeCount * 2)) then
     begin
      Flags[0] := (BCnt - 1) or ((ImgInfo.liFlags1 shr 6) shl 14) or
                  (ImgInfo.liFlags2 shl 6) xor IMG_BITS_REVERSE;
      for W := 1 to Length(Flags) - 1 do Inc(BCnt, (Flags[W] and 31) + 1);
      if BCnt <= 32 then
      begin
       if ImgInfo.liChannelsCnt > 0 then
        GetMem(Temp, ImgInfo.liChannelsCnt * SizeOf(TChannel)) else
        Temp := nil;
       try
        if Temp <> nil then
        begin
         ReadBuffer(Temp^, ImgInfo.liChannelsCnt * SizeOf(TChannel));
         SetColorFormat(Temp, ImgInfo.liChannelsCnt);
        end else
        begin
         ReadBuffer(ClrInfo, SizeOf(TColorFormatRec));
         with FContainer.FInternalClrFmt do
         begin
          SetFormat(ClrInfo.colRedMask,  ClrInfo.colGreenMask,
                    ClrInfo.colBlueMask, ClrInfo.colAlphaMask);
          Flags := ClrInfo.colFlags;
          ColorSize := ClrInfo.colSize;
          UsedBits := ClrInfo.colBits;
         end;
        end;
        FContainer.Reallocate(ImgInfo.liWidth, ImgInfo.liHeight,
            ImgInfo.liImgFmt, Flags, nil, nil, ImgInfo.liWidthRemainder);
        FContainer.ColorsUsed := ImgInfo.liColorsUsed;
        FPixelSize := FContainer.FInternalClrFmt.ColorSize;
        FreeAndNIL(FConverters[True]);
        FreeAndNIL(FConverters[False]);
        if (Read(W, 4) = 4) and (Read(H, 4) = 4) and (W = FContainer.ImageSize) then
        begin
         SetInfoSize(H);
         ClUsed := FContainer.PaletteSize;
         if Mode2 then
         begin
          ReadBuffer(Info^, H);
          ReadBuffer(Cnt, 4);
          if Cnt < 1 then BadDataError;
          with FContainer.FirstFrame do
          begin
           ReadBuffer(fiImgCmprSize, 4);
           ReadBuffer(fiPalCmprSize, 4);
           ReadBuffer(fiDelay, 4);
           if fiImgCmprSize > 0 then
           begin
            W := fiImgCmprSize;
            ReallocMem(fiImage, W);
           end;
           ReadBuffer(fiImage^, W);
           if fiPalCmprSize > 0 then
           begin
            ClUsed := fiPalCmprSize;
            ReallocMem(fiPalette, ClUsed);
           end;
           ReadBuffer(fiPalette^, ClUsed);
          end;
          Dec(Cnt);
          for X := 1 to Cnt do with FContainer.AddFrame^ do
          begin
           ReadBuffer(fiImgCmprSize, 4);
           ReadBuffer(fiPalCmprSize, 4);
           ReadBuffer(fiDelay, 4);
           if fiImgCmprSize > 0 then
            W := fiImgCmprSize else
            W := FContainer.ImageSize;
           GetMem(fiImage, W);
           ReadBuffer(fiImage^, W);
           if fiPalCmprSize > 0 then
            W := fiPalCmprSize else
            W := FContainer.PaletteSize;
           GetMem(fiPalette, W);
           ReadBuffer(fiPalette^, W);
          end;
          Error := False;
         end else
         if (Read(FContainer.FirstFrame.fiImage^, W) = W) and ((ImgInfo.liImgFmt <> ifIndexed) or
            (Read(FContainer.FirstFrame.fiPalette^, ClUsed) = ClUsed)) and
            (Read(Info^, H) = H) then Error := False;
         FContainer.CurrentFrame := Addr(FContainer.FirstFrame);            
        end;
       finally
        if Temp <> nil then FreeMem(Temp);
       end;
      end;
     end;
    end;
    if Error then BadDataError;
   end;
  end;
 end else BadDataError;
 if LoadChild then
  FChildList.ChildLoadFromStream(Stream) else
  FChildList.Clear;  
end;

procedure TLunaGFXSet.SaveToStream(Stream: TStream);
var
 N: TNode; Cnt, X: Integer;
 ImgInfo: TLunaImgInfo;
 ClrInfo: TColorFormatRec;
 FRM: PFrameInfo;
begin
 Stream.WriteBuffer(GFX_SET, SizeOf(GFX_SET));
 Cnt := FInfoStream.Size;
 Stream.WriteBuffer(Cnt, 4);
 with Stream do
 begin
  WriteBuffer(FInfoStream.Memory^, Cnt);
  Cnt := -1;
  WriteBuffer(Cnt, 4);
  Cnt := Length(FName);
  WriteBuffer(Cnt, 4);
  WriteBuffer(Pointer(FName)^, Cnt shl 1);
  Cnt := Byte(FLayersMode);
  WriteBuffer(Cnt, 4);
  WriteBuffer(FMasterWidth, 2);
  WriteBuffer(FMasterHeight, 2);
  if FChildList.Count > 0 then
   X := (Count and $FFFFFF) or Integer($80000000) else
   X := (Count and $FFFFFF);
  WriteBuffer(X, 4);
  N := RootNode;
  while N <> NIL do
  begin
   with TLunaGFX(N) do
   begin
    ImgInfo.liLeft := FContainer.Left;
    ImgInfo.liTop := FContainer.Top;
    ImgInfo.liWidth := FContainer.Width;
    ImgInfo.liWidthRemainder := FContainer.WidthRemainder;
    ImgInfo.liHeight := FContainer.Height;
    ImgInfo.liImgFmt := FContainer.ImageFormat;
    ImgInfo.liCompositeCount := Length(FContainer.CompositeFlags);
    ImgInfo.liFlags1 := ((FContainer.ImageFlags and 31) + 1) or
                         ((FContainer.ImageFlags shr 14) shl 6);
    ImgInfo.liColorsUsed := FContainer.ColorsUsed;
    ImgInfo.liFlags2 := (FContainer.ImageFlags xor IMG_BITS_REVERSE) shr 6;
    if FContainer.FInternalClrFmt <> NIL then
    with FContainer.FInternalClrFmt do
    begin
     ImgInfo.liChannelsCnt := -1;
     ClrInfo.colFlags := Flags;
     ClrInfo.colSize := ColorSize;
     ClrInfo.colBits := UsedBits;
     ClrInfo.colBlueMask := BlueMask;
     ClrInfo.colGreenMask := GreenMask;
     ClrInfo.colRedMask := RedMask;
     ClrInfo.colAlphaMask := AlphaMask;
    end else
     ImgInfo.liChannelsCnt := 0;
    WriteBuffer(ImgInfo, SizeOf(TLunaImgInfo));
    if ImgInfo.liCompositeCount > 0 then
     WriteBuffer(Pointer(FContainer.CompositeFlags)^, ImgInfo.liCompositeCount * 2);
    if ImgInfo.liChannelsCnt <> 0 then
     WriteBuffer(ClrInfo, SizeOf(TColorFormatRec));
    X := ImageSize;
    WriteBuffer(X, 4);
    Cnt := InfoSize;
    WriteBuffer(Cnt, 4);
    WriteBuffer(Info^, Cnt);
    Cnt := FContainer.FramesCount;
    WriteBuffer(Cnt, 4);
    FRM := Addr(FContainer.FirstFrame);
    while FRM <> nil do with FRM^ do
    begin
     WriteBuffer(fiImgCmprSize, 4);
     WriteBuffer(fiPalCmprSize, 4);
     WriteBuffer(fiDelay, 4);
     if fiImgCmprSize > 0 then
      WriteBuffer(fiImage^, fiImgCmprSize) else
      WriteBuffer(fiImage^, X);
     if fiPalCmprSize > 0 then
      WriteBuffer(fiPalette^, fiPalCmprSize) else
      WriteBuffer(fiPalette^, FContainer.PaletteSize);
     FRM := fiNext;
    end;
   end;
   N := N.Next;
  end;
 end;
 if FChildList.Count > 0 then FChildList.ChildSaveToStream(Stream);
end;

function TLunaGFXList.AddSet: TLunaGFXSet;
begin
 Result := TLunaGFXSet.Create;
 AddCreated(Result, True);
end;

procedure TLunaGFXList.ChildLoadFromStream(Stream: TStream);
var
 I, Cnt: Integer;
begin
 Stream.ReadBuffer(Cnt, 4);
 FInfoStream.Size := Cnt;
 Stream.ReadBuffer(FInfoStream.Memory^, Cnt);
 Stream.ReadBuffer(Cnt, 4);
 Clear;
 DoProgress(0);
 DoProgressMax(Cnt);
 for I := 1 to Cnt do
 begin
  AddSet.LoadFromStream(Stream);
  DoProgress(I);
 end;
end;

procedure TLunaGFXList.ChildSaveToStream(Stream: TStream);
var
 Cnt, I: Integer;
 N: TNode;
begin
 Cnt := FInfoStream.Size;
 Stream.WriteBuffer(Cnt, 4);
 Stream.WriteBuffer(FInfoStream.Memory^, Cnt);
 Stream.WriteBuffer(Count, 4);
 DoProgress(0);
 DoProgressMax(Count);
 I := 0;
 N := RootNode;
 while N <> NIL do
 begin
  TLunaGFXSet(N).SaveToStream(Stream);
  Inc(I);  
  DoProgress(I);
  N := N.Next;
 end;
end;

procedure TLunaGFXList.DoProgress(Value: Integer);
begin
 if Assigned(FOnProgress) then FOnProgress(Self, Value);
end;

procedure TLunaGFXList.DoProgressMax(Value: Integer);
begin
 if Assigned(FOnProgressMax) then FOnProgressMax(Self, Value);
end;

function TLunaGFXList.GetSet(Index: Integer): TLunaGFXSet;
begin
 Result := TLunaGFXSet(Nodes[Index]);
end;

procedure TLunaGFXList.Initialize;
begin
 inherited;
 FNodeClass := TLunaGFXSet;
 FAssignableClass := TLunaGFXList;
 FCompressionLevel := zcFastest;
end;

procedure TLunaGFXList.LoadFromStream(Stream: TStream);
var
 HEAD: TGfxHead;
 DS: TStream;
begin
 Stream.ReadBuffer(HEAD, SizeOf(HEAD));
 if HEAD = GFX_HEAD then
 begin
  DS := TZDecompressionStream.Create(Stream);
  try
   ChildLoadFromStream(DS);
  finally
   DS.Free;
  end;
 end else raise ELunaGFXListError.Create('Invalid file format.');
end;

procedure TLunaGFXList.SaveToStream(Stream: TStream);
var
 CS: TStream;
begin
 Stream.WriteBuffer(GFX_HEAD, SizeOf(GFX_HEAD));
 CS := TZCompressionStream.Create(Stream, FCompressionLevel);
 try
  ChildSaveToStream(CS);
 finally
  CS.Free;
 end;
end;

function TLunaGFXAdapter.AddFrame(ImgSz, PalSz: LongInt): PlugInterface.PFrameInfo;
begin
 Result := Pointer(FSource.FContainer.AddFrame);
 GetMem(Result.fiImage, ImgSz);
 GetMem(Result.fiPalette, PalSz); 
end;

procedure TLunaGFXAdapter.CompressImage(Frame: PlugInterface.PFrameInfo);
begin
 FSource.FContainer.Compress(Pointer(Frame));
end;

procedure TLunaGFXAdapter.CompressPalette(Frame: PlugInterface.PFrameInfo;
  SkipColor: LongWord; Cnt: Integer);
begin
 FSource.FContainer.CompressPalette(Pointer(Frame), SkipColor, Cnt);
end;

constructor TLunaGFXAdapter.Create(Source: TLunaGFX; FreeAfterUse: Boolean);
begin
 if Source = NIL then raise ELunaGFXError.Create('Source must not be nil');
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

destructor TLunaGFXAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

procedure TLunaGFXAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TLunaGFXAdapter.GetBitCount: Integer;
begin
 Result := FSource.BitCount;
end;

function TLunaGFXAdapter.GetColorFormat: IColorFormat;
begin
 Result := TColorFormatAdapter.Create(FSource.FContainer.FInternalClrFmt, False);
end;

function TLunaGFXAdapter.GetColorSize: Integer;
begin
 Result := FSource.ColorSize;
end;

function TLunaGFXAdapter.GetColorsUsed: Integer;
begin
 Result := FSource.FContainer.ColorsUsed;
end;

function TLunaGFXAdapter.GetColorTable: Pointer;
begin
 Result := FSource.FContainer.FirstFrame.fiPalette;
end;

procedure TLunaGFXAdapter.GetDIBColorTable(Dest: Pointer; Count: Integer);
begin
 FSource.GetDIBColorTable(Dest, Count);
end;

function TLunaGFXAdapter.GetFirstFrame: PlugInterface.PFrameInfo;
begin
 Result := Addr(FSource.FContainer.FirstFrame);
end;

function TLunaGFXAdapter.GetFramesCount: Integer;
begin
 Result := FSource.FContainer.FramesCount + 1;
end;

function TLunaGFXAdapter.GetHeight: Integer;
begin
 Result := FSource.FContainer.Height;
end;

function TLunaGFXAdapter.GetImageSize: Integer;
begin
 Result := FSource.FContainer.ImageSize;
end;

function TLunaGFXAdapter.GetImgBuf: Pointer;
begin
 Result := FSource.FContainer.FirstFrame.fiImage;
end;

function TLunaGFXAdapter.GetInfo: Pointer;
begin
 Result := FSource.Info;
end;

function TLunaGFXAdapter.GetInfoSize: Integer;
begin
 Result := FSource.InfoSize;
end;

function TLunaGFXAdapter.GetInfoStream: IStream32;
begin
 Result := TStreamAdapter.Create(FSource.FInfoStream);
end;

function TLunaGFXAdapter.GetLeft: Integer;
begin
 Result := FSource.FContainer.Left;
end;

function TLunaGFXAdapter.GetLineSize: Integer;
begin
 Result := FSource.LineSize;
end;

function TLunaGFXAdapter.GetPaletteSize: Integer;
begin
 Result := FSource.FContainer.PaletteSize;
end;

function TLunaGFXAdapter.GetPixelReadFormat: TPixelReadFormat;
begin
 if FSource.FContainer.ImageFlags and IMG_BITS_REVERSE <> 0 then
  Result := prRightToLeft else
  Result := prLeftToRight;
end;

function TLunaGFXAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TLunaGFXAdapter.GetTop: Integer;
begin
 Result := FSource.FContainer.Top;
end;

function TLunaGFXAdapter.GetWidth: Integer;
begin
 Result := FSource.FContainer.Width;
end;

procedure TLunaGFXAdapter.SetColorFormat(Data: PChannel; Count: Integer);
begin
 FSource.SetColorFormat(Data, Count);
end;

procedure TLunaGFXAdapter.SetColorsUsed(Value: Integer);
begin
 FSource.FContainer.ColorsUsed := Value;
end;

procedure TLunaGFXAdapter.SetInfoSize(Value: Integer);
begin
 FSource.InfoSize := Value;
end;

procedure TLunaGFXAdapter.SetLeft(Value: Integer);
begin
 FSource.FContainer.Left := Value;
end;

procedure TLunaGFXAdapter.SetPixelReadFormat(Value: TPixelReadFormat);
begin
 with FSource.FContainer do
 case Value of
  prRightToLeft: ImageFlags := ImageFlags or IMG_BITS_REVERSE;
  prLeftToRight: ImageFlags := ImageFlags and not IMG_BITS_REVERSE;
 end;
end;

procedure TLunaGFXAdapter.SetSize(AWidth, AHeight, ABitCount: Integer);
begin
 FSource.SetSize(AWidth, AHeight, ABitCount);
end;

procedure TLunaGFXAdapter.SetTop(Value: Integer);
begin
 FSource.FContainer.Top := Value;
end;

function TLunaGFXSetAdapter.AddImage: ILunaGFX;
begin
 Result := TLunaGFXAdapter.Create(FSource.AddImage, False);
end;

procedure TLunaGFXSetAdapter.Clear;
begin
 FSource.Clear;
end;

constructor TLunaGFXSetAdapter.Create(Source: TLunaGFXSet; FreeAfterUse: Boolean);
begin
 if Source = NIL then raise ELunaGFXSetError.Create('Source must not be nil');
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

destructor TLunaGFXSetAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

procedure TLunaGFXSetAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TLunaGFXSetAdapter.GetChildList: ILunaGFXList;
begin
 Result := TLunaGFXListAdapter.Create(FSource.ChildList, False);
end;

function TLunaGFXSetAdapter.GetCount: Integer;
begin
 Result := FSource.Count;
end;

function TLunaGFXSetAdapter.GetImage(Index: Integer): ILunaGFX;
begin
 Result := TLunaGFXAdapter.Create(FSource.Images[Index], False);
end;

function TLunaGFXSetAdapter.GetInfoStream: IStream32;
begin
 Result := TStreamAdapter.Create(FSource.FInfoStream);
end;

function TLunaGFXSetAdapter.GetLayersMode: LongBool;
begin
 Result := FSource.FLayersMode;
end;

function TLunaGFXSetAdapter.GetMasterHeight: Integer;
begin
 Result := FSource.FMasterHeight;
end;

function TLunaGFXSetAdapter.GetMasterWidth: Integer;
begin
 Result := FSource.FMasterWidth;
end;

function TLunaGFXSetAdapter.GetName: PWideChar;
begin
 Result := Pointer(FSource.FName);
end;

function TLunaGFXSetAdapter.GetNoBackground: LongBool;
begin
 Result := FSource.FNoBackground;
end;

function TLunaGFXSetAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

procedure TLunaGFXSetAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 FSource.LoadFromStream(TStream(Stream.GetPtr));
end;

procedure TLunaGFXSetAdapter.RemoveImage(const Image: ILunaGFX);
begin
 if Image = NIL then Exit;
 FSource.Remove(TNode(Image.GetPtr));
end;

procedure TLunaGFXSetAdapter.SaveToStream(const Stream: IReadWrite);
begin
 FSource.SaveToStream(TStream(Stream.GetPtr));
end;

procedure TLunaGFXSetAdapter.SetLayersMode(Value: LongBool);
begin
 FSource.FLayersMode := Value;
end;

procedure TLunaGFXSetAdapter.SetMasterHeight(Value: Integer);
begin
 FSource.FMasterHeight := Value;
end;

procedure TLunaGFXSetAdapter.SetMasterWidth(Value: Integer);
begin
 FSource.FMasterWidth := Value;
end;

procedure TLunaGFXSetAdapter.SetName(Value: PWideChar);
begin
 FSource.FName := Value;
end;

procedure TLunaGFXSetAdapter.SetNoBackground(Value: LongBool);
begin
 FSource.FNoBackground := Value;
end;

(* TLunaGFXListAdapter *)

function TLunaGFXListAdapter.AddSet: ILunaGFXSet;
begin
 Result := TLunaGFXSetAdapter.Create(FSource.AddSet, False);
end;

procedure TLunaGFXListAdapter.Clear;
begin
 FSource.Clear;
end;

constructor TLunaGFXListAdapter.Create(Source: TLunaGFXList; FreeAfterUse: Boolean);
begin
 if Source = NIL then raise ELunaGFXListError.Create('Source must not be nil');
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

destructor TLunaGFXListAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

procedure TLunaGFXListAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TLunaGFXListAdapter.GetCount: Integer;
begin
 Result := FSource.Count;
end;

function TLunaGFXListAdapter.GetInfoStream: IStream32;
begin
 Result := TStreamAdapter.Create(FSource.FInfoStream);
end;

function TLunaGFXListAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource)
end;

function TLunaGFXListAdapter.GetSet(Index: Integer): ILunaGFXSet;
begin
 Result := TLunaGFXSetAdapter.Create(FSource.Sets[Index], False);
end;

procedure TLunaGFXListAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 FSource.LoadFromStream(TStream(Stream.GetPtr));
end;

procedure TLunaGFXListAdapter.RemoveSet(const ASet: ILunaGFXSet);
begin
 if ASet = NIL then Exit;
 FSource.Remove(TNode(ASet.GetPtr));
end;

procedure TLunaGFXListAdapter.SaveToStream(const Stream: IReadWrite);
begin
 FSource.SaveToStream(TStream(Stream.GetPtr));
end;

destructor TInfoList.Destroy;
begin
 FInfoStream.Free;
end;

procedure TInfoList.Initialize;
begin
 FInfoStream := TMemStream.Create;
end;

procedure TColorFormatAdapter.ConvertFromRQBQuad(Source, Dest: Pointer;
  Count: Integer);
begin
 FSource.ConvertFromRGBQuad(Source, Dest, Count);
end;

procedure TColorFormatAdapter.ConvertTo(const DestFormat: IColorFormat;
  Source, Dest: Pointer; Count: Integer);
begin
 FSource.ConvertTo(TColorFormat(DestFormat.GetPtr), Source, Dest, Count);
end;

procedure TColorFormatAdapter.ConvertToRGBQuad(Source, Dest: Pointer;
  Count: Integer);
begin
 FSource.ConvertToRGBQuad(Source, Dest, Count);
end;

constructor TColorFormatAdapter.Create(Source: TColorFormat;
  FreeAfterUse: Boolean);
begin
 if Source = NIL then raise EColorFormatError.Create('Source must not be nil');
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

destructor TColorFormatAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
end;

procedure TColorFormatAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TColorFormatAdapter.GetColorBits: LongInt;
begin
 Result := FSource.ColorBits;
end;

function TColorFormatAdapter.GetColorSize: LongInt;
begin
 Result := FSource.ColorSize;
end;

function TColorFormatAdapter.GetFlags: LongWord;
begin
 Result := FSource.Flags;
end;

function TColorFormatAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TColorFormatAdapter.GetUsedBits: LongInt;
begin
 Result := FSource.UsedBits;
end;

procedure TColorFormatAdapter.SetFlags(Value: LongWord);
begin
 FSource.Flags := Value;
end;

end.
