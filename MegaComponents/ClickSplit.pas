unit ClickSplit;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls;

type
  TClickSplitter = class(TSplitter)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
   property OnClick;
   property OnDblClick;
   property OnMouseDown;
   property OnMouseUp;
   property OnMouseMove;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Additional', [TClickSplitter]);
end;

end.
