unit HSColorPickerEx;

interface

uses
 Windows, Messages, SysUtils, Classes, Controls, Graphics, Math, Forms,
 RGBHSLUtils, HTMLColors, SelPropUtils, mbColorPickerControl, Scanlines,
 BitmapEx;

type
 THSColorPickerEx = class(TmbColorPickerControl)
 private
  FSelected: TColor;
  FHSLBmp: TBitmap;
  FOnChange: TNotifyEvent;
  FHue, FSaturation, FLuminance: integer;
  FLum: integer;
  FManual: boolean;
  dx, dy, mxx, myy: integer;
  FColorFormat: TColorFormat;
  FConvert: Boolean;
  procedure SetHValue(h: integer);
  procedure SetSValue(s: integer);
  procedure SetColorFormat(Value: TColorFormat);
  function GetFormattedColor(Value: TColor): TColor;  
 protected
  function GetSelectedColor: TColor; override;
  procedure WebSafeChanged; override;
  procedure SetSelectedColor(c: TColor); override;
  procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN;
  procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
  procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  procedure DrawMarker(x, y: integer);
  procedure Paint; override;
  procedure CreateHSLGradient;
  procedure Resize; override;
  procedure CreateWnd; override;
  procedure CorrectCoords(var x, y: integer);
  function PredictColor: TColor;
 public
  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;

  function GetColorAtPoint(x, y: integer): TColor; override;
  property Lum: integer read FLum write FLum default 120;
  property Manual: boolean read FManual;
  property ColorFormat: TColorFormat read FColorFormat write SetColorFormat;  
 published
  property SelectedColor default clRed;
  property HueValue: integer read FHue write SetHValue default 0;
  property SaturationValue: integer read FSaturation write SetSValue default 240;
  property MarkerStyle default msCross;

  property OnChange: TNotifyEvent read FOnChange write FOnChange;
 end;

implementation

{THSColorPickerEx}

constructor THSColorPickerEx.Create(AOwner: TComponent);
begin
 inherited;
 FColorFormat := TColorFormat.Create;
 FColorFormat.Assign(RGBZ_ColorFormat);
{ FHSLBmp := TBitmap.Create;
 FHSLBmp.PixelFormat := pf32bit;
 FHSLBmp.Width := 240;//361;
 FHSLBmp.Height := 241;}
 Width := 256;
 Height := 256;
 HintFormat := 'H: %h S: %hslS'#13'Hex: %hex';
 FHue := 0;
 FSaturation := 240;
 FLuminance := 120;
 FSelected := clRed;
 FLum := 120;
 FManual := false;
 dx := 0;
 dy := 0;
 mxx := 0;
 myy := 0;
 MarkerStyle := msCross;
end;

destructor THSColorPickerEx.Destroy;
begin
 FHSLBmp.Free;
 FColorFormat.Free;
 inherited;
end;

procedure THSColorPickerEx.CreateWnd;
begin
 inherited;
 CreateHSLGradient;
end;

procedure THSColorPickerEx.CreateHSLGradient;
var
 Hue, Sat : integer;
 row: pRGBQuadArray;
 Temp: TBitmap;
begin
 if FHSLBmp = nil then
 begin
  FHSLBmp := TBitmap.Create;
  FHSLBmp.PixelFormat := pf32bit;
  FHSLBmp.Width := Width;//361;
  FHSLBmp.Height := Height;
 end;
 Temp := TBitmap.Create;
 try
  Temp.PixelFormat := pf32bit;
  Temp.Width := 240;
  Temp.Height := 241;
  for Sat := 0 to 240 do
  begin
   row := Temp.ScanLine[240 - Sat];
   for Hue := 0 to 239 do //360 do
    row[Hue] := RGBToRGBQuad(GetFormattedColor(HSLRangeToRGB(Hue, Sat, 120)))
  end;

  with FHSLBmp.Canvas do
   StretchDraw(ClipRect, Temp);
 finally
  Temp.Free;
 end;
end;

procedure THSColorPickerEx.CorrectCoords(var x, y: integer);
begin
 if x < 0 then x := 0;
 if y < 0 then y := 0;
 if x > Width - 1 then x := Width - 1;
 if y > Height - 1 then y := Height - 1;
end;

procedure THSColorPickerEx.DrawMarker(x, y: integer);
var
 c: TColor;
begin
 CorrectCoords(x, y);
 RGBtoHSLRange(FSelected, FHue, FSaturation, FLuminance);
 if Assigned(FOnChange) then
  FOnChange(Self);
 dx := x;
 dy := y;
 if Focused or (csDesigning in ComponentState) then
  c := clBlack
 else
  c := clWhite;
 case MarkerStyle of
  msCircle: DrawSelCirc(x, y, Canvas);
  msSquare: DrawSelSquare(x, y, Canvas);
  msCross: DrawSelCross(x, y, Canvas, c);
  msCrossCirc: DrawSelCrossCirc(x, y, Canvas, c);
 end;
end;

function THSColorPickerEx.GetSelectedColor: TColor;
begin
 Result := FSelected;
end;

procedure THSColorPickerEx.SetSelectedColor(c: TColor);
begin
 c := GetFormattedColor(c);
 RGBtoHSLRange(c, FHue, FSaturation, FLuminance);
 FSelected := c;
 FManual := false;
 mxx := Round(FHue*(Width/239{360}));
 myy := Round((240-FSaturation)*(Height/240));
 Invalidate;
end;

procedure THSColorPickerEx.Paint;
begin
 Canvas.Draw(0, 0, FHSLBmp);
// Canvas.StretchDraw(ClientRect, FHSLBmp);
// CorrectCoords(mxx, myy);
 DrawMarker(mxx, myy);
end;

procedure THSColorPickerEx.Resize;
begin
 SetSelectedColor(FSelected);
 inherited;
end;

procedure THSColorPickerEx.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 R: TRect;
begin
 inherited;
 mxx := x;
 myy := y;
 if Button = mbLeft then
  begin
   R := ClientRect;
   R.TopLeft := ClientToScreen(R.TopLeft);
   R.BottomRight := ClientToScreen(R.BottomRight);
   ClipCursor(@R);
   FSelected := GetColorAtPoint(x, y);
   FManual := true;
   Invalidate;
  end;
 SetFocus;
end;

procedure THSColorPickerEx.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 ClipCursor(nil);
 mxx := x;
 myy := y;
 FSelected := GetColorAtPoint(x, y);
 FManual := true;
 Invalidate;
end;

procedure THSColorPickerEx.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 if ssLeft in Shift then
  begin
   mxx := x;
   myy := y;
   FSelected := GetColorAtPoint(x, y);
   FManual := true;
   Invalidate;
  end;
end;

function THSColorPickerEx.PredictColor: TColor;
var
 FTHue, FTSat, FTLum: integer;
begin
 RGBtoHSLRange(GetColorUnderCursor, FTHue, FTSat, FTLum);
 Result := HSLRangeToRGB(FTHue, FTSat, FLum);
end;

procedure THSColorPickerEx.CNKeyDown(var Message: TWMKeyDown);
var
 Shift: TShiftState;
 FInherited: boolean;
begin
 FInherited := false;
 Shift := KeyDataToShiftState(Message.KeyData);
 if not (ssCtrl in Shift) then
  case Message.CharCode of
   VK_LEFT:
    begin
     mxx := dx - 1;
     myy := dy;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_RIGHT:
    begin
     mxx := dx + 1;
     myy := dy;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_UP:
    begin
     mxx := dx;
     myy := dy - 1;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_DOWN:
    begin
     mxx := dx;
     myy := dy + 1;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
  else
   begin
    FInherited := true;
    inherited;
   end;
  end
 else
  case Message.CharCode of
   VK_LEFT:
    begin
     mxx := dx - 10;
     myy := dy;
     Refresh;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_RIGHT:
    begin
     mxx := dx + 10;
     myy := dy;
     Refresh;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_UP:
    begin
     mxx := dx;
     myy := dy - 10;
     Refresh;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
   VK_DOWN:
    begin
     mxx := dx;
     myy := dy + 10;
     Refresh;
     FSelected := GetColorAtPoint(mxx, myy);
     FManual := true;
     Invalidate;
    end;
  else
   begin
    FInherited := true;
    inherited;
   end;
  end;
 if not FInherited then
  if Assigned(OnKeyDown) then
   OnKeyDown(Self, Message.CharCode, Shift);
end;

procedure THSColorPickerEx.SetHValue(h: integer);
begin
 if h > 239 then h := 239;
// if h > 360 then h := 360;
 if h < 0 then h := 0;
 FHue := h;
 SetSelectedColor(HSLRangeToRGB(FHue, FSaturation, 120));
end;

procedure THSColorPickerEx.SetSValue(s: integer);
begin
 if s > 240 then s := 240;
 if s < 0 then s := 0;
 FSaturation := s;
 SetSelectedColor(HSLRangeToRGB(FHue, FSaturation, 120));
end;

function THSColorPickerEx.GetColorAtPoint(x, y: integer): TColor;
begin
 {if x < 0 then
  x := 0 else
 if x >= Width then
  x := Width - 1;

 if y < 0 then
  y := 0 else
 if y >= Height then
  y := Height - 1;
                  }
 Result := FHSLBmp.Canvas.Pixels[x, y];
end;

procedure THSColorPickerEx.WebSafeChanged;
begin
 inherited;
 CreateHSLGradient;
 Invalidate;
end;

procedure THSColorPickerEx.SetColorFormat(Value: TColorFormat);
begin
 FColorFormat.Assign(Value);
 FConvert := not FColorFormat.SameAs(RGBZ_ColorFormat);
 CreateHSLGradient;
 Invalidate;
end;

function THSColorPickerEx.GetFormattedColor(Value: TColor): TColor;
begin
 if FConvert then
 begin
  Value := RGBZ_ColorFormat.ConvertValue(FColorFormat, Value);
  Result := FColorFormat.ConvertValue(RGBZ_ColorFormat, Value);
 end else
 if WebSafe then
  Result := GetWebSafe(Value) else
  Result := Value;
end;

end.

