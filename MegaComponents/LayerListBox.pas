unit LayerListBox;

interface

uses
 Windows, Messages, SysUtils, Classes, Controls, StdCtrls, Graphics;

type
  TOnDrawPicEvent = procedure (Sender: TObject; AIndex: Integer;
                    Rect: TRect; State: TOwnerDrawState; var Handled: Boolean) of object;

  TItemClickMode = (llbFlipsDisabled, llbChecked, llbVisible, llbXFlip, llbYFlip, llbTile);
  TItemClickModes = set of TItemClickMode;

  TGetItemClickModeEvent = procedure (Sender: TObject; AIndex: Integer;
                                   var AModes: TItemClickModes) of object;

  TGetItemClickEvent = procedure(Sender: TObject; AIndex: Integer;
                         ChangedMode: TItemClickMode) of object;

  TUnicodeGetDataEvent = procedure(Control: TWinControl; Index: Integer;
    var Data: WideString) of object;

  TLayerListBox = class(TCustomListBox)
  private
    FVisibleBoxRect: TRect;
    FCheckedBoxRect: TRect;
    FXBoxRect: TRect;
    FYBoxRect: TRect;
    FPics: array[0..5] of TBitmap;
    FOnDrawPic: TOnDrawPicEvent;
    FOnGetItemClickMode: TGetItemClickModeEvent;
    FOnUnicodeGetData: TUnicodeGetDataEvent;
    FOnItemClick: TGetItemClickEvent;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure WMLButtonDown(var Message: TWMLButtonDown); message WM_LBUTTONDOWN;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure CNDrawItem(var Message: TWMDrawItem); message CN_DRAWITEM;
    procedure ItemClickTest(var Message: TWMMouse);
  protected
    procedure DrawItem(AIndex: Integer; Rect: TRect;
                         State: TOwnerDrawState); override;
    procedure DoDrawPic(AIndex: Integer; Rect: TRect;
                         State: TOwnerDrawState); virtual;
    procedure DoGetModes(AIndex: Integer; var AModes: TItemClickModes); virtual;
    procedure DoItemClick(AIndex: Integer; ChangedMode: TItemClickMode); virtual;
    function DoGetData(Index: Integer): WideString; virtual;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property OnGetItemClickMode: TGetItemClickModeEvent read FOnGetItemClickMode
                                               write FOnGetItemClickMode;
    property OnDrawPic: TOnDrawPicEvent read FOnDrawPic
                                       write FOnDrawPic;
    property OnItemClick: TGetItemClickEvent read FOnItemClick
                                            write FOnItemClick;
    property OnUnicodeGetData: TUnicodeGetDataEvent read FOnUnicodeGetData
                                                   write FOnUnicodeGetData; 


    property Style;
    property AutoComplete;
    property Align;
    property Anchors;
    property BevelEdges;
    property BevelInner;
    property BevelKind default bkNone;
    property BevelOuter;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Columns;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property ExtendedSelect;
    property Font;
    property ImeMode;
    property ImeName;
    property IntegralHeight;
    property ItemHeight default 34;
    property Items;
    property MultiSelect;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ScrollWidth;
    property ShowHint;
    property Sorted;
    property TabOrder;
    property TabStop;
    property TabWidth;
    property Visible;
    property OnClick;
    property OnContextPopup;
    property OnData;
    property OnDataFind;
    property OnDataObject;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

const
 X_OK_PIC    = 0;
 X_FLIP_PIC  = 1;
 Y_OK_PIC    = 2;
 Y_FLIP_PIC  = 3;
 EYE_PIC     = 4;
 CHECK_PIC   = 5;

implementation

uses MyClasses;

{ TLayerListBox }


procedure TLayerListBox.CNDrawItem(var Message: TWMDrawItem);
var
 State: TOwnerDrawState;
begin
 with Message.DrawItemStruct^ do
 begin
  State := TOwnerDrawState(LongRec(itemState).Lo);
  Canvas.Handle := hDC;
  Canvas.Font := Font;
  Canvas.Brush := Brush;
  
  if (Integer(itemID) >= 0) and (odSelected in State) then
  begin
   Canvas.Brush.Color := clHighlight;
   Canvas.Font.Color := clHighlightText
  end;

  if Integer(itemID) >= 0 then
   DrawItem(itemID, rcItem, State) else

  Canvas.FillRect(rcItem);

  if odFocused in State then
   DrawFocusRect(hDC, rcItem);

  Canvas.Handle := 0;
 end;
end;

constructor TLayerListBox.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 if not (csDesigning in ComponentState) then
 begin
  FPics[X_FLIP_PIC] := TBitmap.Create;
  FPics[X_FLIP_PIC].LoadFromResourceName(HInstance, 'THE_X_FLIP');
  FPics[X_OK_PIC] := TBitmap.Create;
  FPics[X_OK_PIC].LoadFromResourceName(HInstance, 'THE_NO_X_FLIP');
  FPics[Y_FLIP_PIC] := TBitmap.Create;
  FPics[Y_FLIP_PIC].LoadFromResourceName(HInstance, 'THE_Y_FLIP');
  FPics[Y_OK_PIC] := TBitmap.Create;
  FPics[Y_OK_PIC].LoadFromResourceName(HInstance, 'THE_NO_Y_FLIP');
  FPics[CHECK_PIC] := TBitmap.Create;
  FPics[CHECK_PIC].LoadFromResourceName(HInstance, 'THE_CHECK');
  FPics[EYE_PIC] := TBitmap.Create;
  FPics[EYE_PIC].LoadFromResourceName(HInstance, 'THE_EYE');
 end;
 ItemHeight := 34;
end;

destructor TLayerListBox.Destroy;
var
 I: Integer;
begin
 for I := 0 to 5 do
  FPics[I].Free;
 inherited;
end;

procedure TLayerListBox.DoDrawPic(AIndex: Integer; Rect: TRect;
  State: TOwnerDrawState);
var
 Result: Boolean;
begin
 Result := False;
 if Assigned(FOnDrawPic) then
  FOnDrawPic(Self, AIndex, Rect, State, Result);
 with Canvas do
 begin
  if not Result then
  begin
   Brush.Color := clWhite;
   Brush.Style := bsDiagCross;
   FillRect(Rect);
   Pen.Color := clGray;
   Pen.Width := 1;
   Pen.Style := psInsideFrame;
   Pen.Mode := pmXor;
   Brush.Style := bsClear;
   Rectangle(Rect);
  end else
  begin
   Brush.Style := bsClear;
   Pen.Color := clGray;
   Pen.Style := psInsideFrame;
   Pen.Mode := pmMergePenNot;
   Pen.Width := 1;
   Rectangle(Rect);
  end;
 end;
end;

function TLayerListBox.DoGetData(Index: Integer): WideString;
begin
 Result := '';
 if Assigned(FOnUnicodeGetData) then
  FOnUnicodeGetData(Self, Index, Result) else
  Result := UTF8Decode(inherited DoGetData(Index));
end;

procedure TLayerListBox.DoGetModes(AIndex: Integer;
  var AModes: TItemClickModes);
begin
 if Assigned(FOnGetItemClickMode) then
  FOnGetItemClickMode(Self, AIndex, AModes);
end;

procedure TLayerListBox.DoItemClick(AIndex: Integer;
  ChangedMode: TItemClickMode);
begin
 if Assigned(FOnItemClick) then
  FOnItemClick(Self, AIndex, ChangedMode);
 Invalidate;
end;

procedure TLayerListBox.DrawItem(AIndex: Integer; Rect: TRect;
  State: TOwnerDrawState);
var
 AModes: TItemClickModes;
 H: Integer;
 BoxRect: TRect;
 Flags: Integer;
 Str: WideString;
begin
 if AIndex < Count then
 begin
  Canvas.FillRect(Rect);
  H := ItemHeight;
  Dec(Rect.Right, H);
  DoDrawPic(AIndex, Bounds(Rect.Right, Rect.Top, H, H), State);
  Flags := DrawTextBiDiModeFlags(DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX);

  BoxRect.Top := Rect.Top;
  BoxRect.Bottom := Rect.Bottom;
  BoxRect.Left := Rect.Left + FCheckedBoxRect.Right + 2;

  AModes := [];
  DoGetModes(AIndex, AModes);

  if llbFlipsDisabled in AModes then
   BoxRect.Right := Rect.Right - 2 else
   BoxRect.Right := Rect.Left + FXBoxRect.Left - 2;

  Str := '';
  if (Style in [lbVirtual, lbVirtualOwnerDraw]) then
   Str := DoGetData(AIndex) else
   Str := UTF8Decode(Items[AIndex]);
  Windows.DrawTextW(Canvas.Handle, Pointer(Str), Length(Str), BoxRect, Flags);

  Canvas.Pen.Color := clBlack;
  Canvas.Pen.Mode := pmCopy;
  Canvas.Pen.Width := 1;
  Canvas.Brush.Color := clWhite;
  Canvas.Brush.Style := bsSolid;

  BoxRect := Bounds(Rect.Left + FCheckedBoxRect.Left,
                    Rect.Top + FCheckedBoxRect.Top, 18, 14);
  Canvas.Rectangle(BoxRect);
  Inc(BoxRect.Left);
  Dec(BoxRect.Right);
  Inc(BoxRect.Top);
  Dec(BoxRect.Bottom);
  Canvas.Pen.Color := clGray;
  Canvas.Rectangle(BoxRect);
  Inc(BoxRect.Left);
  Dec(BoxRect.Right);
  Inc(BoxRect.Top);
  Dec(BoxRect.Bottom);
  if llbChecked in AModes then
   Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[CHECK_PIC]) else
   Canvas.FillRect(BoxRect);

  BoxRect := Bounds(Rect.Left + FVisibleBoxRect.Left,
                    Rect.Top + FVisibleBoxRect.Top, 18, 14);
  Canvas.Pen.Color := clBlack;
  Canvas.Rectangle(BoxRect);
  Inc(BoxRect.Left);
  Dec(BoxRect.Right);
  Inc(BoxRect.Top);
  Dec(BoxRect.Bottom);
  Canvas.Pen.Color := clGray;
  Canvas.Rectangle(BoxRect);
  Inc(BoxRect.Left);
  Dec(BoxRect.Right);
  Inc(BoxRect.Top);
  Dec(BoxRect.Bottom);
  if llbVisible in AModes then
   Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[EYE_PIC]) else
   Canvas.FillRect(BoxRect);

  if not (llbFlipsDisabled in AModes) then
  begin
   BoxRect := Bounds(Rect.Left + FXBoxRect.Left,
                     Rect.Top + FXBoxRect.Top, 18, 14);
   Canvas.Pen.Color := clBlack;
   Canvas.Rectangle(BoxRect);
   Inc(BoxRect.Left);
   Dec(BoxRect.Right);
   Inc(BoxRect.Top);
   Dec(BoxRect.Bottom);
   Canvas.Pen.Color := clGray;
   Canvas.Rectangle(BoxRect);
   Inc(BoxRect.Left);
   Dec(BoxRect.Right);
   Inc(BoxRect.Top);
   Dec(BoxRect.Bottom);
   if llbXFlip in AModes then
    Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[X_FLIP_PIC]) else
    Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[X_OK_PIC]);
   BoxRect := Bounds(Rect.Left + FYBoxRect.Left,
                     Rect.Top + FYBoxRect.Top, 18, 14);
   Canvas.Pen.Color := clBlack;                    
   Canvas.Rectangle(BoxRect);
   Inc(BoxRect.Left);
   Dec(BoxRect.Right);
   Inc(BoxRect.Top);
   Dec(BoxRect.Bottom);
   Canvas.Pen.Color := clGray;
   Canvas.Rectangle(BoxRect);
   Inc(BoxRect.Left);
   Dec(BoxRect.Right);
   Inc(BoxRect.Top);
   Dec(BoxRect.Bottom);
   if llbYFlip in AModes then
    Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[Y_FLIP_PIC]) else
    Canvas.Draw(BoxRect.Left, BoxRect.Top, FPics[Y_OK_PIC]);
  end;
 end;
end;

procedure TLayerListBox.ItemClickTest(var Message: TWMMouse);
var
 AModes: TItemClickModes;
 AIndex: Integer;
 ItemRect: TRect;
 pt: TPoint;
 H: Integer;
 ChangeMode: TItemClickMode;
begin
 if csDesigning in ComponentState then Exit;
 pt := SmallPointToPoint(Message.Pos);
 AIndex := ItemAtPos(pt, True);
 if AIndex >= 0 then
 begin
  ChangeMode := TItemClickMode(-1);
  Perform(LB_GETITEMRECT, AIndex, Longint(@ItemRect));
  H := ItemHeight;
  if ptInRect(Bounds(ItemRect.Right - H, ItemRect.Top, H, H), pt) then
  begin
   ChangeMode := llbTile;
   if ssDouble in GlobalShiftState then
    Message.Result := 1;
  end else
  if ptInRect(Bounds(ItemRect.Left + FCheckedBoxRect.Left,
                     ItemRect.Top + FCheckedBoxRect.Top, 18, 14), pt) then
  begin
   ChangeMode := llbChecked;
   Message.Result := 1;
  end else
  if ptInRect(Bounds(ItemRect.Left + FVisibleBoxRect.Left,
                     ItemRect.Top + FVisibleBoxRect.Top, 18, 14), pt) then
  begin
   ChangeMode := llbVisible;
   Message.Result := 1;
  end else
  begin
   AModes := [];
   DoGetModes(AIndex, AModes);

   if not (llbFlipsDisabled in AModes) then
   begin
    if ptInRect(Bounds(ItemRect.Left + FXBoxRect.Left,
                       ItemRect.Top + FXBoxRect.Top, 18, 14), pt) then
    begin
     ChangeMode := llbXFlip;
     Message.Result := 1;
    end else
    if ptInRect(Bounds(ItemRect.Left + FYBoxRect.Left,
                       ItemRect.Top + FYBoxRect.Top, 18, 14), pt) then
    begin
     ChangeMode := llbYFlip;
     Message.Result := 1;
    end;
   end;
  end;
  if Message.Result <> 0 then
   DoItemClick(AIndex, ChangeMode);
 end;
end;

procedure TLayerListBox.WMLButtonDblClk(var Message: TWMLButtonDblClk);
begin
 ItemClickTest(Message);
 if Message.Result = 0 then
  inherited;
end;

procedure TLayerListBox.WMLButtonDown(var Message: TWMLButtonDown);
begin
 ItemClickTest(Message);
 if Message.Result = 0 then
  inherited;
end;

procedure TLayerListBox.WMSize(var Message: TWMSize);
var
 TopBox, BottomBox, H: Integer;
begin
 inherited;
 if not (csDesigning in ComponentState) then
 with ClientRect do
 begin
  H := ItemHeight;
  BottomBox := H shr 1;
  TopBox := BottomBox - 15;
  Inc(BottomBox, 1);
  FCheckedBoxRect := Bounds(Left + 2, TopBox, 18, 14);
  FVisibleBoxRect := Bounds(FCheckedBoxRect.Left, BottomBox, 18, 14);
  FXBoxRect := Bounds(Right - H - 20, TopBox, 18, 14);
  FYBoxRect := Bounds(FXBoxRect.Left, BottomBox, 18, 14);
  Repaint;
 end;
end;

end.
