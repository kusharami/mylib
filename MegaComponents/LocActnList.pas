unit LocActnList;

interface

uses
  Classes, Controls, ActnList, Buttons, ExtCtrls, ComCtrls,
  StdCtrls, Menus, LocClasses, UnicodeUtils
{$IFNDEF FORCED_UNICODE}
  , TntActnList
  , TntControls
  , TntMenus
  , TntClasses
  , TntSysUtils;

type
  IWideAction = ITntAction;

{$ELSE}
  , Actions
  , System.Generics.Collections; // end uses

type
  IWideAction = interface
    ['{59D0AE37-8161-4AD6-9102-14B28E5761EB}']
  end;

{$ENDIF}

type
{TNT-WARN TActionList}
  TLocActionList = class(TActionList{TNT-ALLOW TActionList})
   private
    FCheckActionsTimer: TTimer;
    FLink: TLocalizerLink;
    procedure CheckActions(Sender: TObject);
    procedure SetLocalizer(Value: TCustomLocalizer);
    function GetLocalizer: TCustomLocalizer;
   public
    procedure Change; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
   published
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

//---------------------------------------------------------------------------------------------
//                              ACTIONS
//---------------------------------------------------------------------------------------------

{TNT-WARN TCustomAction}
  TLocCustomAction = class(TCustomAction{TNT-ALLOW TCustomAction}, IWideAction)
{$IFNDEF FORCED_UNICODE}
  protected
    procedure DefineProperties(Filer: TFiler); override;
  public
    procedure Assign(Source: TPersistent); override;
  private
    function GetCaption: WideString;
    procedure SetCaption(const Value: WideString);
    function GetHint: WideString;
    procedure SetHint(const Value: WideString);
  public
    property Caption: WideString read GetCaption write SetCaption;
    property Hint: WideString read GetHint write SetHint;
{$ENDIF}
  end;

{TNT-WARN TAction}
  TLocAction = class(TAction{TNT-ALLOW TAction}, IWideAction)
  private
    FIndexChanging: Boolean;
    FLocCaption: WideString;
    FLocHint: WideString;
{$IFNDEF FORCED_UNICODE}
    function GetCaption: WideString;
    procedure SetCaption(const Value: WideString);
    function GetHint: WideString;
    procedure SetHint(const Value: WideString);
  protected
    procedure DefineProperties(Filer: TFiler); override;
  published
    property Caption: WideString read GetCaption write SetCaption;
    property Hint: WideString read GetHint write SetHint;
{$ENDIF}
  protected
    procedure Change; override;
  private
    procedure SetLocHint(const Value: WideString);
    procedure SetLocCaption(const Value: WideString);
  public
    procedure Assign(Source: TPersistent); override;
  published
    property LocCaption: WideString read FLocCaption write SetLocCaption;
    property LocHint: WideString read FLocHint write SetLocHint;
  end;

type
  TLocUpgradeActionListItemsProc = procedure (ActionList: TLocActionList);

var
  LocUpgradeActionListItemsProc: TLocUpgradeActionListItemsProc;

implementation

uses
  SysUtils;

{ TActionListList }

type
  TLocActionListList = class(TList)
  private
    FActionList: TLocActionList;
  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
  end;

procedure TLocActionListList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  inherited;
  if (Action = lnAdded) and (FActionList <> nil) and (Ptr <> nil)
  and (not Supports(TObject(Ptr), IWideAction)) then
  begin
    FActionList.FCheckActionsTimer.Enabled := False;
    FActionList.FCheckActionsTimer.Enabled := True;
  end;
end;

{ THackActionList }

{$IFDEF VER280} // verified against VCL source in Delphi 6 and BCB 6

{$ENDIF}
{$IFDEF VER140} // verified against VCL source in Delphi 6 and BCB 6
  {$DEFINE VERSION_VERIFIED}
{$ENDIF}
{$IFDEF VER150} // verified against VCL source in Delphi 7
  {$DEFINE VERSION_VERIFIED}
{$ENDIF}
{$IFDEF VER170} // verified against VCL source in Delphi 9
  {$DEFINE VERSION_VERIFIED}
{$ENDIF}
{$IFDEF VER180} // verified against VCL source in Delphi 10
  {$DEFINE VERSION_VERIFIED}
{$ENDIF}
{$IFDEF VERSION_VERIFIED}
type
  THackCustomActionList = class(TComponent)
  private
    FActions: TList;
  end;
{$ENDIF}

{ TLocActionList }

constructor TLocActionList.Create(AOwner: TComponent);
begin
  inherited;
  if (csDesigning in ComponentState) then
   begin
    FCheckActionsTimer := TTimer.Create(Self);
    FCheckActionsTimer.Enabled := False;
    FCheckActionsTimer.Interval := 50;
    FCheckActionsTimer.OnTimer := CheckActions;
{$IFNDEF FORCED_UNICODE}
    with THackCustomActionList(Self) do
    begin
      FActions.Free;
      FActions := TLocActionListList.Create;
      TLocActionListList(FActions).FActionList := Self;
    end;
{$ENDIF}
  end;
end;

procedure TLocActionList.CheckActions(Sender: TObject);
begin
  if FCheckActionsTimer <> nil then
    FCheckActionsTimer.Enabled := False;

  Assert(csDesigning in ComponentState);
  Assert(Assigned(LocUpgradeActionListItemsProc));
  LocUpgradeActionListItemsProc(Self);
end;

//---------------------------------------------------------------------------------------------
//                              ACTIONS
//---------------------------------------------------------------------------------------------

procedure TLocActionList.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.LinkList.AddLink(Addr(FLink));
   FLink.OnChange := Change;
  end;
 end;
end;

procedure TLocActionList.Change;
begin
 inherited;
end;

function TLocActionList.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.Localizer Else
  Result := NIL;
end;

destructor TLocActionList.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 inherited;
end;

{$IFNDEF FORCED_UNICODE}

{ TLocCustomAction }

procedure TLocCustomAction.Assign(Source: TPersistent);
begin
  inherited;
  TntAction_AfterInherited_Assign(Self, Source);
end;

procedure TLocCustomAction.DefineProperties(Filer: TFiler);
begin
  inherited;
  TntPersistent_AfterInherited_DefineProperties(Filer, Self);
end;

function TLocCustomAction.GetCaption: WideString;
begin
  Result := TntAction_GetCaption(Self);
end;

procedure TLocCustomAction.SetCaption(const Value: WideString);
begin
  TntAction_SetCaption(Self, Value);
end;

function TLocCustomAction.GetHint: WideString;
begin
  Result := TntAction_GetHint(Self);
end;

procedure TLocCustomAction.SetHint(const Value: WideString);
begin
  TntAction_SetHint(Self, Value);
end;

{$ENDIF}

{ TLocAction }

procedure TLocAction.Assign(Source: TPersistent);
begin
  inherited;
{$IFNDEF FORCED_UNICODE}
  TntAction_AfterInherited_Assign(Self, Source);
{$ENDIF}
  if Source is TLocAction then
  begin
    SetLocCaption(TLocAction(Source).LocCaption);
    SetLocHint(TLocAction(Source).LocHint);
  end;
end;

{$IFNDEF FORCED_UNICODE}

procedure TLocAction.DefineProperties(Filer: TFiler);
begin
  inherited;
  TntPersistent_AfterInherited_DefineProperties(Filer, Self);
end;

function TLocAction.GetCaption: WideString;
begin
  Result := TntAction_GetCaption(Self);
end;

procedure TLocAction.SetCaption(const Value: WideString);
begin
  TntAction_SetCaption(Self, Value);
end;

function TLocAction.GetHint: WideString;
begin
  Result := TntAction_GetHint(Self);
end;

procedure TLocAction.SetHint(const Value: WideString);
begin
  TntAction_SetHint(Self, Value);
end;
{$ENDIF}

procedure TLocAction.Change;
begin
 if not FIndexChanging
    and Assigned(ActionList) and (ActionList is TLocActionList) then
 begin
   with TLocActionList(ActionList) do
   begin
     if Assigned(FLink) then
     begin
       SetLocCaption(FLocCaption);
       SetLocHint(FLocHint);
     end;
   end;
 end;

 inherited;
end;

procedure TLocAction.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
  FLocHint := Value;
  if Assigned(ActionList) and (ActionList is TLocActionList) then
  with TLocActionList(ActionList) do
  begin
  If (Value <> '') and Assigned(FLink) then
  begin
   Lang := Localizer.Language;
   FIndexChanging := True;
   try
     If Lang <> NIL then
     begin
      Item := Lang.ValueItems[FLocHint];
      if Item <> NIL then
    {$IFNDEF FORCED_UNICODE}
        TntAction_SetHint(Self, Item.Value);
    {$ELSE}
        inherited Hint := Item.Value;
    {$ENDIF}
     end;
   finally
     FIndexChanging := False;
   end;
  end;
end;
end;

procedure TLocAction.SetLocCaption(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocCaption := Value;
 If Assigned(ActionList) and (ActionList is TLocActionList) then
 With TLocActionList(ActionList) do
 begin
  If (Value <> '') and Assigned(FLink) then
  begin
   Lang := Localizer.Language;
   FIndexChanging := True;
   try
     If Lang <> NIL then
     begin
      Item := Lang.ValueItems[FLocCaption];
      if Item <> NIL then
  {$IFNDEF FORCED_UNICODE}
        TntAction_SetCaption(Self, Item.Value);
  {$ELSE}
        inherited Caption := Item.Value;
  {$ENDIF}
     end;
   finally
     FIndexChanging := False;
   end;
  end;
 end;
end;

end.
