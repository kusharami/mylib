unit LocClasses;

interface

Uses
 Windows, Messages, SysUtils, Classes, Controls
{$IFNDEF FORCED_UNICODE}
 , TntClasses
 , TntSysUtils
 , TntStdCtrls
 , TntComCtrls
 , TntForms
 , TntDialogs
{$ELSE}
 , StdCtrls
 , ComCtrls
 , Dialogs
 , Forms
 , Buttons
{$ENDIF}
 , NodeLst, MyUtils, MyClassesEx, CommDlg, UnicodeUtils;

Type
{$IFNDEF FORCED_UNICODE}
  TWideLabel = TTntLabel;
  TWideTabSheet = TTntTabSheet;
  TWideForm = TTntForm;
  TWideOpenDialog = TTntOpenDialog;
  TWideGroupBox = TTntGroupBox;
  TWideCheckBox = TTntCheckBox;
  TWideButton = TTntButton;
{$ELSE}
  TWideLabel = TLabel;
  TWideTabSheet = TTabSheet;
  TWideForm = TForm;
  TWideOpenDialog = TOpenDialog;
  TWideGroupBox = TGroupBox;
  TWideCheckBox = TCheckBox;
  TWideButton = TButton;
{$ENDIF}

  TLanguageString = Class(TNode)
   private
    FName: WideString;
    FUpperName: WideString;
    FValue: WideString;
    procedure SetName(const Value: WideString);
   public
    property Name: WideString read FName write SetName;
    property Value: WideString read FValue write FValue;

    constructor Create(const Name, Value: WideString);
    destructor Destroy; override;
   end;

  TLanguageListItem = Class(TNodeList)
   private
    FFileName: WideString;
    FName: WideString;
    function GetStringItem(Index: Integer): TLanguageString;
    function GetString(Index: Integer): WideString;
    function FindValueItem(const Name: WideString): TLanguageString;
    function FindValue(const Name: WideString): WideString;
   public
    property FileName: WideString read FFileName write FFileName;
    property LanguageName: WideString read FName write FName;
    property StringItems[Index: Integer]: TLanguageString read GetStringItem;
    property Strings[Index: Integer]: WideString read GetString;
    property ValueItems[const Name: WideString]: TLanguageString read FindValueItem;
    property Values[const Name: WideString]: WideString read FindValue;

    constructor Create(const Name: WideString);
    destructor Destroy; override;
    function AddString(const FName, FValue: WideString): TLanguageString;
    procedure LoadFromIniFile(Ini: TStreamIniFileW; Section: WideString);
    procedure LoadFromStream(Stream: TStream);
    procedure LoadFromFile(const FileName: WideString);
   end;

  TLanguageList = Class(TNodeList)
   private
    function GetLanguage(Index: Integer): TLanguageListItem;
    function GetByName(const Name: WideString): TLanguageListItem;
   public
    property Languages[Index: Integer]: TLanguageListItem read GetLanguage;
    property Names[const Name: WideString]: TLanguageListItem read GetByName;

    function AddLanguage(const Name: WideString): TLanguageListItem;
    function AddLanguageFile(const FileName: WideString): TLanguageListItem;
    procedure LoadFromStream(Stream: TStream);
    procedure LoadFromFile(const FileName: WideString);
   end;

  TCustomLocalizer = Class;
  TLocalizerLink = Class;

  TLocalizerLinkList = Class(TNodeList)
   private
    FOwner: TCustomLocalizer;
    function GetLink(Index: Integer): TLocalizerLink;
   public
    property Links[Index: Integer]: TLocalizerLink read GetLink;

    function AddLink(LinkFieldAddr: Pointer): TLocalizerLink;
    constructor Create(Owner: TCustomLocalizer);
  end;

  TCustomLocalizer = class(TComponent)
   private
    FOwner: TComponent;
    FLanguageIndex: Integer;
    FLanguageList: TLanguageList;
    FLinkList: TLocalizerLinkList;
    FOnChangeLanguage: TNotifyEvent;
    procedure SetLanguageIndex(Value: Integer);
    function GetLanguage: TLanguageListItem;
   protected
    procedure ChangeLanguage(Index: Integer); virtual;
   public
    property Language: TLanguageListItem read GetLanguage;
    property LanguageIndex: Integer read FLanguageIndex
                                   write SetLanguageIndex default -1;
    property LanguageList: TLanguageList read FLanguageList;
    property LinkList: TLocalizerLinkList read FLinkList;

    property OnChangeLanguage: TNotifyEvent read FOnChangeLanguage
                                           write FOnChangeLanguage;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure RemoveLink(Link: TLocalizerLink);
   end;

  TLocEvent = procedure of Object;

  TLocalizerLink = Class(TNode)
   private
    FLocalizer: TCustomLocalizer;
    FOnChange: TLocEvent;
    FLinkField: PPointer;
    procedure SetOnChange(Value: TLocEvent);
   public
    property Localizer: TCustomLocalizer read FLocalizer write FLocalizer;
    property OnChange: TLocEvent read FOnChange write SetOnChange;

    constructor Create(Localizer: TCustomLocalizer; LinkFieldAddr: Pointer);
    destructor Destroy; override;
   end;

  TLocalizer = class(TCustomLocalizer)
   published
    property LanguageIndex;
    property OnChangeLanguage;
   end;

  TLocButton = class(TWideButton)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    procedure UpdateString;
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                               write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
   end;

  TLocLabel = class(TWideLabel)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    procedure UpdateString;
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                               write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

  TLocTabSheet = class(TWideTabSheet)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    procedure UpdateString;
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                               write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

  TLocForm = class(TWideForm)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    procedure UpdateString;
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                               write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

  TLocOpenDialog = class(TWideOpenDialog)
   private
    FLink: TLocalizerLink;
    FLocTitle: WideString;
    FLocFilter: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocTitle(const Value: WideString);
    procedure SetLocFilter(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    destructor Destroy; override;
   published
    property LocTitle: WideString read FLocTitle
                                 write SetLocTitle;
    property LocFilter: WideString read FLocFilter
                                  write SetLocFilter;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

  TLocSaveDialog = class(TLocOpenDialog)
  public
    function Execute: Boolean; override;
  end;

  TLocGroupBox = class(TWideGroupBox)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                                  write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

  TLocCheckBox = class(TWideCheckBox)
   private
    FLink: TLocalizerLink;
    FLocCaption: WideString;
    FLocHint: WideString;
    procedure SetLocalizer(Value: TCustomLocalizer);
    procedure SetLocCaption(const Value: WideString);
    procedure SetLocHint(const Value: WideString);
    function GetLocalizer: TCustomLocalizer;
    procedure LocalizerChange;
   public
    destructor Destroy; override;
   published
    property LocCaption: WideString read FLocCaption
                                 write SetLocCaption;
    property LocHint: WideString read FLocHint
                                  write SetLocHint;
    property Localizer: TCustomLocalizer read GetLocalizer write SetLocalizer;
  end;

implementation

procedure TCustomLocalizer.ChangeLanguage(Index: Integer);
var
 N: TNode;
 Lang: TLanguageListItem;
begin
 Lang := Language;
 if Lang <> nil then
  Lang.Clear;
 FLanguageIndex := Index;
 Lang := Language;
 if Lang <> nil then with Lang do if WideFileExists(FFileName) then 
  LoadFromFile(FFileName);
 N := FLinkList.RootNode;
 while N <> NIL do
 begin
  with N as TLocalizerLink do if Assigned(FOnChange) then FOnChange;
  N := N.Next;
 end;
 if Assigned(FOnChangeLanguage) then FOnChangeLanguage(Self);
end;

constructor TCustomLocalizer.Create(AOwner: TComponent);
begin
 FOwner := AOwner;
 inherited;
 FLanguageList := TLanguageList.Create;
 FLanguageIndex := -1;
 FLinkList := TLocalizerLinkList.Create(Self);
end;

destructor TCustomLocalizer.Destroy;
begin
 FLinkList.Free;
 FLanguageList.Free;
 inherited;
end;

function TCustomLocalizer.GetLanguage: TLanguageListItem;
begin
 Result := FLanguageList.Languages[FLanguageIndex];
end;

procedure TCustomLocalizer.RemoveLink(Link: TLocalizerLink);
var
 N, R: TNode;
begin
 N := FLinkList.RootNode;
 while N <> NIL do
 begin
  If N = Link then
  begin
   R := N.Next;
   FLinkList.Remove(N);
   N := R;
  end else N := N.Next;
 end;
end;

procedure TCustomLocalizer.SetLanguageIndex(Value: Integer);
begin
 if Value <> FLanguageIndex then
  ChangeLanguage(Value);
end;

function TLanguageList.AddLanguageFile(const FileName:
  WideString): TLanguageListItem;
var
 List: TWideStringList;
 S: WideString;
 L: Integer;
begin
 Result := nil;
 List := TWideStringList.Create;
 try
  List.LoadFromFile(FileName);
  if List.Count > 0 then
  begin
   S := Trim(List[0]);
   L := Length(S);
   if (S[1] = '[') and (S[L] = ']') then
   begin
    SetLength(S, L - 1);   
    Delete(S, 1, 1);
    Result := AddLanguage(S);
    Result.FFileName := FileName;
   end;
  end;
 finally
  List.Free;
 end;
end;

function TLanguageList.AddLanguage(const Name: WideString): TLanguageListItem;
begin
 Result := TLanguageListItem.Create(Name);
 AddCreated(Result, True);
end;

function TLanguageList.GetByName(const Name: WideString): TLanguageListItem;
var
 N: TNode;
 Item: TLanguageListItem absolute N;
begin
 Result := NIL;
 N := RootNode;
 while N <> NIL do
 begin
  if Item.FName = Name then
  begin
   Result := Item;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TLanguageList.GetLanguage(Index: Integer): TLanguageListItem;
begin
 Result := Nodes[Index] as TLanguageListItem;
end;

function TLanguageListItem.AddString(const FName,
  FValue: WideString): TLanguageString;
begin
 if FName <> '' then
 begin
  Result := TLanguageString.Create(FName, FValue);
  AddCreated(Result, True);
 end else Result := NIL;
end;

constructor TLanguageListItem.Create(const Name: WideString);
begin
 inherited Create;
 FName := Name;
end;

destructor TLanguageListItem.Destroy;
begin
 Finalize(FName);
 inherited;
end;

function TLanguageListItem.FindValue(const Name: WideString): WideString;
var
 N: TNode; S: WideString;
 Item: TLanguageString absolute N;
begin
 Result := '';
 if Name = '' then Exit;
 N := RootNode;
 S := WideUpperCase(Name);
 while N <> NIL do
 begin
  if Item.FUpperName = S then
  begin
   Result := Item.FValue;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TLanguageListItem.FindValueItem(const Name: WideString): TLanguageString;
var
 N: TNode; S: WideString;
 Item: TLanguageString absolute N;
begin
 Result := NIL;
 if Name = '' then Exit;
 S := WideUpperCase(Name);
 N := RootNode;
 while N <> NIL do
 begin
  if Item.FUpperName = S then
  begin
   Result := Item;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TLanguageListItem.GetString(Index: Integer): WideString;
Var
 Item: TLanguageString;
begin
 Item := Nodes[Index] as TLanguageString;
 If Item <> NIL then
  Result := Item.FValue Else
  Result := '';
end;

function TLanguageListItem.GetStringItem(Index: Integer): TLanguageString;
begin
 Result := Nodes[Index] as TLanguageString;
end;

constructor TLanguageString.Create(const Name, Value: WideString);
begin
 inherited Create;
 SetName(Name);
 FValue := Value;
end;

destructor TLanguageString.Destroy;
begin
 Finalize(FValue);
 Finalize(FUpperName);
 Finalize(FName); 
 inherited;
end;

procedure TLanguageString.SetName(const Value: WideString);
begin
 FName := Value;
 FUpperName := WideUpperCase(Value);
end;

procedure TLocButton.SetLocCaption(const Value: WideString);
begin
 FLocCaption := Value;
 UpdateString;
end;

procedure TLocButton.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

procedure TLocButton.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocButton.UpdateString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 If (FLocCaption <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

constructor TLocalizerLink.Create(Localizer: TCustomLocalizer;
  LinkFieldAddr: Pointer);
begin
 inherited Create;
 FLocalizer := Localizer;
 FLinkField := LinkFieldAddr;
end;

destructor TLocalizerLink.Destroy;
begin
 if FLinkField <> nil then
  FLinkField^ := nil;
 inherited;
end;

function TLocalizerLinkList.AddLink(LinkFieldAddr: Pointer): TLocalizerLink;
begin
 Result := TLocalizerLink.Create(FOwner, LinkFieldAddr);
 AddCreated(Result, True);
end;

constructor TLocalizerLinkList.Create(Owner: TCustomLocalizer);
begin
 inherited Create;
 FOwner := Owner;
end;

function TLocalizerLinkList.GetLink(Index: Integer): TLocalizerLink;
begin
 Result := Nodes[Index] as TLocalizerLink;
end;

function TLocButton.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

destructor TLocButton.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

procedure TLocButton.LocalizerChange;
begin
 SetLocHint(FLocHint);
 UpdateString;
end;


{ TLocLabel }

destructor TLocLabel.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

function TLocLabel.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocLabel.LocalizerChange;
begin
 SetLocHint(FLocHint);
 UpdateString;
end;

procedure TLocLabel.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocLabel.SetLocCaption(const Value: WideString);
begin
 FLocCaption := Value;
 UpdateString;
end;

procedure TLocLabel.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

procedure TLocLabel.UpdateString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 If (FLocCaption <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

procedure TLanguageList.LoadFromFile(const FileName: WideString);
var
 Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TLanguageList.LoadFromStream(Stream: TStream);
var
 Ini: TStreamIniFileW;
 Sections: TWideStringList;
 I: Integer;
begin
 Ini := TStreamIniFileW.Create(Stream);
 try
  Sections := TWideStringList.Create;
  try
   Ini.ReadSections(Sections);
   for I := 0 to Sections.Count - 1 do with AddLanguage(Sections[I]) do
    LoadFromIniFile(Ini, FName);
  finally
   Sections.Free;
  end;
 finally
  Ini.Free;
 end;
end;

{ TLocTabSheet }

destructor TLocTabSheet.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

function TLocTabSheet.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocTabSheet.LocalizerChange;
begin
 SetLocHint(FLocHint);
 UpdateString;
end;

procedure TLocTabSheet.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocTabSheet.SetLocCaption(const Value: WideString);
begin
 FLocCaption := Value;
 UpdateString;
end;

procedure TLocTabSheet.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

procedure TLocTabSheet.UpdateString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 If (FLocCaption <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

{ TLocForm }

destructor TLocForm.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

function TLocForm.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocForm.LocalizerChange;
begin
 SetLocHint(FLocHint);
 UpdateString;
end;

procedure TLocForm.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocForm.SetLocCaption(const Value: WideString);
begin
 FLocCaption := Value;
 UpdateString;
end;

procedure TLocForm.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

procedure TLocForm.UpdateString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 If (FLocCaption <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

{ TLocOpenDialog }

destructor TLocOpenDialog.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocTitle);
 Finalize(FLocFilter);
 inherited;
end;

function TLocOpenDialog.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocOpenDialog.LocalizerChange;
begin
 SetLocTitle(FLocTitle);
 SetLocFilter(FLocFilter);
end;

procedure TLocOpenDialog.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocOpenDialog.SetLocFilter(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocFilter := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocFilter];
   if Item <> NIL then Filter := Item.FValue;
  end;
 end;
end;

procedure TLocOpenDialog.SetLocTitle(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocTitle := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocTitle];
   if Item <> NIL then Title := Item.FValue;
  end;
 end;
end;

{ TLocSaveDialog }

function TLocSaveDialog.Execute: Boolean;
begin
{$IFDEF FORCED_UNICODE}
  Result := DoExecute(@GetSaveFileNameW);
{$ELSE}
 if (not Win32PlatformIsUnicode) then
  Result := DoExecute(@GetSaveFileNameA) else
  Result := DoExecuteW(@GetSaveFileNameW);
{$ENDIF}
end;

{ TLocGroupBox }

destructor TLocGroupBox.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

function TLocGroupBox.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocGroupBox.LocalizerChange;
begin
 SetLocCaption(FLocCaption);
 SetLocHint(FLocHint);
end;

procedure TLocGroupBox.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocGroupBox.SetLocCaption(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocCaption := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

procedure TLocGroupBox.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

{ TLocGroupBox }

destructor TLocCheckBox.Destroy;
begin
 If Assigned(FLink) then Localizer.RemoveLink(FLink);
 Finalize(FLocCaption);
 Finalize(FLocHint);
 inherited;
end;

function TLocCheckBox.GetLocalizer: TCustomLocalizer;
begin
 If Assigned(FLink) then
  Result := FLink.FLocalizer Else
  Result := NIL;
end;

procedure TLocCheckBox.LocalizerChange;
begin
 SetLocCaption(FLocCaption);
 SetLocHint(FLocHint);
end;

procedure TLocCheckBox.SetLocalizer(Value: TCustomLocalizer);
begin
 If Localizer <> Value then
 begin
  If Assigned(FLink) then Localizer.RemoveLink(FLink);
  If Assigned(Value) then
  begin
   FLink := Value.FLinkList.AddLink(Addr(FLink));
   FLink.OnChange := LocalizerChange;
  end;
 end;
end;

procedure TLocCheckBox.SetLocCaption(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocCaption := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocCaption];
   if Item <> NIL then Caption := Item.FValue;
  end;
 end;
end;

procedure TLocCheckBox.SetLocHint(const Value: WideString);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 FLocHint := Value;
 If (Value <> '') and Assigned(FLink) then
 begin
  Lang := Localizer.Language;
  If Lang <> NIL then
  begin
   Item := Lang.ValueItems[FLocHint];
   if Item <> NIL then Hint := Item.FValue;
  end;
 end;
end;

procedure TLocalizerLink.SetOnChange(Value: TLocEvent);
begin
 FOnChange := Value;
 if Assigned(Value) then Value;;
end;

procedure TLanguageListItem.LoadFromFile(const FileName: WideString);
var
 Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TLanguageListItem.LoadFromStream(Stream: TStream);
var
 Ini: TStreamIniFileW;
begin
 Ini := TStreamIniFileW.Create(Stream);
 try
  LoadFromIniFile(Ini, FName);
 finally
  Ini.Free;
 end;
end;

procedure TLanguageListItem.LoadFromIniFile(Ini: TStreamIniFileW;
  Section: WideString);
var
 PP: PWord;
 J, P: Integer;
 S: WideString;
 SecItems: TWideStringList;
 PD: PLongWord absolute PP;
begin
 SecItems := TWideStringList.Create;
 try
  Ini.ReadSectionValues(FName, SecItems);
  for J := 0 to SecItems.Count - 1 do
  begin
   S := SecItems[J];
   P := Pos('=', S);
   if P > 0 then
   begin
    PP := Addr(S[P + 1]);
    while PP^ <> 0 do
    begin
     if PD^ = $6E005C then
      PD^ := $0A000D else
     Inc(PP);
    end;
    S[P] := #0;
    AddString(PWideChar(Pointer(S)), PWideChar(Addr(S[P + 1])));
   end;
  end;
 finally
  SecItems.Free;
 end;
end;

end.
