unit LocReg;

interface

uses Classes, ActnList, LocClasses, LocActnList
{$IFNDEF FORCED_UNICODE}
  , TntDesignEditors_Design
{$ENDIF}
  , DesignIntf
  , ToolsApi
  , Forms
{$IFDEF FORCED_UNICODE}
  , Actions
{$ENDIF};

procedure Register;

implementation

uses DesignEditors;

procedure Register;
begin
  RegisterClass(TLocAction);
  RegisterClass(TLocTabSheet);
  RegisterCustomModule(TLocForm, TCustomModule);
  RegisterComponents('Localizer Controls',
  [TLocalizer, TLocButton, TLocLabel, TLocActionList,
   TLocOpenDialog, TLocSaveDialog, TLocGroupBox, TLocCheckBox]);
end;

function GetLocActionClass(OldActionClass: TContainedActionClass): TContainedActionClass;
begin
  Result := TContainedActionClass(GetClass('TLoc' +
  Copy(OldActionClass.ClassName, 2, Length(OldActionClass.ClassName))));
end;

type
  TAccessContainedAction = class(TContainedAction);

function UpgradeAction(ActionList: TLocActionList; OldAction: TContainedAction): TContainedAction;
var
  Name: TComponentName;
  i, cnt: integer;
  NewActionClass: TContainedActionClass;
begin
  Result := nil;
  if (OldAction = nil) or (OldAction.Owner = nil) or (OldAction.Name = '') then
    Exit;

  NewActionClass := GetLocActionClass(TContainedActionClass(OldAction.ClassType));
  if NewActionClass <> nil then begin
    // create new action
    Result := NewActionClass.Create(OldAction.Owner) as TContainedAction;
    Include(TAccessContainedAction(Result).FComponentStyle, csTransient);
    // copy base class info
    Result.ActionComponent := OldAction.ActionComponent;
    Result.Category := OldAction.Category; { Assign Category before ActionList/Index to avoid flicker. }
    Result.ActionList := ActionList;
    Result.Index := OldAction.Index;
    // assign props
    Result.Assign(OldAction);
    // point all links to this new action
    cnt := TAccessContainedAction(OldAction).
{$IFDEF FORCED_UNICODE}ClientCount{$ELSE}FClients.Count{$ENDIF};
    for i := cnt - 1 downto 0 do
      with TBasicActionLink(TAccessContainedAction(OldAction).
       {$IFDEF FORCED_UNICODE}Clients{$ELSE}FClients{$ENDIF}[i]) do
        Action := Result;
    // free old object, preserve name...
    Name := OldAction.Name;
    OldAction.Free;
    Result.Name := Name; { link up to old name }
    Exclude(TAccessContainedAction(Result).FComponentStyle, csTransient);
  end;
end;

type
{$IFDEF FORCED_UNICODE}
  TWideDesignerSelections = class(TDesignerSelections)
  public
    procedure ReplaceSelection(const OldInst, NewInst: TPersistent);
  end;
  IWideDesigner = IDesigner;
{$ELSE}
  TWideDesignerSelections = TTntDesignerSelections;
  IWideDesigner = ITntDesigner;
{$ENDIF}

procedure LocActionList_UpgradeActionListItems(ActionList: TLocActionList);
var
  DesignerNotify: IDesignerNotify;
  Designer: IWideDesigner;
  Selections: TWideDesignerSelections;
  i: integer;
  OldAction, NewAction: TContainedAction;
begin
  DesignerNotify := FindRootDesigner(ActionList);
  if (DesignerNotify <> nil) then begin
    DesignerNotify.QueryInterface(IWideDesigner, Designer);
    if (Designer <> nil) then begin
      Selections := TWideDesignerSelections.Create;
      try
        Designer.GetSelections(Selections);
        for i := ActionList.ActionCount - 1 downto 0 do begin
          OldAction := ActionList.Actions[i];
          NewAction := UpgradeAction(ActionList, OldAction);
          if (NewAction <> nil) then
            Selections.ReplaceSelection(OldAction, NewAction);
        end;
        Designer.SetSelections(Selections);
      finally
        Selections.Free;
      end;
    end;
  end;
end;

{$IFDEF FORCED_UNICODE}

type
  THackDesignerSelections = class(TInterfacedObject)
  private
    FList: TList;
  end;

{ TWideDesignerSelections }

procedure TWideDesignerSelections.ReplaceSelection(const OldInst,
  NewInst: TPersistent);
var
  Idx: Integer;
begin
  Idx := THackDesignerSelections(Self).FList.IndexOf(OldInst);
  if Idx <> -1 then
    THackDesignerSelections(Self).FList[Idx] := NewInst;
end;

{$ENDIF}

initialization
  LocUpgradeActionListItemsProc := LocActionList_UpgradeActionListItems;
end.
