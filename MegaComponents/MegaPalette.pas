unit MegaPalette;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics, DIB, BitmapEx;

type
  TColorMode = (cm1bpp, cm2bpp, cm4bpp, cm8bpp);
Const
  ColorsAmount: Array[TColorMode] of Integer = (2, 4, 16, 256);
  BitsAmount: Array[TColorMode] of Byte = (1, 2, 4, 8);
Type
  TMegaPalette = class(TCustomControl)
  private
   FColorMode: TColorMode;
   FForegroundIndex: Integer;
   FBackgroundIndex: Integer;
   FTransparentIndex: Integer;
   FColorsInLine: Integer;

   FOnCreating: TNotifyEvent;
   FOnPaletteChange: TNotifyEvent;
   FOnChange: TNotifyEvent;
   FColorFormat: TColorFormat;
   FColorTable: Pointer;
   FCellXSize, FCellYSize: Integer;
   FNumXSquares, FNumYSquares: Integer;
   FButton: TMouseButton;
   FButtonDown: Boolean;
   FTransBkg: TDIB;
   procedure DrawSquare(Index: Integer);
   procedure UpdateCellSizes(DoRepaint: Boolean);
   procedure SetColorMode(Value: TColorMode);
   procedure SetTransparentIndex(Value: Integer);
   procedure SetColorsInLine(Value: Integer);
   Function GetForegroundColor: TColor;
   Function GetBackgroundColor: TColor;
   procedure SetForegroundIndex(Value: Integer);
   procedure SetBackgroundIndex(Value: Integer);
   function GetColor(X: Integer): TColor;
   procedure SetColor(X: Integer; Color: TColor);
   procedure WMSize(var Message: TWMSize); message WM_SIZE;
   procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CreateWnd; override;
    procedure Paint; override;
    procedure Creating; dynamic;
    procedure Change; dynamic;
    procedure PaletteChange; dynamic;
    function SquareFromPos(X, Y: Integer): Integer;
    procedure UpdateTransBkg;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property ForegroundColor: TColor read GetForegroundColor;
    property BackgroundColor: TColor read GetBackgroundColor;
    procedure SetPalette(ColorFmt: TColorFormat; CTbl: Pointer);
    property Colors[X: Integer]: TColor read GetColor write SetColor;
  published
    property Align;
    property Anchors;
    property Constraints;
    property ColorMode: TColorMode read FColorMode write SetColorMode;
    property ForegroundIndex: Integer read FForegroundIndex write SetForegroundIndex;
    property BackgroundIndex: Integer read FBackgroundIndex write SetBackgroundIndex;
    property ColorsInLine: Integer read FColorsInLine write SetColorsInLine;
    property TransparentIndex: Integer read FTransparentIndex write SetTransparentIndex;
    property Ctl3D;
    property Enabled;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property OnPaletteChange: TNotifyEvent read FOnPaletteChange write FOnPaletteChange;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnCreating: TNotifyEvent read FOnCreating write FOnCreating;
  end;

implementation

procedure Frame3D(Canvas: TCanvas; var Rect: TRect; TopColor, BottomColor: TColor;
  Width: Integer);
  procedure DoRect;
  var
    TopRight, BottomLeft: TPoint;
  begin
    with Canvas, Rect do
    begin
      TopRight.X := Right;
      TopRight.Y := Top;
      BottomLeft.X := Left;
      BottomLeft.Y := Bottom;
      Pen.Color := TopColor;
      PolyLine([BottomLeft, TopLeft, TopRight]);
      Pen.Color := BottomColor;
      Dec(BottomLeft.X);
      PolyLine([TopRight, BottomRight, BottomLeft]);
    end;
  end;
begin
  Canvas.Pen.Width := 1;
  Dec(Rect.Bottom); Dec(Rect.Right);
  while Width > 0 do
  begin
    Dec(Width);
    DoRect;
    InflateRect(Rect, -1, -1);
  end;
  Inc(Rect.Bottom); Inc(Rect.Right);
end;

procedure TMegaPalette.DrawSquare(Index: Integer);
var
 WinTop, WinLeft: Integer;
 CellRect: TRect;
 Col: TColor;
begin
 If (Index >= 0) and (Index <= ColorsAmount[FColorMode]) then
 begin
  WinTop := (Index div FNumXSquares) * FCellYSize;
  WinLeft := (Index mod FNumXSquares) * FCellXSize;
  CellRect := Bounds(WinLeft, WinTop, FCellXSize, FCellYSize);
  If Index = FTransparentIndex then With CellRect do
  begin
   with Canvas do
   begin
    Brush.Style := bsSolid;
    Col := Colors[Index];
    Brush.Color := Col;
    Pen.Width := 1;
    Pen.Mode := pmCopy;
    Pen.Color := Color;
    Rectangle(Left, Top, Right, Bottom);
 //  end;
 //  with Canvas do
 //  begin
    Draw(Left, Top, FTransBkg);
    Brush.Style := bsClear;
    Pen.Width := 1;
    Pen.Color := clBlack;
    Rectangle(Left, Top, Right, Bottom);
   end;
  end Else With CellRect, Canvas do
  begin
   Brush.Style := bsSolid;
   Col := Colors[Index];
   Brush.Color := Col;
   Pen.Color := Col;
   Pen.Mode := pmCopy;
   Rectangle(Left, Top, Right, Bottom);
   Brush.Style := bsClear;
   Pen.Color := clBlack;
   Rectangle(Left, Top, Right, Bottom);
  end;
 end;
end;

procedure TMegaPalette.UpdateCellSizes(DoRepaint: Boolean);
begin
 FCellXSize := Width div FNumXSquares;
 FCellYSize := Height div FNumYSquares;
 if DoRepaint then Invalidate;
end;

procedure TMegaPalette.SetColorsInLine(Value: Integer);
Var CC: Integer;
begin
 If Value = FColorsInLine then Exit;
 FColorsInLine := Value;
 FNumXSquares := Value;
 CC := ColorsAmount[FColorMode];
 While CC mod Value > 0 do Inc(CC);
 FNumYSquares := CC div Value;
 UpdateCellSizes(True);
end;

procedure TMegaPalette.SetColorMode(Value: TColorMode);
Var CC: Integer;
begin
 If Value = FColorMode then Exit;
 FColorMode := Value;
 CC := ColorsAmount[FColorMode];
 While CC mod FNumXSquares > 0 do Inc(CC);
 FNumYSquares := CC div FNumXSquares;
 UpdateCellSizes(False);//BoundsRect := Bounds(Left, Top, FCellXSize * FNumXSquares, FCellYSize * FNumYSquares);
 Invalidate;
end;

procedure TMegaPalette.Creating;
begin
 if Assigned(FOnCreating) then FOnCreating(Self);
end;

procedure TMegaPalette.Change;
begin
 Changed;
 if Assigned(FOnChange) then FOnChange(Self);
end;

procedure TMegaPalette.PaletteChange;
begin
 if Assigned(FOnPaletteChange) then FOnPaletteChange(Self);
end;

procedure TMegaPalette.CreateWnd;
begin
 inherited CreateWnd;
 SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE)
  or WS_CLIPSIBLINGS);
end;

Var
 Created: Boolean = False;

procedure TMegaPalette.Paint;
var
  Row, Col, wEntryIndex, CA: Integer;
begin
 If not Created then
 begin
  Creating;
  Created := True;
 end;
 wEntryIndex := 0;
 Canvas.Brush.Style := bsSolid;
 Canvas.Brush.Color := Color;
 Canvas.FillRect(Canvas.ClipRect);
 CA := ColorsAmount[FColorMode];
 For Row := 0 to FNumYSquares do
  For Col := 0 to FNumXSquares do
  begin
   DrawSquare(wEntryIndex);
   Inc(wEntryIndex);
   If wEntryIndex = CA then Exit;
  end;
end;

constructor TMegaPalette.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 DoubleBuffered := True;
 FColorMode := cm8bpp;
 FColorFormat := TColorFormat.Create;
 FTransBkg := TDIB.Create;
 FTransBkg.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
 FTransBkg.BitCount := 4;
 TColor(FTransBkg.ColorTable[0]) := clWhite;
 TColor(FTransBkg.ColorTable[1]) := clSilver;
 FTransBkg.UpdatePalette;
 FForegroundIndex := 0;
 FBackgroundIndex := 0;
 FTransparentIndex := -1;
 FColorsInLine := 16;
 FNumXSquares := 16;
 FNumYSquares := 16;
 Color := clBtnFace;
 Canvas.Brush.Style := bsSolid;
 Canvas.Pen.Color := clBlack;
 SetBounds(0, 0, 128, 128);
 FCellXSize := 128 div 16;
 FCellYSize := 128 div 16;
 UpdateTransBkg;
end;

procedure TMegaPalette.SetPalette(ColorFmt: TColorFormat; CTbl: Pointer);
begin
 FColorFormat.Assign(ColorFmt);
 FColorTable := CTbl;
 Invalidate;
 PaletteChange;
 Change;
end;

function TMegaPalette.SquareFromPos(X, Y: Integer): Integer;
begin
 if X >= Width then X := Width - 2 else
 if X < 0 then X := 0;
 if Y >= Height then Y := Height - 2 else
 if Y < 0 then Y := 0;
 Result := (Y div FCellYSize) * FNumXSquares + (X div FCellXSize);
end;

procedure TMegaPalette.MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
Var Square: Integer; B: Boolean;
begin
 inherited MouseDown(Button, Shift, X, Y);
 If ssDouble in Shift then Exit;
 FButton := Button;
 FButtonDown := True;
 Square := SquareFromPos(X, Y);
 B := (Square < 0) or (Square >= ColorsAmount[FColorMode]);
 If Button = mbLeft then
 begin
  If B then Exit;
  SetForegroundIndex(Square);
  If ssCtrl in Shift then
  begin
   If Square = FTransparentIndex then Square := -1;
   SetTransparentIndex(Square);
  end;
 end Else
 If Button = mbRight then
 begin
  MouseCapture := True;
  If B then Exit;
  SetBackgroundIndex(Square)
 end;
end;

procedure TMegaPalette.MouseMove(Shift: TShiftState; X, Y: Integer);
Var Square: Integer;
begin
 inherited MouseMove(Shift, X, Y);
 if FButtonDown then
 begin
  Square := SquareFromPos(X, Y);
  If (Square < 0) or (Square >= ColorsAmount[FColorMode]) then Exit;
  if FButton = mbLeft then SetForegroundIndex(Square) else
  if FButton = mbRight then SetBackgroundIndex(Square);
 end;
end;

procedure TMegaPalette.MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
begin
 inherited MouseUp(Button, Shift, X, Y);
 FButtonDown := False;
 if FButton = mbRight then
 begin
  MouseCapture := False;
  Click;
 end;
end;

procedure TMegaPalette.SetForegroundIndex(Value: Integer);
begin
 If Value < 0 then Value := 0 Else
 If Value >= ColorsAmount[FColorMode] then
  Value := ColorsAmount[FColorMode] - 1;
 FForegroundIndex := Value;
 Change;
end;

procedure TMegaPalette.SetBackgroundIndex(Value: Integer);
begin
 If Value < 0 then Value := 0 Else
 If Value >= ColorsAmount[FColorMode] then
  Value := ColorsAmount[FColorMode] - 1;
 FBackgroundIndex := Value;
 Change;
end;

function TMegaPalette.GetColor(X: Integer): TColor;
begin
 if (FColorTable <> nil) and (X >= 0) and (X < ColorsAmount[FColorMode]) then
  Result := RGBZ_ColorFormat.FromRGBQuad(FColorFormat.ToRGBQuad(Pointer(Integer(FColorTable) +
                         X * FColorFormat.ColorSize)^)) else
  Result := 0;
end;

procedure TMegaPalette.SetColor(X: Integer; Color: TColor);
begin
 if (FColorTable <> nil) and (X >= 0) and (X < ColorsAmount[FColorMode]) then
 begin
  FColorFormat.SetColor(FColorTable, X, RGBZ_ColorFormat.ValueToRGBQuad(Color));
  Invalidate;
  PaletteChange;
  Change;
 end;
end;

procedure TMegaPalette.WMSize(var Message: TWMSize);
begin
 DisableAlign;
 try
  inherited;
  UpdateCellSizes(False);
 finally
  EnableAlign;
 end;
end;

procedure TMegaPalette.CMCtl3DChanged(var Message: TMessage);
begin
 inherited;
 Invalidate;
end;

Function TMegaPalette.GetForegroundColor: TColor;
begin
 Result := Colors[FForegroundIndex];
end;

Function TMegaPalette.GetBackgroundColor: TColor;
begin
 Result := Colors[FBackgroundIndex];
end;

procedure TMegaPalette.SetTransparentIndex(Value: Integer);
begin
 If Value < 0 then Value := -1 Else
 If Value >= ColorsAmount[FColorMode] then
  Value := ColorsAmount[FColorMode] - 1;
 FTransparentIndex := Value;
 Change;
 PaletteChange;
 Invalidate;
end;

destructor TMegaPalette.Destroy;
begin
 FTransBkg.Free;
 inherited;
end;

procedure TMegaPalette.UpdateTransBkg;
var
 X, Y: Integer;
 PW: PWord;
 W: Word;
begin
 FTransBkg.SetSize(FCellXSize shr 1, FCellYSize, 4);
 PW := FTransBkg.TopPBits;
 with FTransBkg do
 for Y := 0 to Height - 1 do
 begin
  if Odd(Y shr 2) then
   W := $1100 else
   W := $0011;
  for X := 0 to WidthBytes shr 1 - 1 do
  begin
   PW^ := W;
   Inc(PW);
  end;
  Dec(PW, WidthBytes);
 end;
end;

end.
