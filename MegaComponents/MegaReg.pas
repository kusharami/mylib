unit MegaReg;

interface

uses
  Classes, MegaPalette, MegaRuler, ClickSplit, TileMapView, LayerListBox,
  HSColorPickerEx, LColorPickerEx;

procedure Register;

{$R cursor.res}

implementation

procedure Register;
begin
  RegisterComponents('Mega Components',
    [TMegaPalette, TMegaRuler, TClickSplitter, TTileMapView, TLayerListBox,
     THSColorPickerEx, TLColorPickerEx]);
end;

end.
