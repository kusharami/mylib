unit MegaRuler;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics;

type
  TRulerMode = (rmHorisontal, rmVertical);
  TMegaRuler = class(TCustomControl)
  private
    FScale: Integer;
    FRulerMode: TRulerMode;
    FButton: TMouseButton;
    FButtonDown: Boolean;
    FOnChange: TNotifyEvent;
    FRulerIndex: Integer;
    FHintName: String;
    FBitmap: TBitmap;
    procedure SetRulerIndex(Value: Integer);
    procedure SetScale(Value: Integer);
    procedure SetRulerMode(Value: TRulerMode);
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CreateWnd; override;
    procedure Paint; override;
    procedure Change; dynamic;
  public
    CanChange: Boolean;  
    constructor Create(AOwner: TComponent); override;
  published
    property Align;
    property Ctl3D;
    property Scale: Integer read FScale write SetScale;
    property RulerMode: TRulerMode read FRulerMode write SetRulerMode;
    property RulerIndex: Integer read FRulerIndex write SetRulerIndex;
    property HintName: String read FHintName write FHintName;
    property Enabled;
    property PopupMenu;
    property Color;
    property ShowHint;
    property Visible;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property OnClick;
    property OnDblClick;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
  end;

implementation

Uses Forms;

procedure TMegaRuler.SetRulerMode(Value: TRulerMode);
begin
 If Value = FRulerMode then Exit;
 FRulerMode := Value;
 Invalidate;
end;

procedure TMegaRuler.SetRulerIndex(Value: Integer);
begin
 If Value < 0 then Value := 0;
 If Value > 10000 then Value := 10000;
 If Value = FRulerIndex then Exit; 
 FRulerIndex := Value;
 Invalidate;
 If CanChange then Change;
end;

procedure TMegaRuler.SetScale(Value: Integer);
begin
 If Value = FScale then Exit;
 If Value < 1 then Value := 1;
 If Value > 16 then Value := 16;
 FScale := Value;
 Invalidate;
end;

var
  HintWnd: THintWindow;

function GetHint: THintWindow;
begin
  if HintWnd = nil then
   HintWnd := HintWindowClass.Create(Application);
  Result := HintWnd;
end;

procedure TMegaRuler.MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
Var
 IndexHint: THintWindow;
 S: String;
 RC: TRect;
begin
 inherited MouseDown(Button, Shift, X, Y);
 FButton := Button;
 If Button = mbLeft then
 begin
  IndexHint := GetHint;
  FButtonDown := True;
  Case FRulerMode of
   rmHorisontal: RulerIndex := X div FScale;
   rmVertical: RulerIndex := Y div FScale;
  end;
  S := FHintName + ': ' + IntToStr(FRulerIndex);
  Hint := S;
  IndexHint.Color := clInfoBk;
  RC := IndexHint.CalcHintRect(200, S, NIL);
  OffsetRect(RC, ClientOrigin.X + X, ClientOrigin.Y - IndexHint.Height);
  IndexHint.ActivateHint(RC, S);
  SendMessage(IndexHint.Handle, WM_NCPAINT, 1, 0);
  IndexHint.Invalidate;
  IndexHint.Update;
 end;
end;

procedure TMegaRuler.MouseMove(Shift: TShiftState; X, Y: Integer);
Var
 IndexHint: THintWindow;
 S: String;
 RC: TRect;
begin
 inherited MouseMove(Shift, X, Y);
 if FButtonDown then
 begin
  Case FRulerMode of
   rmHorisontal: RulerIndex := X div FScale;
   rmVertical: RulerIndex := Y div FScale;
  end;
  IndexHint := GetHint;
  S := FHintName + ': ' + IntToStr(FRulerIndex);
  Hint := S;
  RC := IndexHint.CalcHintRect(200, S, NIL);
  OffsetRect(RC, ClientOrigin.X + X, ClientOrigin.Y - IndexHint.Height);
  IndexHint.ActivateHint(RC, S);
  SendMessage(IndexHint.Handle, WM_NCPAINT, 1, 0);
  IndexHint.Invalidate;
  IndexHint.Update;
 end;
end;

procedure TMegaRuler.MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
begin
 inherited MouseUp(Button, Shift, X, Y);
 FButtonDown := False;
 ShowWindow(GetHint.Handle, SW_HIDE);
end;

procedure TMegaRuler.CreateWnd;
begin
 inherited CreateWnd;
 SetWindowLong(Handle, GWL_STYLE, GetWindowLong(Handle, GWL_STYLE)
  or WS_CLIPSIBLINGS);
end;

procedure TMegaRuler.Paint;
Var I, H: Integer;
begin
 With FBitmap, Canvas do
 begin
  Brush.Style := bsSolid;
  Pen.Style := psSolid;
  Pen.Color := clBlack; 
  Brush.Color := Color;
  Rectangle(ClipRect);
  Case FRulerMode of
   rmHorisontal:
   begin
    H := Height;
    If FScale > 1 then
    For I := 0 to Width div FScale do
    begin
     MoveTo(I * FScale, 0);
     LineTo(I * FScale, H);
    end;
    Brush.Color := clBlack;
    I := FRulerIndex * FScale;
    Rectangle(I, 0, I + FScale, H - 1);
   end;
   rmVertical:
   begin
    H := Width;
    If FScale > 1 then
    For I := 0 to Height div FScale do
    begin
     MoveTo(0, I * FScale);
     LineTo(H, I * FScale);
    end;
    Brush.Color := clBlack;
    I := FRulerIndex * FScale;
    Rectangle(0, I, H - 1, I + FScale);
   end;
  end;
 end;
 Canvas.Draw(0, 0, FBitmap);
end;

procedure TMegaRuler.Change;
begin
 Changed;
 if Assigned(FOnChange) then FOnChange(Self);
end;

constructor TMegaRuler.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 FRulerIndex := 0;
 FScale := 1;
 FHintName := '������';
 FRulerMode := rmHorisontal;
 FBitmap := TBitmap.Create;
 FBitmap.Width := Width;
 FBitmap.Height := Height;
 CanChange := True;
 SetBounds(0, 0, 100, 24);
 Align := alTop;
end;

procedure TMegaRuler.WMSize(var Message: TWMSize);
begin
 DisableAlign;
 try
  inherited;
  FBitmap.Width := Width;
  FBitmap.Height := Height;
  Invalidate;
 finally
  EnableAlign;
 end;
end;

procedure TMegaRuler.CMCtl3DChanged(var Message: TMessage);
begin
 inherited;
 Invalidate;
end;

end.
