unit PropEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, StdCtrls, TntClasses, TntStdCtrls,
  Graphics, Controls, Forms, VirtualTrees, Buttons;

Const
 WM_STARTEDITING = WM_USER + 778;

type
  TCustomPropertyEditor = Class;
  TPropertyEditLink = Class;
  TLinkEdit = class(TTntCustomEdit)
  private
    FLink: TPropertyEditLink;
    FFilter: WideString;
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure CMCancelMode(var Message: TCMCancelMode); message CM_CancelMode;
    procedure WMKillFocus(var Message: TMessage); message WM_KILLFOCUS;
    procedure CMRelease(var Message: TMessage); message CM_RELEASE;
    procedure WMChar(var Message: TWMChar); message WM_CHAR;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure KeyPress(var Key: Char); override;
    procedure Click; override;
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(Link: TPropertyEditLink); reintroduce;
    destructor Destroy; override;
    procedure Release; virtual;
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property HideSelection;
    property MaxLength;
    property OEMConvert;
    property PasswordChar;
  end;

  TLinkButton = class(TSpeedButton)
  private
    FLink: TPropertyEditLink;
  public
    constructor Create(Link: TPropertyEditLink); reintroduce;
    procedure Click; override;
  end;

  TPopupListBox = class(TTntCustomListbox)
  private
    FLink: TPropertyEditLink;
  protected
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DoExit; override;
  public
    constructor Create(Link: TPropertyEditLink); reintroduce;
  end;

  TEditType = (etString, etStringReadOnly, etPickString,
               etEllipsis, etEllipsisReadOnly);

  TPropertyEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FEditType: TEditType;
    FEdit: TLinkEdit;
    FButton: TLinkButton;
    FPickList: TPopupListBox;
    FList: TTntStringList;
    FTree: TCustomPropertyEditor;
    FNode: PVirtualNode;
    FStopping: Boolean;
    procedure SetEdit(const Value: TLinkEdit);
    procedure CloseUp(Accept: Boolean);
    procedure DropDown;
  public
    constructor Create(EditType: TEditType);
    destructor Destroy; override;

    function BeginEdit: Boolean; virtual; stdcall;
    function CancelEdit: Boolean; virtual; stdcall;
    property Edit: TLinkEdit read FEdit write SetEdit;
    function EndEdit: Boolean; virtual; stdcall;
    function GetBounds: TRect; virtual; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; virtual; stdcall;
    procedure ProcessMessage(var Message: TMessage); virtual; stdcall;
    procedure SetBounds(R: TRect); virtual; stdcall;
    property CurNode: PVirtualNode read FNode;
  end;

  TGetStringEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var Text: WideString) of object;
  TSetStringEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Text: WideString) of object;
  TGetEditTypeEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var EditType: TEditType) of object;
  TGetPickListEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var List: TTntStrings; Var FreeAfterUse: Boolean) of object;
  TGetColorEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var Color: TColor) of object;
  TGetStyleEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var Style: TFontStyles) of object;
  TEllipsisClickEvent =
  procedure(Sender: TBaseVirtualTree; Node: PVirtualNode;
            Var Accepted: Boolean) of Object;

  TCustomPropertyEditor = Class(TCustomVirtualStringTree)
  private
    FSelectedIndex: Cardinal;
    FHitInfo: THitInfo;
    FAcceptError: Boolean;
    FMouseDown: Boolean;
    FPropertyEditLink: TPropertyEditLink;
    FTracking: Boolean;
    FTrackPos: Integer;
    FValueTextColor: TColor;
    FValueTextStyle: TFontStyles;
    FDoNotCancel: Boolean;
    FOnGetPropertyName: TGetStringEvent;
    FOnGetValue: TGetStringEvent;
    FOnGetPickList: TGetPickListEvent;
    FOnSetValue: TSetStringEvent;
    FOnGetEditType: TGetEditTypeEvent;
    FOnEllipsisClick: TEllipsisClickEvent;
    FOnGetPropertyColor: TGetColorEvent;
    FOnGetPropertyStyle: TGetStyleEvent;
    FOnGetValueColor: TGetColorEvent;
    FOnGetValueStyle: TGetStyleEvent;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure WMStartEditing(var Message: TMessage); message WM_STARTEDITING;
    function GetOptions: TStringTreeOptions;
    procedure SetOptions(const Value: TStringTreeOptions);
    procedure ForceEdit(Node: PVirtualNode);
    function GetPropWidth: Integer;
    function GetValueWidth: Integer;
    procedure SetPropWidth(Value: Integer);
    procedure SetValueWidth(Value: Integer);
    property TreeOptions: TStringTreeOptions read GetOptions write SetOptions;
    property Header;
    procedure SetValTextColor(Col: TColor);
    procedure SetValStyle(Style: TFontStyles);
  protected
    procedure DoFocusNode(Node: PVirtualNode; Ask: Boolean); override;
    function GetOptionsClass: TTreeOptionsClass; override;
    procedure Click; override;
    procedure DoChange(Node: PVirtualNode); override;
    procedure DoCanEdit(Node: PVirtualNode; Column: TColumnIndex;
      var Allowed: Boolean); override;
    procedure DoCollapsed(Node: PVirtualNode); override;
    function DoCreateEditor(Node: PVirtualNode;
      Column: TColumnIndex): IVTEditLink; override;
    procedure DoExpanded(Node: PVirtualNode); override;
    procedure DoGetText(Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var Text: WideString); override;
    procedure DoInitNode(Parent, Node: PVirtualNode;
      var InitStates: TVirtualNodeInitStates); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure HandleMouseDown(var Message: TWMMouse;
      const HitInfo: THitInfo); override;
    function DoEndEdit: Boolean; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer); override;
    procedure Resize; override;
    procedure DoGetPropertyName(Node: PVirtualNode; var Text: WideString);
      virtual;
    procedure DoGetValue(Node: PVirtualNode; var Text: WideString);
      virtual;
    procedure DoGetPickList(Node: PVirtualNode; var List: TTntStrings;
              Var FreeAfterUse: Boolean); virtual;
    procedure DoSetValue(Node: PVirtualNode; Text: WideString); virtual;
    function DoGetEditType(Node: PVirtualNode): TEditType; virtual;
    procedure DoScroll(DeltaX, DeltaY: Integer); override;
    function EllipsisClick(Node: PVirtualNode): Boolean; virtual;
    procedure DoPaintText(Node: PVirtualNode; const Canvas: TCanvas;
      Column: TColumnIndex; TextType: TVSTTextType); override;
    function GetPropertyColor(Node: PVirtualNode): TColor; virtual;
    function GetPropertyStyle(Node: PVirtualNode): TFontStyles; virtual;
    function GetValueStyle(Node: PVirtualNode): TFontStyles; virtual;
    function GetValueColor(Node: PVirtualNode): TColor; virtual;
    function DoCancelEdit: Boolean; override;
  public
    property SelectedIndex: Cardinal read FSelectedIndex write FSelectedIndex;
    property AcceptError: Boolean read FAcceptError write FAcceptError;
    property Canvas;
    property Colors;
    property DoNotCancel: Boolean read FDoNotCancel write FDoNotCancel;
    property ValueTextStyle: TFontStyles read FValueTextStyle write SetValStyle;
    property ValueTextColor: TColor read FValueTextColor write SetValTextColor;
    property PropertyWidth: Integer read GetPropWidth write SetPropWidth;
    property ValueWidth: Integer read GetValueWidth write SetValueWidth;
    property OnGetPropertyName: TGetStringEvent read FOnGetPropertyName
                                               write FOnGetPropertyName;
    property OnGetValue: TGetStringEvent read FOnGetValue
                                        write FOnGetValue;
    property OnGetPickList: TGetPickListEvent read FOnGetPickList
                                           write FOnGetPickList;
    property OnSetValue: TSetStringEvent read FOnSetValue
                                         write FOnSetValue;
    property OnGetEditType: TGetEditTypeEvent read FOnGetEditType
                                             write FOnGetEditType;
    property OnEllipsisClick: TEllipsisClickEvent read FOnEllipsisClick
                                                 write FOnEllipsisClick;
    property OnGetPropertyColor: TGetColorEvent read FOnGetPropertyColor
                                               write FOnGetPropertyColor;
    property OnGetPropertyStyle: TGetStyleEvent read FOnGetPropertyStyle
                                               write FOnGetPropertyStyle;
    property OnGetValueColor: TGetColorEvent read FOnGetValueColor
                                            write FOnGetValueColor;
    property OnGetValueStyle: TGetStyleEvent read FOnGetValueStyle
                                            write FOnGetValueStyle;

    constructor Create(AOwner: TComponent); override;
    procedure AcceptEdit;
    procedure SetFocus; override;
    procedure RefreshValue(Node: PVirtualNode);
  end;

  TVirtualPropertyEditor = Class(TCustomPropertyEditor)
  published
    property Align;
    property Anchors;
    property BiDiMode;
    property BevelEdges;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderStyle;
    property BorderWidth;
    property ChangeDelay;
    property Color default clBtnFace;
    property Constraints;
    property Ctl3D;
    property DefaultNodeHeight;
    property DefaultText;
    property DragMode;
    property Enabled;
    property Font;
    property HintAnimation;
    property HintMode;
    property LineMode;
    property LineStyle default lsSolid;
    property NodeDataSize;
    property ParentBiDiMode;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property RootNodeCount;
    property ScrollBarOptions;
    property ShowHint;
    property TabOrder;
    property TabStop default True;
    property Visible;
    property WantTabs;
    property PropertyWidth default 98;
    property ValueWidth default 98;
    property ValueTextColor default clNavy;
    property ValueTextStyle default [];

    property OnChange;
    property OnClick;
    property OnColumnResize;
    property OnContextPopup;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnFreeNode;
    property OnPaintText;
    property OnGetHelpContext;
    property OnGetHint;
    property OnGetLineStyle;
    property OnGetNodeDataSize;
    property OnGetPopupMenu;
    property OnInitNode;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnLoadNode;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnResetNode;
    property OnResize;
    property OnSaveNode;
    property OnScroll;
    property OnShortenString;
    property OnShowScrollbar;
    property OnStateChange;
    property OnStructureChange;
    property OnUpdating;
    //New properties
    property OnGetPropertyName;
    property OnGetValue;
    property OnGetPickList;
    property OnSetValue;
    property OnGetEditType;
    property OnEllipsisClick;
    property OnGetPropertyColor;
    property OnGetPropertyStyle;
    property OnGetValueColor;
    property OnGetValueStyle;
  end;

implementation

//----------------- TLinkEdit ------------------------------------------------//

procedure TLinkEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
Var I: Integer;
begin
 With FLink do If Assigned(FList) and (FEditType = etPickString) then
 begin
  I := FList.IndexOf(Text);
  If I < 0 then
  begin
   FTree.DoSetValue(FNode, Text);
   Modified := False;
   Font.Style := FTree.GetValueStyle(FNode);
   SelectAll;
  end Else
  begin
   Inc(I);
   If I >= FList.Count then I := 0;
   Text := FList[I];
   FTree.DoSetValue(FNode, Text);
   Modified := False;
   Font.Style := FTree.GetValueStyle(FNode);
   SelectAll;
  end;
 end Else
 If FEditType in [etEllipsis, etEllipsisReadOnly] then FButton.Click Else
  inherited;
end;

procedure TLinkEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  With Message do
  if (Sender <> Self) and (Sender <> FLink.FButton) and
     (Sender <> FLink.FPickList) then
   FLink.CloseUp(False);
end;

procedure TLinkEdit.WMKillFocus(var Message: TMessage);
begin
 inherited;
 FLink.CloseUp(False);
 If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
 begin
  With FLink do
  begin
   FTree.DoSetValue(FNode, Text);
   Modified := False;
  end;
  With FLink do Font.Style := FTree.GetValueStyle(FNode);
  SelectAll;
 end;
end;

procedure TLinkEdit.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    wm_KeyDown, wm_SysKeyDown, wm_Char:
    If Assigned(FLink) then
    With FLink, TWMKey(Message) do
    begin
     if (CharCode <> 0) and Assigned(FPickList) then
     begin
       with TMessage(Message) do
        SendMessage(FPickList.Handle, Msg, WParam, LParam);
       Exit;
     end;
    end;
  end;
  inherited;
end;

constructor TLinkEdit.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
end;

procedure TLinkEdit.KeyPress(var Key: Char);
Var L, LL: Integer; S, WS: WideString;
Function Find: Boolean;
Var I: Integer; SU, WU: WideString;
begin
 SU := WideUpperCase(S);
 L := Length(SU);
 Result := False;
 If L < 1 then Exit;
 With FLink, FList do For I := 0 to Count - 1 do
 begin
  WU := WideUpperCase(FList[I]);
  LL := Length(WU);
  If (LL > 0) and (LL >= L) and
      CompareMem(Addr(SU[1]), Addr(WU[1]), L shl 1) then
  begin
   Result := True;
   WS := FList[I];
   Exit;
  end;
 end;
end;
begin
 If (Key = #13) or FLink.FTree.FTracking then Key := #0 Else
 With FLink do If (FEditType = etPickString) and (Ord(Key) <> VK_BACK) then
 begin
  L := SelLength;
  S := Text;
  If SelStart = Length(S) - L then
  begin
   SetLength(S, SelStart);
   S := S + Key;
   If Find then
   begin
    Delete(WS, 1, L);
    Text := S + WS;
    SelStart := L;
    SelLength := LL;
    Key := #0;
    Modified := True;
   end;
  end;
 end;
end;

procedure TLinkEdit.CMRelease(var Message: TMessage);
begin
 Free;
end;

destructor TLinkEdit.Destroy;
begin
 Finalize(FFilter);
 inherited;
end;

procedure TLinkEdit.WMChar(var Message: TWMChar);
begin
 If not (Message.CharCode in [VK_ESCAPE, VK_TAB]) then inherited;
end;

procedure TLinkEdit.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
 inherited;
 Message.Result := Message.Result or DLGC_WANTALLKEYS or
                   DLGC_WANTTAB or DLGC_WANTARROWS;
end;

procedure TLinkEdit.WMKeyDown(var Message: TWMKeyDown);
Var Shift: TShiftState; Key: Word;
Label KeyAccept;
begin
 IF FLink.FTree.FTracking or
    Assigned(FLink.FPickList) then Exit;
 Key := Message.CharCode;
 Case Key of
  VK_ESCAPE:
  begin
   With FLink, FTree do RefreshValue(FNode);
   Goto KeyAccept;
  end;
  VK_RETURN:
  begin
   If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
   begin
    With FLink do
    begin
     FTree.DoSetValue(FNode, Text);
     Modified := False;
    end;
    With FLink do Font.Style := FTree.GetValueStyle(FNode);
    SelectAll;
   end;
   Goto KeyAccept;
  end;
  VK_UP, VK_DOWN, VK_PRIOR, VK_NEXT, VK_TAB:
  begin KeyAccept:
   Shift := KeyDataToShiftState(Message.KeyData);
   if Key <> VK_TAB then with FLink.FTree do
   begin
    if (FocusedNode = NIL) or
     (((Key = VK_UP) or (Key = VK_PRIOR)) and (GetFirst <> FocusedNode)) or
     (((Key = VK_DOWN) or (Key = VK_NEXT)) and (GetLast <> FocusedNode)) then
    Windows.SetFocus(Handle);
   end else
   begin
    If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
    begin
     With FLink do
     begin
      FTree.DoSetValue(FNode, Text);
      Modified := False;
     end;
     With FLink do Font.Style := FTree.GetValueStyle(FNode);
     SelectAll;
    end;
   end;
   If Shift = [] then
    PostMessage(FLink.FTree.Handle, WM_KEYDOWN, Key, 0);
  end;
  Else inherited;
 end;
end;

procedure TLinkEdit.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := ExStyle and not WS_EX_CLIENTEDGE;
 end;
end;

procedure TLinkEdit.Release;
begin
 if HandleAllocated then
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

procedure TLinkEdit.Click;
begin
 If Assigned(FLink) then With FLink do
 If (FEditType = etPickString) and Assigned(FPickList) then CloseUp(False);
end;

//---------------------------- TLinkButton -----------------------------------//

procedure TLinkButton.Click;
Var S: WideString;
begin
 If Assigned(FLink) then With FLink, FTree do
 begin
  If FEditType = etPickString then
  begin
   If not Assigned(FPickList) then
    DropDown Else
    CloseUp(False);
  end Else
  begin
   With FLink do
   begin
    FTree.DoSetValue(FNode, FEdit.Text);
    FEdit.Modified := False;
   end;
   With FLink do FEdit.Font.Style := FTree.GetValueStyle(FNode);
   FEdit.SelectAll;
   If EllipsisClick(FNode) then
   begin
    Invalidate;
    DoGetValue(FNode, S);
    With FEdit do
    begin
     Text := S;
     SelectAll;
    end;
   end;
  end;
 end;
 inherited;
end;

constructor TLinkButton.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
end;

//---------------------------- TPopupListBox --------------------------------//

procedure TPopupListBox.CreateWnd;
begin
  inherited CreateWnd;
  Windows.SetParent(Handle, 0);
  CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TPopupListBox.DoExit;
begin
 FLink.CloseUp(False);
end;

procedure TPopupListBox.WMKeyDown(var Message: TWMKeyDown);
Var Key: Word;
begin
 Key := Message.CharCode;
 Case Key of
  VK_ESCAPE, VK_RETURN, VK_TAB: FLink.CloseUp(Key = VK_RETURN);
  Else inherited;
 end;
end;

constructor TPopupListBox.Create(Link: TPropertyEditLink);
begin
 inherited Create(NIL);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
end;

procedure TPopupListBox.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
  AddBiDiModeExStyle(ExStyle);
  WindowClass.Style := CS_SAVEBITS;
 end;
end;

procedure TPopupListBox.MouseUp(Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 FLink.CloseUp(True);
end;

//---------------------------- TPropertyEditLink ---------------------------//

constructor TPropertyEditLink.Create(EditType: TEditType);
Var X, Y: Integer; Bitmap: TBitmap;
begin
 FEditType := EditType;
 If EditType in [etPickString, etEllipsis, etEllipsisReadOnly] then
 begin
  FButton := TLinkButton.Create(Self);
  With FButton do
  begin
   Visible := False;
   Bitmap := TBitmap.Create;
   With Bitmap do
   begin
    Width := 11;
    Height := 11;
    With Canvas do
    begin
     Brush.Color := $FF00FF;
     Brush.Style := bsSolid;
     FillRect(ClipRect);
     If EditType = etPickString then
     begin
      For Y := 0 to 3 do For X := Y + 2 to 8 - Y do
       Pixels[X, Y + 4] := clBlack;
      FList := TTntStringList.Create;
     end Else
     For Y := 4 to 5 do
     begin
      For X := 2 to 3 do Pixels[X, Y] := clBlack;
      For X := 5 to 6 do Pixels[X, Y] := clBlack;
      For X := 8 to 9 do Pixels[X, Y] := clBlack;
     end;
    end;
    Transparent := True;
    TransparentColor := $FF00FF;
   end;
   Glyph := Bitmap;
   Bitmap.Free;
  end;
 end;
 FEdit := TLinkEdit.Create(Self);
 with FEdit do
 begin
  Visible := False;
  BorderStyle := bsSingle;
  AutoSize := False;
 end;
end;

destructor TPropertyEditLink.Destroy;
begin
 If Assigned(FList) then FList.Free;
 If Assigned(FButton) then FButton.Free;
 If FEdit.Modified then
  FTree.DoSetValue(FNode, FEdit.Text);
 FEdit.Release;
 inherited;
end;

function TPropertyEditLink.BeginEdit: Boolean;
begin
 Result := not FStopping;
 if Result then
 begin
  FEdit.Show;
  FEdit.SelectAll;
  if FTree.Focused then FEdit.SetFocus;
  If Assigned(FButton) then
  begin
   FButton.Parent := FTree;
   FButton.Show;
  end;
 end;
end;

procedure TPropertyEditLink.SetEdit(const Value: TLinkEdit);
begin
 if Assigned(FEdit) then FEdit.Free;
 FEdit := Value;
end;

function TPropertyEditLink.CancelEdit: Boolean;
begin
 Result := not FStopping;
 If Result then
 begin
  FStopping := True;
  FEdit.Hide;
  If Assigned(FButton) then FButton.Hide;
  FTree.CancelEditNode;
  FEdit.FLink := nil;
  If Assigned(FButton) then FButton.FLink := NIL;
 end;
end;

function TPropertyEditLink.EndEdit: Boolean;
begin
 Result := not FStopping;
 if Result then
 try
  FStopping := True;
  if FEdit.Modified then
  begin
   FTree.DoSetValue(FNode, FEdit.Text);
   FEdit.Modified := False;
  end;
  FEdit.Hide;
  FEdit.FLink := nil;
  If Assigned(FButton) then
  begin
   FButton.Hide;
   FButton.FLink := NIL;
  end;
 except
  FStopping := False;
  raise;
 end;
end;

function TPropertyEditLink.GetBounds: TRect;
begin
 Result := FEdit.BoundsRect;
 If Assigned(FButton) then Result.Right := FButton.BoundsRect.Right;
end;

function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;
Var Txt: WideString; List: TTntStrings; FAU: Boolean;
begin
 Result := Tree is TCustomPropertyEditor;
 If Result then
 begin
  FTree := Tree as TCustomPropertyEditor;
  FNode := Node;
  FTree.DoGetValue(Node, Txt);
  If FEditType in [etStringReadOnly, etEllipsisReadOnly] then
  begin
   FEdit.ReadOnly := True;
   FEdit.Color := clBtnFace;
  end;
  If FEditType = etPickString then
  begin
   FTree.DoGetPickList(Node, List, FAU);
   If Assigned(List) then
   begin
    FList.Assign(List);
    If FAU then List.Free;
   end;
  end;
  FEdit.Font.Color := clWindowText;
  FEdit.Font.Style := FTree.GetValueStyle(Node);
  FEdit.Parent := Tree;
  FEdit.RecreateWnd;
  FEdit.HandleNeeded;
  FEdit.Text := Txt;
 end;
end;

procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
 FEdit.WindowProc(Message);
end;

procedure TPropertyEditLink.SetBounds(R: TRect);
Var Dummy: Integer; RR: TRect;
begin
 FTree.Header.Columns.GetColumnBounds(1, Dummy, R.Right);
 If Assigned(FPickList) then CloseUp(False);
 If Assigned(FButton) then
 begin
  RR.Right := R.Right;
  Dec(R.Right, 17);
  RR.Left := R.Right;
  RR.Top := R.Top;
  RR.Bottom := R.Bottom;
  FButton.BoundsRect := RR;
 end;
 FEdit.BoundsRect := R;
end;

procedure TPropertyEditLink.CloseUp(Accept: Boolean);
Var S: WideString;
begin
 If Assigned(FPickList) then
 begin
  With FPickList do
  begin
   If Accept and (ItemIndex >= 0) then
   begin
    S := Items.Strings[ItemIndex];
    FEdit.Text := S;
    FTree.DoSetValue(FNode, S);
    FEdit.Modified := False;
   end;
   FEdit.SelectAll;
   FPickList.Free;
   FPickList := NIL;
   If FEdit.Visible then FEdit.SetFocus;
  end;
 end;
end;

procedure TPropertyEditLink.DropDown;
Var R: TRect; P: TPoint; I, J: Integer; SZ: TSize; S: WideString;
begin
 If not Assigned(FPickList) then
 begin
  FPickList := TPopupListBox.Create(Self);
  With FPickList do
  begin
   FEdit.SelStart := FEdit.SelLength;
   Visible := False;
   Parent := FLink.FTree;
   If Assigned(FList) then Items.Assign(FList);
   With FButton.BoundsRect do
   begin
    R.Right := Right;
    R.Top := Bottom;
   end;
   With FEdit.BoundsRect do
   begin
    P := Ftree.ClientToScreen(Point(Left, R.Top));
    R.Left := Left;
    R.Bottom := R.Top + 16;
   end;
   Inc(R.Right);
   Inc(R.Top);
   ClientWidth := R.Right - R.Left;
   If Count > 0 then
   begin
    IntegralHeight := True;
    ItemIndex := Items.IndexOf(FEdit.Text);
   end;
   If Count > 0 then
    ClientHeight := ItemHeight * Count Else
    ClientHeight := ItemHeight;
   J := ClientWidth;
   for I := 0 to Count - 1 do
   begin
    S := Items[I];
    SZ.cx := 0;
    SZ.cy := 0;
    GetTextExtentPoint32W(Canvas.Handle, PWideChar(S), Length(S), SZ);
    if SZ.cx + 4 > J then J := SZ.cx + 4;
   end;
   Dec(P.X, J - ClientWidth);
   ClientWidth := J;
   With P do
   begin
    If Y + Height > Screen.Height then Dec(Y, Height);
    SetWindowPos(Handle, HWND_TOP, X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
   end;
   Invalidate;
   Windows.SetFocus(Handle);
  end;
 end;
end;

//----------------- TCustomPropertyEditor ------------------------------------//

Procedure TCustomPropertyEditor.DoChange(Node: PVirtualNode);
begin
 inherited;
 If Assigned(Node) then
  PostMessage(Self.Handle, WM_STARTEDITING, Integer(Node), 0);
end;

function TCustomPropertyEditor.DoCreateEditor(Node: PVirtualNode; Column: TColumnIndex): IVTEditLink;
begin
 FPropertyEditLink := TPropertyEditLink.Create(DoGetEditType(Node));
 Result := FPropertyEditLink;
end;

constructor TCustomPropertyEditor.Create(AOwner: TComponent);
Var
 M: TVTMiscOptions;
 P: TVTPaintOptions;
 S: TVTSelectionOptions;
begin
 inherited;
 ClipboardFormats.Clear;
 Colors.BorderColor := clWindowText;
 Colors.GridLineColor := clBtnShadow;
 Colors.TreeLineColor := clBtnShadow;
 With Header do
 begin
  AutoSizeIndex := -1;
  With Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 0;
   Margin := 0;
   PropertyWidth := 98;
  end;
  With Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 1;
   Margin := 0;
  end;
  MainColumn := 0;
  Options := [hoAutoResize];
 end;
 FocusedColumn := 0;
 ScrollBarOptions.ScrollBars := ssVertical;
 Indent := 12;
 DragOperations := [];
 LineStyle := lsSolid;
 TextMargin := 2;
 Margin := 0;
 FValueTextColor := clNavy;
 Color := clBtnFace;
 With TreeOptions do
 begin
  M := MiscOptions;
  Include(M, toEditable);
  Include(M, toGridExtensions);
  P := PaintOptions;
  Include(P, toHideFocusRect);
  Include(P, toShowHorzGridLines);
  Include(P, toShowVertGridLines);
  Exclude(P, toShowTreeLines);
  Include(P, toAlwaysHideSelection);
  S := SelectionOptions;
  Include(S, toFullRowSelect);
  MiscOptions := M;
  PaintOptions := P;
  SelectionOptions := S;
 end;
end;

procedure TCustomPropertyEditor.DoCanEdit(Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
 inherited;
 If Column = 0 then Allowed := False;
end;

procedure TCustomPropertyEditor.DoGetText(Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var Text: WideString);
begin
 If Node <> NIL then
 Case Column of
  0: DoGetPropertyName(Node, Text);
  1: DoGetValue(Node, Text);
 end;
end;

procedure TCustomPropertyEditor.DoInitNode(Parent, Node: PVirtualNode;
  var InitStates: TVirtualNodeInitStates);
begin
 inherited;
 If not (csDesigning in ComponentState) and (Node.Index = FSelectedIndex) then
  ForceEdit(Node);
end;

procedure TCustomPropertyEditor.KeyDown(var Key: Word; Shift: TShiftState);
begin
 If FTracking then Exit;
 inherited;
 If Key = VK_PRIOR then
 begin
  If FPropertyEditLink.CurNode = GetFirst then Key := 0;
 end Else If Key = VK_NEXT then
 begin
  If FPropertyEditLink.CurNode = GetLast(NIL) then Key := 0;
 end;
end;

procedure TCustomPropertyEditor.WMStartEditing(var Message: TMessage);
var
  Node: PVirtualNode;
begin
 Node := Pointer(Message.WParam);
 EditNode(Node, 1);
end;

procedure TCustomPropertyEditor.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Var N: PVirtualNode;
begin
 inherited;
 If Cursor = crHSplit then
 begin
  DoStateChange([tsSizing]);
  FTracking := True;
  FTrackPos := Header.Columns.Items[0].Width - X;
 end Else
 begin
  If Assigned(FPropertyEditLink) then
  begin
   With FPropertyEditLink do If Assigned(FPickList) then CloseUp(False);
   N := GetNodeAt(X, Y);
   If (N <> NIL) and (FPropertyEditLink.CurNode = N) then
    PostMessage(Self.Handle, WM_STARTEDITING, Integer(N), 0);
  end;
 end;
end;

function TCustomPropertyEditor.GetOptions: TStringTreeOptions;
Var O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 Result := O as TStringTreeOptions;
end;

procedure TCustomPropertyEditor.SetOptions(const Value: TStringTreeOptions);
Var O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 O.Assign(Value);
end;

function TCustomPropertyEditor.GetOptionsClass: TTreeOptionsClass;
begin
 Result := TStringTreeOptions;
end;

procedure TCustomPropertyEditor.DoCollapsed(Node: PVirtualNode);
begin
 ForceEdit(Node);
 inherited;
end;

procedure TCustomPropertyEditor.DoFocusNode(Node: PVirtualNode; Ask: Boolean);
begin
 inherited;
end;

procedure TCustomPropertyEditor.Click;
begin
 If FTracking then Exit;
 ForceEdit(FocusedNode);
 inherited;
end;

procedure TCustomPropertyEditor.DoExpanded(Node: PVirtualNode);
begin
 ForceEdit(Node);
 inherited;
end;

procedure TCustomPropertyEditor.ForceEdit(Node: PVirtualNode);
begin
 If not IsEditing then
  PostMessage(Self.Handle, WM_STARTEDITING, Integer(Node), 0);
end;

procedure TCustomPropertyEditor.HandleMouseDown(var Message: TWMMouse;
  const HitInfo: THitInfo);
begin
 If FTracking then Exit;
 FHitInfo := HitInfo;
 FMouseDown := True;
 inherited;
 FMouseDown := False;
end;

procedure TCustomPropertyEditor.SetFocus;
begin
 If IsEditing then
 begin
  With FPropertyEditLink do
  begin
   If Assigned(FPickList) then
    CloseUp(False) Else
   If FEdit.Visible then
    FEdit.SetFocus;
  end;
 end Else inherited;
end;

function TCustomPropertyEditor.DoEndEdit: Boolean;
begin
 If not FMouseDown or (FHitInfo.HitNode <> NIL) then
  Result := inherited DoEndEdit Else
  Result := False;
end;

function TCustomPropertyEditor.GetPropWidth: Integer;
begin
 Result := Header.Columns.Items[0].Width;
end;

function TCustomPropertyEditor.GetValueWidth: Integer;
begin
 Result := Header.Columns.Items[1].Width;
end;

procedure TCustomPropertyEditor.SetPropWidth(Value: Integer);
begin
 Header.Columns.Items[0].Width := Value;
end;

procedure TCustomPropertyEditor.SetValueWidth(Value: Integer);
begin
 Header.Columns.Items[1].Width := Value;
end;

procedure TCustomPropertyEditor.MouseMove(Shift: TShiftState; X,
  Y: Integer);
Var W: Integer;
begin
 If FTracking then
 begin
  W := X - FTrackPos;
  If W >= ClientWidth - 60 then
   Header.Columns.Items[0].Width := ClientWidth - 60 Else
  begin
   If W < 40 then W := 40;
   Header.Columns.Items[0].Width := W;
  end;
 end Else
 begin
  W := Header.Columns.Items[0].Width;
  If (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit Else
   Cursor := crDefault;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
Var W: Integer;
begin
 If FTracking then
 begin
  DoStateChange([], [tsSizing]);
  FTracking := False;
  W := Header.Columns.Items[0].Width;
  If (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit Else
   Cursor := crDefault;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.Resize;
Var W: Integer;
begin
 If ValueWidth < 60 then
  W := ClientWidth - 60 Else
  W := PropertyWidth;
 If W < 40 then W := 40;
 PropertyWidth := W;
 inherited;
end;

procedure TCustomPropertyEditor.DoGetPropertyName(Node: PVirtualNode;
  var Text: WideString);
begin
 If Assigned(FOnGetPropertyName) then
  FOnGetPropertyName(Self, Node, Text) Else
  Text := DefaultText;
end;

procedure TCustomPropertyEditor.DoGetValue(Node: PVirtualNode;
  var Text: WideString);
begin
 If Assigned(FOnGetValue) then
  FOnGetValue(Self, Node, Text) Else
  Text := DefaultText;
end;

procedure TCustomPropertyEditor.DoGetPickList(Node: PVirtualNode;
  var List: TTntStrings; Var FreeAfterUse: Boolean);
begin
 List := NIL;
 FreeAfterUse := False;
 If Assigned(FOnGetPickList) then
  FOnGetPickList(Self, Node, List, FreeAfterUse);
end;

procedure TCustomPropertyEditor.DoSetValue(Node: PVirtualNode;
  Text: WideString);
begin
 FAcceptError := False;
 If Assigned(FOnSetValue) then FOnSetValue(Self, Node, Text);
end;

function TCustomPropertyEditor.DoGetEditType(Node: PVirtualNode): TEditType;
begin
 Result := etStringReadOnly;
 If Assigned(FOnGetEditType) then
  FOnGetEditType(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DoScroll(DeltaX, DeltaY: Integer);
begin
 If tsEditing in treeStates then With FPropertyEditLink do
 begin
  If Assigned(FPickList) then CloseUp(False);
  If Assigned(FButton) then
  begin
   FButton.Top := FEdit.Top;
   FButton.Height := FEdit.Height;
  end;
 end;
 inherited;
end;

function TCustomPropertyEditor.EllipsisClick(Node: PVirtualNode): Boolean;
begin
 Result := False;
 If Assigned(FOnEllipsisClick) then
  FOnEllipsisClick(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DoPaintText(Node: PVirtualNode;
  const Canvas: TCanvas; Column: TColumnIndex; TextType: TVSTTextType);
begin
 With Canvas.Font do If Column = 1 then
 begin
  Color := GetValueColor(Node);
  Style := GetValueStyle(Node);
 end Else
 begin
  Color := GetPropertyColor(Node);
  Style := GetPropertyStyle(Node);
 end;
 inherited;
end;

procedure TCustomPropertyEditor.SetValTextColor(Col: TColor);
begin
 FValueTextColor := Col;
 Invalidate;
end;

procedure TCustomPropertyEditor.SetValStyle(Style: TFontStyles);
begin
 FValueTextStyle := Style;
 Invalidate;
end;

procedure TCustomPropertyEditor.RefreshValue(Node: PVirtualNode);
Var S: WideString;
begin
 With FPropertyEditLink do
 begin
  FTree.DoGetValue(Node, S);
  FEdit.Font.Style := GetValueStyle(Node);
  FEdit.Text := S;
  FEdit.SelectAll;
 end;
end;

function TCustomPropertyEditor.GetPropertyColor(Node: PVirtualNode): TColor;
begin
 Result := Font.Color;
 If Assigned(FOnGetPropertyColor) then
  FOnGetPropertyColor(Self, Node, Result);
end;

function TCustomPropertyEditor.GetPropertyStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := Font.Style;
 If Assigned(FOnGetPropertyStyle) then
  FOnGetPropertyStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := FValueTextStyle;
 If Assigned(FOnGetValueStyle) then
  FOnGetValueStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueColor(Node: PVirtualNode): TColor;
begin
 Result := FValueTextColor;
 If Assigned(FOnGetValueColor) then
  FOnGetValueColor(Self, Node, Result);
end;

procedure TCustomPropertyEditor.CMVisibleChanged(var Message: TMessage);
begin
 inherited;
 If Message.WParam = 1 then ForceEdit(FocusedNode);
end;

procedure TCustomPropertyEditor.CMEnabledChanged(var Message: TMessage);
begin
 inherited;
 If Enabled then ForceEdit(FocusedNode);
end;

function TCustomPropertyEditor.DoCancelEdit: Boolean;
begin
 If not FDoNotCancel then Result := inherited DoCancelEdit Else
 begin
  Result := False;
  FDoNotCancel := False;
 end;
end;

procedure TCustomPropertyEditor.AcceptEdit;
begin
 If IsEditing then With FPropertyEditLink, FEdit do
 If not (FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
 begin
  DoSetValue(FNode, Text);
  Modified := False;
  Font.Style := GetValueStyle(FNode);
  SelectAll;
 end;
end;

end.

