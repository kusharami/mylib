unit PropEditor;

interface

uses
  Windows, Messages, SysUtils, Classes, StdCtrls, Dialogs, TntClasses, TntDialogs, TntStdCtrls,
  Graphics, Controls, Forms, VirtualTrees, Buttons, HexUnit, PropContainer;

const
 WM_STARTEDITING = WM_USER + 778;

 PL_PROP_CHILD_MOVABLE = 1 shl 31;
 PL_PROP_CHILD_EDITABLE = 1 shl 30;
 PL_PROP_EDITABLE = 1 shl 29;
// PL_EDITABLE_LIST = 1 shl 28;
 PL_EDIT_PROP_CHILDREN ={PL_EDITABLE_LIST or }PL_PROP_CHILD_MOVABLE or PL_PROP_CHILD_EDITABLE;

type
  TCustomPropertyEditor = class;
  TPropertyEditLink = class;

  TLinkButton = class(TSpeedButton)
  private
    FLink: TPropertyEditLink;
    procedure SetLink(Value: TPropertyEditLink);
  protected
    procedure UpdateGlyph; virtual;
  public
    property Link: TPropertyEditLink read FLink write SetLink;
    constructor Create(Link: TPropertyEditLink); reintroduce;
    procedure Click; override;
  end;
      {
  TRemoveButton = class(TSpeedButton)
  private
    FLink: TPropertyEditLink;
  public
    constructor Create(Link: TPropertyEditLink); reintroduce;
    procedure Click; override;
  end;      }

  TUpDownButton = class(TLinkButton)
   private
 //   FLink: TPropertyEditLink;
    FDown: TSpeedButton;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
   protected
    procedure SetParent(AParent: TWinControl); override;
    procedure UpdateGlyph; override;
   public
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    constructor Create(Link: TPropertyEditLink); reintroduce;

    destructor Destroy; override;
    procedure Click; override;
    procedure DownClick(Sender: TObject);
  end;         

  TLinkEdit = class(TTntCustomEdit)
  private
    FLink: TPropertyEditLink;
    FMainButton: TLinkButton;
{    FRemoveButton: TRemoveButton;
    FUpDownButton: TUpDownButton; }
    FShowMainBtn: Boolean;
{    FShowRemoveBtn: Boolean;
    FShowUpDownBtn: Boolean;}
    FEditProperty: Boolean;
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN;    
    procedure WMLButtonDblClk(var Message: TWMLButtonDblClk); message WM_LBUTTONDBLCLK;
    procedure CMCancelMode(var Message: TCMCancelMode); message CM_CancelMode;
    procedure WMKillFocus(var Message: TMessage); message WM_KILLFOCUS;
    procedure CMRelease(var Message: TMessage); message CM_RELEASE;
    procedure WMChar(var Message: TWMChar); message WM_CHAR;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure SetLink(Value: TPropertyEditLink);
  protected
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure KeyPress(var Key: Char); override;
    procedure Click; override;
    procedure WndProc(var Message: TMessage); override;
  public
    procedure AcceptValue;
    property Link: TPropertyEditLink read FLink write SetLink;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;  
    constructor Create(Link: TPropertyEditLink); reintroduce;
    destructor Destroy; override;
    procedure Release; virtual;
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property HideSelection;
    property MaxLength;
    property OEMConvert;
    property PasswordChar;
  end;

  TPopupListBox = class(TTntCustomListbox)
  private
    FLink: TPropertyEditLink;
  protected
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DoExit; override;
  public
    constructor Create(Link: TPropertyEditLink); reintroduce;
  end;

  TEditType = (etString, etStringReadOnly,
               etEllipsis, etEllipsisReadOnly,
{               etPlus, etPlusReadOnly,}
               etUpDown, 
               etPickString);

  TPropertyEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FEditType: TEditType;
    FEdit: TLinkEdit;
    FPickList: TPopupListBox;
    FList: TTntStringList;
    FTree: TCustomPropertyEditor;
    FNode: PVirtualNode;
    FStopping: Boolean;
    procedure CloseUp(Accept: Boolean);
    procedure DropDown;
  public
    constructor Create(EditType: TEditType; Tree: TCustomPropertyEditor);
    destructor Destroy; override;

    function BeginEdit: Boolean; virtual; stdcall;
    function CancelEdit: Boolean; virtual; stdcall;
//    property Edit: TLinkEdit read FEdit write SetEdit;
    function EndEdit: Boolean; virtual; stdcall;
    function GetBounds: TRect; virtual; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; virtual; stdcall;
    procedure ProcessMessage(var Message: TMessage); virtual; stdcall;
    procedure SetBounds(R: TRect); virtual; stdcall;
//    property CurNode: PVirtualNode read FNode;
  end;


  TGetStringEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Text: WideString) of object;
  TSetStringEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            const Text: WideString) of object;
  TGetEditTypeEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var EditType: TEditType) of object;
  TGetPickListEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var List: TTntStringList; var FreeAfterUse: Boolean) of object;
  TGetColorEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Color: TColor) of object;
  TGetStyleEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Style: TFontStyles) of object;
  TEllipsisClickEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Accepted: Boolean) of Object;
  TIsAllowedEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Yes: Boolean) of Object;
  TNodeEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode) of Object;
  TNodeResultEvent =
  procedure(Sender: TCustomPropertyEditor; Node: PVirtualNode;
            var Result: Boolean) of Object;

  TCustomPropertyEditor = Class(TCustomVirtualStringTree)
  private
    FHitInfo: THitInfo;
    FAcceptError: Boolean;
    FMouseDown: Boolean;
    FDoNotCancel: Boolean;
    FTracking: Boolean;
    FTrackPos: Integer;
    FPropertyEditLink: TPropertyEditLink;
    FValueTextColor: TColor;
    FValueTextStyle: TFontStyles;
    FLinkEdit: TLinkEdit;
    FOnGetPropertyName: TGetStringEvent;
    FOnGetValue: TGetStringEvent;
    FOnGetPickList: TGetPickListEvent;
    FOnSetValue: TSetStringEvent;
    FOnSetPropertyName: TSetStringEvent;
    FOnGetEditType: TGetEditTypeEvent;
    FOnEllipsisClick: TEllipsisClickEvent;
//    FOnPlusClick: TEllipsisClickEvent;
    FOnGetPropertyColor: TGetColorEvent;
    FOnGetPropertyStyle: TGetStyleEvent;
    FOnGetValueColor: TGetColorEvent;
    FOnGetValueStyle: TGetStyleEvent;
    FOnIsPropNameEditable: TIsAllowedEvent;
//    FOnIsPropertyChildrenMovable: TIsAllowedEvent;
    FOnIsRemoveAllowed: TIsAllowedEvent;
//    FOnIsUpDownAllowed: TIsAllowedEvent;
    FOnPropertyRemove: TIsAllowedEvent;
    FOnPropertyUp: TIsAllowedEvent;
    FOnPropertyDown: TIsAllowedEvent;
    FOnUpButton: TNodeResultEvent;
    FOnDownButton: TNodeResultEvent;
    FEditFocusing: Boolean;
    procedure CMVisibleChanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure WMStartEditing(var Message: TMessage); message WM_STARTEDITING;
    function GetOptions: TStringTreeOptions;
    procedure SetOptions(const Value: TStringTreeOptions);
    procedure ForceEdit(Node: PVirtualNode);
    function GetPropWidth: Integer;
    function GetValueWidth: Integer;
    procedure SetPropWidth(Value: Integer);
    procedure SetValueWidth(Value: Integer);
    property TreeOptions: TStringTreeOptions read GetOptions write SetOptions;
    property Header;
    procedure SetValTextColor(Col: TColor);
    procedure SetValStyle(Style: TFontStyles);
    procedure DownButton(Node: PVirtualNode);
    procedure UpButton(Node: PVirtualNode);
  protected
//    procedure DoPaintNode(var PaintInfo: TVTPaintInfo); override;
    procedure PerformForceEdit;
    procedure DoEnter; override;
    procedure DoExit; override;
    function DoUpButton(Node: PVirtualNode): Boolean; virtual;
    function DoDownButton(Node: PVirtualNode): Boolean; virtual;
    procedure DoFocusChange(Node: PVirtualNode; Column: TColumnIndex); override;
    function GetOptionsClass: TTreeOptionsClass; override;
    procedure Click; override;
    procedure DoCanEdit(Node: PVirtualNode; Column: TColumnIndex;
      var Allowed: Boolean); override;
    function DoCreateEditor(Node: PVirtualNode;
      Column: TColumnIndex): IVTEditLink; override;
    procedure DoGetText(Node: PVirtualNode; Column: TColumnIndex;
      TextType: TVSTTextType; var Text: WideString); override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;

    procedure HandleMouseDblClick(var Message: TWMMouse; const HitInfo: THitInfo); override;
    procedure HandleMouseDown(var Message: TWMMouse;
      const HitInfo: THitInfo); override;
    function DoEndEdit: Boolean; override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X,
     Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure Resize; override;
    procedure DoGetPropertyName(Node: PVirtualNode; var Text: WideString);
      virtual;
    procedure DoGetValue(Node: PVirtualNode; var Text: WideString);
      virtual;
    procedure DoGetPickList(Node: PVirtualNode; var List: TTntStringList;
              var FreeAfterUse: Boolean); virtual;
    procedure DoSetValue(Node: PVirtualNode; const Text: WideString); virtual;
    function DoGetEditType(Node: PVirtualNode): TEditType; virtual;
    procedure DoScroll(DeltaX, DeltaY: Integer); override;
    function EllipsisClick(Node: PVirtualNode): Boolean; virtual;
//    function PlusClick(Node: PVirtualNode): Boolean; virtual;
    procedure DoPaintText(Node: PVirtualNode; const Canvas: TCanvas;
      Column: TColumnIndex; TextType: TVSTTextType); override;
    function GetPropertyColor(Node: PVirtualNode): TColor; virtual;
    function GetPropertyStyle(Node: PVirtualNode): TFontStyles; virtual;
    function GetValueStyle(Node: PVirtualNode): TFontStyles; virtual;
    function GetValueColor(Node: PVirtualNode): TColor; virtual;
    function DoCancelEdit: Boolean; override;
//    function IsPropertyChildrenMovable(Node: PVirtualNode): Boolean; virtual;
    function IsPropNameEditable(Node: PVirtualNode): Boolean; virtual;
    function IsRemoveAllowed(Node: PVirtualNode): Boolean; virtual;
  //  function IsUpDownAllowed(Node: PVirtualNode): Boolean; virtual;
  public
    property AcceptError: Boolean read FAcceptError write FAcceptError;
    property Canvas;
    property Colors;
    property DoNotCancel: Boolean read FDoNotCancel write FDoNotCancel;
    property ValueTextStyle: TFontStyles read FValueTextStyle write SetValStyle;
    property ValueTextColor: TColor read FValueTextColor write SetValTextColor;
    property PropertyWidth: Integer read GetPropWidth write SetPropWidth;
    property ValueWidth: Integer read GetValueWidth write SetValueWidth;
    property OnGetPropertyName: TGetStringEvent read FOnGetPropertyName
                                               write FOnGetPropertyName;
    property OnGetValue: TGetStringEvent read FOnGetValue
                                        write FOnGetValue;
    property OnGetPickList: TGetPickListEvent read FOnGetPickList
                                           write FOnGetPickList;
    property OnSetValue: TSetStringEvent read FOnSetValue
                                         write FOnSetValue;
    property OnSetPropertyName: TSetStringEvent read FOnSetPropertyName
                                         write FOnSetPropertyName;
    property OnGetEditType: TGetEditTypeEvent read FOnGetEditType
                                             write FOnGetEditType;
    property OnEllipsisClick: TEllipsisClickEvent read FOnEllipsisClick
                                                 write FOnEllipsisClick;
{    property OnPlusClick: TEllipsisClickEvent read FOnPlusClick
                                                 write FOnPlusClick;}
    property OnGetPropertyColor: TGetColorEvent read FOnGetPropertyColor
                                               write FOnGetPropertyColor;
    property OnGetPropertyStyle: TGetStyleEvent read FOnGetPropertyStyle
                                               write FOnGetPropertyStyle;
    property OnGetValueColor: TGetColorEvent read FOnGetValueColor
                                            write FOnGetValueColor;
    property OnGetValueStyle: TGetStyleEvent read FOnGetValueStyle
                                            write FOnGetValueStyle;
    property OnIsPropNameEditable: TIsAllowedEvent read
               FOnIsPropNameEditable write FOnIsPropNameEditable;
{    property OnIsPropertyChildrenMovable: TIsAllowedEvent read
               FOnIsPropertyChildrenMovable write FOnIsPropertyChildrenMovable;}
    property OnIsRemoveAllowed: TIsAllowedEvent read
               FOnIsRemoveAllowed write FOnIsRemoveAllowed;{
    property OnIsUpDownAllowed: TIsAllowedEvent read
               FOnIsUpDownAllowed write FOnIsUpDownAllowed;}
    property OnPropertyRemove: TIsAllowedEvent read FOnPropertyRemove
                               write FOnPropertyRemove;
    property OnPropertyUp: TIsAllowedEvent read FOnPropertyUp
                                          write FOnPropertyUp;
    property OnPropertyDown: TIsAllowedEvent read FOnPropertyDown
                                            write FOnPropertyDown;
    property OnUpButton: TNodeResultEvent read FOnUpButton write FOnUpButton;
    property OnDownButton: TNodeResultEvent read FOnDownButton write FOnDownButton;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetRoute(Node: PVirtualNode; var Route: TIntegerList);
    procedure AcceptEdit;
    procedure SetFocus; override;
    procedure RefreshValue(Node: PVirtualNode);
    procedure PropertyUp(Node: PVirtualNode); virtual;
    procedure PropertyDown(Node: PVirtualNode); virtual;
    procedure PropertyRemove(Node: PVirtualNode); virtual;
  end;

  TVirtualPropertyEditor = Class(TCustomPropertyEditor)
  published
    property ButtonFillMode;
    property ButtonStyle;
    property Align;
    property Anchors;
    property BiDiMode;
    property BevelEdges;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderStyle;
    property BorderWidth;
    property ChangeDelay;
    property Color default clBtnFace;
    property Constraints;
    property Ctl3D;
    property DefaultNodeHeight;
    property DefaultText;
    property DragMode;
    property Enabled;
    property Font;
    property HintAnimation;
    property HintMode;
    property LineMode;
    property LineStyle default lsSolid;
    property NodeDataSize;
    property ParentBiDiMode;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property RootNodeCount;
    property ScrollBarOptions;
    property ShowHint;
    property TabOrder;
    property TabStop default True;
    property Visible;
    property WantTabs;
    property PropertyWidth default 98;
    property ValueWidth default 98;
    property ValueTextColor default clNavy;
    property ValueTextStyle default [];

    property OnChange;
    property OnClick;
    property OnColumnResize;
    property OnContextPopup;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnFreeNode;
    property OnPaintText;
    property OnGetHelpContext;
    property OnGetHint;
    property OnGetLineStyle;
    property OnGetNodeDataSize;
    property OnGetPopupMenu;
    property OnInitNode;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnLoadNode;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnResetNode;
    property OnResize;
    property OnSaveNode;
    property OnScroll;
    property OnShortenString;
    property OnShowScrollbar;
    property OnStateChange;
    property OnStructureChange;
    property OnUpdating;
    //New properties
    property OnGetPropertyName;
    property OnGetValue;
    property OnGetPickList;
    property OnSetValue;
    property OnSetPropertyName;
    property OnGetEditType;
    property OnEllipsisClick;
//    property OnPlusClick;
    property OnGetPropertyColor;
    property OnGetPropertyStyle;
    property OnGetValueColor;
    property OnGetValueStyle;
    property OnIsPropNameEditable;
//    property OnIsPropertyChildrenMovable;
    property OnIsRemoveAllowed;{
    property OnIsUpDownAllowed;}
    property OnPropertyRemove;
    property OnPropertyUp;
    property OnPropertyDown;
    property OnUpButton;
    property OnDownButton;
  end;

  PPropertyData = ^TPropertyData;
  TPropertyData = packed record
   Owner: TPropertyList;
   Item: TPropertyListItem;
   UserData: Pointer;
  end;

  TNewPropertyEvent =
  procedure(Sender: TCustomPropertyEditor; Parent: PVirtualNode;
            AProperty: TPropertyListItem) of Object;

  TGetNewPropertyClassEvent =
  procedure(Sender: TCustomPropertyEditor; Parent: PVirtualNode;
            var AClass: TPropertyListItemClass) of Object;

  TPropertyEditor = class(TCustomPropertyEditor)
   private
    FList: TPropertyList;
    FSelectedItem: TPropertyListItem;
    FOnNewProperty: TNewPropertyEvent;
    FOnGetNewPropertyClass: TGetNewPropertyClassEvent;
    FOnAfterSetValue: TNodeEvent;
    FBoldChangedValues: Boolean;
    FListExternal: Boolean;
    FUserSelect: Boolean;
    procedure SetList(Value: TPropertyList);
    procedure SetListExternal(Value: Boolean);
    function GetRNC: Integer;
    procedure SetRNC(Value: Integer);
   protected
    function DoUpButton(Node: PVirtualNode): Boolean; override;
    function DoDownButton(Node: PVirtualNode): Boolean; override;
    procedure DoFreeNode(Node: PVirtualNode); override;
    function GetNewPropertyClass(Parent: PVirtualNode): TPropertyListItemClass; virtual;
    procedure DoNodeMoved(Node: PVirtualNode); override;
    procedure DoAddNewProperty(Parent: PVirtualNode; List: TPropertyList); virtual;
    procedure DoStructureChange(Node: PVirtualNode; Reason: TChangeReason); override;
    procedure DoInitNode(Parent, Node: PVirtualNode;
                         var InitStates: TVirtualNodeInitStates); override;
//    procedure InternalDisconnectNode(Node: PVirtualNode; KeepFocus: Boolean; Reindex: Boolean = True); override;
    function DoGetEditType(Node: PVirtualNode): TEditType; override;
    procedure DoSetValue(Node: PVirtualNode; const Text: WideString); override;
    procedure DoGetValue(Node: PVirtualNode; var Text: WideString); override;
    procedure DoGetPickList(Node: PVirtualNode; var List: TTntStringList;
              var FreeAfterUse: Boolean); override;
    procedure DoGetPropertyName(Node: PVirtualNode; var Text: WideString);
              override;
    function GetValueStyle(Node: PVirtualNode): TFontStyles; override;
//    function IsPropertyChildrenMovable(Node: PVirtualNode): Boolean; override;
    function IsPropNameEditable(Node: PVirtualNode): Boolean; override;
   public
    LastRoute: TIntegerList;
    property SelectedItem: TPropertyListItem read FSelectedItem;   
    property UserSelect: Boolean read FUserSelect write FUserSelect;
    property RootNodeCount: Integer read GetRNC write SetRNC;
    property ListExternal: Boolean read FListExternal write SetListExternal;
    property PropertyList: TPropertyList read FList write SetList;

    procedure ClearList;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
   published

    property BoldChangedValues: Boolean read FBoldChangedValues
                                       write FBoldChangedValues default True;
    property OnNewProperty: TNewPropertyEvent read FOnNewProperty
                                             write FOnNewProperty;
    property OnGetNewPropertyClass: TGetNewPropertyClassEvent
                                    read FOnGetNewPropertyClass
                                   write FOnGetNewPropertyClass;
    property OnAfterSetValue: TNodeEvent read FOnAfterSetValue
                                        write FOnAfterSetValue;
    //
    property ButtonFillMode;    
    property ButtonStyle;    
    property Align;
    property Anchors;
    property BiDiMode;
    property BevelEdges;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderStyle;
    property BorderWidth;
    property ChangeDelay;
    property Color default clBtnFace;
    property Constraints;
    property Ctl3D;
    property DefaultNodeHeight;
    property DefaultText;
    property DragMode;
    property Enabled;
    property Font;
    property HintAnimation;
    property HintMode;
    property LineMode;
    property LineStyle default lsSolid;
    property ParentBiDiMode;
    property ParentColor default False;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ScrollBarOptions;
    property ShowHint;
    property TabOrder;
    property TabStop default True;
    property Visible;
    property WantTabs;
    property PropertyWidth default 98;
    property ValueWidth default 98;
    property ValueTextColor default clNavy;
    property ValueTextStyle default [];

    property OnChange;
    property OnClick;
    property OnColumnResize;
    property OnContextPopup;
    property OnDblClick;
    property OnEnter;
    property OnExit;
    property OnFreeNode;
    property OnPaintText;
    property OnGetHelpContext;
    property OnGetHint;
    property OnGetLineStyle;
    property OnGetPopupMenu;
    property OnInitNode;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnLoadNode;
    property OnMeasureItem;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnResetNode;
    property OnResize;
    property OnSaveNode;
    property OnScroll;
    property OnShortenString;
    property OnShowScrollbar;
    property OnStateChange;
    property OnStructureChange;
    property OnUpdating;
    //New properties
    property OnGetPropertyName;
    property OnGetValue;
    property OnGetPickList;
    property OnSetValue;
    property OnSetPropertyName;
    property OnGetEditType;
    property OnEllipsisClick;
//    property OnPlusClick;
    property OnGetPropertyColor;
    property OnGetPropertyStyle;
    property OnGetValueColor;
    property OnGetValueStyle;
    property OnIsPropNameEditable;
//    property OnIsPropertyChildrenMovable;
    property OnIsRemoveAllowed;{
    property OnIsUpDownAllowed;}
    property OnPropertyRemove;
    property OnPropertyUp;
    property OnPropertyDown;
    property OnUpButton;
    property OnDownButton;        
  end;

const
 MainButtonSet: set of TEditType = [etEllipsis, etEllipsisReadOnly,
//                                    etPlus, etPlusReadOnly,
                                    etUpDown,
                                    etPickString];

var
 SInvalidPropertyValue: WideString = 'Invalid property value';
 SInvalidIntegerValue: WideString = '''%s'' is not a valid integer value';
 SValueOutOfRange: WideString = 'Value must be in range from %d to %d';
 SInvalidFloatingPointValue: WideString = '''%s'' is not a valid floating point value';
 SLengthExceed: WideString = 'Maximum string length is %d';
 SFloatOutOfRange: WideString = 'Value must be in range from %g to %g';
 SHexaOutOfRange: WideString = 'Value must be in range from 0x%2.2x to 0x%2.2x';

implementation

uses MyClasses;

{ TLinkEdit }

procedure TLinkEdit.AcceptValue;
var
 SaveTree: TCustomPropertyEditor;
begin
 FLink.CloseUp(False);
 SaveTree := FLink.FTree;
 if not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
 begin
  if Modified then
  begin
   Modified := False;
   SaveTree.DoSetValue(FLink.FNode, Text);
  end;
  SelectAll;
 end;
end;

procedure TLinkEdit.Click;
begin
 if Assigned(FLink) then with FLink do
 if (FEditType = etPickString) and Assigned(FPickList) then CloseUp(False);
end;

procedure TLinkEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  with Message do
  if (Sender <> Self) and (Sender <> FMainButton) and (FLink <> nil) and
     (Sender <> FLink.FPickList) then
   FLink.CloseUp(False);
end;

procedure TLinkEdit.CMRelease(var Message: TMessage);
begin
 Free;
end;

procedure TLinkEdit.CMVisibleChanged(var Message: TMessage);
var
 OldRect: TRect;
begin
 inherited;
 if Message.WParam <> 0 then
 begin
  if FShowMainBtn then
  begin
   OldRect := FMainButton.BoundsRect;
   OldRect.Bottom := BoundsRect.Bottom;
   if FLink.FEditType = etUpDown then
   begin
    if FMainButton is TLinkButton then
    begin
     FMainButton.Free;
     FMainButton := TUpDownButton.Create(FLink);
     RecreateWnd;     
     FMainButton.BoundsRect := OldRect;
    end;
   end else
   begin
    if FMainButton is TUpDownButton then
    begin
     FMainButton.Free;
     FMainButton := TLinkButton.Create(Link);
     RecreateWnd;
     FMainButton.BoundsRect := OldRect;
    end;
   end;

   FMainButton.Parent := FLink.FTree;
   FMainButton.UpdateGlyph;
   FMainButton.Show;
  end;
 { if FShowRemoveBtn then
  begin
   FRemoveButton.Parent := FLink.FTree;
   FRemoveButton.Show;
  end;
  if FShowUpDownBtn then
  begin
   FUpDownButton.Parent := FLink.FTree;
   FUpDownButton.Show;
  end;    }
 end else
 begin
  FMainButton.Visible := False;
  //FreeAndNil(FMainButton);
//  FMainButton.Visible := False;
 { FRemoveButton.Visible := False;
  FUpDownButton.Visible := False;}
 end;
end;

procedure TLinkEdit.CNKeyDown(var Message: TWMKeyDown);
begin
 if FLink.FTree.FTracking or
    Assigned(FLink.FPickList) or
    (Message.CharCode = VK_DELETE) then Exit;
 if (KeyDataToShiftState(Message.KeyData) = [ssCtrl]) then
 begin
  case Message.CharCode of
   Word('X'),
   Word('C'),
   Word('V'),
   Word('Z'):
    Message.CharCode := 0;
   Word('A'):
    SelectAll;
  end;
 end;
 inherited;
end;

constructor TLinkEdit.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 if Link.FEditType = etUpDown then
  FMainButton := TUpDownButton.Create(Link) else
  FMainButton := TLinkButton.Create(Link);
// FRemoveButton := TRemoveButton.Create(Link);
// FUpDownButton := TUpDownButton.Create(Link);
 Visible := False;
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
 BorderStyle := bsSingle;
 AutoSize := False;
end;

procedure TLinkEdit.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := ExStyle and not WS_EX_CLIENTEDGE;
 end;
end;

destructor TLinkEdit.Destroy;
begin
 FMainButton.Free;
{ FRemoveButton.Free;
 FUpDownButton.Free;}
 inherited;
end;

procedure TLinkEdit.KeyPress(var Key: Char);
var
 L, LL: Integer;
 S, WS: WideString;

 function Find: Boolean;
 var
  I: Integer;
  SU, WU: WideString;
 begin
  if FLink.FList.CaseSensitive then
   SU := S else
   SU := WideUpperCase(S);
  L := Length(SU);
  Result := False;
  if L < 1 then Exit;
  with FLink, FList do for I := 0 to Count - 1 do
  begin
   if FList.CaseSensitive then
    WU := FList[I] else
    WU := WideUpperCase(FList[I]);
   LL := Length(WU);
   if (LL > 0) and (LL >= L) and
       CompareMem(Addr(SU[1]), Addr(WU[1]), L shl 1) then
   begin
    Result := True;
    WS := FList[I];
    Exit;
   end;
  end;
 end;

begin
 if (Key = #13) or FLink.FTree.FTracking then Key := #0 else
 begin
  if (FLink.FEditType = etPickString) and (Ord(Key) <> VK_BACK) then
  begin
   L := SelLength;
   S := Text;
   if SelStart = Length(S) - L then
   begin
    SetLength(S, SelStart);
    S := S + Key;
    if Find then
    begin
     Delete(WS, 1, L);
     Text := S + WS;
   //  SelStart := L + LL;

     SendMessage(Handle, EM_SETSEL, L + LL, L);
    // SendMessage(Handle, EM_SCROLLCARET, 0,0);
//     SelLength := LL;
     Key := #0;
     Modified := True;
    end;
   end;
  end;
 end;
end;

procedure TLinkEdit.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
 FLink.FTree.Cursor := crDefault;
 inherited;
end;

procedure TLinkEdit.Release;
begin
 if HandleAllocated then
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

procedure TLinkEdit.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
 if not FEditProperty and FShowMainBtn then
 begin
  Dec(AWidth, 17);
  if FMainButton <> nil then
   FMainButton.SetBounds(ALeft + AWidth, ATop, 17, AHeight);
 end;
 inherited;
end;

procedure TLinkEdit.SetLink(Value: TPropertyEditLink);
begin
 FLink := Value;
 if FMainButton <> nil then
  FMainButton.Link := Value;
{ FRemoveButton.FLink := Value;
 FUpDownButton.FLink := Value;   }
end;

procedure TLinkEdit.WMChar(var Message: TWMChar);
begin
 if not (Message.CharCode in [VK_ESCAPE, VK_TAB]) then inherited;
end;

procedure TLinkEdit.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
 inherited;
 Message.Result := Message.Result or DLGC_WANTALLKEYS or
                   DLGC_WANTTAB or DLGC_WANTARROWS;
end;

procedure TLinkEdit.WMKeyDown(var Message: TWMKeyDown);
var
 Key: Word;
 I: Integer;
 SaveTree: TCustomPropertyEditor;
 SaveList: TTntStringList;
label
 KeyAccept;
begin
 SaveTree := FLink.FTree;
 if SaveTree.FTracking or
    Assigned(FLink.FPickList) then Exit;
 Key := Message.CharCode;
 case Key of
  VK_ESCAPE:
  begin
   with FLink, FTree do RefreshValue(FNode);
   Exit; //Goto KeyAccept;
  end;
  {VK_DELETE:
  begin
   if not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
   begin
    ClearSelection;
    Exit;
   end;
  end;  }
  VK_RETURN:
  begin
   AcceptValue;
 {  if not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
   begin
    if Modified then
    begin
     Modified := False;
     SaveTree.DoSetValue(FLink.FNode, Text);
    end;
    SelectAll;
   end;}
   goto KeyAccept;
  end;
  VK_UP, VK_DOWN, VK_PRIOR, VK_NEXT, VK_TAB:
  begin
   SaveTree.FAcceptError := False;
   if Key <> VK_TAB then
   begin // Because it can damage the FLink, it was saved before
    if ssCtrl in GlobalShiftState then
    begin
     if (Key in [VK_UP, VK_DOWN]) then
     case FLink.FEditType of
      etUpDown:
      with SaveTree do     
      begin
       case Key of
        VK_UP: UpButton(FocusedNode);
        else   DownButton(FocusedNode);
       end;
       Exit;
      end;
      etPickString:
      begin
       SaveList := FLink.FList;
       I := SaveList.IndexOf(Text);
       case Key of
        VK_UP: Dec(I);
        else   Inc(I);
       end;
       if (I >= 0) and (I < SaveList.Count) then
       begin
        Text := SaveList[I];
        Modified := False;
        SaveTree.DoSetValue(FLink.FNode, Text);
       end;
       SelectAll;      
       Exit;
      end;
     end;
    end else
    with SaveTree do
    if (((Key = VK_UP) or (Key = VK_PRIOR)) and (GetFirst <> FocusedNode)) or
       (((Key = VK_DOWN) or (Key = VK_NEXT)) and (GetLast <> FocusedNode)) then
    begin
     AcceptValue;
     if FAcceptError then Exit;
     Application.ProcessMessages;
     Windows.SetFocus(Handle);
    end;
   end else
   begin
    if not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
    begin
     if Modified then
     begin
      Modified := False;
      SaveTree.DoSetValue(FLink.FNode, Text);
     end;
     SelectAll;
    end;
   end;
   KeyAccept:
   with SaveTree do
    if (Key = VK_TAB) or not FAcceptError then
    begin
     if GlobalShiftState - MouseStates = [] then
      PostMessage(Handle, WM_KEYDOWN, Key, 0);
    end;
  end;
  else inherited;
 end;
end;

procedure TLinkEdit.WMKillFocus(var Message: TMessage);
begin
 inherited;
 AcceptValue;
end;

procedure TLinkEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
var
 I: Integer;
 SaveTree: TCustomPropertyEditor;
begin
 with FLink do if Assigned(FList) and (FEditType = etPickString) then
 begin
  SaveTree := FTree;
  I := FList.IndexOf(Text);
  if I < 0 then
  begin
   Modified := False;
   SaveTree.DoSetValue(FNode, Text);
   SelectAll;
  end else
  begin
   Inc(I);
   if I >= FList.Count then I := 0;
   Text := FList[I];
   Modified := False;
   SaveTree.DoSetValue(FNode, Text);
   SelectAll;
  end;
 end else
 if FEditType in [etEllipsis, etEllipsisReadOnly{, etPlusReadOnly}] then
  FMainButton.Click else
  inherited;
end;

procedure TLinkEdit.WndProc(var Message: TMessage);
begin
 case Message.Msg of
  wm_KeyDown, wm_SysKeyDown, wm_Char:
  if Assigned(FLink) then
  with FLink, TWMKey(Message) do
  begin
   if (CharCode <> 0) and Assigned(FPickList) then
   begin
    with TMessage(Message) do
     SendMessage(FPickList.Handle, Msg, WParam, LParam);
    Exit;
   end;
  end;
 end;
 inherited;
end;

{ TLinkButton }

procedure TLinkButton.Click;
var
 S: WideString;
 SaveNode: PVirtualNode;
 SaveTree: TCustomPropertyEditor;
begin
 if Assigned(FLink) then with FLink do
 begin
  SaveTree := FTree;
  if FEditType = etPickString then
  begin
   if not Assigned(FPickList) then
    DropDown else
    CloseUp(False);
  end else
  begin
   if FEdit.Modified then
   begin
    FEdit.Modified := False;
    SaveTree.DoSetValue(FNode, FEdit.Text);
   end;
   FEdit.SelectAll;
   SaveNode := FNode;
   case FEditType of
    etEllipsis, etEllipsisReadOnly:
    if SaveTree.EllipsisClick(SaveNode) and (SaveTree.FocusedNode = SaveNode) then
    begin
     Invalidate;
     SaveTree.DoGetValue(SaveNode, S);
     FEdit.Text := S;
     FEdit.SelectAll;
    end;
  {  etPlus, etPlusReadOnly:
    if SaveTree.PlusClick(SaveNode) and (SaveTree.FocusedNode = SaveNode) then
    begin
     Invalidate;
     SaveTree.DoGetValue(SaveNode, S);
     FEdit.Text := S;
     FEdit.SelectAll;
    end;   }
   end;
  end;
 end;
 inherited;
end;

constructor TLinkButton.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 Visible := False;
 FLink := Link;
end;

procedure TLinkButton.SetLink(Value: TPropertyEditLink);
begin
 FLink := Value;
 UpdateGlyph;
end;

procedure TLinkButton.UpdateGlyph;
var
 Bitmap: TBitmap;
 X, Y: Integer;
begin
 if FLink <> nil then
 begin
  Bitmap := TBitmap.Create;
  try
   Bitmap.Width := 11;
   Bitmap.Height := 11;
   with Bitmap.Canvas do
   begin
    Brush.Color := $FF00FF;
    Brush.Style := bsSolid;
    FillRect(ClipRect);
    case FLink.FEditType of
     etPickString:
     begin
      for Y := 0 to 3 do for X := Y + 2 to 8 - Y do
       Pixels[X, Y + 4] := clBlack;
     end;
     etEllipsis, etEllipsisReadOnly:
     for Y := 4 to 5 do
     begin
      for X := 2 to 3 do Pixels[X, Y] := clBlack;
      for X := 5 to 6 do Pixels[X, Y] := clBlack;
      for X := 8 to 9 do Pixels[X, Y] := clBlack;
     end;
   {  etPlus, etPlusReadOnly:
     begin
      Pen.Style := psSolid;
      Pen.Mode := pmBlack;
      Pen.Width := 2;
      Pen.Color := clBlack;
      MoveTo(5, 0);
      LineTo(5, 9);
      MoveTo(-1, 5);
      LineTo(9, 5);
     end;   }
    end;
   end;
   Bitmap.Transparent := True;
   Bitmap.TransparentColor := $FF00FF;
   Glyph := Bitmap;
  finally
   Bitmap.Free;
  end;
 end;
end;
    (*
{ TRemoveButton }

procedure TRemoveButton.Click;
begin
 if FLink <> nil then FLink.FTree.DoPropertyRemove;
 inherited;
end;

constructor TRemoveButton.Create(Link: TPropertyEditLink);
var
 Bitmap: TBitmap;
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 Visible := False;
 FLink := Link;
 Bitmap := TBitmap.Create;
 try
  Bitmap.Width := 11;
  Bitmap.Height := 11;
  with Bitmap.Canvas do
  begin
   Brush.Color := $FF00FF;
   Brush.Style := bsSolid;
   FillRect(ClipRect);
   Pen.Color := clBlack;
   Pen.Mode := pmBlack;
   Pen.Style := psSolid;
   Pen.Width := 2;
   MoveTo(0, 0);
   LineTo(9, 9);
   MoveTo(9, 0);
   LineTo(0, 9);
  end;
  Bitmap.Transparent := True;
  Bitmap.TransparentColor := $FF00FF;
  Glyph := Bitmap;
 finally
  Bitmap.Free;
 end;
end;       *)

{ TUpDownButton }

procedure TUpDownButton.Click;
begin
 if Assigned(FLink) then
  with FLink do
   if FEditType = etUpDown then
    FTree.UpButton(FNode);
end;

procedure TUpDownButton.CMVisibleChanged(var Message: TMessage);
begin
 if FDown <> nil then
  FDown.Visible := Message.WParam <> 0;
 inherited;
end;

constructor TUpDownButton.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 FDown := TSpeedButton.Create(nil);
 FDown.ShowHint := False;
 FDown.ParentShowHint := False;
 FDown.Visible := False;
 Visible := False;
 FDown.OnClick := DownClick;

end;

destructor TUpDownButton.Destroy;
begin
 FreeAndNIL(FDown);
 inherited;
end;

procedure TUpDownButton.DownClick(Sender: TObject);
begin
 if Assigned(FLink) then
  with FLink do
   if (FEditType = etUpDown) then
    FTree.DownButton(FNode);
end;

procedure TUpDownButton.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
 inherited SetBounds(ALeft, ATop, AWidth, AHeight div 2);
 if FDown <> nil then
  FDown.SetBounds(ALeft, (ATop + AHeight) - AHeight div 2, AWidth, AHeight div 2);
end;

procedure TUpDownButton.SetParent(AParent: TWinControl);
begin
 inherited;
 if FDown <> nil then
  FDown.Parent := AParent;
end;

procedure TUpDownButton.UpdateGlyph;
var
 Bitmap: TBitmap;
 X: Integer;
 Y: Integer;
begin
 Bitmap := TBitmap.Create;
 try
  Bitmap.Width := 11;
  Bitmap.Height := 4;
  with Bitmap.Canvas do
  begin
   Brush.Color := $FF00FF;
   Brush.Style := bsSolid;
   FillRect(ClipRect);
   for Y := 0 to 3 do for X := Y + 2 to 8 - Y do
    Pixels[X, 3 - Y] := clBlack;
  end;
  Bitmap.Transparent := True;
  Bitmap.TransparentColor := $FF00FF;
  Glyph := Bitmap;
  with Bitmap.Canvas do
  begin
   Brush.Color := $FF00FF;
   Brush.Style := bsSolid;
   FillRect(ClipRect);
   for Y := 0 to 3 do for X := Y + 2 to 8 - Y do
    Pixels[X, Y] := clBlack;
  end;
  FDown.Glyph := Bitmap;
 finally
  Bitmap.Free;
 end;
end;

{ TPopupListBox }

constructor TPopupListBox.Create(Link: TPropertyEditLink);
begin
 inherited Create(NIL);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
end;

procedure TPopupListBox.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
  AddBiDiModeExStyle(ExStyle);
  WindowClass.Style := CS_SAVEBITS;
 end;
end;

procedure TPopupListBox.CreateWnd;
begin
 inherited CreateWnd;
 Windows.SetParent(Handle, 0);
 CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TPopupListBox.DoExit;
begin
 FLink.CloseUp(False);
end;

procedure TPopupListBox.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
 inherited;
 FLink.CloseUp(True);
end;

procedure TPopupListBox.WMKeyDown(var Message: TWMKeyDown);
var Key: Word;
begin
 Key := Message.CharCode;
 case Key of
  VK_ESCAPE, VK_RETURN, VK_TAB: FLink.CloseUp(Key = VK_RETURN);
  else inherited;
 end;
end;

{ TPropertyEditLink }

function TPropertyEditLink.BeginEdit: Boolean;
begin
 Result := not FStopping;
 if Result then
 begin
  FEdit.Show;
  FEdit.SelectAll;
  if FTree.Focused then
  begin
   FTree.FEditFocusing := True;
   FEdit.SetFocus;
   FTree.FEditFocusing := False;
  end;
 end;
end;

function TPropertyEditLink.CancelEdit: Boolean;
var
 OldFocus: Boolean;
begin
 Result := not FStopping;
 if Result then
 begin
  FStopping := True;
  FTree.FEditFocusing := True;
  OldFocus := FEdit.Focused;
  FEdit.Hide;
  FTree.CancelEditNode;
  FTree.FEditFocusing := False;
  if OldFocus then
   FTree.SetFocus;
  FEdit.Link := nil;
 end;
end;

procedure TPropertyEditLink.CloseUp(Accept: Boolean);
var
 S: WideString;
 SaveTree: TCustomPropertyEditor;
begin
 if Assigned(FPickList) then
 begin
  SaveTree := FTree;
  with FPickList do
  begin
   if Accept and (ItemIndex >= 0) then
   begin
    S := Items.Strings[ItemIndex];
    FEdit.Text := S;
    FEdit.Modified := False;
    SaveTree.DoSetValue(FNode, S);
   end;
   FEdit.SelectAll;
   FreeAndNil(FPickList);
   if FEdit.Visible then
   begin
    SaveTree.FEditFocusing := True;
    FEdit.SetFocus;
    SaveTree.FEditFocusing := False;
   end;
  end;
 end;
end;

constructor TPropertyEditLink.Create(EditType: TEditType;
  Tree: TCustomPropertyEditor);
begin
 FTree := Tree;
 FEditType := EditType;
 if FTree.FLinkEdit = nil then
 begin
  FEdit := TLinkEdit.Create(Self);
  FTree.FLinkEdit := FEdit;
 end else
 begin
  FEdit := FTree.FLinkEdit;
  FEdit.Link := Self;
 end;
 if EditType = etPickString then
  FList := TTntStringList.Create;
end;

destructor TPropertyEditLink.Destroy;
var
 SaveTree: TCustomPropertyEditor;
begin
 SaveTree := FTree;
 SaveTree.FPropertyEditLink := nil;
 if Assigned(FList) then FList.Free;
 if FEdit.Modified then
 begin
  FEdit.Modified := False;
  SaveTree.DoSetValue(FNode, FEdit.Text);
 end;
 SaveTree.FEditFocusing := True;
 FEdit.Hide;
 SaveTree.FEditFocusing := False; 
 inherited;
end;

procedure TPropertyEditLink.DropDown;
var R: TRect; P: TPoint; I, J: Integer; SZ: TSize; S: WideString;
begin
 if not Assigned(FPickList) then
 begin
  FPickList := TPopupListBox.Create(Self);
  with FPickList do
  begin
   FEdit.SelStart := FEdit.SelLength;
   Visible := False;
   Parent := FLink.FTree;
   if Assigned(FList) then Items.Assign(FList);
   with FEdit do if FShowMainBtn then
   with FMainButton.BoundsRect do
   begin
    R.Right := Right;
    R.Top := Bottom;
   end;
   with FEdit.BoundsRect do
   begin
    P := FTree.ClientToScreen(Point(Left, R.Top));
    R.Left := Left;
    R.Bottom := R.Top + 16;
   end;
   Inc(R.Right);
   Inc(R.Top);
   ClientWidth := R.Right - R.Left;
   if Count > 0 then
   begin
    IntegralHeight := True;
    ItemIndex := Items.IndexOf(FEdit.Text);
   end;
   if Count > 0 then
    ClientHeight := ItemHeight * Count else
    ClientHeight := ItemHeight;
   J := ClientWidth;
   for I := 0 to Count - 1 do
   begin
    S := Items[I];
    SZ.cx := 0;
    SZ.cy := 0;
    GetTextExtentPoint32W(Canvas.Handle, PWideChar(S), Length(S), SZ);
    if SZ.cx + 4 > J then J := SZ.cx + 4;
   end;
   Dec(P.X, J - ClientWidth);
   ClientWidth := J;
   with P do
   begin
    if Y + Height > Screen.Height then Dec(Y, Height);
    SetWindowPos(Handle, HWND_TOP, X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
   end;
   Invalidate;
   FTree.FEditFocusing := True;
   Windows.SetFocus(FLink.FEdit.Handle);
   FTree.FEditFocusing := False;
   Windows.SetFocus(Handle);
  end;
 end;
end;

function TPropertyEditLink.EndEdit: Boolean;
var
 SaveTree: TCustomPropertyEditor;
begin
 SaveTree := FTree;
 Result := not FStopping;
 if Result then
 try
  FStopping := True;
  if FEdit.Modified then
  begin
   FEdit.Modified := False;
   SaveTree.DoSetValue(FNode, FEdit.Text);
  end;
  SaveTree.FEditFocusing := True;
  FEdit.Hide;
  SaveTree.FEditFocusing := False;  
  FEdit.Link := nil;
 except
  FStopping := False;
  raise;
 end;
end;

function TPropertyEditLink.GetBounds: TRect;
var
 R: Integer;
begin
 Result := FEdit.BoundsRect;
 with FEdit do
 begin
  if FEditProperty then
  begin
   FTree.Header.Columns.GetColumnBounds(0, Result.Left, R);
{   if FShowRemoveBtn then Dec(Result.Left, FRemoveButton.Width);
   if FShowUpDownBtn then Dec(Result.Left, FUpDownButton.Width); }
  end else
  if FShowMainBtn then Result.Right := FMainButton.BoundsRect.Right;
 end;
end;

function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex): Boolean;
var
 Txt: WideString;
 List: TTntStringList;
 FreeAfterUse: Boolean;
begin
 Result := Tree is TCustomPropertyEditor;
 if Result then
 begin
  FTree := TCustomPropertyEditor(Tree);
  FNode := Node;
  if (Column = 0) and FTree.IsPropNameEditable(Node) then
  begin
   FEdit.FShowMainBtn := False;
   FEdit.FEditProperty := True;
   FTree.DoGetPropertyName(Node, Txt);
   FEdit.ReadOnly := False;
   FEdit.Color := clWindow;
   FEdit.Font.Style := FTree.GetPropertyStyle(Node);
  end else
  begin
   FEdit.FShowMainBtn := FEditType in MainButtonSet;
   FEdit.FEditProperty := False;
   FTree.DoGetValue(Node, Txt);
   if FEditType in [etStringReadOnly, etEllipsisReadOnly{, etPlusReadOnly}] then
   begin
    FEdit.ReadOnly := True;
    FEdit.Color := clBtnFace;
   end else
   begin
    FEdit.ReadOnly := False;
    FEdit.Color := clWindow;
   end;
   if FEditType = etPickString then
   begin
    FTree.DoGetPickList(Node, List, FreeAfterUse);
    if Assigned(List) then
    begin
     FList.Assign(List);
     FList.CaseSensitive := List.CaseSensitive;
     if FreeAfterUse then List.Free;
    end;
   end;
   FEdit.Font.Style := FTree.GetValueStyle(Node);
  end;
{  FEdit.FShowRemoveBtn := FTree.IsRemoveAllowed(Node);
  FEdit.FShowUpDownBtn := FTree.IsUpDownAllowed(Node);}
  FEdit.Font.Color := clWindowText;
  FEdit.Parent := Tree;
  FEdit.RecreateWnd;
  FEdit.HandleNeeded;
  FEdit.Text := Txt;
  FEdit.Modified := False;

  {if FTree.Focused then
   FEdit.SetFocus;    }
 end;
end;

procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
 FEdit.WindowProc(Message);
end;

procedure TPropertyEditLink.SetBounds(R: TRect);
var
 X: Integer;
 Run: PVirtualNode;

begin
 if Assigned(FPickList) then CloseUp(False);
 if FEdit.FEditProperty then
 begin
  if toShowRoot in FTree.TreeOptions.PaintOptions then
    X := 1
  else
    X := 0;
  Run := FTree.FocusedNode;
  // Determine indentation level of top node.
  while Run.Parent <> FTree.RootNode do
  begin
    Inc(X);
    Run := Run.Parent;
  end;

  FTree.Header.Columns.GetColumnBounds(0, R.Left, R.Right);
  Inc(R.Left, X * Integer(FTree.Indent));
 end else
  FTree.Header.Columns.GetColumnBounds(1, R.Left, R.Right);
 FEdit.BoundsRect := R;
end;

{ TCustomPropertyEditor }

procedure TCustomPropertyEditor.AcceptEdit;
begin
 if IsEditing then with FPropertyEditLink, FEdit do
 if not (FEditType in [etStringReadOnly, etEllipsisReadOnly{, etPlusReadOnly}]) then
 begin
  Modified := False;
  DoSetValue(FNode, Text);
  SelectAll;
 end;
end;

procedure TCustomPropertyEditor.Click;
begin
 if not FTracking then
  inherited;
end;

procedure TCustomPropertyEditor.CMEnabledChanged(var Message: TMessage);
begin
 inherited;
 if Enabled then ForceEdit(FocusedNode);
end;

procedure TCustomPropertyEditor.CMVisibleChanged(var Message: TMessage);
begin
 inherited;
 if Message.WParam <> 0 then ForceEdit(FocusedNode);
end;

constructor TCustomPropertyEditor.Create(AOwner: TComponent);
var
 M: TVTMiscOptions;
 P: TVTPaintOptions;
 S: TVTSelectionOptions;
// A: TVTAutoOptions;
begin
 inherited;
 ClipboardFormats.Clear;
 Colors.BorderColor := clWindowText;
 Colors.GridLineColor := clBtnShadow;
 Colors.TreeLineColor := clBtnShadow;
 with Header do
 begin
  AutoSizeIndex := -1;
  with Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 0;
   Margin := 0;
   PropertyWidth := 98;
  end;
  with Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 1;
   Margin := 0;
  end;
  MainColumn := 0;
  Options := [hoAutoResize];
 end;
 FocusedColumn := 0;
 ScrollBarOptions.ScrollBars := ssVertical;
 Indent := 12;
 DragOperations := [];
 LineStyle := lsSolid;
 TextMargin := 2;
 Margin := 0;
 FValueTextColor := clNavy;
 Color := clBtnFace;
 with TreeOptions do
 begin
//  A := AutoOptions;
//  Exclude(A, toAutoScrollOnExpand);
 // AutoOptions := A;
  M := MiscOptions;
  Include(M, toEditable);
  Include(M, toGridExtensions);
  P := PaintOptions;
  Exclude(P, toThemeAware);
  Include(P, toHideFocusRect);
  Include(P, toShowHorzGridLines);
  Include(P, toShowVertGridLines);
  Exclude(P, toShowTreeLines);
  Include(P, toAlwaysHideSelection);
  S := SelectionOptions;
//  Include(S, toFullRowSelect);
  Include(S, toExtendedFocus);
  MiscOptions := M;
  PaintOptions := P;
  SelectionOptions := S;
 end;
 ButtonStyle := bsRectangle;
end;

destructor TCustomPropertyEditor.Destroy;
begin
if FLinkEdit <> nil then
  FLinkEdit.Release;
 inherited;
end;

function TCustomPropertyEditor.DoCancelEdit: Boolean;
begin
 if not FDoNotCancel then Result := inherited DoCancelEdit else
 begin
  Result := False;
  FDoNotCancel := False;
 end;
end;

procedure TCustomPropertyEditor.DoCanEdit(Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
 inherited;
 Allowed := (Column <> 0) or IsPropNameEditable(Node);
end;

function TCustomPropertyEditor.DoCreateEditor(Node: PVirtualNode;
  Column: TColumnIndex): IVTEditLink;
begin
 FPropertyEditLink := TPropertyEditLink.Create(DoGetEditType(Node), Self);
 Result := FPropertyEditLink;
end;

function TCustomPropertyEditor.DoDownButton(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnDownButton) then
  FOnDownButton(Self, Node, Result);
end;

function TCustomPropertyEditor.DoEndEdit: Boolean;
begin
 if not FMouseDown or (FHitInfo.HitNode <> nil) then
  Result := inherited DoEndEdit else
  Result := False;
end;

procedure TCustomPropertyEditor.DoEnter;
begin
 if not FEditFocusing then
 begin
  if FocusedNode = nil then
   FocusedNode := RootNode.FirstChild;
  inherited;
 end;
end;

procedure TCustomPropertyEditor.DoExit;
begin
 if not FEditFocusing then
  inherited;
end;

procedure TCustomPropertyEditor.DoFocusChange(Node: PVirtualNode;
  Column: TColumnIndex);
begin
 ForceEdit(Node);
 inherited;
end;

function TCustomPropertyEditor.DoGetEditType(Node: PVirtualNode): TEditType;
begin
 Result := etStringReadOnly;
 if Assigned(FOnGetEditType) then
  FOnGetEditType(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DoGetPickList(Node: PVirtualNode;
  var List: TTntStringList; var FreeAfterUse: Boolean);
begin
 List := NIL;
 FreeAfterUse := False;
 if Assigned(FOnGetPickList) then
  FOnGetPickList(Self, Node, List, FreeAfterUse);
end;

procedure TCustomPropertyEditor.DoGetPropertyName(Node: PVirtualNode;
  var Text: WideString);
begin
 if Assigned(FOnGetPropertyName) then
  FOnGetPropertyName(Self, Node, Text) else
  Text := DefaultText;
end;

procedure TCustomPropertyEditor.DoGetText(Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var Text: WideString);
begin
 if Node <> NIL then
 case Column of
  0: DoGetPropertyName(Node, Text);
  1: DoGetValue(Node, Text);
 end;
end;

procedure TCustomPropertyEditor.DoGetValue(Node: PVirtualNode;
  var Text: WideString);
begin
 if Assigned(FOnGetValue) then
  FOnGetValue(Self, Node, Text) else
  Text := DefaultText;
end;
            {
procedure TCustomPropertyEditor.DoPaintNode(var PaintInfo: TVTPaintInfo);
var
 TestColumn: Integer;
begin
 if not (tsEditing in TreeStates) and
    (PaintInfo.Node = FocusedNode) then
 begin
  if IsPropNameEditable(PaintInfo.Node) then
   TestColumn := FocusedColumn else
   TestColumn := 1;

  if PaintInfo.Column = TestColumn then
   with PaintInfo.Canvas do
   begin
    Brush.Color := clWindow;
    FillRect(PaintInfo.ContentRect);
   end;
 end;
 inherited;
end;          }

procedure TCustomPropertyEditor.DoPaintText(Node: PVirtualNode;
  const Canvas: TCanvas; Column: TColumnIndex; TextType: TVSTTextType);
begin
 with Canvas.Font do if Column = 0 then
 begin
  Color := GetPropertyColor(Node);
  Style := GetPropertyStyle(Node);
 end else
 begin
  Color := GetValueColor(Node);
  Style := GetValueStyle(Node);
 end;
 inherited;
end;

procedure TCustomPropertyEditor.DoScroll(DeltaX, DeltaY: Integer);
begin
 if tsEditing in treeStates then with FPropertyEditLink do
 begin
  if Assigned(FPickList) then CloseUp(False);
  with FEdit do
  begin
   if FShowMainBtn then
   begin
    FMainButton.Top := Top;
    FMainButton.Height := Height;
   end;
  { if FShowRemoveBtn then
   begin
    FRemoveButton.Top := Top;
    FRemoveButton.Height := Height;
   end;
   if FShowUpDownBtn then
   begin
    FUpDownButton.Top := Top;
    FUpDownButton.Height := Height;
   end;  }
  end;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.DoSetValue(Node: PVirtualNode;
  const Text: WideString);
begin
 FAcceptError := False;
 if FLinkEdit.FEditProperty then
 begin
  if Assigned(FOnSetPropertyName) then
   FOnSetPropertyName(Self, Node, Text);
  FLinkEdit.Font.Style := GetPropertyStyle(Node);
 end else
 begin
  if Assigned(FOnSetValue) then
   FOnSetValue(Self, Node, Text);
  FLinkEdit.Font.Style := GetValueStyle(Node);
 end;
end;

function TCustomPropertyEditor.DoUpButton(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnUpButton) then
  FOnUpButton(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DownButton(Node: PVirtualNode);
begin
 if DoDownButton(Node) then
 begin
  RefreshValue(Node);
  if FPropertyEditLink <> nil then
  begin
   FEditFocusing := True;
   Windows.SetFocus(FPropertyEditLink.FEdit.Handle);
   FEditFocusing := False;
  end;
 end;
end;

function TCustomPropertyEditor.EllipsisClick(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnEllipsisClick) then
  FOnEllipsisClick(Self, Node, Result);
end;

procedure TCustomPropertyEditor.ForceEdit(Node: PVirtualNode);
begin
 if (Node <> nil) and not IsEditing and not (csDesigning in ComponentState) then
  PostMessage(Self.Handle, WM_STARTEDITING, Integer(Node), 0);
end;

function TCustomPropertyEditor.GetOptions: TStringTreeOptions;
var
 O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 Result := O as TStringTreeOptions;
end;

function TCustomPropertyEditor.GetOptionsClass: TTreeOptionsClass;
begin
 Result := TStringTreeOptions;
end;

function TCustomPropertyEditor.GetPropertyColor(Node: PVirtualNode): TColor;
begin
 Result := Font.Color;
 if Assigned(FOnGetPropertyColor) then
  FOnGetPropertyColor(Self, Node, Result);
end;

function TCustomPropertyEditor.GetPropertyStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := Font.Style;
 if Assigned(FOnGetPropertyStyle) then
  FOnGetPropertyStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetPropWidth: Integer;
begin
 Result := Header.Columns.Items[0].Width;
end;

procedure TCustomPropertyEditor.GetRoute(Node: PVirtualNode;
 var Route: TIntegerList);
var
 I, J: Integer;
begin
  if Node <> nil then
  begin
   I := 0;
   while Node <> RootNode do
   begin
    SetLength(Route, I + 1);
    Route[I] := Node.Index;
    Inc(I);
    Node := Node.Parent;
   end;
   J := Length(Route);
   asm
    push esi
    push edi
    mov ecx,[J]
    mov esi,[Route]
    mov esi,[esi]    
    lea edi,[esi+ecx*4-4]
    shr ecx,1
    jz @exit
    std
   @exchg:
    mov eax,[edi]
    xchg eax,[esi]
    stosd
    add esi,4
    loop @exchg
    cld
   @exit:    
    pop edi
    pop esi
   end;
  end;
end;

function TCustomPropertyEditor.GetValueColor(Node: PVirtualNode): TColor;
begin
 Result := FValueTextColor;
 if Assigned(FOnGetValueColor) then
  FOnGetValueColor(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := FValueTextStyle;
 if Assigned(FOnGetValueStyle) then
  FOnGetValueStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueWidth: Integer;
begin
 Result := Header.Columns.Items[1].Width;
end;

procedure TCustomPropertyEditor.HandleMouseDblClick(var Message: TWMMouse;
  const HitInfo: THitInfo);
begin
 if FTracking then Exit;
 FHitInfo := HitInfo;
 FMouseDown := True;
 FocusedNode := HitInfo.HitNode;
 inherited;
 FMouseDown := False;
 ForceEdit(HitInfo.HitNode);
end;

procedure TCustomPropertyEditor.HandleMouseDown(var Message: TWMMouse;
  const HitInfo: THitInfo);
begin
 if FTracking then Exit;
 FHitInfo := HitInfo;
 FMouseDown := True;
 FocusedNode := HitInfo.HitNode; 
 inherited;
 FMouseDown := False;
 ForceEdit(HitInfo.HitNode);
end;

{function TCustomPropertyEditor.IsPropertyChildrenMovable(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnIsPropertyChildrenMovable) then
  FOnIsPropertyChildrenMovable(Self, Node, Result);
end;
 }
function TCustomPropertyEditor.IsPropNameEditable(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnIsPropNameEditable) then
  FOnIsPropNameEditable(Self, Node, Result);
end;

function TCustomPropertyEditor.IsRemoveAllowed(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnIsRemoveAllowed) then
  FOnIsRemoveAllowed(Self, Node, Result);
// Result := Result {or IsPropertyChildrenMovable(Node.Parent)};
end;
{
function TCustomPropertyEditor.IsUpDownAllowed(Node: PVirtualNode): Boolean;
begin
 Result := False;
 if Assigned(FOnIsUpDownAllowed) then
  FOnIsUpDownAllowed(Self, Node, Result);
 Result := Result or IsPropertyChildrenMovable(Node.Parent);
end;               }

procedure TCustomPropertyEditor.KeyDown(var Key: Word; Shift: TShiftState);
begin
 if not FTracking then
  inherited;
end;

procedure TCustomPropertyEditor.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 if (Cursor = crHSplit) and (Button = mbLeft) then
 begin
  DoStateChange([tsSizing]);
  FTracking := True;
  FTrackPos := Header.Columns.Items[0].Width - X;
 end else
 begin
  if Assigned(FPropertyEditLink) then
   with FPropertyEditLink do if Assigned(FPickList) then CloseUp(False);
 end;
end;

procedure TCustomPropertyEditor.MouseMove(Shift: TShiftState; X,
  Y: Integer);
var
 W: Integer;
begin
 if FTracking then
 begin
  W := X - FTrackPos;
  Header.Columns.Items[0].Width := Max(30, Min(W, ClientWidth - 30));
 end else
 begin
  W := Header.Columns.Items[0].Width;
  if (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit else
   Cursor := crDefault;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 W: Integer;
begin
 if FTracking then
 begin
  DoStateChange([], [tsSizing]);
  FTracking := False;
  W := Header.Columns.Items[0].Width;
  if (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit else
   Cursor := crDefault;
 end;
 inherited;
end;
            {
function TCustomPropertyEditor.PlusClick(Node: PVirtualNode): Boolean;
begin
 Result := True;
 if Assigned(FOnPlusClick) then
  FOnPlusClick(Self, Node, Result);
 if Result then
  FocusedNode := AddChild(Node); //InsertNode(Node, amAddChildFirst);
end;
              }

procedure TCustomPropertyEditor.PerformForceEdit;
begin
 if FocusedNode <> nil then
 begin
  if IsPropNameEditable(FocusedNode) then
   EditNode(FocusedNode, FocusedColumn) else
   EditNode(FocusedNode, 1);
 end;
end;

procedure TCustomPropertyEditor.PropertyDown(Node: PVirtualNode);
var
 Accepted, OldFocus: Boolean;
begin
 if Node = nil then Exit;
 Accepted := True;

 OldFocus := Focused or ((FPropertyEditLink <> nil) and
                         FPropertyEditLink.FEdit.Focused);
 CancelEditNode;

 if Assigned(FOnPropertyDown) then
  FOnPropertyDown(Self, Node, Accepted);

 if Accepted and (Node.NextSibling <> nil) then
 begin
  MoveTo(Node, Node.NextSibling, amInsertAfter, False);
  FocusedNode := Node;
 end;

 PerformForceEdit;
 if OldFocus then
  SetFocus;
end;

procedure TCustomPropertyEditor.PropertyRemove(Node: PVirtualNode);
var
 NextNode: PVirtualNode;
 Accepted, OldFocus: Boolean;
begin
 if Node = nil then Exit;
 Accepted := True;
 
 OldFocus := Focused or ((FPropertyEditLink <> nil) and
                         FPropertyEditLink.FEdit.Focused);
 CancelEditNode;

 if Assigned(FOnPropertyRemove) then
  FOnPropertyRemove(Self, Node, Accepted);

 if Accepted then
 begin
  NextNode := Node.NextSibling;
  if NextNode = nil then NextNode := Node.PrevSibling;
  if NextNode = nil then NextNode := Node.Parent;
  DeleteNode(Node);
  if NextNode <> RootNode then
   FocusedNode := NextNode;
 end;

 PerformForceEdit;
 if OldFocus then
  SetFocus;
end;

procedure TCustomPropertyEditor.PropertyUp(Node: PVirtualNode);
var
 Accepted, OldFocus: Boolean;
begin
 if Node = nil then Exit;
 Accepted := True;

 OldFocus := Focused or ((FPropertyEditLink <> nil) and
                          FPropertyEditLink.FEdit.Focused);
 CancelEditNode;

 if Assigned(FOnPropertyUp) then
  FOnPropertyUp(Self, Node, Accepted);

 if Accepted and (Node.PrevSibling <> nil) then
 begin
  MoveTo(Node, Node.PrevSibling, amInsertBefore, False);
  FocusedNode := Node;
 end;

 PerformForceEdit;
 if OldFocus then
  SetFocus;
end;

procedure TCustomPropertyEditor.RefreshValue(Node: PVirtualNode);
var
 S: WideString;
begin
 if FLinkEdit <> nil then
 begin
  DoGetValue(Node, S);
  FLinkEdit.Font.Style := GetValueStyle(Node);
  FLinkEdit.Text := S;
  FLinkEdit.SelectAll;
 end;
end;

procedure TCustomPropertyEditor.Resize;
begin
 PropertyWidth := Max(30, PropertyWidth);
 inherited;
end;

procedure TCustomPropertyEditor.SetFocus;
begin
 if IsEditing then
 begin
  with FPropertyEditLink do
  begin
   if Assigned(FPickList) then
    CloseUp(False) else
   if FEdit.Visible then
   begin
    FEditFocusing := True;
    FEdit.SetFocus;
    FEditFocusing := False;
   end else
    inherited;
  end;
 end else inherited;
end;

procedure TCustomPropertyEditor.SetOptions(const Value: TStringTreeOptions);
var
 O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 O.Assign(Value);
end;

procedure TCustomPropertyEditor.SetPropWidth(Value: Integer);
begin
 Header.Columns.Items[0].Width := Value;
end;

procedure TCustomPropertyEditor.SetValStyle(Style: TFontStyles);
begin
 FValueTextStyle := Style;
 Invalidate;
end;

procedure TCustomPropertyEditor.SetValTextColor(Col: TColor);
begin
 FValueTextColor := Col;
 Invalidate;
end;

procedure TCustomPropertyEditor.SetValueWidth(Value: Integer);
begin
 Header.Columns.Items[1].Width := Value;
end;

procedure TCustomPropertyEditor.UpButton(Node: PVirtualNode);
begin
 if DoUpButton(Node) then
 begin
  RefreshValue(Node);
  if FPropertyEditLink <> nil then
  begin
   FEditFocusing := True;
   Windows.SetFocus(FPropertyEditLink.FEdit.Handle);
   FEditFocusing := False;
  end;
 end;
end;

procedure TCustomPropertyEditor.WMStartEditing(var Message: TMessage);
var
 Node: PVirtualNode;
begin
 Node := Pointer(Message.WParam);
 if Node = nil then Exit;
 if IsPropNameEditable(Node) then
  EditNode(Node, FocusedColumn) else
  EditNode(Node, 1);
end;

{ TPropertyEditor }

procedure TPropertyEditor.ClearList;

 procedure ClearData(Parent: PVirtualNode);
 var
  Node: PVirtualNode;
  Data: PPropertyData;
 begin
  Node := Parent.FirstChild;
  while Node <> nil do
  begin
   Data := GetNodeData(Node);
   FillChar(Data^, SizeOf(TPropertyData), 0);
   ClearData(Node);
   Node := Node.NextSibling;
  end;
 end;

begin
 CancelEditNode;
 FList.Clear;
 ClearData(RootNode);
end;

constructor TPropertyEditor.Create(AOwner: TComponent);
begin
 inherited;
 FList := TPropertyList.Create; 
 NodeDataSize := SizeOf(TPropertyData);
end;

destructor TPropertyEditor.Destroy;
begin
 inherited;
 if not FListExternal then
  FList.Free;
end;

procedure TPropertyEditor.DoAddNewProperty(Parent: PVirtualNode; List: TPropertyList);
var
 Prop: TPropertyListItem;
begin
 if csDesigning in ComponentState then Exit;
 Prop := List.AddProperty(GetNewPropertyClass(Parent));
 if Assigned(FOnNewProperty) then
  FOnNewProperty(Self, Parent, Prop);
end;

function TPropertyEditor.DoDownButton(Node: PVirtualNode): Boolean;
var
 Data: PPropertyData;
begin
 Result := False;
 if Assigned(FOnDownButton) then
 begin
  FOnDownButton(Self, Node, Result);
  Exit;
 end;
 Data := GetNodeData(Node);
 if Data.Owner <> nil then
  with Data.Item do
  begin
   case ValueType of
    vtDecimal,
    vtHexadecimal:
    begin
     if Value > ValueMin then
     begin
      Value := Value - Step;
      Result := True;
     end;
    end;
    vtFloat:
    begin
     if Float > 5.0e-324 then
     begin
      Float := Float - FloatStep;
      Result := True;
     end;
    end;
   end;
  end;
end;

procedure TPropertyEditor.DoFreeNode(Node: PVirtualNode);
var
 Data: PPropertyData;
begin
 if not FListExternal or IsRemoveAllowed(Node) then
 begin
  Data := GetNodeData(Node);
  if (Data <> nil) and (Data.Owner <> nil) then
   Data.Owner.Remove(Data.Item);
 end;
 inherited;
end;

function TPropertyEditor.DoGetEditType(Node: PVirtualNode): TEditType;
var
 Data: PPropertyData;
begin
 Data := GetNodeData(Node);
 with Data^ do
 case Item.ValueType of
  vtDecimal, vtHexadecimal, vtFloat:
 { if Item.Parameters and PL_EDITABLE_LIST <> 0 then
  begin
   if Item.ReadOnly then
    Result := etPlusReadOnly else
    Result := etPlus;
  end else   }
  if Item.ReadOnly then
    Result := etStringReadOnly else
  if Item.ValueType = vtDecimal then
    Result := etUpDown else
    Result := etString;
  vtString: if (Item.Parameters and PL_BUFFER <> 0) or (Item is TFilePickItem) then
  begin
   if Item.ReadOnly then
    Result := etEllipsisReadOnly else
    Result := etEllipsis;
  end else
 { if Item.Parameters and PL_EDITABLE_LIST <> 0 then
  begin
   if Item.ReadOnly then
    Result := etPlusReadOnly else
    Result := etPlus;
  end else    }
  if Item.ReadOnly then
   Result := etStringReadOnly else
   Result := etString;
  vtPickString:
  begin
   if Item.ReadOnly then
    Result := etStringReadOnly else
    Result := etPickString;
  end;
  else Result := etStringReadOnly;
 end;
 if Assigned(FOnGetEditType) then
  FOnGetEditType(Self, Node, Result);
end;

procedure TPropertyEditor.DoGetPickList(Node: PVirtualNode;
  var List: TTntStringList; var FreeAfterUse: Boolean);
var Data: PPropertyData;
begin
 Data := GetNodeData(Node);
 if Data.Owner <> nil then
  List := Data.Item.PickList;
 FreeAfterUse := False; 
 if Assigned(FOnGetPickList) then
  FOnGetPickList(Self, Node, List, FreeAfterUse);
end;

procedure TPropertyEditor.DoGetPropertyName(Node: PVirtualNode;
  var Text: WideString);
var
 Data: PPropertyData;
begin
 Text := '';
 if Assigned(FOnGetPropertyName) then
  FOnGetPropertyName(Self, Node, Text);
 if Text = '' then
 begin
  Data := GetNodeData(Node);
  if Data.Owner <> nil then
   Text := Data.Item.Name;
 end;
end;

procedure TPropertyEditor.DoGetValue(Node: PVirtualNode;
  var Text: WideString);
var Data: PPropertyData;
begin
 Data := GetNodeData(Node);
 if Data.Owner <> nil then
 begin
  with Data.Item do
   if Parameters and PL_MULTIPLE_VALUE = 0 then
    Text := ValueStr else
    Text := '';
 end;
end;

procedure TPropertyEditor.DoInitNode(Parent, Node: PVirtualNode;
  var InitStates: TVirtualNodeInitStates);
var
 Data, ParentData: PPropertyData;
 I, Value: Integer;
begin
 inherited;
 Data := GetNodeData(Node);
 if Parent = nil then
 begin
  Data.Owner := FList;
  Data.Item := FList.Properties[Node.Index];
  if Node.Index = 0 then
   FSelectedItem := FList.FindProperty(LastRoute);
 end else
 begin
  ParentData := GetNodeData(Parent);
  if ParentData = nil then Exit;
  if ParentData.Item = nil then Exit;
  Data.Owner := ParentData.Item.SubProperties;
  Data.Item := Data.Owner.Properties[Node.Index];
 end;
 if (FSelectedItem <> nil) and
    (Data.Item = FSelectedItem) then
  FocusedNode := Node;
 if FocusedNode <> nil then
 begin
  if Node = FocusedNode then
   RefreshValue(Node);
 end;
 with Data.Item.SubProperties do
 begin
  Value := Node.ChildCount;
  if Count < Value then
  begin
   for I := Count to Value - 1 do
    DeleteNode(GetLastChild(Node));
  end else
  for I := Value to Count - 1 do
  begin
   FDoNotCancel := True;
   if IsVisible[AddChild(Node)] then;
   FDoNotCancel := False;
  end;
 end;
end;

procedure TPropertyEditor.DoNodeMoved(Node: PVirtualNode);
var
 Data: PPropertyData;
begin
 if TreeFromNode(Node) = Self then
 begin
  Data := GetNodeData(Node);
  if Data.Owner <> nil then
    Data.Owner.MoveTo(Data.Item.Index, Node.Index);
 end;
 inherited;
end;

procedure TPropertyEditor.DoSetValue(Node: PVirtualNode; const Text: WideString);
var
 Data: PPropertyData; I: Integer;
 Str: WideString;
begin
 FAcceptError := False;
 if FLinkEdit.FEditProperty then
 begin
  if Assigned(FOnSetPropertyName) then
   FOnSetPropertyName(Self, Node, Text);
  if not FAcceptError then
  begin
   Data := GetNodeData(Node);
   Data.Item.Name := Text;
   FLinkEdit.Font.Style := GetPropertyStyle(Node);
  end;
 end else
 begin
  if Assigned(FOnSetValue) then
   FOnSetValue(Self, Node, Text);
  if FAcceptError then Exit; 
  Data := GetNodeData(Node);
  with Data.Item do
  begin
   if ValueType = vtPickString then
   begin
    if Parameters and PL_FIXEDPICK <> 0 then
    begin
     I := PickList.IndexOf(Text);
     if I >= 0 then
      ValueStr := PickList.Strings[I] else
     if Parameters and PL_MULTIPLE_VALUE <> 0 then
     begin
      Parameters := Parameters xor PL_MULTIPLE_VALUE;
      if PickListIndex >= 0 then
      begin
       Str := PickList.Strings[PickListIndex];
       if ValueStr = Str then
        ValueChange else
        ValueStr := Str;
      end;
     end else
     begin
      WideMessageDlg(SInvalidPropertyValue, mtError, [mbOk], 0);
      FAcceptError := True;
     end;
    end else ValueStr := Text;
   end else
   begin
    ValueStr := Text;
    case ConversionResult of
     PI_E_STRTOINT:
     begin
      WideMessageDlg(WideFormat(SInvalidIntegerValue, [Text]),
      mtError, [mbOk], 0);
      FAcceptError := True;
     end;
     PI_E_OUTOFBOUNDS:
     begin
      case ValueType of
       vtString:
        WideMessageDlg(WideFormat(SLengthExceed, [MaxLength]),
                      mtError, [mbOk], 0);
       vtDecimal:
        WideMessageDlg(WideFormat(SValueOutOfRange, [ValueMin, ValueMax]),
                       mtError, [mbOk], 0);
       vtHexadecimal:
        WideMessageDlg(WideFormat(SHexaOutOfRange, [ValueMin, ValueMax]),
                       mtError, [mbOk], 0);
      end;
      FAcceptError := True;
     end;
     PI_E_STRTOFLOAT:
     begin
      WideMessageDlg(WideFormat(SInvalidFloatingPointValue, [Text]),
      mtError, [mbOk], 0);
      FAcceptError := True;
     end;
    end;
   end;
   if Assigned(FOnAfterSetValue) then
    FOnAfterSetValue(Self, Node);
   RefreshValue(Node);
  end;
  FLinkEdit.Font.Style := GetValueStyle(Node);
 end;
end;

procedure TPropertyEditor.DoStructureChange(Node: PVirtualNode;
 Reason: TChangeReason);
var
 Data: PPropertyData;
 I, Cnt: Integer;
begin
 case Reason of
  crChildAdded:
  begin
   if Node = nil then
   begin
    Cnt := inherited RootNodeCount;
    for I := FList.Count to Cnt - 1 do
     DoAddNewProperty(nil, FList);
   end else
   begin
    Data := GetNodeData(Node);
    if (Data <> nil) and (Data.Owner <> nil) then
    begin
     Cnt := Node.ChildCount;
     with Data.Item do
     for I := SubProperties.Count to Cnt - 1 do
      DoAddNewProperty(Node, SubProperties);
    end;
   end;
  end;
  crNodeAdded:
  begin
   if Node.Parent = nil then
   begin
    DoAddNewProperty(Node, FList);
    FList.MoveTo(FList.Count - 1, Node.Index);
   end else
   begin
    Data := GetNodeData(Node.Parent);
    if (Data <> nil) and (Data.Owner <> nil) then
    with Data.Item do
    begin
     DoAddNewProperty(Node, SubProperties);
     SubProperties.MoveTo(SubProperties.Count - 1, Node.Index);
    end;
   end;
  end;
 end;
 inherited;
end;

function TPropertyEditor.DoUpButton(Node: PVirtualNode): Boolean;
var
 Data: PPropertyData;
begin
 Result := False;
 if Assigned(FOnUpButton) then
 begin
  FOnUpButton(Self, Node, Result);
  Exit;
 end;

 Data := GetNodeData(Node);
 if Data.Owner <> nil then
  with Data.Item do
  begin
   case ValueType of
    vtDecimal,
    vtHexadecimal:
    begin
     if Value < ValueMax then
     begin
      Value := Value + Step;
      Result := True;
     end;
    end;
    vtFloat:
    begin
     if Float < 1.7e+308 then
     begin
      Float := Float + FloatStep;
      Result := True;
     end;
    end;
   end;
  end;
end;

function TPropertyEditor.GetNewPropertyClass(Parent: PVirtualNode): TPropertyListItemClass;
begin
 Result := nil;
 if Assigned(FOnGetNewPropertyClass) then
  FOnGetNewPropertyClass(Self, Parent, Result);
end;

function TPropertyEditor.GetRNC: Integer;
begin
 Result := inherited RootNodeCount;
end;

function TPropertyEditor.GetValueStyle(Node: PVirtualNode): TFontStyles;
var Data: PPropertyData;
begin
 if FBoldChangedValues then
 begin
  Data := GetNodeData(Node);
  with Data.Item do if Modified then
   Result := [fsBold] else
   Result := [];
 end else Result := inherited GetValueStyle(Node);
end;

{function TPropertyEditor.IsPropertyChildrenMovable(Node: PVirtualNode): Boolean;
var
 Data: PPropertyData;
begin
 Data := GetNodeData(Node);
 Result := inherited IsPropertyChildrenMovable(Node) or ((Data <> nil) and
           (Data.Item.Parameters and PL_PROP_CHILD_MOVABLE <> 0));
end;
 }
function TPropertyEditor.IsPropNameEditable(Node: PVirtualNode): Boolean;
var
 Data, ParentData: PPropertyData;
begin
 Data := GetNodeData(Node);
 ParentData := GetNodeData(Node.Parent);
 Result := inherited IsPropNameEditable(Node) or ((Data <> nil) and (ParentData <> nil) and
           ((ParentData.Item.Parameters and PL_PROP_CHILD_EDITABLE <> 0) or
            (Data.Item.Parameters and PL_PROP_EDITABLE <> 0)));
end;

procedure TPropertyEditor.SetList(Value: TPropertyList);
begin
 if FListExternal then
  FList := Value else
  FList.Assign(Value);
 if FList <> nil then
  RootNodeCount := FList.Count else
  RootNodeCount := 0;
end;

procedure TPropertyEditor.SetListExternal(Value: Boolean);
begin
 if FListExternal <> Value then
 begin
  RootNodeCount := 0;
  FListExternal := Value;
  if Value then
   FreeAndNil(FList) else
   FList := TPropertyList.Create;
 end;
end;

procedure TPropertyEditor.SetRNC(Value: Integer);
var
 OldFocus: Boolean;
begin
 OldFocus := Focused or ((FPropertyEditLink <> nil) and
                         FPropertyEditLink.FEdit.Focused);
 CancelEditNode;
 if not FUserSelect and (FocusedNode <> nil) then
   GetRoute(FocusedNode, LastRoute);
 inherited RootNodeCount := Value;
 ReinitChildren(nil, True);
 Invalidate;
 PerformForceEdit;
 if OldFocus then
  SetFocus;  
end;

end.

