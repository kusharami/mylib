unit TileMapView;

interface

uses
  Windows, Messages, SysUtils, MyClasses, Classes, Controls, Graphics,
  Forms, StdCtrls;

const
  WM_HSCROLLTOVIEW = WM_USER + 1111;
  WM_VSCROLLTOVIEW = WM_USER + 1112;
  WM_DRAWCELL = WM_USER + 2222;
  crMagicWand = 980;
  crMagicInc  = 981;
  crMagicDec  = 982;
  crSelPaste =  983;
  crBrush =     984;
  crDropper =   985;
  crEraser =    986;
  crFloodFill = 987;
  crPencil =    988;
  crBlack =     989;
  crSelMove =   991;
  crSelCopy =   992;
  crSelCut =    993;
  crSelDrag =   994;
  crSelNew =    995;
  crSelInc =    996;
  crSelDec =    997;
  crMoveHand =  998;
  crDragHand =  999;
  
type
  TSelectionMode = (selNone, selSingle, selMulti);
  TTMV_MouseMode = (mmNothing,
                    mmScroll,
                    mmSingleSelect,
                    mmMultiSelectInit,
                    mmMultiSelectNew,
                    mmMultiSelectInclude,
                    mmMultiSelectExclude,
                    mmMultiSelectMove,
                    mmMultiSelectCopy,
                    mmMultiSelectLayerCopy,
                    mmMultiSelectDrag,
                    mmMultiSelectLayerDrag,
                    mmCustom);


  TCustomTileMapView = class;
  TResizeEvent = procedure(Sender: TCustomTileMapView; var NewWidth, NewHeight: Integer) of object;
  TSetPositionEvent = procedure(Sender: TCustomTileMapView; var NewX, NewY: Integer) of object;  
  TActCheckEvent = procedure(Sender: TCustomTileMapView;
                             Shift: TShiftState; var Act: Boolean) of object;
  TSelectInternalMode = (simNone, simNew, simInclude, simExclude, simDrag, simCopy);
  TSelBtnChkEvent =  procedure(Sender: TCustomTileMapView;
                               Shift: TShiftState;
                               var SelMode: TSelectInternalMode) of object;
  TCellPosEvent = procedure(Sender: TCustomTileMapView; CellX, CellY: Integer) of object;
  TDrawCellEvent = procedure(Sender: TCustomTileMapView; const Rect: TRect;
                 CellX, CellY: Integer) of object;
  TTileMapViewEvent = procedure(Sender: TCustomTileMapView) of object;
  TTileMapActionEvent = procedure(Sender: TCustomTileMapView; var Done: Boolean) of object;
  TGetCellStateEvent = procedure(Sender: TCustomTileMapView; CellX, CellY: Integer; var Dirty: Boolean) of object;
  TGetBufCellStateEvent = procedure(Sender: TCustomTileMapView; Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean) of object;
  TCopyMapDataEvent = procedure(Sender: TCustomTileMapView; var Data: Pointer; const Rect: TRect; SelectionBuffer: Pointer) of object;
  TClearCellEvent = procedure(Sender: TCustomTileMapView; CellX, CellY: Integer) of object;
  TPasteBufferApplyEvent = procedure(Sender: TCustomTileMapView; CellX, CellY: Integer; Data: Pointer) of object;
  TBufClearCellEvent = TPasteBufferApplyEvent;
  TCheckCompatibilityEvent = procedure(Sender: TCustomTileMapView; Data: Pointer; var Result: Boolean) of object;
  TReleasePasteBufferEvent = procedure(Sender: TCustomTileMapView; Data: Pointer) of object;
  TMakePasteBufferCopyEvent = procedure(Sender: TCustomTileMapView; Source: Pointer; var Dest: Pointer) of object;
  TContentsChangedEvent = procedure(Sender: TCustomTileMapView) of object;
  TOnSysKeyEvent = procedure(Sender: TCustomTileMapView;
   Key: Word; State: TShiftState) of object;

  TChangeType = (ccClearSelection, ccPasteBufferApply);
  TContentChangeEvent = procedure(Sender: TCustomTileMapView;
                                  ChangeType: TChangeType) of object;

  TTMV_State = (tmsSizing, tmsScrollBarsChanging, tmsTimerScroll);

  TCustomTileMapView = class(TCustomControl)
   private
    FMapWidth: Integer;
    FMapHeight: Integer;
    FMapSize: Integer;    
    FTileWidth: Integer;
    FTileHeight: Integer;

    FViewWidth: Integer;
    FViewHeight: Integer;
    FViewRect: TRect;
    FViewOffset: TPoint;
    FRangeX: Integer;
    FRangeY: Integer;

    FSelectionEnabled: Boolean;
    FSelectionMode: TSelectionMode;
    FSelectStyle: TPenStyle;
    FSelectMode: TPenMode;
    FSelectWidth: Integer;
    FSelectColor: TColor;
    FSelectedCell: TPoint;
    FSelectionBuffer: array of Boolean;
    FSelectAnimShift: Integer;

    FSelectNewShiftState: TShiftState;
    FSelectIncludeShiftState: TShiftState;
    FSelectExcludeShiftState: TShiftState;
    FSelectCopyShiftState: TShiftState;
    FSelectDragShiftState: TShiftState;

    FDragScrollShiftState: TShiftState;


    FGridStyle: TPenStyle;
    FGridMode: TPenMode;
    FGridWidth: Integer;
    FGridColor: TColor;
    FMinGridH: Integer;
    FMinGridV: Integer;

    FOnMapResize: TResizeEvent;
    FOnScrollButtonCheck: TActCheckEvent;
    FOnSelectButtonCheck: TSelBtnChkEvent;
    FOnCustomMouseActionCheck: TActCheckEvent;
    FOnCustomMouseAction: TTileMapActionEvent;
    FOnCustomMouseActionFinish: TTileMapViewEvent;
    FOnDrawCell: TDrawCellEvent;
    FOnSelectionChanged: TTileMapViewEvent;
    FOnChangeTileSize: TResizeEvent;
    FOnBeforePaint: TTileMapViewEvent;
    FOnAfterPaint: TTileMapViewEvent;
    FOnGetCellState: TGetCellStateEvent;
    FOnGetBufCellState: TGetBufCellStateEvent;
    FOnCopyMapData: TCopyMapDataEvent;
    FOnPasteBufferApply: TPasteBufferApplyEvent;
    FOnClearCell: TClearCellEvent;
    FOnBufClearCell: TBufClearCellEvent;
    FOnReleasePasteBuffer: TReleasePasteBufferEvent;
    FOnCustomDrawAfterGrid: TDrawCellEvent;
    FOnCheckCompatibility: TCheckCompatibilityEvent;
    FOnSetPosition: TSetPositionEvent;
    FOnMakePasteBufferCopy: TMakePasteBufferCopyEvent;
    FOnContentsChanged: TNotifyEvent;


    FClientLeft: Integer;
    FClientTop: Integer;
    FTileX: Integer;
    FTileY: Integer;
    FCursorX: Integer;
    FCursorY: Integer;

    FDefaultCursor: TCursor;

    FScrollBarsAllwaysVisible: Boolean;

    FBorderStyle: TBorderStyle;

    FShowGrid: Boolean;

    FMouseMode: TTMV_MouseMode;

    FNewCursorPosition: Boolean;
    FHScrolled: Boolean;
    FVScrolled: Boolean;
    FCanPopup: Boolean;

//    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMDrag(var Message: TCMDrag); message CM_DRAG;
    procedure CMCursorChanged(var Message: TMessage); message CM_CURSORCHANGED;
    procedure CMMouseEnter(var Message: TMessage); message CM_MOUSEENTER;
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMRButtonDown(var Message: TWMRButtonDown); message WM_RBUTTONDOWN;
    procedure WMLButtonUp(var Message: TWMLButtonUp); message WM_LBUTTONUP;
    procedure WMRButtonUp(var Message: TWMRButtonUp); message WM_RBUTTONUP;
    procedure WMMButtonDown(var Message: TWMRButtonDown); message WM_MBUTTONDOWN;
    procedure WMMButtonUp(var Message: TWMRButtonUp); message WM_MBUTTONUP;
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure WMNCHitTest(var Message: TMessage); message WM_NCHITTEST;
    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
    procedure WMHScrollToView(var Message: TMessage); message WM_HSCROLLTOVIEW;
    procedure WMVScrollToView(var Message: TMessage); message WM_VSCROLLTOVIEW;
    procedure WMDrawCell(var Message: TMessage); message WM_DRAWCELL;
    procedure CMFocusChanged(var Message: TCMFocusChanged); message CM_FOCUSCHANGED;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure CMRecreateWnd(var Message: TMessage); message CM_RECREATEWND;
    procedure SetMapHeight(Value: Integer);
    procedure SetMapWidth(Value: Integer);
    procedure SetGridColor(Value: TColor);
    procedure SetGridMode(Value: TPenMode);
    procedure SetGridStyle(Value: TPenStyle);
    procedure SetGridWidth(Value: Integer);
    procedure SetSelectColor(Value: TColor);
    procedure SetSelectMode(Value: TPenMode);
    procedure SetSelectStyle(Value: TPenStyle);
    procedure SetShowGrid(Value: Boolean);
    procedure SetSelectionMode(Value: TSelectionMode);
    procedure SetSelectWidth(Value: Integer);
    function GetSelected(CellX, CellY: Integer): Boolean;
    procedure SetSelected(CellX, CellY: Integer; Value: Boolean);
    procedure UpdateVerticalScrollBar(DoRepaint: Boolean);
    procedure UpdateHorizontalScrollBar(DoRepaint: Boolean);
    function GetOffsetX: Integer;
    function GetOffsetY: Integer;
    procedure SetOffsetX_Internal(X: Integer);
    procedure SetOffsetY_Internal(Y: Integer);
    procedure SetOffsetX(Value: Integer);
    procedure SetOffsetY(Value: Integer);
    procedure SetOffsetX_NoRepaint(Value: Integer);
    procedure SetOffsetY_NoRepaint(Value: Integer);
    procedure SetTileHeight(Value: Integer);
    procedure SetTileWidth(Value: Integer);
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure SetSelectedCell(const Value: TPoint);
    procedure SetScrollBarsAllwaysVisible(Value: Boolean);
    procedure SetSelectionEnabled(Value: Boolean);
    procedure SetDefaultCursor(Value: TCursor);
    function GetViewRect: TRect;
    function GetSomethingSelected: Boolean;
    function GetSelectionRect: TRect;
    procedure CalculateTileXY;
    function GetMultiSelected: Boolean;
    function GetSelectedByIndex(Index: Integer): Boolean;
    procedure SetSelectedByIndex(Index: Integer; Value: Boolean);
    function GetSelectedCellIndex: Integer;
    procedure SetSelectedCellIndex(Value: Integer);
    function GetTempSelectionRect: TRect;
    function GetSelectionBuffer: PBoolean;
    procedure SetMinGridH(Value: Integer);
    procedure SetMinGridV(Value: Integer);
   protected
    FTempSel: array of Byte;
    FState: set of TTMV_State;
    FMouseRect: TRect;
    FInternalIndex: Integer;
    FSelectionRect: TRect;
    FOnSysKeyDown: TOnSysKeyEvent;
    FOnSysKeyUp: TOnSysKeyEvent;
    FOnMouseEnter: TNotifyEvent;
    FOnMouseLeave: TNotifyEvent;
    FOnBeforeContentChange: TContentChangeEvent;
    procedure RefreshPasteBufferSelection;
    procedure DoBeforeContentChange(ChangeType: TChangeType); virtual;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean); override;
    procedure DoStartDrag(var DragObject: TDragObject); override;
    procedure UpdateCursor(Shift: TShiftState);
    procedure DoFillPasteBuffer(SelectionBuffer: Pointer); virtual;
    procedure DoCopyMapData(var Data: Pointer; var Rect: TRect); virtual;
    procedure DoPasteBufferApply; virtual;
    procedure DoClearCell(X, Y: Integer); virtual;
    procedure DoBufClearCell(X, Y: Integer; Data: Pointer); virtual;
    procedure DoGetCellState(CellX, CellY: Integer; var Dirty: Boolean); virtual;
    procedure DoGetBufCellState(Data: Pointer; CellX, CellY: Integer; var Dirty: Boolean); virtual;
    procedure SysKeyDown(Key: Word; Shift: TShiftState); virtual;
    procedure SysKeyUp(Key: Word; Shift: TShiftState); virtual;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X,
     Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure Paint; override;
    procedure DoAfterPaint; virtual;
    procedure DoBeforePaint; virtual;
    procedure DoDrawCell(const Rect: TRect; CellX, CellY: Integer); virtual;
    procedure DoCustomDrawAfterGrid(const Rect: TRect; CellX, CellY: Integer); virtual;
    procedure DrawCellInternal(const Rect: TRect; CellX, CellY: Integer);
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure DoScrollButtonCheck(Shift: TShiftState; var Act: Boolean); virtual;
    procedure DoSelectButtonCheck(Shift: TShiftState;
               var SelMode: TSelectInternalMode); virtual;
    procedure DoCustomMouseActionCheck(Shift: TShiftState; var Act: Boolean); virtual;
    procedure DoCustomMouseAction(var Done: Boolean); virtual;
    procedure DoCustomMouseActionFinish; virtual;
    procedure WndProc(var Message: TMessage); override;
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure BufFillVisibility(Buffer: Pointer;
  BufWidth, BufHeight: Integer; Data: Pointer; const DataRect: TRect); virtual;
    procedure SelectedAreaClear(const Rect: TRect; SelectionBuffer: Pointer); virtual;
    procedure SelectedBufAreaClear(Data: Pointer;
                                  const DataRect, ClearRect: TRect;
                                  SelectionBuffer: Pointer = nil;
                                  SelBufferWidth: Integer = 0); virtual;
    procedure UpdateCoordinates;
   public
    PasteBuffer: Pointer;
    PasteBufRect: TRect;
    property SelectionBuffer: PBoolean read GetSelectionBuffer;
    property CanPopup: Boolean read FCanPopup write FCanPopup;
    property NewCursorPosition: Boolean read FNewCursorPosition
                                       write FNewCursorPosition;
    property ClientLeft: Integer read FClientLeft;
    property ClientTop: Integer read FClientTop;
    property SelectionRect: TRect read GetSelectionRect;
    property TempSelectionRect: TRect read GetTempSelectionRect;
    property InternalIndex: Integer read FInternalIndex;
    property OffsetX: Integer read GetOffsetX write SetOffsetX;
    property OffsetY: Integer read GetOffsetY write SetOffsetY;
    property MapWidth: Integer read FMapWidth write SetMapWidth;
    property MapHeight: Integer read FMapHeight write SetMapHeight;
    property TileWidth: Integer read FTileWidth write SetTileWidth;
    property TileHeight: Integer read FTileHeight write SetTileHeight;
    property ShowGrid: Boolean read FShowGrid write SetShowGrid;
    property SelectionMode: TSelectionMode read FSelectionMode
                                          write SetSelectionMode;
    property GridStyle: TPenStyle read FGridStyle write SetGridStyle;
    property GridMode: TPenMode read FGridMode write SetGridMode;
    property GridWidth: Integer read FGridWidth write SetGridWidth;
    property GridColor: TColor read FGridColor write SetGridColor;
    property GridMinH: Integer read FMinGridH write SetMinGridH;
    property GridMinV: Integer read FMinGridV write SetMinGridV;
    property SelectStyle: TPenStyle read FSelectStyle write SetSelectStyle;
    property SelectMode: TPenMode read FSelectMode write SetSelectMode;
    property SelectWidth: Integer read FSelectWidth write SetSelectWidth;
    property SelectColor: TColor read FSelectColor write SetSelectColor;
    property SelectedCell: TPoint read FSelectedCell write SetSelectedCell;
    property SelectedCellIndex: Integer read GetSelectedCellIndex write SetSelectedCellIndex;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle;
    property ScrollBarsAllwaysVisible: Boolean read FScrollBarsAllwaysVisible
                                              write SetScrollBarsAllwaysVisible;
    property TileX: Integer read FTileX;
    property TileY: Integer read FTileY;
    property MouseMode: TTMV_MouseMode read FMouseMode write FMouseMode;

    property SelectionEnabled: Boolean read FSelectionEnabled write SetSelectionEnabled;
    property DefaultCursor: TCursor read FDefaultCursor write SetDefaultCursor;

    property ViewRect: TRect read GetViewRect;
    property SomethingSelected: Boolean read GetSomethingSelected;
    property MultiSelected: Boolean read GetMultiSelected;

    property Canvas;

    property DragScrollShiftState: TShiftState read FDragScrollShiftState
                                              write FDragScrollShiftState;
    property SelectNewShiftState: TShiftState read FSelectNewShiftState
                                             write FSelectNewShiftState;
    property SelectIncludeShiftState: TShiftState read FSelectIncludeShiftState
                                              write FSelectIncludeShiftState;
    property SelectExcludeShiftState: TShiftState read FSelectExcludeShiftState
                                              write FSelectExcludeShiftState;
    property SelectCopyShiftState: TShiftState read FSelectCopyShiftState
                                              write FSelectCopyShiftState;
    property SelectDragShiftState: TShiftState read FSelectDragShiftState
                                              write FSelectDragShiftState;

    property Selected[CellX, CellY: Integer]: Boolean read GetSelected
                                                     write SetSelected;
    property SelectedByIndex[Index: Integer]: Boolean read GetSelectedByIndex
                                              write SetSelectedByIndex;

    property OnSelectionChanged: TTileMapViewEvent read FOnSelectionChanged
                                                  write FOnSelectionChanged;
    property OnDrawCell: TDrawCellEvent read FOnDrawCell write FOnDrawCell;
    property OnMapResize: TResizeEvent read FOnMapResize write FOnMapResize;
    property OnSetPosition: TSetPositionEvent read FOnSetPosition
                                             write FOnSetPosition;
    property OnScrollButtonCheck: TActCheckEvent read FOnScrollButtonCheck
                                                  write FOnScrollButtonCheck;
    property OnSelectButtonCheck:  TSelBtnChkEvent read FOnSelectButtonCheck
                                                  write FOnSelectButtonCheck;
    property OnCustomMouseActionCheck: TActCheckEvent read FOnCustomMouseActionCheck
                                                     write FOnCustomMouseActionCheck;
    property OnCustomMouseAction: TTileMapActionEvent read FOnCustomMouseAction
                                               write FOnCustomMouseAction;
    property OnCustomMouseActionFinish: TTileMapViewEvent read FOnCustomMouseActionFinish
                                                     write FOnCustomMouseActionFinish;
    property OnChangeTileSize: TResizeEvent read FOnChangeTileSize
                                           write FOnChangeTileSize;
    property OnBeforePaint: TTileMapViewEvent read FOnBeforePaint
                                             write FOnBeforePaint;
    property OnAfterPaint: TTileMapViewEvent read FOnAfterPaint
                                             write FOnAfterPaint;
    property OnGetCellState: TGetCellStateEvent read FOnGetCellState
                                               write FOnGetCellState;
    property OnGetBufCellState: TGetBufCellStateEvent read FOnGetBufCellState
                                                   write FOnGetBufCellState;
    property OnCopyMapData: TCopyMapDataEvent read FOnCopyMapData
                                             write FOnCopyMapData;
    property OnPasteBufferApply: TPasteBufferApplyEvent read FOnPasteBufferApply
                                                       write FOnPasteBufferApply;
    property OnClearCell: TClearCellEvent read FOnClearCell
                                         write FOnClearCell;
    property OnBufClearCell: TBufClearCellEvent read FOnBufClearCell
                                             write FOnBufClearCell;
    property OnReleasePasteBuffer: TReleasePasteBufferEvent
                                   read FOnReleasePasteBuffer
                                  write FOnReleasePasteBuffer;
    property OnCustomDrawAfterGrid: TDrawCellEvent read FOnCustomDrawAfterGrid
                                                  write FOnCustomDrawAfterGrid;
    property OnCheckCompatibility: TCheckCompatibilityEvent
                                    read FOnCheckCompatibility
                                   write FOnCheckCompatibility;
    property OnMakePasteBufferCopy: TMakePasteBufferCopyEvent
                             read FOnMakePasteBufferCopy
                            write FOnMakePasteBufferCopy;
    property OnContentsChanged: TNotifyEvent read FOnContentsChanged
                                            write FOnContentsChanged;
    property OnBeforeContentChange: TContentChangeEvent
                                    read FOnBeforeContentChange
                                   write FOnBeforeContentChange;
    property OnSysKeyDown: TOnSysKeyEvent read FOnSysKeyDown
                                         write FOnSysKeyDown;
    property OnSysKeyUp: TOnSysKeyEvent read FOnSysKeyUp
                                       write FOnSysKeyUp;
    property OnMouseEnter: TNotifyEvent read FOnMouseEnter
                                       write FOnMouseEnter;
    property OnMouseLeave: TNotifyEvent read FOnMouseLeave
                                       write FOnMouseLeave;


    procedure FillVisibilityBuffer(Buffer: Pointer; const SrcRect: TRect; UseSelection: Boolean);                                       
    procedure MultiSelectModeInit(Shift: TShiftState);
    procedure FillPasteBuffer(var BufData: Pointer;
                              var BufRect: TRect; Cut, UpdateSelection: Boolean);
    procedure FillTempSelection(const Rect: TRect);

    procedure SetMapSize(AWidth, AHeight: Integer; DoRepaint: Boolean = True); virtual;
    procedure SetTileSize(AWidth, AHeight: Integer; DoRepaint: Boolean = True); virtual;

    procedure SelectionChanged; virtual;
    procedure SetGlobalCursor(ACursor: TCursor);
    procedure SetPosition(X, Y: Integer; DoRepaint: Boolean = True); virtual;
    procedure DrawCell(CellX, CellY: Integer);
    procedure ScrollToSelectedCell;
    procedure ScrollToSelectedCellPerform;
    procedure ScrollTo(CellX, CellY: Integer);
    procedure ScrollToPerform(CellX, CellY: Integer);    
    procedure CopyMapData(var Data: Pointer; var Rect: TRect);
    procedure PasteMapData(Data: Pointer; const Rect: TRect; Apply: Boolean = False); virtual;
    procedure PasteBufferApply; virtual;
    procedure ClearSelection;
    procedure Deselect(Apply: Boolean = True);
    procedure InvertSelection;
    procedure UpdateSelection;
    function CheckCompatibility(Data: Pointer): Boolean; virtual;
    procedure DragDrop(Source: TObject; X, Y: Integer); override;
    procedure ApplySelection(Buffer: Pointer; const Rect: TRect; Repaint: Boolean = True);
    function SelectAll: Boolean;
    function MakePasteBufferCopy(Source: Pointer): Pointer; virtual;
    procedure ContentsChanged; virtual;
    procedure UpdateScrollBars(DoRepaint: Boolean);
    function BackupSelection(var Rect: TRect): Pointer;

    procedure ReleasePasteBuffer; virtual;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;        
  end;

  TTileMapView = class(TCustomTileMapView)
  published
    property Width default 180;
    property Height default 180;
    property MapWidth;
    property MapHeight;
    property TileWidth default 16;
    property TileHeight default 16;
    property OffsetX;
    property OffsetY;
    property ShowGrid default True;
    property SelectionMode default selSingle;
    property GridStyle;
    property GridMode default pmNot;
    property GridWidth default 1;
    property GridColor default clBlack;
    property GridMinH default 8;
    property GridMinV default 8;
    property SelectStyle default psDot;
    property SelectMode default pmNot;
    property SelectWidth default 1;
    property SelectColor default clBlack;
    property DragScrollShiftState default [ssMiddle];
    property SelectNewShiftState default [ssLeft];
    property SelectIncludeShiftState default [ssLeft, ssShift];
    property SelectExcludeShiftState default [ssLeft, ssAlt];
    property SelectCopyShiftState default [ssLeft, ssCtrl, ssAlt];
    property SelectDragShiftState default [ssLeft, ssCtrl];
    property ScrollBarsAllwaysVisible default True;
    property SelectionEnabled default True;
    property DefaultCursor default crDefault;
    property CanPopup default True;

    property OnDrawCell;
    property OnMapResize;
    property OnScrollButtonCheck;
    property OnSelectButtonCheck;
    property OnCustomMouseActionCheck;
    property OnCustomMouseAction;
    property OnCustomMouseActionFinish;
    property OnSelectionChanged;
    property OnChangeTileSize;
    property OnBeforePaint;
    property OnAfterPaint;
    property OnGetCellState;
    property OnGetBufCellState;
    property OnCopyMapData;
    property OnPasteBufferApply;
    property OnClearCell;
    property OnBufClearCell;
    property OnReleasePasteBuffer;
    property OnCustomDrawAfterGrid;
    property OnCheckCompatibility;
    property OnSetPosition;
    property OnMakePasteBufferCopy;
    property OnContentsChanged;
    property OnBeforeContentChange;
    property OnSysKeyDown;
    property OnSysKeyUp;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;

  //////////
    property Align;
    property Anchors;
    property BorderStyle default bsSingle;
    property Constraints;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Color nodefault;
    property Ctl3D;
    property Font;
    property ParentBackground default False;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop default True;
    property Visible;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnDockDrop;
    property OnDockOver;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
  end;

  TEmptyDragObject = class(TDragControlObjectEx)
   protected
    function GetDragCursor(Accepted: Boolean; X, Y: Integer): TCursor; override;
  end;

  TBaseSelDragObject = class(TEmptyDragObject)
   private
    FData: Pointer;
    FHotSpot: TPoint;
    FWidth: Integer;
    FHeight: Integer;
   public
    property Data: Pointer read FData write FData;
    property HotSpot: TPoint read FHotSpot write FHotSpot;    
    property Width: Integer read FWidth write FWidth;
    property Height: Integer read FHeight write FHeight;
    procedure Assign(Source: TDragObject); override;
  end;

  TSelectionDragObject = class(TBaseSelDragObject)
   public
    constructor Create(AControl: TControl); override;
  end;

  TPasteBufferDragObject = class(TBaseSelDragObject)
   public
    constructor Create(AControl: TControl); override;
  end;

const
 SysKeyStates = [ssAlt, ssCtrl, ssShift];
 MouseStates = [ssLeft, ssRight, ssMiddle, ssDouble];
 MultiSelectStates = [mmMultiSelectNew,
                      mmMultiSelectInclude,
                      mmMultiSelectExclude,
                      mmMultiSelectMove,
                      mmMultiSelectCopy,
                      mmMultiSelectLayerCopy,
                      mmMultiSelectDrag,
                      mmMultiSelectLayerDrag];

function GetControlAtPos(Parent: TWinControl; const Point: TPoint): TWinControl;

implementation

uses Menus, MyUtils, HexUnit, BmpImg;

var
 MouseHook: HHook = 0;
 KeyboardHook: HHook = 0;
 CallWndProcHook: HHook = 0;
 DragControl: TControl = nil;
 DragObject: TDragObject = nil;
 MenuFocused: Boolean = False;

function GetControlAtPos(Parent: TWinControl; const Point: TPoint): TWinControl;
var
 New: TControl;
begin
 Result := nil;
 New := Parent;
 if Parent <> nil then
 begin
  repeat
   Result := TWinControl(New);
   New := Result.ControlAtPos(Result.ScreenToClient(Point), False);
  until not (New is TWinControl);
 end;
end;

function MouseProc(Code: Integer; WParam: LongInt; LParam: LongInt): LongInt; stdcall;
var
 Pos: TPoint;
 Keys: LongInt;
begin
 Result := CallNextHookEx(MouseHook, Code, WParam, LParam);
 if (Code >= HC_ACTION) then
 begin
  if GetCapture <> 0 then
  begin
   if DragControl <> nil then
   begin
    GetCursorPos(Pos);
    if WParam = WM_LBUTTONUP then
    begin
     try
      DragControl.EndDrag(True);
      DragControl.Perform(WM_LBUTTONUP, 0,
         LongInt(PointToSmallPoint(DragControl.ScreenToClient(Pos))));
     finally
      DragControl := nil;
     end;
    end else
    begin
     Keys := 0;
     if GetKeyState(VK_CONTROL) < 0 then
      Keys := Keys or MK_CONTROL;
     if GetKeyState(VK_SHIFT) < 0 then
      Keys := Keys or MK_SHIFT;
     if ssLeft in GlobalShiftState then
      Keys := Keys or MK_LBUTTON;
     if ssRight in GlobalShiftState then
      Keys := Keys or MK_RBUTTON;
     if ssMiddle in GlobalShiftState then
      Keys := Keys or MK_MBUTTON;

     DragControl.Perform(WM_MOUSEMOVE, Keys,
       LongInt(PointToSmallPoint(DragControl.ScreenToClient(Pos))));
    end;
   end;
  end else
   DragControl := nil;
 end;
end;

function KeyboardProc(Code: Integer; WParam: LongInt; LParam: LongInt): LongInt; stdcall;
var
 Control: TWinControl;
 MapView: TCustomTileMapView absolute Control;
 Key: Word;
 P: TPoint;
begin
 Result := CallNextHookEx(KeyboardHook, Code, WParam, LParam);
 if (Code >= HC_ACTION) and
    ((WParam = VK_MENU) or
     (WParam = VK_CONTROL) or
     (WParam = VK_SHIFT)) then
 begin
  GetCursorPos(P);
  Control := FindVCLWindow(P);
  if (Control <> nil) and not (csDesigning in Control.ComponentState) then
  begin
   Control := GetControlAtPos(Control, P);
   if Control is TCustomTileMapView then
   begin
    Key := WParam;
    if lParam >= 0 then
    begin
     if (WParam <> VK_MENU) and MenuFocused then
      EndMenu;
     MapView.SysKeyDown(Key, GlobalShiftState);
    end else
     MapView.SysKeyUp(Key, GlobalShiftState);
   end;
  end;
 end;
end;

function CallWndProc(Code: Integer; WParam: LongInt; LParam: LongInt): LongInt; stdcall;
var
 Struct: PCWPStruct;
begin
 if Code >= HC_ACTION then
 begin
  Struct := PCWPStruct(Pointer(lParam));
  if Struct <> nil then
  case Struct.Message of
   WM_ENTERMENULOOP: MenuFocused := True;
   WM_EXITMENULOOP: MenuFocused := False;
  end;
 end;
 Result := CallNextHookEx(CallWndProcHook, Code, WParam, LParam);
end;

{ TCustomTileMapView }

procedure TCustomTileMapView.ApplySelection(Buffer: Pointer; const Rect: TRect; Repaint: Boolean);
var
 Src: PByte absolute Buffer;
 Dst: PByte;
 RowStride, W, Y: Integer;
begin
 if Buffer <> FTempSel then
 begin
  SetLength(FTempSel, (Rect.Right - Rect.Left) * (Rect.Bottom - Rect.Top));
  Move(Buffer^, FTempSel[0], Length(FTempSel));
 end;
 FSelectionRect := Rect; 
 FSelectionMode := selMulti;
 if FSelectionBuffer = nil then
  SetLength(FSelectionBuffer, FMapWidth * FMapHeight);
 Src := Buffer;
 FillChar(Pointer(FSelectionBuffer)^, Length(FSelectionBuffer), 0);
 RowStride := Rect.Right - Rect.Left;
 W := RowStride;
 Dst := Pointer(FSelectionBuffer);

 if Rect.Left < 0 then
 begin
  Dec(Src, Rect.Left);
  Inc(W, Rect.Left);
 end else
  Inc(Dst, Rect.Left);

 if Rect.Top < 0 then
  Dec(Src, Rect.Top * RowStride);

 if Rect.Right > FMapWidth then
  Dec(W, Rect.Right - FMapWidth);

 if W > 0 then
 begin
  Y := Max(0, Rect.Top);
  Inc(Dst, Y * FMapWidth);
  for Y := Y to Min(FMapHeight, Rect.Bottom) - 1 do
  begin
   Move(Src^, Dst^, W);
   Inc(Src, RowStride);
   Inc(Dst, FMapWidth);
  end;
 end;

 if Repaint then
 begin
  Invalidate;
  if FMouseMode = mmNothing then
   UpdateCursor(GlobalShiftState);
 end;
 SelectionChanged;
end;

function TCustomTileMapView.BackupSelection(var Rect: TRect): Pointer;
begin
 if FTempSel <> nil then
 begin
  Rect := FSelectionRect;
  GetMem(Result, Length(FTempSel));
  Move(FTempSel[0], Result^, Length(FTempSel));
 end else
  Result := nil;
end;

procedure TCustomTileMapView.BufFillVisibility(Buffer: Pointer;
  BufWidth, BufHeight: Integer; Data: Pointer; const DataRect: TRect);
var
 PB: PBoolean absolute Buffer;
 RowStride, XX, X, Y, YY, H, W: Integer;
begin
 RowStride := BufWidth;
 FillChar(Buffer^, BufWidth * BufHeight, 0);
 W := DataRect.Right - DataRect.Left;
 XX := 0;
 if DataRect.Left < 0 then
 begin
  Dec(XX, DataRect.Left);
  Inc(W, DataRect.Left);
 end else
  Inc(PB, DataRect.Left);
 if DataRect.Right > BufWidth then
  Dec(W, DataRect.Right - BufWidth);
 if W > 0 then
 begin
  H := DataRect.Bottom - DataRect.Top;
  YY := 0;
  if DataRect.Top < 0 then
  begin
   Dec(YY, DataRect.Top);
   Inc(H, DataRect.Top);
  end else
   Inc(PB, DataRect.Top * RowStride);
  if DataRect.Bottom > BufHeight then
   Dec(H, DataRect.Bottom - BufHeight);
  Dec(RowStride, W);
  Inc(W, XX);
  Inc(H, YY);
  for Y := YY to H - 1 do
  begin
   for X := XX to W - 1 do
   begin
    DoGetBufCellState(Data, X, Y, PB^);
    Inc(PB);
   end;
   Inc(PB, RowStride);
  end;
 end;
end;

procedure TCustomTileMapView.CalculateTileXY;
var
 X, Y: Integer;
begin
 if FClientLeft <= 0 then
  X := 0 else
  X := FClientLeft;

 X := FViewOffset.X + FCursorX - X;

 if X < 0 then
  Dec(X, FTileWidth - 1);

 FTileX := X div FTileWidth;

 if FClientTop <= 0 then
  Y := 0 else
  Y := FClientTop;

 Y := FViewOffset.Y + FCursorY - Y;

 if Y < 0 then
  Dec(Y, FTileHeight - 1);

 FTileY := Y div FTileHeight;
end;

function TCustomTileMapView.CheckCompatibility(Data: Pointer): Boolean;
begin
 Result := False;
 if (Data <> nil) and Assigned(FOnCheckCompatibility) then
  FOnCheckCompatibility(Self, Data, Result);
end;

procedure TCustomTileMapView.ClearSelection;
begin
 if PasteBuffer <> nil then
 begin
  ReleasePasteBuffer;
  FillChar(FSelectionBuffer[0], Length(FSelectionBuffer), 0);
  Finalize(FTempSel);
 end else
 if FSelectionMode = selMulti then
  SelectedAreaClear(Bounds(0, 0, FMapWidth, FMapHeight), FSelectionBuffer) else
 if FSelectionMode = selSingle then
  SelectedAreaClear(Bounds(SelectedCell.X, SelectedCell.Y, 1, 1), nil);
 Invalidate;
 SelectionChanged;
end;

procedure TCustomTileMapView.CMCtl3DChanged(var Message: TMessage);
begin
 inherited;
 if FBorderStyle = bsSingle then
  RecreateWnd;
end;

procedure TCustomTileMapView.CMCursorChanged(var Message: TMessage);
var
 P: TPoint;
begin
 GetCursorPos(P);
 if FindDragTarget(P, False) = Self then
   Perform(WM_SETCURSOR, Handle, HTCLIENT);
end;

procedure TCustomTileMapView.CMDrag(var Message: TCMDrag);
begin
 with Message, DragRec^ do
 begin
  if (Target <> nil) and (DragControl = nil) then
  begin
   DragControl := Target;
   DragObject := Source;
  end;
 end;
 inherited;
end;
        {
procedure TCustomTileMapView.CMEnabledChanged(var Message: TMessage);
begin
 KillTimer(Handle, 1);
 if Enabled then
  SetTimer(WindowHandle, 1, 100, nil);
 inherited;
end;       }

procedure TCustomTileMapView.CMFocusChanged(var Message: TCMFocusChanged);
begin
 inherited;
 Invalidate;
end;

procedure TCustomTileMapView.CMMouseEnter(var Message: TMessage);
begin
 if not (csDesigning in ComponentState) and
    (FMouseMode = mmNothing) then
  UpdateCursor(GlobalShiftState);
 inherited;
 if Assigned(FOnMouseEnter) then
  FOnMouseEnter(Self);
end;

procedure TCustomTileMapView.CMMouseLeave(var Message: TMessage);
begin
 if GetCapture = 0 then
 begin
  FTileX := -1;
  FTileY := -1;
  FCursorX := -1;
  FCursorY := -1;
 end;
 if not (csDesigning in ComponentState) and (FMouseMode = mmNothing) then
  Cursor := FDefaultCursor;
 inherited;
 if Assigned(FOnMouseLeave) then
  FOnMouseLeave(Self);
end;

procedure TCustomTileMapView.CMRecreateWnd(var Message: TMessage);
begin
 inherited;
 UpdateScrollBars(True);
end;

procedure TCustomTileMapView.ContentsChanged;
begin
 if Assigned(FOnContentsChanged) then
  FOnContentsChanged(Self);
end;

procedure TCustomTileMapView.CopyMapData(var Data: Pointer; var Rect: TRect);
begin
 GetVisibleRect(Rect, Pointer(FSelectionBuffer), FMapWidth, FMapHeight, 1);
 DoCopyMapData(Data, Rect);
end;

constructor TCustomTileMapView.Create(AOwner: TComponent);
begin
 inherited;
 ControlStyle := ControlStyle - [csSetCaption] +
    [csCaptureMouse, csOpaque, csReplicatable,
     csDisplayDragImage, csReflector];
 FScrollBarsAllwaysVisible := True;
 FTileX := -1;
 FTileY := -1;
 FCursorX := -1;
 FCursorY := -1;
 FMinGridH := 8;
 FMinGridV := 8;
 Width := 180;
 Height := 180;
 TabStop := True;
 FBorderStyle := bsSingle;
 FTileWidth := 16;
 FTileHeight := 16;
 DoubleBuffered := True;
 FSelectedCell.X := 0;
 FSelectedCell.Y := 0;
 FGridWidth := 1;
 FShowGrid := True;
 FSelectionMode := selSingle;
 FGridMode := pmNot;
 FSelectStyle := psDot;
 FSelectColor := clBlack;
 FSelectMode := pmNot;
 FSelectWidth := 1;
 FSelectionEnabled := True;
 FCanPopup := True;

 FDragScrollShiftState := [ssMiddle];
 FSelectNewShiftState := [ssLeft];
 FSelectIncludeShiftState := [ssLeft, ssShift];
 FSelectExcludeShiftState := [ssLeft, ssAlt];
 FSelectCopyShiftState := [ssLeft, ssCtrl, ssAlt];
 FSelectDragShiftState := [ssLeft, ssCtrl];
 FDefaultCursor := crDefault;
 DragCursor := crSelPaste;
end;

procedure TCustomTileMapView.CreateParams(var Params: TCreateParams);
begin
 inherited;
 if not (csDesigning in ComponentState) then
 begin
  if MouseHook = 0 then
   MouseHook := SetWindowsHookEx(WH_MOUSE, @MouseProc, 0, GetCurrentThreadID);
  if KeyBoardHook = 0 then
   KeyboardHook := SetWindowsHookEx(WH_KEYBOARD, @KeyboardProc, 0, GetCurrentThreadID);
  if CallWndProcHook = 0 then
   CallWndProcHook := SetWindowsHookEx(WH_CALLWNDPROC, @CallWndProc, 0, GetCurrentThreadID);
 end;
 with Params do
 begin
  Style := Style or WS_HSCROLL or WS_VSCROLL;
  WindowClass.style := WindowClass.style or CS_HREDRAW or CS_VREDRAW;
  if FBorderStyle = bsSingle then
  begin
   if Ctl3D then
   begin
    ExStyle := ExStyle or WS_EX_CLIENTEDGE;
    Style := Style and not WS_BORDER;
   end else
    Style := Style or WS_BORDER;
  end else
   Style := Style and not WS_BORDER;
 end;
end;


procedure TCustomTileMapView.CreateWnd;
begin
 inherited;
 (*
 InitializeFlatSB(Handle);
 FlatSB_SetScrollProp(Handle, WSB_PROP_HSTYLE, FSB_ENCARTA_MODE, False);
 FlatSB_SetScrollProp(Handle, WSB_PROP_VSTYLE, FSB_ENCARTA_MODE, False);*)
 UpdateScrollBars(True);
{ KillTimer(Handle, 1);
 SetTimer(WindowHandle, 1, 100, nil);}
end;

procedure TCustomTileMapView.Deselect(Apply: Boolean);
begin
 if Apply then
  PasteBufferApply else
  ReleasePasteBuffer;
 if MultiSelected then
 begin
  FillChar(FSelectionBuffer[0], FMapWidth * FMapHeight, 0);
  Finalize(FTempSel);
  Invalidate;
  SelectionChanged;
 end;
end;

destructor TCustomTileMapView.Destroy;
begin
 if not (csDesigning in ComponentState) then
 begin
  KillTimer(WindowHandle, 1);
  if GetCaptureControl = Self then
   SetCaptureControl(nil);

  ReleasePasteBuffer;
 end;
 inherited;
end;

procedure TCustomTileMapView.DoAfterPaint;
begin
 if Assigned(FOnAfterPaint) then
  FOnAfterPaint(Self);
end;

procedure TCustomTileMapView.DoBeforePaint;
begin
 if Assigned(FOnBeforePaint) then
  FOnBeforePaint(Self);
end;

procedure TCustomTileMapView.DoBufClearCell(X, Y: Integer; Data: Pointer);
begin
 if Assigned(FOnBufClearCell) then
  FOnBufClearCell(Self, X, Y, Data);
end;

procedure TCustomTileMapView.DoClearCell(X, Y: Integer);
begin
 if Assigned(FOnClearCell) then
  FOnClearCell(Self, X, Y);
end;

procedure TCustomTileMapView.DoCopyMapData(var Data: Pointer;
  var Rect: TRect);
begin
 if (Data <> nil) and Assigned(FOnReleasePasteBuffer) then
 begin
  FOnReleasePasteBuffer(Self, Data);
  Data := nil;
 end;
 if Assigned(FOnCopyMapData) then
  FOnCopyMapData(Self, Data, Rect, Pointer(FSelectionBuffer));
end;

procedure TCustomTileMapView.DoCustomDrawAfterGrid(const Rect: TRect; CellX, CellY: Integer);
begin
 if Assigned(FOnCustomDrawAfterGrid) then
  FOnCustomDrawAfterGrid(Self, Rect, CellX, CellY);
end;

procedure TCustomTileMapView.DoCustomMouseAction(var Done: Boolean);
begin
 if Assigned(FOnCustomMouseAction) then
  FOnCustomMouseAction(Self, Done);
end;

procedure TCustomTileMapView.DoCustomMouseActionCheck(Shift: TShiftState;
  var Act: Boolean);
begin
 if Assigned(FOnCustomMouseActionCheck) then
  FOnCustomMouseActionCheck(Self, Shift, Act);
end;

procedure TCustomTileMapView.DoCustomMouseActionFinish;
begin
 if Assigned(FOnCustomMouseActionFinish) then
  FOnCustomMouseActionFinish(Self);
end;

procedure TCustomTileMapView.DoDrawCell(const Rect: TRect; CellX, CellY: Integer);
begin
 if Assigned(FOnDrawCell) then
  FOnDrawCell(Self, Rect, CellX, CellY) else
 with Canvas do
 begin
  Brush.Style := bsSolid;
  Brush.Color := Color;
  FillRect(Rect);
 end;
end;

procedure TCustomTileMapView.DoEnter;
begin
 HandleNeeded;
 KillTimer(WindowHandle, 1);
 SetTimer(WindowHandle, 1, 100, nil);
 inherited;
end;

procedure TCustomTileMapView.DoExit;
begin
 KillTimer(WindowHandle, 1);
 inherited;
end;

procedure TCustomTileMapView.DoFillPasteBuffer(SelectionBuffer: Pointer);
begin
 ReleasePasteBuffer;
 if Assigned(FOnCopyMapData) then
  FOnCopyMapData(Self, PasteBuffer, PasteBufRect, SelectionBuffer);
end;

procedure TCustomTileMapView.DoGetBufCellState(Data: Pointer; CellX,
  CellY: Integer; var Dirty: Boolean);
begin
 if Assigned(FOnGetBufCellState) then
  FOnGetBufCellState(Self, Data,CellX, CellY, Dirty);
end;

procedure TCustomTileMapView.DoGetCellState(CellX, CellY: Integer;
  var Dirty: Boolean);
begin
 if Assigned(FOnGetCellState) then
  FOnGetCellState(Self, CellX, CellY, Dirty);
end;

function TCustomTileMapView.DoMouseWheelDown(Shift: TShiftState;
  MousePos: TPoint): Boolean;
begin
 Result := inherited DoMouseWheelDown(Shift, MousePos);
 if not Result and (GlobalShiftState - SysKeyStates = []) then
 begin
  if ssCtrl in Shift then
   SetOffsetX(FViewOffset.X + FTileWidth) else
   SetOffsetY(FViewOffset.Y + FTileHeight);
   
  Result := True;
 end;
end;

function TCustomTileMapView.DoMouseWheelUp(Shift: TShiftState;
  MousePos: TPoint): Boolean;
begin
 Result := inherited DoMouseWheelUp(Shift, MousePos);
 if not Result and (GlobalShiftState - SysKeyStates = []) then
 begin
  if ssCtrl in Shift then
   SetOffsetX(FViewOffset.X - FTileWidth) else
   SetOffsetY(FViewOffset.Y - FTileHeight);

  Result := True;
 end;
end;

procedure TCustomTileMapView.DoPasteBufferApply;
begin
 if PasteBuffer <> nil then
 begin
  DoBeforeContentChange(ccPasteBufferApply);
  if Assigned(FOnPasteBufferApply) then
   FOnPasteBufferApply(Self, PasteBufRect.Left, PasteBufRect.Top, PasteBuffer);
  ContentsChanged;
 end;
end;

procedure TCustomTileMapView.ReleasePasteBuffer;
begin
 if PasteBuffer <> nil then
 begin
  if Assigned(FOnReleasePasteBuffer) then
   FOnReleasePasteBuffer(Self, PasteBuffer);
  PasteBuffer := nil;
 end;
end;

procedure TCustomTileMapView.DoScrollButtonCheck(Shift: TShiftState;
                                                         var Act: Boolean);
begin
 if not Act then
  Act := Shift = FDragScrollShiftState;

 if Assigned(FOnScrollButtonCheck) then
  FOnScrollButtonCheck(Self, Shift, Act);
end;

procedure TCustomTileMapView.DoSelectButtonCheck(Shift: TShiftState;
           var SelMode: TSelectInternalMode);
begin
 if FSelectionEnabled then
 begin
  if SelMode = simNone then
  begin
   if FSelectionMode = selSingle then
   begin
    if (Shift <> FDragScrollShiftState) and
      ((ssLeft in Shift) or
       (ssMiddle in Shift) or
       (ssRight in Shift)) then
     SelMode := simNew;
   end else
   if FSelectionMode = selMulti then
   begin
    if Shift = FSelectNewShiftState then
     SelMode := simNew else
    if Shift = FSelectIncludeShiftState then
     SelMode := simInclude else
    if Shift = FSelectExcludeShiftState then
     SelMode := simExclude;
    if Shift = FSelectCopyShiftState then
     SelMode := simCopy else
    if Shift = FSelectDragShiftState then
     SelMode := simDrag;
   end;
  end;
  if Assigned(FOnSelectButtonCheck) then
   FOnSelectButtonCheck(Self, Shift, SelMode);
 end;
end;

procedure TCustomTileMapView.DoStartDrag(var DragObject: TDragObject);
begin
 inherited;
 if DragObject = nil then
 begin
  if (FSelectionMode = selMulti) then
  case FMouseMode of
   mmMultiSelectMove:
    DragObject := TSelectionDragObject.Create(Self);
   mmMultiSelectCopy,
   mmMultiSelectLayerCopy,
   mmMultiSelectDrag,
   mmMultiSelectLayerDrag:
    DragObject := TPasteBufferDragObject.Create(Self);
  end;
  if DragObject = nil then
   DragObject := TEmptyDragObject.Create(Self);
 end;
end;

procedure TCustomTileMapView.DragDrop(Source: TObject; X, Y: Integer);
var
 Obj: TBaseSelDragObject absolute Source;
 Rect: TRect;
begin
 if Source is TBaseSelDragObject then
 begin
  FCursorX := X;
  FCursorY := Y;
  CalculateTileXY;
  with Obj do
   Rect := Bounds(FTileX - FHotSpot.X, FTileY - FHotSpot.Y, FWidth, FHeight);

  if Source is TPasteBufferDragObject then
  begin
   PasteMapData(MakePasteBufferCopy(Obj.FData), Rect);
   SetFocus;
  end else
  if Source is TSelectionDragObject then
  begin
   PasteBufferApply;
   ApplySelection(Obj.FData, Rect);
   SetFocus;
  end;

  if Obj.Control is TCustomTileMapView then
   with TCustomTileMapView(Obj.Control) do
    if PasteBuffer <> nil then
     ClearSelection else
     Deselect;
 end;
end;

procedure TCustomTileMapView.DragOver(Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var
 Src: TBaseSelDragObject absolute Source;
 BufObj: TPasteBufferDragObject absolute Source;
begin
 inherited;
 Accept := Accept or ((Source is TBaseSelDragObject) and
                      (Self <> Src.Control));
 if Accept then
 begin
  FCursorX := X;
  FCursorY := Y;
  CalculateTileXY;
  Accept := (FTileX >= 0) and (FTileX < MapWidth) and
            (FTileY >= 0) and (FTileY < MapHeight) and
            (((Source is TSelectionDragObject) and
              (FSelectionMode = selMulti)) or
            ((Source is TPasteBufferDragObject) and
             CheckCompatibility(BufObj.Data)));
 end;
end;

procedure TCustomTileMapView.DrawCell(CellX, CellY: Integer);
begin
 PostMessage(Handle, WM_DRAWCELL, CellX, CellY);
end;

procedure TCustomTileMapView.DrawCellInternal(const Rect: TRect; CellX, CellY: Integer);
var
 Temp: TRect;
 X, Y, J, K, X2, Y2, HShift, VShift: Integer;
 Sel: set of (selLeft, selRight, selTop, selBottom);
begin
 DoDrawCell(Rect, CellX, CellY);

 if FShowGrid and
    (FTileWidth >= FMinGridH) and
    (FTileHeight >= FMinGridV) then
  with Canvas do
  begin
   Pen.Style := FGridStyle;
   Pen.Mode := FGridMode;
   Pen.Width := FGridWidth;
   Pen.Color := FGridColor;
   Brush.Style := bsClear;
   Temp := Rect;
   Inc(Temp.Right);
   Inc(Temp.Bottom);
   Rectangle(Temp);
  end;

 if Selected[CellX, CellY] then
  with Canvas do
  begin
   Brush.Style := bsClear;

   Sel := [selLeft, selRight, selTop, selBottom];

   if not Selected[CellX - 1, CellY] then
    Exclude(Sel, selLeft);

   if not Selected[CellX + 1, CellY] then
    Exclude(Sel, selRight);

   if not Selected[CellX, CellY - 1] then
    Exclude(Sel, selTop);

   if not Selected[CellX, CellY + 1] then
    Exclude(Sel, selBottom);

   Temp := Rect;

   if not (selLeft in Sel) then
    Inc(Temp.Left);
   if not (selRight in Sel) then
    Dec(Temp.Right);
   if not (selTop in Sel) then
    Inc(Temp.Top);
   if not (selBottom in Sel) then
    Dec(Temp.Bottom);

   HShift := ((5 - Rect.Left mod 6) + FSelectAnimShift) mod 6;
   VShift := ((5 - Temp.Top mod 6) + FSelectAnimShift) mod 6;

   Pen.Width := FSelectWidth;

   if not (selTop in Sel) then
   begin
    X := Rect.Left + HShift;
    Pen.Style := psDot;
    Pen.Mode := pmCopy;
    Pen.Color := clBlack;
    K := Min(X, Rect.Right);
    if HShift <= 3 then
    begin
     MoveTo(Rect.Left, Rect.Top);
     LineTo(K, Rect.Top);
     Pen.Color := clWhite;
    end else
    begin
     J := X - 3;
     if J < Rect.Right then
     begin
      MoveTo(J, Rect.Top);
      LineTo(K, Rect.Top);
      Pen.Color := clWhite;
      MoveTo(Rect.Left, Rect.Top);
      LineTo(J, Rect.Top);
     end;
     MoveTo(K, Rect.Top);
    end;
    if K < Rect.Right then
     LineTo(Rect.Right, Rect.Top);
   end else
   if FTileWidth >= FMinGridH then
   begin
    Pen.Color := FSelectColor;
    Pen.Style := FSelectStyle;
    Pen.Mode := FSelectMode;
    MoveTo(Temp.Left, Rect.Top);
    LineTo(Temp.Right, Rect.Top);
   end;

   if not (selLeft in Sel) then
   begin
    Y := Temp.Top + VShift;
    Pen.Style := psDot;
    Pen.Mode := pmCopy;
    Pen.Color := clBlack;
    K := Min(Y, Temp.Bottom);
    if VShift <= 3 then
    begin
     MoveTo(Rect.Left, Temp.Top);
     LineTo(Rect.Left, K);
     Pen.Color := clWhite;
    end else
    begin
     J := Y - 3;
     if J < Temp.Bottom then
     begin
      MoveTo(Rect.Left, J);
      LineTo(Rect.Left, K);
      Pen.Color := clWhite;
      MoveTo(Rect.Left, Temp.Top);
      LineTo(Rect.Left, J);
     end;
     MoveTo(Rect.Left, K);
    end;
    if K < Temp.Bottom then
     LineTo(Rect.Left, Temp.Bottom);
   end else
   if FTileHeight >= FMinGridV then   
   begin
    Pen.Color := FSelectColor;
    Pen.Style := FSelectStyle;
    Pen.Mode := FSelectMode;
    MoveTo(Rect.Left, Temp.Top);
    LineTo(Rect.Left, Temp.Bottom);
   end;


   Pen.Style := psDot;
   Pen.Mode := pmCopy;

   if not (selBottom in Sel) then
   begin
    X := Rect.Left + HShift;   
    Y := Rect.Bottom - 1;
    Pen.Color := clBlack;
    K := Min(X, Rect.Right);
    if HShift <= 3 then
    begin
     MoveTo(Rect.Left, Y);
     LineTo(K, Y);
     Pen.Color := clWhite;
    end else
    begin
     J := X - 3;
     if J < Rect.Right then
     begin
      MoveTo(J, Y);
      LineTo(K, Y);
      Pen.Color := clWhite;
      MoveTo(Rect.Left, Y);
      LineTo(J, Y);
     end;
     MoveTo(K, Y);
    end;
    if K < Rect.Right then
     LineTo(Rect.Right, Y);
   end;

   X := Rect.Right - 1;
   if not (selRight in Sel) then
   begin
    Y := Temp.Top + VShift;
    Pen.Color := clBlack;
    K := Min(Y, Temp.Bottom);
    if VShift <= 3 then
    begin
     MoveTo(X, Temp.Top);
     LineTo(X, K);
     Pen.Color := clWhite;
    end else
    begin
     J := Y - 3;
     if J < Temp.Bottom then
     begin
      MoveTo(X, J);
      LineTo(X, K);
      Pen.Color := clWhite;
      MoveTo(X, Temp.Top);
      LineTo(X, J);
     end;
     MoveTo(X, K);
    end;
    if K < Temp.Bottom then
     LineTo(X, Temp.Bottom);
   end;

   
   Pen.Color := clBlack;
   X2 := Rect.Left + 3 + HShift;
   Y2 := Temp.Top + 3 + VShift;
   if not (selTop in Sel) and
      (X2 < Rect.Right) then
   begin
    MoveTo(X2, Rect.Top);
    LineTo(Rect.Right, Rect.Top);
   end;

   if not (selLeft in Sel) and
      (Y2 < Temp.Bottom) then
   begin
    MoveTo(Rect.Left, Y2);
    LineTo(Rect.Left, Temp.Bottom);
   end;

   if not (selBottom in Sel) and
     (X2 < Rect.Right) then
   begin
    Y := Rect.Bottom - 1;
    MoveTo(X2, Y);
    LineTo(Rect.Right, Y);
   end;

   if not (selRight in Sel) and
      (Y2 < Temp.Bottom) then
   begin
    X := Rect.Right - 1;
    MoveTo(X, Y2);
    LineTo(X, Temp.Bottom);
   end;
  end;

 case FMouseMode of
  mmMultiSelectNew,
  mmMultiSelectInclude,
  mmMultiSelectExclude:
  if (CellX >= Min(FMouseRect.Left, FMouseRect.Right)) and
     (CellX <= Max(FMouseRect.Left, FMouseRect.Right)) and
     (CellY >= Min(FMouseRect.Top, FMouseRect.Bottom)) and
     (CellY <= Max(FMouseRect.Top, FMouseRect.Bottom)) then
  with Canvas do
  begin
   Pen.Style := psClear;
   Pen.Mode := pmNot;
   Brush.Style := bsSolid;
   Brush.Color := 0;
   Rectangle(Rect);
  end;
  mmCustom: DoCustomDrawAfterGrid(Rect, CellX, CellY);
 end;
end;

procedure TCustomTileMapView.FillPasteBuffer(var BufData: Pointer;
                                             var BufRect: TRect;
                                             Cut, UpdateSelection: Boolean);
var
 Temp: Pointer;
begin
 GetVisibleRect(BufRect, Pointer(FSelectionBuffer), FMapWidth, FMapHeight, 1);
 if (BufRect.Right - BufRect.Left > 0) and
    (BufRect.Bottom - BufRect.Top > 0) then
 begin
  GetMem(Temp, Length(FSelectionBuffer));
  try
   FillVisibilityBuffer(Temp, BufRect, True);
   if not ZeroMemCheck(Temp, Length(FSelectionBuffer)) then
   begin
    if UpdateSelection then
    begin
     GetVisibleRect(BufRect, Temp, FMapWidth, FMapHeight, 1);
     Move(Temp^, Pointer(FSelectionBuffer)^, Length(FSelectionBuffer));
    end;
    DoCopyMapData(BufData, BufRect);
    if Cut then
     SelectedAreaClear(BufRect, Temp);
   end;
  finally
   FreeMem(Temp);
  end;
 end else
 if (BufData <> nil) and Assigned(FOnReleasePasteBuffer) then
 begin
  FOnReleasePasteBuffer(Self, BufData);
  BufData := nil;
 end;;
end;

procedure TCustomTileMapView.FillTempSelection(const Rect: TRect);
var
 W, WW, H: Integer;
 Src, Dst: PBoolean;
begin
 FSelectionRect := Rect;
 W := Rect.Right - Rect.Left;
 H := Rect.Bottom - Rect.Top;
 SetLength(FTempSel, W * H);
 if FTempSel <> nil then
 begin
  Src := Addr(FSelectionBuffer[Rect.Top * FMapWidth + Rect.Left]);
  Dst := Pointer(FTempSel);
  WW := Min(W, FMapWidth);
  while H > 0 do
  begin
   Move(Src^, Dst^, WW);
   Inc(Src, FMapWidth);
   Inc(Dst, W);
   Dec(H);
  end;
 end;
end;

procedure TCustomTileMapView.FillVisibilityBuffer(Buffer: Pointer;
       const SrcRect: TRect; UseSelection: Boolean);
var
 PB: PBoolean absolute Buffer;
 SB: PBoolean;
 RowStride, X, Y: Integer;
begin
 FInternalIndex := SrcRect.Top * FMapWidth + SrcRect.Left;
 FillChar(PB^, FMapWidth * FMapHeight, 0);
 Inc(PB, FInternalIndex);
 RowStride := FMapWidth - (SrcRect.Right - SrcRect.Left);
 if UseSelection then
 begin
  SB := Addr(FSelectionBuffer[FInternalIndex]);
  for Y := SrcRect.Top to SrcRect.Bottom - 1 do
  begin
   for X := SrcRect.Left to SrcRect.Right - 1 do
   begin
    if SB^ then
     DoGetCellState(X, Y, PB^);
    Inc(PB);
    Inc(SB);
    Inc(FInternalIndex);
   end;
   Inc(PB, RowStride);
   Inc(SB, RowStride);
   Inc(FInternalIndex, RowStride);
  end;
 end else
 for Y := SrcRect.Top to SrcRect.Bottom - 1 do
 begin
  for X := SrcRect.Left to SrcRect.Right - 1 do
  begin
   DoGetCellState(X, Y, PB^);
   Inc(PB);
   Inc(FInternalIndex);
  end;
  Inc(PB, RowStride);
  Inc(FInternalIndex, RowStride);
 end;
end;

function TCustomTileMapView.GetMultiSelected: Boolean;
begin
 Result := (FSelectionMode = selMulti) and SomethingSelected;
end;

function TCustomTileMapView.GetOffsetX: Integer;
begin
 Result := FViewOffset.X;
end;

function TCustomTileMapView.GetOffsetY: Integer;
begin
 Result := FViewOffset.Y;
end;

function TCustomTileMapView.GetSelected(CellX, CellY: Integer): Boolean;
begin
 Result := False;
 case FSelectionMode of
  selSingle: Result := (FSelectedCell.X = CellX) and (FSelectedCell.Y = CellY);
  selMulti: if (CellX >= 0) and (CellY >= 0) and
               (CellX < FMapWidth) and (CellY < FMapHeight) then
    Result := FSelectionBuffer[CellY * FMapWidth + CellX];
 end;
end;

function TCustomTileMapView.GetSelectedByIndex(Index: Integer): Boolean;
begin
 Result := False;
 case FSelectionMode of
  selSingle: Result := (FSelectedCell.Y * FMapWidth + FSelectedCell.X = Index);
  selMulti: if (Index >= 0) and (Index < FMapSize) then
    Result := FSelectionBuffer[Index];
 end;
end;

function TCustomTileMapView.GetSelectedCellIndex: Integer;
begin
 if FSelectionMode = selSingle then
  with FSelectedCell do
  begin
   if (X >= 0) and
      (Y >= 0) and
      (X < FMapWidth) and
      (Y < FMapHeight) then
   begin
    Result := Y * MapWidth + X;
    Exit;
   end;
  end;
 Result := -1;
end;

function TCustomTileMapView.GetSelectionRect: TRect;
begin
 if (FSelectionMode = selMulti) and
    (FTempSel <> nil) then
   Result := FSelectionRect else
 if SomethingSelected then
  Result := Bounds(FSelectedCell.X, FSelectedCell.Y, 1, 1) else
  FillChar(Result, SizeOf(TRect), 0);
end;

function TCustomTileMapView.GetSomethingSelected: Boolean;
begin
 case FSelectionMode of
  selMulti:  Result := (FTempSel <> nil) or
                       Selected[FSelectedCell.X, FSelectedCell.Y];
  selSingle: Result := (FSelectedCell.X >= 0) and
                       (FSelectedCell.Y >= 0) and
                       (FSelectedCell.X < FMapWidth) and
                       (FSelectedCell.Y < FMapHeight);
  else       Result := False;
 end;
end;

function TCustomTileMapView.GetTempSelectionRect: TRect;
begin
 with Result do
 begin
  Left := Min(FMouseRect.Left, FMouseRect.Right);
  Top := Min(FMouseRect.Top, FMouseRect.Bottom);
  Right := Max(FMouseRect.Left, FMouseRect.Right) + 1;  
  Bottom := Max(FMouseRect.Top, FMouseRect.Bottom) + 1;
 end;
end;

function TCustomTileMapView.GetViewRect: TRect;
begin
 Result := FViewRect;
end;

procedure TCustomTileMapView.InvertSelection;
var
 I: Integer;
 PB: PBoolean;
begin
 PasteBufferApply;
 if (FSelectionMode = selMulti) and SomethingSelected then
 begin
  PB := Pointer(FSelectionBuffer);
  for I := 0 to Length(FSelectionBuffer) - 1 do
  begin
   PB^ := not PB^;
   Inc(PB);
  end;
  UpdateSelection;
 end;
end;

procedure TCustomTileMapView.KeyDown(var Key: Word; Shift: TShiftState);
begin
 inherited;
 case Key of
  VK_DELETE: ClearSelection;
  VK_RETURN:
  if PasteBuffer <> nil then
   PasteBufferApply else
   Deselect;
  VK_HOME:
  begin
   if ssCtrl in Shift then
    OffsetX := 0 else
    OffsetY := 0;
  end;
  VK_END:
  begin
   if ssCtrl in Shift then
    OffsetX := (MapWidth - 1) * TileWidth else
    OffsetY := (MapHeight - 1) * TileHeight;
  end;
  VK_PRIOR:
  begin
   if ssCtrl in Shift then
    OffsetX := FViewOffset.X - ClientWidth else
    OffsetY := FViewOffset.Y - ClientHeight;
  end;
  VK_NEXT:
  begin
   if ssCtrl in shift then
    OffsetX := FViewOffset.X + ClientWidth else
    OffsetY := FViewOffset.Y + ClientHeight;
  end;
  VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN:
  if (Shift = []) or not SomethingSelected then
  begin
   case Key of
    VK_LEFT:  OffsetX := FViewOffset.X - FTileWidth shr 1;
    VK_RIGHT: OffsetX := FViewOffset.X + FTileWidth shr 1;
    VK_UP:    OffsetY := FViewOffset.Y - FTileHeight shr 1;
    else      OffsetY := FViewOffset.Y + FTileHeight shr 1;
   end;
  end else
  case FSelectionMode of
   selSingle:
   case Key of
    VK_LEFT:  Selected[FSelectedCell.X - 1, FSelectedCell.Y] := True;
    VK_RIGHT: Selected[FSelectedCell.X + 1, FSelectedCell.Y] := True;
    VK_UP:    Selected[FSelectedCell.X, FSelectedCell.Y - 1] := True;
    else      Selected[FSelectedCell.X, FSelectedCell.Y + 1] := True;
   end;
   selMulti:
   if (PasteBuffer = nil) and
      ((Shift = FSelectIncludeShiftState - MouseStates) or
       (Shift = FSelectExcludeShiftState - MouseStates)) then
   begin
    if Shift = FSelectIncludeShiftState - MouseStates then
    case Key of
     VK_LEFT:
     begin
      Dec(FSelectionRect.Left);
      PostMessage(Handle, WM_HSCROLLTOVIEW, FSelectionRect.Left, 1);
     end;
     VK_RIGHT:
     begin
      Inc(FSelectionRect.Right);
      PostMessage(Handle, WM_HSCROLLTOVIEW, FSelectionRect.Right - 1, 1);
     end;
     VK_UP:
     begin
      Dec(FSelectionRect.Top);
      PostMessage(Handle, WM_VSCROLLTOVIEW, FSelectionRect.Top, 1);
     end;
     else
     begin
      Inc(FSelectionRect.Bottom);
      PostMessage(Handle, WM_VSCROLLTOVIEW, FSelectionRect.Bottom - 1, 1);
     end;
    end else
    case Key of
     VK_LEFT:
     begin
      Dec(FSelectionRect.Right);
      PostMessage(Handle, WM_HSCROLLTOVIEW, FSelectionRect.Right - 1, 1);
     end;
     VK_RIGHT:
     begin
      Inc(FSelectionRect.Left);
      PostMessage(Handle, WM_HSCROLLTOVIEW, FSelectionRect.Left, 1);
     end;
     VK_UP:
     begin
      Dec(FSelectionRect.Bottom);
      PostMessage(Handle, WM_VSCROLLTOVIEW, FSelectionRect.Bottom - 1, 1);
     end;
     else
     begin
      Inc(FSelectionRect.Top);
      PostMessage(Handle, WM_VSCROLLTOVIEW, FSelectionRect.Top, 1);
     end;
    end;

    SetLength(FTempSel, (FSelectionRect.Right - FSelectionRect.Left) *
                        (FSelectionRect.Bottom - FSelectionRect.Top));
    FillChar(FTempSel[0], Length(FTempSel), 1);
    ApplySelection(FTempSel, FSelectionRect, True);
   end else
   if ssCtrl in Shift then
   begin
    case Key of
     VK_LEFT:
     if FSelectionRect.Right > 0 then
     begin
      Dec(FSelectionRect.Left);
      Dec(FSelectionRect.Right);
      Dec(PasteBufRect.Left);
      Dec(PasteBufRect.Right);
      ApplySelection(FTempSel, FSelectionRect, False);
      PostMessage(Handle, WM_HSCROLLTOVIEW, Max(0, FSelectionRect.Left), 1);
     end;
     VK_RIGHT:
     if FSelectionRect.Left < FMapWidth then
     begin
      Inc(FSelectionRect.Left);
      Inc(FSelectionRect.Right);
      Inc(PasteBufRect.Left);
      Inc(PasteBufRect.Right);
      ApplySelection(FTempSel, FSelectionRect, False);
      PostMessage(Handle, WM_HSCROLLTOVIEW, Min(FMapWidth - 1,
                                    FSelectionRect.Right - 1), 1);
     end;
     VK_UP:
     if FSelectionRect.Bottom > 0 then
     begin
      Dec(FSelectionRect.Top);
      Dec(FSelectionRect.Bottom);
      Dec(PasteBufRect.Top);
      Dec(PasteBufRect.Bottom);
      ApplySelection(FTempSel, FSelectionRect, False);
      PostMessage(Handle, WM_VSCROLLTOVIEW, Max(0, FSelectionRect.Top), 1);
     end;
     else
     if FSelectionRect.Top < FMapHeight then
     begin
      Inc(FSelectionRect.Top);
      Inc(FSelectionRect.Bottom);
      Inc(PasteBufRect.Top);
      Inc(PasteBufRect.Bottom);
      ApplySelection(FTempSel, FSelectionRect, False);
      PostMessage(Handle, WM_VSCROLLTOVIEW, Min(FMapHeight - 1,
                    FSelectionRect.Bottom - 1), 1);
     end;
    end;
    Invalidate;
   end;
  end;
 end;
end;

function TCustomTileMapView.MakePasteBufferCopy(Source: Pointer): Pointer;
begin
 Result := nil;
 if Assigned(FOnMakePasteBufferCopy) then
  FOnMakePasteBufferCopy(Self, Source, Result);
end;

procedure TCustomTileMapView.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 Sel: Boolean;
 SelMode: TSelectInternalMode;
 Done: Boolean;
begin
 if csDesigning in ComponentState then Exit;
 FCursorX := X;
 FCursorY := Y;
 CalculateTileXY;
 if FMouseMode = mmNothing then
 begin
  Sel := False;
  DoScrollButtonCheck(Shift, Sel);
  if Sel then
  begin
   FMouseMode := mmScroll;
   FMouseRect.Left := FViewOffset.X + X;
   FMouseRect.Top := FViewOffset.Y + Y;
   SetGlobalCursor(crDragHand);
  end else
  begin
   FMouseRect.Left := FTileX;
   FMouseRect.Top := FTileY;
   SelMode := simNone;
   DoSelectButtonCheck(Shift, SelMode);
   if SelMode <> simNone then
   begin
    Include(FState, tmsTimerScroll);
    case FSelectionMode of
     selSingle:
     begin
      FMouseMode := mmSingleSelect;
      Selected[FMouseRect.Left, FMouseRect.Top] := True;
     end;
     selMulti:
     begin
      FMouseRect.Right := FMouseRect.Left;
      FMouseRect.Bottom := FMouseRect.Top;
      if (Shift = FSelectNewShiftState) and not Selected[FTileX, FTileY] then
       Deselect;
      FMouseMode := mmMultiSelectInit;
     end;
    end;
   end;
  end;
 end;
 Sel := False;
 DoCustomMouseActionCheck(Shift, Sel);
 Done := False;
 if Sel then
 begin
  DoCustomMouseAction(Done);
  if not Done then
   FMouseMode := mmCustom;
 end;
 inherited;
end;

procedure TCustomTileMapView.MouseMove(Shift: TShiftState; X, Y: Integer);
var
 Done: Boolean;
 OldX, OldY: Integer;
begin
 if csDesigning in ComponentState then Exit;
 OldX := FCursorX;
 OldY := FCursorY;
 FCursorX := X;
 FCursorY := Y;
 FNewCursorPosition := False;
 if FMouseMode = mmMultiSelectInit then
 begin
  if (FCursorX <> OldX) or (FCursorY <> OldY) then
  begin
   MultiSelectModeInit(Shift);
   FNewCursorPosition := True;
  end;
 end;
 OldX := FTileX;
 OldY := FTileY;
 CalculateTileXY;
 FNewCursorPosition := FNewCursorPosition or (FTileX <> OldX) or (FTileY <> OldY);
 if FMouseMode = mmCustom then
 begin
  Done := False;
  DoCustomMouseAction(Done);
  if Done then
   FMouseMode := mmNothing;
 end else
 if FMouseMode = mmNothing then
  UpdateCursor(Shift) else
 if Shift = [] then
  MouseUp(TMouseButton(-1), Shift, FCursorX, FCursorY) else
 if FMouseMode = mmScroll then
 begin
  SetPosition(Max(0, Min(FRangeX, FMouseRect.Left - FCursorX)),
              Max(0, Min(FRangeY, FMouseRect.Top - FCursorY)));
 end else
 if FNewCursorPosition then
 case FMouseMode of
  mmSingleSelect:
  begin
   FMouseRect.Left := FTileX;
   FMouseRect.Top := FTileY;
   Selected[FTileX, FTileY] := True;
  end;
  mmMultiSelectNew,
  mmMultiSelectInclude,
  mmMultiSelectExclude:
  begin
   FMouseRect.Right := FTileX;
   FMouseRect.Bottom := FTileY;
   ScrollTo(FTileX, FTileY);
  end;
  mmMultiSelectMove,
  mmMultiSelectCopy,
  mmMultiSelectLayerCopy,
  mmMultiSelectDrag,
  mmMultiSelectLayerDrag:
  begin
   Dec(OldX, FTileX);
   Dec(FMouseRect.Left,     OldX);
   Dec(FMouseRect.Right,    OldX);
   Dec(PasteBufRect.Left,   OldX);
   Dec(PasteBufRect.Right,  OldX);
   Dec(OldY, FTileY);
   Dec(FMouseRect.Top,      OldY);
   Dec(FMouseRect.Bottom,   OldY);
   Dec(PasteBufRect.Top,    OldY);
   Dec(PasteBufRect.Bottom, OldY);
   ApplySelection(FTempSel, FMouseRect, False);
   if GlobalShiftState - MouseStates <> [] then
    ScrollTo(FTileX, FTileY) else
    Invalidate;
  end;
 end;
 inherited MouseMove(Shift, FCursorX, FCursorY);
end;

procedure TCustomTileMapView.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 YY, W, L, R, T, B: Integer;
 Filler: Byte;
 P: PBoolean;
 Point: TPoint;
begin
 FCursorX := X;
 FCursorY := Y;
 CalculateTileXY;
 case FMouseMode of
  mmMultiSelectNew,
  mmMultiSelectInclude,
  mmMultiSelectExclude:
  begin
   L := Min(FMouseRect.Left, FMouseRect.Right);
   R := Max(FMouseRect.Left, FMouseRect.Right);
   T := Min(FMouseRect.Top, FMouseRect.Bottom);
   B := Max(FMouseRect.Top, FMouseRect.Bottom);

   if L < 0 then L := 0;
   if T < 0 then T := 0;

   if R >= FMapWidth then
    R := FMapWidth - 1;
   if B >= FMapHeight then
    B := FMapHeight - 1;

   W := (R + 1) - L;
   if FMouseMode = mmMultiSelectNew then
    FillChar(Pointer(FSelectionBuffer)^, Length(FSelectionBuffer), 0);
   if FMouseMode = mmMultiSelectExclude then
    Filler := 0 else
    Filler := 1;
   P := Addr(FSelectionBuffer[T * FMapWidth + L]);
   for YY := T to B do
   begin
    FillChar(P^, W, Filler);
    Inc(P, FMapWidth);
   end;
   FSelectedCell.X := FMouseRect.Right;
   FSelectedCell.Y := FMouseRect.Bottom;
   if (FMouseMode = mmMultiSelectExclude) and
      (PasteBuffer <> nil) then
   begin
    FMouseRect.Left := L;
    FMouseRect.Right := R + 1;
    FMouseRect.Top := T;
    FMouseRect.Bottom := B + 1;

    SelectedBufAreaClear(PasteBuffer, PasteBufRect, FMouseRect);

    RefreshPasteBufferSelection;
   end else
   begin
    GetVisibleRect(FMouseRect, Pointer(FSelectionBuffer), FMapWidth, FMapHeight, 1);
    FillTempSelection(FMouseRect);
   end;
   ScrollToSelectedCell;
   SelectionChanged;
  end;
  mmMultiSelectMove,
  mmMultiSelectCopy,
  mmMultiSelectLayerCopy,
  mmMultiSelectDrag,
  mmMultiSelectLayerDrag:
  begin
   FSelectedCell.X := FTileX;
   FSelectedCell.Y := FTileY;
   Invalidate;
   SelectionChanged;
  end;
  mmCustom: DoCustomMouseActionFinish;
 end;
 if tmsTimerScroll in FState then
  Exclude(FState, tmsTimerScroll);

 SetGlobalCursor(crDefault);
 UpdateCursor([]);
 FMouseMode := mmNothing;
  
 if CanPopup and (Button = mbRight) and (Shift = []) then
 begin
  Point.X := FCursorX;
  Point.Y := FCursorY;
  with ClientToScreen(Point) do
   Perform(WM_CONTEXTMENU, Handle, X or (Y shl 16));
 end;

 inherited;
end;

procedure TCustomTileMapView.MultiSelectModeInit(Shift: TShiftState);
var
 W, H: Integer;
 NewBuf: Pointer;
begin
 FMouseMode := mmNothing;
 if Shift = FSelectNewShiftState then
 begin
  if Selected[FTileX, FTileY] then
  begin
   FMouseMode := mmMultiSelectMove;
   SetGlobalCursor(crBlack);
  end else
  begin
   FMouseMode := mmMultiSelectNew;
   SetGlobalCursor(crSelNew);
  end;
 end else
 if Shift = FSelectIncludeShiftState then
 begin
  FMouseMode := mmMultiSelectInclude;
  SetGlobalCursor(crSelInc);
 end else
 if Shift = FSelectExcludeShiftState then
 begin
  FMouseMode := mmMultiSelectExclude;
  SetGlobalCursor(crSelDec);
 end else
 if Shift = FSelectCopyShiftState then
 begin
  if Selected[FTileX, FTileY] then
   FMouseMode := mmMultiSelectCopy else
   FMouseMode := mmMultiSelectLayerCopy;
  SetGlobalCursor(crBlack);
 end else
 if Shift = FSelectDragShiftState then
 begin
  if Selected[FTileX, FTileY] then
   FMouseMode := mmMultiSelectDrag else
   FMouseMode := mmMultiSelectLayerDrag;
  SetGlobalCursor(crBlack);
 end;
 if PasteBuffer <> nil then
 case FMouseMode of
  mmMultiSelectLayerCopy,
  mmMultiSelectCopy:
  begin
   NewBuf := MakePasteBufferCopy(PasteBuffer);
   PasteBufferApply;
   PasteBuffer := NewBuf;
   FMouseMode := mmMultiSelectCopy;
  end;
  mmMultiSelectNew,
  mmMultiSelectInclude: PasteBufferApply;
  mmMultiSelectMove,
  mmMultiSelectLayerDrag: FMouseMode := mmMultiSelectDrag;
 end;
 case FMouseMode of
  mmMultiSelectLayerCopy,
  mmMultiSelectLayerDrag:
  begin
   FillVisibilityBuffer(Pointer(FSelectionBuffer), Bounds(0, 0, FMapWidth, FMapHeight), False);
   GetVisibleRect(PasteBufRect, Pointer(FSelectionBuffer), FMapWidth, FMapHeight, 1);
   DoFillPasteBuffer(Pointer(FSelectionBuffer));
   if FMouseMode = mmMultiSelectLayerDrag then
    SelectedAreaClear(PasteBufRect, Pointer(FSelectionBuffer));
   FillTempSelection(PasteBufRect);
   FMouseRect := PasteBufRect;   
   EndDrag(False);
   BeginDrag(True);      
  end;
  mmMultiSelectMove,
  mmMultiSelectCopy,
  mmMultiSelectDrag:
  begin
   if PasteBuffer = nil then
   begin
    if FMouseMode <> mmMultiSelectMove then
    begin
     FillPasteBuffer(PasteBuffer, PasteBufRect, FMouseMode = mmMultiSelectDrag, True);
     if PasteBuffer = nil then
     begin
      FMouseMode := mmMultiSelectMove;
      PasteBufRect := FSelectionRect;         
     end else
      FillTempSelection(PasteBufRect);
    end else
     PasteBufRect := FSelectionRect;   
   end else
   begin
    W := PasteBufRect.Right - PasteBufRect.Left;
    H := PasteBufRect.Bottom - PasteBufRect.Top;
    SetLength(FTempSel, W * H);
    BufFillVisibility(FTempSel, W, H, PasteBuffer, Bounds(0, 0, W, H));
    ApplySelection(FTempSel, PasteBufRect);
   end;
   FMouseRect := PasteBufRect;
   EndDrag(False);
   BeginDrag(True);
  end;
 end;
end;

procedure TCustomTileMapView.Paint;
var
 X, Y, Right, RowStride: Integer;
 Rect: TRect;
begin
 if (FMapWidth > 0) and (FMapHeight > 0) then
 begin
  DoBeforePaint;

  Right := FClientLeft + FTileWidth;
  Rect.Top := FClientTop;
  Rect.Bottom := Rect.Top + FTileHeight;
  FInternalIndex := FViewRect.Top * FMapWidth + FViewRect.Left;
  RowStride := FMapWidth - (FViewRect.Right - FViewRect.Left);

  for Y := FViewRect.Top to FViewRect.Bottom - 1 do
  begin
   Rect.Left := FClientLeft;
   Rect.Right := Right;
   for X := FViewRect.Left to FViewRect.Right - 1 do
   begin
    DrawCellInternal(Rect, X, Y);
    Inc(Rect.Left, FTileWidth);
    Inc(Rect.Right, FTileWidth);
    Inc(FInternalIndex);
   end;
   Inc(FInternalIndex, RowStride);
   Inc(Rect.Top, FTileHeight);
   Inc(Rect.Bottom, FTileHeight);
  end;
  DoAfterPaint;  
 end;
 if Focused then
  with Canvas do
  begin
   Brush.Style := bsClear;
   Brush.Color := clWhite;
   DrawFocusRect(ClientRect);
  end;
end;

procedure TCustomTileMapView.PasteBufferApply;
var
 Change: Boolean;
begin
 Change := PasteBuffer <> nil;
 if Change then
 begin
  DoPasteBufferApply;
  ReleasePasteBuffer;
  if SomethingSelected then
   SelectionChanged;
 end;
end;

procedure TCustomTileMapView.PasteMapData(Data: Pointer; const Rect: TRect; Apply: Boolean);
begin
 if CheckCompatibility(Data) then
 begin
  PasteBufferApply;
  PasteBuffer := Data;
  PasteBufRect := Rect;
  FMouseRect := Rect;
  if Apply then Deselect else
  begin
   FSelectionMode := selMulti;
   RefreshPasteBufferSelection;
   Invalidate;
   UpdateCursor(GlobalShiftState);
  end;
 end else
 begin
  if Assigned(FOnReleasePasteBuffer) then
   FOnReleasePasteBuffer(Self, Data);
 end;
end;

procedure TCustomTileMapView.ScrollTo(CellX, CellY: Integer);
begin
 FHScrolled := False;
 FVScrolled := False;
 PostMessage(Handle, WM_HSCROLLTOVIEW, CellX, 0);
 PostMessage(Handle, WM_VSCROLLTOVIEW, CellY, 0);
 Invalidate;
end;

procedure TCustomTileMapView.ScrollToPerform(CellX, CellY: Integer);
begin
 FHScrolled := False;
 FVScrolled := False;
 Perform(WM_HSCROLLTOVIEW, CellX, 0);
 Perform(WM_VSCROLLTOVIEW, CellY, 0);
 Invalidate;
end;

procedure TCustomTileMapView.ScrollToSelectedCell;
begin
 FHScrolled := False;
 FVScrolled := False;
 PostMessage(Handle, WM_HSCROLLTOVIEW, FSelectedCell.X, 0);
 PostMessage(Handle, WM_VSCROLLTOVIEW, FSelectedCell.Y, 0);
 Invalidate;
end;

procedure TCustomTileMapView.ScrollToSelectedCellPerform;
begin
 FHScrolled := False;
 FVScrolled := False;
 Perform(WM_HSCROLLTOVIEW, FSelectedCell.X, 0);
 Perform(WM_VSCROLLTOVIEW, FSelectedCell.Y, 0);
 Invalidate; 
end;

function TCustomTileMapView.SelectAll: Boolean;
var
 Len: Integer;
begin
 Len := FMapWidth * FMapHeight;
 Result := Len > 0;
 if Result then
 begin
  FSelectionMode := selMulti;
  PasteBufferApply;
  SetLength(FSelectionBuffer, Len);
  FillChar(FSelectionBuffer[0], Len, 1);
  FillTempSelection(Bounds(0, 0, FMapWidth, FMapHeight));  
  Invalidate;
  SelectionChanged;
 end;
end;

procedure TCustomTileMapView.SelectedAreaClear(const Rect: TRect;
                                               SelectionBuffer: Pointer);
var
 SB: PBoolean;
 RowStride, X, Y: Integer;
begin
 DoBeforeContentChange(ccClearSelection);
 FInternalIndex := Rect.Top * FMapWidth + Rect.Left;
 RowStride := FMapWidth - (Rect.Right - Rect.Left);
 if SelectionBuffer <> nil then
 begin
  SB := SelectionBuffer;
  Inc(SB, FInternalIndex);
  for Y := Rect.Top to Rect.Bottom - 1 do
  begin
   for X := Rect.Left to Rect.Right - 1 do
   begin
    if SB^ then
     DoClearCell(X, Y);
    Inc(SB);
    Inc(FInternalIndex);
   end;
   Inc(SB, RowStride);
   Inc(FInternalIndex, RowStride);
  end;
 end else
 for Y := Rect.Top to Rect.Bottom - 1 do
 begin
  for X := Rect.Left to Rect.Right - 1 do
  begin
   DoClearCell(X, Y);
   Inc(FInternalIndex);
  end;
  Inc(FInternalIndex, RowStride);
 end;
 ContentsChanged;
end;

procedure TCustomTileMapView.SelectedBufAreaClear(Data: Pointer;
  const DataRect, ClearRect: TRect; SelectionBuffer: Pointer;
  SelBufferWidth: Integer);
var
 Src: PBoolean absolute SelectionBuffer;
 DstStride, XX, X, Y, WW: Integer;
 Rect: TRect;
begin
 Rect.Left := ClearRect.Left - DataRect.Left;
 Rect.Right := ClearRect.Right - DataRect.Left;
 Rect.Top := ClearRect.Top - DataRect.Top;
 Rect.Bottom := ClearRect.Bottom - DataRect.Top;
 DstStride := DataRect.Right - DataRect.Left;
 WW := Rect.Right - Rect.Left;
 XX := 0;
 if Src = nil then
 begin
  if Rect.Left < 0 then
   Inc(WW, Rect.Left) else
   Inc(XX, Rect.Left);

  if Rect.Right > DstStride then
   Dec(WW, Rect.Right - DstStride);
  if WW > 0 then
  begin
   Inc(WW, XX);
   for Y := Max(0, Rect.Top) to Min(DataRect.Bottom - DataRect.Top,
                                    Rect.Bottom) - 1 do
    for X := XX to WW - 1 do
     DoBufClearCell(X, Y, Data);
  end;
 end else
 begin
  Inc(Src, ClearRect.Left + ClearRect.Top * SelBufferWidth);
  if Rect.Left < 0 then
  begin
   Dec(Src, Rect.Left);
   Inc(WW, Rect.Left);
  end else
   Inc(XX, Rect.Left);

  if Rect.Right > DstStride then
   Dec(WW, Rect.Right - DstStride);
  if WW > 0 then
  begin
   Dec(SelBufferWidth, WW);
   Inc(WW, XX);
   for Y := Max(0, Rect.Top) to Min(DataRect.Bottom - DataRect.Top,
                                    Rect.Bottom) - 1 do
   begin
    for X := XX to WW - 1 do
    begin
     if Src^ then
      DoBufClearCell(X, Y, Data);
     Inc(Src);
    end;
    Inc(Src, SelBufferWidth);
   end;
  end;
 end;
end;

procedure TCustomTileMapView.SelectionChanged;
begin
 if not (csDesigning in ComponentState) and
    not (csLoading in ComponentState) and Assigned(FOnSelectionChanged) then
  FOnSelectionChanged(Self);
end;

procedure TCustomTileMapView.SetBorderStyle(Value: TBorderStyle);
begin
 if Value <> FBorderStyle then
 begin
  FBorderStyle := Value;
  RecreateWnd;
 end;
end;

procedure TCustomTileMapView.SetDefaultCursor(Value: TCursor);
begin
 if Cursor = FDefaultCursor then
  Cursor := Value;
 FDefaultCursor := Value;
end;

procedure TCustomTileMapView.SetGlobalCursor(ACursor: TCursor);
begin
 Screen.Cursor := ACursor;
 if ACursor = crDefault then
  Cursor := FDefaultCursor else
  Cursor := ACursor;
end;

procedure TCustomTileMapView.SetGridColor(Value: TColor);
begin
 if FGridColor <> Value then
 begin
  FGridColor := Value;
  if FShowGrid then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridMode(Value: TPenMode);
begin
 if FGridMode <> Value then
 begin
  FGridMode := Value;
  if FShowGrid then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridStyle(Value: TPenStyle);
begin
 if FGridStyle <> Value then
 begin
  FGridStyle := Value;
  if FShowGrid then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridWidth(Value: Integer);
begin
 Value := Max(1, Min(Min(FTileWidth, FTileHeight) shr 2, Value));
 if FGridWidth <> Value then
 begin
  FGridWidth := Value;
  if FShowGrid then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetMapHeight(Value: Integer);
begin
 SetMapSize(FMapWidth, Value);
end;

procedure TCustomTileMapView.SetMapSize(AWidth, AHeight: Integer; DoRepaint: Boolean);
begin
 if Assigned(FOnMapResize) then
  FOnMapResize(Self, AWidth, AHeight);
 if AWidth < 0 then AWidth := 0;
 if AHeight < 0 then AHeight := 0; 
 if (FMapWidth <> AWidth) or
    (FMapHeight <> AHeight) then
 begin
  ReleasePasteBuffer;
  Finalize(FTempSel);
  Finalize(FSelectionBuffer);
  if FSelectionMode = selMulti then
   SetLength(FSelectionBuffer, AWidth * AHeight);
  FMapWidth := AWidth;
  FMapHeight := AHeight;
  FMapSize := FMapWidth * FMapHeight;
  UpdateScrollBars(True);
  SelectionChanged;
 end;
 if DoRepaint then Invalidate;
end;

procedure TCustomTileMapView.SetMapWidth(Value: Integer);
begin
 SetMapSize(Value, FMapHeight);
end;

procedure TCustomTileMapView.SetOffsetX(Value: Integer);
var
 OldY: Integer;
 Done: Boolean;
begin
 OldY := FViewOffset.Y;
 if Assigned(FOnSetPosition) then
  FOnSetPosition(Self, Value, FViewOffset.Y);
 Done := False;
 if FViewOffset.X <> Value then
 begin
  FViewOffset.X := Value;
  UpdateHorizontalScrollBar(True);
  Done := True;
 end;
 if FViewOffset.Y <> OldY then
 begin
  UpdateVerticalScrollBar(True);
  Done := True;
 end; 
 if Done then
 begin
  UpdateCoordinates;
  Invalidate;
 end;
end;

procedure TCustomTileMapView.SetOffsetX_Internal(X: Integer);
var
 OldY: Integer;
begin
 OldY := FViewOffset.Y;
 if Assigned(FOnSetPosition) then
  FOnSetPosition(Self, X, FViewOffset.Y);
 FViewOffset.X := X;
 UpdateHorizontalScrollBar(True);
 if FViewOffset.Y  <> OldY then
  UpdateVerticalScrollBar(True);
 UpdateCoordinates;
end;

procedure TCustomTileMapView.SetOffsetX_NoRepaint(Value: Integer);
var
 W: Integer;
begin
 if Value > FRangeX then Value := FRangeX;
 if Value < 0 then Value := 0;
 FViewOffset.X := Value;
 FViewRect.Left := Value div FTileWidth;
 FViewRect.Right := Min(FMapWidth, FViewRect.Left + FViewWidth);

 FViewWidth := Max(0, (ClientWidth + FTileWidth - 1) div FTileWidth +
                              Ord(FViewOffset.X mod FTileWidth > 0));
 FViewRect.Right := Min(FMapWidth, FViewRect.Left + FViewWidth);
  
 FClientLeft := FViewRect.Left * FTileWidth - FViewOffset.X;
 if FClientLeft = 0 then
 begin
  W := ClientWidth - FViewRect.Right * FTileWidth;
  if W > 0 then
   FClientLeft := W shr 1;
 end;
end;

procedure TCustomTileMapView.SetOffsetY(Value: Integer);
var
 OldX: Integer;
 Done: Boolean;
begin
 OldX := FViewOffset.X;
 if Assigned(FOnSetPosition) then
  FOnSetPosition(Self, FViewOffset.X, Value);
 Done := False;
 if FViewOffset.X <> OldX then
 begin
  UpdateHorizontalScrollBar(True);
  Done := True;
 end;
 if FViewOffset.Y <> Value then
 begin
  FViewOffset.Y := Value;
  UpdateVerticalScrollBar(True);
  Done := True;
 end;
 if Done then
 begin
  UpdateCoordinates; 
  Invalidate;
 end;
end;

procedure TCustomTileMapView.SetOffsetY_Internal(Y: Integer);
var
 OldX: Integer;
begin
 OldX := FViewOffset.X;
 if Assigned(FOnSetPosition) then
  FOnSetPosition(Self, FViewOffset.X, Y);
 FViewOffset.Y := Y;
 if FViewOffset.X <> OldX then
  UpdateHorizontalScrollBar(True);
 UpdateVerticalScrollBar(True);
 UpdateCoordinates; 
end;

procedure TCustomTileMapView.SetOffsetY_NoRepaint(Value: Integer);
var
 H: Integer;
begin
 if Value > FRangeY then Value := FRangeY;
 if Value < 0 then Value := 0;
 FViewOffset.Y := Value;
 FViewRect.Top := Value div FTileHeight;
 FViewRect.Bottom := Min(FMapHeight, FViewRect.Top + FViewHeight);

 FViewHeight := Max(0, (ClientHeight + FTileHeight - 1) div FTileHeight +
                                Ord(FViewOffset.Y mod FTileHeight > 0));
 FViewRect.Bottom := Min(FMapHeight, FViewRect.Top + FViewHeight);

 FClientTop := FViewRect.Top * FTileHeight - FViewOffset.Y;
 if FClientTop = 0 then
 begin
  H := ClientHeight - FViewRect.Bottom * FTileHeight;
  if H > 0 then
   FClientTop := H shr 1;
 end;  
end;

procedure TCustomTileMapView.SetPosition(X, Y: Integer; DoRepaint: Boolean);
begin
 FHScrolled := True;
 FVScrolled := True;
 if Assigned(FOnSetPosition) then
  FOnSetPosition(Self, X, Y);

 if (FViewOffset.X <> X) or
    (FViewOffset.Y <> Y) then
 begin
  FViewOffset.X := X;
  FViewOffset.Y := Y;
  UpdateVerticalScrollBar(True);
  UpdateHorizontalScrollBar(True);
 end;
 if DoRepaint then Invalidate;
end;

procedure TCustomTileMapView.SetScrollBarsAllwaysVisible(Value: Boolean);
begin
 if FScrollBarsAllwaysVisible <> Value then
 begin
  FScrollBarsAllwaysVisible := Value;
  RecreateWnd;
 end;
end;

procedure TCustomTileMapView.SetSelectColor(Value: TColor);
begin
 if FSelectColor <> Value then
 begin
  FSelectColor := Value;
  if FSelectionMode <> selNone then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelected(CellX, CellY: Integer; Value: Boolean);
begin
 case FSelectionMode of
  selSingle:
  begin
   if not Value then
   begin
    FSelectedCell.X := -1;
    FSelectedCell.Y := -1;
    DrawCell(CellX, CellY);
   end else
   begin
    if CellX < 0 then
     CellX := 0 else
    if CellX >= FMapWidth then
     CellX := FMapWidth - 1;
    if CellY < 0 then
     CellY := 0 else
    if CellY >= FMapHeight then
     CellY := FMapHeight - 1;
    FSelectedCell.X := CellX;
    FSelectedCell.Y := CellY;
    ScrollToSelectedCell;
    SelectionChanged;
   end; 
  end;
  selMulti:
  if not Value then
  begin
   FSelectedCell.X := -1;
   FSelectedCell.Y := -1;
   DrawCell(CellX, CellY);
  end else
  if (CellX >= 0) and
     (CellY >= 0) and
     (CellX < FMapWidth) and
     (CellY < FMapHeight) then
  begin
   FSelectionBuffer[CellY * FMapWidth + CellX] := Value;
   FSelectedCell.X := CellX;
   FSelectedCell.Y := CellY;
   ScrollToSelectedCell;
   SelectionChanged;
  end;
 end;
end;

procedure TCustomTileMapView.SetSelectedByIndex(Index: Integer;
  Value: Boolean);
begin
 if FMapSize > 0 then
  Selected[Index mod FMapWidth, Index div FMapWidth] := Value;
end;

procedure TCustomTileMapView.SetSelectedCell(const Value: TPoint);
begin
 if FMapSize > 0 then
  Selected[Value.X, Value.Y] := True;
end;

procedure TCustomTileMapView.SetSelectedCellIndex(Value: Integer);
begin
 if FMapSize > 0 then
  Selected[Value mod FMapWidth, Value div FMapWidth] := True;
end;

procedure TCustomTileMapView.SetSelectionEnabled(Value: Boolean);
begin
 FSelectionEnabled := Value;
 if not Value then
 begin
  if FSelectionMode = selMulti then
   Cursor := FDefaultCursor;
 end;
end;

procedure TCustomTileMapView.SetSelectionMode(Value: TSelectionMode);
begin
 if FSelectionMode <> Value then
 begin
  FSelectionMode := Value;
  Finalize(FTempSel);  
  Finalize(FSelectionBuffer);
  if Value = selMulti then
   SetLength(FSelectionBuffer, FMapWidth * FMapHeight);
  PasteBufferApply;
  Invalidate;
  SelectionChanged;
 end;
end;

procedure TCustomTileMapView.SetSelectMode(Value: TPenMode);
begin
 if FSelectMode <> Value then
 begin
  FSelectMode := Value;
  if FSelectionMode <> selNone then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelectStyle(Value: TPenStyle);
begin
 if FSelectStyle <> Value then
 begin
  FSelectStyle := Value;
  if FSelectionMode <> selNone then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelectWidth(Value: Integer);
begin
 Value := Max(1, Min(Min(FTileWidth, FTileHeight) shr 2, Value));
 if FSelectWidth <> Value then
 begin
  FSelectWidth := Value;
  if FSelectionMode <> selNone then
   Invalidate;
 end;
end;

procedure TCustomTileMapView.SetShowGrid(Value: Boolean);
begin
 if FShowGrid <> Value then
 begin
  FShowGrid := Value;
  Invalidate;
 end;
end;

procedure TCustomTileMapView.SetTileHeight(Value: Integer);
begin
 SetTileSize(FTileWidth, Value);
end;

procedure TCustomTileMapView.SetTileSize(AWidth, AHeight: Integer; DoRepaint: Boolean);
var
 Value: Integer;
begin
 if Assigned(FOnChangeTileSize) then
  FOnChangeTileSize(Self, AWidth, AHeight);
 if AWidth <= 0 then AWidth := 1;
 if AHeight <= 0 then AHeight := 1;
 if (FTileWidth <> AWidth) or
    (FTileHeight <> AHeight) then
 begin
  FTileWidth := AWidth;
  FTileHeight := AHeight;
  Value := Min(AWidth, AHeight) shr 2;
  FGridWidth := Max(0, Min(Value, FGridWidth));
  FSelectWidth := Max(0, Min(Value, FSelectWidth));
  UpdateScrollBars(True);
 end;
 if DoRepaint then
  Invalidate;
end;

procedure TCustomTileMapView.SetTileWidth(Value: Integer);
begin
 SetTileSize(Value, FTileHeight);
end;

procedure TCustomTileMapView.SysKeyDown(Key: Word; Shift: TShiftState);
begin
 if FSelectionEnabled and (FSelectionMode = selMulti) then
 begin
  if FMouseMode in [mmNothing, mmMultiSelectInit] then
   UpdateCursor(Shift);
 end;
 if Assigned(FOnSysKeyDown) then
  FOnSysKeyDown(Self, Key, Shift);
end;

procedure TCustomTileMapView.SysKeyUp(Key: Word; Shift: TShiftState);
begin
 if FSelectionEnabled and (FSelectionMode = selMulti) then
 begin
   if FMouseMode in [mmNothing, mmMultiSelectInit] then
   begin
    if Selected[FTileX, FTileY] then
    begin
     if PasteBuffer = nil then
      Cursor := crSelMove else
      Cursor := crBlack;
    end else
     Cursor := crSelNew;
   end;
 end;
 if Assigned(FOnSysKeyUp) then
  FOnSysKeyUp(Self, Key, Shift);
end;

procedure TCustomTileMapView.UpdateCoordinates;
var
 pt: TPoint;
begin
 if GetCursorPos(pt) then
 begin
  pt := ScreenToClient(pt);
  if ptInRect(ClientRect, pt) then
  begin
   FCursorX := pt.X;
   FCursorY := pt.Y;
   CalculateTileXY;
   if FMouseMode = mmNothing then
    UpdateCursor(GlobalShiftState);
   Exit;
  end;
 end;
 if GetCapture = 0 then
 begin
  FCursorX := -1;
  FCursorY := -1;
  FTileX := -1;
  FTileY := -1;
 end;
end;

procedure TCustomTileMapView.UpdateCursor(Shift: TShiftState);
var
 Cur: TCursor;
begin
 if FSelectionMode = selMulti then
 begin
  Cur := FDefaultCursor;
  if FSelectionEnabled then
  begin
   if Shift = FSelectNewShiftState - MouseStates then
   begin
    if Selected[FTileX, FTileY] then
    begin
     if PasteBuffer = nil then
      Cur := crSelMove else
      Cur := crBlack;
    end else
     Cur := crSelNew;
   end else
   if Shift = FSelectDragShiftState - MouseStates then
   begin
    if PasteBuffer = nil then
    begin
     if Selected[FTileX, FTileY] then
      Cur := crSelCut else
      Cur := crSelDrag;
    end else
     Cur := crSelDrag;
   end else
   if Shift = FSelectCopyShiftState - MouseStates then
    Cur := crSelCopy else
   if Shift = FSelectIncludeShiftState - MouseStates then
    Cur := crSelInc else
   if Shift = FSelectExcludeShiftState - MouseStates then
    Cur := crSelDec;
  end;
  Cursor := Cur;
 end;
end;

procedure TCustomTileMapView.UpdateHorizontalScrollBar(DoRepaint: Boolean);
var
 ScrollInfo: TScrollInfo;
begin
 ScrollInfo.cbSize := SizeOf(TScrollInfo);
 ScrollInfo.fMask := SIF_ALL;
 (*FlatSB_*)GetScrollInfo(Handle, SB_HORZ, ScrollInfo);
 if FMapHeight <= 0 then
  FRangeX := 0 else
  FRangeX := FMapWidth * FTileWidth;
 if (FRangeX > ClientWidth) or FScrollBarsAllwaysVisible then
 begin
  (*FlatSB_*)ShowScrollBar(Handle, SB_HORZ, True);
  ScrollInfo.nMin := 0;
  ScrollInfo.nMax := FRangeX - 1;
  Dec(FRangeX, ClientWidth);
  ScrollInfo.nPos := FViewOffset.X;
  ScrollInfo.nTrackPos := FViewOffset.X;
  ScrollInfo.nPage := Max(0, ClientWidth);
  if FScrollBarsAllwaysVisible then
   ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
   ScrollInfo.fMask := SIF_ALL;
  (*FlatSB_*)SetScrollInfo(Handle, SB_HORZ, ScrollInfo, DoRepaint);
 end else
 begin
  ScrollInfo.nMin := 0;
  ScrollInfo.nMax := 0;
  ScrollInfo.nPos := 0;
  ScrollInfo.nPage := 0;
  ShowScrollBar(Handle, SB_HORZ, False);
  SetScrollInfo(Handle, SB_HORZ, ScrollInfo, False);
 end;
 SetOffsetX_NoRepaint(GetScrollPos(Handle, SB_HORZ));
end;

procedure TCustomTileMapView.UpdateScrollBars(DoRepaint: Boolean);
var
 VScrollInfo,
 HScrollInfo: TScrollInfo;
 W, H, MW, MH: Integer;
begin
 if HandleAllocated then
 begin
  if not FScrollBarsAllwaysVisible then
  begin
   if not (tmsScrollBarsChanging in FState) then
   begin
    Include(FState, tmsScrollBarsChanging);
    try  
     VScrollInfo.cbSize := SizeOf(TScrollInfo);
     VScrollInfo.fMask := SIF_ALL;
     if GetScrollInfo(Handle, SB_VERT, VScrollInfo) and
       (VScrollInfo.nPage > 0) then
      W := ClientWidth + GetSystemMetrics(SM_CXVSCROLL) else
      W := 0;
     HScrollInfo.cbSize := SizeOf(TScrollInfo);
     HScrollInfo.fMask := SIF_ALL;
     if GetScrollInfo(Handle, SB_HORZ, HScrollInfo) and
        (HScrollInfo.nPage > 0) then
      H := ClientHeight + GetSystemMetrics(SM_CYHSCROLL) else
      H := 0;
     if FMapWidth <= 0 then
      MH := 0 else
      MH := FMapHeight * FTileHeight;
     if FMapHeight <= 0 then
      MW := 0 else
      MW := FMapWidth * FTileWidth;
     if (MW <= W) and (MH <= H) then
     begin
      VScrollInfo.nMin := 0;   
      VScrollInfo.nMax := 0;
      VScrollInfo.nPos := 0;
      VScrollInfo.nPage := 0;
      ShowScrollBar(Handle, SB_VERT, False);
      SetScrollInfo(Handle, SB_VERT, VScrollInfo, False);

      HScrollInfo.nMin := 0;
      HScrollInfo.nMax := 0;
      HScrollInfo.nPos := 0;
      HScrollInfo.nPage := 0;
      ShowScrollBar(Handle, SB_HORZ, False);
      SetScrollInfo(Handle, SB_HORZ, HScrollInfo, False);
     end;
    finally
     Exclude(FState, tmsScrollBarsChanging);
    end;
   end;
   UpdateHorizontalScrollBar(DoRepaint);
  end;
  UpdateVerticalScrollBar(DoRepaint);
  UpdateHorizontalScrollBar(DoRepaint);
 end;
end;

procedure TCustomTileMapView.UpdateVerticalScrollBar(DoRepaint: Boolean);
var
 ScrollInfo: TScrollInfo;
begin
 ScrollInfo.cbSize := SizeOf(TScrollInfo);
 ScrollInfo.fMask := SIF_ALL;
 (*FlatSB_*)GetScrollInfo(Handle, SB_VERT, ScrollInfo);
 if FMapWidth <= 0 then
  FRangeY := 0 else
  FRangeY := FMapHeight * FTileHeight;
 if (FRangeY > ClientHeight) or FScrollBarsAllwaysVisible then
 begin
  (*FlatSB_*)ShowScrollBar(Handle, SB_VERT, True);
  ScrollInfo.nMin := 0;
  ScrollInfo.nMax := FRangeY - 1;
  Dec(FRangeY, ClientHeight);
  ScrollInfo.nPos := FViewOffset.Y;
  ScrollInfo.nTrackPos := FViewOffset.Y;
  ScrollInfo.nPage := Max(0, ClientHeight);
  if FScrollBarsAllwaysVisible then
   ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL else
   ScrollInfo.fMask := SIF_ALL;
  (*FlatSB_*)SetScrollInfo(Handle, SB_VERT, ScrollInfo, DoRepaint);
 end else
 begin
  ScrollInfo.nMin := 0;
  ScrollInfo.nMax := 0;
  ScrollInfo.nPos := 0;
  ScrollInfo.nPage := 0;
  ShowScrollBar(Handle, SB_VERT, False);
  SetScrollInfo(Handle, SB_VERT, ScrollInfo, False);
 end;
 SetOffsetY_NoRepaint(GetScrollPos(Handle, SB_VERT));
end;

procedure TCustomTileMapView.WMDrawCell(var Message: TMessage);
var
 CellX, CellY: Integer;
 Rect: TRect;
begin
 CellX := Message.WParam;
 CellY := Message.LParam;
 if (CellX >= 0) and (CellY >= 0) and
    (CellX < FMapWidth) and (CellY < FMapHeight) then
 begin
  FInternalIndex := CellY * FMapWidth + CellX;
  Rect := Bounds(Max(FClientLeft, 0) + (CellX * FTileWidth - FViewOffset.X),
                   Max(FClientTop, 0) + (CellY * FTileHeight - FViewOffset.Y),
                   FTileWidth, FTileHeight);
  DrawCellInternal(Rect, CellX, CellY);
  if Focused then
  begin
   with Canvas do
   begin
    Brush.Color := clWhite;
    if Rect.Left <= 0 then
     DrawFocusRect(Bounds(-1, Rect.Top, 2, FTileHeight)) else
    if Rect.Right >= ClientWidth then
     DrawFocusRect(Bounds(ClientWidth - 1, Rect.Top, 2, FTileHeight));

    if Rect.Top <= 0 then
     DrawFocusRect(Bounds(Rect.Left, -1, FTileWidth, 2)) else
    if Rect.Bottom >= ClientHeight then
     DrawFocusRect(Bounds(Rect.Left, ClientHeight - 1, FTileWidth, 2));
   end;
  end;
 end; 
end;

procedure TCustomTileMapView.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
 inherited;
 Message.Result := Message.Result or DLGC_WANTALLKEYS or
                  DLGC_WANTARROWS;
end;

procedure TCustomTileMapView.WMHScroll(var Message: TWMHScroll);
var
 SI: TScrollInfo;
begin
 case Message.ScrollCode of
  SB_LEFT:      OffsetX := 0;
  SB_RIGHT:     OffsetX := FRangeX;
  SB_ENDSCROLL: UpdateHorizontalScrollBar(False);
  SB_LINELEFT:  OffsetX := FViewOffset.X - TileWidth shr 1;
  SB_LINERIGHT: OffsetX := FViewOffset.X + TileWidth shr 1;
  SB_PAGELEFT:  OffsetX := FViewOffset.X - ClientWidth;
  SB_PAGERIGHT: OffsetX := FViewOffset.X + ClientWidth;
  SB_THUMBPOSITION,
  SB_THUMBTRACK:
  begin
   SI.cbSize := SizeOf(TScrollInfo);
   SI.fMask := SIF_TRACKPOS;
   (*FlatSB_*)GetScrollInfo(Handle, SB_HORZ, SI);
   OffsetX := SI.nTrackPos;
  end;
 end;
 Message.Result := 0;
end;

procedure TCustomTileMapView.WMHScrollToView(var Message: TMessage);
var
 X, XX: Integer;
begin
 if (Message.LParam = 0) and FHScrolled then Exit;
 if Message.WParam < 0 then Exit;
 X := Message.WParam * FTileWidth;
 if X < FViewOffset.X then SetOffsetX_Internal(X) else
 begin
  XX := FViewOffset.X + ClientWidth;
  Inc(X, FTileWidth);
  if X > XX then
   SetOffsetX_Internal(FViewOffset.X + (X - XX));
 end;
 FHScrolled := True;
end;

procedure TCustomTileMapView.WMLButtonUp(var Message: TWMLButtonUp);
begin
 if csCaptureMouse in ControlStyle then MouseCapture := False;
 if csClicked in ControlState then
 begin
  ControlState := ControlState - [csClicked];
  if PtInRect(ClientRect, SmallPointToPoint(Message.Pos)) then Click;
 end;
 if not (ssLeft in GlobalShiftState) then // prevent mouse up handler when start dragging
 begin
  if not (csNoStdEvents in ControlStyle) then
   with Message do MouseUp(mbLeft, KeysToShiftState(Keys), XPos, YPos);
 end;
end;

procedure TCustomTileMapView.WMMButtonDown(var Message: TWMRButtonDown);
begin
 SendCancelMode(Self);
 if csCaptureMouse in ControlStyle then MouseCapture := True;
 if csClickEvents in ControlStyle then ControlState := ControlState + [csClicked];
 if not (csNoStdEvents in ControlStyle) then
 with Message do if (Width > 32768) or (Height > 32768) then
 with CalcCursorPos do
  MouseDown(mbMiddle, KeysToShiftState(Keys), X, Y) else
  MouseDown(mbMiddle, KeysToShiftState(Keys), Message.XPos, Message.YPos);
end;

procedure TCustomTileMapView.WMMButtonUp(var Message: TWMRButtonUp);
begin
 if csCaptureMouse in ControlStyle then MouseCapture := False;
 if csClicked in ControlState then
 begin
  ControlState := ControlState - [csClicked];
  if PtInRect(ClientRect, SmallPointToPoint(Message.Pos)) then Click;
 end;
 if not (csNoStdEvents in ControlStyle) then
  with Message do MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
end;

procedure TCustomTileMapView.WMNCHitTest(var Message: TMessage);
begin
 DefaultHandler(Message);
end;

procedure TCustomTileMapView.WMRButtonDown(var Message: TWMRButtonDown);
begin
 SendCancelMode(Self);
 if csCaptureMouse in ControlStyle then MouseCapture := True;
 if csClickEvents in ControlStyle then ControlState := ControlState + [csClicked];
 if not (csNoStdEvents in ControlStyle) then
 with Message do if (Width > 32768) or (Height > 32768) then
 with CalcCursorPos do
  MouseDown(mbRight, KeysToShiftState(Keys), X, Y) else
  MouseDown(mbRight, KeysToShiftState(Keys), Message.XPos, Message.YPos);
end;

procedure TCustomTileMapView.WMRButtonUp(var Message: TWMRButtonUp);
begin
 if csCaptureMouse in ControlStyle then MouseCapture := False;
 if csClicked in ControlState then
 begin
  ControlState := ControlState - [csClicked];
  if PtInRect(ClientRect, SmallPointToPoint(Message.Pos)) then Click;
 end;
 if not (csNoStdEvents in ControlStyle) then
  with Message do MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
end;

procedure TCustomTileMapView.WMSize(var Message: TWMSize);
begin
 inherited;
 if not (tmsSizing in FState) then
 begin
  Include(FState, tmsSizing);
  try
   UpdateScrollBars(True);
  finally
   Exclude(FState, tmsSizing);
  end;
 end;
end;

procedure TCustomTileMapView.WMVScroll(var Message: TWMVScroll);
var
 SI: TScrollInfo;
begin
 case Message.ScrollCode of
  SB_TOP:       OffsetY := 0;
  SB_BOTTOM:    OffsetY := FRangeY;
  SB_ENDSCROLL: UpdateVerticalScrollBar(False);
  SB_LINEUP:    OffsetY := FViewOffset.Y - TileHeight shr 1;
  SB_LINEDOWN:  OffsetY := FViewOffset.Y + TileHeight shr 1;
  SB_PAGEUP:    OffsetY := FViewOffset.Y - ClientHeight;
  SB_PAGEDOWN:  OffsetY := FViewOffset.Y + ClientHeight;
  SB_THUMBPOSITION,
  SB_THUMBTRACK:
  begin
   SI.cbSize := SizeOf(TScrollInfo);
   SI.fMask := SIF_TRACKPOS;
   (*FlatSB_*)GetScrollInfo(Handle, SB_VERT, SI);
   OffsetY := SI.nTrackPos;
  end;
 end;
 Message.Result := 0;
end;

procedure TCustomTileMapView.WMVScrollToView(var Message: TMessage);
var
 Y, YY: Integer;
begin
 if (Message.LParam = 0) and FVScrolled then Exit;
 if Message.WParam < 0 then Exit;
 Y := Message.wParam * FTileHeight;
 if Y < FViewOffset.Y then SetOffsetY_Internal(Y) else
 begin
  YY := FViewOffset.Y + ClientHeight;
  Inc(Y, FTileHeight);
  if Y > YY then
   SetOffsetY_Internal(FViewOffset.Y + (Y - YY));
 end;
 FVScrolled := True;
end;

procedure TCustomTileMapView.WndProc(var Message: TMessage);
var
 Saved: TDragMode;
 Msg: Cardinal;
 Pos: TPoint;
begin
 Msg := Message.Msg;
 if (csDesigning in ComponentState) or
    ((Msg >= WM_KEYFIRST) and (Msg <= WM_KEYLAST)) then
  inherited WndProc(Message) else
 begin
  Saved := DragMode;
  DragMode := dmManual;
  if not (csDesigning in ComponentState) then
  begin
   case Msg of
    WM_LBUTTONDOWN,
    WM_RBUTTONDOWN,
    WM_MBUTTONDOWN: if not Focused then SetFocus;
    WM_TIMER:
    begin
     if (tmsTimerScroll in FState) and
        ((DragControl = Self) or
         (GetCaptureControl = Self)) then
     begin
      GetCursorPos(Pos);
      Pos := ScreenToClient(Pos);
      if (Pos.X < 0) or (Pos.X >= ClientWidth) or
         (Pos.Y < 0) or (Pos.Y >= ClientHeight) then
       MouseMove(GlobalShiftState, Pos.X, Pos.Y);
     end;
     if SomethingSelected then
     begin
      Inc(FSelectAnimShift);
      if FSelectAnimShift >= 6 then
       FSelectAnimShift := 0;
      Invalidate; 
     end;
    end;
   end;
  end;
  inherited WndProc(Message);
  DragMode := Saved;
  if (Msg = WM_LBUTTONDOWN) and (Saved = dmAutomatic) then
   BeginAutoDrag;
 end;
end;

procedure TCustomTileMapView.RefreshPasteBufferSelection;
var
 W: Integer;
 H: Integer;
begin
 W := PasteBufRect.Right - PasteBufRect.Left;
 H := PasteBufRect.Bottom - PasteBufRect.Top;
 SetLength(FTempSel, W * H);
 BufFillVisibility(FTempSel, W, H, PasteBuffer, Bounds(0, 0, W, H));
 ApplySelection(FTempSel, PasteBufRect, False);
 if ZeroMemCheck(FTempSel, Length(FTempSel)) then
  Deselect;
end;

function TCustomTileMapView.GetSelectionBuffer: PBoolean;
begin
 Result := Pointer(FSelectionBuffer);
end;

procedure TCustomTileMapView.UpdateSelection;
var
 Rect: TRect;
begin
 GetVisibleRect(Rect, FSelectionBuffer, FMapWidth, FMapHeight, 1);
 FillTempSelection(Rect);
 SelectionChanged;
 Invalidate;
end;

procedure TCustomTileMapView.SetMinGridH(Value: Integer);
begin
 FMinGridH := Value;
 Invalidate;
end;

procedure TCustomTileMapView.SetMinGridV(Value: Integer);
begin
 FMinGridV := Value;
 Invalidate;
end;

procedure TCustomTileMapView.DoBeforeContentChange(ChangeType: TChangeType);
begin
 if Assigned(FOnBeforeContentChange) then
  FOnBeforeContentChange(Self, ChangeType);
end;

{ TEmptyDragObject }

function TEmptyDragObject.GetDragCursor(Accepted: Boolean; X,
  Y: Integer): TCursor;
var
 P: TPoint;
 Rect: TRect;
begin
 if Control is TCustomTileMapView then
  with TCustomTileMapView(Control) do
   begin
    P := ScreenToClient(Point(X, Y));
    Rect := ClientRect;
    if FClientLeft > 0 then
    begin
     Rect.Left := FClientLeft;
     Rect.Right := FClientLeft + FMapWidth * FTileWidth;
    end;

    if FClientTop > 0 then
    begin
     Rect.Top := FClientTop;
     Rect.Bottom := FClientTop + FMapHeight * FTileHeight;
    end;

    if ptInRect(Rect, P) then
    begin
     Result := Cursor;
     Exit;
    end;
   end;
 Result := inherited GetDragCursor(Accepted, X, Y);
end;

{ TSelectionDragObject }

constructor TSelectionDragObject.Create(AControl: TControl);
var
 View: TCustomTileMapView absolute AControl;
begin
 inherited;
 if AControl is TCustomTileMapView then
 begin
  FData := Pointer(View.FTempSel);
  with View.FSelectionRect do
  begin
   FHotSpot.X := View.FTileX - Left;
   FHotSpot.Y := View.FTileY - Top;  
   FWidth := Right - Left;
   FHeight := Bottom - Top;
  end;
 end;
end;

{ TPasteBufferDragObject }

constructor TPasteBufferDragObject.Create(AControl: TControl);
var
 View: TCustomTileMapView absolute AControl;
begin
 inherited;
 if AControl is TCustomTileMapView then
 begin
  FData := View.PasteBuffer;
  with View.PasteBufRect do
  begin
   FHotSpot.X := View.FTileX - Left;
   FHotSpot.Y := View.FTileY - Top;
   FWidth := Right - Left;
   FHeight := Bottom - Top;
  end;
 end;
end;

{ TBaseSelDragObject }

procedure TBaseSelDragObject.Assign(Source: TDragObject);
var
 Src: TBaseSelDragObject absolute Source;
begin
 if Source is TBaseSelDragObject then
 begin
  FData := Src.FData;
  FWidth := Src.FWidth;
  FHeight := Src.FHeight;
 end else
 begin
  FData := nil;
  FWidth := 0;
  FHeight := 0;
 end;
 inherited;
end;

initialization
 Screen.Cursors[crMagicWand] := LoadCursor(HInstance, 'MAGIC_WAND');
 Screen.Cursors[crMagicInc]  := LoadCursor(HInstance, 'MAGIC_INC');
 Screen.Cursors[crMagicDec]  := LoadCursor(HInstance, 'MAGIC_DEC');
 Screen.Cursors[crSelPaste]  := LoadCursor(HInstance, 'SEL_PASTE');
 Screen.Cursors[crBrush]     := LoadCursor(HInstance, 'BRUSH');
 Screen.Cursors[crDropper]   := LoadCursor(HInstance, 'DROPPER');
 Screen.Cursors[crEraser]    := LoadCursor(HInstance, 'ERASER');
 Screen.Cursors[crFloodFill] := LoadCursor(HInstance, 'FLOODFILL');
 Screen.Cursors[crPencil]    := LoadCursor(HInstance, 'PENCIL');
 Screen.Cursors[crBlack]     := LoadCursor(HInstance, 'CUR_BLACK');
 Screen.Cursors[crSelMove]   := LoadCursor(HInstance, 'SEL_MOVE');
 Screen.Cursors[crSelCopy]   := LoadCursor(HInstance, 'SEL_COPY');
 Screen.Cursors[crSelCut]    := LoadCursor(HInstance, 'SEL_CUT');
 Screen.Cursors[crSelDrag]   := LoadCursor(HInstance, 'SEL_DRAG');
 Screen.Cursors[crSelNew]    := LoadCursor(HInstance, 'SEL_NEW');
 Screen.Cursors[crSelInc]    := LoadCursor(HInstance, 'SEL_INC');
 Screen.Cursors[crSelDec]    := LoadCursor(HInstance, 'SEL_DEC');
 Screen.Cursors[crMoveHand]  := LoadCursor(HInstance, 'MOVE_HAND');
 Screen.Cursors[crDragHand]  := LoadCursor(HInstance, 'DRAG_HAND');
finalization
 if CallWndProcHook <> 0 then UnhookWindowsHookEx(CallWndProcHook);
 if KeyboardHook <> 0 then UnhookWindowsHookEx(KeyboardHook);
 if MouseHook <> 0 then UnhookWindowsHookEx(MouseHook);
end.

