unit UltimateListEdit;

interface

uses
  SysUtils, Classes, Controls, StdCtrls, SynEditKbdHandler;

type
  TUltimateListEdit = class(TCustomControl)
  private
///
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Mega Components', [TUltimateListEdit]);
end;

end.
