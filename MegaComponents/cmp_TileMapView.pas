unit cmp_TileMapView;

interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Graphics, Forms;

const
  WM_SCROLLTOVIEW = WM_USER + 1111;
  WM_DRAWCELL = WM_USER + 2222;

type
  TSelectionMode = (selNone, selSingle, selMulti);

  TCustomTileMapView = class;
  TTileMapViewArea = class;
  TResizeEvent = procedure(Sender: TCustomTileMapView; NewWidth, NewHeight: Integer) of object;
  TActCheckEvent = procedure(Sender: TCustomTileMapView;
                             Shift: TShiftState; var Act: Boolean) of object;
  TSelectInternalMode = (simNone, simNew, simInclude, simExclude);
  TSelBtnChkEvent =  procedure(Sender: TCustomTileMapView;
                               Shift: TShiftState;
                               var SelMode: TSelectInternalMode) of object;
  TCellPosEvent = procedure(Sender: TCustomTileMapView; CellX, CellY: Integer) of object;
  TDrawCellEvent = procedure(Sender: TCustomTileMapView; const Rect: TRect;
                 CellX, CellY: Integer) of object;

//  TParentKeyEvent = procedure(Sender: TComponent; var KeyCode: Word);



  TCustomTileMapView = class(TWinControl)
   private
    FViewArea: TTileMapViewArea;

    FMapWidth: Integer;
    FMapHeight: Integer;
    FTileWidth: Integer;
    FTileHeight: Integer;

    FViewWidth: Integer;
    FViewHeight: Integer;
    FMapRect: TRect;
    FViewOffset: TPoint;
    FRangeX: Integer;
    FRangeY: Integer;

    FSelectionMode: TSelectionMode;
//    FSelectButton: TMouseButton;
    FSelectStyle: TPenStyle;
    FSelectMode: TPenMode;
    FSelectWidth: Integer;
    FSelectColor: TColor;
    FSelectNewShiftState: TShiftState;
    FSelectIncludeShiftState: TShiftState;
    FSelectExcludeShiftState: TShiftState;
    FSelectedCell: TPoint;
    FSelectionBuffer: array of Boolean;

    FGridStyle: TPenStyle;
    FGridMode: TPenMode;
    FGridWidth: Integer;
    FGridColor: TColor;
    FShowGrid: Boolean;

    FBorderStyle: TBorderStyle;
//    FDragScrollButton: TMouseButton;
    FDragScrollShiftState: TShiftState;

    FOnMapResize: TResizeEvent;
    FOnScrollButtonCheck: TActCheckEvent;
    FOnSelectButtonCheck: TSelBtnChkEvent;
    FOnCustomMouseActionCheck: TActCheckEvent;
    FOnCustomMouseAction: TCellPosEvent;
    FOnCustomMouseActionFinish: TCellPosEvent;
    FOnDrawCell: TDrawCellEvent;
    procedure SetBorderStyle(Value: TBorderStyle);
    procedure WMSize(var Message: TWMSize); message WM_SIZE;
    procedure WMNCHitTest(var Message: TMessage); message WM_NCHITTEST;
    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
    procedure WMScrollToView(var Message: TMessage); message WM_SCROLLTOVIEW;
    procedure WMDrawCell(var Message: TMessage); message WM_DRAWCELL;
    procedure CMCtl3DChanged(var Message: TMessage); message CM_CTL3DCHANGED;
    procedure SetMapHeight(Value: Integer);
    procedure SetMapWidth(Value: Integer);
    procedure SetGridColor(Value: TColor);
    procedure SetGridMode(Value: TPenMode);
    procedure SetGridStyle(Value: TPenStyle);
    procedure SetGridWidth(Value: Integer);
    procedure SetSelectColor(Value: TColor);
    procedure SetSelectMode(Value: TPenMode);
    procedure SetSelectStyle(Value: TPenStyle);
    procedure SetShowGrid(Value: Boolean);
    procedure SetSelectionMode(Value: TSelectionMode);
    procedure SetSelectWidth(Value: Integer);
    function GetSelected(CellX, CellY: Integer): Boolean;
    procedure SetSelected(CellX, CellY: Integer; Value: Boolean);
    procedure UpdateVerticalScrollBar(DoRepaint: Boolean);
    procedure UpdateHorizontalScrollBar(DoRepaint: Boolean);
    procedure UpdateScrollBars(DoRepaint: Boolean);
    function GetOffsetX: Integer;
    function GetOffsetY: Integer;
    procedure SetOffsetX(Value: Integer);
    procedure SetOffsetY(Value: Integer);
    procedure SetOffsetX_NoRepaint(Value: Integer);
    procedure SetOffsetY_NoRepaint(Value: Integer);
    procedure SetTileHeight(Value: Integer);
    procedure SetTileWidth(Value: Integer);
    procedure CalcHorizontalSizes;
    procedure CalcVerticalSizes;
    function GetCanvas: TCanvas;
   protected
    procedure Paint; virtual;
    procedure DoDrawCell(const Rect: TRect; CellX, CellY: Integer); virtual;
    procedure DrawCellInternal(CellX, CellY: Integer);
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure DoScrollButtonCheck(Shift: TShiftState; var Act: Boolean); virtual;
    procedure DoSelectButtonCheck(Shift: TShiftState;
               var SelMode: TSelectInternalMode); virtual;
    procedure DoCustomMouseActionCheck(Shift: TShiftState; var Act: Boolean); virtual;
    procedure DoCustomMouseAction(CellX, CellY: Integer); virtual;
    procedure DoCustomMouseActionFinish(CellX, CellY: Integer); virtual;
   public
    property Canvas: TCanvas read GetCanvas;
    property OffsetX: Integer read GetOffsetX write SetOffsetX;
    property OffsetY: Integer read GetOffsetY write SetOffsetY;
    property MapWidth: Integer read FMapWidth write SetMapWidth;
    property MapHeight: Integer read FMapHeight write SetMapHeight;
    property TileWidth: Integer read FTileWidth write SetTileWidth;
    property TileHeight: Integer read FTileHeight write SetTileHeight;
    property ShowGrid: Boolean read FShowGrid write SetShowGrid;
    property SelectionMode: TSelectionMode read FSelectionMode
                                          write SetSelectionMode;
    property GridStyle: TPenStyle read FGridStyle write SetGridStyle;
    property GridMode: TPenMode read FGridMode write SetGridMode;
    property GridWidth: Integer read FGridWidth write SetGridWidth;
    property GridColor: TColor read FGridColor write SetGridColor;
    property SelectStyle: TPenStyle read FSelectStyle write SetSelectStyle;
    property SelectMode: TPenMode read FSelectMode write SetSelectMode;
    property SelectWidth: Integer read FSelectWidth write SetSelectWidth;
    property SelectColor: TColor read FSelectColor write SetSelectColor;
    property SelectedCell: TPoint read FSelectedCell;
    property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle;
    
    property DragScrollShiftState: TShiftState read FDragScrollShiftState
                                              write FDragScrollShiftState;

    property Selected[CellX, CellY: Integer]: Boolean read GetSelected
                                                     write SetSelected;

    property OnDrawCell: TDrawCellEvent read FOnDrawCell write FOnDrawCell;    
    property OnMapResize: TResizeEvent read FOnMapResize write FOnMapResize;
    property OnScrollButtonCheck: TActCheckEvent read FOnScrollButtonCheck
                                                  write FOnScrollButtonCheck;
    property OnSelectButtonCheck:  TSelBtnChkEvent read FOnSelectButtonCheck
                                                  write FOnSelectButtonCheck;
    property OnCustomMouseActionCheck: TActCheckEvent read FOnCustomMouseActionCheck
                                                     write FOnCustomMouseActionCheck;
    property OnCustomMouseAction: TCellPosEvent read FOnCustomMouseAction
                                               write FOnCustomMouseAction;
    property OnCustomMouseActionFinish: TCellPosEvent read FOnCustomMouseActionFinish
                                                     write FOnCustomMouseActionFinish;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DrawCell(CellX, CellY: Integer);
    procedure SetMapSize(AWidth, AHeight: Integer); virtual;
  end;

  TTileMapView = class(TCustomTileMapView)
  published
    property OffsetX;
    property OffsetY;
    property MapWidth;
    property MapHeight;
    property TileWidth default 16;
    property TileHeight default 16;
    property ShowGrid default True;
    property SelectionMode default selSingle;
    property GridStyle;
    property GridMode default pmNot;
    property GridWidth default 1;
    property GridColor;
    property SelectStyle default psInsideFrame;
    property SelectMode default pmNot;
    property SelectWidth default 2;
    property SelectColor;
    property DragScrollShiftState default [ssMiddle];

    property OnDrawCell;
    property OnMapResize;
    property OnScrollButtonCheck;
    property OnSelectButtonCheck;
    property OnCustomMouseActionCheck;
    property OnCustomMouseAction;
    property OnCustomMouseActionFinish;

  //////////
    property Align;
    property Anchors;
    property AutoSize;
    property BevelEdges;
    property BevelInner;
    property BevelOuter;
    property BevelKind;
    property BevelWidth;
    property BorderStyle default bsSingle;
    property Constraints;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Color nodefault;
    property Ctl3D;
    property Font;
    property ParentBackground default False;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnDockDrop;
    property OnDockOver;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnResize;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
  end;

  TTMVAMouseMode = (mmNothing,
                    mmScroll,
                    mmSingleSelect,
                    mmMultiSelectNew,
                    mmMultiSelectInclude,
                    mmMultiSelectExclude,
                    mmCustom);
  TTileMapViewArea = class(TControl)
  private
    FControl: TCustomTileMapView;
    FCanvas: TCanvas;
    FMouseMode: TTMVAMouseMode;
    FMouseRect: TRect;
    FOldSel: TPoint;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure WMGetDlgCode(var Message: TWMGetDlgCode); message WM_GETDLGCODE;
    procedure WMNCMouseMove(var Message: TWMMouse); message WM_NCMOUSEMOVE;
  protected
    property Canvas: TCanvas read FCanvas;
    procedure WMRButtonDown(var Message: TWMRButtonDown); message WM_RBUTTONDOWN;
    procedure WMRButtonUp(var Message: TWMRButtonUp); message WM_RBUTTONUP;
    procedure WMMButtonDown(var Message: TWMRButtonDown); message WM_MBUTTONDOWN;
    procedure WMMButtonUp(var Message: TWMRButtonUp); message WM_MBUTTONUP;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X,
     Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;            
  public
    constructor Create(AOwner: TComponent; AControl: TCustomTileMapView); reintroduce;
    destructor Destroy; override;
  end;

implementation

uses HexUnit;

{$R cursor.res}

{ TCustomTileMapView }

procedure TCustomTileMapView.CalcHorizontalSizes;
begin
 FViewWidth := Min(FMapWidth, (FViewArea.ClientWidth + FTileWidth - 1) div FTileWidth +
                              Ord(FViewOffset.X mod FTileWidth > 0));
 FMapRect.Right := FMapRect.Left + FViewWidth;
end;

procedure TCustomTileMapView.CalcVerticalSizes;
begin
 FViewHeight := Min(FMapHeight, (FViewArea.ClientHeight + FTileHeight - 1) div FTileHeight +
                                Ord(FViewOffset.Y mod FTileHeight > 0));
 FMapRect.Bottom := FMapRect.Top + FViewHeight;
end;

procedure TCustomTileMapView.CMCtl3DChanged(var Message: TMessage);
begin
 inherited;
 if FBorderStyle = bsSingle then
  RecreateWnd;
end;

constructor TCustomTileMapView.Create(AOwner: TComponent);
begin
 inherited;
 ControlStyle := ControlStyle - [csSetCaption] +
    [csCaptureMouse, csOpaque, csReplicatable, csDisplayDragImage, csReflector];
 FBorderStyle := bsSingle;    
 FTileWidth := 16;
 FTileHeight := 16;
 FViewArea := TTileMapViewArea.Create(AOwner, Self);
 FViewArea.Parent := Self;
 FViewArea.Align := alCLient;
 FViewArea.ParentColor := True;
 DoubleBuffered := True;
 FSelectedCell.X := -1;
 FSelectedCell.Y := -1;
// FDragScrollButton := mbLeft;
 FDragScrollShiftState := [ssMiddle];
 FSelectNewShiftState := [ssLeft];
 FSelectIncludeShiftState := [ssLeft, ssCtrl];
 FSelectExcludeShiftState := [ssLeft, ssShift];
// FSelectButton := mbLeft;
end;

procedure TCustomTileMapView.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_HSCROLL or WS_VSCROLL;
  WindowClass.style := WindowClass.style or CS_HREDRAW or CS_VREDRAW;
  if FBorderStyle = bsSingle then
  begin
   if Ctl3D then
   begin
    ExStyle := ExStyle or WS_EX_CLIENTEDGE;
    Style := Style and not WS_BORDER;
   end else
    Style := Style or WS_BORDER;
  end else
   Style := Style and not WS_BORDER;
 end;
end;

procedure TCustomTileMapView.CreateWnd;
begin
 inherited;
 (*
 InitializeFlatSB(Handle);
 FlatSB_SetScrollProp(Handle, WSB_PROP_HSTYLE, FSB_ENCARTA_MODE, False);
 FlatSB_SetScrollProp(Handle, WSB_PROP_VSTYLE, FSB_ENCARTA_MODE, False);*)
 UpdateScrollBars(True);
end;

destructor TCustomTileMapView.Destroy;
begin
 FViewArea.Free;
 inherited;
end;

procedure TCustomTileMapView.DoCustomMouseAction(CellX, CellY: Integer);
begin
 if Assigned(FOnCustomMouseAction) then
  FOnCustomMouseAction(Self, CellX, CellY);
end;

procedure TCustomTileMapView.DoCustomMouseActionCheck(Shift: TShiftState;
  var Act: Boolean);
begin
 if Assigned(FOnCustomMouseActionCheck) then
  FOnCustomMouseActionCheck(Self, Shift, Act);
end;

procedure TCustomTileMapView.DoCustomMouseActionFinish(CellX, CellY: Integer);
begin
 if Assigned(FOnCustomMouseActionFinish) then
  FOnCustomMouseActionFinish(Self, CellX, CellY);
end;

procedure TCustomTileMapView.DoDrawCell(const Rect: TRect; CellX, CellY: Integer);
begin
 if Assigned(FOnDrawCell) then
  FOnDrawCell(Self, Rect, CellX, CellY) else
 with FViewArea.Canvas do
 begin
  Brush.Style := bsSolid;
  Brush.Color := Color;
  FillRect(Rect);
 end;     
end;

procedure TCustomTileMapView.DoScrollButtonCheck(Shift: TShiftState;
                                                         var Act: Boolean);
begin
 if not Act then
  Act := Shift = FDragScrollShiftState;

 if Assigned(FOnScrollButtonCheck) then
  FOnScrollButtonCheck(Self, Shift, Act);
end;

procedure TCustomTileMapView.DoSelectButtonCheck(Shift: TShiftState;
           var SelMode: TSelectInternalMode);
begin
 if SelMode = simNone then
 begin
  if FSelectionMode = selSingle then
  begin
   if Shift = FSelectNewShiftState then
    SelMode := simNew;
  end else
  if FSelectionMode = selMulti then
  begin
   if Shift = FSelectNewShiftState then
    SelMode := simNew else
   if Shift = FSelectIncludeShiftState then
    SelMode := simInclude else
   if Shift = FSelectExcludeShiftState then
    SelMode := simExclude;
  end;
 end;
 if Assigned(FOnSelectButtonCheck) then
  FOnSelectButtonCheck(Self, Shift, SelMode);
end;

procedure TCustomTileMapView.DrawCell(CellX, CellY: Integer);
begin
 PostMessage(Handle, WM_DRAWCELL, CellX, CellY);
end;

procedure TCustomTileMapView.DrawCellInternal(CellX, CellY: Integer);
var
 Rect: TRect;
begin
 Rect := Bounds(CellX * FTileWidth  - FViewOffset.X,
                CellY * FTileHeight - FViewOffset.Y,
                FTileWidth, FTileHeight);
 DoDrawCell(Rect, CellX, CellY);

 if FShowGrid then
  with FViewArea.Canvas do
  begin
   Pen.Style := FGridStyle;
   Pen.Mode := FGridMode;
   Pen.Width := FGridWidth;
   Pen.Color := FGridColor;
   Brush.Style := bsClear;
   Rectangle(Rect);
  end;

 if Selected[CellX, CellY] then
  with FViewArea.Canvas do
  begin
   Pen.Style := FSelectStyle;
   Pen.Mode := FSelectMode;
   Pen.Width := FSelectWidth;
   Pen.Color := FSelectColor;
   Brush.Style := bsClear;
   Rectangle(Rect);
  end;

 with FViewArea do
  if (FMouseMode in [mmMultiSelectNew,
                     mmMultiSelectInclude,
                     mmMultiSelectExclude]) and
     (CellX >= Min(FMouseRect.Left, FMouseRect.Right)) and
     (CellX <= Max(FMouseRect.Left, FMouseRect.Right)) and
     (CellY >= Min(FMouseRect.Top, FMouseRect.Bottom)) and
     (CellY <= Max(FMouseRect.Top, FMouseRect.Bottom)) then
   with Canvas do
   begin
    Pen.Style := psClear;
    Pen.Mode := pmNot;
    Brush.Style := bsSolid;
    Brush.Color := 0;
    Rectangle(Rect);
   end;
end;

function TCustomTileMapView.GetCanvas: TCanvas;
begin
 Result := FViewArea.Canvas;
end;

function TCustomTileMapView.GetOffsetX: Integer;
begin
 Result := FViewOffset.X;
end;

function TCustomTileMapView.GetOffsetY: Integer;
begin
 Result := FViewOffset.Y;
end;

function TCustomTileMapView.GetSelected(CellX, CellY: Integer): Boolean;
begin
 case FSelectionMode of
  selSingle:
  begin
   Result := (FSelectedCell.X = CellX) and (FSelectedCell.Y = CellY);
   Exit;
  end;
  selMulti:
  if (CellX >= 0) and (CellY >= 0) and
     (CellX < FMapWidth) and (CellY < FMapHeight) then
  begin
   Result := FSelectionBuffer[CellY * FMapWidth + CellX];
   Exit;
  end;
 end;
 Result := False;
end;

procedure TCustomTileMapView.Paint;
var
 X, Y: Integer;
begin
 for Y := FMapRect.Top to FMapRect.Bottom - 1 do
  for X := FMapRect.Left to FMapRect.Right - 1 do
   DrawCellInternal(X, Y);
end;

procedure TCustomTileMapView.SetBorderStyle(Value: TBorderStyle);
begin
 if Value <> FBorderStyle then
 begin
  FBorderStyle := Value;
  RecreateWnd;
 end;
end;

procedure TCustomTileMapView.SetGridColor(Value: TColor);
begin
 if FGridColor <> Value then
 begin
  FGridColor := Value;
  if FShowGrid then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridMode(Value: TPenMode);
begin
 if FGridMode <> Value then
 begin
  FGridMode := Value;
  if FShowGrid then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridStyle(Value: TPenStyle);
begin
 if FGridStyle <> Value then
 begin
  FGridStyle := Value;
  if FShowGrid then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetGridWidth(Value: Integer);
begin
 Value := Max(0, Min(Min(FTileWidth, FTileHeight) shr 2, Value));
 if FGridWidth <> Value then
 begin
  FGridWidth := Value;
  if FShowGrid then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetMapHeight(Value: Integer);
begin
 SetMapSize(FMapWidth, Max(1, Min(32768, Value)));
end;

procedure TCustomTileMapView.SetMapSize(AWidth, AHeight: Integer);
begin
 if (FMapWidth <> AWidth) or
    (FMapHeight <> AHeight) then
 begin
  if Assigned(FOnMapResize) then
   FOnMapResize(Self, AWidth, AHeight);
  FMapWidth := AWidth;
  FMapHeight := AHeight;
  FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetMapWidth(Value: Integer);
begin
 SetMapSize(Max(1, Min(32768, Value)), FMapHeight);
end;

procedure TCustomTileMapView.SetOffsetX(Value: Integer);
begin
 if FViewOffset.X <> Value then
 begin
  FViewOffset.X := Value;
  UpdateHorizontalScrollBar(True);
  CalcHorizontalSizes;  
  FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetOffsetX_NoRepaint(Value: Integer);
begin
 if Value > FRangeX then Value := FRangeX;
 if Value < 0 then Value := 0;
 FViewOffset.X := Value;
 FMapRect.Left := Value div FTileWidth;
 FMapRect.Right := FMapRect.Left + FViewWidth;
end;

procedure TCustomTileMapView.SetOffsetY(Value: Integer);
begin
 if FViewOffset.Y <> Value then
 begin
  FViewOffset.Y := Value;
  UpdateVerticalScrollBar(True);
  CalcVerticalSizes;
  FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetOffsetY_NoRepaint(Value: Integer);
begin
 if Value > FRangeY then Value := FRangeY;
 if Value < 0 then Value := 0;
 FViewOffset.Y := Value;
 FMapRect.Top := Value div FTileHeight;
 FMapRect.Bottom := FMapRect.Top + FViewHeight;
end;

procedure TCustomTileMapView.SetSelectColor(Value: TColor);
begin
 if FSelectColor <> Value then
 begin
  FSelectColor := Value;
  if FSelectionMode <> selNone then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelected(CellX, CellY: Integer; Value: Boolean);
var
 P: PBoolean;
 Temp: Integer;
begin
 case FSelectionMode of
  selSingle:
  begin
   if not Value then
   begin
    FSelectedCell.X := -1;
    FSelectedCell.Y := -1;
    DrawCell(CellX, CellY);
   end else
   begin
    if CellX < 0 then
     CellX := 0 else
    if CellX >= FMapWidth then
     CellX := FMapWidth - 1;
    if CellY < 0 then
     CellY := 0 else
    if CellY >= FMapHeight then
     CellY := FMapHeight - 1;
    Temp := FSelectedCell.X;
    FSelectedCell.X := CellX;
    CellX := Temp; // Old X
    Temp := FSelectedCell.Y;
    FSelectedCell.Y := CellY;
    DrawCell(CellX, Temp); // Clear old selection
    DrawCell(FSelectedCell.X, FSelectedCell.Y);
    PostMessage(Handle, WM_SCROLLTOVIEW, FSelectedCell.X, FSelectedCell.Y);
   end;
  end;
  selMulti:
  begin
   P := Addr(FSelectionBuffer[CellY * FMapWidth + CellX]);
   if P^ <> Value then
   begin
    P^ := Value;
    if not Value then
    begin
     FSelectedCell.X := -1;
     FSelectedCell.Y := -1;
    end else
    begin
     if CellX < 0 then
      CellX := 0 else
     if CellX >= FMapWidth then
      CellX := FMapWidth - 1;
     if CellY < 0 then
      CellY := 0 else
     if CellY >= FMapHeight then
      CellY := FMapHeight - 1;
     FSelectedCell.X := CellX;
     FSelectedCell.Y := CellY;
     PostMessage(Handle, WM_SCROLLTOVIEW, FSelectedCell.X, FSelectedCell.Y);
    end;
    DrawCell(CellX, CellY);
   end;
  end;
 end;
end;

procedure TCustomTileMapView.SetSelectionMode(Value: TSelectionMode);
begin
 if FSelectionMode <> Value then
 begin
  FSelectionMode := Value;
  Finalize(FSelectionBuffer);
  case Value of
   selSingle: Selected[FSelectedCell.X, FSelectedCell.Y] := True;
   selMulti: SetLength(FSelectionBuffer, FMapWidth * FMapHeight);
  end;
  FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelectMode(Value: TPenMode);
begin
 if FSelectMode <> Value then
 begin
  FSelectMode := Value;
  if FSelectionMode <> selNone then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelectStyle(Value: TPenStyle);
begin
 if FSelectStyle <> Value then
 begin
  FSelectStyle := Value;
  if FSelectionMode <> selNone then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetSelectWidth(Value: Integer);
begin
 Value := Max(0, Min(Min(FTileWidth, FTileHeight) shr 2, Value));
 if FSelectWidth <> Value then
 begin
  FSelectWidth := Value;
  if FSelectionMode <> selNone then
   FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetShowGrid(Value: Boolean);
begin
 if FShowGrid <> Value then
 begin
  FShowGrid := Value;
  FViewArea.Invalidate;
 end;
end;

procedure TCustomTileMapView.SetTileHeight(Value: Integer);
begin
 Value := Max(1, Min(32768, Value));
 if FTileWidth <> Value then
 begin
  FTileHeight := Value;
  Value := Min(FTileWidth, Value) shr 2;
  FGridWidth := Max(0, Min(Value, FGridWidth));
  FSelectWidth := Max(0, Min(Value, FSelectWidth));
  FViewOffset.Y := not FViewOffset.Y;
  OffsetY := FViewOffset.Y;
 end;
end;

procedure TCustomTileMapView.SetTileWidth(Value: Integer);
begin
 Value := Max(1, Min(32768, Value));
 if FTileWidth <> Value then
 begin
  FTileWidth := Value;
  Value := Min(Value, FTileHeight) shr 2;
  FGridWidth := Max(0, Min(Value, FGridWidth));
  FSelectWidth := Max(0, Min(Value, FSelectWidth));
  FViewOffset.X := not FViewOffset.X;
  OffsetX := FViewOffset.X;
 end;
end;

procedure TCustomTileMapView.UpdateHorizontalScrollBar(DoRepaint: Boolean);
var
 ScrollInfo: TScrollInfo;
begin
 ScrollInfo.cbSize := SizeOf(TScrollInfo);
 ScrollInfo.fMask := SIF_ALL;
 (*FlatSB_*)GetScrollInfo(Handle, SB_HORZ, ScrollInfo);
 (*FlatSB_*)ShowScrollBar(Handle, SB_HORZ, True);
 ScrollInfo.nMin := 0;
 FRangeX := FMapWidth * FTileWidth;
 ScrollInfo.nMax := FRangeX - 1;
 Dec(FRangeX, FViewArea.ClientWidth);
 ScrollInfo.nPos := FViewOffset.X;
 ScrollInfo.nTrackPos := FViewOffset.X;
 ScrollInfo.nPage := Max(0, FViewArea.ClientWidth);
 ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL;
 (*FlatSB_*)SetScrollInfo(Handle, SB_HORZ, ScrollInfo, DoRepaint);
 SetOffsetX_NoRepaint(GetScrollPos(Handle, SB_HORZ));
end;

procedure TCustomTileMapView.UpdateScrollBars(DoRepaint: Boolean);
begin
 if HandleAllocated then
 begin
  UpdateVerticalScrollBar(DoRepaint);
  UpdateHorizontalScrollBar(DoRepaint);
 end;
end;

procedure TCustomTileMapView.UpdateVerticalScrollBar(DoRepaint: Boolean);
var
 ScrollInfo: TScrollInfo;
begin
 ScrollInfo.cbSize := SizeOf(TScrollInfo);
 ScrollInfo.fMask := SIF_ALL;
 (*FlatSB_*)GetScrollInfo(Handle, SB_VERT, ScrollInfo);
 (*FlatSB_*)ShowScrollBar(Handle, SB_VERT, True);
 ScrollInfo.nMin := 0;
 FRangeY := FMapHeight * FTileHeight;
 ScrollInfo.nMax := FRangeY - 1;
 Dec(FRangeY, FViewArea.ClientHeight);
 ScrollInfo.nPos := FViewOffset.Y;
 ScrollInfo.nTrackPos := FViewOffset.Y;
 ScrollInfo.nPage := Max(0, FViewArea.ClientHeight);
 ScrollInfo.fMask := SIF_ALL or SIF_DISABLENOSCROLL;
 (*FlatSB_*)SetScrollInfo(Handle, SB_VERT, ScrollInfo, DoRepaint);
 SetOffsetY_NoRepaint(GetScrollPos(Handle, SB_VERT));
end;

procedure TCustomTileMapView.WMDrawCell(var Message: TMessage);
var
 CellX, CellY: Integer;
begin
 CellX := Message.WParam;
 CellY := Message.LParam;
 if (CellX >= 0) and (CellY >= 0) and
    (CellX < FMapWidth) and (CellY < FMapHeight) then
  DrawCellInternal(CellX, CellY);
end;

procedure TCustomTileMapView.WMHScroll(var Message: TWMHScroll);
var
 SI: TScrollInfo;
begin
 case Message.ScrollCode of
  SB_LEFT:      OffsetX := 0;
  SB_RIGHT:     OffsetX := FRangeX;
  SB_ENDSCROLL: UpdateHorizontalScrollBar(False);
  SB_LINELEFT:  OffsetX := FViewOffset.X - FTileWidth shr 1;
  SB_LINERIGHT: OffsetX := FViewOffset.X + FTileWidth shr 1;
  SB_PAGELEFT:  OffsetX := FViewOffset.X - FViewArea.ClientWidth;
  SB_PAGERIGHT: OffsetX := FViewOffset.X + FViewArea.ClientWidth;
  SB_THUMBPOSITION,
  SB_THUMBTRACK:
  begin
   SI.cbSize := SizeOf(TScrollInfo);
   SI.fMask := SIF_TRACKPOS;
   (*FlatSB_*)GetScrollInfo(Handle, SB_HORZ, SI);
   OffsetX := SI.nTrackPos;
  end;
 end;
 Message.Result := 0;
end;

procedure TCustomTileMapView.WMNCHitTest(var Message: TMessage);
begin
 DefaultHandler(Message);
end;

procedure TCustomTileMapView.WMScrollToView(var Message: TMessage);
var
 X, XX, Y, YY: Integer;
begin
 X := Message.WParam * FTileWidth;
 if X < FViewOffset.X then
 begin
  OffsetX := X;
  UpdateHorizontalScrollBar(True);
 end else
 begin
  XX := FViewOffset.X + FViewArea.ClientWidth;
  Inc(X, FTileWidth);
  if X > XX then
  begin
   OffsetX := FViewOffset.X + (X - XX);
   UpdateHorizontalScrollBar(True);
  end;
 end;
 Y := Message.LParam * FTileHeight;
 if Y < FViewOffset.Y then
 begin
  OffsetY := Y;
  UpdateVerticalScrollBar(True);
 end else
 begin
  YY := FViewOffset.Y + FViewArea.ClientHeight;
  Inc(Y, FTileHeight);
  if Y > YY then
  begin
   OffsetY := FViewOffset.Y + (Y - YY);
   UpdateVerticalScrollBar(True);
  end;
 end;
end;

procedure TCustomTileMapView.WMSize(var Message: TWMSize);
begin
 inherited;
 CalcHorizontalSizes;
 CalcVerticalSizes;
 UpdateScrollBars(True);
end;

procedure TCustomTileMapView.WMVScroll(var Message: TWMVScroll);
var
 SI: TScrollInfo;
begin
 case Message.ScrollCode of
  SB_TOP:       OffsetY := 0;
  SB_BOTTOM:    OffsetY := FRangeY;
  SB_ENDSCROLL: UpdateVerticalScrollBar(False);
  SB_LINEUP:    OffsetY := OffsetY - FTileHeight shr 1;
  SB_LINEDOWN:  OffsetY := OffsetY + FTileHeight shr 1;
  SB_PAGEUP:    OffsetY := OffsetY - FViewArea.ClientHeight;
  SB_PAGEDOWN:  OffsetY := OffsetY + FViewArea.ClientHeight;
  SB_THUMBPOSITION,
  SB_THUMBTRACK:
  begin
   SI.cbSize := SizeOf(TScrollInfo);
   SI.fMask := SIF_TRACKPOS;
   (*FlatSB_*)GetScrollInfo(Handle, SB_VERT, SI);
   OffsetY := SI.nTrackPos;
  end;
 end;
 Message.Result := 0;
end;

{ TTileMapViewArea }

constructor TTileMapViewArea.Create(AOwner: TComponent; AControl: TCustomTileMapView);
begin
 inherited Create(AOwner);
 FControl := AControl;
 FCanvas := TControlCanvas.Create;
 TControlCanvas(FCanvas).Control := Self;
// Cursor := 998;
end;

destructor TTileMapViewArea.Destroy;
begin
 if GetCaptureControl = Self then SetCaptureControl(nil);
 FCanvas.Free;
 inherited;
end;

procedure TTileMapViewArea.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 Sel: Boolean; mm: TTMVAMouseMode;
 SelMode: TSelectInternalMode;
begin
 if FMouseMode <> mmNothing then
 begin
  mm := FMouseMode;
  FMouseMode := mmNothing;
  if mm = mmSingleSelect then
   FControl.Selected[FOldSel.X, FOldSel.Y] := True else
   Invalidate;
  Exit;
 end;
 FControl.DoScrollButtonCheck(Shift, Sel);
 if Sel then
 begin
  FMouseMode := mmScroll;
  FMouseRect.Left := FControl.FViewOffset.X + X;
  FMouseRect.Top := FControl.FViewOffset.Y + Y;
  Screen.Cursor := 999; // Dragging hand
 end else
 begin
  with FControl do
  begin
   FMouseRect.Left := (FViewOffset.X + X) div FTileWidth;
   FMouseRect.Top := (FViewOffset.Y + Y) div FTileHeight;
  end;
  SelMode := simNone;
  FControl.DoSelectButtonCheck(Shift, SelMode);
  if SelMode <> simNone then with FControl do
  begin
   case FSelectionMode of
    selSingle:
    begin
     FMouseMode := mmSingleSelect;
     FOldSel.X := FSelectedCell.X;
     FOldSel.Y := FSelectedCell.Y;
     Selected[FMouseRect.Left, FMouseRect.Top] := True;
    end;
    selMulti:
    begin
     FMouseRect.Right := FMouseRect.Left;
     FMouseRect.Bottom := FMouseRect.Top;
     if SelMode = simInclude then
      FMouseMode := mmMultiSelectInclude else
     if SelMode = simExclude then
      FMouseMode := mmMultiSelectExclude else
      FMouseMode := mmMultiSelectNew;
     Self.Invalidate;
    end;
   end;
  end else
  begin
   Sel := False;
   FControl.DoCustomMouseActionCheck(Shift, Sel);
   if Sel then
   begin
    FMouseMode := mmCustom;
    FControl.DoCustomMouseAction(FMouseRect.Left, FMouseRect.Top);
   end;
  end;
 end;
 FControl.MouseDown(Button, Shift, X, Y);
end;

procedure TTileMapViewArea.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
 with FControl do
 begin
  case FMouseMode of
   mmScroll:
   begin
    OffsetX := Max(0, Min(FRangeX, FMouseRect.Left - X));
    OffsetY := Max(0, Min(FRangeY, FMouseRect.Top - Y));
   end;
   mmSingleSelect:
   begin
    FMouseRect.Left := (FViewOffset.X + X) div FTileWidth;
    FMouseRect.Top := (FViewOffset.Y + Y) div FTileHeight;
    Selected[FMouseRect.Left, FMouseRect.Top] := True;
   end;
   mmMultiSelectNew,
   mmMultiSelectInclude,
   mmMultiSelectExclude:
   begin
    if Shift = FSelectNewShiftState then
     FMouseMode := mmMultiSelectNew else
    if Shift = FSelectIncludeShiftState then
     FMouseMode := mmMultiSelectInclude else
    if Shift = FSelectExcludeShiftState then
     FMouseMode := mmMultiSelectExclude;
    FMouseRect.Right := (FViewOffset.X + X) div FTileWidth;
    FMouseRect.Bottom := (FViewOffset.Y + Y) div FTileHeight;
    PostMessage(Handle, WM_SCROLLTOVIEW, FMouseRect.Right, FMouseRect.Bottom);
    Self.Invalidate;
   end;
   mmCustom:
   begin
    FMouseRect.Left := (FViewOffset.X + X) div FTileWidth;
    FMouseRect.Top := (FViewOffset.Y + Y) div FTileHeight;   
    DoCustomMouseAction(FMouseRect.Left, FMouseRect.Top);
   end;
  end;
  MouseMove(Shift, X, Y);
 end;
end;

procedure TTileMapViewArea.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
 YY, W, L, R, T, B: Integer;
 Filler: Byte;
 P: PBoolean;
begin
 with FControl do
 if FMouseMode = mmCustom then
  DoCustomMouseActionFinish(FMouseRect.Left, FMouseRect.Top) else
 if FMouseMode in [mmMultiSelectNew,
                   mmMultiSelectInclude,
                   mmMultiSelectExclude] then
 begin
  L := Min(FMouseRect.Left, FMouseRect.Right);
  R := Max(FMouseRect.Left, FMouseRect.Right);
  T := Min(FMouseRect.Top, FMouseRect.Bottom);
  B := Max(FMouseRect.Top, FMouseRect.Bottom);

  if L < 0 then L := 0;
  if T < 0 then T := 0;

  if R >= FMapWidth then
   R := FMapWidth - 1;
  if B >= FMapHeight then
   B := FMapHeight - 1;

  W := (R + 1) - L;
  if FMouseMode = mmMultiSelectNew then
   FillChar(Pointer(FSelectionBuffer)^, Length(FSelectionBuffer), 0);
  if FMouseMode = mmMultiSelectExclude then
   Filler := 0 else
   Filler := 1;
  P := Addr(FSelectionBuffer[T * FMapWidth + L]);
  for YY := T to B do
  begin
   FillChar(P^, W, Filler);
   Inc(P, FMapWidth);
  end;
  FSelectedCell.X := FMouseRect.Right;
  FSelectedCell.Y := FMouseRect.Bottom;
  PostMessage(Handle, WM_SCROLLTOVIEW, FSelectedCell.X, FSelectedCell.Y);
  Self.Invalidate;
 end;
 Screen.Cursor := crDefault;
 FMouseMode := mmNothing;
 FControl.MouseUp(Button, Shift, X, Y);
end;

procedure TTileMapViewArea.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
 inherited;
 Message.Result := Message.Result or DLGC_WANTALLKEYS or
                  DLGC_WANTTAB or DLGC_WANTARROWS;
end;

procedure TTileMapViewArea.WMMButtonDown(var Message: TWMRButtonDown);
begin
 SendCancelMode(Self);
 if csCaptureMouse in ControlStyle then MouseCapture := True;
 if csClickEvents in ControlStyle then ControlState := ControlState + [csClicked];
 if not (csNoStdEvents in ControlStyle) then
 with Message do if (Width > 32768) or (Height > 32768) then
 with CalcCursorPos do
  MouseDown(mbMiddle, KeysToShiftState(Keys), X, Y) else
  MouseDown(mbMiddle, KeysToShiftState(Keys), Message.XPos, Message.YPos);
end;

procedure TTileMapViewArea.WMMButtonUp(var Message: TWMRButtonUp);
begin
 if csCaptureMouse in ControlStyle then MouseCapture := False;
 if csClicked in ControlState then
 begin
  ControlState := ControlState - [csClicked];
  if PtInRect(ClientRect, SmallPointToPoint(Message.Pos)) then Click;
 end;
 if not (csNoStdEvents in ControlStyle) then
  with Message do MouseUp(mbMiddle, KeysToShiftState(Keys), XPos, YPos);
end;

procedure TTileMapViewArea.WMNCMouseMove(var Message: TWMMouse);
begin
 if Message.XPos = 15 then;
end;

procedure TTileMapViewArea.WMPaint(var Message: TWMPaint);
begin
 if Message.DC <> 0 then
 begin
  FCanvas.Lock;
  try
   FCanvas.Handle := Message.DC;
   try
    FControl.Paint;
   finally
    FCanvas.Handle := 0;
   end;
  finally
   FCanvas.Unlock;
  end;
 end;
end;

procedure TTileMapViewArea.WMRButtonDown(var Message: TWMRButtonDown);
begin
 SendCancelMode(Self);
 if csCaptureMouse in ControlStyle then MouseCapture := True;
 if csClickEvents in ControlStyle then ControlState := ControlState + [csClicked];
 if not (csNoStdEvents in ControlStyle) then
 with Message do if (Width > 32768) or (Height > 32768) then
 with CalcCursorPos do
  MouseDown(mbRight, KeysToShiftState(Keys), X, Y) else
  MouseDown(mbRight, KeysToShiftState(Keys), Message.XPos, Message.YPos);
end;

procedure TTileMapViewArea.WMRButtonUp(var Message: TWMRButtonUp);
begin
 if csCaptureMouse in ControlStyle then MouseCapture := False;
 if csClicked in ControlState then
 begin
  ControlState := ControlState - [csClicked];
  if PtInRect(ClientRect, SmallPointToPoint(Message.Pos)) then Click;
 end;
 if not (csNoStdEvents in ControlStyle) then
  with Message do MouseUp(mbRight, KeysToShiftState(Keys), XPos, YPos);
end;

initialization
 Screen.Cursors[998] := LoadCursor(HInstance,'MOVE_HAND');
 Screen.Cursors[999] := LoadCursor(HInstance,'DRAG_HAND');
end.
