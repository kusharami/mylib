unit temp;

interface

implementation

//----------------- TLinkEdit ------------------------------------------------//

procedure TLinkEdit.WMLButtonDblClk(var Message: TWMLButtonDblClk);
var
 I: Integer;
 SaveTree: TCustomPropertyEditor;
begin
 With FLink do If Assigned(FList) and (FEditType = etPickString) then
 begin
  SaveTree := FTree;
  SaveTree.BeginUpdate;
  try
   I := FList.IndexOf(Text);
   If I < 0 then
   begin
    Modified := False;
    SaveTree.DoSetValue(FNode, Text);
    Font.Style := SaveTree.GetValueStyle(FNode);
    SelectAll;
   end Else
   begin
    Inc(I);
    If I >= FList.Count then I := 0;
    Text := FList[I];
    Modified := False;
    SaveTree.DoSetValue(FNode, Text);
    Font.Style := SaveTree.GetValueStyle(FNode);
    SelectAll;
   end;
  finally
   SaveTree.EndUpdate;
  end;
 end Else
 If FEditType in [etEllipsis, etEllipsisReadOnly] then FButton.Click Else
  inherited;
end;

procedure TLinkEdit.CMCancelMode(var Message: TCMCancelMode);
begin
  With Message do
  if (Sender <> Self) and (Sender <> FLink.FButton) and
     (Sender <> FLink.FPickList) then
   FLink.CloseUp(False);
end;

procedure TLinkEdit.WMKillFocus(var Message: TMessage);
var
 SaveTree: TCustomPropertyEditor;
begin
 inherited;
 FLink.CloseUp(False);
 SaveTree := FLink.FTree;
 SaveTree.BeginUpdate;
 try
  If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
  begin
   if Modified then
   begin
    Modified := False;
    SaveTree.DoSetValue(FLink.FNode, Text);
   end;
   Font.Style := SaveTree.GetValueStyle(FLink.FNode);
   SelectAll;
  end;
 finally
  SaveTree.EndUpdate;
 end;
end;

procedure TLinkEdit.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    wm_KeyDown, wm_SysKeyDown, wm_Char:
    If Assigned(FLink) then
    With FLink, TWMKey(Message) do
    begin
     if (CharCode <> 0) and Assigned(FPickList) then
     begin
       with TMessage(Message) do
        SendMessage(FPickList.Handle, Msg, WParam, LParam);
       Exit;
     end;
    end;
  end;
  inherited;
end;

constructor TLinkEdit.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
end;

procedure TLinkEdit.KeyPress(var Key: Char);
var L, LL: Integer; S, WS: WideString;
Function Find: Boolean;
var I: Integer; SU, WU: WideString;
begin
 SU := WideUpperCase(S);
 L := Length(SU);
 Result := False;
 If L < 1 then Exit;
 With FLink, FList do For I := 0 to Count - 1 do
 begin
  WU := WideUpperCase(FList[I]);
  LL := Length(WU);
  If (LL > 0) and (LL >= L) and
      CompareMem(Addr(SU[1]), Addr(WU[1]), L shl 1) then
  begin
   Result := True;
   WS := FList[I];
   Exit;
  end;
 end;
end;
begin
 If (Key = #13) or FLink.FTree.FTracking then Key := #0 Else
 With FLink do If (FEditType = etPickString) and (Ord(Key) <> VK_BACK) then
 begin
  L := SelLength;
  S := Text;
  If SelStart = Length(S) - L then
  begin
   SetLength(S, SelStart);
   S := S + Key;
   If Find then
   begin
    Delete(WS, 1, L);
    Text := S + WS;
    SelStart := L;
    SelLength := LL;
    Key := #0;
    Modified := True;
   end;
  end;
 end;
end;

procedure TLinkEdit.CMRelease(var Message: TMessage);
begin
 Free;
end;

procedure TLinkEdit.WMChar(var Message: TWMChar);
begin
 If not (Message.CharCode in [VK_ESCAPE, VK_TAB]) then inherited;
end;

procedure TLinkEdit.WMGetDlgCode(var Message: TWMGetDlgCode);
begin
 inherited;
 Message.Result := Message.Result or DLGC_WANTALLKEYS or
                   DLGC_WANTTAB or DLGC_WANTARROWS;
end;

procedure TLinkEdit.WMKeyDown(var Message: TWMKeyDown);
var
 Shift: TShiftState;
 CallInherited: Boolean;
 Key: Word;
 SaveTree: TCustomPropertyEditor;
label
 KeyAccept;
begin
 SaveTree := FLink.FTree;
 IF SaveTree.FTracking or
    Assigned(FLink.FPickList) then Exit;
 CallInherited := False;
 SaveTree.BeginUpdate;
 try
  Key := Message.CharCode;
  Case Key of
   VK_ESCAPE:
   begin
    With FLink, FTree do RefreshValue(FNode);
    Goto KeyAccept;
   end;
   VK_RETURN:
   begin
    If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
    begin
     if Modified then
     begin
      Modified := False;
      SaveTree.DoSetValue(FLink.FNode, Text);
     end;
     Font.Style := SaveTree.GetValueStyle(FLink.FNode);
     SelectAll;
    end;
    Goto KeyAccept;
   end;
   VK_UP, VK_DOWN, VK_PRIOR, VK_NEXT, VK_TAB:
   begin KeyAccept:
    Shift := KeyDataToShiftState(Message.KeyData);
    SaveTree.FAcceptError := False;
    if Key <> VK_TAB then with SaveTree do
    begin // Because it can damage the FLink, it was saved before
     if (((Key = VK_UP) or (Key = VK_PRIOR)) and (GetFirst <> FocusedNode)) or
        (((Key = VK_DOWN) or (Key = VK_NEXT)) and (GetLast <> FocusedNode)) then
      Windows.SetFocus(Handle);
    end else
    begin
     If not (FLink.FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
     begin
      if Modified then
      begin
       Modified := False;
       SaveTree.DoSetValue(FLink.FNode, Text);
      end;
      Font.Style := SaveTree.GetValueStyle(FLink.FNode);
      SelectAll;
     end;
    end;
    with SaveTree do if (Key = VK_TAB) or not FAcceptError then
    begin
     If Shift = [] then
      PostMessage(Handle, WM_KEYDOWN, Key, 0);
    end;
   end;
   Else CallInherited := True;
  end;
 finally
  SaveTree.EndUpdate;
 end;
 if CallInherited then inherited;
end;

procedure TLinkEdit.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := ExStyle and not WS_EX_CLIENTEDGE;
 end;
end;

procedure TLinkEdit.Release;
begin
 if HandleAllocated then
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

procedure TLinkEdit.Click;
begin
 If Assigned(FLink) then With FLink do
 If (FEditType = etPickString) and Assigned(FPickList) then CloseUp(False);
end;

//---------------------------- TLinkButton -----------------------------------//

procedure TLinkButton.Click;
var
 S: WideString;
 SaveTree: TCustomPropertyEditor;
begin
 If Assigned(FLink) then With FLink do
 begin
  SaveTree := FTree;
  If FEditType = etPickString then
  begin
   If not Assigned(FPickList) then
    DropDown Else
    CloseUp(False);
  end Else
  begin
   SaveTree.BeginUpdate;
   try
    if FEdit.Modified then
    begin
     FEdit.Modified := False;
     SaveTree.DoSetValue(FNode, FEdit.Text);
    end;
    FEdit.Font.Style := SaveTree.GetValueStyle(FNode);
    FEdit.SelectAll;    
    if SaveTree.EllipsisClick(FNode) then
    begin
     Invalidate;
     SaveTree.DoGetValue(FNode, S);
     FEdit.Text := S;
     FEdit.SelectAll;
    end;
   finally
    SaveTree.EndUpdate
   end;
  end;
 end;
 inherited;
end;

constructor TLinkButton.Create(Link: TPropertyEditLink);
begin
 inherited Create(nil);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
end;

//---------------------------- TPopupListBox --------------------------------//

procedure TPopupListBox.CreateWnd;
begin
  inherited CreateWnd;
  Windows.SetParent(Handle, 0);
  CallWindowProc(DefWndProc, Handle, wm_SetFocus, 0, 0);
end;

procedure TPopupListBox.DoExit;
begin
 FLink.CloseUp(False);
end;

procedure TPopupListBox.WMKeyDown(var Message: TWMKeyDown);
var Key: Word;
begin
 Key := Message.CharCode;
 Case Key of
  VK_ESCAPE, VK_RETURN, VK_TAB: FLink.CloseUp(Key = VK_RETURN);
  Else inherited;
 end;
end;

constructor TPopupListBox.Create(Link: TPropertyEditLink);
begin
 inherited Create(NIL);
 ShowHint := False;
 ParentShowHint := False;
 FLink := Link;
 DoubleBuffered := True;
end;

procedure TPopupListBox.CreateParams(var Params: TCreateParams);
begin
 inherited;
 with Params do
 begin
  Style := Style or WS_BORDER;
  ExStyle := WS_EX_TOOLWINDOW or WS_EX_TOPMOST;
  AddBiDiModeExStyle(ExStyle);
  WindowClass.Style := CS_SAVEBITS;
 end;
end;

procedure TPopupListBox.MouseUp(Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 FLink.CloseUp(True);
end;

//---------------------------- TPropertyEditLink ---------------------------//

constructor TPropertyEditLink.Create(EditType: TEditType; Tree: TCustomPropertyEditor);
var X, Y: Integer; Bitmap: TBitmap;
begin
 FTree := Tree;
 FEditType := EditType;
 If EditType in [etPickString, etEllipsis, etEllipsisReadOnly] then
 begin
  FButton := TLinkButton.Create(Self);
  With FButton do
  begin
   Visible := False;
   Bitmap := TBitmap.Create;
   try
    With Bitmap do
    begin
     Width := 11;
     Height := 11;
     With Canvas do
     begin
      Brush.Color := $FF00FF;
      Brush.Style := bsSolid;
      FillRect(ClipRect);
      If EditType = etPickString then
      begin
       For Y := 0 to 3 do For X := Y + 2 to 8 - Y do
        Pixels[X, Y + 4] := clBlack;
       FList := TTntStringList.Create;
      end Else
      For Y := 4 to 5 do
      begin
       For X := 2 to 3 do Pixels[X, Y] := clBlack;
       For X := 5 to 6 do Pixels[X, Y] := clBlack;
       For X := 8 to 9 do Pixels[X, Y] := clBlack;
      end;
     end;
     Transparent := True;
     TransparentColor := $FF00FF;
    end;
    Glyph := Bitmap;
   finally
    Bitmap.Free;
   end;
  end;
 end;
 if FTree.FLinkEdit = nil then
 begin
  FEdit := TLinkEdit.Create(Self);
  FTree.FLinkEdit := FEdit;
 end else
 begin
  FEdit := FTree.FLinkEdit;
  FEdit.FLink := Self;
 end;
 with FEdit do
 begin
  Visible := False;
  BorderStyle := bsSingle;
  AutoSize := False;
 end;
end;

destructor TPropertyEditLink.Destroy;
var
 SaveTree: TCustomPropertyEditor;
begin
 SaveTree := FTree;
 SaveTree.FPropertyEditLink := nil;
 If Assigned(FList) then FList.Free;
 If Assigned(FButton) then FButton.Free;
 SaveTree.BeginUpdate;
 try
  if FEdit.Modified then
  begin
   FEdit.Modified := False;
   SaveTree.DoSetValue(FNode, FEdit.Text);
  end;
  FEdit.Hide;
 finally
  SaveTree.EndUpdate;
 end;
 inherited;
end;

function TPropertyEditLink.BeginEdit: Boolean;
begin
 Result := not FStopping;
 if Result then
 begin
  FEdit.Show;
  FEdit.SelectAll;
  if FTree.Focused then FEdit.SetFocus;
  If Assigned(FButton) then
  begin
   FButton.Parent := FTree;
   FButton.Show;
  end;
 end;
end;

function TPropertyEditLink.CancelEdit: Boolean;
begin
 Result := not FStopping;
 If Result then
 begin
  FStopping := True;
  FEdit.Hide;
  If Assigned(FButton) then FButton.Hide;
  FTree.CancelEditNode;
  FEdit.FLink := nil;
  If Assigned(FButton) then FButton.FLink := NIL;
 end;
end;

function TPropertyEditLink.EndEdit: Boolean;
var
 SaveTree: TCustomPropertyEditor;
begin
 SaveTree := FTree;
 SaveTree.BeginUpdate;
 try
  Result := not FStopping;
  if Result then
  try
   FStopping := True;
   if FEdit.Modified then
   begin
    FEdit.Modified := False;
    SaveTree.DoSetValue(FNode, FEdit.Text);
   end;
   FEdit.Hide;
   FEdit.FLink := nil;
   If Assigned(FButton) then
   begin
    FButton.Hide;
    FButton.FLink := NIL;
   end;
  except
   FStopping := False;
   raise;
  end;
 finally
  SaveTree.EndUpdate;
 end;
end;

function TPropertyEditLink.GetBounds: TRect;
begin
 Result := FEdit.BoundsRect;
 If Assigned(FButton) then Result.Right := FButton.BoundsRect.Right;
end;

function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;
var Txt: WideString; List: TTntStrings; FAU: Boolean;
begin
 Result := Tree is TCustomPropertyEditor;
 If Result then
 begin
  FTree := Tree as TCustomPropertyEditor;
  FNode := Node;
  FTree.DoGetValue(Node, Txt);
  If FEditType in [etStringReadOnly, etEllipsisReadOnly] then
  begin
   FEdit.ReadOnly := True;
   FEdit.Color := clBtnFace;
  end else
  begin
   FEdit.ReadOnly := False;
   FEdit.Color := clWindow;
  end;
  If FEditType = etPickString then
  begin
   FTree.DoGetPickList(Node, List, FAU);
   If Assigned(List) then
   begin
    FList.Assign(List);
    If FAU then List.Free;
   end;
  end;
  FEdit.Font.Color := clWindowText;
  FEdit.Font.Style := FTree.GetValueStyle(Node);
  FEdit.Parent := Tree;
  FEdit.RecreateWnd;
  FEdit.HandleNeeded;
  FEdit.Text := Txt;
 end;
end;

procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
 FEdit.WindowProc(Message);
end;

procedure TPropertyEditLink.SetBounds(R: TRect);
var Dummy: Integer; RR: TRect;
begin
 FTree.Header.Columns.GetColumnBounds(1, Dummy, R.Right);
 If Assigned(FPickList) then CloseUp(False);
 If Assigned(FButton) then
 begin
  RR.Right := R.Right;
  Dec(R.Right, 17);
  RR.Left := R.Right;
  RR.Top := R.Top;
  RR.Bottom := R.Bottom;
  FButton.BoundsRect := RR;
 end;
 FEdit.BoundsRect := R;
end;

procedure TPropertyEditLink.CloseUp(Accept: Boolean);
var
 S: WideString;
 SaveTree: TCustomPropertyEditor;
begin
 if Assigned(FPickList) then
 begin
  SaveTree := FTree;
  SaveTree.BeginUpdate;
  try
   with FPickList do
   begin
    If Accept and (ItemIndex >= 0) then
    begin
     S := Items.Strings[ItemIndex];
     FEdit.Text := S;
     FEdit.Modified := False;
     SaveTree.DoSetValue(FNode, S);
    end;
    FEdit.SelectAll;
    FreeAndNil(FPickList);
    If FEdit.Visible then FEdit.SetFocus;
   end;
  finally
   SaveTree.EndUpdate;
  end;
 end;
end;

procedure TPropertyEditLink.DropDown;
var R: TRect; P: TPoint; I, J: Integer; SZ: TSize; S: WideString;
begin
 If not Assigned(FPickList) then
 begin
  FPickList := TPopupListBox.Create(Self);
  With FPickList do
  begin
   FEdit.SelStart := FEdit.SelLength;
   Visible := False;
   Parent := FLink.FTree;
   If Assigned(FList) then Items.Assign(FList);
   With FButton.BoundsRect do
   begin
    R.Right := Right;
    R.Top := Bottom;
   end;
   With FEdit.BoundsRect do
   begin
    P := Ftree.ClientToScreen(Point(Left, R.Top));
    R.Left := Left;
    R.Bottom := R.Top + 16;
   end;
   Inc(R.Right);
   Inc(R.Top);
   ClientWidth := R.Right - R.Left;
   If Count > 0 then
   begin
    IntegralHeight := True;
    ItemIndex := Items.IndexOf(FEdit.Text);
   end;
   If Count > 0 then
    ClientHeight := ItemHeight * Count Else
    ClientHeight := ItemHeight;
   J := ClientWidth;
   for I := 0 to Count - 1 do
   begin
    S := Items[I];
    SZ.cx := 0;
    SZ.cy := 0;
    GetTextExtentPoint32W(Canvas.Handle, PWideChar(S), Length(S), SZ);
    if SZ.cx + 4 > J then J := SZ.cx + 4;
   end;
   Dec(P.X, J - ClientWidth);
   ClientWidth := J;
   With P do
   begin
    If Y + Height > Screen.Height then Dec(Y, Height);
    SetWindowPos(Handle, HWND_TOP, X, Y, 0, 0,
      SWP_NOSIZE or SWP_NOACTIVATE or SWP_SHOWWINDOW);
   end;
   Invalidate;
   Windows.SetFocus(Handle);
  end;
 end;
end;

//----------------- TCustomPropertyEditor ------------------------------------//

Procedure TCustomPropertyEditor.DoChange(Node: PVirtualNode);
begin
 inherited;
 If Assigned(Node) then
  PostMessage(Self.Handle, WM_STARTEDITING, Integer(Node), 0);
end;

function TCustomPropertyEditor.DoCreateEditor(Node: PVirtualNode; Column: TColumnIndex): IVTEditLink;
begin
 FPropertyEditLink := TPropertyEditLink.Create(DoGetEditType(Node), Self);
 Result := FPropertyEditLink;
end;

constructor TCustomPropertyEditor.Create(AOwner: TComponent);
var
 M: TVTMiscOptions;
 P: TVTPaintOptions;
 S: TVTSelectionOptions;
 A: TVTAutoOptions;
begin
 inherited;
 ClipboardFormats.Clear;
 Colors.BorderColor := clWindowText;
 Colors.GridLineColor := clBtnShadow;
 Colors.TreeLineColor := clBtnShadow;
 With Header do
 begin
  AutoSizeIndex := -1;
  With Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 0;
   Margin := 0;
   PropertyWidth := 98;
  end;
  With Columns.Add do
  begin
   Text := '';
   Options := [coEnabled, coResizable, coParentColor, coParentBidiMode,
               coVisible, coFixed];
   Position := 1;
   Margin := 0;
  end;
  MainColumn := 0;
  Options := [hoAutoResize];
 end;
 FocusedColumn := 0;
 ScrollBarOptions.ScrollBars := ssVertical;
 Indent := 12;
 DragOperations := [];
 LineStyle := lsSolid;
 TextMargin := 2;
 Margin := 0;
 FValueTextColor := clNavy;
 Color := clBtnFace;
 With TreeOptions do
 begin
  A := AutoOptions;
  Exclude(A, toAutoScrollOnExpand);
  AutoOptions := A;
  M := MiscOptions;
  Include(M, toEditable);
  Include(M, toGridExtensions);
  P := PaintOptions;
  Include(P, toHideFocusRect);
  Include(P, toShowHorzGridLines);
  Include(P, toShowVertGridLines);
  Exclude(P, toShowTreeLines);
  Include(P, toAlwaysHideSelection);
  S := SelectionOptions;
  Include(S, toFullRowSelect);
  MiscOptions := M;
  PaintOptions := P;
  SelectionOptions := S;
 end;
end;

procedure TCustomPropertyEditor.DoCanEdit(Node: PVirtualNode;
  Column: TColumnIndex; var Allowed: Boolean);
begin
 inherited;
 If Column = 0 then Allowed := False;
end;

procedure TCustomPropertyEditor.DoGetText(Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var Text: WideString);
begin
 If Node <> NIL then
 Case Column of
  0: DoGetPropertyName(Node, Text);
  1: DoGetValue(Node, Text);
 end;
end;

procedure TCustomPropertyEditor.KeyDown(var Key: Word; Shift: TShiftState);
begin
 If not FTracking then
  inherited;
end;

procedure TCustomPropertyEditor.WMStartEditing(var Message: TMessage);
var
  Node: PVirtualNode;
begin
 Node := Pointer(Message.WParam);
 if Node = nil then Exit;
 EditNode(Node, 1);
end;

procedure TCustomPropertyEditor.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 inherited;
 If (Cursor = crHSplit) and (Button = mbLeft) then
 begin
  DoStateChange([tsSizing]);
  FTracking := True;
  FTrackPos := Header.Columns.Items[0].Width - X;
 end Else
 begin
  If Assigned(FPropertyEditLink) then
   With FPropertyEditLink do If Assigned(FPickList) then CloseUp(False);
 end;
end;

function TCustomPropertyEditor.GetOptions: TStringTreeOptions;
var O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 Result := O as TStringTreeOptions;
end;

procedure TCustomPropertyEditor.SetOptions(const Value: TStringTreeOptions);
var O: TCustomStringTreeOptions;
begin
 O := inherited TreeOptions;
 O.Assign(Value);
end;

function TCustomPropertyEditor.GetOptionsClass: TTreeOptionsClass;
begin
 Result := TStringTreeOptions;
end;

procedure TCustomPropertyEditor.Click;
begin
 if not FTracking then
 inherited;
end;

procedure TCustomPropertyEditor.ForceEdit(Node: PVirtualNode);
begin
 If not IsEditing then
  PostMessage(Self.Handle, WM_STARTEDITING, Integer(Node), 0);
end;

procedure TCustomPropertyEditor.HandleMouseDown(var Message: TWMMouse;
  const HitInfo: THitInfo);
begin
 If FTracking then Exit;
 BeginUpdate;
 try
  FHitInfo := HitInfo;
//  if not ((hiOnItemButton in HitInfo.HitPositions) and (vsHasChildren in HitInfo.HitNode.States)) then
  FMouseDown := True;
  inherited;
  FMouseDown := False;
  //if (hiOnItemButton in HitInfo.HitPositions) and (vsHasChildren in HitInfo.HitNode.States) then
  ForceEdit(HitInfo.HitNode);
 finally
  EndUpdate;
 end;
end;

procedure TCustomPropertyEditor.SetFocus;
begin
 If IsEditing then
 begin
  With FPropertyEditLink do
  begin
   If Assigned(FPickList) then
    CloseUp(False) Else
   If FEdit.Visible then
    FEdit.SetFocus;
  end;
 end Else inherited;
end;

function TCustomPropertyEditor.DoEndEdit: Boolean;
begin
 If not FMouseDown or (FHitInfo.HitNode <> nil) then
  Result := inherited DoEndEdit Else
  Result := False;
end;

function TCustomPropertyEditor.GetPropWidth: Integer;
begin
 Result := Header.Columns.Items[0].Width;
end;

function TCustomPropertyEditor.GetValueWidth: Integer;
begin
 Result := Header.Columns.Items[1].Width;
end;

procedure TCustomPropertyEditor.SetPropWidth(Value: Integer);
begin
 Header.Columns.Items[0].Width := Value;
end;

procedure TCustomPropertyEditor.SetValueWidth(Value: Integer);
begin
 Header.Columns.Items[1].Width := Value;
end;

procedure TCustomPropertyEditor.MouseMove(Shift: TShiftState; X,
  Y: Integer);
var W: Integer;
begin
 If FTracking then
 begin
  W := X - FTrackPos;
  Header.Columns.Items[0].Width := Max(30, Min(W, ClientWidth - 30));
 end Else
 begin
  W := Header.Columns.Items[0].Width;
  If (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit Else
   Cursor := crDefault;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.MouseUp(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var W: Integer;
begin
 If FTracking then
 begin
  DoStateChange([], [tsSizing]);
  FTracking := False;
  W := Header.Columns.Items[0].Width;
  If (X >= W - 4) and (X <= W + 4) then
   Cursor := crHSplit Else
   Cursor := crDefault;
 end;
 inherited;
end;

procedure TCustomPropertyEditor.Resize;
begin
 PropertyWidth := Max(30, PropertyWidth);
 inherited;
end;

procedure TCustomPropertyEditor.DoGetPropertyName(Node: PVirtualNode;
  var Text: WideString);
begin
 If Assigned(FOnGetPropertyName) then
  FOnGetPropertyName(Self, Node, Text) Else
  Text := DefaultText;
end;

procedure TCustomPropertyEditor.DoGetValue(Node: PVirtualNode;
  var Text: WideString);
begin
 If Assigned(FOnGetValue) then
  FOnGetValue(Self, Node, Text) Else
  Text := DefaultText;
end;

procedure TCustomPropertyEditor.DoGetPickList(Node: PVirtualNode;
  var List: TTntStrings; var FreeAfterUse: Boolean);
begin
 List := NIL;
 FreeAfterUse := False;
 If Assigned(FOnGetPickList) then
  FOnGetPickList(Self, Node, List, FreeAfterUse);
end;

procedure TCustomPropertyEditor.DoSetValue(Node: PVirtualNode;
  const Text: WideString);
begin
 FAcceptError := False;
 If Assigned(FOnSetValue) then FOnSetValue(Self, Node, Text);
end;

function TCustomPropertyEditor.DoGetEditType(Node: PVirtualNode): TEditType;
begin
 Result := etStringReadOnly;
 If Assigned(FOnGetEditType) then
  FOnGetEditType(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DoScroll(DeltaX, DeltaY: Integer);
begin
 If tsEditing in treeStates then With FPropertyEditLink do
 begin
  If Assigned(FPickList) then CloseUp(False);
  If Assigned(FButton) then
  begin
   FButton.Top := FEdit.Top;
   FButton.Height := FEdit.Height;
  end;
 end;
 inherited;
end;

function TCustomPropertyEditor.EllipsisClick(Node: PVirtualNode): Boolean;
begin
 Result := False;
 If Assigned(FOnEllipsisClick) then
  FOnEllipsisClick(Self, Node, Result);
end;

procedure TCustomPropertyEditor.DoPaintText(Node: PVirtualNode;
  const Canvas: TCanvas; Column: TColumnIndex; TextType: TVSTTextType);
begin
 With Canvas.Font do If Column = 1 then
 begin
  Color := GetValueColor(Node);
  Style := GetValueStyle(Node);
 end Else
 begin
  Color := GetPropertyColor(Node);
  Style := GetPropertyStyle(Node);
 end;
 inherited;
end;

procedure TCustomPropertyEditor.SetValTextColor(Col: TColor);
begin
 FValueTextColor := Col;
 Invalidate;
end;

procedure TCustomPropertyEditor.SetValStyle(Style: TFontStyles);
begin
 FValueTextStyle := Style;
 Invalidate;
end;

procedure TCustomPropertyEditor.RefreshValue(Node: PVirtualNode);
var S: WideString;
begin
 if FPropertyEditLink <> nil then with FPropertyEditLink do
 begin
  FTree.DoGetValue(Node, S);
  FEdit.Font.Style := GetValueStyle(Node);
  FEdit.Text := S;
  FEdit.SelectAll;
 end;
end;

function TCustomPropertyEditor.GetPropertyColor(Node: PVirtualNode): TColor;
begin
 Result := Font.Color;
 If Assigned(FOnGetPropertyColor) then
  FOnGetPropertyColor(Self, Node, Result);
end;

function TCustomPropertyEditor.GetPropertyStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := Font.Style;
 If Assigned(FOnGetPropertyStyle) then
  FOnGetPropertyStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueStyle(Node: PVirtualNode): TFontStyles;
begin
 Result := FValueTextStyle;
 If Assigned(FOnGetValueStyle) then
  FOnGetValueStyle(Self, Node, Result);
end;

function TCustomPropertyEditor.GetValueColor(Node: PVirtualNode): TColor;
begin
 Result := FValueTextColor;
 If Assigned(FOnGetValueColor) then
  FOnGetValueColor(Self, Node, Result);
end;

procedure TCustomPropertyEditor.CMVisibleChanged(var Message: TMessage);
begin
 inherited;
 If Message.WParam = 1 then ForceEdit(FocusedNode);
end;

procedure TCustomPropertyEditor.CMEnabledChanged(var Message: TMessage);
begin
 inherited;
 If Enabled then ForceEdit(FocusedNode);
end;

function TCustomPropertyEditor.DoCancelEdit: Boolean;
begin
 If not FDoNotCancel then Result := inherited DoCancelEdit Else
 begin
  Result := False;
  FDoNotCancel := False;
 end;
end;

procedure TCustomPropertyEditor.AcceptEdit;
begin
 If IsEditing then with FPropertyEditLink, FEdit do
 If not (FEditType in [etStringReadOnly, etEllipsisReadOnly]) then
 begin
  BeginUpdate;
  try
   Modified := False;
   DoSetValue(FNode, Text);
   Font.Style := GetValueStyle(FNode);
   SelectAll;
  finally
   EndUpdate;
  end;
 end;
end;

procedure TCustomPropertyEditor.GetRoute(Node: PVirtualNode; var Route: TIntegerList);
var
 I, J: Integer;
begin
  if Node <> nil then
  begin
   I := 0;
   while Node <> RootNode do
   begin
    SetLength(Route, I + 1);
    Route[I] := Node.Index;
    Inc(I);
    Node := Node.Parent;
   end;
   J := Length(Route);
   asm
    push esi
    push edi
    mov ecx,[J]
    mov esi,[Route]
    mov esi,[esi]    
    lea edi,[esi+ecx*4-4]
    shr ecx,1
    jz @exit
    std
   @exchg:
    mov eax,[edi]
    xchg eax,[esi]
    stosd
    add esi,4
    loop @exchg
   @exit:
    cld
    pop edi
    pop esi
   end;
  end;
end;

destructor TCustomPropertyEditor.Destroy;
begin
 if FLinkEdit <> nil then
  FLinkEdit.Release;
 inherited;
end;

procedure TCustomPropertyEditor.DoFocusChange(Node: PVirtualNode;
  Column: TColumnIndex);
begin
 ForceEdit(Node);
 inherited;
end;

procedure TCustomPropertyEditor.HandleMouseDblClick(var Message: TWMMouse;
  const HitInfo: THitInfo);
begin
 If FTracking then Exit;
 BeginUpdate;
 try
  FHitInfo := HitInfo;
  FMouseDown := True;
  inherited;
  FMouseDown := False;
  ForceEdit(HitInfo.HitNode);
 finally
  EndUpdate;
 end;
end;

end.
 