object Form1: TForm1
  Left = 230
  Top = 185
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LocLabel1: TLocLabel
    Left = 408
    Top = 120
    Width = 50
    Height = 13
    Caption = 'LocLabel1'
    LocCaption = 'TestCaption'
    LocHint = 'TestHint'
    Localizer = Localizer1
  end
  object LangLabel: TTntLabel
    Left = 416
    Top = 232
    Width = 121
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'LangLabel'
  end
  object TntButton1: TTntButton
    Left = 224
    Top = 168
    Width = 75
    Height = 25
    Action = Action1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
  end
  object TntStatusBar1: TTntStatusBar
    Left = 0
    Top = 447
    Width = 862
    Height = 19
    AutoHint = True
    Panels = <
      item
        Width = 50
      end>
  end
  object LocButton1: TLocButton
    Left = 128
    Top = 320
    Width = 75
    Height = 25
    Hint = 'erryry'
    Caption = 'Qwertyuiop'
    TabOrder = 2
    OnClick = Action1Execute
    LocCaption = 'TestCaption'
    LocHint = 'TestHint'
    Localizer = Localizer1
  end
  object SpinEdit1: TSpinEdit
    Left = 416
    Top = 248
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 0
    OnChange = SpinEdit1Change
  end
  object LocActionList1: TLocActionList
    Localizer = Localizer1
    Left = 184
    Top = 112
    object Action1: TLocAction
      Caption = #12450#12463#12471#12519#12531
      Hint = #12498#12531#12488
      OnExecute = Action1Execute
      LocCaption = 'TestCaption'
      LocHint = 'TestHint'
    end
  end
  object Localizer1: TLocalizer
    Left = 72
    Top = 64
  end
end
