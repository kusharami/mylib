unit test;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TntStdCtrls, LocClasses, ActnList, LocActnList,
  ComCtrls, TntComCtrls, VirtualTrees, TntSysUtils, Spin;

type
  TForm1 = class(TForm)
    LocActionList1: TLocActionList;
    Action1: TLocAction;
    Localizer1: TLocalizer;
    TntButton1: TTntButton;
    TntStatusBar1: TTntStatusBar;
    LocButton1: TLocButton;
    LocLabel1: TLocLabel;
    SpinEdit1: TSpinEdit;
    LangLabel: TTntLabel;
    procedure FormCreate(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
//Var S: WideString;
begin
 Localizer1.LanguageList.LoadFromFile(
 WideExtractFilePath(ParamStr(0)) + 'lang.ini');
{ With Localizer1.LanguageList.AddLanguage('English') do
 begin
  AddString('TestCaption', 'Button');
  AddString('TestHint', 'Hint');
 end;
 With Localizer1.LanguageList.AddLanguage('Russian') do
 begin
  AddString('TestCaption', '������');
  AddString('TestHint', '���������');
 end;
 With Localizer1.LanguageList.AddLanguage('Japanese') do
 begin
  SetLength(S, 3);
  S[1] := #$30DC;
  S[2] := #$30BF;
  S[3] := #$30F3;
  AddString('TestCaption', S);
  S[1] := #$30D2;
  S[2] := #$30F3;
  S[3] := #$30C8;
  AddString('TestHint', S);
 end;}
 SpinEdit1.MinValue := 0;
 SpinEdit1.MaxValue := Localizer1.LanguageList.Count - 1;
 Localizer1.LanguageIndex := 0;
 LangLabel.Caption := Localizer1.Language.LanguageName;
end;

procedure TForm1.Action1Execute(Sender: TObject);
Var I: Integer;
begin
 With Localizer1 do
 begin
  I := LanguageIndex + 1;
  If I >= LanguageList.Count then
   SpinEdit1.Value := 0 Else
   SpinEdit1.Value := I;
 end;
end;

procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
 try
  Localizer1.LanguageIndex := SpinEdit1.Value;
  LangLabel.Caption := Localizer1.Language.LanguageName;
 except
 end;
end;

end.
