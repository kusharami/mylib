unit MyClasses;

interface

(*   Copyright!  *)
(* Made by Jinni *)
(*  Do not sell! *)

uses
 Windows, Messages, SysUtils, Classes, HexUnit, MyUtils, NodeLst, IniFiles;

type
 TStreamClass = class of TStream;

 TFileStreamEx = class(THandleStream)
  private
    FFileName: WideString;
    FRemoveOnDestroy: Boolean;
  public
    constructor Create(const FileName: WideString; Mode: Integer;
            RemoveOnDestroy: Boolean = False);
    destructor Destroy; override;
  end;

 TStreamedNode = class(TNode)
  public
    procedure LoadFromStream(Stream: TStream); virtual; abstract;
    procedure SaveToStream(Stream: TStream); virtual; abstract;
    procedure LoadFromFile(const FileName: WideString); overload;
    procedure SaveToFile(const FileName: WideString); overload;
    procedure LoadFromFile(const FileName: AnsiString); overload;
    procedure SaveToFile(const FileName: AnsiString); overload;
 end;

 TStreamedList = class(TNodeList)
  public
    procedure LoadFromStream(Stream: TStream); virtual; abstract;
    procedure SaveToStream(Stream: TStream); virtual; abstract;
    procedure LoadFromFile(const FileName: WideString); overload;
    procedure SaveToFile(const FileName: WideString); overload;
    procedure LoadFromFile(const FileName: AnsiString); overload;
    procedure SaveToFile(const FileName: AnsiString); overload;
 end;

const
 SPID_PROGRESS_ID_FIRST      = 0;
 SPID_READ_START             = 0;

 SPID_READ_SEC_HEADER_START  = 1;
 SPID_READ_SECTION_HEADER    = 2;
 SPID_READ_SEC_HEADER_END    = 3;

 SPID_READ_HEADER_START      = 4;
 SPID_READ_DATA_HEADER       = 5;
 SPID_READ_HEADER_END        = 6;

 SPID_READ_DATA_START        = 7;
 SPID_READ_DATA              = 8;
 SPID_READ_DATA_END          = 9;

 SPID_READ_END               = 10;

 SPID_READ_ERROR             = 11;


 SPID_WRITE_START            = 12;

 SPID_WRITE_SEC_HEADER_START = 13;
 SPID_WRITE_SECTION_HEADER   = 14;
 SPID_WRITE_SEC_HEADER_END   = 15;

 SPID_WRITE_HEADER_START     = 16;
 SPID_WRITE_DATA_HEADER      = 17;
 SPID_WRITE_HEADER_END       = 18;

 SPID_WRITE_DATA_START       = 19;
 SPID_WRITE_DATA             = 20;
 SPID_WRITE_DATA_END         = 21;

 SPID_WRITE_END              = 22;

 SPID_WRITE_ERROR            = 23;
 SPID_PROGRESS_ID_LAST       = 23;

type
 TChecksum = LongWord;

 PSectionHeader = ^TSectionHeader;
 TSectionHeader = packed record
  shSignature: LongWord;
  shHeaderSize: LongInt;
  shDataSize: LongInt;
  shChecksum: TChecksum;
  dh: record end;
 end;

 PListDataHeaderV1 = ^TListDataHeaderV1;
 TListDataHeaderV1 = packed record
  lshItemsCount: LongInt;
 end;

 TListDataHeaderV2 = packed record
  lshItemsCount: LongInt;
  lshNameLength: Word;
 end;

 PProgressRec = ^TProgressRec;
 TProgressRec = record
  Stream: TStream;
  Header: PSectionHeader;
  ProgressID: Integer;
  Value: Integer;
 end;

 TConsoleInput = class
  private
    stdin: THandle;
    function OldConsoleMode(handle: THandle): Cardinal;
  public
    function GetCh: WideChar;
    function KeyPressed: Boolean;

    constructor Create;
    destructor Destroy; override;
 end;

 TConsoleOutputOption = (cooAbortOnKeyPress, cooSkipDuplicates);
 TConsoleOutputOptions = set of TConsoleOutputOption;

 TConsoleOutput = class(TConsoleInput)
  private
    FLog: TextFile;
    FTextAssigned: Boolean;
    FLast: WideString;
    FOptions: TConsoleOutputOptions;

    function GetCanAbort: Boolean;
    procedure SetCanAbort(Value: Boolean);
    function GetSkipDups: Boolean;
    procedure SetSkipDups(Value: Boolean);
  public
    procedure Output(Sender: TObject; const Text: WideString); virtual;
    procedure AbortCheck(Sender: TObject; var Abort: Boolean); virtual;

    constructor Create(const LogFileName: String);
    destructor Destroy; override;

    procedure WaitKeyPress;

    property AbortOnKeyPressEnabled: Boolean read GetCanAbort write SetCanAbort;
    property SkipDuplicatesEnabled: Boolean read GetSkipDups write SetSkipDups;
 end;

 TBaseSectionedList = class;

 TSectionProgressEvent = procedure (Owner: TBaseSectionedList; Sender: TObject;
                        const Rec: TProgressRec) of object;

 TInternalFlags = (secLoading, secSaving, secSkipChecksumTest, secSkipDataSizeTest, secCustom);

 TBaseSectionedList = class(TStreamedList)
  private
    function GetTotalSize: LongInt;
    procedure SetName(Value: WideString);
  protected
    FSignature: LongWord;
    FHeaderSize: LongInt;
    FOnProgress: TSectionProgressEvent;
    FName: WideString;
    FMaxNameLength: Integer;
    FNoRename: Boolean;
    function CheckSignature(Value: LongWord): Boolean; virtual;
    function CheckHeaderSize(var Size: LongInt): Boolean; virtual;
    function ReadSectionHeader(Stream: TStream; var Header: TSectionHeader): Boolean; virtual;
    procedure WriteSectionHeader(Stream: TStream; var Header: TSectionHeader); virtual;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; virtual;
    function ReadSetCount(var Header: TSectionHeader): LongInt; virtual;
    procedure WriteHeader(Stream: TStream; var Header: TSectionHeader); virtual;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); virtual;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); virtual;
    procedure FillSectionHeader(var Header: TSectionHeader); virtual;
    procedure FillHeader(var Header: TSectionHeader); virtual;
    procedure Progress(Sender: TObject; const Rec: TProgressRec); virtual;
    procedure OperationProgress(Stream: TStream; var Header: TSectionHeader;
                                ProgressID: Integer; Value: Integer = 0);
    procedure Read(var Rec: TProgressRec; var Buffer; Size: Integer);
    procedure Write(var Rec: TProgressRec; var Buffer; Size: Integer);

    function CalculateChecksum: TChecksum; virtual;
    function CalculateDataSize: LongInt; virtual;

    procedure Initialize; override;

    procedure SetOnProgress(Value: TSectionProgressEvent); virtual;
  public
    FInternalFlags: set of TInternalFlags;
    property Name: WideString read FName write SetName;
    function FindByContent(AChecksum: LongWord;
                           ADataSize: Integer{;
                           CheckIndex: Integer}): TBaseSectionedList;
    property Signature: LongWord read FSignature write FSignature;
    property Checksum: TChecksum read CalculateChecksum;
    property DataSize: LongInt read CalculateDataSize;
    property TotalSize: LongInt read GetTotalSize;
    property NoRename: Boolean read FNoRename write FNoRename;

    property OnProgress: TSectionProgressEvent read FOnProgress
                                              write SetOnProgress;

    function FindByName(const AName: WideString): TBaseSectionedList;
    function FindIndexByFirstLetters(const ALetters: WideString): Integer;
    
    function AddChild(const AName: WideString): TBaseSectionedList;

    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;

    procedure LoadFromFile(const FileName: WideString); 
    procedure SaveToFile(const FileName: WideString); 

    procedure Assign(Source: TNode); override;
 end;

 TSectionedList = class(TBaseSectionedList)
  protected
    procedure Initialize; override;
    function ReadSetCount(var Header: TSectionHeader): LongInt; override;
    procedure FillHeader(var Header: TSectionHeader); override;
 end;

 TNamedList = class(TSectionedList)
  protected
    procedure Initialize; override;
    procedure FillHeader(var Header: TSectionHeader); override;
 end;

 (* Streams *)

 TSubStream = class(TStream)
  private
    FStream: TStream;
    FStreamPosition: Int64;
    FStreamSize: Int64;
    FCurPos: Int64;
    function GetOriginalStream: TStream;
  protected
    procedure SetSize(NewSize: Integer); override;
    procedure SetSize(const NewSize: Int64); override;
    function GetSize: Int64; override;
  public
    property StartPosition: Int64 read FStreamPosition;
    property OriginalStream: TStream read GetOriginalStream;

    constructor Create; overload;
    constructor Create(Source: TStream; Size: Int64); overload;
    constructor Create(Source: TStream; Size: Int64; Position: Int64); overload;
    function Read(var Buffer; Count: Integer): Integer; override;
    function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    function Write(const Buffer; Count: Integer): Integer; override;

    destructor Destroy; override;
 end;

 TSubStreamClass = class of TSubStream;

 TBufferStream = class(TCustomMemoryStream)
  public
    constructor Create(Source: Pointer; Size: Integer);
    function Write(const Buffer; Count: Integer): Integer; override;
 end;

 TMemStream = class(TMemoryStream)
  protected
   function Realloc(var NewCapacity: Longint): Pointer; override;
 end;

 TVarStringStream = class(TCustomMemoryStream)
  private
    FStringPtr: PAnsiString;
  protected
    procedure SetSize(NewSize: Integer); override;
  public
    constructor Create(AStringPtr: PAnsiString);
    function Read(var Buffer; Count: Integer): Integer; override;
    function Write(const Buffer; Count: Integer): Integer; override;
 end;

 TVarWideStringStream = class(TCustomMemoryStream)
  private
    FStringPtr: PWideString;
  protected
    procedure SetSize(NewSize: Integer); override;
  public
    constructor Create(AStringPtr: PWideString);
    function Read(var Buffer; Count: Integer): Integer; override;
    function Write(const Buffer; Count: Integer): Integer; override;
 end;

 (* INI files*)

 TStreamIniFile = class(TMemIniFile)
  public
    constructor Create; overload;
    constructor Create(Source: TStream); overload;
    procedure UpdateFile; override;
    procedure LoadFromStream(Stream: TStream); virtual;
    procedure SaveToStream(Stream: TStream); virtual;
    procedure LoadFromFile(const FileName: String); virtual;
    procedure SaveToFile(const FileName: String); virtual;
 end;

 TAnsiStringList = class(TStringList)
  public
    procedure LoadFromFile(const FileName: WideString); reintroduce;
    procedure SaveToFile(const FileName: WideString); reintroduce;
  {$IFDEF FORCED_UNICODE}
    constructor Create;
  {$ENDIF}
 end;

(* Exception classes *)

 ESubStreamError = class(Exception);
 ESectionError = class(Exception)
  public
   SectionOffset: Int64;
   ErrorOffset: Int64;
   constructor Create(const SecOfs, ErrOfs: Int64; const Msg: String);
 end;

 ESectionSignatureError = class(ESectionError);
 ESectionHeaderError = class(ESectionError);
 ESectionDataSizeError = class(ESectionError);
 ESectionChecksumError = class(ESectionError);

 ESectionOperationAborted = class(Exception);

const
 SInvalidStreamOperation: PChar = 'Invalid stream operation';
 SInvalidInitialParameters: PChar = 'Invalid initial parameters';
 SFilenameNotSpecified: PChar = 'File name is not specified';
 SSignatureError: PChar = 'Signature check failed (Range: 0x%8.8x - 0x%8.8x)';
 SHeaderError: PChar = 'Header check failed (Range: 0x%8.8x - 0x%8.8x)';
 SDataSizeError: PChar = 'Data size check failed (Range: 0x%8.8x - 0x%8.8x)';
 SChecksumError: PChar = 'Data is damaged (Range: 0x%8.8x - 0x%8.8x)';
 SOperationAborted: PChar = 'Operation aborted';

procedure LoadBuffer(const FileName: String; var Buffer; ASize: Integer;
  APosition: Cardinal = 0);
procedure SaveBuffer(const FileName: String; const Buffer; ASize: Integer);
procedure SaveBufferW(const FileName: WideString; const Buffer; ASize: Integer);


const
 CP_WideBOM = 1 shl 16;
 CP_MbcsBOM = 1 shl 17;

function ReadCodePagedString(Source: TStream; Flags: LongWord;
  var ResultCodePage: LongWord): WideString;
procedure WriteCodePagedString(const S: WideString; Dest:TStream; Flags: LongWord);

const
 ATTR_READ_ONLY = 1 shl 31;
 ATTR_LIST = 1 shl 30;
 ATTR_MASK = (1 shl 30) - 1;

type
 TNumerationSystem = (nsDecimal, nsHexadecimal);
 TWideStringArray = array of WideString;

 PAttrDef = ^TAttrDef;
 TAttrDef = record
  adSelf: record end;
  adName: WideString;
  adType: TVarType;
  adEnum: TWideStringArray;
  adMaxArrayLen: LongInt;
  case Integer of
   0: (adMin: Int64;
       adMax: Int64;
       adNumSys: TNumerationSystem);
   1: (adPrecision: Byte);
   2: (adMaxStrLen: LongInt);
 end;

function GetArrayString(AttrDef: PAttrDef; const Arr: Variant): WideString;

function FindEnumMember(const Enum: array of WideString;
                        const FindValue: Variant): Integer; overload;
function FindEnumMember(const FindValue: WideString;
                        const Enum: array of WideString): Integer; overload;
procedure GetEnumListFromString(Str: WideString; var Enum: TWideStringArray);
function CompareStrArrays(const Src, Dst: array of WideString): Boolean;
function GetEnumString(const Enum: array of WideString): WideString;
function GetEnumStringEx(const Enum: array of Variant): WideString;
function GetEnumStringExFromVariant(const Arr: Variant): WideString;
function GetEnumStringFromVarArray(const Enum: array of WideString;
                                   const Arr: array of Variant): WideString;
function GetEnumStringFromVariant(const Enum: array of WideString;
                                  const Arr: Variant): WideString;                                  

procedure AttrDefsCopy(const Src: array of TAttrDef;
                      var Dst: array of TAttrDef;
                      Count: Integer = -1);
function AttrDefGetStoreSize(const Def: TAttrDef): Integer;                      
procedure AttrDefSaveToStream(const Def: TAttrDef; Stream: TStream);
procedure AttrDefLoadFromStream(var Def: TAttrDef; Stream: TStream);
procedure InitArrayAttr(var Def: TAttrDef; const AName: WideString;
                        AType: TVarType; AMaxLength: Integer = 0);
procedure InitStringAttr(var Def: TAttrDef; const AName: WideString;
                         AMaxLength: Integer = 0);
procedure InitWStringAttr(var Def: TAttrDef; const AName: WideString;
                         AMaxLength: Integer = 0);
procedure InitIntegerAttr(var Def: TAttrDef; const AName: WideString;
                          AMin, AMax: Int64; ANumSys: TNumerationSystem = nsDecimal);
procedure InitFloatAttr(var Def: TAttrDef; const AName: WideString; Dbl: Boolean;
                     APrecision: Integer = 0);
procedure InitCustomAttr(var Def: TAttrDef; const AName: WideString;
                           AType: TVarType);

function CompareVariants(const Var1, Var2: Variant): Boolean;
function CompareVarArrays(const Var1, Var2: Variant): Boolean;                           
function VariantGetStoreSize(const Value: Variant): Integer;                           
procedure VariantSaveToStream(const Value: Variant; Stream: TStream);
function VariantLoadFromStream(Stream: TStream): Variant;

function CalcCheckSum(var Buffer; Size: Integer): TChecksum;

procedure SectionOperationAborted;
procedure SectionSignatureError(SectionOffset, ErrorOffset: Int64);
procedure SectionHeaderError(SectionOffset, ErrorOffset: Int64);
procedure SectionDataSizeError(SectionOffset, ErrorOffset: Int64);
procedure SectionChecksumError(SectionOffset, ErrorOffset: Int64);

var
 GlobalShiftState: TShiftState = [];
 GlobalMouseInfo: TMouseHookStruct;

const
 MouseStates = [ssLeft, ssRight, ssMiddle, ssDouble];
 SysKeyStates = [ssAlt, ssCtrl, ssShift];
 EnumSpaceSet: TCharSet = [#0..#255] -
                          ['A'..'Z', 'a'..'z', '0'..'9', '_', '+', '-', '.'];

implementation

uses Variants;

function GetArrayString(AttrDef: PAttrDef; const Arr: Variant): WideString;
var
 IsArray: Boolean;
 J, L, H: Integer;
 Int: Int64;
 Float: Double;
 Bool: Boolean;
 VStr: WideString;
begin
 with AttrDef^ do
 begin
  if adEnum = nil then
  begin
   IsArray := VarIsArray(Arr, True);
   if IsArray then
   begin
    L := VarArrayLowBound(Arr, 1);
    H := VarArrayHighBound(Arr, 1);
   end else
   begin
    L := 0;
    H := 0;
   end; 
   Result := '(';
   for J := L to H do
   begin
    case adType and varTypeMask of
     varSmallInt,
     varInteger,
     varShortInt,
     varByte,
     varWord,
     varLongWord,
     varInt64:
     begin
      try
       if IsArray then
        Int := Arr[J] else
        Int := Arr;
      except
       Int := 0;
      end;
      if Int < adMin then
       Int := adMin else
      if Int > adMax then
       Int := adMax;
      if adNumSys = nsHexadecimal then
       Result := Result + IntTo0xHex(Int, 2) else
       Result := Result + IntToStr(Int);
     end;
     varBoolean:
     begin
      try
       if IsArray then
        Bool := Arr[J] else
        Bool := Arr;
      except
       Bool := False;
      end;
      if Bool then
       Result := Result + 'True' else
       Result := Result + 'False';
     end;
     varSingle,
     varDouble,
     varCurrency:
     begin
      try
       if IsArray then
        Float := Arr[J] else
        Float := Arr;
      except
       Float := 0;
      end;
      Result := Result +
       WideQuotedStr(FloatToStrF(Float, ffGeneral,
                     Max(2, Min(15, adPrecision)), 0), EnumSpaceSet);
     end;
     varOleStr,
     varString:
     begin
      try
       if IsArray then
        VStr := Arr[J] else
        VStr := Arr;
      except
       VStr := '';
      end;
      Result := Result + WideQuotedStr(VStr, EnumSpaceSet);
     end;
    end;
    if J < H then
     Result := Result + ', ';
   end;
   Result := Result + ')';   
  end else
   Result := GetEnumStringFromVariant(adEnum, Arr);
 end;  
end;

procedure EnumStrConvertError;
begin
 raise Exception.Create('String to enum list convert error');
end;

procedure GetEnumListFromString(Str: WideString; var Enum: TWideStringArray);
var
 SaveP, P: PWideChar;
 SaveChar: WideChar;
 L: Integer;
begin
 Finalize(Enum);
 if Str <> '' then
 begin
  Str := Trim(Str);
  if (Str[1] = '(') and (Str[Length(Str)] = ')') then
  begin
   repeat
    Delete(Str, 1, 1);
    Str := TrimLeft(Str);
    if Str[1] = '"' then
    begin
     P := Pointer(Str);
     Inc(P);
     SaveP := P;
     while (P^ <> #0) and (P^ <> '"') do
     begin
      if P^ = '\' then
      begin
       Inc(P);
       if P^ = #0 then Break;
      end;
      Inc(P);
     end;
     if P^ = '"' then
     begin
      P^ := #0;
      L := Length(Enum);
      SetLength(Enum, L + 1);
      Enum[L] := EscapeStringDecode(SaveP);
      P^ := '"';
      Delete(Str, 1, (Integer(P) - Integer(Pointer(Str))) shr 1 + 1);
      Str := TrimLeft(Str);
      if Str[1] = ')' then
       Exit else
      if Str[1] = ',' then
       Continue;
     end;
    end else
    begin
     P := Pointer(Str);
     while (P^ <> #0) and
           (Word(P^) < 255) and
           not (AnsiChar(Byte(P^)) in EnumSpaceSet) do
      Inc(P);
     if P^ <> #0 then
     begin
      SaveChar := P^;
      P^ := #0;
      L := Length(Enum);
      SetLength(Enum, L + 1);
      Enum[L] := EscapeStringDecode(PWideChar(Pointer(Str)));
      P^ := SaveChar;
      Delete(Str, 1, (Integer(P) - Integer(Pointer(Str))) shr 1);
      Str := TrimLeft(Str);
      if Str[1] = ')' then
       Exit else
      if Str[1] = ',' then
       Continue;
     end;
    end;
    EnumStrConvertError;    
   until False;
  end;
 end;
 EnumStrConvertError;
end;

function CompareStrArrays(const Src, Dst: array of WideString): Boolean;
var
 I: Integer;
begin
 Result := Length(Src) = Length(Dst);
 if Result then
  for I := 0 to Length(Src) - 1 do
   if Src[I] <> Dst[I] then
   begin
    Result := False;
    Exit;
   end;
end;

function GetEnumString(const Enum: array of WideString): WideString;
var
 I: Integer;
begin
 Result := '(';
 for I := 0 to Length(Enum) - 2 do
  Result := Result + WideQuotedStr(Enum[I], EnumSpaceSet) + ', ';
 I := Length(Enum) - 1;
 if I >= 0 then
  Result := Result + WideQuotedStr(Enum[I], EnumSpaceSet);
 Result := Result + ')';
end;

function GetEnumStringEx(const Enum: array of Variant): WideString;
var
 I: Integer;
begin
 try
  Result := '(';
  for I := 0 to Length(Enum) - 2 do
   Result := Result + WideQuotedStr(Enum[I], EnumSpaceSet) + ', ';
  I := Length(Enum) - 1;
  if I >= 0 then
   Result := Result + WideQuotedStr(Enum[I], EnumSpaceSet);
  Result := Result + ')';
 except
  Result := '()';
 end;
end;

function GetEnumStringExFromVariant(const Arr: Variant): WideString;
var
 I, L, H: Integer;
begin
 try
  Result := '(';
  if VarIsArray(Arr, True) then
  begin
   L := VarArrayLowBound(Arr, 1);
   H := VarArrayHighBound(Arr, 1);
   for I := L to H - 1 do
    Result := Result + WideQuotedStr(Arr[I], EnumSpaceSet) + ', ';
   if H >= L then
    Result := Result + WideQuotedStr(Arr[H], EnumSpaceSet);
  end else
   Result := Result + WideQuotedStr(Arr, EnumSpaceSet);
  Result := Result + ')';
 except
  Result := '()';
 end;
end;

function FindEnumMember(const FindValue: WideString;
                        const Enum: array of WideString): Integer;
var
 I: Integer;
begin
 if Length(Enum) > 0 then
 begin
  for I := 0 to Length(Enum) - 1 do
   if FindValue = Enum[I] then
   begin
    Result := I;
    Exit;
   end;
 end;
 Result := -1;
end;

function FindEnumMember(const Enum: array of WideString;
                   const FindValue: Variant): Integer;
var
 I: Integer;
 Str: WideString;
 V: Variant;
begin
 if Length(Enum) > 0 then
 try
  if VarIsArray(FindValue, True) then
   V := FindValue[VarArrayLowBound(FindValue, 1)] else
   V := FindValue;
  Result := V;
  if (Result < 0) or (Result >= Length(Enum)) then
   Result := 0;
 except
  try
   Str := V;
   for I := 0 to Length(Enum) - 1 do
    if Str = Enum[I] then
    begin
     Result := I;
     Exit;
    end;
  except
  end;
  Result := 0;
 end else
  Result := -1;
end;

function GetEnumStringFromVarArray(const Enum: array of WideString;
                                  const Arr: array of Variant): WideString;

var
 I: Integer;
begin
 Result := '(';
 if Length(Enum) > 0 then
 begin
  for I := 0 to Length(Arr) - 1 do
   Result := Result + WideQuotedStr(Enum[FindEnumMember(Enum, Arr[I])], EnumSpaceSet) + ', ';
  I := Length(Arr) - 1;
  if I >= 0 then
   Result := Result + WideQuotedStr(Enum[FindEnumMember(Enum, Arr[I])], EnumSpaceSet);
 end;
 Result := Result + ')';
end;

function GetEnumStringFromVariant(const Enum: array of WideString;
                                  const Arr: Variant): WideString;
var
 I, L, H: Integer;
begin
 Result := '(';
 if Length(Enum) > 0 then
 begin
  if VarIsArray(Arr, True) then
  begin
   L := VarArrayLowBound(Arr, 1);
   H := VarArrayHighBound(Arr, 1);
   for I := L to H do
   begin
    Result := Result + WideQuotedStr(Enum[FindEnumMember(Enum, Arr[I])], EnumSpaceSet);
    if I < H then
     Result := Result + ', ';
   end;
  end else
   Result := Result + WideQuotedStr(Enum[FindEnumMember(Enum, Arr)], EnumSpaceSet);
 end;
 Result := Result + ')';
end;

function CalcCheckSum(var Buffer; Size: Integer): TChecksum;
var
 PB: PByte;
begin
 PB := @Buffer;
 Result := 0;
 while (Size > 0) do
 begin
  Dec(Size);
  Inc(Result, Size + PB^);
  Inc(PB);
 end;
end;

var
 MouseHook: HHook = 0;
 KeyboardHook: HHook = 0;

function KeyboardProc(Code: Integer; WParam: LongInt; LParam: LongInt): LongInt; stdcall;
begin
 if Code >= HC_ACTION then
 begin
  GlobalShiftState := GlobalShiftState - [ssAlt, ssCtrl, ssShift];
  if GetKeyState(VK_MENU) < 0 then Include(GlobalShiftState, ssAlt);
  if GetKeyState(VK_CONTROL) < 0 then Include(GlobalShiftState, ssCtrl);
  if GetKeyState(VK_SHIFT) < 0 then Include(GlobalShiftState, ssShift);
 end;
 Result := CallNextHookEx(KeyboardHook, Code, WParam, LParam);
end;

function MouseProc(Code: Integer; WParam: LongInt; LParam: LongInt): LongInt; stdcall;
begin
 if (Code >= HC_ACTION) then
 begin
  case WParam of
   WM_LBUTTONDOWN: Include(GlobalShiftState, ssLeft);
   WM_RBUTTONDOWN: Include(GlobalShiftState, ssRight);
   WM_MBUTTONDOWN: Include(GlobalShiftState, ssMiddle);
   WM_LBUTTONDBLCLK:
   begin
    Include(GlobalShiftState, ssLeft);
    Include(GlobalShiftState, ssDouble);
   end;
   WM_RBUTTONDBLCLK:
   begin
    Include(GlobalShiftState, ssRight);
    Include(GlobalShiftState, ssDouble);
   end;
   WM_MBUTTONDBLCLK:
   begin
    Include(GlobalShiftState, ssMiddle);
    Include(GlobalShiftState, ssDouble);
   end;
   WM_LBUTTONUP:
   begin
    Exclude(GlobalShiftState, ssLeft);
    Exclude(GlobalShiftState, ssDouble);
   end;
   WM_RBUTTONUP:
   begin
    Exclude(GlobalShiftState, ssRight);
    Exclude(GlobalShiftState, ssDouble);
   end;
   WM_MBUTTONUP:
   begin
    Exclude(GlobalShiftState, ssMiddle);
    Exclude(GlobalShiftState, ssDouble);
   end;
  end;

  if lParam <> 0 then
   GlobalMouseInfo := PMouseHookStruct(lParam)^ else
   FillChar(GlobalMouseInfo, SizeOf(TMouseHookStruct), 0);
 end;
 Result := CallNextHookEx(MouseHook, Code, WParam, LParam);
end;

function varArrayCompareData(const V1, V2: Variant;
                             var Indices: array of Integer;
                             Dim: Integer): Boolean;
var
 I, L, H: Integer;
begin
 Result := False;
 L := VarArrayLowBound(V1, Dim);
 H := VarArrayHighBound(V1, Dim);
 if (L = VarArrayLowBound(V2, Dim)) and
    (H = VarArrayHighBound(V2, Dim)) then
 begin
  Dec(Dim);
  if Dim > 0 then
  begin
   for I := L to H do
   begin
    Indices[Dim] := I;
    if not varArrayCompareData(V1, V2, Indices, Dim) then Exit;
   end;
  end else
  begin
   for I := L to H do
   begin
    Indices[0] := I;
    if VarArrayGet(V1, Indices) <> VarArrayGet(V2, Indices) then Exit;
   end;
  end;
  Result := True;
 end;
end;

function CompareVarArrays(const Var1, Var2: Variant): Boolean;
var
 Len: Integer;
 V1, V2: PVarData;
 Indices: array of Integer;
begin
 Result := False;
 V1 := FindVarData(Var1);
 V2 := FindVarData(Var2);
 if (V1 <> nil) and (V2 <> nil) and
    (VarIsArray(Variant(V1^), False) and VarIsArray(Variant(V2^), False)) then
 begin
  Len := VarArrayDimCount(Variant(V1^));
  if Len = VarArrayDimCount(Variant(V2^)) then
  begin
   SetLength(Indices, Len);
   Result := varArrayCompareData(Variant(V1^), Variant(V2^), Indices, Len);
  end;
 end;
end;

function CompareVariants(const Var1, Var2: Variant): Boolean;
begin
 try
  if VarIsArray(Var1, True) and VarIsArray(Var2, True) then
   Result := CompareVarArrays(Var1, Var2) else
   Result := Var1 = Var2;
 except
  Result := False;
 end;
end;

function VarArrayGetStoreSize(const Value: Variant;
                               var Indices: array of Integer;
                               Dim: Integer): Integer;
var
 I: Integer;
begin
 Result := 0;
 if Dim > 0 then
 begin
  for I := VarArrayLowBound(Value, Dim + 1) to
           VarArrayHighBound(Value, Dim + 1) do
  begin
   Indices[Dim] := I;
   Inc(Result, VarArrayGetStoreSize(Value, Indices, Dim - 1));
  end;
 end else
 begin
  for I := VarArrayLowBound(Value, 1) to VarArrayHighBound(Value, 1) do
  begin
   Indices[0] := I;
   Inc(Result, VariantGetStoreSize(VarArrayGet(Value, Indices)));
  end;
 end;
end;

function VariantGetStoreSize(const Value: Variant): Integer;
var
 Cnt: LongInt;
 V: PVarData;
 Indices: array of Integer;
begin
 Result := 0;
 V := FindVarData(Value);
 if V <> nil then
 begin
  Inc(Result, SizeOf(TVarType));
  if VarIsArray(Variant(V^), False) then
  begin
   Cnt := VarArrayDimCount(Variant(V^));
   SetLength(Indices, Cnt);
   Inc(Result, 4 + Cnt * 8 + VarArrayGetStoreSize(Variant(V^), Indices, Cnt - 1));
  end else
  with V^ do
  begin
   case VType of
    varSmallInt,
    varWord:     Inc(Result, 2);
    varInteger,
    varLongWord,
    varSingle:   Inc(Result, 4);
    varDouble,
    varInt64:    Inc(Result, 8);
    varCurrency: Inc(Result, SizeOf(Currency));
    varDate:     Inc(Result, SizeOf(TDateTime));
    varOleStr:   Inc(Result, 4 + WideStrLen(VOleStr) shl 1);
    varError:    Inc(Result, SizeOf(HRESULT));
    varBoolean,
    varShortInt,
    varByte:     Inc(Result, 1);
    varString:   Inc(Result, 1 + PByte(VString)^);
   end;
  end;
 end;
end;

procedure VarArraySaveToStream(const Value: Variant;
                               Stream: TStream;
                               var Indices: array of Integer;
                               Dim: Integer);
var
 I: Integer;
begin
 if Dim > 0 then
 begin
  for I := VarArrayLowBound(Value, Dim + 1) to
           VarArrayHighBound(Value, Dim + 1) do
  begin
   Indices[Dim] := I;
   VarArraySaveToStream(Value, Stream, Indices, Dim - 1);
  end;
 end else
 begin
  for I := VarArrayLowBound(Value, 1) to VarArrayHighBound(Value, 1) do
  begin
   Indices[0] := I;
   VariantSaveToStream(VarArrayGet(Value, Indices), Stream);
  end;
 end;
end;

procedure VariantSaveToStream(const Value: Variant; Stream: TStream);
var
 Len, Cnt: LongInt;
 I: Integer;
 V: PVarData;
 Indices: array of Integer;
begin
 V := FindVarData(Value);
 if V <> nil then
 begin
  Stream.WriteBuffer(V.VType, SizeOf(TVarType)); 
  if VarIsArray(Variant(V^), False) then
  begin
   Cnt := VarArrayDimCount(Variant(V^));
   Stream.WriteBuffer(Cnt, 4);
   for I := 1 to Cnt do
   begin
    Len := VarArrayLowBound(Variant(V^), I);
    Stream.WriteBuffer(Len, 4);
    Len := VarArrayHighBound(Variant(V^), I);
    Stream.WriteBuffer(Len, 4);
   end;
   SetLength(Indices, Cnt);
   VarArraySaveToStream(Variant(V^), Stream, Indices, Cnt - 1);
  end else
  with V^ do
  begin
   case VType of
    varSmallInt: Stream.WriteBuffer(VSmallInt, 2);
    varInteger:  Stream.WriteBuffer(VInteger, 4);
    varSingle:   Stream.WriteBuffer(VSingle, 4);
    varDouble:   Stream.WriteBuffer(VDouble, 8);
    varCurrency: Stream.WriteBuffer(VCurrency, SizeOf(Currency));
    varDate:     Stream.WriteBuffer(VDate, SizeOf(TDateTime));
    varOleStr:
    begin
     Len := WideStrLen(VOleStr);
     Stream.WriteBuffer(Len, 4);
     Stream.WriteBuffer(VOleStr^, Len shl 1);
    end;
    varError:    Stream.WriteBuffer(VError, SizeOf(HRESULT));
    varBoolean:  Stream.WriteBuffer(VBoolean, 1);
    varShortInt: Stream.WriteBuffer(VShortInt, 1);
    varByte:     Stream.WriteBuffer(VByte, 1);
    varWord:     Stream.WriteBuffer(VWord, 2);
    varLongWord: Stream.WriteBuffer(VLongWord, 4);
    varInt64:    Stream.WriteBuffer(VInt64, 8);
    varString:
    begin
     Len := PByte(VString)^ + 1;
     Stream.WriteBuffer(VString^, Len);
    end;
   end;
  end;
 end;
end;

procedure VarArrayLoadFromStream(var Value: Variant;
                               Stream: TStream;
                               var Indices: array of Integer;
                               Dim: Integer);
var
 I: Integer;
begin
 if Dim > 0 then
 begin
  for I := VarArrayLowBound(Value, Dim + 1) to
           VarArrayHighBound(Value, Dim + 1) do
  begin
   Indices[Dim] := I;
   VarArrayLoadFromStream(Value, Stream, Indices, Dim - 1);
  end;
 end else
 begin
  for I := VarArrayLowBound(Value, 1) to VarArrayHighBound(Value, 1) do
  begin
   Indices[0] := I;
   VarArrayPut(Value, VariantLoadFromStream(Stream), Indices);
  end;
 end;
end;

function VariantLoadFromStream(Stream: TStream): Variant;
var
 Cnt, Len: LongInt;
 WStr: WideString;
 AStr: AnsiString;
 V: TVarType;
 Bounds: array of LongInt;
 Indices: array of Integer;
begin
 Stream.ReadBuffer(V, SizeOf(TVarType));
 if V and varArray <> 0 then
 begin
  Stream.ReadBuffer(Cnt, 4);
  SetLength(Bounds, Cnt * 2);
  Stream.ReadBuffer(Pointer(Bounds)^, Cnt * 8);
  Result := VarArrayCreate(Bounds, V and varTypeMask);
  SetLength(Indices, Cnt);
  VarArrayLoadFromStream(Result, Stream, Indices, Cnt - 1);
 end else
 with TVarData(Result) do
 begin
  case V of
   varSmallInt: Stream.ReadBuffer(VSmallInt, 2);
   varInteger:  Stream.ReadBuffer(VInteger, 4);
   varSingle:   Stream.ReadBuffer(VSingle, 4);
   varDouble:   Stream.ReadBuffer(VDouble, 8);
   varCurrency: Stream.ReadBuffer(VCurrency, SizeOf(Currency));
   varDate:     Stream.ReadBuffer(VDate, SizeOf(TDateTime));
   varOleStr:
   begin
    Stream.ReadBuffer(Len, 4);
    SetLength(WStr, Len);
    Stream.ReadBuffer(Pointer(WStr)^, Len shl 1);
    Result := WStr;
    Exit;
   end;
   varError:    Stream.ReadBuffer(VError, SizeOf(HRESULT));
   varBoolean:
   begin
    Stream.ReadBuffer(VBoolean, 1);
    VBoolean := Byte(VBoolean) <> 0;
   end;
   varShortInt: Stream.ReadBuffer(VShortInt, 1);
   varByte:     Stream.ReadBuffer(VByte, 1);
   varWord:     Stream.ReadBuffer(VWord, 2);
   varLongWord: Stream.ReadBuffer(VLongWord, 4);
   varInt64:    Stream.ReadBuffer(VInt64, 8);
   varString:
   begin
    Stream.ReadBuffer(Len, 1);
    SetLength(AStr, Len);
    Stream.ReadBuffer(Pointer(AStr)^, Len);
    Result := AStr;
    Exit;
   end;
  end;
  VType := V;
 end;
end;

procedure AttrDefsCopy(const Src: array of TAttrDef; var Dst: array of TAttrDef;
                      Count: Integer = -1);
var
 AD: PAttrDef;
 I, J: Integer;
begin
 if Count < 0 then
  Count := Min(Length(Src), Length(Dst)) else
  Count := Min(Min(Length(Src), Length(Dst)), Count);
 for I := 0 to Count - 1 do
 begin
  AD := Addr(Dst[I]);
  with Src[I] do
  begin
   AD.adName := adName;
   SetLength(AD.adEnum, Length(adEnum));
   for J := 0 to Length(adEnum) - 1 do
    AD.adEnum[J] := adEnum[J];
   AD.adType := adType;
   AD.adMaxArrayLen := adMaxArrayLen;
   AD.adMin := adMin;
   AD.adMax := adMax;
   AD.adNumSys := adNumSys;
//   Move(adType, AD.adType, SizeOf(TAttrDef) - SizeOf(Pointer));
  // AD.adEnum := adEnum;
   //AD.adType := adType;
  // Move(adMaxArrayLen, AD.adMaxArrayLen, SizeOf(TAttrDef) - SizeOf(Pointer) shl 1);
  end;
 end;
end;

procedure AttrDefSaveToStream(const Def: TAttrDef; Stream: TStream);
var
 I, Cnt: Integer;
begin
 Cnt := 255;
 Stream.WriteBuffer(Cnt, 1);
 Cnt := Min(Length(Def.adName), 255);
 Stream.WriteBuffer(Cnt, 1);
 Stream.WriteBuffer(Pointer(Def.adName)^, Cnt shl 1);
 Stream.WriteBuffer(Def.adType, 2);
 if Def.adType and varArray <> 0 then
  Stream.WriteBuffer(Def.adMaxArrayLen, 4);
 Cnt := Min(Length(Def.adEnum), High(Word));
 Stream.WriteBuffer(Cnt, 2);
 for I := 0 to Cnt - 1 do
 begin
  Cnt := Min(Length(Def.adEnum[I]), High(Word));
  Stream.WriteBuffer(Cnt, 2);
  Stream.WriteBuffer(Pointer(Def.adEnum[I])^, Cnt shl 1);
 end;
 if Cnt = 0 then
  case Def.adType and varTypeMask of
   varSmallInt,
   varInteger,
   varShortInt,
   varByte,
   varWord,
   varLongWord,
   varInt64: Stream.WriteBuffer(Def.adMin, 17);
   varSingle,
   varDouble,
   varCurrency: Stream.WriteBuffer(Def.adPrecision, 1);
   varOleStr,
   varString: Stream.WriteBuffer(Def.adMaxStrLen, 4);
  end;
end;

function AttrDefGetStoreSize(const Def: TAttrDef): Integer;
var
 I, Cnt: Integer;
begin
 Cnt := Min(Length(Def.adEnum), High(Word));
 Result := 2 + Min(Length(Def.adName), 255) shl 1 + 2 +
           2 + Cnt;
 for I := 0 to Cnt - 1 do
  Inc(Result, 2 + Min(Length(Def.adEnum[I]), High(Word)) shl 1);
 if Def.adType and varArray <> 0 then
  Inc(Result, 4);
 if Cnt = 0 then
  case Def.adType and varTypeMask of
   varSmallInt,
   varInteger,
   varShortInt,
   varByte,
   varWord,
   varLongWord,
   varInt64: Inc(Result, 17);
   varSingle,
   varDouble,
   varCurrency: Inc(Result);
   varOleStr,
   varString: Inc(Result, 4);
  end;
end;

procedure AttrDefLoadFromStream(var Def: TAttrDef; Stream: TStream);
var
 I, Cnt: Integer;
begin
 Cnt := 0;
 Stream.ReadBuffer(Cnt, 1);
 if Cnt = 255 then
 begin
  Stream.ReadBuffer(Cnt, 1);
  SetLength(Def.adName, Cnt);
  Stream.ReadBuffer(Pointer(Def.adName)^, Length(Def.adName) shl 1);
  Stream.ReadBuffer(Def.adType, 2);

  if Def.adType and varArray <> 0 then
   Stream.ReadBuffer(Def.adMaxArrayLen, 4);

  Stream.ReadBuffer(Cnt, 2);
  SetLength(Def.adEnum, Cnt);
  for I := 0 to Cnt - 1 do
  begin
   Stream.ReadBuffer(Cnt, 2);
   Stream.ReadBuffer(Pointer(Def.adEnum[I])^, Cnt shl 1);
  end;

  if Cnt = 0 then
   case Def.adType and varTypeMask of
    varSmallInt,
    varInteger,
    varShortInt,
    varByte,
    varWord,
    varLongWord,
    varInt64: Stream.ReadBuffer(Def.adMin, 17);
    varSingle,
    varDouble,
    varCurrency: Stream.ReadBuffer(Def.adPrecision, 1);
    varOleStr,
    varString: Stream.ReadBuffer(Def.adMaxStrLen, 4);
   end;
 end else
 begin
  SetLength(Def.adName, Cnt);
  Finalize(Def.adEnum);
  Stream.ReadBuffer(Pointer(Def.adName)^, Length(Def.adName) shl 1);
  Stream.ReadBuffer(Def.adType, 2);
  if Def.adType and varArray <> 0 then
  begin
   Stream.ReadBuffer(Def.adMaxArrayLen, 4);
   case Def.adType and varTypeMask of
    varSmallInt:
    begin
     Def.adMin := Low(SmallInt);
     Def.adMax := High(SmallInt);
    end;
    varInteger:
    begin
     Def.adMin := Low(LongInt);
     Def.adMax := High(LongInt);
    end;
    varShortInt:
    begin
     Def.adMin := Low(ShortInt);
     Def.adMax := High(ShortInt);
    end;
    varByte:
    begin
     Def.adMin := Low(Byte);
     Def.adMax := High(Byte);
    end;
    varWord:
    begin
     Def.adMin := Low(Word);
     Def.adMax := High(Word);
    end;
    varLongWord:
    begin
     Def.adMin := Low(LongWord);
     Def.adMax := High(LongWord);
    end;
    varInt64:
    begin
     Def.adMin := Low(Int64);
     Def.adMax := High(Int64);
    end;
    varSingle:
     Def.adPrecision := 7;
    varDouble:
     Def.adPrecision := 15;
    varCurrency:
     Def.adPrecision := 2;
    varOleStr,
    varString: Def.adMaxStrLen := 0;
   end;
  end else
  case Def.adType and varTypeMask of
   varSmallInt,
   varInteger,
   varShortInt,
   varByte,
   varWord,
   varLongWord,
   varInt64: Stream.ReadBuffer(Def.adMin, 17);
   varSingle,
   varDouble,
   varCurrency:
    Stream.ReadBuffer(Def.adPrecision, 1);
   varOleStr,
   varString: Stream.ReadBuffer(Def.adMaxStrLen, 4);
  end;
 end;
end;

procedure InitArrayAttr(var Def: TAttrDef; const AName: WideString;
         AType: TVarType; AMaxLength: Integer = 0);
begin
 Def.adName := AName;
 case AType of
  varByte:
  begin
   Def.adMin := 0;
   Def.adMax := 255;
  end;
  varShortInt:
  begin
   Def.adMin := -128;
   Def.adMax := 127;
  end;
  varWord:
  begin
   Def.adMin := 0;
   Def.adMax := 65535;
  end;
  varSmallInt:
  begin
   Def.adMin := -32768;
   Def.adMax := 32767;
  end;
  varInteger:
  begin
   Def.adMin := Low(LongInt);
   Def.adMax := High(LongInt);
  end;
  varLongWord:
  begin
   Def.adMin := 0;
   Def.adMax := High(LongWord);
  end;
  varInt64:
  begin
   AType := varVariant;
   Def.adMin := Low(Int64);
   Def.adMax := High(Int64);
  end;
  varSingle: Def.adPrecision := 7;
  varDouble: Def.adPrecision := 15;
  varCurrency: Def.adPrecision := 2;
  varString,
  varOleStr: Def.adMaxStrLen := 0;
 end;
 Def.adType := varArray or AType;
 Def.adMaxStrLen := AMaxLength;
end;

procedure InitStringAttr(var Def: TAttrDef; const AName: WideString;
                         AMaxLength: Integer = 0);
begin
 Def.adName := AName;
 Def.adType := varString;
 Def.adMaxStrLen := AMaxLength;
end;

procedure InitWStringAttr(var Def: TAttrDef; const AName: WideString;
                          AMaxLength: Integer = 0);
begin
 Def.adName := AName;
 Def.adType := varOleStr;
 Def.adMaxStrLen := AMaxLength;
end;

procedure InitIntegerAttr(var Def: TAttrDef; const AName: WideString;
                    AMin, AMax: Int64; ANumSys: TNumerationSystem = nsDecimal);
begin
 Def.adName := AName;
 if AMin = AMax then
 begin
  Def.adMin := Low(Int64);
  Def.adMax := High(Int64);
 end else
 begin
  if (AMin >= Low(Byte)) and (AMax <= High(Byte)) then
   Def.adType := varByte else
  if (AMin >= Low(Word)) and (AMax <= High(Word)) then
   Def.adType := varWord else
  if (AMin >= Low(LongWord)) and (AMax <= High(LongWord)) then
   Def.adType := varLongWord else
  if (AMin >= Low(ShortInt)) and (AMax <= High(ShortInt)) then
   Def.adType := varShortInt else
  if (AMin >= Low(SmallInt)) and (AMax <= High(SmallInt)) then
   Def.adType := varSmallInt else
  if (AMin >= Low(LongInt)) and (AMax <= High(LongInt)) then
   Def.adType := varInteger else
   Def.adType := varInt64;
  Def.adMin := AMin;
  Def.adMax := AMax;
 end;
 Def.adNumSys := ANumSys;
end;

procedure InitCustomAttr(var Def: TAttrDef;
                        const AName: WideString; AType: TVarType);
begin
 Finalize(Def.adName);
 FillChar(Def, SizeOf(TAttrDef), 0);
 Def.adName := AName;
 Def.adType := AType;
end;

procedure InitFloatAttr(var Def: TAttrDef; const AName: WideString;
                       Dbl: Boolean; APrecision: Integer = 0);
begin
 Def.adName := AName;
 if APrecision <= 0 then APrecision := 15;
 if APrecision < 2 then APrecision := 2;
 if Dbl then
 begin
  Def.adType := varDouble;
  Def.adPrecision := Min(APrecision, 15);
 end else
 begin
  Def.adType := varSingle;
  Def.adPrecision := Min(APrecision, 7);
 end;
end;

type
 TStreamCharSet = (csAnsi, csUnicode, csUnicodeSwapped, csUtf8);

const
  UNICODE_BOM = WideChar($FEFF);
  UNICODE_BOM_SWAPPED = WideChar($FFFE);
  UTF8_BOM = AnsiString(#$EF#$BB#$BF);

function AutoDetectCharacterSet(Stream: TStream): TStreamCharSet;
var
  ByteOrderMark: WideChar;
  BytesRead: Integer;
  Utf8Test: array[0..2] of AnsiChar;
begin
  // Byte Order Mark
  ByteOrderMark := #0;
  if (Stream.Size - Stream.Position) >= SizeOf(ByteOrderMark) then begin
    BytesRead := Stream.Read(ByteOrderMark, SizeOf(ByteOrderMark));
    if (ByteOrderMark <> UNICODE_BOM) and (ByteOrderMark <> UNICODE_BOM_SWAPPED) then begin
      ByteOrderMark := #0;
      Stream.Seek(-BytesRead, soFromCurrent);
      if (Stream.Size - Stream.Position) >= Length(Utf8Test) * SizeOf(AnsiChar) then begin
        BytesRead := Stream.Read(Utf8Test[0], Length(Utf8Test) * SizeOf(AnsiChar));
        if Utf8Test <> UTF8_BOM then
          Stream.Seek(-BytesRead, soFromCurrent);
      end;
    end;
  end;
  // Test Byte Order Mark
  if ByteOrderMark = UNICODE_BOM then
    Result := csUnicode
  else if ByteOrderMark = UNICODE_BOM_SWAPPED then
    Result := csUnicodeSwapped
  else if Utf8Test = UTF8_BOM then
    Result := csUtf8
  else
    Result := csAnsi;
end;

function ReadCodePagedString(Source: TStream; Flags: LongWord;
  var ResultCodePage: LongWord): WideString;
var
 DataLeft, I, X: Integer;
 StreamCharSet: TStreamCharSet;
 SA: AnsiString;
 CP: array[0..7] of AnsiChar;
 CRLF: AnsiChar;
 RCP: LongWord;
begin
 if Flags and CP_WideBOM <> 0 then
  StreamCharSet := AutoDetectCharacterSet(Source) else
  StreamCharSet := csAnsi;
 if (Flags and CP_MbcsBOM = 0) and (StreamCharSet = csAnsi) then
 begin
  if Word(Flags) = CP_UTF16 then
   StreamCharSet := csUnicode else
  if Word(Flags) = CP_UTF16_BE then
   StreamCharSet := csUnicodeSwapped else
  if Word(Flags) = CP_UTF8 then
   StreamCharSet := csUtf8;
 end;
 if (Word(Flags) = CP_UTF7) or
    (Word(Flags) = CP_UTF8) or IsValidCodePage(Word(Flags)) then
  RCP := Word(Flags) else
  RCP := GetACP;
 if (Flags and CP_WideBOM = 0) then ResultCodePage := RCP else
 case StreamCharSet of
  csUnicode: ResultCodePage := CP_UTF16;
  csUnicodeSwapped: ResultCodePage := CP_UTF16_BE;
  csUtf8: ResultCodePage := CP_UTF8;
  else
  begin
   DataLeft := Source.Read(CP, SizeOf(CP));
   for I := 0 to DataLeft - 1 do
   if CP[I] in [#10, #13] then
   begin
    CRLF := CP[I];
    Source.Seek(-(DataLeft - (I + 1)), soFromCurrent);
    DataLeft := I + 1;
    Break;
   end;
   CP[DataLeft - 1] := #0;
   if (DataLeft > 4) and
      (UpCase(CP[0]) = 'C') and
      (UpCase(CP[1]) = 'P') and
      (CRLF in [#10, #13]) and
      TryStrToInt(String(AnsiString(PAnsiChar(@CP[2]))), X) and
      IsValidCodePage(X) then
   begin
    ResultCodePage := X;
    if CRLF = #13 then
    repeat
      DataLeft := Source.Read(CRLF, 1);
      if DataLeft <> 1 then Break;
      if CRLF <> #13 then
      begin
        if CRLF <> #10 then
          Source.Seek(-1, soFromCurrent);
        Break;
      end;
    until False;
   end else
   begin
    Source.Seek(-DataLeft, soFromCurrent);
    ResultCodePage := RCP;
   end;
  end;
 end;
 DataLeft := Source.Size - Source.Position;
 if StreamCharSet in [csUnicode, csUnicodeSwapped] then
 begin
  if DataLeft < 2 then Result := '' else
  begin
   DataLeft := DataLeft shr 1;
   SetLength(Result, DataLeft);
   Source.ReadBuffer(Pointer(Result)^, DataLeft shl 1);
   if StreamCharSet = csUnicodeSwapped then
    SwapUTF16_P(Pointer(Result), Length(Result));
  end;
 end else
 begin
  SetLength(SA, DataLeft);
  Source.ReadBuffer(Pointer(SA)^, DataLeft);
  Result := CodePageStringDecode(ResultCodePage, SA);
 end;
end;

procedure WriteCodePagedString(const S: WideString; Dest: TStream; Flags: LongWord);

 function CheckForceBOM: Boolean;
 var
  X: Integer;
  SW: WideString;
 begin
  SW := Copy(S, 1, 8);
  Delete(SW, Pos(#10, SW), 8);
  Delete(SW, Pos(#13, SW), 8);
  X := Length(SW);
  Result := (X > 3) and (X < 8) and
    (AnsiChar(SW[1]) in ['c', 'C']) and
    (AnsiChar(SW[2]) in ['p', 'P']) and
    TryStrToInt(PWideChar(Addr(SW[3])), X) and
    IsValidCodePage(X);
 end;

var
 SA: AnsiString;
 SW: WideString;
 Info: TCPInfoEx;
begin
 if Flags and CP_WideBOM <> 0 then
 begin
  case Word(Flags) of
   CP_UTF16: SA := #$FF#$FE;
   CP_UTF16_BE: SA := #$FE#$FF;
   CP_UTF8: SA := UTF8_BOM;
   else
   begin
    if (Flags and CP_MbcsBOM <> 0) or CheckForceBOM then
     SA := AnsiString(Format(AnsiString('cp%d') + sLineBreak, [Word(Flags)])) else
     SA := '';
   end;
  end;
  if SA <> '' then Dest.WriteBuffer(Pointer(SA)^, Length(SA));
 end;
 case Word(Flags) of
  CP_UTF16: Dest.WriteBuffer(Pointer(S)^, Length(S) shl 1);
  CP_UTF16_BE:
  begin
   SW := SwapUTF16(Pointer(S), Length(S));
   Dest.WriteBuffer(Pointer(SW)^, Length(SW) shl 1);
  end;
  else
  begin
   GetCPInfoEx(Word(Flags), 0, Info);
   SA := CodePageStringEncode(Word(Flags), Info.MaxCharSize, S);
   Dest.WriteBuffer(Pointer(SA)^, Length(SA));
  end;
 end;
end;

procedure LoadBuffer(const FileName: String; var Buffer; ASize: Integer;
  APosition: Cardinal = 0);
begin
 with TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite) do
 try
  Seek(APosition, soBeginning);
  ReadBuffer(Buffer, ASize);
 finally
  Free;
 end;
end;

procedure SaveBuffer(const FileName: String; const Buffer; ASize: Integer);
begin
 with TFileStream.Create(FileName, fmCreate) do
 try
  WriteBuffer(Buffer, ASize);
 finally
  Free;
 end;
end;

procedure SaveBufferW(const FileName: WideString; const Buffer; ASize: Integer);
var
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ or GENERIC_WRITE,
                  0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  with  THandleStream.Create(H) do
  try
   WriteBuffer(Buffer, ASize);
  finally
   Free;
  end;
 finally
  FileClose(H);
 end;
end;

(* TStreamedNode *)

procedure TStreamedNode.LoadFromFile(const FileName: AnsiString);
var Stream: TFileStream;
begin
 Stream := TFileStream.Create(String(FileName), fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamedNode.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   LoadFromStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure TStreamedNode.SaveToFile(const FileName: AnsiString);
var Stream: TFileStream;
begin
 Stream := TFileStream.Create(String(FileName), fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamedNode.SaveToFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ or GENERIC_WRITE,
                  0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   SaveToStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

(* TStreamedList *)

procedure TStreamedList.LoadFromFile(const FileName: AnsiString);
var Stream: TFileStream;
begin
 Stream := TFileStream.Create(String(FileName), fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamedList.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   LoadFromStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure TStreamedList.SaveToFile(const FileName: AnsiString);
var Stream: TFileStream;
begin
 Stream := TFileStream.Create(String(FileName), fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamedList.SaveToFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ or GENERIC_WRITE,
                  0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   SaveToStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

(* TSubStream *)

constructor TSubStream.Create(Source: TStream; Size: Int64);
begin
 if Assigned(Source) and (Size >= 0) then
 begin
  FStream := Source;
  FStreamSize := Size;
  FStreamPosition := Source.Position;
  FCurPos := 0;
 end else
  raise ESubStreamError.Create(SInvalidInitialParameters);
end;

constructor TSubStream.Create(Source: TStream; Size: Int64;
  Position: Int64);
begin
 If Assigned(Source) and (Size >= 0) then
 begin
  FStream := Source;
  FStreamSize := Size;
  if Position < 0 then
   FStreamPosition := Source.Position else
   FStreamPosition := Position;
  FCurPos := 0;
 end Else raise ESubStreamError.Create(SInvalidInitialParameters);
end;

constructor TSubStream.Create;
begin
 raise ESubStreamError.Create(SInvalidInitialParameters);
end;

destructor TSubStream.Destroy;
begin
 FStream.Position := FStreamPosition + FCurPos;
 inherited;
end;

function TSubStream.GetOriginalStream: TStream;
begin
 Result := Self;
 repeat
  Result := TSubStream(Result).FStream;
  Result.Position := FStreamPosition + FCurPos;
 until not (Result is TSubStream);
end;

function TSubStream.GetSize: Int64;
begin
 Result := FStreamSize;
end;

function TSubStream.Read(var Buffer; Count: Integer): Integer;
var
 OldPos: Int64;
begin
 If (FCurPos >= 0) and (FCurPos < FStreamSize) then
 begin
  FStream.Position := FStreamPosition + FCurPos;
  Result := FStream.Read(Buffer, Count);
  OldPos := FCurPos;
  Inc(FCurPos, Result);
  If FCurPos > FStreamSize then
  begin
   Result := FStreamSize - OldPos;
   FCurPos := FStreamSize;
  end;
 end Else Result := 0;
end;

function TSubStream.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
 case Origin of
  soBeginning: FCurPos := Offset;
  soCurrent: Inc(FCurPos, Offset);
  soEnd: FCurPos := FStreamSize + Offset;
 end;
 Result := FCurPos;
end;

procedure TSubStream.SetSize(const NewSize: Int64);
begin
 FStreamSize := NewSize;
end;

procedure TSubStream.SetSize(NewSize: Integer);
begin
 FStreamSize := NewSize;
end;

function TSubStream.Write(const Buffer; Count: Integer): Integer;
begin
 raise ESubStreamError.Create(SInvalidStreamOperation);
end;

(* TBufferStream *)

constructor TBufferStream.Create(Source: Pointer; Size: Integer);
begin
 SetPointer(Source, Size);
end;

function TBufferStream.Write(const Buffer; Count: Integer): Integer;
var
 Pos: Integer;
begin
 if Count > 0 then
 begin
  Pos := Seek(0, soFromCurrent);
  if Pos >= 0 then
  begin
   Result := Size - Pos;
   if Result > 0 then
   begin
    if Result > Count then Result := Count;
    Move(Buffer, Pointer(Integer(Memory) + Pos)^, Result);
    Seek(Result, soFromCurrent);
    Exit;
   end;
  end;
 end;
 Result := 0;
end;

(* TVarStringStream *)

constructor TVarStringStream.Create(AStringPtr: PAnsiString);
begin
 FStringPtr := AStringPtr;
 SetPointer(Pointer(AStringPtr^), Length(AStringPtr^));
end;

function TVarStringStream.Read(var Buffer; Count: Integer): Integer;
begin
 if Memory <> Pointer(FStringPtr^) then
  SetPointer(Pointer(FStringPtr^), Length(FStringPtr^));
 Result := inherited Read(Buffer, Count); 
end;

procedure TVarStringStream.SetSize(NewSize: Integer);
begin
 SetLength(FStringPtr^, NewSize);
 SetPointer(Pointer(FStringPtr^), NewSize);
end;

function TVarStringStream.Write(const Buffer; Count: Integer): Integer;
var
 FPosition, Pos: Integer;
begin
 if Count > 0 then
 begin
  FPosition := Position;
  if FPosition >= 0 then
  begin
   Pos := FPosition + Count;
   if Pos > 0 then
   begin
    if Pos > Length(FStringPtr^) then SetSize(Pos);
    if Memory <> Pointer(FStringPtr^) then
     SetPointer(Pointer(FStringPtr^), Length(FStringPtr^));
    System.Move(Buffer, PByteArray(FStringPtr^)[FPosition], Count);
    Seek(Pos, soFromBeginning);
    Result := Count;
    Exit;
   end;
  end;
 end;
 Result := 0;
end;

(* TStreamIniFile *)

constructor TStreamIniFile.Create;
begin
 inherited Create('');
end;

constructor TStreamIniFile.Create(Source: TStream);
begin
 inherited Create('');
 LoadFromStream(Source);
end;

procedure TStreamIniFile.LoadFromFile(const FileName: String);
var
 Stream: TFileStream;
begin
 Stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamIniFile.LoadFromStream(Stream: TStream);
var
 List: TStringList;
begin
 List := TStringList.Create;
 try
  List.LoadFromStream(Stream);
  SetStrings(List);
 finally
  List.Free;
 end;
end;

procedure TStreamIniFile.SaveToFile(const FileName: String);
var
 Stream: TFileStream;
begin
 Stream := TFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamIniFile.SaveToStream(Stream: TStream);
var
 List: TStringList;
begin
 List := TStringList.Create;
 try
  GetStrings(List);
  List.SaveToStream(Stream);
 finally
  List.Free;
 end;
end;

procedure TStreamIniFile.UpdateFile;
begin
 if FileName <> '' then
  inherited else
  raise EIniFileException.Create(SFilenameNotSpecified);
end;

{ TVarWideStringStream }

constructor TVarWideStringStream.Create(AStringPtr: PWideString);
begin
 FStringPtr := AStringPtr;
 SetPointer(Pointer(AStringPtr^), Length(AStringPtr^) shl 1);
end;

function TVarWideStringStream.Read(var Buffer; Count: Integer): Integer;
begin
 if Memory <> Pointer(FStringPtr^) then
  SetPointer(Pointer(FStringPtr^), Length(FStringPtr^) shl 1);
 Result := inherited Read(Buffer, Count);
end;

procedure TVarWideStringStream.SetSize(NewSize: Integer);
begin
 SetLength(FStringPtr^, NewSize shr 1);
 SetPointer(Pointer(FStringPtr^), NewSize);
end;

function TVarWideStringStream.Write(const Buffer; Count: Integer): Integer;
var
 FPosition, Pos: Integer;
begin
 if Count > 0 then
 begin
  FPosition := Position;
  if FPosition >= 0 then
  begin
   Pos := FPosition + Count;
   if Pos > 0 then
   begin
    if Pos > Length(FStringPtr^) shl 1 then SetSize(Pos);
    if Memory <> Pointer(FStringPtr^) then
     SetPointer(Pointer(FStringPtr^), Length(FStringPtr^) shl 1);
    System.Move(Buffer, PByteArray(FStringPtr^)[FPosition], Count);
    Seek(Pos, soFromBeginning);
    Result := Count;
    Exit;
   end;
  end;
 end;
 Result := 0;
end;

{ TMemStream }

function TMemStream.Realloc(var NewCapacity: Integer): Pointer;
const
 MemoryDelta = 256; { Must be a power of 2 }
begin
 if (NewCapacity > 0) and (NewCapacity <> Size) then
  NewCapacity := (NewCapacity + (MemoryDelta - 1)) and not (MemoryDelta - 1);
 Result := Memory;
 if NewCapacity <> Capacity then
 begin
  if NewCapacity = 0 then
  begin
   FreeMem(Memory);
   Result := nil;
  end else
  begin
   if Capacity = 0 then
    GetMem(Result, NewCapacity)
   else
    ReallocMem(Result, NewCapacity);
  end;
 end;
end;

{ TAnsiStringList }

{$IFDEF FORCED_UNICODE}

constructor TAnsiStringList.Create;
begin
  inherited;

  SetEncoding(TEncoding.ANSI);
end;

{$ENDIF}

procedure TAnsiStringList.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(PWideChar(FileName), GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   LoadFromStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure TAnsiStringList.SaveToFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(PWideChar(FileName), GENERIC_READ or GENERIC_WRITE,
      0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   SaveToStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure SectionOperationAborted;
begin
 raise ESectionOperationAborted.Create(SOperationAborted);
end;

procedure SectionSignatureError(SectionOffset, ErrorOffset: Int64);
begin
 raise ESectionSignatureError.Create(SectionOffset, ErrorOffset, SSignatureError);
end;

procedure SectionHeaderError(SectionOffset, ErrorOffset: Int64);
begin
 raise ESectionHeaderError.Create(SectionOffset, ErrorOffset, SHeaderError);
end;

procedure SectionDataSizeError(SectionOffset, ErrorOffset: Int64);
begin
 raise ESectionDataSizeError.Create(SectionOffset, ErrorOffset, SDataSizeError);
end;

procedure SectionChecksumError(SectionOffset, ErrorOffset: Int64);
begin
 raise ESectionChecksumError.Create(SectionOffset, ErrorOffset, SChecksumError);
end;

{ TBaseSectionedList }

function TBaseSectionedList.AddChild(const AName: WideString): TBaseSectionedList;
begin
 Result := AddNode as TBaseSectionedList;
 Result.Name := AName;
end;

procedure TBaseSectionedList.Assign(Source: TNode);
var
 Src: TBaseSectionedList absolute Source;
begin
 if Source <> Self then
 begin
  inherited;
  Name := Src.FName;
 end;
end;

function TBaseSectionedList.CalculateChecksum: TChecksum;
var
 N: TNode;
 Lst: TBaseSectionedList absolute N;
begin
 if FMaxNameLength > 0 then
  Result := CalcCheckSum(Pointer(FName)^, Min(Length(FName), FMaxNameLength) * 2) else
  Result := CalcCheckSum(Pointer(FName)^, Length(FName) * 2);
 N := RootNode;
 while N <> nil do
 begin
  if N is TBaseSectionedList then
   Inc(Result, Lst.Checksum);
  N := N.Next;
 end;
end;

function TBaseSectionedList.CalculateDataSize: LongInt;
var
 N: TNode;
 Lst: TBaseSectionedList absolute N;
begin
 if FMaxNameLength > 0 then
  Result := Min(Length(FName), FMaxNameLength) * 2 else
  Result := Length(FName);
 N := RootNode;
 while N <> nil do
 begin
  if N is TBaseSectionedList then
  with Lst do
  begin
   Inc(Result, SizeOf(TSectionHeader));
   Inc(Result, FHeaderSize);
   Inc(Result, DataSize);
  end;
  N := N.Next;
 end;
end;

function TBaseSectionedList.CheckHeaderSize(var Size: LongInt): Boolean;
begin
 Result := Size = FHeaderSize;
end;

function TBaseSectionedList.CheckSignature(Value: LongWord): Boolean;
begin
 Result := Value = FSignature;
end;

procedure TBaseSectionedList.FillHeader(var Header: TSectionHeader);
begin
 FillChar(Header.dh, Header.shHeaderSize, 0);
end;


procedure TBaseSectionedList.FillSectionHeader(var Header: TSectionHeader);
begin
 Header.shSignature := FSignature;
 Header.shHeaderSize := FHeaderSize;
 Header.shDataSize := DataSize;
 Header.shChecksum := Checksum;
end;

function TBaseSectionedList.FindByContent(AChecksum: LongWord;
                           ADataSize: Integer{;
                           CheckIndex: Integer}): TBaseSectionedList;
var
 N: TNode;
begin
 {if CheckIndex >= 0 then
 begin
  N := Nodes[CheckIndex];
  if N <> nil then
   with N as TBaseSectionedList do
   begin
    if (AChecksum = Checksum) and (ADataSize = DataSize) then
    begin
     Result := TBaseSectionedList(N);
     Exit;
    end;
   end;
 end;      }
 if (AChecksum <> 0) or (ADataSize <> 0) then
 begin
  N := RootNode;
  while N <> nil do
  begin
   with N as TBaseSectionedList do
   begin
    if (AChecksum = Checksum) and (ADataSize = DataSize) then
    begin
     Result := TBaseSectionedList(N);
     Exit;
    end;
   end;
   N := N.Next;
  end;
 end;
 Result := nil;
end;

function TBaseSectionedList.FindIndexByFirstLetters(
  const ALetters: WideString): Integer;
var
 I: Integer;
begin
 for I := 0 to Count - 1 do
 begin
  with Nodes[I] as TBaseSectionedList do
   if Pos(ALetters, WideFormat('%d. %s', [I + 1, Name])) = 1 then
   begin
    Result := I;
    Exit;
   end;
 end;
 Result := -1;
end;

function TBaseSectionedList.FindByName(const AName: WideString): TBaseSectionedList;
begin
 Result := RootNode as TBaseSectionedList;
 while Result <> nil do
 begin
  if AName = Result.FName then Exit;
  Result := Result.Next as TBaseSectionedList;
 end;
end;

function TBaseSectionedList.GetTotalSize: LongInt;
begin
 Result := SizeOf(TSectionHeader) + FHeaderSize + DataSize;
end;

procedure TBaseSectionedList.Initialize;
begin
 inherited;
 FNodeClass := TBaseSectionedList;
 FAssignableClass := TBaseSectionedList;
end;

procedure TBaseSectionedList.LoadFromStream(Stream: TStream);
var
 Header: PSectionHeader;
 SavePos: Int64;
begin
 GetMem(Header, SizeOf(TSectionHeader));
 try
  FInternalFlags := [secLoading];
  OperationProgress(Stream, Header^, SPID_READ_START);
  try
   SavePos := Stream.Position;
   OperationProgress(Stream, Header^, SPID_READ_SEC_HEADER_START);
   if not ReadSectionHeader(Stream, Header^) then
    SectionSignatureError(SavePos, Stream.Position);
   OperationProgress(Stream, Header^, SPID_READ_SEC_HEADER_END);

   ReallocMem(Header, SizeOf(TSectionHeader) + Header.shHeaderSize);

   SavePos := Stream.Position;
   OperationProgress(Stream, Header^, SPID_READ_HEADER_START);
   if not ReadHeader(Stream, Header^) then
    SectionHeaderError(SavePos, Stream.Position);
   OperationProgress(Stream, Header^, SPID_READ_HEADER_END);

   SavePos := Stream.Position;
   OperationProgress(Stream, Header^, SPID_READ_DATA_START);
   ReadData(Stream, Header^);
   OperationProgress(Stream, Header^, SPID_READ_DATA_END);

   if not (secSkipChecksumTest in FInternalFlags) and (Header.shChecksum <> Checksum) then
    SectionChecksumError(SavePos, Stream.Position);

   if not (secSkipDataSizeTest in FInternalFlags) and
      (Header.shDataSize <> Stream.Position - SavePos) then
    SectionDataSizeError(SavePos, Stream.Position);
  except
   OperationProgress(Stream, Header^, SPID_READ_ERROR);
   raise;
  end;
  OperationProgress(Stream, Header^, SPID_READ_END);
 finally
  FInternalFlags := []; 
  FreeMem(Header);
 end;
end;

procedure TBaseSectionedList.OperationProgress(Stream: TStream;
  var Header: TSectionHeader; ProgressID, Value: Integer);
var
 p: TProgressRec;
begin
 p.Stream := Stream;
 p.Header := @Header;
 p.ProgressID := ProgressID;
 p.Value := Value;
 Progress(Self, p);
end;

procedure TBaseSectionedList.Progress(Sender: TObject; const Rec: TProgressRec);
begin
 if (Sender = Self) and
    (Rec.ProgressID = SPID_READ_SEC_HEADER_END) and
    (Owner is TBaseSectionedList) and
    ((Rec.Header.shHeaderSize <> FHeaderSize) or
     (Rec.Header.shSignature <> FSignature)) then
 begin
   Include(TBaseSectionedList(Owner).FInternalFlags, secSkipDataSizeTest);
   Include(TBaseSectionedList(Owner).FInternalFlags, secSkipChecksumTest);
 end;

 if Assigned(FOnProgress) then
  FOnProgress(Self, Sender, Rec);

 if Owner is TBaseSectionedList then
  TBaseSectionedList(Owner).Progress(Sender, Rec) else
end;

procedure TBaseSectionedList.Read(var Rec: TProgressRec; var Buffer; Size: Integer);
begin
 Rec.Stream.ReadBuffer(Buffer, Size);
 Rec.Value := Size;
 Progress(Self, Rec);
end;

procedure TBaseSectionedList.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 N: TNode;
 Lst: TBaseSectionedList absolute N;
 Len: Integer;
begin
 Len := Length(FName) shl 1;
 Stream.ReadBuffer(Pointer(FName)^, Len);
 OperationProgress(Stream, Header, SPID_READ_DATA, Len);

 N := RootNode;
 while N <> nil do
 begin
  if N is TBaseSectionedList then
   Lst.LoadFromStream(Stream);
 
  N := N.Next;
 end;
end;

function TBaseSectionedList.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
begin
 Result := Stream.Read(Header.dh, Header.shHeaderSize) = Header.shHeaderSize;
 if Result then
 begin
  OperationProgress(Stream, Header, SPID_READ_DATA_HEADER, Header.shHeaderSize);
  Clear;
  Count := ReadSetCount(Header);
 end;
end;

function TBaseSectionedList.ReadSectionHeader(Stream: TStream;
  var Header: TSectionHeader): Boolean;
begin
 Result := Stream.Read(Header, SizeOf(TSectionHeader)) = SizeOf(TSectionHeader);
 if Result then
 begin
  OperationProgress(Stream, Header, SPID_READ_SECTION_HEADER, SizeOf(TSectionHeader));
  Result := CheckSignature(Header.shSignature) and
            CheckHeaderSize(Header.shHeaderSize);
 end;
end;

function TBaseSectionedList.ReadSetCount(var Header: TSectionHeader): LongInt;
begin
 Result := 0;
end;

procedure TBaseSectionedList.SaveToStream(Stream: TStream);
var
 Header: PSectionHeader;
begin
 GetMem(Header, SizeOf(TSectionHeader));
 try
  FInternalFlags := [secSaving];
  OperationProgress(Stream, Header^, SPID_WRITE_START);
  try
   FillSectionHeader(Header^);

   OperationProgress(Stream, Header^, SPID_WRITE_SEC_HEADER_START);
   WriteSectionHeader(Stream, Header^);
   OperationProgress(Stream, Header^, SPID_WRITE_SEC_HEADER_END);

   ReallocMem(Header, SizeOf(TSectionHeader) + Header.shHeaderSize);

   FillHeader(Header^);

   OperationProgress(Stream, Header^, SPID_WRITE_HEADER_START);
   WriteHeader(Stream, Header^);
   OperationProgress(Stream, Header^, SPID_WRITE_HEADER_END);

   OperationProgress(Stream, Header^, SPID_WRITE_DATA_START);
   WriteData(Stream, Header^);
   OperationProgress(Stream, Header^, SPID_WRITE_DATA_END);

  except
   OperationProgress(Stream, Header^, SPID_WRITE_ERROR);
   raise;
  end;
  OperationProgress(Stream, Header^, SPID_WRITE_END);
 finally
  FInternalFlags := [];
  FreeMem(Header);
 end;
end;

procedure TBaseSectionedList.SetName(Value: WideString);

 function FindName(const Value: WideString): Boolean;
 var
  Node: TNode;
 begin
  Node := (Owner as TNodeList).RootNode;
  while Node <> nil do
  begin
   if Node <> Self then
    with Node as TBaseSectionedList do
     if FName = Value then
     begin
      Result := True;
      Exit;
     end;
   Node := Node.Next;
  end;
  Result := False;  
 end;
var
 NumName: WideString;
 L: Integer;
 Idx: Integer;
 P: PWideChar;
begin
 if not FNoRename and (Value = '') then
  Value := 'Unnamed';
 if FName <> Value then
 begin
  if FNoRename or (Owner = nil) then
  begin
   FName := Value;
  end else
  begin
   NumName := Value;
   L := Length(NumName);
   P := Addr(NumName[L]);
   while L > 0 do
   begin
    if (Word(P^) < Ord('0')) or (Word(P^) > Ord('9')) then
     Break;
    Dec(P);
    Dec(L);
   end;
   Inc(P);
   if (P^ <> #0) and TryStrToInt(P, Idx) then
    SetLength(Value, L) else
    Value := Value + ' ';
   Idx := 1;
   while FindName(NumName) do
   begin
    NumName := WideFormat('%s%d', [Value, Idx]);
    Inc(Idx);
   end;
   FName := NumName;
  end;
  if FMaxNameLength > 0 then
   SetLength(FName, Min(Length(FName), FMaxNameLength));
 end;
end;

procedure TBaseSectionedList.SetOnProgress(Value: TSectionProgressEvent);
begin
 FOnProgress := Value;
end;

procedure TBaseSectionedList.Write(var Rec: TProgressRec; var Buffer; Size: Integer);
begin
 Rec.Stream.WriteBuffer(Buffer, Size);
 Rec.Value := Size; 
 Progress(Self, Rec);
end;

procedure TBaseSectionedList.WriteData(Stream: TStream; var Header: TSectionHeader);
var
 N: TNode;
 SN: TBaseSectionedList absolute N;
 Len: Integer;
begin
 if FMaxNameLength > 0 then
  Len := Min(Length(FName), FMaxNameLength) shl 1 else
  Len := Length(FName);

 Stream.WriteBuffer(Pointer(FName)^, Len);
 OperationProgress(Stream, Header, SPID_WRITE_DATA, Len);

 N := RootNode;
 while N <> nil do
 begin
  if N is TBaseSectionedList then
   SN.SaveToStream(Stream);

  N := N.Next;
 end;
end;

procedure TBaseSectionedList.WriteHeader(Stream: TStream; var Header: TSectionHeader);
begin
 Stream.WriteBuffer(Header.dh, Header.shHeaderSize);
 OperationProgress(Stream, Header, SPID_WRITE_DATA_HEADER, Header.shHeaderSize);
end;

procedure TBaseSectionedList.WriteSectionHeader(Stream: TStream;
  var Header: TSectionHeader);
begin
 Stream.WriteBuffer(Header, SizeOf(TSectionHeader));
 OperationProgress(Stream, Header, SPID_WRITE_SECTION_HEADER, SizeOf(TSectionHeader));
end;

procedure TBaseSectionedList.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   LoadFromStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

procedure TBaseSectionedList.SaveToFile(const FileName: WideString);
var
 Stream: TStream;
 H: THandle;
begin
 H := CreateFileW(Pointer(FileName), GENERIC_READ or GENERIC_WRITE,
                  0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  Stream := THandleStream.Create(H);
  try
   SaveToStream(Stream);
  finally
   Stream.Free;
  end;
 finally
  FileClose(H);
 end;
end;

{ TSectionedList }

procedure TSectionedList.FillHeader(var Header: TSectionHeader);
var
 V1: ^TListDataHeaderV1;
begin
 if Header.shHeaderSize >= SizeOf(TListDataHeaderV1) then
 begin
  V1 := Addr(Header.dh);
  V1.lshItemsCount := Count;
 end;
end;

procedure TSectionedList.Initialize;
begin
 inherited;
 FHeaderSize := SizeOf(TListDataHeaderV1);
end;

function TSectionedList.ReadSetCount(var Header: TSectionHeader): LongInt;
begin
 if Header.shHeaderSize >= SizeOf(TListDataHeaderV1) then
  Result := PListDataHeaderV1(Addr(Header.dh)).lshItemsCount else
  Result := 0;
end;

{ TNamedList }

procedure TNamedList.FillHeader(var Header: TSectionHeader);
var
 V2: ^TListDataHeaderV2;
begin
 inherited;
 if Header.shHeaderSize >= SizeOf(TListDataHeaderV2) then
 begin
  V2 := Addr(Header.dh);
  if FMaxNameLength > 0 then
   V2.lshNameLength := Min(Length(FName), FMaxNameLength) else
   V2.lshNameLength := Length(FName);
 end;
end;

procedure TNamedList.Initialize;
begin
 inherited;
 FHeaderSize := SizeOf(TListDataHeaderV2);
 FMaxNameLength := High(Word);
end;

{ ESectionError }

constructor ESectionError.Create(const SecOfs, ErrOfs: Int64;
  const Msg: String);
begin
 SectionOffset := SecOfs;
 ErrorOffset := ErrOfs;
 inherited CreateFmt(Msg, [SecOfs, ErrOfs]);
end;

{ TConsoleInput }

constructor TConsoleInput.Create;
begin
 stdin := GetStdHandle(STD_INPUT_HANDLE);
 SetConsoleMode(stdin, OldConsoleMode(stdin) and
  not (ENABLE_LINE_INPUT or ENABLE_ECHO_INPUT));
end;

destructor TConsoleInput.Destroy;
begin
 SetConsoleMode(stdin,
  OldConsoleMode(stdin) or (ENABLE_LINE_INPUT or ENABLE_ECHO_INPUT));
 inherited;
end;

{$WARN SYMBOL_PLATFORM OFF}

function TConsoleInput.GetCh: WideChar;
var
 CharsRead: Cardinal;
begin
 Win32Check(ReadConsoleW(stdin, @Result, 1, CharsRead, nil));
end;

function TConsoleInput.KeyPressed: Boolean;
var
 i, numEvents: Cardinal;
 events: array of TInputRecord;
begin
 Result := False;
 Win32Check(GetNumberOfConsoleInputEvents(stdin, numEvents));
 if numEvents > 0 then
 begin
  SetLength(events, numEvents);
  Win32Check(PeekConsoleInputW(stdin, events[0], numEvents, numEvents));
  for i:= 0 to numEvents - 1 do
   if (events[i].EventType = key_event) and
      (events[i].Event.KeyEvent.bKeyDown) then
   begin
    Result := True;
    Break;
   end;
 end;
end;

function TConsoleInput.OldConsoleMode(handle: THandle): Cardinal;
begin
 Win32Check(GetConsoleMode(handle, Result));
end;

{ TConsoleOutput }

procedure TConsoleOutput.AbortCheck(Sender: TObject; var Abort: Boolean);
begin
  Abort := AbortOnKeyPressEnabled and KeyPressed;
end;

constructor TConsoleOutput.Create(const LogFileName: String);
begin
  inherited Create;

  if CheckFileForWrite(LogFileName) in [cfNotExist, cfCanWrite] then
  begin
    AssignFile(FLog, LogFileName);
    Rewrite(FLog);
    Write(FLog, #$EF#$BB#$BF);
    FTextAssigned := True;
  end;

  FOptions := [cooAbortOnKeyPress];
end;

destructor TConsoleOutput.Destroy;
begin
  if FTextAssigned then
    CloseFile(FLog);
  inherited;
end;

function TConsoleOutput.GetCanAbort: Boolean;
begin
  Result := cooAbortOnKeyPress in FOptions;
end;

function TConsoleOutput.GetSkipDups: Boolean;
begin
  Result := cooSkipDuplicates in FOptions;
end;

procedure TConsoleOutput.Output(Sender: TObject; const Text: WideString);
begin
 if not SkipDuplicatesEnabled or (Text <> FLast) then
 begin
  Writeln(Text);
  if FTextAssigned then
   Writeln(FLog, UTF16toUTF8(Text));
  FLast := Text;
 end;
end;

procedure TConsoleOutput.SetCanAbort(Value: Boolean);
begin
  if Value then
    Include(FOptions, cooAbortOnKeyPress)
  else
    Exclude(FOptions, cooAbortOnKeyPress);
end;

procedure TConsoleOutput.SetSkipDups(Value: Boolean);
begin
  if Value then
    Include(FOptions, cooSkipDuplicates)
  else
    Exclude(FOptions, cooSkipDuplicates);
end;

procedure TConsoleOutput.WaitKeyPress;
begin
  repeat until not KeyPressed;
  repeat until KeyPressed;
end;

{ TFileStreamEx }

constructor TFileStreamEx.Create(const FileName: WideString; Mode: Integer;
  RemoveOnDestroy: Boolean);
var
  H: THandle;
begin
  FFileName := FileName;
  FRemoveOnDestroy := RemoveOnDestroy;

  case Mode of
    fmCreate:
      H := MyUtils.NewFile(PWideChar(FFileName));
    fmOpenRead, fmOpenRead or fmShareDenyWrite, fmOpenRead or fmShareDenyNone:
      H := MyUtils.OpenFile(PWideChar(FFileName));
    fmOpenReadWrite:
      H := MyUtils.ModifyFile(PWideChar(FFileName));
    else
      H := INVALID_HANDLE_VALUE;
  end;
  inherited Create(H);
end;

destructor TFileStreamEx.Destroy;
begin
  if THandle(FHandle) <> INVALID_HANDLE_VALUE then
    FileClose(FHandle);

  if FRemoveOnDestroy then
    DeleteFile(FFileName);
  inherited;
end;

initialization
 KeyboardHook := SetWindowsHookEx(WH_KEYBOARD, @KeyboardProc, 0, GetCurrentThreadID);
 MouseHook := SetWindowsHookEx(WH_MOUSE, @MouseProc, 0, GetCurrentThreadID);
finalization
 if MouseHook <> 0 then UnhookWindowsHookEx(MouseHook);
 if KeyboardHook <> 0 then UnhookWindowsHookEx(KeyboardHook);
end.
