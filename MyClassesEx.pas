unit MyClassesEx;

interface

{$WARN SYMBOL_PLATFORM OFF}

uses
 Windows, SysUtils, MyUtils, Classes, NodeLst, MyClasses,
 IniFiles, UnicodeUtils;

type
 TPathInspector = class;
 TProcessProc = procedure(const Path: WideString; const SR: TSearchRecW);
 TProcessEvent = procedure(Sender: TPathInspector;
                           const Path: WideString;
                           const SR: TSearchRecW) of Object;

 TPathInspector = class
  private
    FPath: WideString;
    FMask: WideString;
    FFileAttr: Integer;
    FProcessSubFolders: Boolean;
    FOnProcess: TProcessEvent;
    FProcessProc: TProcessProc;
    procedure SetPath(Value: WideString);
  protected
    procedure ProcessFile(const Path: WideString; const SR: TSearchRecW); virtual;
    procedure Initialize; virtual;
  public
    property InitialPath: WideString
        read FPath
       write SetPath;
    property Mask: WideString
        read FMask
       write FMask;
    property FileAttr: Integer
        read FFileAttr
       write FFileAttr;
    property ProcessSubFolders: Boolean
        read FProcessSubFolders
       write FProcessSubFolders;
    property ProcessProc: TProcessProc
        read FProcessProc
       write FProcessProc;
    property OnProcess: TProcessEvent
        read FOnProcess
       write FOnProcess;

    procedure Run;
    constructor Create; overload;
    constructor Create(const Path, Mask: WideString; ProcessSubFolders: Boolean;
      Attr: Integer = faArchive or faReadOnly or faHidden); overload;
    destructor Destroy; override;
 end;

 TUTF8StringList = class(TWideStringList)
  public
    procedure SaveToStream_BOM(Stream: TStream; WithBOM: Boolean); override;
  {$IFDEF FORCED_UNICODE}
    constructor Create;
  {$ENDIF}
 end;

 TCodePagedStringList = class(TWideStringList)
  private
    FCodePage: LongWord;
    FRealCodePage: LongWord;
    FLastCodePage: LongWord;
    FLoadWithBOM: Boolean;
    FSaveWithBOM: Boolean;
    procedure SetCodePage(Value: LongWord);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    property CodePage: LongWord read FCodePage write SetCodePage;
    property LastCodePage: LongWord read FLastCodePage;
    property LoadWithBOM: Boolean read FLoadWithBOM write FLoadWithBOM;
    property SaveWithBOM: Boolean read FSaveWithBOM write FSaveWithBOM;

    constructor Create; overload;
    constructor Create(CodePage: LongWord); overload;
    procedure LoadFromFile(const FileName: WideString); override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure LoadFromStream_BOM(Stream: TStream; WithBOM: Boolean); override;
    procedure SaveToStream(Stream: TStream); override;
    procedure SaveToStream_BOM(Stream: TStream; WithBOM: Boolean); override;

    procedure Assign(Source: TPersistent); override;
 end;

 TNodeListEx = class(TNodeList)
  public
    procedure LoadFromStream(Stream: TStream); virtual; abstract;
    procedure SaveToStream(Stream: TStream); virtual; abstract;
    procedure LoadFromFile(const FileName: WideString);
    procedure SaveToFile(const FileName: WideString);
 end;

 TClassListItem = class(TNode)
  private
    FClass: TClass;
  protected
    procedure Initialize; override;
  public
    property AClass: TClass read FClass;

    constructor Create(AClass: TClass);
    procedure Assign(Source: TNode); override;
 end;

 TClassList = class(TNodeList)
  private
    function GetClass(Index: Integer): TClassListItem;
  protected
    procedure Initialize; override;
    procedure AssignAdd(Source: TNode); override;
  public
    property Classes[Index: Integer]: TClassListItem read GetClass;

    function AddClass(AClass: TClass): TClassListItem;
    procedure RegisterClasses(const AList: array of TClass);
    function Find(const Name: String): TClassListItem; overload;
    function Find(ClType: TClass): TClassListItem; overload;
 end;

{$IFDEF FORCED_UNICODE}

 TStreamIniFileW = class(TStreamIniFile)
  private
    function GetFileName: WideString;
    procedure SetFileName(const Value: WideString);
  public
    property FileName: WideString read GetFileName write SetFileName;

    constructor Create; overload;
    constructor Create(Source: TStream); overload;
    constructor Create(const AFileName: WideString); overload;

    function ReadBool(const Section, Ident: String; Default: Boolean): Boolean; override;
    procedure WriteInteger(const Section, Ident: WideString; Value: Longint; HexDigits: Integer = 0); reintroduce;
    function ReadInt64(const Section, Ident: WideString; const Default: Int64): Int64;
    procedure WriteInt64(const Section, Ident: WideString; const Value: Int64; HexDigits: Integer = 0);

    procedure UpdateFile; override;
 end;

{$ELSE}

 TStreamIniFileW = class(TStreamIniFile)
  private
    FFileNameW: WideString;
    function AddSection(const Section: AnsiString): TStrings;
    procedure SetFileName(const Value: WideString);
  public
    property FileName: WideString read FFileNameW write SetFileName;

    constructor Create; overload;
    constructor Create(Source: TStream); overload;
    constructor Create(const AFileName: WideString); overload;
    procedure UpdateFile; override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;
    procedure LoadFromFile(const FileName: WideString);
              reintroduce; overload; virtual;
    procedure SaveToFile(const FileName: WideString);
              reintroduce; overload; virtual;
    procedure SetStrings(List: TWideStrings);
    procedure WriteString(const Section, Ident, Value: WideString); reintroduce;
    function ReadString(const Section, Ident, Default: WideString): WideString; reintroduce;
    function ReadInteger(const Section, Ident: WideString; Default: Longint): Longint; reintroduce;
    function ReadInt64(const Section, Ident: WideString; const Default: Int64): Int64; reintroduce;
    procedure WriteInteger(const Section, Ident: WideString; Value: Longint; HexDigits: Integer = 0); reintroduce;
    procedure WriteInt64(const Section, Ident: WideString; const Value: Int64; HexDigits: Integer = 0);
    function ReadBool(const Section, Ident: WideString; Default: Boolean): Boolean; reintroduce;
    procedure WriteBool(const Section, Ident: WideString; Value: Boolean); reintroduce;
    function ReadBinaryStream(const Section, Name: WideString; Value: TStream): Integer; reintroduce;
    function ReadDate(const Section, Name: WideString; Default: TDateTime): TDateTime; reintroduce;
    function ReadDateTime(const Section, Name: WideString; Default: TDateTime): TDateTime; reintroduce;
    function ReadFloat(const Section, Name: WideString; Default: Double): Double; reintroduce;
    function ReadTime(const Section, Name: WideString; Default: TDateTime): TDateTime; reintroduce;
    procedure WriteBinaryStream(const Section, Name: WideString; Value: TStream); reintroduce;
    procedure WriteDate(const Section, Name: WideString; Value: TDateTime); reintroduce;
    procedure WriteDateTime(const Section, Name: WideString; Value: TDateTime); reintroduce;
    procedure WriteFloat(const Section, Name: WideString; Value: Double); reintroduce;
    procedure WriteTime(const Section, Name: WideString; Value: TDateTime); reintroduce;
    procedure ReadSection(const Section: WideString; Strings: TWideStrings); reintroduce;
    procedure ReadSections(Strings: TWideStrings); reintroduce;
    procedure ReadSectionValues(const Section: WideString; Strings: TWideStrings); reintroduce;
    procedure EraseSection(const Section: WideString); reintroduce;
    procedure DeleteKey(const Section, Ident: WideString); reintroduce;
    function ValueExists(const Section, Ident: WideString): Boolean;
    function SectionExists(const Section: WideString): Boolean;

    destructor Destroy; override;
 end;

 THashedStringListW = class(TStringList)
  private
    FValueHash: TStringHash;
    FNameHash: TStringHash;
    FValueHashValid: Boolean;
    FNameHashValid: Boolean;
    procedure UpdateValueHash;
    procedure UpdateNameHash;
  protected
    procedure Changed; override;
    function CompareStrings(const S1, S2: String): Integer; override;
  public
    destructor Destroy; override;
    function IndexOf(const S: String): Integer; override;
    function IndexOfName(const Name: String): Integer; override;
 end;

{$ENDIF}

 EClassListError = class(Exception);

const
  BoolStr: array[Boolean] of WideString = ('False', 'True');

implementation

uses
  HexUnit
{$IFNDEF FORCED_UNICODE}
  , TntClasses
{$ENDIF};

constructor TPathInspector.Create;
begin
 Initialize;
 InitialPath := WideGetCurrentDir;
 FMask := '*.*';
end;

constructor TPathInspector.Create(const Path, Mask: WideString;
                                  ProcessSubFolders: Boolean; Attr: Integer);
begin
 Initialize;
 InitialPath := Path;
 FMask := Mask;
 FFileAttr := Attr;
 FProcessSubFolders := ProcessSubFolders;
end;

destructor TPathInspector.Destroy;
begin
 Finalize(FPath);
 Finalize(FMask);
 inherited;
end;

procedure TPathInspector.Initialize;
begin
 FFileAttr := faArchive or faHidden or faReadOnly;
end;

procedure TPathInspector.ProcessFile(const Path: WideString;
                                       const SR: TSearchRecW);
begin
 If Assigned(FOnProcess) then FOnProcess(Self, Path, SR);
 If Assigned(FProcessProc) then FProcessProc(Path, SR);
end;

procedure TPathInspector.Run;
procedure Process(const Pattern: WideString);
Var PDir, Mask: WideString; SR: TSearchRecW;
begin
 PDir := WideExtractFilePath(Pattern);
 Mask := WideExtractFileName(Pattern);
 If WideFindFirst(Pattern, FFileAttr, SR) = 0 then
 begin
  Repeat
   ProcessFile(PDir, SR);
  Until WideFindNext(SR) <> 0;
  WideFindClose(SR);
 end;
 If FProcessSubFolders and (WideFindFirst(PDir+'*.*', faDirectory, SR) = 0) then
 begin
  Repeat
   If (SR.Attr and faDirectory <> 0) and
      (SR.Name <> '.') and (SR.Name <> '..') then
   Process(PDir + SR.Name + '\' + Mask);
  Until WideFindNext(SR) <> 0;
  WideFindClose(SR);
 end;
end;
begin
 Process(FPath + FMask);
end;

procedure TPathInspector.SetPath(Value: WideString);
begin
 FPath := WideIncludeTrailingPathDelimiter(Value);
end;

procedure TUTF8StringList.SaveToStream_BOM(Stream: TStream; WithBOM: Boolean);
{$IFNDEF FORCED_UNICODE}
var
 S: AnsiString;
begin
  if WithBOM then
    Stream.WriteBuffer(UTF8BOM, SizeOf(UTF8BOM));
  S := UTF16toUTF8(GetTextStr);
  Stream.WriteBuffer(Pointer(S)^, Length(S));
end;
{$ELSE}
begin
  if WithBOM then
    Stream.WriteBuffer(UTF8BOM, SizeOf(UTF8BOM));

  inherited SaveToStream_BOM(Stream, False);
end;

constructor TUTF8StringList.Create;
begin
  inherited Create;
  SetEncoding(TEncoding.UTF8);
end;
{$ENDIF}

procedure TNodeListEx.LoadFromFile(const FileName: WideString);
var Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TNodeListEx.SaveToFile(const FileName: WideString);
var Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TClassListItem.Assign(Source: TNode);
begin
 inherited;
 FClass := TClassListItem(Source).FClass;
end;

constructor TClassListItem.Create(AClass: TClass);
begin
 inherited Create;
 FClass := AClass;
end;

procedure TClassListItem.Initialize;
begin
 FAssignableClass := TClassListItem;
end;

function TClassList.AddClass(AClass: TClass): TClassListItem;
begin
 if AClass = NIL then raise EClassListError.Create('AClass must not be nil');
 Result := TClassListItem.Create(AClass);
 AddCreated(Result, True);
end;

procedure TClassList.AssignAdd(Source: TNode);
var S: String;
begin
 if (Source <> NIL) and (Source is TClassListItem) then
  AddClass(TClassListItem(Source).FClass) else
 begin
  if Source = NIL then S := 'nil' else S := Source.ClassName;
  raise EClassListError.CreateFmt(SAssignError, [S, ClassName]);
 end;
end;

function TClassList.Find(const Name: String): TClassListItem;
var I: Integer;
begin
 for I := 0 to Count - 1 do
 begin
  Result := Classes[I];
  if (Result <> NIL) and (Result.FClass.ClassName = Name) then Exit;
 end;
 Result := NIL;
end;

function TClassList.Find(ClType: TClass): TClassListItem;
var I: Integer;
begin
 for I := 0 to Count - 1 do
 begin
  Result := Classes[I];
  if (Result <> NIL) and (Result.FClass = ClType) then Exit;
 end;
 Result := NIL;
end;

function TClassList.GetClass(Index: Integer): TClassListItem;
begin
 Result := Nodes[Index] as TClassListItem;
end;

procedure TClassList.Initialize;
begin
 FNodeClass := TClassListItem;
 FAssignableClass := TClassList;
end;

procedure TClassList.RegisterClasses(const AList: array of TClass);
var I: Integer;
begin
 for I := 0 to Length(AList) - 1 do AddClass(AList[I]);
end;

{$IFNDEF FORCED_UNICODE}

type
 TMemIniFileHack = class(TCustomIniFile)
  private
    FSections: TStringList;
 end;

{ TStreamIniFileW }

function TStreamIniFileW.AddSection(const Section: AnsiString): TStrings;
begin
 Result := THashedStringListW.Create;
 try
  THashedStringListW(Result).CaseSensitive := CaseSensitive;
  TMemIniFileHack(Self).FSections.AddObject(Section, Result);
 except
  Result.Free;
  raise;
 end;
end;

constructor TStreamIniFileW.Create(const AFileName: WideString);
begin
 Create;
 SetFileName(AFileName);
end;

constructor TStreamIniFileW.Create;
begin
 inherited Create('');
 with TMemIniFileHack(Self) do
 begin
  FSections.Free;
  FSections := THashedStringListW.Create;
  FSections.CaseSensitive := False;
 end;
end;

constructor TStreamIniFileW.Create(Source: TStream);
begin
 Create;
 LoadFromStream(Source);
end;

procedure TStreamIniFileW.DeleteKey(const Section, Ident: WideString);
begin
 inherited DeleteKey(UTF16toUTF8(Section), UTF16toUTF8(Ident));
end;

destructor TStreamIniFileW.Destroy;
begin
 try
  UpdateFile;
 except
 end;
 inherited Destroy;
end;

procedure TStreamIniFileW.EraseSection(const Section: WideString);
begin
 inherited EraseSection(UTF16toUTF8(Section));
end;

procedure TStreamIniFileW.LoadFromFile(const FileName: WideString);
var
 Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamIniFileW.LoadFromStream(Stream: TStream);
var
 List: TWideStringList;
begin
 List := TWideStringList.Create;
 try
  List.LoadFromStream(Stream);
  SetStrings(List);
 finally
  List.Free;
 end;
end;

function TStreamIniFileW.ReadBinaryStream(const Section, Name: WideString;
  Value: TStream): Integer;
begin
 Result := inherited ReadBinaryStream(UTF16toUTF8(Section),
                     UTF16toUTF8(Name), Value);
end;

function TStreamIniFileW.ReadBool(const Section, Ident: WideString;
  Default: Boolean): Boolean;
var
 Str: WideString;
begin
 Str := ReadString(Section, Ident, BoolStr[Default]);
 if Str = BoolStr[False] then
  Result := False else
 if Str = BoolStr[True] then
  Result := True else
  Result := StrToIntDef(Str, 0) <> 0;
end;

function TStreamIniFileW.ReadDate(const Section, Name: WideString;
  Default: TDateTime): TDateTime;
begin
 Result := inherited ReadDate(UTF16toUTF8(Section),
                     UTF16toUTF8(Name), Default);
end;

function TStreamIniFileW.ReadDateTime(const Section, Name: WideString;
  Default: TDateTime): TDateTime;
begin
 Result := inherited ReadDateTime(UTF16toUTF8(Section),
                     UTF16toUTF8(Name), Default);
end;

function TStreamIniFileW.ReadFloat(const Section, Name: WideString;
  Default: Double): Double;
begin
 Result := inherited ReadFloat(UTF16toUTF8(Section),
                     UTF16toUTF8(Name), Default);
end;

function TStreamIniFileW.ReadInt64(const Section, Ident: WideString;
  const Default: Int64): Int64;
var
 E: Integer;
 Str: WideString;
begin
 Str := ReadString(Section, Ident, '');
 if Str <> '' then
 begin
  Val(Str, Result, E);
  if E = 0 then
   Exit;
 end;
 Result := Default;
end;

function TStreamIniFileW.ReadInteger(const Section, Ident: WideString;
  Default: Integer): Longint;
begin
 Result := inherited ReadInteger(UTF16toUTF8(Section),
                     UTF16toUTF8(Ident), Default);
end;

procedure TStreamIniFileW.ReadSection(const Section: WideString;
  Strings: TWideStrings);
var
 Temp: TStringList;
begin
 Temp := TStringList.Create;
 try
  inherited ReadSection(UTF16toUTF8(Section), Temp);
  Strings.Text := UTF8toUTF16(Temp.Text);
 finally
  Temp.Free;
 end;
end;

procedure TStreamIniFileW.ReadSections(Strings: TWideStrings);
var
 Temp: TStringList;
begin
 Temp := TStringList.Create;
 try
  inherited ReadSections(Temp);
  Strings.Text := UTF8toUTF16(Temp.Text);
 finally
  Temp.Free;
 end;
end;

procedure TStreamIniFileW.ReadSectionValues(const Section: WideString;
  Strings: TWideStrings);
var
 Temp: TStringList;
begin
 Temp := TStringList.Create;
 try
  inherited ReadSectionValues(UTF16toUTF8(Section), Temp);
  Strings.Text := UTF8toUTF16(Temp.Text);
 finally
  Temp.Free;
 end;
end;

function TStreamIniFileW.ReadString(const Section, Ident,
  Default: WideString): WideString;
var
 I: Integer;
 Id: AnsiString;
 Strings: TStrings;
begin
 with TMemIniFileHack(Self) do
 begin
  I := FSections.IndexOf(UTF16toUTF8(Section));
  if I >= 0 then
  begin
   Strings := TStrings(FSections.Objects[I]);
   Id := UTF16toUTF8(Ident);
   I := Strings.IndexOfName(Id);
   if I >= 0 then
   begin
    Result := UTF8toUTF16(Copy(Strings[I], Length(Id) + 2, MaxInt));
    Exit;
   end;
  end;
  Result := Default;
 end;
end;

function TStreamIniFileW.ReadTime(const Section, Name: WideString;
  Default: TDateTime): TDateTime;
begin
 Result := inherited ReadTime(UTF16toUTF8(Section), UTF16toUTF8(Name), Default);
end;

procedure TStreamIniFileW.SaveToFile(const FileName: WideString);
var
 Stream: TWideFileStream;
begin
 Stream := TWideFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TStreamIniFileW.SaveToStream(Stream: TStream);
var
 List: TStringList;
begin
 List := TStringList.Create;
 try
  GetStrings(List);
  Stream.WriteBuffer(UTF8BOM, SizeOf(UTF8BOM));
  List.SaveToStream(Stream);
 finally
  List.Free;
 end;
end;

function TStreamIniFileW.SectionExists(const Section: WideString): Boolean;
begin
 Result := inherited SectionExists(UTF16toUTF8(Section));
end;

procedure TStreamIniFileW.SetFileName(const Value: WideString);
begin
 if FFileNameW <> Value then
 begin
  UpdateFile;
  FFileNameW := Value;
  if (Value <> '') and WideFileExists(Value) then
   LoadFromFile(Value) else
   Clear;
 end;
end;

procedure TStreamIniFileW.SetStrings(List: TWideStrings);
var
  I, J: Integer;
  S, X: AnsiString;
  P: PChar;
  W: PWord absolute P;
  Strings: TStrings;
begin
 Clear;
 Strings := nil;
 for I := 0 to List.Count - 1 do
 begin
  S := Trim(UTF16toUTF8(List[I]));
  if (S <> '') and (S[1] <> ';') then
  begin
   if (S[1] = '[') and (S[Length(S)] = ']') then
   begin
    Delete(S, 1, 1);
    SetLength(S, Length(S) - 1);
    Strings := AddSection(Trim(S));
   end else if Strings <> NIL then
   begin
    J := Pos('=', S);
    if J > 0 then // remove spaces before and after '='
    begin
     X := Trim(Copy(S, J + 1, MaxInt));
     if X <> '' then
     begin
      if X[1] <> '"' then X := '"' + X + '"';
      P := Pointer(X);
      while P^ <> #0 do
      begin
       if W^ = $225C then
        W^ := $2222;
       Inc(P);
      end;
      Strings.Add(Trim(Copy(S, 1, J - 1)) + '=' + AnsiDequotedStr(X, '"'));
     end else Strings.Add(Trim(Copy(S, 1, J - 1)) + '=');
    end else Strings.Add(S);
   end;
  end;
 end;
end;

procedure TStreamIniFileW.UpdateFile;
begin
 if FFileNameW <> '' then
 begin
  if (TMemIniFileHack(Self).FSections.Count > 0) and
     MakeDirs(FFileNameW) then
   SaveToFile(FFileNameW);
 end;
end;

function TStreamIniFileW.ValueExists(const Section,
  Ident: WideString): Boolean;
begin
 Result := inherited ValueExists(UTF16toUTF8(Section), UTF16toUTF8(Ident));
end;

procedure TStreamIniFileW.WriteBinaryStream(const Section,
  Name: WideString; Value: TStream);
begin
 inherited WriteBinaryStream(UTF16toUTF8(Section), UTF16toUTF8(Name), Value);
end;

procedure TStreamIniFileW.WriteBool(const Section, Ident: WideString;
  Value: Boolean);
const
 Values: array[Boolean] of WideString = ('0', '1');
begin
 WriteString(Section, Ident, Values[Value <> False]);
end;

procedure TStreamIniFileW.WriteDate(const Section, Name: WideString;
  Value: TDateTime);
begin
 WriteString(Section, Name, DateToStr(Value));
end;

procedure TStreamIniFileW.WriteDateTime(const Section, Name: WideString;
  Value: TDateTime);
begin
 WriteString(Section, Name, DateTimeToStr(Value));
end;

procedure TStreamIniFileW.WriteFloat(const Section, Name: WideString;
  Value: Double);
begin
 WriteString(Section, Name, FloatToStr(Value));
end;

procedure TStreamIniFileW.WriteInt64(const Section, Ident: WideString;
  const Value: Int64; HexDigits: Integer);
begin
 if HexDigits > 0 then
  WriteString(Section, Ident, IntTo0xHex(Value, HexDigits)) else
  WriteString(Section, Ident, IntToStr(Value));
end;

procedure TStreamIniFileW.WriteInteger(const Section, Ident: WideString;
  Value, HexDigits: Integer);
begin
 if HexDigits > 0 then
  WriteString(Section, Ident, '0x' + IntToHex(Value, HexDigits)) else
  WriteString(Section, Ident, IntToStr(Value));
end;

procedure TStreamIniFileW.WriteString(const Section, Ident, Value: WideString);
var
 I: Integer;
 S, Id: AnsiString;
 Strings: TStrings;
begin
 with TMemIniFileHack(Self) do
 begin
  S := UTF16toUTF8(Section);
  I := FSections.IndexOf(S);
  if I >= 0 then
   Strings := TStrings(FSections.Objects[I]) else
   Strings := AddSection(S);
  Id := UTF16toUTF8(Ident);
  if (Value <> '') and ((Value[1] <= #32) or (Value[Length(Value)] <= #32)) then
   S := UTF16toUTF8('"' + Value + '"') else
   S := UTF16toUTF8(Value);
  S := Id + '=' + S;
  I := Strings.IndexOfName(Id);
  if I >= 0 then
   Strings[I] := S else
   Strings.Add(S);
 end;
end;

procedure TStreamIniFileW.WriteTime(const Section, Name: WideString;
  Value: TDateTime);
begin
 WriteString(Section, Name, TimeToStr(Value));
end;

{ THashedStringListW }

procedure THashedStringListW.Changed;
begin
 inherited Changed;
 FValueHashValid := False;
 FNameHashValid := False;
end;

function THashedStringListW.CompareStrings(const S1, S2: AnsiString): Integer;
begin
 if CaseSensitive then
  Result := WideCompareStr(UTF8toUTF16(S1), UTF8toUTF16(S2)) else
  Result := WideCompareText(UTF8toUTF16(S1), UTF8toUTF16(S2))
end;

destructor THashedStringListW.Destroy;
begin
 FValueHash.Free;
 FNameHash.Free;
 inherited Destroy;
end;

function THashedStringListW.IndexOf(const S: AnsiString): Integer;
begin
 UpdateValueHash;
 if not CaseSensitive then
  Result := FValueHash.ValueOf(UTF16toUTF8(WideUpperCase(UTF8toUTF16(S)))) else
  Result := FValueHash.ValueOf(S);
end;

function THashedStringListW.IndexOfName(const Name: AnsiString): Integer;
begin
 UpdateNameHash;
 if not CaseSensitive then
  Result := FNameHash.ValueOf(UTF16toUTF8(WideUpperCase(UTF8toUTF16(Name))))else
  Result := FNameHash.ValueOf(Name);
end;

procedure THashedStringListW.UpdateNameHash;
var
 I: Integer;
 P: Integer;
 Key: WideString;
begin
 if FNameHashValid then Exit;

 if FNameHash = nil then
  FNameHash := TStringHash.Create else
  FNameHash.Clear;
 for I := 0 to Count - 1 do
 begin
  Key := UTF8toUTF16(Get(I));
  P := Pos(NameValueSeparator, Key);
  if P <> 0 then
  begin
   if not CaseSensitive then
    Key := WideUpperCase(Copy(Key, 1, P - 1)) else
    Key := Copy(Key, 1, P - 1);
   FNameHash.Add(UTF16toUTF8(Key), I);
  end;
 end;
 FNameHashValid := True;
end;

procedure THashedStringListW.UpdateValueHash;
var
 I: Integer;
begin
 if FValueHashValid then Exit;

 if FValueHash = nil then
  FValueHash := TStringHash.Create else
  FValueHash.Clear;
 for I := 0 to Count - 1 do
  if not CaseSensitive then
   FValueHash.Add(UTF16toUTF8(WideUpperCase(UTF8toUTF16(Self[I]))), I) else
   FValueHash.Add(Self[I], I);
 FValueHashValid := True;
end;

{$ENDIF} // TStreamIniFileW with Tnt

{ TCodePagedStringList }

constructor TCodePagedStringList.Create;
begin
 inherited;
 SetCodePage(GetACP);
 FLoadWithBOM := True;
 FSaveWithBOM := True;
end;

procedure TCodePagedStringList.Assign(Source: TPersistent);
begin
  if Source is TCodePagedStringList then
  begin
    with TCodePagedStringList(Source) do
    begin
      Self.FCodePage := FCodePage;
      Self.FRealCodePage := FRealCodePage;
      Self.FLastCodePage := FLastCodePage;
      Self.FLoadWithBOM := FLoadWithBOM;
      Self.FSaveWithBOM := FSaveWithBOM;
    end;
  end;
  
  inherited;
end;

procedure TCodePagedStringList.AssignTo(Dest: TPersistent);
begin
  Dest.Assign(Self);
end;

constructor TCodePagedStringList.Create(CodePage: LongWord);
begin
 inherited Create;
 SetCodePage(CodePage);
 FLoadWithBOM := True;
 FSaveWithBOM := True;
end;

procedure TCodePagedStringList.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TCodePagedStringList.LoadFromStream(Stream: TStream);
begin
 LoadFromStream_BOM(Stream, FLoadWithBOM);
end;

procedure TCodePagedStringList.LoadFromStream_BOM(Stream: TStream;
  WithBOM: Boolean);
var
 Flags: LongWord;
begin
 BeginUpdate;
 try
  Flags := FCodePage;
  if FLoadWithBOM then
   Flags := Flags or CP_WideBOM;
  if WithBOM then
   Flags := Flags or CP_MbcsBOM;

  SetTextStr(ReadCodePagedString(Stream, Flags, FLastCodePage));
 finally
  EndUpdate;
 end;
end;

procedure TCodePagedStringList.SaveToStream(Stream: TStream);
begin
 SaveToStream_BOM(Stream, FSaveWithBOM);
end;

procedure TCodePagedStringList.SaveToStream_BOM(Stream: TStream;
  WithBOM: Boolean);
var
 Flags: Cardinal;
begin
 Flags := FCodePage;
 if FSaveWithBOM then
  Flags := Flags or CP_WideBOM;
 if WithBOM then
  Flags := Flags or CP_MbcsBOM;
 WriteCodePagedString(GetTextStr, Stream, Flags);
end;

procedure TCodePagedStringList.SetCodePage(Value: LongWord);
var
{$IFDEF FORCED_UNICODE}
  Encoding: TEncoding;
{$ELSE}
  Ptr: ^TObject;
{$ENDIF}
begin
  FCodePage := Value;
  if not ((Value = CP_UTF7) or (Value = CP_UTF8) or IsValidCodePage(Value)) then
    Value := GetACP;
  FRealCodePage := Value;
{$IFNDEF FORCED_UNICODE}
  AnsiStrings.Free;
  Ptr := Addr(AnsiStrings);
  Ptr^ := TAnsiStringsForWideStringsAdapter.Create(Self, Value);
{$ELSE}
  Encoding := TEncoding.GetEncoding(Value);
  try
    SetEncoding(Encoding);
  finally
    Encoding.Free;
  end;
{$ENDIF}
end;

{$IFDEF FORCED_UNICODE}

{ TStreamIniFileW }

constructor TStreamIniFileW.Create(Source: TStream);
begin
  Create;

  LoadFromStream(Source);
end;

constructor TStreamIniFileW.Create;
begin
  inherited Create('');

  CaseSensitive := False;
end;

constructor TStreamIniFileW.Create(const AFileName: WideString);
begin
  Create;
  SetFileName(AFileName);
end;

function TStreamIniFileW.GetFileName: WideString;
begin
  Result := inherited FileName;
end;

function TStreamIniFileW.ReadBool(const Section, Ident: String;
  Default: Boolean): Boolean;
var
  Str: WideString;
begin
  Str := ReadString(Section, Ident, BoolStr[Default]);
  if Str = BoolStr[False] then
    Result := False else
  if Str = BoolStr[True] then
    Result := True else
    Result := StrToIntDef(Str, 0) <> 0;
end;

function TStreamIniFileW.ReadInt64(const Section, Ident: WideString;
  const Default: Int64): Int64;
var
 E: Integer;
 Str: String;
begin
 Str := ReadString(Section, Ident, '');
 if Str <> '' then
 begin
  Val(Str, Result, E);
  if E = 0 then
   Exit;
 end;
 Result := Default;
end;

procedure TStreamIniFileW.SetFileName(const Value: WideString);
var
  FileNameP: ^String;
begin
  // HACK
  FileNameP := Addr(inherited FileName);
  if FileNameP^ <> Value then
  begin
    UpdateFile;
    FileNameP^ := Value;
    if (Value <> '') and WideFileExists(Value) then
      LoadFromFile(Value) else
      Self.Clear;
 end;
end;

procedure TStreamIniFileW.UpdateFile;
begin
  if inherited FileName <> '' then
  begin
    if MakeDirs(inherited FileName) then
      SaveToFile(inherited FileName);
  end;
end;


procedure TStreamIniFileW.WriteInt64(const Section, Ident: WideString;
  const Value: Int64; HexDigits: Integer);
begin
  if HexDigits > 0 then
    WriteString(Section, Ident, IntTo0xHex(Value, HexDigits)) else
    WriteString(Section, Ident, IntToStr(Value));
end;

procedure TStreamIniFileW.WriteInteger(const Section, Ident: WideString; Value,
  HexDigits: Integer);
begin
  if HexDigits > 0 then
    WriteString(Section, Ident, '0x' + IntToHex(Value, HexDigits)) else
    WriteString(Section, Ident, IntToStr(Value));
end;

{$ENDIF}

end.
