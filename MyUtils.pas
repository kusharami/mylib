unit MyUtils;

interface

uses Windows, SysUtils, NodeLst, HexUnit;

//******** Working with files *******//
type
{$IFNDEF UNICODE}
  RawByteString = AnsiString;
{$ENDIF}

 TCheckFileResult = (cfNotExist, cfNoName, cfLocked, cfCanWrite);

 TWideFileName = type WideString;

 TSearchRecW = record
  Time: LongInt;
  Size: Int64;
  Attr: LongInt;
  Name: TWideFileName;
  ExcludeAttr: LongInt;
  FindHandle: THandle;
  FindData: TWin32FindDataW;
 end;

 TCharSet = set of AnsiChar;

const
 INVALID_FILE_ATTRIBUTES = DWORD(-1);
 FNIC: TCharSet = [#0..#$1F, '\', '/', ':', '*', '?', '"', '<', '>', '|'];
 DNIC: TCharSet = [#0..#$1F, '/', '*', '?', '"', '<', '>', '|'];

function GetCh: WideChar;
function KeyPressed: Boolean;
procedure SetInputHandle;
procedure RestoreInputHandle;

function CheckFileName(const FileName: AnsiString): Boolean; overload;
function CheckFileName(const FileName: AnsiString;
                       const S: TCharSet): Boolean; overload;
function CheckFileName(const FileName: WideString): Boolean; overload;
function CheckFileName(const FileName: WideString;
                       const S: TCharSet): Boolean; overload;
function FixedFileName(const FileName: AnsiString;
                     FixChar: AnsiChar = '_'): AnsiString; overload;
function FixedFileName(const FileName: AnsiString; FixChar: AnsiChar;
 const S: TCharSet; Reverse: Boolean = false): AnsiString; overload
procedure FixFileName(var FileName: AnsiString; FixChar: AnsiChar = '_');
                      overload;
procedure FixFileName(var FileName: AnsiString; FixChar: AnsiChar;
                      const S: TCharSet; Reverse: Boolean = False); overload;
function FixedFileName(const FileName: WideString;
                     FixChar: WideChar = '_'): WideString; overload;
function FixedFileName(const FileName: WideString; FixChar: WideChar;
 const S: TCharSet; Reverse: Boolean = false): WideString; overload
procedure FixFileName(var FileName: WideString; FixChar: WideChar = '_');
                      overload;
procedure FixFileName(var FileName: WideString; FixChar: WideChar;
                      const S: TCharSet; Reverse: Boolean = False); overload;
//Validates file name AnsiString
function CheckFileForWrite(const FileName: String): TCheckFileResult;
         overload;
function CheckFileForWrite(const FileName: WideString): TCheckFileResult;
         overload;
//Returns: cfNotExist if file not found;
//         cfLocked if file cannot be changed;
//         cfCanWrite if file can be modified.
function WideExtractFilePath(const FileName: WideString): WideString;
function WideExtractFileName(const FileName: WideString): WideString;
function WideExtractFileExt(const FileName: WideString): WideString;
function WideExtractFileDrive(const FileName: WideString): WideString;
function WideExtractRelativePath(const BaseName, DestName: WideString): WideString;
function WideExtractShortPathName(const FileName: WideString): WideString;
function ExpandFileNameEx(const Path, FileName: AnsiString): AnsiString;
         overload;
function ExpandFileNameEx(const Path, FileName: WideString): WideString;
         overload;
function WideIncludeTrailingPathDelimiter(const S: WideString): WideString;
//Converts the relative file name into a fully qualified path name
function GetShortFileName(const FileName: WideString): String;
//Converts unicode file name to ansi file name
function MakeDirs(const Path: AnsiString): Boolean; overload;
function MakeDirs(Path: WideString): Boolean; overload;
//Create all directories founded in Path parameter
function WideGetCurrentDir: WideString;
//Returns current directory name
function WideCreateDir(const Dir: WideString): Boolean;
function WideRemoveDir(const Dir: WideString): Boolean;
function WideSetCurrentDir(const Dir: WideString): Boolean;
function WideFindFirst(const Path: WideString; Attr: Integer; var F: TSearchRecW): Integer;
function WideFindNext(var F: TSearchRecW): Integer;
procedure WideFindClose(var F: TSearchRecW);
function WideFileExists(const FileName: WideString): Boolean;
function WideFileAge(const FileName: WideString): Integer;
function WideChangeFileExt(const FileName, Extension: WideString): WideString;

//****** Other utils ******//

function PAnsiCharPos(SubStr, Str: PAnsiChar; L1, L2: Integer): Integer;
function PWideCharPos(SubStr, Str: PWideChar; L1, L2: Integer): Integer;
function MaskedPos(const SubStr, Str: AnsiString): Integer; overload;
function MaskedPos(const SubStr, Str: WideString): Integer; overload;
//Finds masked with ? sub string in a specified string.
//Example:
// Position := MaskedPos('.???', 'filename.txt');
//Result will be same as System.Pos('.txt', 'filename.txt') gives
function WideMatchesMask(FileName, Mask: WideString;
                         CaseSensitive: Boolean = False): Boolean;
//Indicates whether a file name conforms to the format specified by a filter string
function WidePosEx(const SubStr, S: WideString; Offset: Cardinal = 1): Integer;

//Fills buffer with specified word value
function WideParamStr(Index: Integer): WideString;
function ZeroMemCheck(Ptr: Pointer; Len: Integer): Boolean;
function ValueMemCheck(Value: Byte; Ptr: Pointer; Len: Integer): Integer;
function Value16MemCheck(Value: Word; Ptr: Pointer; Len: Integer): Integer;
function Value32MemCheck(Value: LongWord; Ptr: Pointer; Len: Integer): Integer;
function ZeroMemVCheck(Ptr: Pointer; Skip, Len: Integer): Boolean;
function ValueMemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: Byte): Boolean;
function Value16MemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: Word): Boolean;
function Value32MemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: LongWord): Boolean;
function EscapeStringEncode(const S: WideString): WideString;
procedure EscapeStringDecodeProc(S: PWideChar);
function EscapeStringDecode(const S: WideString): WideString;
function WideQuotedStr(const S: WideString; const SpaceSet: TCharSet = [' ', #9]): WideString;
function WideDequotedStr(const S: WideString): WideString;
function NewFile(FileName: PWideChar): THandle;
function OpenFile(FileName: PWideChar): THandle;
function ModifyFile(FileName: PWideChar): THandle;

function WideStringReplace(const S, OldPattern, NewPattern: WideString;
  Flags: TReplaceFlags): WideString;

type
 TWideCharRange = class(TNode)
  private
   FStart: Word;
   FEnd: Word;
 end;

 TWideCharSet = class(TNodeList)
  private
   FMin: Word;
   FMax: Word;
   function Find(X: Word): TWideCharRange;
  protected
   procedure Initialize; override;
   procedure ClearData; override;
  public
   constructor Create(AFirstChar, ALastChar: Word); overload;
   constructor Create(const AList: WideString); overload;
   constructor Create(const AArray: array of Word); overload;
   procedure Include(AFirstChar, ALastChar: Word); overload;
   procedure Include(const AList: WideString); overload;
   procedure Include(const AArray: array of Word); overload;
   procedure Include(const ACharSet: TWideCharSet); overload;
   procedure Exclude(AFirstChar, ALastChar: Word); overload;
   procedure Exclude(const AList: WideString); overload;
   procedure Exclude(const AArray: array of Word); overload;   
   procedure Exclude(const ACharSet: TWideCharSet); overload;
   function IsOwnerOf(AChar: WideChar): Boolean;
 end;

(* CODEPAGE TYPES *)
type
 TCPInfoEx = packed record
  MaxCharSize: UINT;
  DefaultChar: array[0..MAX_DEFAULTCHAR - 1] of BYTE;
  LeadByte: array[0..MAX_LEADBYTES - 1] of BYTE;
  UnicodeDefaultChar: WCHAR;
  CodePage: UINT;
  CodePageName: array[0..MAX_PATH - 1] of AnsiChar;
 end;
 TCPInfoExW = packed record
  MaxCharSize: UINT;
  DefaultChar: array[0..MAX_DEFAULTCHAR - 1] of BYTE;
  LeadByte: array[0..MAX_LEADBYTES - 1] of BYTE;
  UnicodeDefaultChar: WCHAR;
  CodePage: UINT;
  CodePageName: array[0..MAX_PATH - 1] of WideChar;
 end;
 TCodePageInfo = packed record
  CodePage: Word;
  CodePageName: WideString;
 end;
 TCodePageList = array of TCodePageInfo;
 TUTF8toUTF16 = function(const S: RawByteString): WideString;
 TUTF16toUTF8 = function(const S: WideString): RawByteString;

(* CODEPAGE ROUTINES *)
function GetCPInfoEx(CodePage: UINT; dwFlags: DWord;
  var lpCPInfoEx: TCPInfoEx): BOOL; stdcall;
  external kernel32 name 'GetCPInfoExA';
function GetCPInfoExA(CodePage: UINT; dwFlags: DWord;
  var lpCPInfoEx: TCPInfoEx): BOOL; stdcall;
  external kernel32 name 'GetCPInfoExA';
function GetCPInfoExW(CodePage: UINT; dwFlags: DWord;
  var lpCPInfoEx: TCPInfoExW): BOOL; stdcall;
  external kernel32 name 'GetCPInfoExW';
function GetCodePageName(CodePage: UINT): AnsiString;
function GetCodePageNameW(CodePage: UINT): WideString;
function MakeCodePageList: TCodePageList;

(* CODEPAGE CONSTANTS *)
const
 CP_UTF16      = 1200;  // Unicode (UTF-16), little endian byte order
 CP_UTF16_BE   = 1201;  // Unicode (UTF-16), big endian byte order
 CP_UTF32      = 12000; // Unicode (UTF-32), little endian byte order
 CP_UTF32_BE   = 12001; // Unicode (UTF-32), big endian byte order
 CP_IBM037     = 037;   // IBM EBCDIC US-Canada
 CP_IBM437     = 437;   // OEM United States
 CP_IBM500     = 500;   // IBM EBCDIC International
 CP_ASMO_708   = 708;   // Arabic (ASMO 708)
 CP_709        = 709;   // Arabic (ASMO-449+, BCON V4)
 CP_710        = 710;   // Arabic - Transparent Arabic
 CP_DOS_720    = 720;   // Arabic (Transparent ASMO); Arabic (DOS)
 CP_IBM737     = 737;   // OEM Greek (formerly 437G); Greek (DOS)
 CP_IBM775     = 775;   // OEM Baltic; Baltic (DOS)
 CP_IBM850     = 850;   // OEM Multilingual Latin 1; Western European (DOS)
 CP_IBM852     = 852;   // OEM Latin 2; Central European (DOS)
 CP_IBM855     = 855;   // OEM Cyrillic (primarily Russian)
 CP_IBM857     = 857;   // OEM Turkish; Turkish (DOS)
 CP_IBM858     = 858;   // OEM Multilingual Latin 1 + Euro symbol
 CP_IBM860     = 860;   // OEM Portuguese; Portuguese (DOS)
 CP_IBM861     = 861;   // OEM Icelandic; Icelandic (DOS)
 CP_DOS_862    = 862;   // OEM Hebrew; Hebrew (DOS)
 CP_IBM863     = 863;   // OEM French Canadian; French Canadian (DOS)
 CP_IBM864     = 864;   // OEM Arabic; Arabic (864)
 CP_IBM865     = 865;   // OEM Nordic; Nordic (DOS)
 CP_OEM_RUS    = 866;   // OEM Russian; Cyrillic (DOS)
 CP_IBM869     = 869;   // OEM Modern Greek; Greek, Modern (DOS)
 CP_IBM870     = 870;   (* IBM EBCDIC Multilingual/ROECE (Latin 2);
                           IBM EBCDIC Multilingual Latin 2 *)
 CP_WIN_874    = 874;   (* ANSI/OEM Thai (same as 28605, ISO 8859-15);
                           Thai (Windows) *)
 CP_IBM875     = 875;   // IBM EBCDIC Greek Modern
 CP_SHIFT_JIS  = 932;   // ANSI/OEM Japanese; Japanese (Shift-JIS)
 CP_GB2312     = 936;   (* ANSI/OEM Simplified Chinese (PRC, Singapore);
                           Chinese Simplified (GB2312) *)
 CP_KS_C       = 949;   // ANSI/OEM Korean (Unified Hangul Code)
 CP_BIG5       = 950;   (* ANSI/OEM Traditional Chinese (Taiwan;
                           Hong Kong SAR, PRC); Chinese Traditional (Big5) *)
 CP_IBM1026    = 1026;  // IBM EBCDIC Turkish (Latin 5)
 CP_IBM1047    = 1047;  // IBM EBCDIC Latin 1/Open System
 CP_IBM1140    = 1140;  (* IBM EBCDIC US-Canada (037 + Euro symbol);
                           IBM EBCDIC (US-Canada-Euro) *)
 CP_IBM1141    = 1141;  // IBM EBCDIC Germany (20273 + Euro symbol);
                        // IBM EBCDIC (Germany-Euro)
 CP_IBM1142    = 1142;  (* IBM EBCDIC Denmark-Norway (20277 + Euro symbol);
                           IBM EBCDIC (Denmark-Norway-Euro) *)
 CP_IBM1143    = 1143;  (* IBM EBCDIC Finland-Sweden (20278 + Euro symbol);
                           IBM EBCDIC (Finland-Sweden-Euro *)
 CP_IBM1144    = 1144;  (* IBM EBCDIC Italy (20280 + Euro symbol);
                           IBM EBCDIC (Italy-Euro) *)
 CP_IBM1145    = 1145;  (* IBM EBCDIC Latin America-Spain (20284 + Euro symbol);
                           IBM EBCDIC (Spain-Euro) *)
 CP_IBM1146    = 1146;  (* IBM EBCDIC United Kingdom (20285 + Euro symbol);
                           IBM EBCDIC (UK-Euro) *)
 CP_IBM1147    = 1147;  (* IBM EBCDIC France (20297 + Euro symbol);
                           IBM EBCDIC (France-Euro) *)
 CP_IBM1148    = 1148;  (* IBM EBCDIC International (500 + Euro symbol);
                           IBM EBCDIC (International-Euro) *)
 CP_IBM1149    = 1149;  (* IBM EBCDIC Icelandic (20871 + Euro symbol);
                           IBM EBCDIC (Icelandic-Euro) *)
 CP_ANSI_1250  = 1250;  // ANSI Central European; Central European (Windows)
 CP_ANSI_1251  = 1251;  // ANSI Cyrillic; Cyrillic (Windows)
 CP_ANSI_1252  = 1252;  // ANSI Latin 1; Western European (Windows)
 CP_ANSI_1253  = 1253;  // ANSI Greek; Greek (Windows)
 CP_ANSI_1254  = 1254;  // ANSI Turkish; Turkish (Windows)
 CP_ANSI_1255  = 1255;  // ANSI Hebrew; Hebrew (Windows)
 CP_ANSI_1256  = 1256;  // ANSI Arabic; Arabic (Windows)
 CP_ANSI_1257  = 1257;  // ANSI Baltic; Baltic (Windows)
 CP_ANSI_1258  = 1258;  // ANSI/OEM Vietnamese; Vietnamese (Windows)
 CP_JOHAB      = 1361;  // Korean (Johab)
 CP_MAC        = 10000; // MAC Roman; Western European (Mac)
 CP_MAC_JP     = 10001; // Japanese (Mac)
 CP_MAC_TW     = 10002; (* MAC Traditional Chinese (Big5);
                           Chinese Traditional (Mac) *)
 CP_MAC_KR     = 10003; // Korean (Mac)
 CP_MAC_AR     = 10004; // Arabic (Mac)
 CP_MAC_HB     = 10005; // Hebrew (Mac)
 CP_MAC_GR     = 10006; // Greek (Mac)
 CP_MAC_RU     = 10007; // Cyrillic (Mac)
 CP_MAC_CN     = 10008; (* MAC Simplified Chinese (GB 2312);
                           Chinese Simplified (Mac) *)
 CP_MAC_RO     = 10010; // Romanian (Mac)
 CP_MAC_UK     = 10017; // Ukrainian (Mac)
 CP_MAC_TH     = 10021; // Thai (Mac)
 CP_MAC_CE     = 10029; // MAC Latin 2; Central European (Mac)
 CP_MAC_IC     = 10079; // Icelandic (Mac)
 CP_MAC_TR     = 10081; // Turkish (Mac)
 CP_MAC_CR     = 10082; // Croatian (Mac)
 CP_CNS_TW     = 20000; // CNS Taiwan; Chinese Traditional (CNS)
 CP_TCA_TW     = 20001; // TCA Taiwan
 CP_ETEN_TW    = 20002; // Eten Taiwan; Chinese Traditional (Eten)
 CP_IBM_TW     = 20003; // IBM5550 Taiwan
 CP_TT_TW      = 20004; // TeleText Taiwan
 CP_WANG_TW    = 20005; // Wang Taiwan
 CP_IA5        = 20105; (* IA5 (IRV International Alphabet No. 5, 7-bit);
                           Western European (IA5) *)
 CP_IA5_GR     = 20106; // IA5 German (7-bit)
 CP_IA5_SW     = 20107; // IA5 Swedish (7-bit)
 CP_IA5_NW     = 20108; // IA5 Norwegian (7-bit)
 CP_US_ASCII   = 20127; // US-ASCII (7-bit)
 CP_T_61       = 20261; // T.61
 CP_ISO6937    = 20269; // ISO 6937 Non-Spacing Accent
 CP_IBM273     = 20273; // IBM EBCDIC Germany
 CP_IBM277     = 20277; // IBM EBCDIC Denmark-Norway
 CP_IBM278     = 20278; // IBM EBCDIC Finland-Sweden
 CP_IBM280     = 20280; // IBM EBCDIC Italy
 CP_IBM284     = 20284; // IBM EBCDIC Latin America-Spain
 CP_IBM285     = 20285; // IBM EBCDIC United Kingdom
 CP_IBM290     = 20290; // IBM EBCDIC Japanese Katakana Extended
 CP_IBM297     = 20297; // IBM EBCDIC France
 CP_IBM420     = 20420; // IBM EBCDIC Arabic
 CP_IBM423     = 20423; // IBM EBCDIC Greek
 CP_IBM424     = 20424; // IBM EBCDIC Hebrew
 CP_KR_EX      = 20833; // IBM EBCDIC Korean Extended
 CP_IBM_THAI   = 20838; // IBM EBCDIC Thai
 CP_KOI8_R     = 20866; // Russian (KOI8-R); Cyrillic (KOI8-R)
 CP_IBM871     = 20871; // IBM EBCDIC Icelandic
 CP_IBM880     = 20880; // IBM EBCDIC Cyrillic Russian
 CP_IBM905     = 20905; // IBM EBCDIC Turkish
 CP_IBM924     = 20924; // IBM EBCDIC Latin 1/Open System (1047 + Euro symbol)
 CP_EUC_JP     = 20932; // Japanese (JIS 0208-1990 and 0121-1990)
 CP_GB2312_X   = 20936; (* Simplified Chinese (GB2312);
                           Chinese Simplified (GB2312-80) *)
 CP_WANSUNG    = 20949; // Korean Wansung
 CP_1025       = 21025; // IBM EBCDIC Cyrillic Serbian-Bulgarian
 CP_1027       = 21027; // (deprecated)
 CP_KOI8_U     = 21866; // Ukrainian (KOI8-U); Cyrillic (KOI8-U)
 CP_ISO8859_1  = 28591; // ISO 8859-1 Latin 1; Western European (ISO)
 CP_ISO8859_2  = 28592; // ISO 8859-2 Central European; Central European (ISO)
 CP_ISO8859_3  = 28593; // ISO 8859-3 Latin 3
 CP_ISO8859_4  = 28594; // ISO 8859-4 Baltic
 CP_ISO8859_5  = 28595; // ISO 8859-5 Cyrillic
 CP_ISO8859_6  = 28596; // ISO 8859-6 Arabic
 CP_ISO8859_7  = 28597; // ISO 8859-7 Greek
 CP_ISO8859_8  = 28598; // ISO 8859-8 Hebrew; Hebrew (ISO-Visual)
 CP_ISO8859_9  = 28599; // ISO 8859-9 Turkish
 CP_ISO8859_13 = 28603; // ISO 8859-13 Estonian
 CP_ISO8859_15 = 28605; // ISO 8859-15 Latin 9
 CP_EUROPA     = 29001; // Europa 3
 CP_ISO8859_8i = 38598; // ISO 8859-8 Hebrew; Hebrew (ISO-Logical)
 CP_ISO2022_J1 = 50220; (* ISO 2022 Japanese with no halfwidth Katakana;
                           Japanese (JIS) *)
 CP_ISO2022_J2 = 50221; (* ISO 2022 Japanese with halfwidth Katakana;
                           Japanese (JIS-Allow 1 byte Kana) *)
 CP_ISO2022_J3 = 50222; (* ISO 2022 Japanese JIS X 0201-1989;
                           Japanese (JIS-Allow 1 byte Kana - SO/SI) *)
 CP_ISO2022_KR = 50225; // ISO 2022 Korean
 CP_ISO2022_CN = 50227; (* ISO 2022 Simplified Chinese;
                           Chinese Simplified (ISO 2022) *)
 CP_ISO2022_TW = 50229; // ISO 2022 Traditional Chinese
 CP_50930      = 50930; // EBCDIC Japanese (Katakana) Extended
 CP_50931      = 50931; // EBCDIC US-Canada and Japanese
 CP_50933      = 50933; // EBCDIC Korean Extended and Korean
 CP_50935      = 50935; (* EBCDIC Simplified Chinese Extended and
                           Simplified Chinese *)
 CP_50936      = 50936; // EBCDIC Simplified Chinese
 CP_50937      = 50937; // EBCDIC US-Canada and Traditional Chinese
 CP_50939      = 50939; // EBCDIC Japanese (Latin) Extended and Japanese
 CP_EUC_JP_2   = 51932; // EUC Japanese
 CP_EUC_CN     = 51936; // EUC Simplified Chinese; Chinese Simplified (EUC)
 CP_EUC_KR     = 51949; // EUC Korean
 CP_EUC_TW     = 51950; // EUC Traditional Chinese
 CP_HZ_GB2312  = 52936; // HZ-GB2312 Simplified Chinese; Chinese Simplified (HZ)
 CP_GB18030    = 54936; // WinXP+: GB18030 Simplified Chinese (4 byte);
                        // Chinese Simplified (GB18030)
 CP_ISCII_DE   = 57002; // ISCII Devanagari
 CP_ISCII_BE   = 57003; // ISCII Bengali
 CP_ISCII_TA   = 57004; // ISCII Tamil
 CP_ISCII_TE   = 57005; // ISCII Telugu
 CP_ISCII_AS   = 57006; // ISCII Assamese
 CP_ISCII_OR   = 57007; // ISCII Oriya
 CP_ISCII_KA   = 57008; // ISCII Kannada
 CP_ISCII_MA   = 57009; // ISCII Malayalam
 CP_ISCII_GU   = 57010; // ISCII Gujarati
 CP_ISCII_PA   = 57011; // ISCII Punjabi
 CP_UTF7       = 65000; // Unicode (UTF-7)
 CP_UTF8       = 65001; // Unicode (UTF-8)
 CP_Count = 146;
 CP_List: array[0..CP_Count - 1] of Word =
 (
  CP_IBM037,
  CP_IBM437,
  CP_IBM500,
  CP_ASMO_708,
  CP_709,
  CP_710,
  CP_DOS_720,
  CP_IBM737,
  CP_IBM775,
  CP_IBM850,
  CP_IBM852,
  CP_IBM855,
  CP_IBM857,
  CP_IBM858,
  CP_IBM860,
  CP_IBM861,
  CP_DOS_862,
  CP_IBM863,
  CP_IBM864,
  CP_IBM865,
  CP_OEM_RUS,
  CP_IBM869,
  CP_IBM870,
  CP_WIN_874,
  CP_IBM875,
  CP_SHIFT_JIS,
  CP_GB2312,
  CP_KS_C,
  CP_BIG5,
  CP_IBM1026,
  CP_IBM1047,
  CP_IBM1140,
  CP_IBM1141,
  CP_IBM1142,
  CP_IBM1143,
  CP_IBM1144,
  CP_IBM1145,
  CP_IBM1146,
  CP_IBM1147,
  CP_IBM1148,
  CP_IBM1149,
  CP_ANSI_1250,
  CP_ANSI_1251,
  CP_ANSI_1252,
  CP_ANSI_1253,
  CP_ANSI_1254,
  CP_ANSI_1255,
  CP_ANSI_1256,
  CP_ANSI_1257,
  CP_ANSI_1258,
  CP_JOHAB,
  CP_MAC,
  CP_MAC_JP,
  CP_MAC_TW,
  CP_MAC_KR,
  CP_MAC_AR,
  CP_MAC_HB,
  CP_MAC_GR,
  CP_MAC_RU,
  CP_MAC_CN,
  CP_MAC_RO,
  CP_MAC_UK,
  CP_MAC_TH,
  CP_MAC_CE,
  CP_MAC_IC,
  CP_MAC_TR,
  CP_MAC_CR,
  CP_CNS_TW,
  CP_TCA_TW,
  CP_ETEN_TW,
  CP_IBM_TW,
  CP_TT_TW,
  CP_WANG_TW,
  CP_IA5,
  CP_IA5_GR,
  CP_IA5_SW,
  CP_IA5_NW,
  CP_US_ASCII,
  CP_T_61,
  CP_ISO6937,
  CP_IBM273,
  CP_IBM277,
  CP_IBM278,
  CP_IBM280,
  CP_IBM284,
  CP_IBM285,
  CP_IBM290,
  CP_IBM297,
  CP_IBM420,
  CP_IBM423,
  CP_IBM424,
  CP_KR_EX,
  CP_IBM_THAI,
  CP_KOI8_R,
  CP_IBM871,
  CP_IBM880,
  CP_IBM905,
  CP_IBM924,
  CP_EUC_JP,
  CP_GB2312_X,
  CP_WANSUNG,
  CP_1025,
  CP_1027,
  CP_KOI8_U,
  CP_ISO8859_1,
  CP_ISO8859_2,
  CP_ISO8859_3,
  CP_ISO8859_4,
  CP_ISO8859_5,
  CP_ISO8859_6,
  CP_ISO8859_7,
  CP_ISO8859_8,
  CP_ISO8859_9,
  CP_ISO8859_13,
  CP_ISO8859_15,
  CP_EUROPA,
  CP_ISO8859_8i,
  CP_ISO2022_J1,
  CP_ISO2022_J2,
  CP_ISO2022_J3,
  CP_ISO2022_KR,
  CP_ISO2022_CN,
  CP_ISO2022_TW,
  CP_50930,
  CP_50931,
  CP_50933,
  CP_50935,
  CP_50936,
  CP_50937,
  CP_50939,
  CP_EUC_JP_2,
  CP_EUC_CN,
  CP_EUC_KR,
  CP_EUC_TW,
  CP_HZ_GB2312,
  CP_GB18030,
  CP_ISCII_DE,
  CP_ISCII_BE,
  CP_ISCII_TA,
  CP_ISCII_TE,
  CP_ISCII_AS,
  CP_ISCII_OR,
  CP_ISCII_KA,
  CP_ISCII_MA,
  CP_ISCII_GU,
  CP_ISCII_PA
 );
 UTF8BOM: array[0..2] of Byte = ($EF, $BB, $BF);
 UTF16LE_BOM: array[0..1] of Byte = ($FF, $FE);
 UTF16BE_BOM: array[0..1] of Byte = ($FE, $FF);

function CodePageDetect(var Src; Size: Integer): Integer;
function CodePageStringEncode(CodePage: LongWord; MaxCharSize: Integer;
                 const S: WideString): RawByteString; overload;
function CodePageStringEncode(CodePage: LongWord;
                 const S: WideString): RawByteString; overload;

//Converts unicode string to the string with a specified code page
function CodePageStringDecode(CodePage: LongWord;
                 const S: RawByteString): WideString;
//Converts string with a specified code page to the unicode string
function UTF16toOEM(const S: WideString): RawByteString;
//Converts unicode string to the string with a OEM code page
function OEMtoUTF16(const S: RawByteString): WideString;
//Converts string with an OEM code page to the unicode string
function UTF16toEUCJP(const S: WideString): RawByteString;
//Converts unicode string to EUC-JP string
function EUCJPtoUTF16({$IFNDEF MY_EUC_JP}const{$ENDIF}
                       S: RawByteString): WideString;
//Converts EUC-JP string to unicode string
function SwapUTF16(S: PWideChar; Len: Integer): WideString;
//Converts UTF-16 string to UTF-16_BE string or vice versa
procedure SwapUTF16_P(S: PWideChar; Len: Integer);
//Converts UTF-16 string to UTF-16_BE string or vice versa
function WideStrLen(Str: PWideChar): Integer;
//Returns null-terminated unicode string length

(* GLOBAL VARIABLES AND CONSTANTS *)
var
 UTF8toUTF16: TUTF8toUTF16;
 UTF16toUTF8: TUTF16toUTF8;

{$WARN SYMBOL_PLATFORM OFF}
{$WARN SYMBOL_DEPRECATED OFF}

const
 E_ERROR = -1;
 E_OK = 0;
 faAnyTrueFile = faArchive or faHidden or faReadOnly or faSysFile;

implementation

function WideQuotedStr(const S: WideString; const SpaceSet: TCharSet): WideString;
var
 P: PWord;
 NoSpace: Boolean;
begin
 NoSpace := S <> '';
 P := Pointer(S);
 if P <> nil then
  while (P^ <> 0) do
  begin
   if (P^ > 255) or (AnsiChar(Ord(P^)) in SpaceSet) then
   begin
    NoSpace := False;
    Break;
   end;
   Inc(P);
  end;
 if not NoSpace then
 begin
  Result := '"' + EscapeStringEncode(S) + '"';
 end else
  Result := EscapeStringEncode(S);
end;

function WideDequotedStr(const S: WideString): WideString;
var
 L: Integer;
begin
 Result := S;
 if (S <> '') and (Result[1] = '"') then
 begin
  Delete(Result, 1, 1);
  L := Length(Result);
  if Result[L] = '"' then
   SetLength(Result, L - 1);
  Result := EscapeStringDecode(Result);
 end;
end;

function CheckFileName(const FileName: AnsiString): Boolean;
begin
 Result := CheckFileName(FileName, FNIC);
end;

function CheckFileName(const FileName: AnsiString;
                       const S: TCharSet): Boolean;
var
 P: PAnsiChar;
 I: Integer;
begin
 P := Pointer(FileName);
 for I := 1 to Length(FileName) do
 begin
  if P^ in S then
  begin
   Result := False;
   Exit;
  end;
  Inc(P);
 end;
 Result := True;
end;

function CheckFileName(const FileName: WideString): Boolean;
begin
 Result := CheckFileName(FileName, FNIC);
end;

function CheckFileName(const FileName: WideString;
                       const S: TCharSet): Boolean;
var
 P: PWideChar;
 C: AnsiChar;
 I: Integer;
begin
 P := Pointer(FileName);
 for I := 1 to Length(FileName) do
 begin
  if Word(P^) >= 128 then
   C := ' ' else
   Byte(C) := Byte(P^);
  if C in S then
  begin
   Result := False;
   Exit;
  end;
  Inc(P);
 end;
 Result := True;
end;

function FixedFileName(const FileName: AnsiString;
                     FixChar: AnsiChar = '_'): AnsiString;
begin
 Result := FileName;
 FixFileName(Result, FixChar);
end;

function FixedFileName(const FileName: AnsiString; FixChar: AnsiChar;
 const S: TCharSet; Reverse: Boolean): AnsiString;
begin
 Result := FileName;
 FixFileName(Result, FixChar, S, Reverse);
end;

procedure FixFileName(var FileName: AnsiString; FixChar: AnsiChar);
begin
 FixFileName(FileName, FixChar, FNIC);
end;

procedure FixFileName(var FileName: AnsiString; FixChar: AnsiChar;
  const S: TCharSet; Reverse: Boolean);
var
 P: PAnsiChar;
 I: Integer;
begin
 P := Pointer(FileName);
 for I := 1 to Length(FileName) do
 begin
  if P^ in S then
  begin
   if not Reverse then
    P^ := FixChar;
  end else
  if Reverse then P^ := FixChar;
  Inc(P);
 end;
end;

function FixedFileName(const FileName: WideString;
                     FixChar: WideChar = '_'): WideString;
begin
 Result := FileName;
 FixFileName(Result, FixChar);
end;

function FixedFileName(const FileName: WideString; FixChar: WideChar;
 const S: TCharSet; Reverse: Boolean): WideString;
begin
 Result := FileName;
 FixFileName(Result, FixChar, S, Reverse);
end;

procedure FixFileName(var FileName: WideString; FixChar: WideChar);
begin
 FixFileName(FileName, FixChar, FNIC);
end;

procedure FixFileName(var FileName: WideString; FixChar: WideChar;
  const S: TCharSet; Reverse: Boolean);
var
 P: PWideChar;
 C: Word;
 I: Integer;
begin
 P := Pointer(FileName);
 for I := 1 to Length(FileName) do
 begin
  C := Word(P^);
  if C <= 255 then
  begin
   if AnsiChar(C) in S then
   begin
    if not Reverse then
     P^ := FixChar;
   end else
   if Reverse then P^ := FixChar;
  end else
  if Reverse then P^ := FixChar;
  Inc(P);
 end;
end;

function EscapeStringEncode(const S: WideString): WideString;
var
 P: PWideChar;
begin
 Result := '';
 P := Pointer(S);
 if P <> NIL then
  while P^ <> #0 do
  begin
   case P^ of
    '"': Result := Result + '\"';
    '\': Result := Result + '\\';
    #09: Result := Result + '\t';
    #10, #13:
    begin
     Result := Result + '\n';
     case P^ of
      #10: if PWideChar(Integer(P) + 1)^ = #13 then Inc(P);
      else if PWideChar(Integer(P) + 1)^ = #10 then Inc(P);
     end;
     if P^ = #0 then Break;
    end;
    else
    if P^ < #32 then
     Result := Result + WideFormat('\[%4.4x]', [Word(P^)]) else
     Result := Result + P^;
   end;
   Inc(P);
  end;
end;

procedure EscapeStringDecodeProc(S: PWideChar);
var
 P1, P2: PWideChar;
begin
 P1 := Pointer(S);
 if P1 <> NIL then
 while P1^ <> #0 do
 begin
  if P1^ = '\' then
  begin
   P2 := Pointer(P1);
   Inc(P1);
   if P1^ = 'n' then
   begin
    P2^ := #$0D;
    P1^ := #$0A;
   end;
  end;
  Inc(P1);
 end;
end;

function EscapeStringDecode(const S: WideString): WideString;
var
 P, P2: PWideChar;
 Cnt: Integer;
 Num: array[0..5] of AnsiChar;
begin
 Result := '';
 P := Pointer(S);
 if P <> NIL then
  while P^ <> #0 do
  begin
   if P^ = '\' then
   begin
    Inc(P);
    case P^ of
     't': Result := Result + #9;
     'n': Result := Result + #13#10;
     'r': ;
     '[':
     begin
      Inc(P);
      if P^ <> #0 then
      begin
       P2 := P;
       Cnt := 0;
       while (Cnt < 4) and
             (P2^ <> #0) and (P2^ <> ']') and
             (Word(P2^) <= 255) and
             (AnsiChar(Byte(P2^)) in [#0..#9, 'A'..'F', 'a'..'f']) do
       begin
        Num[Cnt] := AnsiChar(Byte(P2^));
        Inc(Cnt);
        Inc(P2);
       end;
       case P2^ of
        ']':
        begin
         Result := Result + WideChar(HexToInt(String(AnsiString(PAnsiChar(@Num)))));
         P := P2;
        end;
        #0:  Break;
        else Result := Result + '[';
       end;
      end else
      begin
       Result := Result + '[';
       Break;
      end;
     end;
     else Result := Result + P^;
    end;
   end else
    Result := Result + P^;
   Inc(P);
  end;
end;

function CodePageDetect(var Src; Size: Integer): Integer;
begin
 if (Size >= 3) and
    (Integer(Addr(UTF8BOM)^) shl 8 = Integer(Src) shl 8) then
  Result := CP_UTF8 else
 if (Size >= 2) and
    (Word(Addr(UTF16LE_BOM)^) = Word(Src)) then
  Result := CP_UTF16 else
 if (Size >= 2) and
    (Word(Addr(UTF16BE_BOM)^) = Word(Src)) then
  Result := CP_UTF16_BE else
  Result := 0;
end;

function CodePageStringEncode(CodePage: LongWord; MaxCharSize: Integer;
  const S: WideString): RawByteString;
begin
  Result := '';
  if (S <> '') and (MaxCharSize > 0) then
  begin
    SetLength(Result, Length(S) * MaxCharSize);
    SetLength(Result, WideCharToMultiByte(CodePage, 0, Pointer(S), Length(S),
                      Pointer(Result), Length(Result), NIL, NIL));
  end;
end;

function CodePageStringEncode(CodePage: LongWord;
                 const S: WideString): RawByteString;
var
  Inf: CPINFO;
begin
  Result := '';
  if S <> '' then
  begin
    Inf.MaxCharSize := 1;
    GetCPInfo(CodePage, Inf);

    Result := CodePageStringEncode(CodePage, Inf.MaxCharSize, S);
  end;
end;


function CodePageStringDecode(CodePage: LongWord;
  const S: RawByteString): WideString;
begin
 if S <> '' then
 begin
  SetLength(Result, Length(S));
  SetLength(Result, MultiByteToWideChar(CodePage, 0, Pointer(S), Length(S),
                    Pointer(Result), Length(Result)));
 end else Result := '';
end;

function _UTF16toUTF8(const S: WideString): RawByteString;
begin
 if (S <> '') then
 begin
  SetLength(Result, Length(S) * 3);
  SetLength(Result, WideCharToMultiByte(CP_UTF8, 0,
                    Pointer(S), Length(S),
                    Pointer(Result), Length(Result),
                    NIL, NIL));
 end else Result := '';
end;

function _UTF8toUTF16(const S: RawByteString): WideString;
begin
 if S <> '' then
 begin
  SetLength(Result, Length(S));
  SetLength(Result, MultiByteToWideChar(CP_UTF8, 0,
                    Pointer(S), Length(S),
                    Pointer(Result), Length(Result)));
 end else Result := '';
end;

var
 OEMCodePage: LongWord;
 OEMCPInfo: PCPInfo = NIL;

function UTF16toOEM(const S: WideString): RawByteString;
begin
 if OEMCPInfo = NIL then
 begin
  OEMCodePage := GetOEMCP;
  New(OEMCPInfo);
  GetCPInfo(OEMCodePage, OEMCPInfo^);
 end;
 Result := CodePageStringEncode(OEMCodePage, OEMCPInfo.MaxCharSize, S);
end;

function OEMtoUTF16(const S: RawByteString): WideString;
begin
 if OEMCPInfo = NIL then
 begin
  OEMCodePage := GetOEMCP;
  New(OEMCPInfo);
  GetCPInfo(OEMCodePage, OEMCPInfo^);
 end;
 Result := CodePageStringDecode(OEMCodePage, S);
end;

function EUCJPtoUTF16;
{$IFNDEF MY_EUC_JP}
begin
 if S <> '' then
 begin
  SetLength(Result, Length(S));
  SetLength(Result, MultiByteToWideChar(CP_EUC_JP, 0,
                    Pointer(S), Length(S),
                    Pointer(Result), Length(Result)));
 end else Result := '';
end;
{$ELSE}
var
 P, PP: PByte;
 FirstByte: Byte;
 OddA: Boolean;
begin
 if S <> '' then
 begin
  P := Pointer(S);
  FirstByte := $81; OddA := False;
  while P^ <> 0 do
  begin
   if P^ >= $80 then
   begin
    PP := P;
    Inc(P);
    case PP^ of
     $A1..$DE:
     begin
      FirstByte := PP^ - $A1;
      OddA := Odd(FirstByte);
      FirstByte := FirstByte shr 1 + $81;
     end;
     $DF..$F4:
     begin
      FirstByte := PP^ - $DF;
      OddA := Odd(FirstByte);
      FirstByte := FirstByte shr 1 + $E0;
     end;
     else
     begin
      PP^ := $81;
      P^ := $A1;
      Inc(P);
      Continue;
     end;
    end;
    if P^ < $A1 then
    begin
     PP^ := $81;
     P^ := $A1;
    end else if OddA then
    begin
     PP^ := FirstByte;
     P^ := (P^ - $A1) + $9F;
    end else if P^ >= $E0 then
    begin
     PP^ := FirstByte;
     P^ := (P^ - $E0) + $80;
    end else
    begin
     PP^ := FirstByte;
     P^ := (P^ - $A1) + $40;
    end;;
    Inc(P);
   end else Inc(P);
  end;
  Result := CodePageStringDecode(CP_SHIFT_JIS, S);
 end else Result := '';
end; //EUCJPtoUTF16
{$ENDIF}

function UTF16toEUCJP(const S: WideString): RawByteString;
{$IFNDEF MY_EUC_JP}
begin
 if (S <> '')  then
 begin
  SetLength(Result, Length(S) * 2);
  SetLength(Result, WideCharToMultiByte(CP_EUC_JP, 0,
                    Pointer(S), Length(S),
                    Pointer(Result), Length(Result),
                    NIL, NIL));
 end else Result := '';
end;
{$ELSE}
var
 P, PP: PByte;
 FirstByte: Byte;
begin
 if S <> '' then
 begin
  Result := CodePageStringEncode(CP_SHIFT_JIS, 2, S);
  P := Pointer(Result);
  FirstByte := $A1;
  while P^ <> 0 do
  begin
   if P^ >= $80 then
   begin
    PP := P;
    Inc(P);
    case PP^ of
     $81..$9F: FirstByte := (PP^ - $81) shl 1 + $A1;
     $E0..$EA: FirstByte := (PP^ - $E0) shl 1 + $DF;
     else
     begin
      PP^ := $A2;
      P^ := $A3;
      Inc(P);
      Continue;
     end;
    end;
    if P^ >= $9F then
    begin
     PP^ := FirstByte + 1;
     P^ := (P^ - $9F) + $A1;
    end else if P^ >= $80 then
    begin
     PP^ := FirstByte;
     P^ := (P^ - $80) + $E0;
    end else if P^ >= $40 then
    begin
     PP^ := FirstByte;
     P^ := (P^ - $40) + $A1;
    end else
    begin
     PP^ := $A2;
     P^ := $A3;
    end;
    Inc(P);
   end else Inc(P);
  end;
 end else Result := '';
end; //UTF16toEUCJP
{$ENDIF}

function ExpandFileNameEx(const Path, FileName: AnsiString): AnsiString;
var
 D: AnsiString;
 J, I, K: Integer;
begin
 Result := FileName;
 if (Result <> '') and (Path <> '') then
 begin
  if (Result <> '') and
    ((Result[1] = PathDelim) or
    ((Result[1] in ['A'..'Z', 'a'..'z']) and
     (Pos(AnsiString(DriveDelim + PathDelim), Result) = 2))) then Exit;
  while (Length(Result) >= 2) and
        (Result[1] = '.') and
        (Result[2] = PathDelim) do Delete(Result, 1, 2);
  D := Path;
  if D[Length(D)] <> PathDelim then D := D + PathDelim;
  I := 0;
  while (Length(Result) >= 3) and
        (Result[1] = '.') and
        (Result[2] = '.') and
        (Result[3] = PathDelim) do
  begin
   Delete(Result, 1, 3);
   Inc(I);
  end;
  for J := 1 to I do
  begin
   K := Length(D) - 1;
   while (K > 0) and (D[K] <> PathDelim) do Dec(K);
   Delete(D, K + 1, Length(D) - (K + 1) + 1);
  end;
  Result := D + Result;
 end;
end; //ExpandFileNameEx

function ExpandFileNameEx(const Path, FileName: WideString): WideString;
var
 D: WideString;
 J, I, K: Integer;
begin
 Result := FileName;
 if (Result <> '') and (Path <> '') then
 begin
  if (Result <> '') and
    ((Result[1] = PathDelim) or
    ((Word(Result[1]) <= Word('z')) and
     (Byte(Result[1]) in [Byte('A')..Byte('Z'), Byte('a')..Byte('z')]) and
     (Pos(WideString(DriveDelim + PathDelim), Result) = 2))) then Exit;
  while (Length(Result) >= 2) and
        (Result[1] = '.') and
        (Result[2] = PathDelim) do Delete(Result, 1, 2);
  D := Path;
  if D[Length(D)] <> PathDelim then D := D + PathDelim;
  I := 0;
  while (Length(Result) >= 3) and
        (Result[1] = '.') and
        (Result[2] = '.') and
        (Result[3] = PathDelim) do
  begin
   Delete(Result, 1, 3);
   Inc(I);
  end;
  for J := 1 to I do
  begin
   K := Length(D) - 1;
   while (K > 0) and (D[K] <> PathDelim) do Dec(K);
   Delete(D, K + 1, Length(D) - (K + 1) + 1);
  end;
  Result := D + Result;
 end;
end; //ExpandFileNameExW

function CheckFileForWrite(const FileName: String): TCheckFileResult;
var
 SR: TSearchRec;
begin
 if FileName = '' then
 begin
  Result := cfNoName;
  Exit;
 end;
 Result := cfNotExist;
 if FindFirst(FileName, faAnyFile, SR) = 0 then
 begin
  if SR.Attr and (faReadOnly or faVolumeID or faDirectory or faSymLink) = 0 then
   Result := cfCanWrite else
   Result := cfLocked;
  FindClose(SR);
 end;
end;

function CheckFileForWrite(const FileName: WideString): TCheckFileResult;
var
 SR: TSearchRecW;
begin
 if FileName = '' then
 begin
  Result := cfNoName;
  Exit;
 end;
 Result := cfNotExist;
 if WideFindFirst(FileName, faAnyFile, SR) = 0 then
 begin
  if SR.Attr and (faReadOnly or faVolumeID or faDirectory or faSymLink) = 0 then
   Result := cfCanWrite else
   Result := cfLocked;
  WideFindClose(SR);
 end;
end;

function MakeDirs(const Path: AnsiString): Boolean;
var
 I, L, J: Integer;
 P: PChar;
 _Path, SS: String;
begin
 Result := False;
 _Path := ExtractFilePath(String(Path));
 L := Length(_Path);
 if (_Path <> '') and (_Path[L] <> PathDelim) then _Path := _Path + PathDelim;
 P := Pointer(_Path);
 I := L;
 SS := '';
 if L > 2 then
 begin
  if _Path[2] = ':' then
  begin
   SS := Copy(_Path, 1, 3);
   Inc(P, 3);
   Dec(I, 3);
  end else if (_Path[1] = PathDelim) and (_Path[2] = PathDelim) then
  begin
   SS := PathDelim + PathDelim;
   Inc(P, 2);
   Dec(I, 2);
   J := 0;
   while (I > 0) and (J < 2) do
   begin
    if P^ = PathDelim then Inc(J);
    SS := SS + P^;
    Inc(P);
    Dec(I);
   end;
  end;
 end;
 while I > 0 do
 begin
  if (P^ = PathDelim) and not DirectoryExists(SS) then
  begin
   if not CreateDir(SS) then
    Exit;
  end;
  SS := SS + P^;
  Inc(P);
  Dec(I);
 end;
 Result := True;
end; //MakeDirs

function WStrScan(const Str: PWideChar; Chr: WideChar): PWideChar;
begin
  Result := Str;
  while Result^ <> Chr do
  begin
    if Result^ = #0 then
    begin
      Result := nil;
      Exit;
    end;
    Inc(Result);
  end;
end;

function WideLastDelimiter(const Delimiters, S: WideString): Integer;
var
  P: PWideChar;
begin
  Result := Length(S);
  P := PWideChar(Delimiters);
  while Result > 0 do
  begin
    if (S[Result] <> #0) and (WStrScan(P, S[Result]) <> nil) then
      Exit;
    Dec(Result);
  end;
end;

function WideExtractFilePath(const FileName: WideString): WideString;
var
  I: Integer;
begin
  I := WideLastDelimiter('\:', FileName);
  Result := Copy(FileName, 1, I);
end;

function WideExtractFileName(const FileName: WideString): WideString;
var
  I: Integer;
begin
  I := WideLastDelimiter('\:', FileName);
  Result := Copy(FileName, I + 1, MaxInt);
end;

function WideExtractShortPathName(const FileName: WideString): WideString;
var
  Buffer: array[0..MAX_PATH - 1] of WideChar;
begin
  SetString(Result, Buffer, GetShortPathNameW(Pointer(FileName), Buffer, MAX_PATH));
end;

function WideDirectoryExists(const Name: WideString): Boolean;
var
 Code: LongWord;
begin
 Code := GetFileAttributesW(Pointer(Name));
 Result := (Code <> INVALID_FILE_ATTRIBUTES) and (FILE_ATTRIBUTE_DIRECTORY and Code <> 0);
end;

function MakeDirs(Path: WideString): Boolean;
var
 I, L, J: Integer;
 P: PWideChar;
 SS: WideString;
begin
 Result := False;
 Path := WideExtractFilePath(Path);
 L := Length(Path);
 if (Path <> '') and (Path[L] <> PathDelim) then Path := Path + PathDelim;
 P := Pointer(Path);
 I := L;
 SS := '';
 if L > 2 then
 begin
  if Path[2] = ':' then
  begin
   SS := Copy(Path, 1, 3);
   Inc(P, 3);
   Dec(I, 3);
  end else if (Path[1] = PathDelim) and (Path[2] = PathDelim) then
  begin
   SS := PathDelim + PathDelim;
   Inc(P, 2);
   Dec(I, 2);
   J := 0;
   while (I > 0) and (J < 2) do
   begin
    if P^ = PathDelim then Inc(J);
    SS := SS + P^;
    Inc(P);
    Dec(I);
   end;
  end;
 end;
 while I > 0 do
 begin
  if (P^ = PathDelim) and
   not WideDirectoryExists(SS) then
  begin
   if not WideCreateDir(SS) then
    Exit;
  end;
  SS := SS + P^;
  Inc(P);
  Dec(I);
 end;
 Result := True;
end; //MakeDirsW

function PAnsiCharPos(SubStr, Str: PAnsiChar; L1, L2: Integer): Integer;
asm
{     ->EAX     Pointer to substr               }
{       EDX     Pointer to string               }
{       ECX     substr Length               }
{       [EBP]   string Length               }
{     <-EAX     Position of substr in s or 0    }
        TEST    EAX,EAX
        JE      @@stringEmpty
        TEST    EDX,EDX
        JE      @@stringEmpty

        PUSH    EBX
        PUSH    ESI
        PUSH    EDI

        MOV     ESI,EAX         { Point ESI to substr           }
        MOV     EDI,EDX         { Point EDI to s                }
                                { EDX = Length(substr)               }
        MOV     EDX,ECX
                                { Point EDI to first char of s  }
        PUSH    EDI             { remember s position to calculate index        }
                                { ECX = Length(s)          }
        MOV     ECX,[L2]
                                { Point ESI to first char of substr     }
        DEC     EDX             { EDX = Length(substr) - 1              }
        JS      @@fail          { < 0 ? return 0                        }
        MOV     AL,[ESI]        { AL = first char of substr             }
        INC     ESI             { Point ESI to 2'nd char of substr      }

        SUB     ECX,EDX { #positions in s to look at    }
                        { = Length(s) - Length(substr) + 1      }
        JLE     @@fail
@@loop:
        REPNE   SCASB
        JNE     @@fail
        MOV     EBX,ECX { save outer loop counter               }
        PUSH    ESI             { save outer loop substr pointer        }
        PUSH    EDI             { save outer loop s pointer             }

        MOV     ECX,EDX
        REPE    CMPSB
        POP     EDI             { restore outer loop s pointer  }
        POP     ESI             { restore outer loop substr pointer     }
        JE      @@found
        MOV     ECX,EBX { restore outer loop counter    }
        JMP     @@loop
@@stringEmpty:
        XOR     EAX,EAX
        mov     EAX,-1
        JMP     @@noWork
@@fail:
        POP     EDX             { get rid of saved s pointer    }
        mov     EAX,-1
        JMP     @@exit

@@found:
        POP     EDX             { restore pointer to first char of s    }
        MOV     EAX,EDI { EDI points of char after match        }
        SUB     EAX,EDX { the difference is the correct index   }
        DEC     EAX
@@exit:
        POP     EDI
        POP     ESI
        POP     EBX
@@noWork:
end;

function PWideCharPos(SubStr, Str: PWideChar; L1, L2: Integer): Integer;
asm
{     ->EAX     Pointer to substr               }
{       EDX     Pointer to string               }
{       ECX     substr Length               }
{       [EBP]   string Length               }
{     <-EAX     Position of substr in s or 0    }
        TEST    EAX,EAX
        JE      @@stringEmpty
        TEST    EDX,EDX
        JE      @@stringEmpty

        PUSH    EBX
        PUSH    ESI
        PUSH    EDI

        MOV     ESI,EAX         { Point ESI to substr           }
        MOV     EDI,EDX         { Point EDI to s                }
                                { EDX = Length(substr)               }
        MOV     EDX,ECX
                                { Point EDI to first char of s  }
        PUSH    EDI             { remember s position to calculate index        }
                                { ECX = Length(s)          }
        MOV     ECX,[L2]
                                { Point ESI to first char of substr     }
        DEC     EDX             { EDX = Length(substr) - 1              }
        JS      @@fail          { < 0 ? return 0                        }
        MOV     AX,[ESI]        { AL = first char of substr             }
        INC     ESI             { Point ESI to 2'nd char of substr      }
        INC     ESI

        SUB     ECX,EDX { #positions in s to look at    }
                        { = Length(s) - Length(substr) + 1      }
        JLE     @@fail
@@loop:
        REPNE   SCASW
        JNE     @@fail
        MOV     EBX,ECX { save outer loop counter               }
        PUSH    ESI             { save outer loop substr pointer        }
        PUSH    EDI             { save outer loop s pointer             }

        MOV     ECX,EDX
        REPE    CMPSW
        POP     EDI             { restore outer loop s pointer  }
        POP     ESI             { restore outer loop substr pointer     }
        JE      @@found
        MOV     ECX,EBX { restore outer loop counter    }
        JMP     @@loop
@@stringEmpty:
        XOR     EAX,EAX
        mov     EAX,-1
        JMP     @@noWork
@@fail:
        POP     EDX             { get rid of saved s pointer    }
        mov     EAX,-1
        JMP     @@exit

@@found:
        POP     EDX             { restore pointer to first char of s    }
        MOV     EAX,EDI { EDI points of char after match        }
        SUB     EAX,EDX { the difference is the correct index   }
        DEC     EAX
        SHR     EAX,1
@@exit:
        POP     EDI
        POP     ESI
        POP     EBX
@@noWork:
end;

function MaskedPos(const SubStr, Str: AnsiString): Integer;
asm
        { EAX = SubStr }
        { EDX = Str    }
        { Result = EAX }
        TEST    EAX,EAX
        JE      @@noWork

        TEST    EDX,EDX
        JE      @@stringEmpty

        PUSH    EBX
        PUSH    ESI
        PUSH    EDI

        MOV     ESI,EAX                 // Point ESI to SubStr
        MOV     EDI,EDX                 // Point EDI to Str
        MOV     ECX,[EDI-4]             // ECX = Length(Str)
        PUSH    EDI                     // remember Str position to
                                        // calculate index
        MOV     EDX,[ESI-4]             // EDX = Length(SubStr)
        DEC     EDX                     // EDX = Length(SubStr) - 1
        JS      @@fail                  // < 0 ? return 0
        MOV     AL,[ESI]                // AX = first char of SubStr
        INC     ESI                     // Point ESI to 2'nd char of SubStr
        SUB     ECX,EDX                 // #positions in Str to look at
                                        // = Length(Str) - Length(SubStr) + 1
        JLE     @@fail

@@find:
        cmp     al,$3F                  // if first char of SubStr = '?' then
        je      @@foundchar             // first char found

@@loop:
        cmp     byte ptr [edi],al       // find first char of SubStr
        je      @@foundchar
        inc     edi
        loop    @@loop

        jmp     @@fail

@@foundchar:
        inc     edi
        test    edx,edx
        je      @@found
        MOV     EBX,ECX                 // save outer loop counter
        dec     ebx
        PUSH    ESI                     // save outer loop SubStr pointer
        PUSH    EDI                     // save outer loop Str pointer
        MOV     ECX,EDX

@@cmpstr:
        mov     ah,byte ptr [esi]
        cmp     ah,$3F
        je      @@proceed
        cmp     byte ptr [edi],ah
        jne     @@foundfail
@@proceed:
        inc     esi
        inc     edi
        loop    @@cmpstr

        POP     EDI                     // restore outer loop Str pointer
        POP     ESI                     // restore outer loop SubStr pointer
        jmp     @@found

@@foundfail:
        pop     edi
        pop     esi
        MOV     ECX,EBX                 // restore outer loop counter
        JMP     @@find

@@fail:
        POP     EDX                     // get rid of saved Str pointer
        XOR     EAX,EAX
        JMP     @@exit

@@stringEmpty:
        XOR     EAX,EAX
        JMP     @@noWork

@@found:
        POP     EDX                     // restore pointer to first char of Str
        MOV     EAX,EDI                 // EDI points of char after match
        SUB     EAX,EDX                 // the difference is the correct index
        DEC     EAX
@@exit:
        POP     EDI
        POP     ESI
        POP     EBX
@@noWork:
end; //MaskedPos

function MaskedPos(const SubStr, Str: WideString): Integer;
asm
                { EAX = SubStr }
                { EDX = Str    }
                { Result = EAX }
        TEST    EAX,EAX
        JE      @@noWork

        TEST    EDX,EDX
        JE      @@stringEmpty

        PUSH    EBX
        PUSH    ESI
        PUSH    EDI

        MOV     ESI,EAX                 // Point ESI to SubStr
        MOV     EDI,EDX                 // Point EDI to Str
        MOV     ECX,[EDI-4]
        SHR     ECX,1                   // ECX = Length(Str)
        PUSH    EDI                     // remember Str position to
                                        // calculate index
        MOV     EDX,[ESI-4]
        SHR     EDX,1                   // EDX = Length(SubStr)
        DEC     EDX                     // EDX = Length(SubStr) - 1
        JS      @@fail                  // < 0 ? return 0
        MOV     AX,[ESI]                // AX = first char of SubStr
        ADD     ESI,2                   // Point ESI to 2'nd char of SubStr
        SUB     ECX,EDX                 // #positions in Str to look at
                                        // = Length(Str) - Length(SubStr) + 1
        JLE     @@fail

@@find:
        cmp     ax,$3F                  // if first char of SubStr = '?' then
        je      @@foundchar             // first char found

@@loop:
        cmp     word ptr [edi],ax       // find first char of SubStr
        je      @@foundchar
        add     edi,2
        loop    @@loop

        jmp     @@fail

@@foundchar:
        add     edi,2
        test    edx,edx
        je      @@found
        bswap   eax
        MOV     EBX,ECX                 // save outer loop counter
        dec     ebx
        PUSH    ESI                     // save outer loop SubStr pointer
        PUSH    EDI                     // save outer loop Str pointer
        MOV     ECX,EDX

@@cmpstr:
        mov     ax,word ptr [esi]
        cmp     ax,$3F
        je      @@proceed
        cmp     word ptr [edi],ax
        jne     @@foundfail
@@proceed:
        add     esi,2
        add     edi,2
        loop    @@cmpstr

        POP     EDI                     // restore outer loop Str pointer
        POP     ESI                     // restore outer loop SubStr pointer
        jmp     @@found

@@foundfail:
        bswap   eax
        pop     edi
        pop     esi
        MOV     ECX,EBX                 // restore outer loop counter
        JMP     @@find

@@fail:
        POP     EDX                     // get rid of saved Str pointer
        XOR     EAX,EAX
        JMP     @@exit

@@stringEmpty:
        XOR     EAX,EAX
        JMP     @@noWork

@@found:
        POP     EDX                     // restore pointer to first char of Str
        MOV     EAX,EDI                 // EDI points of char after match
        SUB     EAX,EDX                 // the difference is the correct index
        SHR     EAX,1
@@exit:
        POP     EDI
        POP     ESI
        POP     EBX
@@noWork:
end; //MaskedPosW

function WideExtractFileExt(const FileName: WideString): WideString;
var
  I: Integer;
begin
  I := WideLastDelimiter('.\:', FileName);
  if (I > 0) and (FileName[I] = '.') then
    Result := Copy(FileName, I, MaxInt) else
    Result := '';
end;

function WideExtractFileDrive(const FileName: WideString): WideString;
var
  I, J: Integer;
begin
  if (Length(FileName) >= 2) and (FileName[2] = DriveDelim) then
    Result := Copy(FileName, 1, 2)
  else if (Length(FileName) >= 2) and (FileName[1] = PathDelim) and
    (FileName[2] = PathDelim) then
  begin
    J := 0;
    I := 3;
    While (I < Length(FileName)) and (J < 2) do
    begin
      if FileName[I] = PathDelim then Inc(J);
      if J < 2 then Inc(I);
    end;
    if FileName[I] = PathDelim then Dec(I);
    Result := Copy(FileName, 1, I);
  end else Result := '';
end;


function WideExtractRelativePath(const BaseName, DestName: WideString): WideString;
var
  BasePath, DestPath: WideString;
  BaseLead, DestLead: PWideChar;
  BasePtr, DestPtr: PWideChar;

  function WideExtractFilePathNoDrive(const FileName: WideString): WideString;
  begin
    Result := WideExtractFilePath(FileName);
    Delete(Result, 1, Length(WideExtractFileDrive(FileName)));
  end;

  function Next(var Lead: PWideChar): PWideChar;
  begin
    Result := Lead;
    if Result = nil then Exit;
    Lead := WStrScan(Lead, PathDelim);
    if Lead <> nil then
    begin
      Lead^ := #0;
      Inc(Lead);
    end;
  end;

begin
  if WideSameText(WideExtractFileDrive(BaseName), WideExtractFileDrive(DestName)) then
  begin
    BasePath := WideExtractFilePathNoDrive(BaseName);
    DestPath := WideExtractFilePathNoDrive(DestName);
    BaseLead := Pointer(BasePath);
    BasePtr := Next(BaseLead);
    DestLead := Pointer(DestPath);
    DestPtr := Next(DestLead);
    while (BasePtr <> nil) and (DestPtr <> nil) and WideSameText(BasePtr, DestPtr) do
    begin
      BasePtr := Next(BaseLead);
      DestPtr := Next(DestLead);
    end;
    Result := '';
    while BaseLead <> nil do
    begin
      Result := Result + '..' + PathDelim;             { Do not localize }
      Next(BaseLead);
    end;
    if (DestPtr <> nil) and (DestPtr^ <> #0) then
      Result := Result + DestPtr + PathDelim;
    if DestLead <> nil then
      Result := Result + DestLead;     // destlead already has a trailing backslash
    Result := Result + WideExtractFileName(DestName);
  end
  else
    Result := DestName;
end;


function WideMatchesMask(FileName, Mask: WideString;
  CaseSensitive: Boolean): Boolean;
var
 MP: PWideChar;
 WS: WideString;
 Parts: array of WideString;
 I, J: Integer;
 SlowFind: Boolean;
begin
 if not CaseSensitive then
 begin
  FileName := WideUpperCase(FileName);
  Mask := WideUpperCase(Mask);
 end;
 if FileName = Mask then
 begin
  Result := True;
  Exit;
 end;
 WS := WideExtractFileExt(FileName);
 if WS = '' then FileName := FileName + '.';
 SlowFind := False;
 MP := Pointer(Mask);
 SetLength(Parts, 0);                   //*nto*old*.xls
 while MP^ <> #0 do                     //PhantomasFolder.xls
 begin
  WS := '';
  while (MP^ <> '*') and (MP^ <> #0) do
  begin
   if MP^ = '?' then SlowFind := True;
   WS := WS + MP^;
   Inc(MP);
  end;
  if MP^ = '?' then SlowFind := True;
  if MP^ <> #0 then Inc(MP);
  if WS <> '' then
  begin
   I := Length(Parts);
   SetLength(Parts, I + 1);
   Parts[I] := WS;
  end;
 end;
 Result := True;
 for I := 0 to Length(Parts) - 1 do
 begin
  WS := Parts[I];
  Finalize(Parts[I]);
  if SlowFind then
   J := MaskedPos(WS, FileName) else
   J := Pos(WS, FileName);
  if J > 0 then Delete(FileName, 1, Length(WS) + (J - 1)) else
  begin
   Result := False;
   Break;
  end;
 end;
end;

function WideStrLen(Str: PWideChar): Integer;
asm
        MOV     EDX,EDI
        MOV     EDI,EAX
        MOV     ECX,0FFFFFFFFH
        XOR     AX,AX
        REPNE   SCASW
        MOV     EAX,0FFFFFFFEH
        SUB     EAX,ECX
        MOV     EDI,EDX
end;

function WideGetCurrentDir: WideString;
var
 Buf: array[0..MAX_PATH] of WideChar;
 X: Integer;
begin
 X := GetCurrentDirectoryW(MAX_PATH + 1, Addr(Buf));
 SetLength(Result, X);
 Move(Buf, Pointer(Result)^, X * 2);
end;

function WideCreateDir(const Dir: WideString): Boolean;
begin
 Result := CreateDirectoryW(Pointer(Dir), nil);
end;

function WideRemoveDir(const Dir: WideString): Boolean;
begin
 Result := RemoveDirectoryW(Pointer(Dir));
end;

function WideChangeFileExt(const FileName, Extension: WideString): WideString;
var
  I: Integer;
begin
  I := WideLastDelimiter('.\:',Filename);
  if (I = 0) or (FileName[I] <> '.') then I := MaxInt;
  Result := Copy(FileName, 1, I - 1) + Extension;
end;


function WideSetCurrentDir(const Dir: WideString): Boolean;
begin
 Result := SetCurrentDirectoryW(Pointer(Dir));
end;

function _WideFindMatchingFile(var F: TSearchRecW): Integer;
var
 LocalFileTime: TFileTime;
begin
 with F do
 begin
  while FindData.dwFileAttributes and ExcludeAttr <> 0 do
   if not FindNextFileW(FindHandle, FindData) then
   begin
    Result := GetLastError;
    Exit;
   end;
  FileTimeToLocalFileTime(FindData.ftLastWriteTime, LocalFileTime);
  FileTimeToDosDateTime(LocalFileTime, LongRec(Time).Hi, LongRec(Time).Lo);
  Size := (Int64(FindData.nFileSizeHigh) shl 32) + FindData.nFileSizeLow;
  Attr := FindData.dwFileAttributes;
  Name := FindData.cFileName;
 end;
 Result := 0;
end;

function WideFindFirst(const Path: WideString; Attr: Integer; var F: TSearchRecW): Integer;
const
 faSpecial = faHidden or faSysFile
 {$IFNDEF COMPILER_9_UP} or faVolumeID {$ENDIF} or faDirectory;
begin
 F.ExcludeAttr := not Attr and faSpecial;
 F.FindHandle := FindFirstFileW(Pointer(Path), F.FindData);
 if F.FindHandle <> INVALID_HANDLE_VALUE then
 begin
  Result := _WideFindMatchingFile(F);
  if Result <> 0 then WideFindClose(F);
 end else
  Result := GetLastError;
end;

function WideFindNext(var F: TSearchRecW): Integer;
begin
 if FindNextFileW(F.FindHandle, F.FindData) then
  Result := _WideFindMatchingFile(F) else
  Result := GetLastError;
end;

procedure WideFindClose(var F: TSearchRecW);
begin
 if F.FindHandle <> INVALID_HANDLE_VALUE then
 begin
  Windows.FindClose(F.FindHandle);
  F.FindHandle := INVALID_HANDLE_VALUE;
 end;
end;

function SwapUTF16(S: PWideChar; Len: Integer): WideString;
var
 Src: PWord absolute S;
 Dst: PWord;
begin
 SetLength(Result, Len);
 Dst := Pointer(Result);
 while Len > 0 do
 begin
  Dst^ := ByteSwap16(Src^);
  Inc(Src);
  Inc(Dst);
  Dec(Len);
 end;
end;

procedure SwapUTF16_P(S: PWideChar; Len: Integer);
var
 Dst: PWord absolute S;
begin
 while Len > 0 do
 begin
  Dst^ := ByteSwap16(Dst^);
  Inc(Dst);
  Dec(Len);
 end;
end;

function WideParamStr(Index: Integer): WideString;
// Sub functions //
 function GetParamStr(P: PWideChar; var Param: WideString): PWideChar;
 var
  i, Len: Integer;
  Start, S, Q: PWideChar;
 begin
  while True do
  begin
   while (P[0] <> #0) and (P[0] <= ' ') do P := CharNextW(P);
   if (P[0] = '"') and (P[1] = '"') then Inc(P, 2) else Break;
  end;
  Len := 0;
  Start := P;
  while P[0] > ' ' do
  begin
   if P[0] = '"' then
   begin
    P := CharNextW(P);
    while (P[0] <> #0) and (P[0] <> '"') do
    begin
     Q := CharNextW(P);
     Inc(Len, Q - P);
     P := Q;
    end;
    if P[0] <> #0 then P := CharNextW(P);
   end else
   begin
    Q := CharNextW(P);
    Inc(Len, Q - P);
    P := Q;
   end;
  end;
  SetLength(Param, Len);
  P := Start;
  S := Pointer(Param);
  i := 0;
  while P[0] > ' ' do
  begin
   if P[0] = '"' then
   begin
    P := CharNextW(P);
    while (P[0] <> #0) and (P[0] <> '"') do
    begin
     Q := CharNextW(P);
     while P < Q do
     begin
      S[i] := P^;
      Inc(P);
      Inc(i);
     end;
    end;
    if P[0] <> #0 then P := CharNextW(P);
   end else
   begin
    Q := CharNextW(P);
    while P < Q do
    begin
     S[i] := P^;
     Inc(P);
     Inc(i);
    end;
   end;
  end;
  Result := P;
 end;
//WideParamStr
var
 P: PWideChar;
 Buffer: array[0..1023] of WideChar;
begin
 Result := '';
 if Index = 0 then
  SetString(Result, Buffer,
  GetModuleFileNameW(0, @Buffer, SizeOf(Buffer) shr 1)) else
 begin
  P := GetCommandLineW;
  while True do
  begin
   P := GetParamStr(P, Result);
   if (Index = 0) or (Result = '') then Break;
   Dec(Index);
  end;
 end;
end;

function GetShortFileName(const FileName: WideString): String;
var
  SW: WideString;
  Len: Integer;
begin
  Result := String(FileName);
  if not FileExists(Result) then
  begin
    Len := GetShortPathNameW(Pointer(FileName), nil, 0);
    SetLength(SW, Len);
    SetLength(SW, GetShortPathNameW(Pointer(FileName), Pointer(SW), Len));

    Result := String(SW);
  end;
end;

function GetUnicodeCodePageName(CodePage: UINT): String;
begin
 case CodePage of
  CP_UTF7:     Result := 'UTF-7';
  CP_UTF8:     Result := 'UTF-8';
  CP_UTF16:    Result := 'UTF-16 LE';
  CP_UTF16_BE: Result := 'UTF-16 BE';
  CP_UTF32:    Result := 'UTF-32 LE';
  CP_UTF32_BE: Result := 'UTF-32 BE';
  else
  begin
   Result := '';
   Exit;
  end;
 end;
 Result := Format('%-5d (%s)', [CodePage, Result]);
end;

function GetCodePageName(CodePage: UINT): AnsiString;
var
 Info: TCPInfoEx;
begin
 if IsValidCodePage(CodePage) and GetCPInfoExA(CodePage, 0, Info) then
  Result := Info.CodePageName else
  Result := AnsiString(GetUnicodeCodePageName(CodePage));
end;

function GetCodePageNameW(CodePage: UINT): WideString;
var
 Info: TCPInfoExW;
begin
 if IsValidCodePage(CodePage) and GetCPInfoExW(CodePage, 0, Info) then
  Result := Info.CodePageName else
  Result := GetUnicodeCodePageName(CodePage);
end;

function MakeCodePageList: TCodePageList;

 procedure Add(CP: Integer);
 var
  L: Integer;
  S: WideString;
 begin
  S := GetCodePageNameW(CP);
  if S <> '' then
  begin
   L := Length(Result);
   SetLength(Result, L + 1);
   with Result[L] do
   begin
    CodePage := CP;
    CodePageName := S;
   end;
  end;
 end;

var
 I, CP, ACP, OCP: Integer;
begin
 SetLength(Result, 0);
 ACP := GetACP;
 Add(ACP);
 OCP := GetOEMCP;
 if OCP <> ACP then Add(OCP);
 if (ACP <> CP_UTF7) and (OCP <> CP_UTF7) then Add(CP_UTF7);
 if (ACP <> CP_UTF8) and (OCP <> CP_UTF8) then Add(CP_UTF8);
 Add(CP_UTF16);
 Add(CP_UTF16_BE);
 for I := 0 to CP_Count - 1 do
 begin
  CP := CP_List[I];
  if (CP <> ACP) and (CP <> OCP) then Add(CP);
 end;
end;

function ZeroMemCheck(Ptr: Pointer; Len: Integer): Boolean;
asm
 push  edi
 mov   edi,eax
 mov   ecx,edx
 xor   eax,eax
 repe  scasb
 setz  al
 pop   edi
end;

function ZeroMemVCheck(Ptr: Pointer; Skip, Len: Integer): Boolean;
asm
 push  edi
 mov   edi,eax // edi = Ptr
               // edx = Skip
               // ecx = Len
 xor   eax,eax
@scan:
 cmp   [edi],al
 jnz   @not_zero
 add   edi,edx
 loop  @scan
 inc   eax
@not_zero:
 pop   edi
end;

function ValueMemCheck(Value: Byte; Ptr: Pointer; Len: Integer): Integer;
asm
 push  edi     // eax = Value
 mov   edi,edx // edi = Ptr; edx = Ptr
 repe  scasb   // ecx = Len
 setne al
 and   eax,1
 sub   edi,eax
 mov   eax,edi
 sub   eax,edx // return equal count
 pop   edi
end;

function Value16MemCheck(Value: Word; Ptr: Pointer; Len: Integer): Integer;
asm
 push  edi     // eax = Value
 mov   edi,edx // edi = Ptr; edx = Ptr
 repe  scasw   // ecx = Len
 setne al
 and   eax,1
 sub   edi,eax
 mov   eax,edi
 sub   eax,edx // return equal count
 shr   eax,1
 pop   edi
end;

function Value32MemCheck(Value: LongWord; Ptr: Pointer; Len: Integer): Integer;
asm
 push  edi     // eax = Value
 mov   edi,edx // edi = Ptr; edx = Ptr
 repe  scasd   // ecx = Len
 setne al
 and   eax,1
 sub   edi,eax
 mov   eax,edi
 sub   eax,edx // return equal count
 shr   eax,2
 pop   edi
end;

function ValueMemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: Byte): Boolean;
asm
 push  edi
 mov   edi,eax // edi = Ptr
               // edx = Skip
               // ecx = Len
 xor   eax,eax
 mov   al,[ebp+8]
@scan:
 cmp   [edi],al
 jnz   @not_zero
 add   edi,edx
 loop  @scan
 mov   eax,1
 jmp   @exit
@not_zero:
 xor   eax,eax
@exit:
 pop   edi
end;

function Value16MemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: Word): Boolean;
asm
 push  edi
 shl   edx,1
 mov   edi,eax // edi = Ptr
               // edx = Skip
               // ecx = Len
 xor   eax,eax
 mov   ax,[ebp+8]
@scan:
 cmp   [edi],ax
 jnz   @not_zero
 add   edi,edx
 loop  @scan
 mov   eax,1
 jmp   @exit
@not_zero:
 xor   eax,eax
@exit:
 pop   edi
end;

function Value32MemVCheck(Ptr: Pointer; Skip, Len: Integer; Value: LongWord): Boolean;
asm
 push  edi
 shl   edx,2
 mov   edi,eax // edi = Ptr
               // edx = Skip
               // ecx = Len
 mov   eax,[ebp+8]
@scan:
 cmp   [edi],eax
 jnz   @not_zero
 add   edi,edx
 loop  @scan
 mov   eax,1
 jmp   @exit
@not_zero:
 xor   eax,eax
@exit:
 pop   edi
end;

function WidePosEx(const SubStr, S: WideString; Offset: Cardinal = 1): Integer;
var
 I, X: Integer;
 Len, LenSubStr: Integer;
begin
 if Offset = 1 then Result := Pos(SubStr, S) else
 begin
  I := Offset;
  LenSubStr := Length(SubStr);
  Len := Length(S) - LenSubStr + 1;
  while I <= Len do
  begin
   if S[I] = SubStr[1] then
   begin
    X := 1;
    while (X < LenSubStr) and (S[I + X] = SubStr[X + 1]) do Inc(X);
    if X = LenSubStr then
    begin
     Result := I;
     Exit;
    end;
   end;
   Inc(I);
  end;
  Result := 0;
 end;
end;

function WideFileExists(const FileName: WideString): Boolean;
var
 FindData: TWIN32FindDataW;
 Handle: THandle;
begin
 Result := False;
 Handle := FindFirstFileW(Pointer(FileName), FindData);
 if Handle <> INVALID_HANDLE_VALUE then
 begin
  Windows.FindClose(Handle);
  if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
   Result := True;
 end;
end;

function WideFileAge(const FileName: WideString): Integer;
var
  Handle: THandle;
  FindData: TWin32FindDataW;
  LocalFileTime: TFileTime;
begin
  Handle := FindFirstFileW(PWideChar(FileName), FindData);
  if Handle <> INVALID_HANDLE_VALUE then
  begin
    Windows.FindClose(Handle);
    if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
    begin
      FileTimeToLocalFileTime(FindData.ftLastWriteTime, LocalFileTime);
      if FileTimeToDosDateTime(LocalFileTime, LongRec(Result).Hi, LongRec(Result).Lo) then
        Exit
    end;
  end;
  Result := -1;
end;

function WideIsPathDelimiter(const S: WideString; Index: Integer): Boolean;
begin
  Result := (Index > 0) and (Index <= Length(S)) and (S[Index] = PathDelim);
end;

function WideIncludeTrailingPathDelimiter(const S: WideString): WideString;
begin
  Result := S;
  if not WideIsPathDelimiter(Result, Length(Result)) then Result := Result + PathDelim;
end;

function NewFile(FileName: PWideChar): THandle;
begin
 Result := CreateFileW(FileName, GENERIC_READ or GENERIC_WRITE,
                  0, nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
end;

function OpenFile(FileName: PWideChar): THandle;
begin
 Result := CreateFileW(FileName, GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
end;

function ModifyFile(FileName: PWideChar): THandle;
begin
 Result := CreateFileW(FileName, GENERIC_READ or GENERIC_WRITE,
                0, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
end;



{ TWideCharSet }

procedure TWideCharSet.ClearData;
begin
 FMin := $FFFF;
 FMax := 0;
end;

constructor TWideCharSet.Create(AFirstChar, ALastChar: Word);
begin
 inherited Create;
 Include(AFirstChar, ALastChar);
end;

constructor TWideCharSet.Create(const AList: WideString);
begin
 inherited Create;
 Include(AList);
end;

constructor TWideCharSet.Create(const AArray: array of Word);
begin
 inherited Create;
 Include(AArray);
end;

procedure TWideCharSet.Exclude(AFirstChar, ALastChar: Word);
var
 Node, Node2: TNode;
 Range1: TWideCharRange absolute Node;
 Range2: TWideCharRange absolute Node2;
 FC1, LC1, FC2, LC2: Integer;
begin
 if ALastChar < AFirstChar then
 begin
  FC1 := AFirstChar;
  AFirstChar := ALastChar;
  ALastChar := FC1;
 end;
 Range1 := Find(AFirstChar);
 Range2 := Find(ALastChar);
 LC1 := AFirstChar - 1;
 FC2 := ALastChar + 1;
 if Range1 <> nil then
 begin
  FC1 := Range1.FStart;
  if FC1 < AFirstChar then AFirstChar := FC1;
 end else FC1 := LC1 + 1;
 if Range2 <> nil then
 begin
  LC2 := Range2.FEnd;
  if LC2 > ALastChar then ALastChar := LC2;
 end else LC2 := FC2 - 1;
 FMin := $FFFF;
 FMax := 0;
 Node := RootNode;
 while Node <> nil do with Range1 do
 begin
  if (FStart >= AFirstChar) and (FEnd <= ALastChar) then
  begin
   Node2 := Node;
   Node := Next;
   Remove(Node2);
  end else
  begin
   if FStart < FMin then FMin := FStart;
   if FEnd > FMax then FMax := FEnd;
   Node := Next;
  end;
 end;
 if FC1 <= LC1 then
 begin
  Node := AddNode;
  Range1.FStart := FC1;
  Range1.FEnd := LC1;
  if FC1 < FMin then FMin := FC1;
  if LC1 > FMax then FMax := LC1;
 end;
 if FC2 <= LC2 then
 begin
  Node := AddNode;
  Range1.FStart := FC2;
  Range1.FEnd := LC2;
  if FC2 < FMin then FMin := FC2;
  if LC2 > FMax then FMax := LC2;
 end;
end;

procedure TWideCharSet.Exclude(const ACharSet: TWideCharSet);
var
 Node: TNode; Range: TWideCharRange absolute Node;
begin
 if ACharSet = nil then Exit;
 Node := ACharSet.RootNode;
 while Node <> nil do
 with Range do
 begin
  Exclude(FStart, FEnd);
  Node := Next;
 end;
end;

procedure TWideCharSet.Exclude(const AList: WideString);
var
 P, PSave: PWord;
 L: Integer;
begin
 L := Length(AList) - 1;
 P := Pointer(AList);
 PSave := nil;
 while L > 0 do
 begin
  if (PSave <> nil) and (P^ = PSave^) then
  begin
   Inc(P);
   Exclude(PSave^, P^);
   Dec(L);
  end else Exclude(P^, P^);
  PSave := P;
  Inc(P);
  Dec(L);
 end;
end;

procedure TWideCharSet.Exclude(const AArray: array of Word);
var
 P, PSave: PWord;
 L: Integer;
begin
 L := Length(AArray);
 P := @AArray;
 PSave := nil;
 while L > 0 do
 begin
  if (PSave <> nil) and (P^ = PSave^) then
  begin
   Inc(P);
   Exclude(PSave^, P^);
   Dec(L);
  end else Exclude(P^, P^);
  PSave := P;
  Inc(P);
  Dec(L);
 end;
end;

procedure TWideCharSet.Include(AFirstChar, ALastChar: Word);
var
 Node, Node2: TNode;
 Range1: TWideCharRange absolute Node;
 Range2: TWideCharRange absolute Node2;
 I: Integer;
begin
 if ALastChar < AFirstChar then
 begin
  I := AFirstChar;
  AFirstChar := ALastChar;
  ALastChar := I;
 end;
 Range1 := Find(AFirstChar);
 if AFirstChar = ALastChar then
 begin
  if Range1 <> nil then Exit else
  begin
   Range1 := Find(AFirstChar - 1);
   if Range1 <> nil then
   begin
    Inc(Range1.FEnd);
    if AFirstChar < FMin then FMin := AFirstChar;
    if ALastChar > FMax then FMax := ALastChar;
    Exit;
   end else
   begin
    Range1 := Find(AFirstChar + 1);
    if Range1 <> nil then
    begin
     Dec(Range1.FStart);
     if AFirstChar < FMin then FMin := AFirstChar;
     if ALastChar > FMax then FMax := ALastChar;
     Exit;
    end
   end;
  end;
 end;
 Range2 := Find(ALastChar);
 if Range1 <> nil then
 begin
  if Range1 = Range2 then Exit;
  with Range1 do if FStart < AFirstChar then AFirstChar := FStart;
  Remove(Range1);
 end;
 if Range2 <> nil then
 begin
  with Range2 do if FEnd > ALastChar then ALastChar := FEnd;
  Remove(Range2);
 end;
 Node := RootNode;
 while Node <> nil do
 begin
  with Range1 do
  begin
   if (FStart >= AFirstChar) and (FEnd <= ALastChar) then
   begin
    Node2 := Node;
    Node := Next;
    Remove(Node2);
   end else Node := Next;
  end;
 end;
 Node := AddNode;
 Range1.FStart := AFirstChar;
 Range1.FEnd := ALastChar;
 if AFirstChar < FMin then FMin := AFirstChar;
 if ALastChar > FMax then FMax := ALastChar;
end;

function TWideCharSet.Find(X: Word): TWideCharRange;
begin
 if (X < FMin) or (X > FMax) then
 begin
  Result := nil;
  Exit;
 end;
 Result := TWideCharRange(RootNode);
 while Result <> nil do
 begin
  if (X >= Result.FStart) and (X <= Result.FEnd) then Exit;
  Result := TWideCharRange(Result.Next);
 end;
end;

procedure TWideCharSet.Include(const ACharSet: TWideCharSet);
var
 Node: TNode; Range: TWideCharRange absolute Node;
begin
 if ACharSet = nil then Exit;
 Node := ACharSet.RootNode;
 while Node <> nil do
 with Range do
 begin
  Include(FStart, FEnd);
  Node := Next;
 end;
end;

procedure TWideCharSet.Include(const AList: WideString);
var
 P, PSave: PWord;
 L: Integer;
begin
 L := Length(AList);
 P := Pointer(AList);
 PSave := nil;
 while L > 0 do
 begin
  if (PSave <> nil) and (P^ = PSave^) then
  begin
   Inc(P);
   Include(PSave^, P^);
   Dec(L);
  end else Include(P^, P^);
  PSave := P;
  Inc(P);
  Dec(L);
 end;
end;

procedure TWideCharSet.Include(const AArray: array of Word);
var
 P, PSave: PWord;
 L: Integer;
begin
 L := Length(AArray);
 P := @AArray;
 PSave := nil;
 while L > 0 do
 begin
  if (PSave <> nil) and (P^ = PSave^) then
  begin
   Inc(P);
   Include(PSave^, P^);
   Dec(L);
  end else Include(P^, P^);
  PSave := P;
  Inc(P);
  Dec(L);
 end;
end;

procedure TWideCharSet.Initialize;
begin
 FNodeClass := TWideCharRange;
 FAssignableClass := TWideCharSet;
 FMin := $FFFF;
 FMax := 0;
end;

function TWideCharSet.IsOwnerOf(AChar: WideChar): Boolean;
begin
 Result := Find(Ord(AChar)) <> nil;
end;

var
 stdin: THandle;

function GetCh: WideChar;
var
 CharsRead: LongWord;
begin
 Win32Check(ReadConsoleW(stdin, @Result, 1, CharsRead, nil));
end;

function OldConsoleMode(handle: THandle): LongWord;
begin
 Win32Check(GetConsoleMode(handle, Result));
end;

function KeyPressed: Boolean;
var
 i, numEvents: LongWord;
 events: array of TInputRecord;
begin
 Result := False;
 Win32Check(GetNumberOfConsoleInputEvents(stdin, numEvents));
 if numEvents > 0 then
 begin
  SetLength(events, numEvents);
  Win32Check(PeekConsoleInputW(stdin, events[0], numEvents, numEvents));
  for i:= 0 to numEvents - 1 do
   if (events[i].EventType = key_event) and
      (events[i].Event.KeyEvent.bKeyDown) then
   begin
    Result := True;
    Break;
   end;
 end;
end;

procedure SetInputHandle;
begin
 stdin := GetStdHandle(STD_INPUT_HANDLE);
 SetConsoleMode(stdin, OldConsoleMode(stdin) and
  not (ENABLE_LINE_INPUT or ENABLE_ECHO_INPUT));
end;

procedure RestoreInputHandle;
begin
 SetConsoleMode(stdin,
  OldConsoleMode(stdin) or (ENABLE_LINE_INPUT or ENABLE_ECHO_INPUT));
end;

function WideStringReplace(const S, OldPattern, NewPattern: WideString;
  Flags: TReplaceFlags): WideString;
var
  SearchStr, Patt, NewStr: WideString;
  Offset: Integer;
begin
  if rfIgnoreCase in Flags then
  begin
    SearchStr := WideUpperCase(S);
    Patt := WideUpperCase(OldPattern);
  end else
  begin
    SearchStr := S;
    Patt := OldPattern;
  end;
  NewStr := S;
  Result := '';
  while SearchStr <> '' do
  begin
    Offset := Pos(Patt, SearchStr);
    if Offset = 0 then
    begin
      Result := Result + NewStr;
      Break;
    end;
    Result := Result + Copy(NewStr, 1, Offset - 1) + NewPattern;
    NewStr := Copy(NewStr, Offset + Length(OldPattern), MaxInt);
    if not (rfReplaceAll in Flags) then
    begin
      Result := Result + NewStr;
      Break;
    end;
    SearchStr := Copy(SearchStr, Offset + Length(Patt), MaxInt);
  end;
end;


initialization
{$IFDEF MYUTILS_UTF8}
 @UTF8toUTF16 := Addr(_UTF8toUTF16);
 @UTF16toUTF8 := Addr(_UTF16toUTF8);
{$ELSE}
 @UTF8toUTF16 := Addr(Utf8Decode);
 @UTF16toUTF8 := Addr(Utf8Encode);
{$ENDIF}
finalization
 if OEMCPInfo <> NIL then Dispose(OEMCPInfo);
end.
