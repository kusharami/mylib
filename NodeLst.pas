unit NodeLst;

interface

(*   Copyright!  *)
(* Made by Jinni *)
(*  Do not sell! *)


uses SysUtils;

type
 TNode = class;
 TNodeClass = class of TNode;
 TNotifyEvent = procedure (Sender: TObject) of object;
 TNode = class
  protected
    FIndex: Integer; //Node index
    FOwner: TNode;   //Link to owner node
    FPrev: TNode;    //Link to previous node
    FNext: TNode;    //Link to next node
    FAssignableClass: TNodeClass;
    FTag: Integer;
    FMaxCount: Integer;         // Maximum nodes to have
    FOnChange: TNotifyEvent;

    procedure AssignError(Source: TNode);
    procedure Initialize; virtual;
    procedure JustAdded; virtual;
  public
    property AssignableClass: TNodeClass read FAssignableClass;
    property Index: Integer read FIndex write FIndex;
    property Owner: TNode read FOwner write FOwner;
    property Prev: TNode read FPrev;
    property Next: TNode read FNext;
    property UserTag: Integer read FTag write FTag;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;

    constructor Create;
    procedure Assign(Source: TNode); virtual;
    procedure CheckSource(Source: TNode);
    procedure Changed; virtual;
 end;

 TNodeArray = array of TNode;

 TNodeList = class;

 TNodeListEvent = procedure(Sender: TNodeList; Node: TNode) of object;

 TNodeList = class(TNode)
  private
    FRoot: TNode;               // Link to first node
    FLast: TNode;               // Link to last node
    FCount: Integer;            // Nodes count
    FNodes: TNodeArray;         // Array of links to all nodes
    FOnRemove: TNodeListEvent;
    FOnAdded: TNodeListEvent;
    function GetNode(Index: Integer): TNode;
    function GetRootLink: Pointer;
    function GetLastLink: Pointer;
    procedure SetCount(Value: Integer);
    function GetLevel: Integer;
    procedure SetMaxCount(Value: Integer);
    function GetUnited: Boolean;
  protected
    FNodeClass: TNodeClass;     // Default class used in AddNode method
    FNodeClassAssignable: Boolean;
    procedure SetUnited(Value: Boolean); virtual;
    procedure Initialize; override;
    procedure ClearData; virtual;
    procedure AssignAdd(Source: TNode); virtual;
    procedure InsertError(Source: TNodeList);
    procedure AttachNodeError(ANode: TNode);

    procedure DoRemove(Node: TNode); virtual;    
  public
    property NodeClass: TNodeClass read FNodeClass;  
    property Level: Integer read GetLevel;
    property RootNode: TNode read FRoot;
    property LastNode: TNode read FLast;
    property RootLink: Pointer read GetRootLink;
    property LastLink: Pointer read GetLastLink;
    property Count: Integer read FCount write SetCount;
    property MaxCount: Integer read FMaxCount write SetMaxCount;
    property United: Boolean read GetUnited write SetUnited;
    property Nodes[Index: Integer]: TNode read GetNode;

    property OnRemove: TNodeListEvent read FOnRemove write FOnRemove;
    property OnAdded: TNodeListEvent read FOnAdded write FOnAdded;

    function IsChild(Node: TNode): Boolean;
    function AddNode: TNode; overload;
    function AddNode(NodeClass: TNodeClass): TNode; overload;
    procedure AddCreated(Node: TNode; FirstTime: Boolean);
    procedure DetachNode(Node: TNode);
    procedure AttachNode(Node: TNode; AIndex: Integer);
    function InsertNode(Source: TNode; AIndex: Integer): TNode;
    function InsertList(Source: TNodeList; AIndex: Integer): TNode;
    procedure Assign(Source: TNode); override;
    procedure Clear;
    destructor Destroy; override;
    procedure Exchange(Index1, Index2: Integer);
    procedure Remove(Node: TNode); overload;
    procedure Remove(Index: Integer); overload;
    procedure MoveTo(CurIndex, NewIndex: Integer);
 end;

 TVariousNodeList = class(TNodeList)
  protected
    procedure Initialize; override;
 end;

 EAttachNodeError = class(Exception);
 ENodeListError = class(Exception);

const
 SAttachNodeError: PChar = 'Cannot attach %s node to %s';
 SAssignError: PChar = 'Cannot assign %s to %s';
 SInsertError: PChar = 'Cannot insert %s to %s';
 SMaxCountExceedError: PChar = 'Cannot add more nodes';

implementation

procedure CannotAddMoreNodes;
begin
 raise ENodeListError.Create(SMaxCountExceedError);
end;

(* TNode *)

procedure TNode.Assign(Source: TNode);
begin
 CheckSource(Source);
end;

procedure TNode.AssignError(Source: TNode);
var
 SourceName: String;
begin
 if Source <> nil then
  SourceName := Source.ClassName else
  SourceName := 'nil';
 raise EConvertError.CreateFmt(SAssignError, [SourceName, ClassName]);
end;

procedure TNode.Changed;
begin
 if Assigned(FOnChange) then
  FOnChange(Self);
end;

procedure TNode.CheckSource(Source: TNode);
begin
 if (Source = NIL) or not (Source is FAssignableClass) then
  AssignError(Source);
end;

constructor TNode.Create;
begin
 FAssignableClass := TNodeClass(ClassType);
 FMaxCount := MaxInt; 
 Initialize;
end;

procedure TNode.Initialize;
begin
 // do nothing
end;

procedure TNode.JustAdded;
begin
 // do nothing
end;

(* TNodeList *)

procedure TNodeList.AddCreated(Node: TNode; FirstTime: Boolean);
begin
 Node.FOwner := Self;
 Node.FIndex := FCount;
 Node.FPrev := FLast;
 Node.FNext := NIL;
 if FRoot = NIL then
  FRoot := Node else
  FLast.FNext := Node;
 FLast := Node;
 Inc(FCount);
 United := False;
 Changed;
 if FirstTime then
  Node.JustAdded;
 if Assigned(FOnAdded) then
  FOnAdded(Self, Node);  
end;

function TNodeList.AddNode: TNode;
begin
 if FCount >= FMaxCount then
  CannotAddMoreNodes;
 Result := FNodeClass.Create;
 AddCreated(Result, True);
end;

function TNodeList.AddNode(NodeClass: TNodeClass): TNode;
begin
 if FCount >= FMaxCount then
  CannotAddMoreNodes;
 if NodeClass = NIL then
  Result := FNodeClass.Create else
  Result := NodeClass.Create;
 AddCreated(Result, True);
end;

procedure TNodeList.Assign(Source: TNode);
var
 N: TNode;
begin
 if Source <> Self then
 begin
  inherited;
  FNodeClass := TNodeList(Source).FNodeClass;
  Clear;
  N := TNodeList(Source).FRoot;
  while N <> NIL do
  begin
   AssignAdd(N);
   N := N.Next;
  end;
 end;
end;

procedure TNodeList.AssignAdd(Source: TNode);
begin
 if FNodeClassAssignable then
  AddNode(TNodeClass(Source.ClassType)).Assign(Source) else
  AddNode.Assign(Source);
end;

procedure TNodeList.AttachNode(Node: TNode; AIndex: Integer);
var
 Dest, PR: TNode;
begin
 if not FNodeClassAssignable and not (Node is FNodeClass) then
  AttachNodeError(Node);
 if (FCount <= 0) or (AIndex >= FCount) then
 begin
  AddCreated(Node, False);
  Exit;
 end else
 begin
  Node.FOwner := Self;
  if AIndex < 0 then
  begin
   FRoot.FPrev := Node;
   Node.FPrev := nil;
   Node.FNext := FRoot;
  end else
  begin
   Dest := Nodes[AIndex];
   PR := Dest.FPrev;
   if PR <> nil then
    PR.FNext := Node else
    FRoot := Node;
   Node.FPrev := PR;
   Node.FNext := Dest;
   Dest.FPrev := Node;
  end;
 end;
 Inc(FCount);
 United := False;
 Changed;
 if Assigned(FOnAdded) then
  FOnAdded(Self, Node);
end;

procedure TNodeList.AttachNodeError(ANode: TNode);
var
 S: String;
begin
 if ANode = nil then
  S := 'nil' else
  S := ANode.ClassName;
 raise EAttachNodeError.CreateFmt(SAttachNodeError, [ClassName, S]);
end;

procedure TNodeList.Clear;
var
 N: TNode;
begin
 ClearData;
 Finalize(FNodes);
 FLast := NIL;
 FCount := 0; 
 while FRoot <> NIL do
 begin
  N := FRoot.FNext;
  DoRemove(FRoot);
  FRoot.Free;
  FRoot := N;
 end;
 Changed;
end;

procedure TNodeList.ClearData;
begin
 (* do nothing *)
end;

destructor TNodeList.Destroy;
begin
 Clear;
 inherited;
end;

procedure TNodeList.DetachNode(Node: TNode);
var
 PR: TNode;
begin
 if IsChild(Node) then
 begin
  PR := Node.FPrev;
  with Node do
  begin
   if Node = FRoot then
   begin
    if FRoot = FLast then FLast := NIL;
    FRoot := FNext;
    if FRoot <> NIL then FRoot.FPrev := NIL;
   end else
   begin
    PR.FNext := FNext;
    if FNext = NIL then
     FLast := PR else
     FNext.FPrev := PR;
   end;
   FIndex := -1;
  end;
  Dec(FCount);
  United := False;
  Changed;
 end;
end;

procedure TNodeList.DoRemove(Node: TNode);
begin
 if Assigned(FOnRemove) then
  FOnRemove(Self, FRoot);
end;

procedure TNodeList.Exchange(Index1, Index2: Integer);
var
 N1, N2, TempNext, TempPrev: TNode;
 TmpIndex: Integer;
begin
 if Index1 <> Index2 then
 begin
  if Index2 < Index1 then
  begin
   TmpIndex := Index1;
   Index1 := Index2;
   Index2 := TmpIndex;
  end;
  N1 := Nodes[Index1];
  N2 := Nodes[Index2];
  if (N1 <> NIL) and (N2 <> NIL) then
  begin
   if Index1 + 1 <> Index2 then
   begin
    TempPrev := N1.FPrev;
    TempNext := N1.FNext;

    N1.FIndex := Index2;
    N2.FPrev.FNext := N1;
    N1.FPrev := N2.FPrev;
    N2.FNext.FPrev := N1;
    N1.FNext := N2.FNext;

    N2.FIndex := Index1;
    N1.FPrev.FNext := N2;
    N2.FPrev := TempPrev;
    N1.FNext.FPrev := N2;
    N2.FNext := TempNext;
   end else
   begin
    TempPrev := N1.FPrev;

    N1.FIndex := Index2;
    N1.FNext := N2.FNext;
    N1.FPrev := N2;

    N2.FIndex := Index1;
    N2.FNext := N1;
    N2.FPrev := TempPrev;
   end;
   if Index1 = 0 then FRoot := N2;
   if Index2 = FCount - 1 then FLast := N1;
   if FNodes <> nil then
   begin
    N1 := FNodes[Index1];
    FNodes[Index1] := FNodes[Index2];
    FNodes[Index2] := N1;
   end;
  end;
 end;
 Changed;
end;

function TNodeList.GetLastLink: Pointer;
begin
 if FCount > 0 then
 begin
  United := True;
  Result := Addr(FNodes[FCount - 1]);
 end else Result := NIL;
end;

function TNodeList.GetLevel: Integer;
var
 Node: TNode;
begin
 Node := FOwner;
 Result := 0;
 while Node <> nil do
 begin
  Node := Node.FOwner;
  Inc(Result);
 end;
end;

function TNodeList.GetNode(Index: Integer): TNode;
begin
 if (Index >= 0) and (Index < FCount) then
 begin
  United := True;
  Result := FNodes[Index];
 end else Result := NIL;
end;

function TNodeList.GetRootLink: Pointer;
begin
 if FCount > 0 then
 begin
  United := True;
  Result := Pointer(FNodes);
 end else Result := NIL;
end;

function TNodeList.GetUnited: Boolean;
begin
 Result := FNodes <> nil;
end;

procedure TNodeList.Initialize;
begin
 FNodeClass := TNode;
 FAssignableClass := TNodeList;
 FNodeClassAssignable := False;
end;

procedure TNodeList.InsertError(Source: TNodeList);
var
 SourceName: String;
begin
 if Source <> nil then
  SourceName := Source.ClassName else
  SourceName := 'nil';
 raise ENodeListError.CreateFmt(SInsertError, [SourceName, ClassName]);
end;

function TNodeList.InsertList(Source: TNodeList; AIndex: Integer): TNode;
var
 N, Next: TNode;
begin
 Result := nil;
 if Source <> Self then
 begin
  if (Source = nil) or not (Source is FAssignableClass) then
   InsertError(Source);
  N := Source.FRoot;
  while N <> nil do
  begin
   Next := N.Next;
   if Result = nil then
    Result := InsertNode(N, AIndex) else
    InsertNode(N, AIndex);
   Inc(AIndex);
   N := Next;
  end;
 end;
end;

function TNodeList.InsertNode(Source: TNode; AIndex: Integer): TNode;
begin
 AssignAdd(Source);
 Result := FLast;
 DetachNode(Result);
 AttachNode(Result, AIndex);
 United := True;
end;

function TNodeList.IsChild(Node: TNode): Boolean;
begin
 Result := (Node <> nil) and
           (FCount > 0) and
           (Node.Index >= 0) and
           (Node.Owner = Self);
end;

procedure TNodeList.MoveTo(CurIndex, NewIndex: Integer);
var
 Node: TNode;
begin
 Node := Nodes[CurIndex];
 DetachNode(Node);
 AttachNode(Node, NewIndex);
 United := True;
end;

procedure TNodeList.Remove(Node: TNode);
begin
 if IsChild(Node) then
 begin
  DoRemove(Node);
  DetachNode(Node);
  Node.Free;
  United := True;
 end;
end;

procedure TNodeList.Remove(Index: Integer);
begin
 Remove(Nodes[Index]);
end;

procedure TNodeList.SetCount(Value: Integer);
var
 I: Integer;
 Node, Next: TNode;
begin
 if Value < Count then
 begin
  Node := Nodes[Value];
  for I := Value to Count - 1 do
  begin
   Next := Node.Next;
   DoRemove(Node);
   DetachNode(Node);
   Node.Free;
   Node := Next;
  end;
  United := True;
 end else
  for I := Count to Value - 1 do AddNode;
end;

procedure TNodeList.SetMaxCount(Value: Integer);
begin
 FMaxCount := Value;
 if FCount > Value then
  SetCount(Value);
end;

procedure TNodeList.SetUnited(Value: Boolean);
var
 P: ^TNode;
 N: TNode;
 I: Integer;
begin
 if not Value then
  Finalize(FNodes) else
 if FNodes = nil then
 begin
  SetLength(FNodes, FCount);
  P := Pointer(FNodes);
  N := FRoot;
  I := 0;
  while N <> NIL do
  begin
   P^ := N;
   Inc(P);
   N.FIndex := I;
   N := N.FNext;
   Inc(I);
  end;
 end;
end;

{ TVariousNodeList }

procedure TVariousNodeList.Initialize;
begin
 inherited;
 FNodeClassAssignable := True;
end;

end.
