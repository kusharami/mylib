unit NodeLst_old;

interface

Type
 PNodeRec = ^TNodeRec;
 TNodeRec = Record
  Node: TObject;
  Prev: PNodeRec;
  Next: PNodeRec;
 end;
 TObjectArray = Array of TObject;

 TNodeList = Class(TObject)
  private
   FRoot: PNodeRec;
   FCur: PNodeRec;
   FCount: Integer;
   FUnited: Boolean;
   FAutoUnite: Boolean;
   FObjects: TObjectArray;
   function Get(Index: Integer): PNodeRec;
   procedure SetAutoUnite(Value: Boolean);
   function GetObject(Index: Integer): TObject;
   function GetRootObject: Pointer;
   function GetLastObject: Pointer;
  public
   procedure Clear; virtual;
   procedure Unite; virtual;
   Constructor Create;
   Destructor Destroy; override;
   Function Add: PNodeRec;
   Procedure Remove(Item: PNodeRec); virtual;
   Procedure Exchange(Index1, Index2: Integer); virtual;
   Procedure Move(CurIndex, NewIndex: Integer); virtual;
   Property Count: Integer read FCount;
   Property Root: PNodeRec read FRoot;
   Property Cur: PNodeRec read FCur;
   Property Items[Index: Integer]: PNodeRec read Get;
   Property Objects[Index: Integer]: TObject read GetObject;
   Property United: Boolean read FUnited;
   Property AutoUnite: Boolean read FAutoUnite write SetAutoUnite;
   Property RootObject: Pointer read GetRootObject;
   Property LastObject: Pointer read GetLastObject;
 end;
 TNodeListEx = Class(TNodeList)
 public
  Procedure Clear; override;
 end;
 TDataNode = Class(TObject)
 private
  FIndex: Integer;
  FPosition: Integer;
  FDataSize: Integer;
  FFreeAfterUse: Boolean;
  FData: Pointer;
 public
  property Index: Integer read FIndex;
  property Data: Pointer read FData;
  property DataSize: Integer read FDataSize;
  property FreeAfterUse: Boolean read FFreeAfterUse write FFreeAfterUse;
  property Position: Integer read FPosition;

  constructor Create; overload; virtual;
  constructor Create(Size: Integer); overload; virtual;
  Destructor Destroy; override;
 end;
 TDataNodeClass = class of TDataNode;
 TDataList = Class(TNodeList)
 private
  FDataNodeClass: TDataNodeClass;
  FDataSize: Integer;
  FFullSize: Integer;
  Function GetData(Index: Integer): Pointer;
 public
  property FullSize: Integer read FFullSize;
  property DataSize: Integer read FDataSize write FDataSize;
  property DataNodeClass: TDataNodeClass read FDataNodeClass
                                        write FDataNodeClass;
  Property Buffers[Index: Integer]: Pointer read GetData;

  Constructor Create(Size: Integer = -1);
  Function AddData(Size: Integer = - 1): Pointer;
  function AddNode(Data: Pointer; Size: Integer = -1): TDataNode;
  function FindData(var Buffer; Size: Integer = -1): Pointer;
  function FindNode(var Buffer; Size: Integer = -1): TDataNode;
  function GetFullSize: Integer;
 end;

implementation

uses SysUtils;

Procedure TNodeList.Unite;
Var N: PNodeRec; P: ^TObject;
begin
 If FUnited then Exit;
 FUnited := True;
 Finalize(FObjects);
 SetLength(FObjects, FCount);
 If FCount = 0 then Exit;
 N := FRoot;
 P := Addr(FObjects[0]);
 While N <> NIL do With N^ do
 begin
  P^ := Node;
  Inc(P);
  N := Next;
 end;
end;

Function TNodeList.Get(Index: Integer): PNodeRec;
Var I: Integer; N: PNodeRec;
begin
 I := 0; N := FRoot; Result := NIL;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Constructor TNodeList.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TNodeList.Destroy;
begin
 Finalize(FObjects);
 Clear;
 inherited Destroy;
end;

Function TNodeList.Add: PNodeRec;
begin
 FUnited := False;
 New(Result);
 Result.Prev := FCur;
 If FRoot = NIL then FRoot := Result Else FCur^.Next := Result;
 FCur := Result;
 Result.Node := NIL;
 Result.Next := NIL;
 Inc(FCount);
end;

Procedure TNodeList.Clear;
Var N: PNodeRec;
begin
 FUnited := False;
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  With FRoot^ do
  begin
   Node.Free;
   Node := NIL;
  end;
  Dispose(FRoot);
  FRoot := N;
 end;
 Finalize(FObjects);
 FCur := NIL;
 FCount := 0;
end;

Procedure TNodeList.Remove(Item: PNodeRec);
Var PR: PNodeRec;
begin
 If Item = NIL then Exit;
 PR := Item.Prev;
 With Item^ do
 begin
  If Item = FRoot then
  begin
   If FRoot = FCur then FCur := NIL;
   FRoot := Next;
   If FRoot <> NIL then FRoot.Prev := NIL;
  end Else If Next = NIL then
  begin
   PR.Next := NIL;
   FCur := PR;
  end Else
  begin
   PR.Next := Next;
   Next.Prev := PR;
  end;
  Node.Free;
  Node := NIL;
 end;
 Dispose(Item);
 Dec(FCount);
 FUnited := False;
 If FAutoUnite then Unite;
end;

Procedure TNodeList.Exchange(Index1, Index2: Integer);
Var N1, N2: PNodeRec; Node: TObject; X: TObject; I: Integer;
begin
 If (Index2 = Index1) or
    (Index1 < 0) or (Index1 >= FCount) or
    (Index2 < 0) or (Index2 >= FCount) then Exit;
 If Index2 < Index1 then
 begin
  I := Index2;
  Index2 := Index1;
  Index1 := I;
 end;
 N1 := FRoot; I := 0;
 While N1 <> NIL do
 begin
  If I = Index1 then Break;
  Inc(I);
  N1 := N1.Next;
 end;
 Inc(I);
 N2 := N1.Next;
 While N2 <> NIL do
 begin
  If I = Index2 then
  begin
   Node := N1.Node;
   N1.Node := N2.Node;
   N2.Node := Node;
   If FUnited then
   begin
    X := FObjects[Index1];
    FObjects[Index1] := FObjects[Index2];
    FObjects[Index2] := X;
   end Else If FAutoUnite then Unite Else FUnited := False;
   Exit;
  end;
  Inc(I);
  N2 := N2.Next;
 end;
end;

Procedure TNodeList.Move(CurIndex, NewIndex: Integer);
Var C1, C2, NN: PNodeRec; N: TObject;
begin
 If (CurIndex = NewIndex) or (FCount < 2) or (NewIndex >= FCount) then Exit;
 C1 := Get(CurIndex);
 If C1 = NIL then Exit;
 N := C1.Node;
 C1.Node := NIL;
 Remove(C1);
 If NewIndex > 0 then
 begin
  C1 := Get(NewIndex - 1);
  If C1 = NIL then Exit;
  C2 := C1.Next;
  New(NN);
  NN.Node := N;
  NN.Next := C2;
  NN.Prev := C1;
  If C2 = NIL then FCur := NN;
  C1.Next := NN;
  Inc(FCount);
 end Else If NewIndex = 0 then
 begin
  New(NN);
  NN.Node := N;
  NN.Next := FRoot;
  NN.Prev := NIL;
  FRoot := NN;
  Inc(FCount);
 end;
 FUnited := False;
 If FAutoUnite then Unite;
end;

Procedure TNodeListEx.Clear;
Var N: PNodeRec;
begin
 FUnited := False;
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
 If FAutoUnite then Unite;
end;

Constructor TDataList.Create(Size: Integer = -1);
begin
 if Size < 0 then Size := 0;
 inherited Create;
 FDataSize := Size;
 FDataNodeClass := TDataNode;
end;

Function TDataList.AddData(Size: Integer = - 1): Pointer;
Var N: PNodeRec;
begin
 N := Add;
 With N^ do
 begin
  if Size < 0 then
   Node := FDataNodeClass.Create(FDataSize) else
   Node := FDataNodeClass.Create(Size);
  TDataNode(Node).FIndex := Count - 1;
  Result := TDataNode(Node).FData;
 end;
 If FAutoUnite then Unite;
end;

function TDataList.AddNode(Data: Pointer; Size: Integer = -1): TDataNode;
begin
 Result := FDataNodeClass.Create;
 Result.FData := Data;
 Result.FPosition := FFullSize;
 if Size < 0 then
  Result.FDataSize := FDataSize else
  Result.FDataSize := Size;
 Result.FIndex := Count;
 Add.Node := Result;
 Inc(FFullSize, Result.FDataSize);
end;

Function TDataList.GetData(Index: Integer): Pointer;
Var N: PNodeRec; DataNode: TDataNode;
begin
 N := Items[Index];
 Result := NIL;
 If N <> NIL then
 begin
  DataNode := TDataNode(N^.Node);
  If DataNode <> NIL then Result := DataNode.Data;
 end;
end;

Constructor TDataNode.Create(Size: Integer);
begin
 if Size < 0 then Size := 0;
 GetMem(FData, Size);
 FillChar(FData^, Size, 0);
 FDataSize := Size;
 FFreeAfterUse := True;
end;

Constructor TDataNode.Create;
begin
 FData := NIL;
 FDataSize := 0;
 FFreeAfterUse := True;
end;

Destructor TDataNode.Destroy;
begin
 If FFreeAfterUse and (FData <> NIL) then FreeMem(FData);
 inherited Destroy;
end;

procedure TNodeList.SetAutoUnite(Value: Boolean);
begin
 If FAutoUnite <> Value then
 begin
  FAutoUnite := Value;
  Unite;
 end;
end;

function TNodeList.GetObject(Index: Integer): TObject;
begin
 If United then
 begin
  If (Index < 0) or (Index >= Count) then
   Result := NIL Else
   Result := FObjects[Index];
 end Else Result := Items[Index].Node;
end;

function TNodeList.GetRootObject: Pointer;
begin
 If FUnited and (Length(FObjects) = FCount) then
  Result := Addr(FObjects[0]) Else
  Result := NIL;
end;

function TNodeList.GetLastObject: Pointer;
begin
 If FUnited and (Length(FObjects) = FCount) then
  Result := Addr(FObjects[FCount - 1]) Else
  Result := NIL;
end;

function TDataList.FindData(var Buffer; Size: Integer = -1): Pointer;
var Node: TDataNode;
begin
 Node := FindNode(Buffer, Size);
 if Node <> NIL then
  Result := Node.FData else
  Result := NIL;
end;

function TDataList.FindNode(var Buffer; Size: Integer = -1): TDataNode;
var N: PNodeRec; DN: TDataNode;
begin
 if Size < 0 then Size := FDataSize;
 Result := NIL;
 N := FRoot;
 while N <> NIL do with N^ do
 begin
  if (Node <> NIL) and (Node is TDataNode) then
  begin
   DN := TDataNode(Node);
   with DN do if (FDataSize = Size) and CompareMem(@Buffer, FData, Size) then
   begin
    Result := DN;
    Exit;
   end;
  end;
  N := Next;
 end;
end;

function TDataList.GetFullSize: Integer;
var N: PNodeRec;
begin
 N := FRoot;
 Result := 0;
 while N <> NIL do with N^ do
 begin
  if (Node <> NIL) and (Node is TDataNode) then
   Inc(Result, TDataNode(Node).FDataSize);
  N := Next;
 end;
end;

end.
