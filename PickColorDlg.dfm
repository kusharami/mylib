object PickColorDialog: TPickColorDialog
  Left = 246
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Color Picker'
  ClientHeight = 291
  ClientWidth = 509
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poDesktopCenter
  OnCreate = TntFormCreate
  DesignSize = (
    509
    291)
  PixelsPerInch = 96
  TextHeight = 13
  object ScreenPickerBtn: TSpeedButton
    Left = 400
    Top = 8
    Width = 23
    Height = 22
    Hint = 'Pick color from screen'
    Anchors = [akTop, akRight]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFBFBF00BFBF00BFBF00BFBF00BFBF00BFBF00BF
      BF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBFBF00BFBF00
      000000000000000000BFBF00BFBF00BFBF00BFBF00BFBF00FF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFBFBF00000000BFBF00BFBF00000000BFBF00BF
      BF00BFBF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      000000FFFFFFFFFFFFBFBF00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFBFBF0000
      0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FF000000FFFFFFFFFFFFBFBF00000000FF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFF
      FFFF808080000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFF808080000000FF00FF0000
      00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00
      0000FFFFFFFFFFFF808080000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFF0000000000000000
      00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FF000000000000000000000000000000000000FF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000000000000000
      00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FF000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000C0C0
      C0000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
      00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FF}
    OnMouseDown = ScreenPickerBtnMouseDown
    OnMouseMove = ScreenPickerBtnMouseMove
    OnMouseUp = ScreenPickerBtnMouseUp
  end
  object ColorPropLabel: TTntLabel
    Left = 341
    Top = 72
    Width = 108
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Picked color properties'
  end
  object CollectedColorPropLabel: TTntLabel
    Left = 341
    Top = 176
    Width = 124
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Collection color properties'
  end
  object OKBtn: TTntButton
    Left = 427
    Top = 8
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TTntButton
    Left = 427
    Top = 38
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object HSPanel: TPanel
    Left = 8
    Top = 8
    Width = 260
    Height = 260
    BevelOuter = bvNone
    BorderStyle = bsSingle
    TabOrder = 2
    object HSColorPickerEx: THSColorPickerEx
      Left = 0
      Top = 0
      Width = 256
      Height = 256
      HintFormat = 'H: %h S: %hslS'#13'Hex: %hex'
      Align = alClient
      TabOrder = 0
      OnMouseDown = HSColorPickerMouseDown
      OnMouseMove = HSColorPickerMouseMove
      OnMouseUp = PickerMouseUp
    end
  end
  object LColorPickerPanel: TPanel
    Left = 280
    Top = 4
    Width = 22
    Height = 268
    BevelOuter = bvNone
    TabOrder = 3
    object LColorPickerEx: TLColorPickerEx
      Left = 0
      Top = 0
      Width = 22
      Height = 268
      HintFormat = 'Luminance: %value'
      Align = alClient
      TabOrder = 0
      OnMouseDown = LColorPickerMouseDown
      OnMouseMove = LColorPickerMouseMove
      OnMouseUp = PickerMouseUp
    end
  end
  object ColorsCollectionView: TTileMapView
    Left = 312
    Top = 8
    Width = 20
    Height = 260
    Hint = 
      'Double click to select the color / Ctrl and left click to remove' +
      ' color /  Ctrl and right click to replace color'
    MapWidth = 1
    MapHeight = 16
    OffsetX = 0
    OffsetY = 0
    GridStyle = psSolid
    GridMode = pmMask
    DragScrollShiftState = []
    ScrollBarsAllwaysVisible = False
    OnDrawCell = ColorsCollectionViewDrawCell
    OnSelectionChanged = ColorsCollectionViewSelectionChanged
    DragCursor = 983
    ParentShowHint = False
    ShowHint = False
    TabOrder = 4
    OnMouseDown = ColorsCollectionViewMouseDown
  end
  object ColorPropEditor: TPropertyEditor
    Left = 341
    Top = 88
    Width = 161
    Height = 76
    BoldChangedValues = False
    Anchors = [akTop, akRight]
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 5
    PropertyWidth = 80
    ValueWidth = 77
    OnEnter = PropEditorEnter
    OnExit = PropEditorExit
    OnUpButton = PropEditorUpButton
    OnDownButton = PropEditorDownButton
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 80
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 77
      end>
  end
  object CollectedColorPropEditor: TPropertyEditor
    Left = 341
    Top = 192
    Width = 161
    Height = 76
    BoldChangedValues = False
    Anchors = [akTop, akRight]
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 6
    PropertyWidth = 80
    ValueWidth = 77
    OnEnter = PropEditorEnter
    OnExit = PropEditorExit
    OnUpButton = PropEditorUpButton
    OnDownButton = PropEditorDownButton
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 80
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 77
      end>
  end
  object ColorPreview: TmbColorPreview
    Left = 340
    Top = 8
    Width = 56
    Height = 56
    Hint = 
      'Double click to add color to collection / Ctrl and click to repl' +
      'ace selected color in collection'
    Color = clBlack
    SwatchStyle = True
    Anchors = [akTop, akRight]
    ShowHint = False
    ParentShowHint = False
    OnMouseDown = ColorPreviewMouseDown
    OnDblClick = ColorPreviewDblClick
  end
  object StatusBar: TTntStatusBar
    Left = 0
    Top = 272
    Width = 509
    Height = 19
    AutoHint = True
    Panels = <
      item
        Width = 50
      end>
  end
end
