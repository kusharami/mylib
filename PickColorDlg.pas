unit PickColorDlg;

interface

uses Windows, SysUtils, TntClasses, TntForms, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, mbColorPickerControl, VirtualTrees, PropEditor,
  TileMapView, mbTrackBarPicker, LColorPickerEx, mbColorPreview, PropContainer,
  HSColorPickerEx, TntStdCtrls, ComCtrls, TntComCtrls,
  Dialogs, TntDialogs, BitmapEx, bmpimg;

type
  TPickColorDialog = class(TTntForm)
    OKBtn: TTntButton;
    CancelBtn: TTntButton;
    HSPanel: TPanel;
    ColorPreview: TmbColorPreview;
    LColorPickerPanel: TPanel;
    HSColorPickerEx: THSColorPickerEx;
    LColorPickerEx: TLColorPickerEx;
    ScreenPickerBtn: TSpeedButton;
    ColorsCollectionView: TTileMapView;
    ColorPropEditor: TPropertyEditor;
    CollectedColorPropEditor: TPropertyEditor;
    ColorPropLabel: TTntLabel;
    CollectedColorPropLabel: TTntLabel;
    StatusBar: TTntStatusBar;
    procedure TntFormCreate(Sender: TObject);
    procedure ColorsCollectionViewDrawCell(Sender: TCustomTileMapView;
      const Rect: TRect; CellX, CellY: Integer);
    procedure ColorValueChange(Sender: TPropertyListItem);
    procedure ColorPreviewDblClick(Sender: TObject);
    procedure ScreenPickerBtnMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ScreenPickerBtnMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure HSColorPickerMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure HSColorPickerMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LColorPickerMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PickerMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LColorPickerMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PropEditorExit(Sender: TObject);
    procedure PropEditorEnter(Sender: TObject);
    procedure ColorsCollectionViewSelectionChanged(
      Sender: TCustomTileMapView);
    procedure ScreenPickerBtnMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ColorsCollectionViewMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ColorPreviewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PropEditorDownButton(
      Sender: TCustomPropertyEditor; Node: PVirtualNode;
      var Result: Boolean);
    procedure PropEditorUpButton(
      Sender: TCustomPropertyEditor; Node: PVirtualNode;
      var Result: Boolean);
  private
    FColorFormat: TColorFormat;
    FColor: TColor;
    FColorsCollection: array[0..15] of TColor;
    FColorPickerCursor: TCursor;
    FMode: Byte;
    FConvert: Boolean;
    function GetFormattedColor(Value: TColor): TColor;
    procedure RefreshProps(Editor: TPropertyEditor);
    procedure SetColor(Value: TColor);
    function GetCollectedColorPtr: PColor;
    function GetCollectionColor(I: Integer): TColor;
    procedure SetCollectionColor(I: Integer; Value: TColor);
    function GetCollectedColor: TColor;
    procedure SetCollectedColor(Value: TColor);
    function GetCollectionSize: Integer;
    procedure SetColorFormat(Value: TColorFormat);
  protected
   property CollectedColorPtr: PColor read GetCollectedColorPtr;
  public
    property ColorPickerCursor: TCursor read FColorPickerCursor write FColorPickerCursor;
    property Color: TColor read FColor write SetColor;
    property CollectionSize: Integer read GetCollectionSize;
    property ColorsCollection[I: Integer]: TColor read GetCollectionColor
                                                 write SetCollectionColor;
    property CollectedColor: TColor read GetCollectedColor
                                   write SetCollectedColor;
    property ColorFormat: TColorFormat read FColorFormat
                                      write SetColorFormat;

    function Execute: Boolean;

    procedure AddToCollection(Color: TColor);
    destructor Destroy; override;
  end;

 TRGB = packed record
  R, G, B, Z: Byte;
 end;

function CreatePickColorDialog: TPickColorDialog;

implementation

uses HexUnit;

{$R *.dfm}

function CreatePickColorDialog: TPickColorDialog;
begin
 Application.CreateForm(TPickColorDialog, Result);
end;

function TPickColorDialog.Execute: Boolean;
begin
 Result := ShowModal = mrOk;
end;

procedure TPickColorDialog.TntFormCreate(Sender: TObject);
begin
 FColorFormat := TColorFormat.Create;
 FColorFormat.Assign(RGBZ_ColorFormat);
 FColor := -1;
 Color := clBlack;
 FillChar(FColorsCollection, SizeOf(FColorsCollection), 255);
 ColorPropEditor.PropertyList.OnValueChange := ColorValueChange;
 with CollectedColorPropEditor.PropertyList do
 begin
  UserTag := 1;
  OnValueChange := ColorValueChange;
 end;
 RefreshProps(CollectedColorPropEditor);
end;

procedure TPickColorDialog.ColorsCollectionViewDrawCell(
  Sender: TCustomTileMapView; const Rect: TRect; CellX, CellY: Integer);
var
 cl: TColor;
begin
 cl := FColorsCollection[ColorsCollectionView.InternalIndex];
 with Sender, Canvas do
 begin
  if cl < 0 then
  begin
   Brush.Style := bsDiagCross;
   cl := clWhite;
  end else
   Brush.Style := bsSolid;
  Brush.Color := cl;
  FillRect(Rect);
 end;
end;

procedure TPickColorDialog.RefreshProps(Editor: TPropertyEditor);
var
 cl: TColor;
 RGBQ: TRGBQuad;
begin
 with Editor, PropertyList do
 begin
  ClearList;

  if UserTag <> 0 then
   cl := CollectedColor else
   cl := FColor;

  with TRGB(cl) do
  begin
   if Z <> 0 then
    LongWord(cl) := $FF000000;

   with AddDecimal('Red', R, 0, 255) do
    if Z <> 0 then
     Parameters := PL_MULTIPLE_VALUE;

   with AddDecimal('Green', G, 0, 255) do
    if Z <> 0 then
     Parameters := PL_MULTIPLE_VALUE;

   with AddDecimal('Blue', B, 0, 255) do
    if Z <> 0 then
     Parameters := PL_MULTIPLE_VALUE;

   with TRGB(cl) do
   begin
     RGBQ.rgbRed := R;
     RGBQ.rgbGreen := G;
     RGBQ.rgbBlue := B;
     RGBQ.rgbReserved := 0;
   end;
   with AddString('HEX-value', IntToHex(LongWord(RGBQ), 6)) do
    if Z <> 0 then
     Parameters := PL_MULTIPLE_VALUE;
  end;

  RootNodeCount := Count;
 end;
end;

procedure TPickColorDialog.SetColor(Value: TColor);
begin
 Value := GetFormattedColor(Value);
 if FColor <> Value then
 begin
  FColor := Value;
  RefreshProps(ColorPropEditor);
  ColorPreview.Color := Value;
  HSColorPickerEx.SelectedColor := Value;
  LColorPickerEx.SelectedColor := Value;
 end;
end;

procedure TPickColorDialog.ColorValueChange(Sender: TPropertyListItem);
var
 List: TPropertyList;
 RGB: ^TRGB;

 procedure UpdateRGB;
 var
  I: Integer;
  P: PByte;
 begin
  P := Pointer(RGB);
  with List do
  begin
   for I := 0 to 2 do
    with Properties[I] do
    begin
     Changing := True;
     Parameters := 0;
     Value := P^;
     Changing := False;
     Inc(P);
    end;
  end;
 end;

 procedure UpdateHex;
 var
  RGBQ: TRGBQuad;
 begin
  with List do
   with Properties[3] do
   begin
    Changing := True;
    RGBQ.rgbRed := RGB.R;
    RGBQ.rgbGreen := RGB.G;
    RGBQ.rgbBlue := RGB.B;
    RGBQ.rgbReserved := 0;
    ValueStr := IntToHex(LongInt(RGBQ), 6);
    Changing := False;
   end;
 end;
var
 RGBQ: TRGBQuad;
begin
 if Sender.Parameters and PL_MULTIPLE_VALUE = 0 then
 begin
  List := Sender.Owner as TPropertyList;
  if List.UserTag <> 0 then
   RGB := Pointer(CollectedColorPtr) else
   RGB := @FColor;

  case Sender.Index of
   0, 1, 2:
   begin
    if FColorFormat.Flags and 3 = CFLG_GRAYSCALE then
    begin
     RGB.R := Sender.Value;
     RGB.G := RGB.R;
     RGB.B := RGB.R;
     RGB.Z := 0;
    end else
    case Sender.Index of
     0:
     begin
      RGB.R := Sender.Value;
      if RGB.Z <> 0 then
      begin
       RGB.Z := 0;
       RGB.G := 0;
       RGB.B := 0;
      end;
     end;
     1:
     begin
      RGB.G := Sender.Value;
      if RGB.Z <> 0 then
      begin
       RGB.Z := 0;
       RGB.R := 0;
       RGB.B := 0;
      end;
     end;
     2:
     begin
      RGB.B := Sender.Value;
      if RGB.Z <> 0 then
      begin
       RGB.Z := 0;
       RGB.R := 0;
       RGB.G := 0;
      end;
     end;
    end;
    TColor(RGB^) := GetFormattedColor(TColor(RGB^));
    UpdateRGB;
    UpdateHex;    
   end;
   3:
   begin
    LongInt(RGBQ) := HexToInt(Sender.ValueStr);
    if not HexError and
       (LongInt(RGBQ) >= 0) and
       (LongInt(RGBQ) <= $FFFFFF) then
    begin
     RGB.R := RGBQ.rgbRed;
     RGB.G := RGBQ.rgbGreen;
     RGB.B := RGBQ.rgbBlue;
     RGB.Z := 0;
     TColor(RGB^) := GetFormattedColor(TColor(RGB^));
     UpdateRGB;
    end else
     WideMessageDlg('Value must be in range from 000000 to FFFFFF', mtError, [mbOk], 0);

    UpdateHex;
   end;
  end;
  with List do
  begin
   if UserTag <> 0 then
   begin
    with CollectedColorPropEditor do
     InvalidateChildren(nil, False);
    ColorsCollectionView.Invalidate;
   end else
   begin
    FColor := TColor(RGB^);
    ColorPreview.Color := FColor;
    HSColorPickerEx.SelectedColor := FColor;
    LColorPickerEx.SelectedColor := FColor;
    with ColorPropEditor do
     InvalidateChildren(nil, False);
   end;
  end;
 end;
end;

function TPickColorDialog.GetCollectedColorPtr: PColor;
begin
 Result := Addr(FColorsCollection[ColorsCollectionView.SelectedCell.Y]);
end;

function TPickColorDialog.GetCollectionColor(I: Integer): TColor;
begin
 if (I >= 0) and (I <= 15) then
  Result := FColorsCollection[I] else
  Result := -1;
end;

procedure TPickColorDialog.SetCollectionColor(I: Integer; Value: TColor);
begin
 if (I >= 0) and (I <= 15) and
    (FColorsCollection[I] <> Value) then
 begin
  if (Value < 0) or (Value > $FFFFFF) then
   Value := -1 else
   Value := GetFormattedColor(Value);
  FColorsCollection[I] := Value;
  ColorsCollectionView.Invalidate;
  if I = ColorsCollectionView.SelectedCell.Y then
   RefreshProps(CollectedColorPropEditor);
 end;
end;

procedure TPickColorDialog.ColorPreviewDblClick(Sender: TObject);
begin
 AddToCollection(FColor);
end;

function TPickColorDialog.GetCollectedColor: TColor;
begin
 Result := FColorsCollection[ColorsCollectionView.SelectedCell.Y];
end;

procedure TPickColorDialog.SetCollectedColor(Value: TColor);
var
 I: Integer;
begin
 I := ColorsCollectionView.SelectedCell.Y;
 if Value < 0 then
 begin
  for I := I + 1 to 15 do
   FColorsCollection[I - 1] := FColorsCollection[I];
  FColorsCollection[15] := -1;
 end else
  FColorsCollection[I] := GetFormattedColor(Value);

 ColorsCollectionView.Invalidate;
 RefreshProps(CollectedColorPropEditor);
end;

procedure TPickColorDialog.ScreenPickerBtnMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Screen.Cursor := crDefault;
end;

procedure TPickColorDialog.ScreenPickerBtnMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  c: TCanvas;
begin
 if GetCapture <> 0 then
 begin
  c := TCanvas.Create;
  try
   c.Handle := GetWindowDC(GetDesktopWindow);
   with ScreenPickerBtn.ClientToScreen(Point(X, Y)) do
    Color := GetPixel(c.Handle, X, Y);
  finally
   c.Free;
  end;
 end;
end;

procedure TPickColorDialog.HSColorPickerMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 if FMode = 1 then
 begin
  LColorPickerEx.Hue := HSColorPickerEx.HueValue;
  LColorPickerEx.Saturation := HSColorPickerEx.SaturationValue;
  FColor := LColorPickerEx.SelectedColor;
  RefreshProps(ColorPropEditor);
  ColorPreview.Color := FColor;
 end;
end;

procedure TPickColorDialog.HSColorPickerMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 FMode := 1;
 HSColorPickerMouseMove(Sender, Shift, X, Y);
end;

procedure TPickColorDialog.LColorPickerMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
 if FMode = 2 then
 begin       
  FColor := LColorPickerEx.SelectedColor;
  RefreshProps(ColorPropEditor);
  ColorPreview.Color := FColor;
 end;
end;

procedure TPickColorDialog.PickerMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 FMode := 0;
end;

procedure TPickColorDialog.LColorPickerMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 FMode := 2;
 LColorPickerMouseMove(Sender, Shift, X, Y);
end;

procedure TPickColorDialog.PropEditorExit(Sender: TObject);
begin
 with Sender as TPropertyEditor do
 begin
  UserSelect := True;
  Finalize(LastRoute);
  FocusedNode := nil;
  CancelEditNode;
 end;
end;

procedure TPickColorDialog.PropEditorEnter(Sender: TObject);
begin
 with Sender as TPropertyEditor do
  UserSelect := False;
end;

procedure TPickColorDialog.ColorsCollectionViewSelectionChanged(
  Sender: TCustomTileMapView);
begin
 RefreshProps(CollectedColorPropEditor);
end;

procedure TPickColorDialog.ScreenPickerBtnMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 Screen.Cursor := FColorPickerCursor;
end;

procedure TPickColorDialog.ColorsCollectionViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 if ssCtrl in Shift then
 begin
  case Button of
   mbLeft: CollectedColor := -1;
   mbRight:
   begin
    with ColorsCollectionView.SelectedCell do
     if (Y = 0) or (FColorsCollection[Y - 1] > 0) then
      CollectedColor := FColor;
   end;
  end;
 end else
 if ssDouble in Shift then
  Color := CollectedColor;  
end;

procedure TPickColorDialog.ColorPreviewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 if (ssCtrl in Shift) and
   (CollectedColor >= 0) then
  CollectedColor := FColor;
end;

function TPickColorDialog.GetCollectionSize: Integer;
var
 I: Integer;
begin
 for I := 0 to 15 do
  if FColorsCollection[I] < 0 then
  begin
   Result := I;
   Exit;
  end;
 Result := 16;
end;

destructor TPickColorDialog.Destroy;
begin
 inherited;
 FColorFormat.Free; 
end;

function TPickColorDialog.GetFormattedColor(Value: TColor): TColor;
begin
 if FConvert then
 begin
  Value := RGBZ_ColorFormat.ConvertValue(FColorFormat, Value);
  Result := FColorFormat.ConvertValue(RGBZ_ColorFormat, Value);
 end else
  Result := Value;
end;

procedure TPickColorDialog.PropEditorUpButton(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Result: Boolean);
var
 NodeData: PPropertyData;
 Shift: Byte;
 X: Integer;
begin
 NodeData := Sender.GetNodeData(Node);
 if NodeData.Owner <> nil then
  with NodeData.Item do
  begin
   if Value = 255 then Exit;
   case Index of
    0:   Shift := FColorFormat.RedByteShift;
    1:   Shift := FColorFormat.GreenByteShift;
    else Shift := FColorFormat.BlueByteShift;
   end;
   X := Value shr Shift;
   Inc(X);
   if X shl Shift > 255 then
    X := 255 else
    X := ConvertColorValue(8 - Shift, X);
   Value := X;
   Result := True;
  end;
end;

procedure TPickColorDialog.PropEditorDownButton(
  Sender: TCustomPropertyEditor; Node: PVirtualNode; var Result: Boolean);
var
 NodeData: PPropertyData;
 Shift: Byte;
 X: Integer;
begin
 NodeData := Sender.GetNodeData(Node);
 if NodeData.Owner <> nil then
  with NodeData.Item do
  begin
   if Value = 0 then Exit;
   case Index of
    0:   Shift := FColorFormat.RedByteShift;
    1:   Shift := FColorFormat.GreenByteShift;
    else Shift := FColorFormat.BlueByteShift;
   end;
   X := Value shr Shift;
   Dec(X);
   if X < 0 then
    X := 0 else
    X := ConvertColorValue(8 - Shift, X);
   Value := X;
   Result := True;
  end;
end;

procedure TPickColorDialog.SetColorFormat(Value: TColorFormat);
begin
 FColorFormat.Assign(Value);
 FConvert := not FColorFormat.SameAs(RGBZ_ColorFormat);
 HSColorPickerEx.ColorFormat := Value;
 LColorPickerEx.ColorFormat := Value;
end;

procedure TPickColorDialog.AddToCollection(Color: TColor);
var
 I: Integer;
begin
 if Color >= 0 then
 begin
  for I := 0 to 15 do
   if FColorsCollection[I] < 0 then
   begin
    FColorsCollection[I] := Color;
    ColorsCollectionView.Selected[0, I] := True;
    Exit;
   end;

  CollectedColor := Color;
 end;
end;

end.
