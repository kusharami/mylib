unit PlugAdapters;

interface

uses
 SysUtils, Classes, TntClasses, MyClassesEx, PlugInterface;

type
 TStreamAdapter = class(TInterfacedObject, IStream32, IStream64)
  private
    FSource: TStream;
    FFreeAfterUse: Boolean;
    function GetPtr: Pointer; stdcall;
    function Read(var Buffer; Count: Integer): Integer; stdcall;
    function Write(const Buffer; Count: Integer): Integer; stdcall;
    function GetPos: Integer; stdcall;
    function GetPos64: Int64; stdcall;
    function IStream64.GetPos = GetPos64;
    function GetSize: Integer; stdcall;
    function GetSize64: Int64; stdcall;
    function IStream64.GetSize = GetSize64;
    procedure SetPos(Value: Integer); stdcall;
    procedure SetPos64(const Value: Int64); stdcall;
    procedure IStream64.SetPos = SetPos64;
    procedure SetSize(Value: Integer); stdcall;
    procedure SetSize64(const Value: Int64); stdcall;
    procedure IStream64.SetSize = SetSize64;
    procedure Seek(Offset, SeekOrigin: Integer); stdcall;
    procedure Seek64(const Offset: Int64; SeekOrigin: Integer); stdcall;
    procedure IStream64.Seek = Seek64;
    function CopyFrom(const Stream: IReadWrite; Count: Integer): Integer;
      stdcall;
    function CopyFrom64(const Stream: IReadWrite;
      const Count: Int64): Int64; stdcall;
    function IStream64.CopyFrom = CopyFrom64;
    procedure FreeObject; stdcall;
  public
    constructor Create(Source: TStream; FreeAfterUse: Boolean = False);
    destructor Destroy; override;
 end;

 TMemoryStreamAdapter = class(TStreamAdapter, IMemoryStream)
    function GetMemory: Pointer;
 end;

 TStringsAdapter = class(TInterfacedObject, IStringList, IWideStringList)
  private
    FTempA: AnsiString;
    FTempW: WideString;
    FSourceA: TStringList;
    FSourceW: TTntStringList;
    FFreeAfterUse: Boolean;
    function GetString(Index: Integer): PAnsiChar; stdcall;
    function GetStringW(Index: Integer): PWideChar; stdcall;
    function IWideStringList.GetString = GetStringW;
    procedure SetString(Index: Integer; Value: PAnsiChar); stdcall;
    procedure SetStringW(Index: Integer; Value: PWideChar); stdcall;
    procedure IWideStringList.SetString = SetStringW;
    function GetText: PAnsiChar; stdcall;
    function GetTextW: PWideChar; stdcall;
    function IWideStringList.GetText = GetTextW;
    procedure SetText(Value: PAnsiChar); stdcall;
    procedure SetTextW(Value: PWideChar); stdcall;
    procedure IWideStringList.SetText = SetTextW;    
    function Add(S: PAnsiChar): Integer; stdcall;
    function AddW(S: PWideChar): Integer; stdcall;
    function IWideStringList.Add = AddW;
    procedure Append(S: PAnsiChar); stdcall;
    procedure AppendW(S: PWideChar); stdcall;
    procedure IWideStringList.Append = AppendW;
    function Find(S: PAnsiChar; var Index: Integer): LongBool; stdcall;
    function FindW(S: PWideChar; var Index: Integer): LongBool; stdcall;
    function IWideStringList.Find = FindW;
    function IndexOf(S: PAnsiChar): Integer; stdcall;
    function IndexOfW(S: PWideChar): Integer; stdcall;
    function IWideStringList.IndexOf = IndexOfW;
    procedure Insert(Index: Integer; S: PAnsiChar); stdcall;
    procedure InsertW(Index: Integer; S: PWideChar); stdcall;
    procedure IWideStringList.Insert = InsertW;
    procedure Delete(Index: Integer); stdcall;
    procedure Exchange(Index1, Index2: Integer); stdcall;
    procedure Move(CurIndex, NewIndex: Integer); stdcall;
    procedure Sort; stdcall;
    procedure AddStrings(const Strings: IStrings); stdcall;
    procedure Assign(const Source: IStrings); stdcall;
    function Equals(const Strings: IStrings): LongBool; stdcall;
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    function GetCount: Integer; stdcall;
    procedure Clear; stdcall;
    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create; overload;
    constructor Create(Source: TTntStringList; FreeAfterUse: Boolean = False); overload;
    constructor Create(Source: TStringList; FreeAfterUse: Boolean = False); overload;
    destructor Destroy; override;
 end;

 TIniFileAdapter = class(TInterfacedObject, IIniFile)
   private
    FTemp: WideString;
    FSource: TStreamIniFileW;
    FFreeAfterUse: Boolean;
    FUpdateBeforeFree: Boolean;
    function GetFileName: PWideChar; stdcall;
   public
    constructor Create(Source: TStreamIniFileW; UpdateBeforeFree, FreeAfterUse: Boolean);
    destructor Destroy; override;   
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure LoadFromFile(FileName: PWideChar); stdcall;
    procedure SaveToFile(FileName: PWideChar); stdcall;
    procedure SetStrings(const List: IWideStringList); stdcall;
    procedure WriteString(Section, Ident, Value: PWideChar); stdcall;
    function ReadString(Section, Ident, Default: PWideChar): PWideChar; stdcall;
    function ReadInteger(Section, Ident: PWideChar; Default: Longint): Longint; stdcall;
    procedure WriteInteger(Section, Ident: PWideChar; Value: Longint; HexDigits: Integer); stdcall;
    procedure WriteInt64(Section, Ident: PWideChar; const Value: Int64; HexDigits: Integer); stdcall;
    function ReadBool(Section, Ident: PWideChar; Default: Boolean): Boolean; stdcall;
    procedure WriteBool(Section, Ident: PWideChar; Value: Boolean); stdcall;
    function ReadBinaryStream(Section, Name: PWideChar; const Value: IReadWrite): Integer; stdcall;
    function ReadDate(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadDateTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    function ReadFloat(Section, Name: PWideChar; const Default: Double): Double; stdcall;
    function ReadTime(Section, Name: PWideChar; const Default: TDateTime): TDateTime; stdcall;
    procedure WriteBinaryStream(Section, Name: PWideChar; const Value: IReadWrite); stdcall;
    procedure WriteDate(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteDateTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure WriteFloat(Section, Name: PWideChar; const Value: Double); stdcall;
    procedure WriteTime(Section, Name: PWideChar; const Value: TDateTime); stdcall;
    procedure ReadSection(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure ReadSections(const Strings: IWideStringList); stdcall;
    procedure ReadSectionValues(Section: PWideChar; const Strings: IWideStringList); stdcall;
    procedure EraseSection(Section: PWideChar); stdcall;
    procedure DeleteKey(Section, Ident: PWideChar); stdcall;
    function ValueExists(Section, Ident: PWideChar): Boolean; stdcall;
    function SectionExists(Section: PWideChar): Boolean; stdcall;
    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;    
 end;

implementation

{ TStreamAdapter }

function TStreamAdapter.CopyFrom(const Stream: IReadWrite;
  Count: Integer): Integer;
begin
 if Stream = NIL then
  Result := 0 else
  Result := FSource.CopyFrom(TStream(Stream.GetPtr), Count);
end;

function TStreamAdapter.CopyFrom64(const Stream: IReadWrite;
  const Count: Int64): Int64;
begin
 if Stream = NIL then
  Result := 0 else
  Result := FSource.CopyFrom(TStream(Stream.GetPtr), Count);
end;

constructor TStreamAdapter.Create(Source: TStream; FreeAfterUse: Boolean = False);
begin
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

destructor TStreamAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

procedure TStreamAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TStreamAdapter.GetPos: Integer;
begin
 Result := FSource.Position;
end;

function TStreamAdapter.GetPos64: Int64;
begin
 Result := FSource.Position;
end;

function TStreamAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TStreamAdapter.GetSize: Integer;
begin
 Result := FSource.Size;
end;

function TStreamAdapter.GetSize64: Int64;
begin
 Result := FSource.Size;
end;

function TStreamAdapter.Read(var Buffer; Count: Integer): Integer;
begin
 Result := FSource.Read(Buffer, Count);
end;

procedure TStreamAdapter.Seek(Offset, SeekOrigin: Integer);
begin
 FSource.Seek(Offset, TSeekOrigin(SeekOrigin));
end;

procedure TStreamAdapter.Seek64(const Offset: Int64; SeekOrigin: Integer);
begin
 FSource.Seek(Offset, TSeekOrigin(SeekOrigin));
end;

procedure TStreamAdapter.SetPos(Value: Integer);
begin
 FSource.Position := Value;
end;

procedure TStreamAdapter.SetPos64(const Value: Int64);
begin
 FSource.Position := Value;
end;

procedure TStreamAdapter.SetSize(Value: Integer);
begin
 FSource.Size := Value;
end;

procedure TStreamAdapter.SetSize64(const Value: Int64);
begin
 FSource.Size := Value;
end;

function TStreamAdapter.Write(const Buffer; Count: Integer): Integer;
begin
 Result := FSource.Write(Buffer, Count);
end;

{ TMemoryStreamAdapter }

function TMemoryStreamAdapter.GetMemory: Pointer;
begin
 Result := (FSource as TMemoryStream).Memory;
end;

{ TStringsAdapter }

function TStringsAdapter.Add(S: PAnsiChar): Integer;
begin
 if FSourceA <> NIL then
  Result := FSourceA.Add(S) else
  Result := FSourceW.Add(S);
end;

procedure TStringsAdapter.AddStrings(const Strings: IStrings);
var Obj: TObject; I: Integer;
begin
 Obj := TObject(Strings.GetPtr);
 if Obj is TStringList then
 begin
  if FSourceA <> NIL then
   FSourceA.AddStrings(TStringList(Obj)) else
   FSourceW.AddStrings(TStringList(Obj));
 end else if Obj is TTntStringList then
 begin
  if FSourceW <> NIL then FSourceW.AddStrings(TTntStringList(Obj)) else
  with TTntStringList(Obj) do
   for I := 0 to Count - 1 do FSourceA.Add(AnsiStrings[I]);
 end;
end;

function TStringsAdapter.AddW(S: PWideChar): Integer;
begin
 if FSourceW <> NIL then
  Result := FSourceW.Add(S) else
  Result := FSourceA.Add(S);
end;

procedure TStringsAdapter.Append(S: PAnsiChar);
begin
 if FSourceA <> NIL then
  FSourceA.Append(S) else
  FSourceW.Append(S);
end;

procedure TStringsAdapter.AppendW(S: PWideChar);
begin
 if FSourceW <> NIL then
  FSourceW.Append(S) else
  FSourceA.Append(S);
end;

procedure TStringsAdapter.Assign(const Source: IStrings);
var Obj: TObject; I: Integer; S: AnsiString;
begin
 Obj := TObject(Source.GetPtr);
 if Obj is TStringList then
 begin
  if FSourceA <> NIL then
   FSourceA.Assign(TStringList(Obj)) else
   FSourceW.Assign(TStringList(Obj));
 end else if Obj is TTntStringList then
 begin
  if FSourceW <> NIL then FSourceW.Assign(TTntStringList(Obj)) else
  with TTntStringList(Obj) do
  begin
   FSourceA.BeginUpdate;
   try
    FSourceA.Clear;
    S := NameValueSeparator;
    FSourceA.NameValueSeparator := S[1];
    S := QuoteChar;
    FSourceA.QuoteChar := S[1];
    S := Delimiter;
    FSourceA.Delimiter := S[1];
    for I := 0 to Count - 1 do FSourceA.Add(AnsiStrings[I]);
   finally
    FSourceA.EndUpdate;
   end;
  end;
 end;
end;

constructor TStringsAdapter.Create;
begin
 raise EStringListError.Create('This constructor is not allowed');
end;

procedure TStringsAdapter.Clear;
begin
 if FSourceA <> NIL then
  FSourceA.Clear else
  FSourceW.Clear;
end;

constructor TStringsAdapter.Create(Source: TStringList; FreeAfterUse: Boolean = False);
begin
 if Source = NIL then raise EStringListError.Create('Source must not be nil');
 FSourceA := Source;
 FSourceW := NIL;
 FFreeAfterUse := FreeAfterUse;
end;

constructor TStringsAdapter.Create(Source: TTntStringList; FreeAfterUse: Boolean = False);
begin
 if Source = NIL then raise EStringListError.Create('Source must not be nil');
 FSourceA := NIL;
 FSourceW := Source;
 FFreeAfterUse := FreeAfterUse;
end;

procedure TStringsAdapter.Delete(Index: Integer);
begin
 If FSourceA <> NIL then
  FSourceA.Delete(Index) else
  FSourceW.Delete(Index);
end;

function TStringsAdapter.Equals(const Strings: IStrings): LongBool;
var Obj: TObject;
begin
 Result := False;
 Obj := TObject(Strings.GetPtr);
 if Obj is TStringList then
 begin
  if FSourceA <> NIL then
   Result := FSourceA.Equals(TStringList(Obj));
 end else if Obj is TTntStringList then
 begin
  if FSourceW <> NIL then Result := FSourceW.Equals(TTntStringList(Obj));
 end;
end;

procedure TStringsAdapter.Exchange(Index1, Index2: Integer);
begin
 if FSourceA <> NIL then
  FSourceA.Exchange(Index1, Index2) else
  FSourceW.Exchange(Index1, Index2);
end;

function TStringsAdapter.Find(S: PAnsiChar; var Index: Integer): LongBool;
begin
 if FSourceA <> NIL then
  Result := FSourceA.Find(S, Index) else
  Result := FSourceW.Find(S, Index);
end;

function TStringsAdapter.FindW(S: PWideChar; var Index: Integer): LongBool;
begin
 if FSourceW <> NIL then
  Result := FSourceW.Find(S, Index) else
  Result := FSourceA.Find(S, Index);
end;

function TStringsAdapter.GetCount: Integer;
begin
 if FSourceA <> NIL then
  Result := FSourceA.Count else
  Result := FSourceW.Count;
end;

function TStringsAdapter.GetString(Index: Integer): PAnsiChar;
begin
 if FSourceA <> NIL then
  FTempA := FSourceA.Strings[Index] else
  FTempA := FSourceW.AnsiStrings[Index];
 Result := Pointer(FTempA);
end;

function TStringsAdapter.GetStringW(Index: Integer): PWideChar;
begin
 if FSourceW <> NIL then
  FTempW := FSourceW.Strings[Index] else
  FTempW := FSourceA.Strings[Index];
 Result := Pointer(FTempW);
end;

function TStringsAdapter.GetText: PAnsiChar;
begin
 if FSourceA <> NIL then
  FTempA := FSourceA.Text else
  FTempA := FSourceW.Text;
 Result := Pointer(FTempA);
end;

function TStringsAdapter.GetTextW: PWideChar;
begin
 if FSourceW <> NIL then
  FTempW := FSourceW.Text else
  FTempW := FSourceA.Text;
 Result := Pointer(FTempW);
end;

function TStringsAdapter.IndexOf(S: PAnsiChar): Integer;
begin
 if FSourceA <> NIL then
  Result := FSourceA.IndexOf(S) else
  Result := FSourceW.IndexOf(S);
end;

function TStringsAdapter.IndexOfW(S: PWideChar): Integer;
begin
 if FSourceW <> NIL then
  Result := FSourceW.IndexOf(S) else
  Result := FSourceA.IndexOf(S);
end;

procedure TStringsAdapter.Insert(Index: Integer; S: PAnsiChar);
begin
 if FSourceA <> NIL then
  FSourceA.Insert(Index, S) else
  FSourceW.Insert(Index, S);
end;

procedure TStringsAdapter.InsertW(Index: Integer; S: PWideChar);
begin
 if FSourceW <> NIL then
  FSourceW.Insert(Index, S) else
  FSourceA.Insert(Index, S);
end;

procedure TStringsAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 if FSourceA <> NIL then
  FSourceA.LoadFromStream(TStream(Stream.GetPtr)) else
  FSourceW.LoadFromStream(TStream(Stream.GetPtr));
end;

procedure TStringsAdapter.Move(CurIndex, NewIndex: Integer);
begin
 if FSourceA <> NIL then
  FSourceA.Move(CurIndex, NewIndex) else
  FSourceW.Move(CurIndex, NewIndex);
end;

procedure TStringsAdapter.SaveToStream(const Stream: IReadWrite);
begin
 if FSourceA <> NIL then
  FSourceA.SaveToStream(TStream(Stream.GetPtr)) else
  FSourceW.SaveToStream(TStream(Stream.GetPtr));
end;

procedure TStringsAdapter.SetString(Index: Integer; Value: PAnsiChar);
begin
 if FSourceA <> NIL then
  FSourceA.Strings[Index] := Value else
  FSourceW.Strings[Index] := Value;
end;

procedure TStringsAdapter.SetStringW(Index: Integer; Value: PWideChar);
begin
 if FSourceW <> NIL then
  FSourceW.Strings[Index] := Value else
  FSourceA.Strings[Index] := Value;
end;

procedure TStringsAdapter.SetText(Value: PAnsiChar);
begin
 if FSourceA <> NIL then
  FSourceA.Text := Value else
  FSourceW.Text := Value;
end;

procedure TStringsAdapter.SetTextW(Value: PWideChar);
begin
 if FSourceW <> NIL then
  FSourceW.Text := Value else
  FSourceA.Text := Value;
end;

procedure TStringsAdapter.Sort;
begin
 if FSourceA <> NIL then
  FSourceA.Sort else
  FSourceW.Sort;
end;

function TStringsAdapter.GetPtr: Pointer;
begin
 if FSourceA <> NIL then
  Result := Pointer(FSourceA) else
  Result := Pointer(FSourceW);
end;

destructor TStringsAdapter.Destroy;
begin
 if FFreeAfterUse then
 begin
  FSourceA.Free;
  FSourceW.Free;
 end;
 inherited;
end;

procedure TStringsAdapter.FreeObject;
begin
 FreeAndNIL(FSourceA);
 FreeAndNIL(FSourceW);
end;

{ TIniFileAdapter }

constructor TIniFileAdapter.Create(Source: TStreamIniFileW;
          UpdateBeforeFree, FreeAfterUse: Boolean);
begin
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
 FUpdateBeforeFree := UpdateBeforeFree;
end;

procedure TIniFileAdapter.DeleteKey(Section, Ident: PWideChar);
begin
 FSource.DeleteKey(Section, Ident);
end;

destructor TIniFileAdapter.Destroy;
begin
 if FFreeAfterUse then
 try
  FreeObject;
 except
 end;
 inherited;
end;

procedure TIniFileAdapter.EraseSection(Section: PWideChar);
begin
 FSource.EraseSection(Section);
end;

procedure TIniFileAdapter.FreeObject;
begin
 try
  if FUpdateBeforeFree then FSource.UpdateFile;
 finally
  FreeAndNIL(FSource);
 end;
end;

function TIniFileAdapter.GetFileName: PWideChar;
begin
 Result := Pointer(FSource.FileName);
end;

function TIniFileAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

procedure TIniFileAdapter.LoadFromFile(FileName: PWideChar);
begin
 FSource.LoadFromFile(WideString(FileName));
end;

procedure TIniFileAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 FSource.LoadFromStream(TStream(Stream.GetPtr));
end;

function TIniFileAdapter.ReadBinaryStream(Section, Name: PWideChar;
  const Value: IReadWrite): Integer;
begin
 Result := FSource.ReadBinaryStream(Section, Name, TStream(Value.GetPtr));
end;

function TIniFileAdapter.ReadBool(Section, Ident: PWideChar;
  Default: Boolean): Boolean;
begin
 Result := FSource.ReadBool(Section, Ident, Default);
end;

function TIniFileAdapter.ReadDate(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := FSource.ReadDate(Section, Name, Default);
end;

function TIniFileAdapter.ReadDateTime(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := FSource.ReadDateTime(Section, Name, Default);
end;

function TIniFileAdapter.ReadFloat(Section, Name: PWideChar;
  const Default: Double): Double;
begin
 Result := FSource.ReadFloat(Section, Name, Default);
end;

function TIniFileAdapter.ReadInteger(Section, Ident: PWideChar;
  Default: Integer): Longint;
begin
 Result := FSource.ReadInteger(Section, Ident, Default);
end;

procedure TIniFileAdapter.ReadSection(Section: PWideChar;
  const Strings: IWideStringList);
begin
 FSource.ReadSection(Section, TTntStringList(Strings.GetPtr));
end;

procedure TIniFileAdapter.ReadSections(const Strings: IWideStringList);
begin
 FSource.ReadSections(TTntStringList(Strings.GetPtr));
end;

procedure TIniFileAdapter.ReadSectionValues(Section: PWideChar;
  const Strings: IWideStringList);
begin
 FSource.ReadSectionValues(Section, TTntStringList(Strings.GetPtr));
end;

function TIniFileAdapter.ReadString(Section, Ident,
  Default: PWideChar): PWideChar;
begin
 FTemp := FSource.ReadString(Section, Ident, Default);
 Result := Pointer(FTemp);
end;

function TIniFileAdapter.ReadTime(Section, Name: PWideChar;
  const Default: TDateTime): TDateTime;
begin
 Result := FSource.ReadTime(Section, Name, Default);
end;

procedure TIniFileAdapter.SaveToFile(FileName: PWideChar);
begin
 FSource.SaveToFile(WideString(FileName));
end;

procedure TIniFileAdapter.SaveToStream(const Stream: IReadWrite);
begin
 FSource.SaveToStream(TStream(Stream.GetPtr));
end;

function TIniFileAdapter.SectionExists(Section: PWideChar): Boolean;
begin
 Result := FSource.SectionExists(Section);
end;

procedure TIniFileAdapter.SetStrings(const List: IWideStringList);
begin
 FSource.SetStrings(TTntStringList(List.GetPtr));
end;

function TIniFileAdapter.ValueExists(Section, Ident: PWideChar): Boolean;
begin
 Result := FSource.ValueExists(Section, Ident);
end;

procedure TIniFileAdapter.WriteBinaryStream(Section, Name: PWideChar;
  const Value: IReadWrite);
begin
 FSource.WriteBinaryStream(Section, Name, TStream(Value.GetPtr));
end;

procedure TIniFileAdapter.WriteBool(Section, Ident: PWideChar;
  Value: Boolean);
begin
 FSource.WriteBool(Section, Ident, Value);
end;

procedure TIniFileAdapter.WriteDate(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 FSource.WriteDate(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteDateTime(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 FSource.WriteDateTime(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteFloat(Section, Name: PWideChar;
  const Value: Double);
begin
 FSource.WriteFloat(Section, Name, Value);
end;

procedure TIniFileAdapter.WriteInt64(Section, Ident: PWideChar;
  const Value: Int64; HexDigits: Integer);
begin
 FSource.WriteInt64(Section, Ident, Value, HexDigits);
end;

procedure TIniFileAdapter.WriteInteger(Section, Ident: PWideChar;
  Value, HexDigits: Integer);
begin
 FSource.WriteInteger(Section, Ident, Value, HexDigits);
end;

procedure TIniFileAdapter.WriteString(Section, Ident, Value: PWideChar);
begin
 FSource.WriteString(Section, Ident, Value);
end;

procedure TIniFileAdapter.WriteTime(Section, Name: PWideChar;
  const Value: TDateTime);
begin
 FSource.WriteTime(Section, Name, Value);
end;

end.
