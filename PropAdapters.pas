unit PropAdapters;

interface

uses
 SysUtils, Classes, Dialogs, NodeLst, PropContainer, PlugAdapters, PlugInterface;

type 
 TPropertyAdapter = class(TInterfacedObject, IPropertyListItem)
  private
    FSource: TPropertyListItem;
    FFreeAfterUse: Boolean;
    function NameGet: PWideChar; stdcall;
    procedure NameSet(Value: PWideChar); stdcall;
    function ValueTypeGet: TValueType; stdcall;
    procedure ValueTypeSet(Value: TValueType); stdcall;
    function ValueStrGet: PWideChar; stdcall;
    procedure ValueStrSet(Value: PWideChar); stdcall;
    function DefValStrGet: PWideChar; stdcall;
    procedure DefValStrSet(Value: PWideChar); stdcall;
    function ValueGet: Int64; stdcall;
    procedure ValueSet(const Value: Int64); stdcall;
    function MinGet: Int64; stdcall;
    procedure MinSet(const Value: Int64); stdcall;
    function MaxGet: Int64; stdcall;
    procedure MaxSet(const Value: Int64); stdcall;
    function HexDigitsGet: Integer; stdcall;
    procedure HexDigitsSet(Value: Integer); stdcall;
    function ReadOnlyGet: LongBool; stdcall;
    procedure ReadOnlySet(Value: LongBool); stdcall;
    function ParamsGet: Integer; stdcall;
    procedure ParamsSet(Value: Integer); stdcall;
    function TagGet: Integer; stdcall;
    procedure TagSet(Value: Integer); stdcall;
    function DataGet: Pointer; stdcall;
    function DataSizeGet: Integer; stdcall;
    procedure DataSizeSet(Value: Integer); stdcall;
    function GetPickListIndex: Integer; stdcall;
    procedure SetPickListIndex(Value: Integer); stdcall;
    function GetDataStream: IStream32; stdcall;
    function SubPropsGet: IPropertyList; stdcall;
    function GetPtr: Pointer; stdcall;
    function GetPickList: IWideStringList; stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create(ASource: TPropertyListItem; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

 TPropertyListAdapter = class(TInterfacedObject, IPropertyList)
  private
    FSource: TPropertyList;
    FFreeAfterUse: Boolean;
    function PropertyGet(Index: Integer): IPropertyListItem; stdcall;
    function AddProperty: IPropertyListItem; stdcall;
    procedure RemoveProperty(const Prop: IPropertyListItem); stdcall;
    function BooleanAdd(Name: PWideChar;
      Value: LongBool): IPropertyListItem; stdcall;
    function DecimalAdd(Name: PWideChar; const Value, Min,
      Max: Int64): IPropertyListItem; stdcall;
    function HexadecimalAdd(Name: PWideChar; const Value, Min, Max: Int64;
      Digits: Integer): IPropertyListItem; stdcall;
    function StringAdd(Name, Value: PWideChar;
      ReadOnly: LongBool): IPropertyListItem; stdcall;
    function PickListAdd(Name, Value, List: PWideChar; 
      Fixed: LongBool = True): IPropertyListItem; stdcall;
    function FilePickAdd(Name, FileName, Filter, DefaultExt,
      InitialDir: PWideChar; Save: LongBool = False; Options: Integer =
      FO_HideReadOnly or FO_EnableSizing): IPropertyListItem; stdcall;
    function DataAdd(Name: PWideChar): IPropertyListItem; stdcall;
    function GetPtr: Pointer; stdcall;
    function GetCount: Integer; stdcall;
    procedure Clear; stdcall;
    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;
    procedure FreeObject; stdcall;
  public
    constructor Create(Source: TPropertyList; FreeAfterUse: Boolean);
    destructor Destroy; override;
 end;

implementation

constructor TPropertyAdapter.Create(ASource: TPropertyListItem; FreeAfterUse: Boolean);
begin
 if ASource = NIL then raise EPropertyListItemError.Create('Source must not be nil');
 FSource := ASource;
 FFreeAfterUse := FreeAfterUse;
end;

function TPropertyAdapter.DataGet: Pointer;
begin
 Result := FSource.DataStream.Memory;
end;

function TPropertyAdapter.DataSizeGet: Integer;
begin
 Result := FSource.DataStream.Size;
end;

procedure TPropertyAdapter.DataSizeSet(Value: Integer);
begin
 FSource.DataStream.Size := Value;
end;

function TPropertyAdapter.DefValStrGet: PWideChar;
begin
 Result := Pointer(FSource.DefaultStr);
end;

procedure TPropertyAdapter.DefValStrSet(Value: PWideChar);
begin
 FSource.DefaultStr := Value;
end;

destructor TPropertyAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

procedure TPropertyAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TPropertyAdapter.GetDataStream: IStream32;
begin
 Result := TStreamAdapter.Create(FSource.DataStream);
end;

function TPropertyAdapter.GetPickList: IWideStringList;
begin
 Result := TStringsAdapter.Create(FSource.PickList);
end;

function TPropertyAdapter.GetPickListIndex: Integer;
begin
 Result := FSource.PickListIndex;
end;

function TPropertyAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TPropertyAdapter.HexDigitsGet: Integer;
begin
 Result := FSource.HexDigits;
end;

procedure TPropertyAdapter.HexDigitsSet(Value: Integer);
begin
 FSource.HexDigits := Value;
end;

function TPropertyAdapter.MaxGet: Int64;
begin
 Result := FSource.ValueMax;
end;

procedure TPropertyAdapter.MaxSet(const Value: Int64);
begin
 FSource.ValueMax := Value;
end;

function TPropertyAdapter.MinGet: Int64;
begin
 Result := FSource.ValueMin;
end;

procedure TPropertyAdapter.MinSet(const Value: Int64);
begin
 FSource.ValueMin := Value;
end;

function TPropertyAdapter.NameGet: PWideChar;
begin
 Result := Pointer(FSource.Name);
end;

procedure TPropertyAdapter.NameSet(Value: PWideChar);
begin
 FSource.Name := Value;
end;

function TPropertyAdapter.ParamsGet: Integer;
begin
 Result := FSource.Parameters;
end;

procedure TPropertyAdapter.ParamsSet(Value: Integer);
begin
 FSource.Parameters := Value;
end;

function TPropertyAdapter.ReadOnlyGet: LongBool;
begin
 Result := FSource.ReadOnly;
end;

procedure TPropertyAdapter.ReadOnlySet(Value: LongBool);
begin
 FSource.ReadOnly := Value;
end;

procedure TPropertyAdapter.SetPickListIndex(Value: Integer);
begin
 FSource.PickListIndex := Value;
end;

function TPropertyAdapter.SubPropsGet: IPropertyList;
begin
 Result := TPropertyListAdapter.Create(FSource.SubProperties, False);
end;

function TPropertyAdapter.TagGet: Integer;
begin
 Result := FSource.Tag;
end;

procedure TPropertyAdapter.TagSet(Value: Integer);
begin
 FSource.Tag := Value;
end;

function TPropertyAdapter.ValueGet: Int64;
begin
 Result := FSource.Value;
end;

procedure TPropertyAdapter.ValueSet(const Value: Int64);
begin
 FSource.Value := Value;
end;

function TPropertyAdapter.ValueStrGet: PWideChar;
begin
 Result := Pointer(FSource.ValueStr);
end;

procedure TPropertyAdapter.ValueStrSet(Value: PWideChar);
begin
 FSource.ValueStr := Value;
end;

function TPropertyAdapter.ValueTypeGet: TValueType;
begin
 Result := TValueType(FSource.ValueType);
end;

procedure TPropertyAdapter.ValueTypeSet(Value: TValueType);
begin
 FSource.ValueType := PropContainer.TValueType(Value);
end;


function TPropertyListAdapter.AddProperty: IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddProperty, False);
end;

function TPropertyListAdapter.BooleanAdd(Name: PWideChar;
  Value: LongBool): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddBoolean(Name, Value), False);
end;

procedure TPropertyListAdapter.Clear;
begin
 FSource.Clear;
end;

constructor TPropertyListAdapter.Create(Source: TPropertyList; FreeAfterUse: Boolean);
begin
 if Source = NIL then raise EPropertyListError.Create('Source must not be nil');
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

function TPropertyListAdapter.DataAdd(Name: PWideChar): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddData(Name), False);
end;

function TPropertyListAdapter.DecimalAdd(Name: PWideChar; const Value, Min,
  Max: Int64): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddDecimal(Name, Value, Min, Max), False);
end;

destructor TPropertyListAdapter.Destroy;
begin
 if FFreeAfterUse then FSource.Free;
 inherited;
end;

function TPropertyListAdapter.FilePickAdd(Name, FileName, Filter,
  DefaultExt, InitialDir: PWideChar; Save: LongBool;
  Options: Integer): IPropertyListItem;
var Temp: TOpenOptions; I: TOpenOption;
begin
 Temp := [];
 for I := Low(TOpenOption) to High(TOpenOption) do
 begin
  if Options and 1 <> 0 then Include(Temp, I);
  Options := Options shr 1;
 end;
 Result := TPropertyAdapter.Create(FSource.AddFilePick(Name, FileName, Filter,
                                   DefaultExt, InitialDir, Save, Temp), False);
end;

procedure TPropertyListAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TPropertyListAdapter.GetCount: Integer;
begin
 Result := FSource.Count;
end;

function TPropertyListAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TPropertyListAdapter.HexadecimalAdd(Name: PWideChar; const Value,
  Min, Max: Int64; Digits: Integer): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddHexadecimal(Name, Value,
                                   Min, Max, Digits), False);
end;

procedure TPropertyListAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 FSource.LoadFromStream(TStream(Stream.GetPtr));
end;

function TPropertyListAdapter.PickListAdd(Name, Value,
  List: PWideChar; Fixed: LongBool): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddPickList(Name, Value, List, Fixed), False);
end;

function TPropertyListAdapter.PropertyGet(
  Index: Integer): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.Properties[Index], False);
end;

procedure TPropertyListAdapter.RemoveProperty(
  const Prop: IPropertyListItem);
begin
 FSource.Remove(TNode(Prop.GetPtr));
end;

procedure TPropertyListAdapter.SaveToStream(const Stream: IReadWrite);
begin
 FSource.SaveToStream(TStream(Stream.GetPtr));
end;

function TPropertyListAdapter.StringAdd(Name, Value: PWideChar;
  ReadOnly: LongBool): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(FSource.AddString(Name, Value, not ReadOnly), False);
end;

end.
 