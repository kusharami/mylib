unit PropContainer;

interface

uses
 SysUtils, Classes, Dialogs, TntClasses, TntDialogs, NodeLst, HexUnit,
 MyClasses, MyClassesEx;

const
 PL_FIXEDPICK = 1;
 PL_BUFFER = 1;
 PL_MULTIPLE_VALUE = 2;
 PL_MULTIPLE_NAME = 4;
 PI_E_OK = 0;
 PI_E_STRTOINT = 1;
 PI_E_OUTOFBOUNDS = 2;
 PI_E_STRTOFLOAT = 3;
 BoolStrs: Array[Boolean] of String = ('False', 'True');

type
 TPropertyListItem = class;
 TPropertyList = class;

 TValueChangeEvent = procedure(Sender: TPropertyListItem) of Object;
 TPropertyListClass = class of TPropertyList;
 
 TValueType =  (vtDecimal, vtHexadecimal, vtString, vtPickString, vtFloat);
// TValueChangeProc = procedure(Sender: Pointer); stdcall;

 TPropertyListItem = class(TNode)
  private
    FItemOwner: TPropertyListItem;
    FSubProperties: TPropertyList;
    FName: WideString;
    FMaxLength: Integer;
    FValueStr: WideString;
    FValue: Int64;
    FStep: Int64;
    FMin, FMax: Int64;
    FParameters: Integer;
    FPickListIndex: Integer;
    FPickList: TTntStringList;
    FConversionResult: Integer;
    FDefaultStr: WideString;
    FData: TMemStream;
    FFloat: Double;
    FFloatStep: Double;
    FValueType: TValueType;
    FPrecision: Byte;
    FHexDigits: Byte;
    FReadOnly: Boolean;
    FChanging: Boolean;
    FListExternal: Boolean;
    FOnValueChange: TValueChangeEvent;
//    FValueChangeProc: TValueChangeProc;
    procedure SetValue(Value: Int64);
    procedure SetValueStr(Value: WideString);
    procedure SetHexDigits(Value: Byte);
    procedure SetDataSize(Size: Integer);
    function GetDataSize: Integer;
    function GetModified: Boolean;
    function GetData: Pointer;
    procedure SetFloat(const Value: Double);
    procedure SetPrecision(Value: Byte);
    procedure SetListExternal(Value: Boolean);
    procedure SetPickList(Value: TTntStringList);
    procedure SetPickListIndex(Value: Integer);
{    function GetHexValueMax: Int64;
    function GetHexValueMin: Int64;}
//    procedure SetValueChangeProc(Value: TValueChangeProc);
  protected
    procedure JustAdded; override;
    procedure Initialize; override;
    procedure LoadFromStream(const Stream: TStream); virtual;
    procedure SaveToStream(const Stream: TStream); virtual;
//    procedure ItemValueChange(Sender: TPropertyListItem);    
  public
    property ListExternal: Boolean read FListExternal write SetListExternal;
    property ItemOwner: TPropertyListItem read FItemOwner;
    property Name: WideString read FName write FName;
    property ValueType: TValueType read FValueType write FValueType;
    property MaxLength: Integer read FMaxLength write FMaxLength;
    property ValueStr: WideString read FValueStr write SetValueStr;
    property DefaultStr: WideString read FDefaultStr write FDefaultStr;
    property Modified: Boolean read GetModified;
    property Value: Int64 read FValue write SetValue;
    property Step: Int64 read FStep write FStep;
    property ValueMin: Int64 read FMin write FMin;
    property ValueMax: Int64 read FMax write FMax;
{    property HexValueMin: Int64 read GetHexValueMin;
    property HexValueMax: Int64 read GetHexValueMax;}
    property HexDigits: Byte read FHexDigits write SetHexDigits;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property PickListIndex: Integer read FPickListIndex write SetPickListIndex;
    property PickList: TTntStringList read FPickList write SetPickList;
    property SubProperties: TPropertyList read FSubProperties;
    property Parameters: Integer read FParameters write FParameters;
    property ConversionResult: Integer read FConversionResult;
    property Changing: Boolean read FChanging write FChanging;
    property DataStream: TMemStream read FData;
    property Data: Pointer read GetData;
    property DataSize: Integer read GetDataSize write SetDataSize;
    property Float: Double read FFloat write SetFloat;
    property FloatStep: Double read FFloatStep write FFloatStep;
    property Precision: Byte read FPrecision write SetPrecision;
    property OnValueChange: TValueChangeEvent read FOnValueChange write FOnValueChange;
{    property ValueChangeProc: TValueChangeProc
             read FValueChangeProc
            write SetValueChangeProc;}

    procedure ValueChange; virtual;
    procedure InitBoolean(const AName: WideString; AValue: Boolean);
    procedure InitFloat(const AName: WideString; const AValue: Double;
             APrecision: Byte);
    procedure InitDecimal(const AName: WideString; AValue, AMin, AMax: Int64);
    procedure InitHexadecimal(const AName: WideString; AValue, AMin, AMax: Int64;
             ADigits: Byte);
    procedure InitString(const AName, AValue: WideString;
             Editable: Boolean = True);

    procedure InitPickList(const AName: WideString; List: TTntStringList;
                           AExternal: Boolean; Index: Integer; Fixed: Boolean = True); overload;
    procedure InitPickList(const AName: WideString; const List: array
      of WideString; Index: Integer); overload;
    procedure InitPickList(const AName, AValue: WideString; const List: array
      of WideString; Fixed: Boolean = True); overload;
    procedure InitPickList(const AName, AValue, List: WideString;
      Fixed: Boolean = True); overload;
    procedure InitPickList(const AName, List: WideString; Index: Integer;
      Fixed: Boolean = True); overload;
    procedure InitData(const AName: WideString; const Description: WideString = '');    
    procedure Assign(Source: TNode); override;
    destructor Destroy; override;
 end;

 TFilePickItem = Class(TPropertyListItem)
  private
    FFilter, FInitialDir, FDefaultExt: WideString;
    FOptions: TOpenOptions;
    FSave: Boolean;
  protected
    procedure LoadFromStream(const Stream: TStream); override;
    procedure SaveToStream(const Stream: TStream); override;
  public
    procedure Init(const AName, AFileName, AFilter, ADefaultExt,
      AInitialDir: WideString; ASave: Boolean = False; AOptions: TOpenOptions =
      [ofHideReadOnly, ofEnableSizing]);
    property Filter: WideString read FFilter write FFilter;
    property InitialDir: WideString read FInitialDir write FInitialDir;
    property DefaultExt: WideString read FDefaultExt write FDefaultExt;
    property Options: TOpenOptions read FOptions write FOptions;
    property Save: Boolean read FSave write FSave;

    destructor Destroy; override;
    procedure FillOpenDialog(OpenDialog: TTntOpenDialog);
    procedure FillSaveDialog(SaveDialog: TTntSaveDialog);
 end;

 TPropertyListItemClass = class of TPropertyListItem;

 TPropertyList = class(TNodeListEx)
  private
    FItemOwner: TPropertyListItem;
    FOnValueChange: TValueChangeEvent;
//    FValueChangeProc: TValueChangeProc;
    function GetProp(Index: Integer): TPropertyListItem;
    function GetLevel: Integer;
//    procedure SetValueChangeProc(Value: TValueChangeProc);
  protected
    procedure Initialize; override;
//    procedure ItemValueChange(Sender: TPropertyListItem);
  public
    property OnValueChange: TValueChangeEvent read FOnValueChange write FOnValueChange;
    property Level: Integer read GetLevel;
    property ItemOwner: TPropertyListItem read FItemOwner;
    property Properties[Index: Integer]: TPropertyListItem read GetProp;

    function FindProperty(const Route: array of Integer): TPropertyListItem;
    function AddProperty(NodeClass: TPropertyListItemClass =
                         NIL): TPropertyListItem;
    function AddBoolean(const Name: WideString;
      Value: Boolean): TPropertyListItem;
    function AddFloat(const Name: WideString; const Value: Double; Precision: Byte): TPropertyListItem;
    function AddDecimal(const Name: WideString; Value, Min,
      Max: Int64): TPropertyListItem;
    function AddHexadecimal(const Name: WideString; Value, Min, Max: Int64;
      Digits: Byte): TPropertyListItem;
    function AddString(const Name, Value: WideString;
      Editable: Boolean = True): TPropertyListItem;

    function AddPickList(const Name: WideString; List: TTntStringList;
             AExternal: Boolean; Index: Integer; Fixed: Boolean = True): TPropertyListItem; overload;
    function AddPickList(const Name: WideString; const List: array
      of WideString; Index: Integer): TPropertyListItem; overload;
    function AddPickList(const Name, Value: WideString; const List: array
      of WideString; Fixed: Boolean = True): TPropertyListItem; overload;
    function AddPickList(const Name, Value, List: WideString;
      Fixed: Boolean = True): TPropertyListItem; overload;
    function AddPickList(const Name, List: WideString; Index: Integer;
      Fixed: Boolean = True): TPropertyListItem; overload;
    function AddData(const Name: WideString; const Description: WideString = ''): TPropertyListItem;
    function AddFilePick(const Name, FileName, Filter, DefaultExt,
      InitialDir: WideString; Save: Boolean = False; Options: TOpenOptions =
      [ofHideReadOnly, ofEnableSizing]): TFilePickItem;
    procedure SaveToStream(Stream: TStream); override;
    procedure LoadFromStream(Stream: TStream); override;

{    property ValueChangeProc: TValueChangeProc
             read FValueChangeProc
            write SetValueChangeProc;}    
 end;

 EPropertyListItemError = class(Exception);
 EPropertyListError = class(Exception);

procedure RegisterPropClasses(const List: array of TPropertyListItemClass);

implementation

var PropClassList: TClassList;

const
 LIST_HEAD: array[0..7] of Char = 'PROPLIST';

procedure RegisterPropClasses(const List: array of TPropertyListItemClass);
var I: Integer;
begin
 for I := 0 to Length(List) - 1 do PropClassList.AddClass(List[I]);
end;

procedure TPropertyListItem.Assign(Source: TNode);
var Src: TPropertyListItem absolute Source;
begin
 inherited;
 Assert((FData <> NIL) and (FPickList <> NIL) and (FSubProperties <> NIL),
        'Property subclasses are not assigned.');
 FName := Src.FName;
 FValueType := Src.FValueType;
 FValue := Src.FValue;
 FValueStr := Src.FValueStr;
 FStep := Src.FStep;
 FFloatStep := Src.FFloatStep;
 FFloat := Src.FFloat;
 FMin := Src.FMin;
 FMax := Src.FMax;
 FHexDigits := Src.FHexDigits;
 FReadOnly := Src.FReadOnly;
 FParameters := Src.FParameters;
 FPickListIndex := Src.FPickListIndex;
 if FListExternal and Src.FListExternal then
  FPickList := Src.FPickList else
 if not Src.FListExternal then
 begin
  if FListExternal then
   ListExternal := False;
  FPickList.Assign(Src.FPickList);
 end;
 FSubProperties.FItemOwner := Self;
 FSubProperties.Assign(Src.FSubProperties);
 FDefaultStr := Src.FDefaultStr;
 FTag := Src.FTag;
 FData.Size := Src.DataSize;
 Move(Src.Data^, Data^, FData.Size);
 FOnValueChange := Src.FOnValueChange;
end;

destructor TPropertyListItem.Destroy;
begin
 FData.Free;
 if not FListExternal then
  FPickList.Free;
 FSubProperties.Free;
 inherited;
end;

function TPropertyListItem.GetData: Pointer;
begin
 Assert(Assigned(FData), 'FData is not assigned.');

 Result := FData.Memory;
end;

function TPropertyListItem.GetDataSize: Integer;
begin
 Assert(Assigned(FData), 'FData is not assigned.');

 Result := FData.Size;
end;
          {
function TPropertyListItem.GetHexValueMax: Int64;
begin
 if FMin < 0 then
  Result := FMin else
  Result := FMax;
end;

function TPropertyListItem.GetHexValueMin: Int64;
begin
 if FMin >= 0 then
  Result := FMin else
  Result := 0;
end;   }

function TPropertyListItem.GetModified: Boolean;
begin
 Result := FValueStr <> FDefaultStr;
end;

procedure TPropertyListItem.InitBoolean(const AName: WideString;
  AValue: Boolean);
var I: Boolean;
begin
 FValueType := vtPickString;
 for I := False to True do
  FPickList.Add(BoolStrs[I]);
 FName := AName;

 FChanging := True;
 try
  ValueStr := BoolStrs[AValue];
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
 FParameters := PL_FIXEDPICK;
end;

procedure TPropertyListItem.InitData(const AName, Description: WideString);
begin
 FValueType := vtString;
 FParameters := PL_BUFFER;
 FReadOnly := True;
 FName := AName;
 if Description = '' then
  FValueStr := '(Binary Data)' else
  FValueStr := Description;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitDecimal(const AName: WideString; AValue, AMin,
  AMax: Int64);
begin
 FValueType := vtDecimal;
 FName := AName;
 if AMax < AMin then
 begin
  FMin := Low(Int64);
  FMax := High(Int64);
 end else
 begin
  FMin := AMin;
  FMax := AMax;
 end;
 FChanging := True;
 try
  Value := AValue;
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitFloat(const AName: WideString;
  const AValue: Double; APrecision: Byte);
begin
 FValueType := vtFloat;
 FName := AName;
 FPrecision := APrecision;
 FChanging := True;
 try
  Float := AValue;
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitHexadecimal(const AName: WideString; AValue,
  AMin, AMax: Int64; ADigits: Byte);
begin
 FValueType := vtHexadecimal;
 FName := AName;
 if AMax < AMin then
 begin
  FMin := AMax;
  FMax := AMin;
 end else
 begin
  FMin := AMin;
  FMax := AMax;
 end;
 FHexDigits := ADigits;
 FChanging := True;
 try
  Value := AValue;
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.Initialize;
begin
 FAssignableClass := TPropertyListItem;
 FStep := 1;
 FFloatStep := 1.0;
 FMin := Low(Int64);
 FMax := High(Int64);
 FData := TMemStream.Create;
 FPickList := TTntStringList.Create;
 FSubProperties := TPropertyList.Create;
 FSubProperties.FItemOwner := Self;
end;

procedure TPropertyListItem.InitPickList(const AName, AValue,
  List: WideString; Fixed: Boolean);
begin
 FValueType := vtPickString;
 FName := AName;
 if Fixed then
  FParameters := PL_FIXEDPICK else
  FParameters := 0;
 ListExternal := False;
 FPickList.Text := List;
 FChanging := True;
 try
  ValueStr := AValue;
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitPickList(const AName, AValue: WideString;
  const List: array of WideString; Fixed: Boolean);
var I: Integer;
begin
 FValueType := vtPickString;
 FName := AName;
 if Fixed then
  FParameters := PL_FIXEDPICK else
  FParameters := 0;
 ListExternal := False; 
 with FPickList do
  for I := 0 to Length(List) - 1 do
   Add(List[I]);
 FChanging := True;
 try
  ValueStr := AValue;
 finally
  FChanging := False;
 end;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitPickList(const AName: WideString;
  const List: array of WideString; Index: Integer);
var I: Integer;
begin
 FValueType := vtPickString;
 FName := AName;
 FParameters := PL_FIXEDPICK;
 ListExternal := False;
 with FPickList do
  for I := 0 to Length(List) - 1 do
   Add(List[I]);
 if (Index >= 0) and (Index < FPickList.Count) then
  FValueStr := List[Index] else
  FValueStr := '';
 FPickListIndex := Index;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitPickList(const AName: WideString;
  List: TTntStringList; AExternal: Boolean; Index: Integer; Fixed: Boolean);
begin
 FValueType := vtPickString;
 FName := AName;
 if Fixed then
  FParameters := PL_FIXEDPICK else
  FParameters := 0;
 ListExternal := AExternal;
 PickList := List;
 if (Index >= 0) and (Index < FPickList.Count) then
  FValueStr := FPickList[Index] else
  FValueStr := '';
 FPickListIndex := Index;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitPickList(const AName, List: WideString;
  Index: Integer; Fixed: Boolean);
begin
 FValueType := vtPickString;
 FName := AName;
 if Fixed then
  FParameters := PL_FIXEDPICK else
  FParameters := 0;
 ListExternal := False;
 FPickList.Text := List;
 if (Index >= 0) and (Index < FPickList.Count) then
  FValueStr := FPickList[Index] else
  FValueStr := '';
 FPickListIndex := Index;
 FDefaultStr := FValueStr;
end;

procedure TPropertyListItem.InitString(const AName, AValue: WideString;
  Editable: Boolean);
begin
 FValueType := vtString;
 FName := AName;
 FValueStr := AValue;
 FReadOnly := not Editable;
 FDefaultStr := AValue;
end;
{
procedure TPropertyListItem.ItemValueChange(Sender: TPropertyListItem);
begin
 if Assigned(FValueChangeProc) then
  FValueChangeProc(Sender);
end;
 }
procedure TPropertyListItem.JustAdded;
begin
 FItemOwner := TPropertyList(Owner).FItemOwner;
 inherited;
end;

procedure TPropertyListItem.LoadFromStream(const Stream: TStream);
var Cnt, I: Integer; WS: WideString;
begin
 Assert((FData <> NIL) and (FPickList <> NIL), 'Unexpected error');

 if Stream <> NIL then with Stream do
 begin
  FChanging := True;
  try
   Read(Cnt, 4);
   SetLength(FName, Cnt);
   Read(Pointer(FName)^, Cnt shl 1);
   Read(FValueType, SizeOf(TValueType));
   Read(FTag, 4);
   Read(FParameters, 4);
   Read(Cnt, 4); DataSize := Cnt;
   if Cnt > 0 then Read(Data^, Cnt);
   if FValueType in [vtDecimal, vtHexadecimal] then
   begin
    Read(FValue, 8);
    Read(FMin, 8);
    Read(FMax, 8);
    SetValue(FValue);
    if FValueType = vtHexadecimal then Read(FHexDigits, 1);
   end else
   begin
    if (FValueType <> vtPickString) or (FParameters and PL_FIXEDPICK = 0) then
    begin
     Read(Cnt, 4);
     SetLength(FValueStr, Cnt);
     Read(Pointer(FValueStr)^, Cnt shl 1);
    end;
    if FValueType = vtPickString then
    begin
     if FParameters and PL_FIXEDPICK <> 0 then Read(FPickListIndex, 4);
     Read(Cnt, 4);
     for I := 0 to Cnt - 1 do
     begin
      Read(Cnt, 4);
      SetLength(WS, Cnt);
      Read(Pointer(WS)^, Cnt shl 1);
      FPickList.Add(WS);
      Finalize(WS);
     end;
     if FParameters and PL_FIXEDPICK <> 0 then
      ValueStr := FPickList.Strings[FPickListIndex];
    end else if FValueType = vtString then Read(FReadOnly, 1);
   end;
   FDefaultStr := FValueStr;
  finally
   FChanging := False;
  end;
 end;
end;

procedure TPropertyListItem.SaveToStream(const Stream: TStream);
var Cnt, I: Integer; WS: WideString;
begin
 Assert((FData <> NIL) and (FPickList <> NIL), 'Unexpected error');

 if Stream <> NIL then with Stream do
 begin
  Cnt := Length(FName);
  Write(Cnt, 4);
  Write(Pointer(FName)^, Cnt shl 1);
  Write(FValueType, SizeOf(TValueType));
  Write(FTag, 4);
  Write(FParameters, 4);
  Cnt := DataSize; Write(Cnt, 4);
  if Cnt > 0 then Write(Data^, Cnt);
  if FValueType in [vtDecimal, vtHexadecimal] then
  begin
   Write(FValue, 8);
   Write(FMin, 8);
   Write(FMax, 8);
   if FValueType = vtHexadecimal then Write(FHexDigits, 1);
  end else
  begin
   if (FValueType <> vtPickString) or (FParameters and PL_FIXEDPICK = 0) then
   begin
    Cnt := Length(FValueStr);
    Write(Cnt, 4);
    Write(Pointer(FValueStr)^, Cnt shl 1);
   end;
   if FValueType = vtPickString then
   begin
    if FParameters and PL_FIXEDPICK <> 0 then Write(FPickListIndex, 4);
    Cnt := FPickList.Count; Write(Cnt, 4);
    for I := 0 to FPickList.Count - 1 do
    begin
     WS := FPickList.Strings[I];
     Cnt := Length(WS);
     Write(Cnt, 4);
     Write(Pointer(WS)^, Cnt shl 1);
    end;
   end else Write(FReadOnly, 1);
  end;
 end;
end;

procedure TPropertyListItem.SetDataSize(Size: Integer);
begin
 Assert(Assigned(FData), 'FData is not assigned.');

 if Size >= 0 then FData.Size := Size;
end;

procedure TPropertyListItem.SetFloat(const Value: Double);
var
 Accepted: Boolean;
begin
 Accepted := Value <> FFloat;

 if (FValueStr = '') or Accepted then
  FValueStr := FloatToStrF(Value, ffGeneral, Max(2, Min(15, FPrecision)), 0);
 if Accepted then
 begin
  FParameters := FParameters and not PL_MULTIPLE_VALUE; 
  FFloat := Value;
  ValueChange;
 end;
end;

procedure TPropertyListItem.SetHexDigits(Value: Byte);
begin
 FHexDigits := Value;
 If FValueType = vtHexadecimal then
  FValueStr := IntToDelphiHex(FValue, Value);
end;

procedure TPropertyListItem.SetListExternal(Value: Boolean);
var
 Temp: TTntStringList;
begin
 if FListExternal <> Value then
 begin
  FListExternal := Value;
  if Value then FreeAndNil(FPickList) else
  begin
   Temp := TTntStringList.Create;
   if FPickList <> nil then
    Temp.Assign(FPickList);
   FPickList := Temp;
  end;
 end;
end;

procedure TPropertyListItem.SetPickList(Value: TTntStringList);
begin
 if not FListExternal then
 begin
  FPickList.Assign(Value);
  FPickList.CaseSensitive := Value.CaseSensitive;
 end else
  FPickList := Value;
end;

procedure TPropertyListItem.SetPickListIndex(Value: Integer);
begin
 FPickListIndex := Value;
 FValueStr := FPickList.Strings[Value];
end;

procedure TPropertyListItem.SetPrecision(Value: Byte);
begin
 FPrecision := Value;
 if FValueType = vtFloat then
  SetFloat(FFloat);
end;

procedure TPropertyListItem.SetValue(Value: Int64);
var
 Accepted: Boolean;
begin
 if Value < FMin then
  Value := FMin else
 if Value > FMax then
  Value := FMax;

 Accepted := Value <> FValue;
 if (FValueStr = '') or Accepted then
 begin
  if FValueType = vtHexadecimal then
   FValueStr := IntTo0xHex(Value, FHexDigits) Else
   FValueStr := IntToStr(Value);
 end;

 if Accepted then
 begin
  FParameters := FParameters and not PL_MULTIPLE_VALUE;
  FValue := Value;
  ValueChange;
 end;
end;

{procedure TPropertyListItem.SetValueChangeProc(Value: TValueChangeProc);
begin
 FValueChangeProc := Value;
 FOnValueChange := ItemValueChange;
end;
 }
procedure TPropertyListItem.SetValueStr(Value: WideString);
var
 E: Integer;
// P: PWideChar;
 C: Int64;
// Negative: Boolean;
 X: Double;
begin
 FConversionResult := PI_E_OK;
 if (Value = '') and (FParameters and PL_MULTIPLE_VALUE <> 0) then
  Exit;
 if (Value <> FValueStr) or (FParameters and PL_MULTIPLE_VALUE <> 0) then
 begin
  FParameters := FParameters and not PL_MULTIPLE_VALUE;
  case FValueType of
   vtDecimal, vtHexadecimal:
   begin
    Val(Value, C, E);
    If E <> 0 then
     FConversionResult := PI_E_STRTOINT else
    If (C < FMin) or (C > FMax) then
     FConversionResult := PI_E_OUTOFBOUNDS;

    if FConversionResult = PI_E_OK then
    begin
     FValue := C;
     If FValueType = vtHexadecimal then
      Value := IntTo0xHex(C, FHexDigits) Else
      Value := IntToStr(C);
    end;
   end;
   vtFloat:
   if TryStrToFloat(Value, X) then
   begin
    Value := FloatToStrF(X, ffGeneral, Max(2, Min(15, FPrecision)), 0);
    FFloat := X;
   end else FConversionResult := PI_E_STRTOFLOAT;
  end;
  if FValueType = vtPickString then
   FPickListIndex := FPickList.IndexOf(Value);
  if FConversionResult = PI_E_OK then
  begin
   if (ValueType = vtPickString) or
     (FMaxLength <= 0) or
     (Length(Value) <= FMaxLength) then
   begin
    FValueStr := Value;
    ValueChange;
   end else
    FConversionResult := PI_E_OUTOFBOUNDS;
  end;
 end;
end;

procedure TPropertyListItem.ValueChange;
var
 Item: TPropertyListItem;
begin
 if FChanging or
    (FParameters and PL_MULTIPLE_VALUE <> 0) then Exit;
 Item := Self;
 while Item <> nil do
 begin
  if Assigned(Item.FOnValueChange) then
  begin
   Item.FOnValueChange(Self);
   Exit;
  end else
  with TPropertyList(Item.Owner) do
  begin
   if Assigned(FOnValueChange) then
   begin
    FOnValueChange(Self);
    Exit;
   end else
   begin
    Item := FItemOwner;
    if Item <> nil then
      if Assigned(Item.FOnValueChange) then
      begin
       Item.FOnValueChange(Self);
       Exit;
      end;
   end;
  end;
 end;
end;

destructor TFilePickItem.Destroy;
begin
 Finalize(FDefaultExt);
 Finalize(FInitialDir);
 Finalize(FFilter);
 inherited;
end;

procedure TFilePickItem.FillOpenDialog(OpenDialog: TTntOpenDialog);
begin
 If Assigned(OpenDialog) then With OpenDialog do
 begin
  Title := Self.Name;
  FileName := FValueStr;
  DefaultExt := FDefaultExt;
  InitialDir := FInitialDir;
  Filter := FFilter;
  Options := FOptions;
 end;
end;

procedure TFilePickItem.FillSaveDialog(SaveDialog: TTntSaveDialog);
begin
 FillOpenDialog(SaveDialog);
end;

procedure TFilePickItem.Init(const AName, AFileName, AFilter, ADefaultExt,
  AInitialDir: WideString; ASave: Boolean; AOptions: TOpenOptions);
begin
 Name := AName;
 FValueType := vtString;
 FValueStr := AFileName;
 FFilter := AFilter;
 FInitialDir := AInitialDir;
 FDefaultExt := ADefaultExt;
 FOptions := AOptions;
 FSave := ASave;
end;

procedure TFilePickItem.LoadFromStream(const Stream: TStream);
var Cnt: Integer;
begin
 inherited;
 with Stream do
 begin
  Read(Cnt, 4);
  SetLength(FFilter, Cnt);
  Read(Pointer(FFilter)^, Cnt shl 1);

  Read(Cnt, 4);
  SetLength(FInitialDir, Cnt);
  Read(Pointer(FInitialDir)^, Cnt shl 1);

  Read(Cnt, 4);
  SetLength(FDefaultExt, Cnt);
  Read(Pointer(FDefaultExt)^, Cnt shl 1);

  Read(FOptions, SizeOf(TOpenOptions));
  Read(FSave, 1);
 end;
end;

procedure TFilePickItem.SaveToStream(const Stream: TStream);
var Cnt: Integer;
begin
 inherited;
 with Stream do
 begin
  Cnt := Length(FFilter);
  Write(Cnt, 4);
  Write(Pointer(FFilter)^, Cnt shl 1);

  Cnt := Length(FInitialDir);
  Write(Cnt, 4);
  Write(Pointer(FInitialDir)^, Cnt shl 1);

  Cnt := Length(FDefaultExt);
  Write(Cnt, 4);
  Write(Pointer(FDefaultExt)^, Cnt shl 1);

  Write(FOptions, SizeOf(TOpenOptions));
  Write(FSave, 1);
 end;
end;

function TPropertyList.AddBoolean(const Name: WideString;
  Value: Boolean): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitBoolean(Name, Value);
end;

function TPropertyList.AddData(const Name, Description: WideString): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitData(Name, Description);
end;

function TPropertyList.AddDecimal(const Name: WideString; Value, Min,
  Max: Int64): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitDecimal(Name, Value, Min, Max);
end;

function TPropertyList.AddFilePick(const Name, FileName, Filter, DefaultExt,
  InitialDir: WideString; Save: Boolean; Options: TOpenOptions): TFilePickItem;
begin
 Result := AddProperty(TFilePickItem) as TFilePickItem;
 Result.Init(Name, FileName, Filter, DefaultExt, InitialDir, Save, Options);
end;

function TPropertyList.AddFloat(const Name: WideString;
  const Value: Double; Precision: Byte): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitFloat(Name, Value, Precision);
end;

function TPropertyList.AddHexadecimal(const Name: WideString; Value, Min,
  Max: Int64; Digits: Byte): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitHexadecimal(Name, Value, Min, Max, Digits);
end;

function TPropertyList.AddPickList(const Name, Value: WideString;
  const List: array of WideString; Fixed: Boolean): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitPickList(Name, Value, List, Fixed);
end;

function TPropertyList.AddPickList(const Name, Value, List: WideString;
      Fixed: Boolean = True): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitPickList(Name, Value, List, Fixed);
end;

function TPropertyList.AddPickList(const Name: WideString;
  const List: array of WideString; Index: Integer): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitPickList(Name, List, Index);
end;

function TPropertyList.AddPickList(const Name: WideString;
  List: TTntStringList; AExternal: Boolean;
  Index: Integer; Fixed: Boolean): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitPickList(Name, List, AExternal, Index, Fixed);
end;

function TPropertyList.AddPickList(const Name, List: WideString;
  Index: Integer; Fixed: Boolean): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitPickList(Name, List, Index, Fixed);
end;

function TPropertyList.AddProperty(NodeClass:
  TPropertyListItemClass): TPropertyListItem;
begin
 Result := AddNode(NodeClass) as TPropertyListItem;
end;

function TPropertyList.AddString(const Name, Value: WideString;
  Editable: Boolean = True): TPropertyListItem;
begin
 Result := AddProperty;
 Result.InitString(Name, Value, Editable);
end;

function TPropertyList.FindProperty(const Route: array of Integer): TPropertyListItem;
var
 List: TPropertyList;
 I: Integer;
begin
 List := Self;
 Result := nil;
 for I := 0 to Length(Route) - 1 do
 begin
  Result := List.Properties[Route[I]];
  if Result = nil then Break;
  List := Result.FSubProperties;
 end;
end;

function TPropertyList.GetLevel: Integer;
var
 Item: TPropertyListItem;
begin
 Item := FItemOwner;
 Result := 0;
 while Item <> nil do
 begin
  Item := Item.FItemOwner;
  Inc(Result);
 end;
end;

function TPropertyList.GetProp(Index: Integer): TPropertyListItem;
begin
 Result := Nodes[Index] as TPropertyListItem;
end;

procedure TPropertyList.Initialize;
begin
 FAssignableClass := TPropertyList;
 FNodeClass := TPropertyListItem;
end;

{procedure TPropertyList.ItemValueChange(Sender: TPropertyListItem);
begin
 if Assigned(FValueChangeProc) then
  FValueChangeProc(Sender);
end;
 }
procedure TPropertyList.LoadFromStream(Stream: TStream);
function ReadList(List: TPropertyList): Boolean;
var
 HEAD: array[0..7] of Char;
 Cnt, I, J: Integer;
 Item: TClassListItem;
begin
 Result := False;
 with Stream, List do
 begin
  Read(HEAD, SizeOf(HEAD));
  if HEAD = LIST_HEAD then
  begin
   Result := True;
   Read(Cnt, 4);
   for I := 0 to Cnt - 1 do
   begin
    Read(J, 4);
    Item := PropClassList.Classes[J];

    if Item <> NIL then with AddProperty(TPropertyListItemClass(Item.AClass)) do
    begin
     LoadFromStream(Stream);
     Assert(FSubProperties <> NIL, 'Unexpected error');
     Result := ReadList(FSubProperties);
    end else Result := False;
    if not Result then Break;
   end;
  end;
 end;
end;
var Temp: TPropertyList;
begin
 if Stream = NIl then Exit;
 Temp := TPropertyListClass(ClassType).Create;
 try
  if ReadList(Temp) then Assign(Temp) else
   raise EPropertyListError.Create('Error loading property list.');
 finally
  Temp.Free;
 end;
end;

procedure TPropertyList.SaveToStream(Stream: TStream);
procedure WriteList(List: TPropertyList);
var I: Integer; Item: TClassListItem;
begin
 with Stream, List do
 begin
  Write(LIST_HEAD, SizeOf(LIST_HEAD));
  Write(Count, 4);
  for I := 0 to Count - 1 do with Properties[I] do
  begin
   Item := PropClassList.Find(ClassType);
   if Item = NIL then
    Write(Item, 4) else
    Write(Item.Index, 4);
   SaveToStream(Stream);
   Assert(FSubProperties <> NIL, 'Unexpected error');
   WriteList(FSubProperties);
  end;
 end;
end;
begin
 WriteList(Self);
end;

{procedure TPropertyList.SetValueChangeProc(Value: TValueChangeProc);
begin
 FValueChangeProc := Value;
 FOnValueChange := ItemValueChange;
end;
 }
initialization
 PropClassList := TClassList.Create;
 PropClassList.RegisterClasses([TPropertyListItem, TFilePickItem]);
finalization
 PropClassList.Free;
end.
