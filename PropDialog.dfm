object PropDialogForm: TPropDialogForm
  Left = 280
  Top = 189
  BorderStyle = bsDialog
  ClientHeight = 177
  ClientWidth = 395
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = TntFormCloseQuery
  DesignSize = (
    395
    177)
  PixelsPerInch = 96
  TextHeight = 13
  object PropertyEditor: TVirtualPropertyEditor
    Left = 8
    Top = 8
    Width = 377
    Height = 131
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Ctl3D = True
    ParentCtl3D = False
    ScrollBarOptions.ScrollBars = ssVertical
    TabOrder = 2
    PropertyWidth = 150
    ValueWidth = 223
    OnInitNode = PropertyEditorInitNode
    OnGetPropertyName = PropertyEditorGetPropertyName
    OnGetValue = PropertyEditorGetValue
    OnGetPickList = PropertyEditorGetPickList
    OnSetValue = PropertyEditorSetValue
    OnGetEditType = PropertyEditorGetEditType
    OnEllipsisClick = PropertyEditorEllipsisClick
    Columns = <
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 0
        Width = 150
      end
      item
        Margin = 0
        Options = [coEnabled, coParentBidiMode, coParentColor, coResizable, coVisible, coFixed]
        Position = 1
        Width = 223
      end>
    WideDefaultText = 'No text'
  end
  object CancelButton: TTntButton
    Left = 312
    Top = 148
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    BiDiMode = bdLeftToRight
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    ParentBiDiMode = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
  end
  object OkButton: TTntButton
    Left = 232
    Top = 148
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    BiDiMode = bdLeftToRight
    Caption = 'OK'
    Default = True
    ModalResult = 1
    ParentBiDiMode = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = OkButtonClick
  end
  object OpenDialog: TTntOpenDialog
    Left = 8
    Top = 224
  end
end
