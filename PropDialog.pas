unit PropDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  TntForms, Dialogs, VirtualTrees, PropEditor, ExtCtrls, StdCtrls, TntClasses,
  PropContainer, TntStdCtrls, ComCtrls, ActnList, PropUtils, TntDialogs,
  TntComCtrls, TntSysUtils, MyUtils;

type
  TOkClickEvent = procedure (Sender: TTntButton; Props: TPropertyList;
   Var Accepted: Boolean) of Object;
  TPropDialogForm = class(TTntForm)
    PropertyEditor: TVirtualPropertyEditor;
    CancelButton: TTntButton;
    OkButton: TTntButton;
    OpenDialog: TTntOpenDialog;
    procedure PropertyEditorInitNode(Sender: TBaseVirtualTree; ParentNode,
      Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure PropertyEditorGetEditType(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var EditType: TEditType);
    procedure PropertyEditorGetPickList(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var List: TTntStringList;
      var FreeAfterUse: Boolean);
    procedure PropertyEditorGetPropertyName(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Text: WideString);
    procedure PropertyEditorGetValue(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Text: WideString);
    procedure PropertyEditorSetValue(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; Text: WideString);
    procedure PropertyEditorEllipsisClick(Sender: TCustomPropertyEditor;
      Node: PVirtualNode; var Accepted: Boolean);
    procedure OkButtonClick(Sender: TObject);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
   private
    FSelectedItem: TPropertyListItem;
    FPropertyList: TPropertyList;
    FOnEllipsisClick: TEllipsisClickEvent;
    FOnOkClick: TOkClickEvent;
    FOkAccepted: Boolean;
   public
    property SelectedItem: TPropertyListItem read FSelectedItem
                                            write FSelectedItem;
    Function Execute(PropertyList: TPropertyList): Boolean;
  end;

Function PropertyDialog(const Caption: WideString;
                        const PropertyList: TPropertyList;
                        EllipsisClick: TEllipsisClickEvent = NIL;
   OkPress: TOkClickEvent = NIL; OkTag: Integer = 0; WindowHeight: Integer = -1): Boolean;

implementation

{$R *.dfm}

Function PropertyDialog(const Caption: WideString;
                        const PropertyList: TPropertyList;
                        EllipsisClick: TEllipsisClickEvent;
   OkPress: TOkClickEvent; OkTag: Integer; WindowHeight: Integer): Boolean;
var LoadForm: TPropDialogForm;
begin
 If Assigned(PropertyList) and (PropertyList.Count > 0) then
 begin
  Application.CreateForm(TPropDialogForm, LoadForm);
  With LoadForm do
  begin
   If WindowHeight < 0 then
    ClientHeight := Integer(PropertyEditor.DefaultNodeHeight) *
    PropertyList.Count + 50 Else
    ClientHeight := WindowHeight;
  end;
  LoadForm.FOnOkClick := OkPress;
  LoadForm.FOnEllipsisClick := EllipsisClick;
  LoadForm.OkButton.Tag := OkTag;
  LoadForm.Caption := Caption;
  Result := LoadForm.Execute(PropertyList);
  LoadForm.Free;
 end Else Result := False;
end;

Function TPropDialogForm.Execute(PropertyList: TPropertyList): Boolean;
begin
 FPropertyList := PropertyList;
 PropertyEditor.NodeDataSize := SizeOf(TPropertyData);
 PropertyEditor.RootNodeCount := FPropertyList.Count;
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TPropDialogForm.PropertyEditorInitNode(Sender: TBaseVirtualTree;
  ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
 DefaultInitNode(Sender, FPropertyList, Node, FSelectedItem, InitialStates);
end;

procedure TPropDialogForm.PropertyEditorGetEditType(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var EditType: TEditType);
begin
 EditType := DefaultGetEditType(Sender, Node);
end;

procedure TPropDialogForm.PropertyEditorGetPickList(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var List: TTntStringList; var FreeAfterUse: Boolean);
Var Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 List := Data.Item.PickList;
 FreeAfterUse := False;
end;

procedure TPropDialogForm.PropertyEditorGetPropertyName(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var Text: WideString);
Var Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 Text := Data.Item.Name;
end;

procedure TPropDialogForm.PropertyEditorGetValue(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var Text: WideString);
Var Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 Text := Data.Item.ValueStr;
end;

procedure TPropDialogForm.PropertyEditorSetValue(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; Text: WideString);
begin
 DefaultSetValue(Sender, FPropertyList, Node, Text);
end;

procedure TPropDialogForm.PropertyEditorEllipsisClick(Sender: TCustomPropertyEditor;
  Node: PVirtualNode; var Accepted: Boolean);
Var Data: PPropertyData;
begin
 Data := Sender.GetNodeData(Node);
 With Data^ do If Item is TFilePickItem then With Item as TFilePickItem do
 begin
  FillOpenDialog(OpenDialog);
  Accepted := OpenDialog.Execute;
  If Accepted then ValueStr := OpenDialog.FileName;
 end else
 if Assigned(FOnEllipsisClick) then
  FOnEllipsisClick(Sender, Node, Accepted);
end;

procedure TPropDialogForm.OkButtonClick(Sender: TObject);
var I: Integer; Prop: TPropertyListItem;
begin
 FOkAccepted := not PropertyEditor.AcceptError;
 If FOkAccepted then
 begin
  PropertyEditor.AcceptEdit;
  FOkAccepted := not PropertyEditor.AcceptError;
  if FOkAccepted then with FPropertyList do
  begin
   for I := 0 to Count - 1 do
   begin
    Prop := Properties[I];
    if (Prop <> NIL) and (Prop is TFilePickItem) then
    with TFilePickItem(Prop) do
    begin
     if (ValueStr = '') then
     begin
      FOkAccepted := False;
      WideMessageDlg(WideFormat('Value of "%s" property must be correct file name.',
                                [Name]), mtError, [mbOk], 0);
     end else if ofFileMustExist in Options then
     begin
      if not WideFileExists(ValueStr) then
      begin
       FOkAccepted := False;
       WideMessageDlg(WideFormat('File "%s" does not exist.', [ValueStr]),
                      mtError, [mbOk], 0);
      end;
     end;
    end;
   end;
  end;
  if FOkAccepted and Assigned(FOnOkClick) then
   FOnOkClick(OkButton, FPropertyList, FOkAccepted);
 end;
 PropertyEditor.AcceptError := False;
end;

procedure TPropDialogForm.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := (ModalResult <> mrOk) or (FOkAccepted);
end;

end.
