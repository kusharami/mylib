unit PropUtils;

interface

Uses
 SysUtils, Dialogs, TntDialogs, Graphics, VirtualTrees, PropEditor, PropContainer;

Type
 //Property List Types
 PPropertyData = ^TPropertyData;
 TPropertyData = Packed Record
  Owner: TPropertyList;
  Item: TPropertyListItem;
 end;

Procedure DefaultInitNode(Tree: TBaseVirtualTree;
    Props: TPropertyList; Node: PVirtualNode; SelectedItem: TPropertyListItem;
     var InitialStates: TVirtualNodeInitStates);
Function DefaultGetEditType(Tree: TBaseVirtualTree;
    Node: PVirtualNode): TEditType;
Procedure DefaultSetValue(Tree: TBaseVirtualTree;
    Props: TPropertyList; Node: PVirtualNode; Text: WideString);
function DefaultGetValueStyle(Tree: TBaseVirtualTree; Node: PVirtualNode): TFontStyles;

implementation

Procedure DefaultSetValue(Tree: TBaseVirtualTree;
    Props: TPropertyList; Node: PVirtualNode; Text: WideString);
Var
 PropertyEditor: TVirtualPropertyEditor absolute Tree;
 Data: PPropertyData; I: Integer;
begin
 Data := Tree.GetNodeData(Node);
 With Data.Item do
 begin
  if ValueType = vtPickString then
  begin
   If Parameters and PL_FIXEDPICK <> 0 then
   begin
    I := PickList.IndexOf(Text);
    If I >= 0 then ValueStr := PickList.Strings[I] Else
    begin
     WideMessageDlg(SInvalidPropertyValue, mtError, [mbOk], 0);
     PropertyEditor.AcceptError := True;
    end;
   end Else ValueStr := Text;
  end Else
  begin
   ValueStr := Text;
   Case ConversionResult of
    PI_E_STRTOINT:
    begin
     WideMessageDlg(WideFormat(SInvalidIntegerValue, [Text]),
     mtError, [mbOk], 0);
     PropertyEditor.AcceptError := True;
    end;
    PI_E_OUTOFBOUNDS:
    begin
     case ValueType of
      vtString:
       WideMessageDlg(WideFormat(SLengthExceed, [MaxLength]),
                     mtError, [mbOk], 0);
      vtDecimal:
       WideMessageDlg(WideFormat(SValueOutOfRange, [ValueMin, ValueMax]),
                      mtError, [mbOk], 0);
      vtHexadecimal:
       WideMessageDlg(WideFormat(SHexaOutOfRange, [ValueMin, ValueMax]),
                      mtError, [mbOk], 0);
     end;
     PropertyEditor.AcceptError := True;
    end;
    PI_E_STRTOFLOAT:
    begin
     WideMessageDlg(WideFormat(SInvalidFloatingPointValue, [Text]),
     mtError, [mbOk], 0);
     PropertyEditor.AcceptError := True;
    end;
   end;
  end;
  PropertyEditor.RefreshValue(Node);
 end;
end;

Function DefaultGetEditType(Tree: TBaseVirtualTree;
    Node: PVirtualNode): TEditType;
Var Data: PPropertyData;
Label ETSTR;
begin
 Data := Tree.GetNodeData(Node);
 With Data^ do
 Case Item.ValueType of
  vtDecimal, vtHexadecimal: ETSTR: If Item.ReadOnly then
    Result := etStringReadOnly Else
    Result := etString;
  vtString: If Item is TFilePickItem then
  begin
   If Item.ReadOnly then
    Result := etEllipsisReadOnly Else
    Result := etEllipsis;
  end Else
  begin
   if Item.Parameters and PL_BUFFER <> 0 then
    Result := etEllipsisReadOnly else
    Goto ETSTR;
  end;
  vtPickString:
  begin
   If Item.ReadOnly then
    Result := etStringReadOnly else
    Result := etPickString;
  end;
  Else Result := etStringReadOnly;
 end;
end;

Procedure DefaultInitNode(Tree: TBaseVirtualTree;
    Props: TPropertyList; Node: PVirtualNode; SelectedItem: TPropertyListItem;
    var InitialStates: TVirtualNodeInitStates);
var
 Data, ParentData: PPropertyData; I: Integer;
begin
 with Tree as TCustomPropertyEditor do
 begin
  Data := GetNodeData(Node);
  If Node.Parent = RootNode then
  begin
   Data.Owner := Props;
   Data.Item := Props.Properties[Node.Index];
  end Else
  begin
   ParentData := GetNodeData(Node.Parent);
   Data.Owner := ParentData.Item.SubProperties;
   Data.Item := Data.Owner.Properties[Node.Index];
  end;
  if Data.Item = SelectedItem then
  begin
 //  Include(InitialStates, ivsSelected);
  // Include(Node.Parent.States, vsInitialUserData);
//   NoForceEdit := True;
   FocusedNode := Node;
  end; {else
  if Data.Item.SubProperties = Props.SelectedItem.Owner then
  begin
   Include(InitialStates, ivsExpanded);
   Include(Node.States, vsInitialUserData);
   (*
   with Data.Item.SubProperties do
    for I := 0 to Count - 1 do
    begin
     DoNotCancel := True;
     AddChild(Node);
     if IsVisible[AddChild(Node)] then; // InitNode
    end;
   Exit;
   *)
  end; }
  with Data.Item.SubProperties do
   for I := 0 to Count - 1 do
   begin
    DoNotCancel := True;
    if IsVisible[AddChild(Node)] then; // InitNode;
   end;
 end;
end;

function DefaultGetValueStyle(Tree: TBaseVirtualTree; Node: PVirtualNode): TFontStyles;
var Data: PPropertyData;
begin
 Data := Tree.GetNodeData(Node);
 With Data.Item do If Modified then
  Result := [fsBold] else
  Result := [];
end;

end.
