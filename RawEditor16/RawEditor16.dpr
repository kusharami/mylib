program RawEditor16;

uses
  Forms,
  mainview in 'mainview.pas' {MainForm},
  namer in 'namer.pas' {NamesDialog},
  SEF in '..\SEF.pas' {SEFORM};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Raw Editor 16 bit';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSEFORM, SEFORM);
  Application.CreateForm(TNamesDialog, NamesDialog);
  Application.CreateForm(TSEFORM, SEFORM);
  Application.Run;
end.
