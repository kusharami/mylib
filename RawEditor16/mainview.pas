unit mainview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, Spin, ExtCtrls, gbaUnit, hexunit, sef, Buttons, DIB;

type
  TMainForm = class(TForm)
    Image: TImage;
    WidthEdit: TSpinEdit;
    HeightEdit: TSpinEdit;
    SizeEdit: TSpinEdit;
    ScrollBar: TScrollBar;
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    OpenItem: TMenuItem;
    SaveItem: TMenuItem;
    SaveAsItem: TMenuItem;
    ExitItem: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PosEdit: TEdit;
    LeftButton: TSpeedButton;
    RightButton: TSpeedButton;
    ListBox: TListBox;
    AddButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    BookmarksMenu: TMenuItem;
    AddItem: TMenuItem;
    RemoveItem: TMenuItem;
    OpenBookmarksItem: TMenuItem;
    SaveBookmarksItem: TMenuItem;
    OpenBmkDialog: TOpenDialog;
    SaveBmkDialog: TSaveDialog;
    ImageScrollVertical: TScrollBar;
    ImageScrollHorisontal: TScrollBar;
    EditMenu: TMenuItem;
    SaveBitmapItem: TMenuItem;
    LoadBitmapItem: TMenuItem;
    OpenBmpDialog: TOpenDialog;
    SaveBmpDialog: TSaveDialog;
    WidthLabel: TLabel;
    HeightLabel: TLabel;
    SizeLabel: TLabel;
    PositionLabel: TLabel;
    procedure FormShow(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    procedure OpenItemClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ScrollBarChange(Sender: TObject);
    procedure WidthEditChange(Sender: TObject);
    procedure HeightEditChange(Sender: TObject);
    procedure LeftButtonClick(Sender: TObject);
    procedure RightButtonClick(Sender: TObject);
    procedure PosEditKeyPress(Sender: TObject; var Key: Char);
    procedure PosEditChange(Sender: TObject);
    procedure AddItemClick(Sender: TObject);
    procedure RemoveItemClick(Sender: TObject);
    procedure OpenBookmarksItemClick(Sender: TObject);
    procedure SaveBookmarksItemClick(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure SizeEditChange(Sender: TObject);
    procedure ImageScrollVerticalChange(Sender: TObject);
    procedure ImageScrollHorisontalChange(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SaveItemClick(Sender: TObject);
    procedure SaveAsItemClick(Sender: TObject);
    procedure SaveBitmapItemClick(Sender: TObject);
    procedure LoadBitmapItemClick(Sender: TObject);
  private
    Procedure DrawRom;
    Procedure InitScroll(W, H, S: Integer);
    Procedure EnableAll;
  public
    { Public declarations }
  end;
  TRGB = Packed Record R, G, B: Byte end;

var
  MainForm: TMainForm;
  FName: String;
  ROM: Array of Byte;
  RSZ: Integer;
  Pos: Integer;
  DIB: TDIB;
  Saved: Boolean;

Type
 PBookmark = ^TBookmark;
 TBookmark = Record
  Name: String;
  Position: DWord;
  Width: Integer;
  Height: Integer;
  Size: Integer;
  Next: PBookmark;
 end;
 TBookmarks = Class
  Root, Cur: PBookmark;
  Count: Integer;
  Constructor Create;
  Function Add: PBookmark;
  Procedure Remove(N: PBookmark);
  Destructor Destroy; override;
 end;

Var
 Bookmarks: TBookmarks;
 WW: Integer = 32;
 HH: Integer = 32;
 SS: Integer = 0;

implementation

uses namer;

{$R *.dfm}

Constructor TBookmarks.Create;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

Function TBookmarks.Add: PBookmark;
begin
 New(Result);
 If Root = NIL then
 begin
  Root := Result;
  Cur := Result;
 end Else
 begin
  Cur^.Next := Result;
  Cur := Result;
 end;
 FillChar(Result^, SizeOf(TBookmark), 0);
 Inc(Count);
end;

Destructor TBookmarks.Destroy;
Var N: PBookmark;
begin
 While Root <> NIL do
 begin
  N := Root^.Next;
  Root^.Name := '';
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

procedure TMainForm.EnableAll;
begin
 SaveItem.Enabled := RSZ > 0;
 SaveAsItem.Enabled := RSZ > 0;
 ScrollBar.Enabled := RSZ > 0;
 PosEdit.Enabled := RSZ > 0;
 WidthEdit.Enabled := RSZ > 0;
 HeightEdit.Enabled := RSZ > 0;
 SizeEdit.Enabled := RSZ > 0;
 LeftButton.Enabled := RSZ > 0;
 RightButton.Enabled := RSZ > 0;
 AddButton.Enabled := RSZ > 0;
 DeleteButton.Enabled := RSZ > 0;
 ListBox.Enabled := RSZ > 0;
 BookmarksMenu.Enabled := RSZ > 0;
 EditMenu.Enabled := RSZ > 0;
 If RSZ > 0 then
 begin
  Caption := 'Raw Editor 16 - [' + FName + ']';
  If not Saved then Caption := Caption + '*';
 end Else Caption := 'Raw Editor 16';
end;

Var
MW, MH, Isv, Sb, We, He, Se, Pe, Lb, lbh, Rb, Ab, Db, Lib, ISH, IMW, IMH: Integer;

procedure TMainForm.FormShow(Sender: TObject);
begin
 RSZ := 0;
 Bookmarks := TBookmarks.Create;
 DIB := TDIB.Create;
 DIB.PixelFormat := MakeDIBPixelFormat(5, 5, 5);
 DIB.BitCount := 16;
 MW := Width;
 MH := Height;
 Isv := Width - ImageScrollVertical.Left;
 ISH := Height - ImageScrollHorisontal.Top;
 Sb := Width - ScrollBar.Left;
 WE := Width - WidthEdit.Left;
 HE := Width - HeightEdit.Left;
 SE := Width - SizeEdit.Left;
 PE := Width - PosEdit.Left;
 LB := Width - LeftButton.Left;
 RB := Width - RightButton.Left;
 AB := Width - AddButton.Left;
 DB := Width - DeleteButton.Left;
 LIB := Width - ListBox.Left;
 IMW := Width - Image.Width;
 IMH := Height - Image.Height;
 LBH := Height - ListBox.Height;
 Image.Picture.Bitmap.Width := WW * (SS+ 1);
 Image.Picture.Bitmap.Height := HH * (SS + 1);
 EnableAll;
end;

procedure TMainForm.ExitItemClick(Sender: TObject);
begin
 Close;
end;

Procedure TMainForm.DrawRom;
Var
 TX, TY, W, H, S, X, Y: Integer; RD: ^Byte; Col: TColor;
 Rct: TRect;
begin
 If RSZ = 0 then Exit;
 W := WW;
 H := HH;
 S := SS;
 TX := ImageScrollHorisontal.Position;
 TY := ImageScrollVertical.Position;
 DIB.Width := W;
 DIB.Height := H;
 With DIB do For Y := 0 to H - 1 do
  Move(ROM[Pos + Y * (W shl 1)], ScanLine[Y]^, W shl 1);
 Image.Picture.Bitmap.Width := W * (S + 1);
 Image.Picture.Bitmap.Height := H * (S + 1);
 With Image.Canvas do
 begin
  Brush.Color := clInactiveBorder;
  FillRect(ClipRect);
  Rct := DIB.Canvas.ClipRect;
  Rct.Left := TX;
  Rct.Top := TY;
  CopyRect(ClipRect, DIB.Canvas, Rct);
{  RD := Addr(ROM^[Pos]);
  For Y := 0 to H - 1 do
   For X := 0 to W - 1 do
   begin
    Move(Palette^[RD^], Col, 3);
    PutPixel(X - TX, Y - TY);
    Inc(DWord(RD));
   end;}
 end;
end;

Procedure TMainForm.InitScroll(W, H, S: Integer);
Var WH: Integer;
begin
 WW := W;
 HH := H;
 SS := S;
 WH := W * H * 2;
 ScrollBar.LargeChange := WH;
 ScrollBar.Max := Integer(RSZ) - WH;
 ImageScrollHorisontal.Position := 0;
 ImageScrollVertical.Position := 0;
 If W * S > Image.Width then
 begin
  ImageScrollHorisontal.Enabled := True;
  ImageScrollHorisontal.Max := W - Image.Width div S;
 end Else ImageScrollHorisontal.Enabled := False;
 If H * S > Image.Height then
 begin
  ImageScrollVertical.Enabled := True;
  ImageScrollVertical.Max := H - Image.Height div S;
 end Else ImageScrollVertical.Enabled := False;
end;

procedure TMainForm.OpenItemClick(Sender: TObject);
Var F: File;
begin
 If OpenDialog.Execute then
 begin
  FName := OpenDialog.FileName;
  Saved := True;
  AssignFile(F, FName);
  Reset(F, 1);
  RSZ := FileSize(F);
  SetLength(ROM, RSZ);
  BlockRead(F, ROM[0], RSZ);
  CloseFile(F);
  Pos := 0;
  ScrollBar.Position := 0;
  InitScroll(WW, HH, SS);
  DrawRom;
  EnableAll;
 end;
end;

Var DontChange, CanChange: Boolean;

procedure TMainForm.ScrollBarChange(Sender: TObject);
begin
 If DontChange then Exit;
 Pos := ScrollBar.Position;
 If Pos > RSZ - WW * HH * 2 then
  Pos := RSZ - WW * HH * 2;
 DontChange := True;
 ScrollBar.Position := Pos;
 DontChange := False;
 If not CanChange then PosEdit.Text := 'h' + IntToHex(Pos, 8);
 DrawRom;
end;

procedure TMainForm.WidthEditChange(Sender: TObject);
Var W, H, S: Integer;
begin
 If IntCheckOut(WidthEdit.Text) then W := WidthEdit.Value Else W := 1;
 If IntCheckOut(HeightEdit.Text) then H := HeightEdit.Value Else H := 1;
 If IntCheckOut(SizeEdit.Text) then S := SizeEdit.Value Else S := 0;
 Image.Picture.Bitmap.Width := W * (S + 1);
 Image.Picture.Bitmap.Height := H * (S + 1);
 InitScroll(W, H, S + 1);
 DrawRom;
end;

procedure TMainForm.HeightEditChange(Sender: TObject);
Var W, H, S: Integer;
begin
 If IntCheckOut(WidthEdit.Text) then W := WidthEdit.Value Else W := 1;
 If IntCheckOut(HeightEdit.Text) then H := HeightEdit.Value Else H := 1;
 If IntCheckOut(SizeEdit.Text) then S := SizeEdit.Value Else S := 0;
 Image.Picture.Bitmap.Width := W * (S + 1);
 Image.Picture.Bitmap.Height := H * (S + 1);
 InitScroll(W, H, S + 1);
 DrawRom;
end;

procedure TMainForm.LeftButtonClick(Sender: TObject);
begin
 If Pos > 0 then
 begin
  If Pos >= WW * 2 then Dec(Pos, WW * 2) Else Pos := 0;
  ScrollBar.Position := Pos;
  DrawRom;
 end;
end;

procedure TMainForm.RightButtonClick(Sender: TObject);
begin
 Inc(Pos, WW * 2);
 If Pos > RSZ then Pos := RSZ;
 ScrollBar.Position := Pos;
 DrawRom;
end;

procedure TMainForm.PosEditKeyPress(Sender: TObject; var Key: Char);
begin
 If not (UpCase(Key) in ['0'..'9', 'A'..'F', 'H', #8]) then Key := #0;
 CanChange := True;
end;

procedure TMainForm.PosEditChange(Sender: TObject);
begin
 If not CanChange then Exit;
 If UnsCheckOut(PosEdit.Text) then
  ScrollBar.Position := GetLDW(PosEdit.Text);
 CanChange := False;
end;

procedure TMainForm.AddItemClick(Sender: TObject);
Var B: PBookmark; S: String;
begin
 If NamesDialog.Execute then
 begin
  S := NamesDialog.Edit1.Text;
  B := Bookmarks.Root;
  While B <> NIL do With B^ do
  begin
   If Name = S then
   begin
    Position := Pos;
    Width := WW;
    Height := HH;
    Size := SS;
    Exit;
   end;
   B := Next;
  end;
  With Bookmarks.Add^ do
  begin
   Name := S;
   Position := Pos;
   Width := WW;
   Height := HH;
   Size := SS;
   ListBox.Items.Add(Name);
  end;
 end;
end;

Procedure TBookmarks.Remove(N: PBookmark);
Var P, O: PBookmark;
begin
 If N = NIL then Exit;
 P := NIL; O := Root;
 While O <> NIL do
 begin
  If O = N then
  begin
   If P = NIL then Root := O^.Next Else P^.Next := O^.Next;
   If O^.Next = NIL then Cur := P;
   O^.Name := '';
   Dispose(O);
   Dec(Count);
   Break;
  end;
  P := O;
  O := O^.Next;
 end;
end;

procedure TMainForm.RemoveItemClick(Sender: TObject);
Var B: PBookmark; S: String;
begin
 With ListBox do
 begin
  If ItemIndex < 0 then Exit;
  If Items.Count = 0 then Exit;
  S := Items.Strings[ItemIndex];
  B := Bookmarks.Root;
  While B <> NIL do With B^ do
  begin
   If S = Name then
   begin
    Bookmarks.Remove(B);
    Items.Delete(ItemIndex);
    Exit;
   end;
   B := Next;
  end;
 end;
end;

procedure TMainForm.OpenBookmarksItemClick(Sender: TObject);
Var F: TextFile;
begin
 If OpenBmkDialog.Execute then
 begin
  Bookmarks.Free;
  Bookmarks := TBookMarks.Create;
  AssignFile(F, OpenBmkDialog.FileName);
  Reset(F);
  ListBox.Clear;
  While not Eof(F) do With Bookmarks.Add^ do
  begin
   Readln(F, Name);
   Readln(F, Position);
   Readln(F, Width);
   Readln(F, Height);
   Readln(F, Size);
   ListBox.Items.Add(Name);
  end;
  CloseFile(F);
 end;
end;

procedure TMainForm.SaveBookmarksItemClick(Sender: TObject);
Var F: TextFile; B: PBookmark;
begin
 If SaveBmkDialog.Execute then
 begin
  AssignFile(F, SaveBmkDialog.FileName);
  Rewrite(F);
  B := Bookmarks.Root;
  While B <> NIL do With B^ do
  begin
   Writeln(F, Name);
   Writeln(F, Position);
   Writeln(F, Width);
   Writeln(F, Height);
   Writeln(F, Size);
   B := Next;
  end;
  CloseFile(F);
 end;
end;

procedure TMainForm.ListBoxClick(Sender: TObject);
Var S: String; B: PBookmark;
begin
 With ListBox do
 begin
  If ItemIndex < 0 then Exit;
  If Items.Count = 0 then Exit;
  S := Items.Strings[ItemIndex];
  B := Bookmarks.Root;
  While B <> NIL do With B^ do
  begin
   If S = Name then
   begin
    WidthEdit.Value := Width;
    HeightEdit.Value := Height;
    SizeEdit.Value := Size;
    ScrollBar.Position := Position;
    Exit;
   end;
   B := Next;
  end;
 end;
end;

procedure TMainForm.SizeEditChange(Sender: TObject);
Var W, H, S: Integer;
begin
 If IntCheckOut(WidthEdit.Text) then W := WidthEdit.Value Else W := 1;
 If IntCheckOut(HeightEdit.Text) then H := HeightEdit.Value Else H := 1;
 If IntCheckOut(SizeEdit.Text) then S := SizeEdit.Value Else S := 0;
 Image.Picture.Bitmap.Width := W * (S + 1);
 Image.Picture.Bitmap.Height := H * (S + 1);
 InitScroll(W, H, S + 1);
 DrawRom;
end;

procedure TMainForm.ImageScrollVerticalChange(Sender: TObject);
begin
 DrawRom;
end;

procedure TMainForm.ImageScrollHorisontalChange(Sender: TObject);
begin
 DrawRom;
end;

procedure TMainForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
 If NewWidth < MW then  NewWidth := MW;
 If NewHeight < MH then NewHeight := MH;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
 ImageScrollVertical.Left := Width - ISV;
 ScrollBar.Left := Width - SB;
 WidthEdit.Left := Width - WE;
 WidthLabel.Left := WidthEdit.Left - 40;
 HeightEdit.Left := Width - HE;
 HeightLabel.Left := HeightEdit.Left - 40;
 SizeEdit.Left := Width - SE;
 SizeLabel.Left := SizeEdit.Left - 32;
 PosEdit.Left := Width - PE;
 PositionLabel.Left := PosEdit.Left - 48;
 LeftButton.Left := Width - LB;
 RightButton.Left := Width - RB;
 AddButton.Left := Width - AB;
 DeleteButton.Left := Width - DB;
 ListBox.Left := Width - LIB;
 ImageScrollHorisontal.Top := Height - ISH;
 Image.Width := Width - IMW;
 Image.Height := Height - IMH;
 ListBox.Height := Height - LBH;
 ImageScrollHorisontal.Width := Image.Width;
 ImageScrollVertical.Height := Image.Height;
 ScrollBar.Height := Image.Height;
 DrawRom;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 If (RSZ > 0) and not Saved then
  Case MessageDlg('Save?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
   mrYes:
   begin
    CanClose := True;
    SaveItemClick(Sender);
   end;
   mrNo: CanClose := True;
   mrCancel: CanClose := False;
  end;
end;

procedure TMainForm.SaveItemClick(Sender: TObject);
Var F: File;
begin
 AssignFile(F, FName);
 Rewrite(F, 1);
 BlockWrite(F, ROM[0], RSZ);
 CloseFile(F);
 Saved := True;
 EnableAll;
end;

procedure TMainForm.SaveAsItemClick(Sender: TObject);
Var F: File;
begin
 If RSZ = 0 then Exit;
 If SaveDialog.Execute then
 begin
  FName := SaveDialog.FileName;
  AssignFile(F, FName);
  Rewrite(F, 1);
  BlockWrite(F, ROM[0], RSZ);
  CloseFile(F);
  Saved := True;
  EnableAll;
 end;
end;

procedure TMainForm.SaveBitmapItemClick(Sender: TObject);
begin
 If SaveBmpDialog.Execute then
  DIB.SaveToFile(SaveBmpDialog.FileName);
end;

procedure TMainForm.LoadBitmapItemClick(Sender: TObject);
Var Bmp: TBitmap; W, H, X, Y: Integer; RD: ^Byte;
begin
 If OpenBmpDialog.Execute then
 begin
  Bmp := TBitmap.Create;
  Bmp.LoadFromFile(OpenBmpDialog.FileName);
  Bmp.PixelFormat := pf15bit;
  W := DIB.Width;
  H := DIB.Height;
  Bmp.Width := W;
  Bmp.Height := H;
  DIB.Canvas.Draw(0, 0, Bmp);
  Bmp.Free;
  With DIB do For Y := 0 to H - 1 do
   Move(ScanLine[Y]^, ROM[Pos + Y * (W shl 1)], W shl 1);
{  Bmp := TBitmap.Create;
  Bmp.LoadFromFile(OpenBmpDialog.FileName);
  W := WidthEdit.Value;
  H := HeightEdit.Value;
  With Bmp.Canvas do
  begin
   RD := Addr(ROM^[Pos]);
   For Y := 0 to H - 1 do
    For X := 0 to W - 1 do
    begin
     RD^ := GetCol(Pixels[X, Y]);
     Inc(DWord(RD));
    end;
  end;
  Bmp.Free;      }
  Saved := False;
  DrawRom;
  EnableAll;
 end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 DIB.Free;
 Bookmarks.Free;
 SetLength(ROM, 0);
 RSZ := 0;
end;

end.
