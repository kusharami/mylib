unit namer;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs;

type
  TNamesDialog = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    Bevel: TBevel;
    Edit1: TEdit;
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    Function Execute: Boolean;
  end;

Var
 NamesDialog: TNamesDialog;
 OkPressed: Boolean;

implementation

{$R *.dfm}

Function TNamesDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TNamesDialog.FormShow(Sender: TObject);
begin
 Edit1.SetFocus;
 OkPressed := False;
end;

procedure TNamesDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TNamesDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := False;
 If OkPressed then
 begin
  OkPressed := False;
  If Edit1.Text <> '' then CanClose := True Else
   ShowMessage('Input name!');
 end Else CanClose := True;
end;

end.
