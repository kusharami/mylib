program setfilesize;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes;

Var FileStream: TFileStream;

begin
 FileStream := TFileStream.Create(ParamStr(1), fmOpenReadWrite);
 FileStream.Size := StrToInt(ParamStr(2));
 FileStream.Free;
end.
