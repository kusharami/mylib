unit StringConsts;

interface

resourcestring
 SMUIndexOutOfBounds = '����� ��� ��������� %d..%d.';
 SMUIntegerError = '�������� �������� ��������.';
 SMUHexError = '�������� ���������������� ��������.';
 SMUInvalidTableItem = '�������� ������ �������.';
 SMUTerminateStringAbsence = '����������� ������ ����� ������.';
 SMUFileNotFound = '���� "%s" �� ������.';
 SMUFileNotFoundLoadOther = '���� "%s" �� ������. ��������� ������?';
 SMUFileNotFoundLoadOtherX = '���� "%s" �� ������. ��������� ������.';
 SMUSYNS_ScrollInfoFmtTop = '������� ������: %d';
 SMUFileNotFoundInProject = '���� "%s" �� ������ � ����� �������.';
 SMULibraryNotFound = '���������� "%s" �� �������.';
 SMUSuchTableIsExist = '������� � ����� ������ ��� ����������.';
 SMUSuchListIsExist = '������ � ����� ������ ��� ����������.';
 SMUSuchGroupIsExist = '������ � ����� ������ ��� ����������.';
 SMUInvalidString = '�������� ����. ������ "[HexCode]-[HexCode]".';
 SMUInvalidHexadecimalValue = '"%s" - �������� ���������������� ��������.';
 SMUInvalidIntegerValue = '"%s" - �������� �������� ��������.';
 SMUTableExistIn = '��� ������� ������������ �';
 SMUStringNotFound = '������ "%s" ��� � ������.';
 SMUTableNotSelected = '������� �� �������.';
 SMUStringTerminateCodeNotExist = '� ������� "%s" ��� ������� ��������� ������.';
 SMUPointerIsExist = '������� �� ������ %x ��� ����������.';
 SMUStringIsExist = '������ �� ������ %x ��� ����������.';
 SMUUnableToLoad = '�� ������� ��������� ���� "%s".';
 SMUUnableToSave = '�� ������� ��������� ���� "%s".';
 SMUTextStringNotFound = '������ �� �������.';
 SMUInsufficientSpaceForText = '���� ����� �� ���������.';

 SMUDeleteTableConfirmation = '�� ���������� ������� ����� ��������. �������?';
 SMUDeleteListConfirmation = '�� ���������� ������ ����� ��������. �������?';
 SMUDeleteGroupConfirmation = '�� ���������� ������ ����� ��������. �������?';
 SMUReloadConfirmation = '������ "%s" ��� ��������. �������������?' + #13#10 +
                                '��� ���������� ������ ����� ��������!';
 SMUSaveBeforeClosingConfirmation = '��������� ������ "%s" ����� ���������?';
 SMUReplaceConfirmation = '��������?';
 SAPFirstAddressLessThenZero = '������ ����� ������ ����.';
 SAPSecondAddressLessThenZero = '������ ����� ������ ����.';
 SAPSecondAddressLessThenFirst = '������ ����� ������ �������.';
 SPDCloseConfirmation = '��� ���������� ������ ����� ��������. �������?';
 SRTInputSearchText = '������� ������ ��� ������.';

 SPDValueIsExist = '���������� � ����� ������ ��� ����������.';
 SPDBytesOccupied = '���� ������: %u';
 SPDZeroInterval = '�������� ����� ����.';
 SDDNoDescription = '��� ��������';
 SLBLineNumber = '������ %u �� %u';
 SSBInsert = '�������';
 SSBOwerwrite = '������';
 SMUOneWord = '1 �����';
 SMUWordsCount1 = '%u �����';
 SMUWordsCount2 = '%u ����';
 SMUWordsCount3 = '%u �����';
 SMUWordsFounded = '���� �������: %u';
 SMUWordSearching = '����� �����';
 SMUWordIterationSearching = '����� ��������';
 SMUAddTextBlock = '�������� ���� ������';
 SMUFirstCharacterAddress = '����� ������� �������';
 SMULastCharacterAddress = '����� ���������� �������';
 SMUAddPointersBlock = '�������� ���� ���������';
 SMUFirstPointerAddress = '����� ������� ��������';
 SMULastPointerAddress = '����� ���������� ��������';
 SMUNone = '[���]';
 SMUNoName = '��� ����� %u';
 SMUTableName = '������� �%u';
 SMUListName = '������ �%u';
 SMUGroupName = '������ �%u';
 SMUPointersCount = '���������� ���������';
 SMURomsFilter = '��� ������� (*.gba;*.gb;*.gbc;*.sgb;*.smd;*.bin;*.smc;*.fig;*.nes;*.sms;*.gg)|' +
 '*.gba;*.gb;*.gbc;*.smd;*.bin;*.smc;*.fig;*.nes;*.sms;*.gg|' +
 'GameBoy Advance (*.gba)|*.gba|GameBoy (*.gb;*.gbc;*.sgb)|*.gb;*.gbc;*.sgb|' +
 'Super Nintendo (*.smc;*.fig)|*.smc;*.fig|Nintendo (*.nes)|*.nes|' +
 'Sega Genesis (*.smd;*.bin)|*.smd;*.bin|Sega Master System (*.sms)|*.sms|' +
 'Sega Game Gear (*.gg)|*.gg|��� ����� (*.*)|*.*';
 SMUExeFilter = '���������� (*.exe)|*.exe';
 SESHint = '�����';
 SESWarning = '��������������';
 SESError = '������';
 SESFatalError = '����������� ������';
 STVTables = '�������';
 STVGroups = '������';
 STVTerminateCodes = '���� ��������� ������';
 STVCRLFCodes = '���� ������� ������';
 STVCharacters = '�������';
 STVSpaceForText = '����� ��� ������';
 STVPointers = '��������';

implementation

end.
