object GotoDialog: TGotoDialog
  Left = 417
  Top = 308
  BorderStyle = bsDialog
  Caption = 'Options'
  ClientHeight = 200
  ClientWidth = 249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 235
    Height = 153
    Shape = bsFrame
  end
  object OkButton: TButton
    Left = 8
    Top = 168
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelButton: TButton
    Left = 168
    Top = 168
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object GotoEdit: TLabeledEdit
    Left = 16
    Top = 32
    Width = 217
    Height = 21
    EditLabel.Width = 29
    EditLabel.Height = 13
    EditLabel.Caption = 'Go to:'
    TabOrder = 2
  end
  object WidthEdit: TLabeledEdit
    Left = 16
    Top = 72
    Width = 217
    Height = 21
    EditLabel.Width = 31
    EditLabel.Height = 13
    EditLabel.Caption = 'Width:'
    TabOrder = 3
  end
  object HeightEdit: TLabeledEdit
    Left = 16
    Top = 112
    Width = 217
    Height = 21
    EditLabel.Width = 34
    EditLabel.Height = 13
    EditLabel.Caption = 'Height:'
    TabOrder = 4
  end
  object Radio4: TRadioButton
    Left = 56
    Top = 136
    Width = 49
    Height = 17
    Caption = '4 bpp'
    TabOrder = 5
  end
  object Radio8: TRadioButton
    Left = 128
    Top = 136
    Width = 49
    Height = 17
    Caption = '8 bpp'
    TabOrder = 6
  end
end
