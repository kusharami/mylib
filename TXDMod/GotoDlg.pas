unit GotoDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, HexUnit;

type
  TGotoDialog = class(TForm)
    Bevel: TBevel;
    OkButton: TButton;
    CancelButton: TButton;
    GotoEdit: TLabeledEdit;
    WidthEdit: TLabeledEdit;
    HeightEdit: TLabeledEdit;
    Radio4: TRadioButton;
    Radio8: TRadioButton;
    procedure FormShow(Sender: TObject);
  private
    function GetPos: Integer;
    function GetWidth: Integer;
    function GetHeight: Integer;
    function GetBpp: Integer;
  public
    function Execute(P, W, H, B: Integer): Boolean;
    property Position: Integer read GetPos;
    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;
    property BitCount: Integer read GetBpp;
  end;

var
  GotoDialog: TGotoDialog;

implementation

{$R *.dfm}

function TGotoDialog.Execute(P, W, H, B: Integer): Boolean;
begin
 GotoEdit.Text := 'h' + IntToHex(P, 1);
 WidthEdit.Text := IntToStr(W);
 HeightEdit.Text := IntToStr(H);
 If B = 4 then
  Radio4.Checked := True Else
  Radio8.Checked := True;
 Result := (ShowModal = mrOk);
end;

function TGotoDialog.GetBpp: Integer;
begin
 If Radio4.Checked then
  Result := 4 Else
  Result := 8;  
end;

function TGotoDialog.GetHeight: Integer;
Var S: String;
begin
 S := HeightEdit.Text;
 If HexCheckOut(S) then
  Result := DecHexToInt(S) Else
  Result := -1;
end;

function TGotoDialog.GetPos: Integer;
Var S: String;
begin
 S := GotoEdit.Text;
 If HexCheckOut(S) then
  Result := DecHexToInt(S) Else
  Result := -1;
end;

function TGotoDialog.GetWidth: Integer;
Var S: String;
begin
 S := WidthEdit.Text;
 If HexCheckOut(S) then
  Result := DecHexToInt(S) Else
  Result := -1;
end;

procedure TGotoDialog.FormShow(Sender: TObject);
begin
 GotoEdit.SetFocus;
 GotoEdit.SelectAll;
end;

end.
