unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, XPMan, StdCtrls, ExtCtrls, DIB, HexUnit, ExtDlgs, ComCtrls,
  ToolWin, VirtualTrees, ActnList, ImgList, StdActns, Menus;

type
 TBookmarkNode = Class;
  TMainForm = class(TForm)
    Image: TImage;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    OpenActDialog: TOpenDialog;
    OpenBmpDialog: TOpenPictureDialog;
    SaveBmpDialog: TSavePictureDialog;
    StatusBar: TStatusBar;
    Tree: TVirtualStringTree;
    Panel: TPanel;
    ToolBar: TToolBar;
    ImageList: TImageList;
    ActionList: TActionList;
    AddAction: TAction;
    OpenAction: TAction;
    SaveAction: TAction;
    MoveUpAction: TAction;
    MoveDownAction: TAction;
    RemoveAction: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ClearAction: TAction;
    ToolButton7: TToolButton;
    UpdateAction: TAction;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    OpenBmkDialog: TOpenDialog;
    SaveBmkDialog: TSaveDialog;
    OpenBMP: TAction;
    SaveBMP: TAction;
    GotoAction: TAction;
    GetPalette: TAction;
    MainMenu: TMainMenu;
    File1: TMenuItem;
    Edit1: TMenuItem;
    OpenTXDAction: TAction;
    SaveTXDAction: TAction;
    OpenActAction: TAction;
    FileExitAction: TFileExit;
    Exit1: TMenuItem;
    N1: TMenuItem;
    OpenTXD1: TMenuItem;
    SaveTXD1: TMenuItem;
    OpenACT1: TMenuItem;
    Getpalette1: TMenuItem;
    N2: TMenuItem;
    OpenBMP1: TMenuItem;
    SaveBMP1: TMenuItem;
    N3: TMenuItem;
    Goto1: TMenuItem;
    N4: TMenuItem;
    OpenTM21: TMenuItem;
    OpenTM2Action: TAction;
    OpenTM2dialog: TOpenDialog;
    procedure OpenButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure OpenActButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GotoButtonClick(Sender: TObject);
    procedure SaveBmpButtonClick(Sender: TObject);
    procedure OpenBmpButtonClick(Sender: TObject);
    procedure AddActionExecute(Sender: TObject);
    procedure OpenActionExecute(Sender: TObject);
    procedure SaveActionExecute(Sender: TObject);
    procedure MoveUpActionExecute(Sender: TObject);
    procedure MoveUpActionUpdate(Sender: TObject);
    procedure MoveDownActionExecute(Sender: TObject);
    procedure MoveDownActionUpdate(Sender: TObject);
    procedure RemoveActionExecute(Sender: TObject);
    procedure RemoveActionUpdate(Sender: TObject);
    procedure TreeChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure TreeFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure TreeGetImageIndex(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
      var Ghosted: Boolean; var ImageIndex: Integer);
    procedure TreeGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure TreeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ClearActionExecute(Sender: TObject);
    procedure TreeNewText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; NewText: WideString);
    procedure TreeDblClick(Sender: TObject);
    procedure ClearActionUpdate(Sender: TObject);
    procedure UpdateActionExecute(Sender: TObject);
    procedure UpdateActionUpdate(Sender: TObject);
    procedure GetPalButtonClick(Sender: TObject);
    procedure OpenTM2ActionExecute(Sender: TObject);
  private
    { Private declarations }
  public
    procedure DrawDIB;
    procedure UpdateSize(W, H, B: Integer);
    function AddItem(Name: String; P, W, H, BC: Integer;
    Const CT: TRGBQuads; Silent: Boolean = False): TBookmarkNode;
  end;
 TBookmarkNode = Class
 private
  FName: String;
  FPosition: Integer;
  FWidth: Integer;
  FHeight: Integer;
  FBitCount: Integer;
  FColorTable: TRGBQuads;
 public
  constructor Create(Name: String; P, W, H, BC: Integer);
  destructor Destroy; override;
  property Name: String read FName write FName;
  property Position: Integer read FPosition write FPosition;
  property Width: Integer read FWidth write FWidth;
  property Height: Integer read FHeight write FHeight;
  property BitCount: Integer read FBitCount write FBitCount;
  property ColorTable: TRGBQuads read FColorTable write FColorTable;  
 end;
 PRTreeData = ^RTreeData;
 RTreeData = Record
  BookmarkNode: TBookmarkNode;
 end;
Const
 TIM2Sign = $324D4954;
Type
 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;
 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;

var
  MainForm: TMainForm; DIB: TDIB;
  RAW: TMemoryStream;
  RawPosition: Integer = 0;
  WW, HH, BPP: Integer;

implementation

uses GotoDlg;

{$R *.dfm}

Function GetR(V: Word): Byte;
begin
 Result := (V and $1F) shl 3;
end;

Function GetG(V: Word): Byte;
begin
 Result := ((V and $3FF) shr 5) shl 3;
end;

Function GetB(V: Word): Byte;
begin
 Result := ((V and $7FFF) shr 10) shl 3;
end;

constructor TBookmarkNode.Create(Name: String; P, W, H, BC: Integer);
begin
 FName := Name;
 FPosition := P;
 FWidth := W;
 FHeight := H;
 FBitCount := BC;
end;

destructor TBookmarkNode.Destroy;
begin
 FName := '';
 inherited Destroy;
end;

procedure TMainForm.OpenButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
 begin
  RAW.LoadFromFile(OpenDialog.FileName);
  RAW.Position := RawPosition;
  DrawDIB;
 end;
end;

procedure TMainForm.SaveButtonClick(Sender: TObject);
begin
 SaveDialog.FileName := OpenDialog.FileName;
 If SaveDialog.Execute then
 begin
  RAW.SaveToFile(SaveDialog.FileName);
  OpenBmkDialog.FileName := SaveBmkDialog.FileName;  
 end;
end;

procedure TMainForm.OpenActButtonClick(Sender: TObject);
Type
 TRGB = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
 end;
Var Pal: Array[0..255] of TRGB; F: File; I: Integer;
begin
 If OpenActDialog.Execute then
 begin
  AssignFile(F, OpenActDialog.FileName);
  Reset(F, 1);
  BlockRead(F, Pal, SizeOf(Pal), I);
  CloseFile(F);
  FillChar(DIB.ColorTable, 1024, 0);
  With DIB do For I := 0 to 255 do With ColorTable[I], Pal[I] do
  begin
   rgbRed := R;
   rgbGreen := G;
   rgbBlue := B;
   rgbReserved := 0;
  end;
  DIB.UpdatePalette;
  Image.Picture.Graphic := DIB;
 end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
 Tree.NodeDataSize := SizeOf(RTreeData);
 UpdateSize(WW, HH, BPP);
 Image.Picture.Graphic := DIB;
end;

Type
 PBytes = ^TBytes;
 TBytes = Array[Byte] of Byte;
Const
 InterlaceMatrix: Array[0..7] of Byte =
 ($00, $10, $02, $12, $11, $01, $13, $03);
 Matrix: Array[0..3] of Integer = (0, 1, -1, 0);
 TileMatrix: Array[0..1] of Integer = (4, -4);

procedure TMainForm.DrawDIB;
Var
 Mem: Pointer; B, S: ^Byte; I, X, Y, XX, YY: Integer; BB: PBytes;
 IM: TDIB;
begin
 GetMem(Mem, WW * HH);
 If BPP = 4 then
 begin
  FillChar(Mem^, (WW shr 1) * HH, 0);
  RAW.Position := RawPosition;
  RAW.Read(Mem^, (WW shr 1) * HH);
  BB := Addr(Mem^);
  With DIB do
  begin
   Fill8bit(0);
   IM := TDIB.Create;
   IM.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
   IM.BitCount := 4;
   IM.Width := WW;
   IM.Height := HH;
   IM.ColorTable := ColorTable;
   IM.UpdatePalette;
   B := Addr(BB^[0]);
   For Y := 0 to HH - 1 do
   begin
    S := Addr(IM.ScanLine[Y]^);
    Move(B^, S^, WW shr 1);
    Inc(B, WW shr 1);
   end;
   B := Addr(BB^[0]);
   For Y := 0 to HH - 1 do
   begin
    S := Addr(IM.ScanLine[Y]^);
    For X := 0 to WW shr 1 - 1 do
    begin
     B^ := S^ and 15;
     Inc(B);
     B^ := S^ shr 4;
     Inc(B);
     Inc(S);
    end;
   end;
   IM.Free;
  end;
 end Else
 begin
  FillChar(Mem^, WW * HH, 0);
  RAW.Position := RawPosition;
  RAW.Read(Mem^, WW * HH);
  BB := Addr(Mem^);
  With DIB do
  begin
   Fill8bit(0);
   IM := TDIB.Create;
   IM.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
   IM.BitCount := 8;
   IM.Width := WW;
   IM.Height := HH;
   IM.ColorTable := ColorTable;
   IM.UpdatePalette;
   B := Addr(BB^[0]);
   For Y := 0 to HH - 1 do
   begin
    S := Addr(IM.ScanLine[Y]^);
    Move(B^, S^, WW);
    Inc(B, WW);
   end;
   B := Addr(BB^[0]);
   For Y := 0 to HH - 1 do
   begin
    S := Addr(IM.ScanLine[Y]^);
    For X := 0 to WW - 1 do
    begin
     B^ := S^;
     Inc(B);
     Inc(S);
    end;
   end;
   IM.Free;
  end;
 end;
 With DIB do For Y := 0 to Height - 1 do
 begin
  If Odd(Y) then For X := 0 to Width - 1 do
  begin
   XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
   YY := Y + Matrix[Y mod 4];
   I := InterlaceMatrix[(X div 4) mod 4 + 4] +
  (X * 4) mod 16 + (X div 16) * 32 + ((Y - 1) * Width);
   Pixels[XX, YY] := BB^[I];
  end Else
  For X := 0 to Width - 1 do
  begin
   XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
   YY := Y + Matrix[Y mod 4];
   I := InterlaceMatrix[(X div 4) mod 4] +
   (X * 4) mod 16 + (X div 16) * 32 + (Y * Width);
   Pixels[XX, YY] := BB^[I];
  end;
 end;
 Image.Picture.Graphic := DIB;
 FreeMem(Mem);
end;

procedure TMainForm.GotoButtonClick(Sender: TObject);
begin
 With GotoDialog do If Execute(RawPosition, WW, HH, BPP) then
 begin
  RawPosition := Position;
  WW := Width;
  HH := Height;
  BPP := BitCount;
  DIB.Width := WW;
  DIB.Height := HH;
  UpdateSize(WW, HH, BPP);
  DrawDIB;
 end;
end;

procedure TMainForm.SaveBmpButtonClick(Sender: TObject);
Var Node: PVirtualNode;
begin
 Node := Tree.GetFirstSelected;
 If Node <> NIL then
 With PRTreeData(Tree.GetNodeData(Node))^ do
 If BookmarkNode <> NIL then With BookmarkNode do
  SaveBmpDialog.FileName := Name + '.bmp';
 If SaveBmpDialog.Execute then
  DIB.SaveToFile(SaveBmpDialog.FileName);
end;

procedure TMainForm.OpenBmpButtonClick(Sender: TObject);
Var
 Temp: TRGBQuads; Node: PVirtualNode;
 Bitmap: TBitmap; BB: PBytes; S: ^Byte; X, Y, XX, YY, I: Integer; B: Byte;
begin
 Node := Tree.GetFirstSelected;
 If Node <> NIL then
 With PRTreeData(Tree.GetNodeData(Node))^ do
 If BookmarkNode <> NIL then With BookmarkNode do
  OpenBmpDialog.FileName := Name + '.bmp';
 If OpenBmpDialog.Execute then
 begin
  Bitmap := TBitmap.Create;
  Bitmap.LoadFromFile(OpenBmpDialog.FileName);
  If BPP = 4 then
  begin
   Temp := DIB.ColorTable;
   With DIB do For I := 16 to 255 do
    ColorTable[I] := ColorTable[0];
   DIB.UpdatePalette;
  end;
  DIB.Fill8bit(0);
  With DIB.Canvas do StretchDraw(ClipRect, Bitmap);
  If BPP = 4 then
  begin
   DIB.ColorTable := Temp;
   DIB.UpdatePalette;
  end;
  Bitmap.Free;

  GetMem(BB, WW * HH);
  With DIB do For Y := 0 to Height - 1 do
  begin
   If Odd(Y) then For X := 0 to Width - 1 do
   begin
    XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
    YY := Y + Matrix[Y mod 4];
    I := InterlaceMatrix[(X div 4) mod 4 + 4] +
   (X * 4) mod 16 + (X div 16) * 32 + ((Y - 1) * Width);
    BB^[I] := Pixels[XX, YY];
   end Else
   For X := 0 to Width - 1 do
   begin
    XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
    YY := Y + Matrix[Y mod 4];
    I := InterlaceMatrix[(X div 4) mod 4] +
    (X * 4) mod 16 + (X div 16) * 32 + (Y * Width);
    BB^[I] := Pixels[XX, YY];
   end;
  end;
  RAW.Position := RawPosition;
  S := Addr(BB^[0]);
  If BPP = 4 then For Y := 0 to HH - 1 do
  For X := 0 to WW shr 1 - 1 do
  begin
   B := S^;
   Inc(S);
   B := B or (S^ shl 4);
   Inc(S);
   RAW.Write(B, 1);
  end Else For Y := 0 to HH - 1 do
  begin
   Raw.Write(S^, WW);
   Inc(S, WW);
  end;
  FreeMem(BB);
  DrawDIB;
 end;
end;

procedure TMainForm.UpdateSize(W, H, B: Integer);
begin
 If (W < 1) or (H < 1) or (B < 1) then
 begin
  StatusBar.Panels.Items[0].Text := '';
  StatusBar.Panels.Items[1].Text := '';
  StatusBar.Panels.Items[2].Text := '';
 end Else
 begin
  StatusBar.Panels.Items[0].Text := Format('Width: %d', [W]);
  StatusBar.Panels.Items[1].Text := Format('Height: %d', [H]);
  StatusBar.Panels.Items[2].Text := Format('%D bpp', [B]);
 end;
end;

procedure TMainForm.AddActionExecute(Sender: TObject);
begin
 AddItem('Bookmark', RawPosition, WW, HH, BPP, DIB.ColorTable);
 Tree.SetFocus;
end;

Const
 BMLSign: Cardinal = $4C4D4200;

procedure TMainForm.OpenActionExecute(Sender: TObject);
Var
 FileStream: TFileStream; I, J, C, P, W, H, B: Integer; CT: TRGBQuads;
 Node: PVirtualNode; S: String;
begin
 If OpenBmkDialog.Execute then With Tree do
 begin
  try
   FileStream := TFileStream.Create(OpenBmkDialog.FileName, fmOpenRead);
   FileStream.Read(J, 4);
   FileStream.Read(C, 4);
   If (Cardinal(J) = BMLSign) and (C >= 0) and (C <= 10000) then
   begin
    Tree.Clear;
    For I := 0 to C - 1 do
    begin
     FileStream.Read(J, 4);
     If J < 0 then J := 0;
     SetLength(S, J);
     If J > 0 then FileStream.Read(S[1], J);
     FileStream.Read(P, 4);
     FileStream.Read(W, 4);
     FileStream.Read(H, 4);
     FileStream.Read(B, 4);
     FileStream.Read(CT, SizeOf(TRGBQuads));          
     AddItem(S, P, W, H, B, CT, True);
    end;
    Node := GetFirst;
    If Node <> NIL then
    begin
     Selected[Node] := True;
     FocusedNode := Node;
    end;
   end Else ShowMessage('Invalid file format.');
   FileStream.Free;
  except
   on E: Exception do ShowMessage(Format('Cannot open "%s"', [OpenBmkDialog.FileName]));
  end;
 end;
end;

procedure TMainForm.SaveActionExecute(Sender: TObject);
Var FileStream: TFileStream; J: Integer; Node: PVirtualNode;
begin
 SaveBmkDialog.FileName := OpenBmkDialog.FileName;
 If SaveBmkDialog.Execute then With Tree do
 begin
  OpenBmkDialog.FileName := SaveBmkDialog.FileName;
  try
   FileStream := TFileStream.Create(SaveBmkDialog.FileName, fmCreate);
   J := RootNodeCount;
   FileStream.Write(BMLSign, 4);
   FileStream.Write(J, 4);
   Node := GetFirst;
   try
    While Node <> NIL do
    begin
     With PRTreeData(GetNodeData(Node))^ do
     If BookmarkNode <> NIL then With BookmarkNode do
     begin
      J := Length(FName);
      FileStream.Write(J, 4);
      If J > 0 then FileStream.Write(FName[1], J);
      FileStream.Write(FPosition, 4);
      FileStream.Write(FWidth, 4);
      FileStream.Write(FHeight, 4);
      FileStream.Write(FBitCount, 4);
      FileStream.Write(FColorTable, SizeOf(TRGBQuads));
     end;
     Node := Node^.NextSibling;
    end;
   except
    on E: Exception do ShowMessage('Error saving file.');
   end;
   FileStream.Free;
  except
   on E: Exception do ShowMessage(Format('Cannot save "%s"', [SaveBmkDialog.FileName]));
  end;
 end;
end;

procedure TMainForm.MoveUpActionExecute(Sender: TObject);
Var N1, N2: PVirtualNode;
begin
 N1 := Tree.GetFirstSelected;
 If N1 = NIL then Exit;
 N2 := Tree.GetPrevious(N1);
 If N2 <> NIL then Tree.MoveTo(N1, N2, amInsertBefore, False);
end;

procedure TMainForm.MoveUpActionUpdate(Sender: TObject);
begin
 With Tree do
  MoveUpAction.Enabled := (RootNodeCount > 0) and not Selected[GetFirst];
end;

procedure TMainForm.MoveDownActionExecute(Sender: TObject);
Var N1, N2: PVirtualNode;
begin
 N1 := Tree.GetFirstSelected;
 If N1 = NIL then Exit;
 N2 := Tree.GetNext(N1);
 If N2 <> NIL then Tree.MoveTo(N1, N2, amInsertAfter, False);
end;

procedure TMainForm.MoveDownActionUpdate(Sender: TObject);
begin
 With Tree do
  MoveDownAction.Enabled := (RootNodeCount > 0) and not Selected[GetLast];
end;

procedure TMainForm.RemoveActionExecute(Sender: TObject);
Var Node, NextNode: PVirtualNode;
begin
 Node := Tree.GetFirstSelected;
 NextNode := Tree.GetNext(Node);
 If NextNode = NIL then NextNode := Tree.GetPrevious(Node);
 If Node <> NIL then Tree.DeleteNode(Node);
 TreeChange(NIL, NIL);
 If NextNode <> NIL then
 begin
  Tree.Selected[NextNode] := True;
  Tree.FocusedNode := NextNode;
 end;
end;

procedure TMainForm.RemoveActionUpdate(Sender: TObject);
begin
 RemoveAction.Enabled := Tree.RootNodeCount > 0;
end;

Var CurNode: PVirtualNode = NIL;

procedure TMainForm.TreeChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
 CurNode := Node;
 If Node <> NIL then
 begin
  With PRTreeData(Tree.GetNodeData(Node))^ do
  If BookmarkNode <> NIL then With BookmarkNode do
  begin
   RawPosition := Position;
   WW := Width;
   HH := Height;
   BPP := BitCount;
   DIB.Width := WW;
   DIB.Height := HH;
   DIB.ColorTable := ColorTable;
   DIB.UpdatePalette;
   UpdateSize(WW, HH, BPP);
   DrawDIB;
  end;
 end;
end;

procedure TMainForm.TreeFreeNode(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
 If Node <> NIL then
 begin
  With PRTreeData(Tree.GetNodeData(Node))^ do
  If BookmarkNode <> NIL then BookmarkNode.Free;
 end;
end;

procedure TMainForm.TreeGetImageIndex(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Kind: TVTImageKind; Column: TColumnIndex;
  var Ghosted: Boolean; var ImageIndex: Integer);
begin
 ImageIndex := 0;
end;

procedure TMainForm.TreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
begin
 With PRTreeData(Sender.GetNodeData(Node))^ do If BookmarkNode = NIL then
  CellText := '' else
  CellText := BookmarkNode.Name;
end;

procedure TMainForm.TreeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If Key = VK_DELETE then RemoveActionExecute(Sender);
end;

function TMainForm.AddItem(Name: String; P, W, H,
  BC: Integer; Const CT: TRGBQuads; Silent: Boolean = False): TBookmarkNode;
Var Node: PVirtualNode;
begin
 Node := Tree.AddChild(NIL);
 With PRTreeData(Tree.GetNodeData(Node))^ do
 begin
  BookmarkNode := TBookmarkNode.Create(Name, P, W, H, BC);
  BookmarkNode.ColorTable := CT;
  Result := BookmarkNode;
 end;
 If not Silent then
 begin
  Tree.Selected[Node] := True;
  Tree.FocusedNode := Node;
 end;
end;

procedure TMainForm.ClearActionExecute(Sender: TObject);
begin
 Tree.Clear;
 CurNode := NIl;
end;

procedure TMainForm.TreeNewText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; NewText: WideString);
begin
 If Node <> NIL then
 begin
  With PRTreeData(Tree.GetNodeData(Node))^ do If BookmarkNode <> NIL then
   BookmarkNode.Name := NewText;
 end;
end;

procedure TMainForm.TreeDblClick(Sender: TObject);
begin
 If CurNode <> NIL then TreeChange(NIL, CurNode);
end;

procedure TMainForm.ClearActionUpdate(Sender: TObject);
begin
 ClearAction.Enabled := Tree.RootNodeCount > 0;
end;

procedure TMainForm.UpdateActionExecute(Sender: TObject);
Var Node: PVirtualNode;
begin
 Node := Tree.GetFirstSelected;
 If Node <> NIL then With PRTreeData(Tree.GetNodeData(Node))^ do
 If BookmarkNode <> NIL then With BookmarkNode do
 begin
  Position := RawPosition;
  Width := WW;
  Height := HH;
  BitCount := BPP;
  ColorTable := DIB.ColorTable;
 end;
end;

procedure TMainForm.UpdateActionUpdate(Sender: TObject);
begin
 UpdateAction.Enabled := Tree.RootNodeCount > 0;
end;

Var LastPal: Integer = 0;

procedure TMainForm.GetPalButtonClick(Sender: TObject);
Type
 TRGB = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
  A: Byte;
 end;
 TPalBlock = Array[0..7] of Cardinal;
 TPalette32 = Array[0..31] of TPalBlock; 
Var
 Pal: Array[0..255] of TRGB; I: Integer; Max: Integer;
 S: String; PalBlock: TPalBlock; Palette: ^TPalette32;
begin
 S := InputBox('Get palette', 'Offset:', 'h' + IntToHex(LastPal, 1));
 If UnsCheckOut(S) then
 begin
  LastPal := DecHexToInt(S);
  RAW.Position := LastPal;
  RAW.Read(Pal, SizeOf(Pal));
  FillChar(DIB.ColorTable, 1024, 0);
  If BPP = 4 then Max := 15 Else Max := 255;
  With DIB do For I := 0 to Max do With ColorTable[I], Pal[I] do
  begin
   rgbRed := R;
   rgbGreen := G;
   rgbBlue := B;
   rgbReserved := A;
  end;
  If BPP = 8 then
  begin
   Palette := Addr(DIB.ColorTable);
   I := 1;
   Repeat
    Move(Palette^[I], PalBlock, SizeOf(TPalBlock));
    Move(Palette^[I + 1], Palette^[I], SizeOf(TPalBlock));
    Move(PalBlock, Palette^[I + 1], SizeOf(TPalBlock));
    Inc(I, 4);
   Until I > 29;
  end;
  DIB.UpdatePalette;
  Image.Picture.Graphic := DIB;
 end;
end;


procedure TMainForm.OpenTM2ActionExecute(Sender: TObject);
Type
 TRGBA = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
  A: Byte;
 end;
 TPalBlock = Array[0..7] of Cardinal;
 TPalette32 = Array[0..31] of TPalBlock;

Var
 PalBlock: TPalBlock; Palette: ^TPalette32;

Function FindPalette(Var Data; Colors: Integer): Pointer;
Var Pal: Array[Byte] of TRGBA; Quad: ^TRGBQuad; I, L, CC: Integer;
begin
 Quad := Addr(Data);
 For I := 0 to Colors - 1 do
 begin
  With Quad^, Pal[I] do
  begin
   R := rgbRed;
   G := rgbGreen;
   B := rgbBlue;
   A := rgbReserved;
  end;
  Inc(Quad);
 end;
 Result := Addr(RAW.Memory^);
 If BPP = 8 then
 begin
  Palette := Addr(Pal);
  I := 1;
  Repeat
   Move(Palette^[I], PalBlock, SizeOf(TPalBlock));
   Move(Palette^[I + 1], Palette^[I], SizeOf(TPalBlock));
   Move(PalBlock, Palette^[I + 1], SizeOf(TPalBlock));
   Inc(I, 4);
  Until I > 29;
 end;
 I := 0;
 CC := Colors shl 2;
 L := RAW.Size - CC;
 While I <= L do
 begin
  If CompareMem(Result, Addr(Pal), CC) then Exit;
  Inc(Integer(Result));
  Inc(I);
 end;
 Result := NIL;
end;
Type
 TRGB = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
 end;
Type
 PByteArray = ^TByteArray;
 TByteArray = Array[Byte] of Byte;
Var
 Layer: ^TLayerHeader; I, XX, YY, X, Y, Cnt: Integer; PIC: TDIB;
 Pal32: ^TRGBA; Pal24: ^TRGB absolute Pal32; Pal16: ^Word absolute Pal32;
 PalX: ^Byte absolute Pal32; GFX, B, S: ^Byte; BA: PByteArray absolute GFX;
 NormalPal: Boolean; K: Byte; BB: PBytes; F: File;
Var
 TIM2: ^TTIM2Header; MemoryStream: TMemoryStream;
begin
 If OpenTM2dialog.Execute then
 begin
  MemoryStream := TMemoryStream.Create;
  MemoryStream.LoadFromFile(OpenTM2dialog.FileName);
  B := MemoryStream.Memory;
  TIM2 := MemoryStream.Memory;
  If TIM2 <> NIL then With TIM2^ do
  If (thSignTag = TIM2Sign) and (thFormatTag in [3, 4]) then
  begin
   If thAlign128 then
    Inc(B, 128) Else
    Inc(B, SizeOf(TTIM2Header));
   If thLayersCount > 0 then
   begin
    Layer := Addr(B^);
    With Layer^ do If lhFormat in [4, 5] then
    begin
     GFX := Addr(Layer^);
     Inc(GFX, lhHeaderSize);
     PIC := TDIB.Create;
     NormalPal := (lhControlByte and $F0 = $80) or (lhFormat = 4);
     If lhFormat = 4 then
      PIC.BitCount := 4 Else
      PIC.BitCount := 8;
     PIC.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
     PalX := Addr(GFX^);
     Inc(PalX, lhImageSize);
     PIC.Width := lhWidth;
     PIC.Height := lhHeight;
     Case lhControlByte and $F of
      1:
      begin
       Cnt := lhPaletteSize div 2;
       If Cnt > 256 then Cnt := 256;
       For I := 0 to Cnt - 1 do With PIC.ColorTable[I] do
       begin
        rgbRed := GetR(Pal16^);
        rgbGreen := GetG(Pal16^);
        rgbBlue := GetB(Pal16^);
        rgbReserved := 128;
        Inc(Pal16);
       end;
      end;
      2:
      begin
       Cnt := lhPaletteSize div 3;
       If Cnt > 256 then Cnt := 256;
       For I := 0 to Cnt - 1 do With PIC.ColorTable[I] do
       begin
        With Pal24^ do
        begin
         rgbRed := R;
         rgbGreen := G;
         rgbBlue := B;
        end;
        rgbReserved := 128;
        Inc(Pal24);
       end;
      end;
      3:
      begin
       Cnt := lhPaletteSize div 4;
       If Cnt > 256 then Cnt := 256;
       For I := 0 to Cnt - 1 do With PIC.ColorTable[I] do
       begin
        With Pal32^ do
        begin
         rgbRed := R;
         rgbGreen := G;
         rgbBlue := B;
         rgbReserved := A;
        end;
        Inc(Pal32);
       end;
      end;
     end;
     With PIC do Case BitCount of
      4: For I := 0 to Height - 1 do
      begin
       PALX := ScanLine[I];
       For X := 0 to Width shr 1 - 1 do
       begin
        PALX^ := GFX^ shr 4;
        PALX^ := PALX^ or ((GFX^ and 15) shl 4);
        Inc(GFX);
        Inc(PALX);
       end;
      end;
      8: For I := 0 to Height - 1 do
      begin
       PALX := ScanLine[I];
       Move(GFX^, PALX^, Width);
       Inc(GFX, Width);
      end;
     end;
     If BPP = 8 then
     begin
      PALX := FindPalette(DIB.ColorTable, 256);
      If PALX <> NIL then
      begin
       If not NormalPal then
       begin
        Palette := Addr(PIC.ColorTable);
        I := 1;
        Repeat
         Move(Palette^[I], PalBlock, SizeOf(TPalBlock));
         Move(Palette^[I + 1], Palette^[I], SizeOf(TPalBlock));
         Move(PalBlock, Palette^[I + 1], SizeOf(TPalBlock));
         Inc(I, 4);
        Until I > 29;
       end;
       DIB.ColorTable := PIC.ColorTable;
       DIB.UpdatePalette;
       PIC.UpdatePalette;
       With PIC do For I := 0 to 255 do With ColorTable[I] do
       begin
        X := rgbRed;
        rgbRed := rgbBlue;
        rgbBlue := X;
       end;
       Palette := Addr(PIC.ColorTable);
       I := 1;
       Repeat
        Move(Palette^[I], PalBlock, SizeOf(TPalBlock));
        Move(Palette^[I + 1], Palette^[I], SizeOf(TPalBlock));
        Move(PalBlock, Palette^[I + 1], SizeOf(TPalBlock));
        Inc(I, 4);
       Until I > 29;
       Move(PIC.ColorTable, PALX^, 1024);
      end;
     end Else If BPP = 4 then
     begin
      PALX := FindPalette(DIB.ColorTable, 16);
      If PALX <> NIL then
      begin
       DIB.ColorTable := PIC.ColorTable;
       DIB.UpdatePalette;
       PIC.UpdatePalette;
       With PIC do For I := 0 to 15 do With ColorTable[I] do
       begin
        X := rgbRed;
        rgbRed := rgbBlue;
        rgbBlue := X;
       end;
       Move(PIC.ColorTable, PALX^, 16 * 4);
      end;
     end;
     PIC.SetSize(WW, HH, PIC.BitCount);
     GetMem(BB, WW * HH);
     With PIC do For Y := 0 to Height - 1 do
     begin
      If Odd(Y) then For X := 0 to Width - 1 do
      begin
       XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
       YY := Y + Matrix[Y mod 4];
       I := InterlaceMatrix[(X div 4) mod 4 + 4] +
      (X * 4) mod 16 + (X div 16) * 32 + ((Y - 1) * Width);
       BB^[I] := Pixels[XX, YY];
      end Else
      For X := 0 to Width - 1 do
      begin
       XX := X + Byte(Odd(Y div 4)) * TileMatrix[Byte(Odd(X div 4))];
       YY := Y + Matrix[Y mod 4];
       I := InterlaceMatrix[(X div 4) mod 4] +
       (X * 4) mod 16 + (X div 16) * 32 + (Y * Width);
       BB^[I] := Pixels[XX, YY];
      end;
     end;
     RAW.Position := RawPosition;
     S := Addr(BB^[0]);
     If BPP = 4 then For Y := 0 to HH - 1 do
     For X := 0 to WW shr 1 - 1 do
     begin
      K := S^;
      Inc(S);
      K := K or (S^ shl 4);
      Inc(S);
      RAW.Write(K, 1);
     end Else For Y := 0 to HH - 1 do
     begin
      Raw.Write(S^, WW);
      Inc(S, WW);
     end;
     FreeMem(BB);
     PIC.Free;
     DrawDIB;
    end Else ShowMessage('Graphic data must be 4 or 8 bpp.');
   end Else ShowMessage('Graphic data not found.');
  end;
  MemoryStream.Free;
 end;
end;

initialization
 DIB := TDIB.Create;
 DIB.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
 DIB.BitCount := 8;
 DIB.Width := 512;
 DIB.Height := 512;
 DIB.Fill8bit(0);
 RAW := TMemoryStream.Create;
 WW := 512;
 HH := 512;
 BPP := 4;
finalization
 RAW.Free;
 DIB.Free;
end.
