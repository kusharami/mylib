program PS2_TXD_Builder;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, NodeLst_old, PS2swizzle, HexUnit;

Type
 TChunk = Packed Record
  chDataType: Integer;
  chDataSize: Integer;
  chMagic: Integer; //$1803FFFF
 end;

 TContainerInfo = Packed Record
  miCount: Word;
  miType: Word;
 end;

 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;

 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;

 TTextureHeader = Packed Record
  Width: Integer;
  Height: Integer;
  BitCount: Integer;
  FormatData: Array[0..11] of Byte;
  ImageSizeShr8: Integer; //Min $1C
  Magic1: Integer; //0
  Magic2: Integer; //1024
  Magic3: Integer; //$400004
  Magic4: Integer; //1024
  Magic5: Integer; //$400004
  ImageSize: Integer;
  PaletteSize: Integer;
  MagicSize: Integer; //((ImageSize - $50) + $2000) shr 13) shl 11
  Magic6: Integer; //$0FC0
 end;

 TGraphicHeader = Packed Record
  ghMagic1: Integer; //3
  ghMagic2: Integer; //$10000000
  ghMagic3: Int64;   //$0E
  ghMagic4: Int64;   //0
  ghMagic5: Int64;   //$51
  ghWidth:  Integer;
  ghHeight: Integer;
  ghMagic6: Int64;   //$52
  ghMagic7: Int64;   //0
  ghMagic8: Int64;   //$53
  ghSzShr4: Integer;
  ghMagic9: Integer; //$08000000
  ghMagic10: Int64;  //0
 end;

 TChunkList = Class(TNodeList)
   private
    FMagic: Integer;
    FChunkType: Integer;
    FStream: TMemoryStream;
    function GetChunkList(Index: Integer): TChunkList;
    function GetFullSize: Integer;
   public
    property FullSize: Integer read GetFullSize;
    property ChunkType: Integer read FChunkType write FChunkType;
    property Stream: TMemoryStream read FStream;
    property Chunks[Index: Integer]: TChunkList read GetChunkList;

    constructor Create(ChunkType: Integer; Magic: Integer);
    destructor Destroy; override;
    function AddChunkList(ChunkType: Integer): TChunkList;
    procedure SaveToFile(const FileName: String);
   end;

Const
 TXD_CONTAINER = $16;
 TXD_ARRAY = $01;
 TXD_TEXTURE = $15;
 TXD_STRING = $02;
 TXD_TEXEND = $03;
 TXD_FINISH = $110;
 TIM2SIGN = $324D4954;
 PS2TEX: Int64 = $110100325350;

Procedure Build(Pattern, FileName: String; MagicCode: Integer);
Var
 SR: TSearchRec; ChunkList: TChunkList; Info: ^TContainerInfo; A, B, J: Integer;
 F: TFileStream; Path, FName: String; TIM: TTIM2Header; Layer: TLayerHeader;
 Header: ^TTextureHeader; WidthBytes: Integer; Gfx, Pal: ^TGraphicHeader;
 Buffer: Pointer;
Label FormatError;
begin
 Path := ExtractFilePath(Pattern);
 If FindFirst(Pattern, faArchive, SR) = 0 then
 begin
  ChunkList := TChunkList.Create(TXD_CONTAINER, MagicCode);
  With ChunkList.AddChunkList(TXD_ARRAY) do
  begin
   Stream.Size := 4;
   Info := Stream.Memory;
  end;
  With Info^ do
  begin
   miCount := 0;
   miType := 6;
   Repeat
    FName := Path + SR.Name;
    Write(Format('Adding "%s"...', [FName]));
    try
     F := TFileStream.Create(FName, fmOpenRead);
     With TIM, Layer do
     If (F.Read(TIM, SizeOf(TIM)) = SizeOf(TIM)) and (thSignTag = TIM2SIGN) and
        (thFormatTag in [3, 4]) and (thLayersCount > 0) then
     begin
      If thAlign128 then With F do If Position mod 128 > 0 then
       Position := (Position div 128) * 128 + 128;
      If (F.Read(Layer, SizeOf(Layer)) = SizeOf(Layer)) and
         (lhFormat in [4, 5]) and (lh256 = 256) then
      begin
       FName := ChangeFileExt(SR.Name, '') + #0;
       While Length(FName) mod 4 > 0 do FName := FName + #0;
       With ChunkList.AddChunkList(TXD_TEXTURE) do
       begin
        AddChunkList(TXD_ARRAY).Stream.Write(PS2TEX, 8);
        AddChunkList(TXD_STRING).Stream.Write(FName[1], Length(FName));
        J := 0; AddChunkList(TXD_STRING).Stream.Write(J, 4);
        With AddChunkList(TXD_ARRAY) do
        begin
         With AddChunkList(TXD_ARRAY) do
         begin
          Stream.Size := SizeOf(TTextureHeader);
          Header := Stream.Memory;
          With Header^ do
          begin
           Width := lhWidth;
           Height := lhHeight;
           If lhFormat = 4 then
            BitCount := 4 Else
            BitCount := 8;
           WidthBytes := Width div (8 div BitCount);
           ImageSizeShr8 := (WidthBytes * Height) shr 8;
           If ImageSizeShr8 < $1C then ImageSizeShr8 := $1C;
           Magic1 := 0;
           Magic2 := 1024;
           Magic3 := $400004;
           Magic4 := 1024;
           Magic5 := $400004;
           Magic6 := $0FC0;
           ImageSize := lhImageSize + $50;
           PaletteSize := lhPaletteSize + $50;
           MagicSize := ((lhImageSize + $2000) shr 13) shl 11;
           FillChar(FormatData, SizeOf(FormatData), 0);
           FormatData[0] := 4;
           If BitCount = 4 then
           begin
            FormatData[1] := $45;
            FormatData[6] := $40;
           end Else
           begin
            FormatData[1] := $25;
            FormatData[6] := $30;
           end;
           FormatData[2] := 2;
           FormatData[5] := lhControlByte and $F0;
           A := Width shr 4 + 5;
           If (A < 9) or (A > 15) then A := 5;
           B := (Height shr 4) + 5;
           If (B < 9) or (B > 15) then B := 6;
           FormatData[7] := A or (B shl 4);
           If FormatData[7] <> $65 then FormatData[8] := 5 Else
           begin
            FormatData[8] := 6;
            Inc(FormatData[6], 2);
           end;
           FormatData[11] := $20;
          end;
         end;
         With AddChunkList(TXD_ARRAY), Header^ do
         begin
          Stream.Size := ImageSize + PaletteSize;
          Gfx := Stream.Memory;
          Pal := Pointer(Integer(Stream.Memory) + ImageSize);
          With Gfx^ do
          begin
           ghMagic1 := 3;
           ghMagic2 := $10000000;
           ghMagic3 := $0E;
           ghMagic4 := 0;
           ghMagic5 := $51;
           ghWidth := Width div 2;
           ghHeight := Height div 2;
           ghMagic6 := $52;
           ghMagic7 := 0;
           ghMagic8 := $53;
           ghSzShr4 := (ghWidth * (4 div (8 div BitCount)) * ghHeight) shr 4;
           ghMagic9 := $08000000;
           ghMagic10 := 0;
          end;
          Inc(Gfx);
          If thAlign128 then With F do If Position mod 128 > 0 then
           Position := (Position div 128) * 128 + 128;
          GetMem(Buffer, lhImageSize);
          F.Read(Buffer^, lhImageSize);
          If BitCount = 4 then
           Swizzle4x(Buffer^, Gfx^, lhWidth, lhHeight) Else
           Swizzle8(Buffer^, Gfx^, lhWidth, lhHeight);
          FreeMem(Buffer);
          With Pal^ do
          begin
           ghMagic1 := 3;
           ghMagic2 := $10000000;
           ghMagic3 := $0E;
           ghMagic4 := 0;
           ghMagic5 := $51;
           If BitCount = 4 then
           begin
            ghWidth := 8;
            ghHeight := 2;
           end Else
           begin
            ghWidth := 16;
            ghHeight := 16;
           end;
           ghMagic6 := $52;
           ghMagic7 := 0;
           ghMagic8 := $53;
           ghSzShr4 := (ghWidth * 4 * ghHeight) shr 4;
           ghMagic9 := $08000000;
           ghMagic10 := 0;
          end;
          Inc(Pal);
          If thAlign128 then With F do If Position mod 128 > 0 then
           Position := (Position div 128) * 128 + 128;
          F.Read(Pal^, lhPaletteSize);
         end;
        end;
        J := $FC0;
        AddChunkList(TXD_TEXEND).AddChunkList(TXD_FINISH).Stream.Write(J, 4);
       end;
       Inc(miCount);
       Writeln('Done.');
      end Else Goto FormatError;
     end Else FormatError: Writeln('Format error.');
     F.Free;
    except
     Writeln('Open error.');
    end;
   Until FindNext(SR) <> 0;
   ChunkList.AddChunkList(TXD_TEXEND);
  end;
  ChunkList.SaveToFile(FileName);
  ChunkList.Free;
 end;
end;

function TChunkList.AddChunkList(ChunkType: Integer): TChunkList;
begin
 Result := TChunkList.Create(ChunkType, FMagic);
 Add^.Node := Result;
end;

constructor TChunkList.Create(ChunkType: Integer; Magic: Integer);
begin
 inherited Create;
 FStream := TMemoryStream.Create;
 FChunkType := ChunkType;
 FMagic := Magic;
end;

destructor TChunkList.Destroy;
begin
 FStream.Free;
 inherited;
end;

function TChunkList.GetChunkList(Index: Integer): TChunkList;
begin
 Result := Objects[Index] as TChunkList;
end;

function TChunkList.GetFullSize: Integer;
Var N: PNodeRec;
begin
 Result := Stream.Size;
 If Result = 0 then
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   Inc(Result, SizeOf(TChunk) + TChunkList(Node).GetFullSize);
   N := Next;
  end;
 end;
end;

procedure TChunkList.SaveToFile(const FileName: String);
Var F: TFileStream;
 Procedure WriteChunk(Const ChunkList: TChunkList);
 Var N: PNodeRec; Chunk: TChunk;
 begin
  If Assigned(ChunkList) then With ChunkList do
  begin
   Chunk.chDataType := ChunkType;
   Chunk.chDataSize := FullSize;
   Chunk.chMagic := FMagic;//$1803FFFF;
   F.Write(Chunk, SizeOf(TChunk));
   With Stream do If Size > 0 then F.Write(Memory^, Size) Else
   begin
    N := Root;
    While N <> NIL do With N^ do
    begin
     WriteChunk(TChunkList(Node));
     N := Next;
    end;
   end;
  end;
 end;
begin
 try
  F := TFileStream.Create(FileName, fmCreate);
  WriteChunk(Self);
  F.Free;
 except
 end;
end;

Var Code: Integer;
begin
 Writeln('PS2 TXD BUILDER V1.0');
 Writeln('neluhus.vagus@gmail.com');
 Writeln('USAGE: PS2_TXD_BUILDER [PATTERN] [FILENAME] [MAGIC]');
 Writeln('EXAMPLE: PS2_TXD_BUILDER GFX\*.TM2 OUTPUT.TXD 1C020065');
 If ParamCount >= 3 then
  Code := HexToInt(ParamStr(3)) Else
  Code := $1803FFFF;
 Build(ParamStr(1), ParamStr(2), Code);
end.
