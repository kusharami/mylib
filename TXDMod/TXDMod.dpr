program TXDMod;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  GotoDlg in 'GotoDlg.pas' {GotoDialog};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'TXDMod';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TGotoDialog, GotoDialog);
  Application.Run;
end.
