unit Thingy;

interface

uses SysUtils, Classes, NodeLst;

type
  TArrayOfByte = array of Byte;
  TArrayOfWord = array of Word;
  TArrayOfInt = array of Integer;
  TThingyItem = class;
  PByteItems = ^TByteItems;
  TByteItems = array[Byte] of TThingyItem;
  PWordItems = ^TWordItems;
  TWordItems = array[Word] of TThingyItem;
  TThingyTable = class(TNodeList)
   private
    FMaxCodeLen: Integer;
    FMaxValueLen: Integer;
    FBytes: PByteItems;
    FWords: PWordItems;
    FDecodePrepared: Boolean;
    FEncodePrepared: Boolean;
    FThingyLen: Integer;
    FThingy: array of TThingyTable;
    function GetCode(Value: WideString): TThingyItem;
    function GetItem(Index: Integer): TThingyItem;
   protected
    procedure Initialize; override;
    procedure AssignAdd(Source: TNode); override;
   public
    property Codes[Value: WideString]: TThingyItem read GetCode;
    property ThingyItems[Index: Integer]: TThingyItem read GetItem;
    property MaxCodeLen: Integer read FMaxCodeLen write FMaxCodeLen;
    property MaxValueLen: Integer read FMaxValueLen write FMaxValueLen;

    constructor Create;
    destructor Destroy; override;
    function GetVal(const Code: array of Byte): TThingyItem;    
    function GetValue(var Source; Count: Integer): TThingyItem;
    function GetString(var Source; Count: Integer): TThingyItem;
    procedure Assign(Source: TNode); override;
    procedure PrepareForDecode;
    procedure CleanUp;
    function Decode(var Source): TThingyItem;
    procedure PrepareForEncode;
    function Encode(var Source): TThingyItem;
    procedure LoadFromFile(const FileName: WideString);
    procedure SaveToFile(const FileName: WideString);    
    procedure LoadFromStream(Stream: TStream); virtual;
    procedure SaveToStream(Stream: TStream); virtual;
 end;
  TThingyItem = class(TNode)
   private
    FOwner: TThingyTable;
    FCode: TArrayOfByte;
    FValue: WideString;
    function IsLineBreak: Boolean;
   public
    property Code: TArrayOfByte read FCode write FCode;
    property Value: WideString read FValue write FValue;
    property LineBreak: Boolean read IsLineBreak;

    constructor Create(const Owner: TThingyTable);
    destructor Destroy; override;
    function SetString(const S: WideString): Boolean;
    function GetString: WideString;
    procedure Assign(Source: TNode); override;
   end;

implementation

uses TntClasses;

function TThingyItem.IsLineBreak: Boolean;
begin
 Result := FValue[Length(FValue)] = #10;
end;

constructor TThingyItem.Create(const Owner: TThingyTable);
begin
 FValue := '';
 SetLength(FCode, 0);
 FOwner := Owner;
 FAssignableClass := TThingyItem;
 Initialize;
end;

destructor TThingyItem.Destroy;
begin
 Finalize(FValue);
 Finalize(FCode);
 inherited Destroy;
end;

function TThingyItem.SetString(const S: WideString): Boolean;
var
 P: PWideChar;
 I, Mode, H, C, W: Integer;
begin
 I := Length(S);
 Result := False;
 if I >= 4 then
 begin
  P := Addr(S[1]);
  Mode := 0;
  H := -1;
  C := 0;
  SetLength(FCode, 0);
  repeat
   Dec(I);
   if Mode = 0 then
   begin
    W := Word(P^);
    Inc(P);
    if (W = $3D) and (H = 0) then
    begin
     Mode := 1;
     FValue := '';
     Continue;
    end Else
    if (W >= $30) and (W <= $39) then C := (C shl 4) or (W - $30) Else
    if (W >= $41) and (W <= $46) then C := (C shl 4) or (W - $37) Else
    if (W >= $61) and (W <= $66) then C := (C shl 4) or (W - $57) Else Exit;
    if (H < 0) then H := 0;
    Inc(H);
    if H = 2 then
    begin
     H := Length(FCode);
     SetLength(FCode, H + 1);
     FCode[H] := C;
     H := 0;
     C := 0;
    end;
   end Else if Mode = 1 then
   begin
    FValue := FValue + P^;
    Inc(P);
   end;
  Until I = 0;
  if FOwner <> NIL then with FOwner do
  begin
   if FMaxCodeLen < Length(FCode) then FMaxCodeLen := Length(FCode);
   if FMaxValueLen < Length(FValue) then FMaxValueLen := Length(FValue);
  end;
  Result := True;
 end;
end;

function TThingyItem.GetString: WideString;
var
 I: Integer;
begin
 Result := '';
 if Length(FCode) <= 0 then Exit;
 for I := 0 to Length(FCode) - 1 do
  Result := Result + IntToHex(FCode[I], 2);
 Result := Result + '=' + FValue;
end;

constructor TThingyTable.Create;
begin
 FAssignableClass := TThingyTable;
 Initialize;
end;

destructor TThingyTable.Destroy;
begin
 CleanUp;
 inherited Destroy;
end;

function TThingyTable.GetValue(var Source; Count: Integer): TThingyItem;
var
 N: TNode;
 I: TThingyItem;
begin
 Result := NIL;
 N := RootNode;
 while N <> NIL do
 begin
  I := N as TThingyItem;
  if I <> NIL then if (Length(I.FCode) = Count) and
     CompareMem(Addr(I.FCode[0]), Addr(Source), Count) then
  begin
   Result := I;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TThingyTable.GetString(var Source; Count: Integer): TThingyItem;
var
 N: TNode;
 I: TThingyItem;
begin
 Result := NIL;
 N := RootNode;
 while N <> NIL do
 begin
  I := N as TThingyItem;
  if I <> NIL then if (Length(I.FValue) = Count) and
     CompareMem(Addr(I.FValue[1]), Addr(Source), Count shl 1) then
  begin
   Result := I;
   Exit;
  end;
  N := N.Next;
 end;
end;


function TThingyTable.GetCode(Value: WideString): TThingyItem;
var N: TNode; I: TThingyItem;
begin
 Result := NIL;
 N := RootNode;
 while N <> NIL do
 begin
  I := N as TThingyItem;
  if (I <> NIL) and (I.FValue = Value) then
  begin
   Result := I;
   Exit;
  end;
  N := N.Next;
 end;
end;

function TThingyTable.GetItem(Index: Integer): TThingyItem;
begin
 Result := Nodes[Index] as TThingyItem;
end;

procedure TThingyTable.LoadFromFile(const FileName: WideString);
var
 Stream: TStream;
begin
 Stream := TTntFileStream.Create(FileName, fmOpenRead);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TThingyTable.SaveToFile(const FileName: WideString);
var
 Stream: TStream;
begin
 Stream := TTntFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TThingyTable.Assign(Source: TNode);
begin
 inherited;
 with Source as TThingyTable do
 begin
  if FDecodePrepared then Self.PrepareForDecode;
  if FEncodePrepared then Self.PrepareForEncode;
 end;
end;

procedure TThingyTable.PrepareForDecode;
var
 I, L, Cnt: Integer; N: TNode; BEmpty, WEmpty: Boolean; P: ^TThingyItem;
 Item1, Item: TThingyItem; TBL: TThingyTable; Temp: array of TThingyTable;
begin
 CleanUp;
 if FMaxCodeLen <= 0 then Exit;
 New(FBytes);
 FillChar(FBytes^[0], 256 shl 2, 0);
 Cnt := FMaxCodeLen - 2;
 if FMaxCodeLen >= 2 then
 begin
  New(FWords);
  FillChar(FWords^[0], 65536 shl 2, 0);;
  SetLength(Temp, Cnt);
  for I := 0 to Cnt - 1 do
  begin
   Temp[I] := TThingyTable.Create;
   Temp[I].FMaxCodeLen := FMaxCodeLen - I;
  end;
 end;
 BEmpty := True;
 WEmpty := True;
 N := RootNode;
 while N <> NIL do
 begin
  Item1 := N as TThingyItem;
  with Item1 do
  begin
   L := Length(FCode);
   if L = 1 then
   begin
    P := Addr(FBytes^[FCode[0]]);
    if P^ = NIL then P^ := Item1;
    BEmpty := False;
   end Else
   if L = 2 then
   begin
    P := Addr(FWords^[Word(Addr(FCode[0])^)]);
    if P^ = NIL then P^ := Item1;
    WEmpty := False;
   end Else
   if (L > 0) and (L <= FMaxCodeLen) and (Length(Temp) > 0) then
   begin
    TBL := Temp[FMaxCodeLen - L];
    Item := TThingyItem.Create(TBL);
    SetLength(Item.FCode, L);
    System.Move(FCode[0], Item.FCode[0], L);
    Item.FValue := FValue;
    TBL.AddCreated(Item);
   end;
  end;
  N := N.Next;
 end;
 if BEmpty then
 begin
  Dispose(FBytes);
  FBytes := NIL;
 end;
 if WEmpty then
 begin
  Dispose(FWords);
  FWords := NIL;
 end;
 for I := 0 to Cnt - 1 do if Temp[I].Count > 0 then
 begin
  L := Length(FThingy);
  SetLength(FThingy, L + 1);
  FThingy[L] := Temp[I];
 end;
 FThingyLen := Length(FThingy);
 SetLength(Temp, 0);
 Finalize(Temp);
 FDecodePrepared := True;
end;

procedure TThingyTable.CleanUp;
var
 I: Integer;
begin
 if Assigned(FBytes) then Dispose(FBytes);
 if Assigned(FWords) then Dispose(FWords);
 FBytes := NIL;
 FWords := NIL;
 for I := 0 to Length(FThingy) - 1 do FreeAndNil(FThingy[I]);
 Finalize(FThingy);
 FThingyLen := 0;
 FDecodePrepared := False;
 FEncodePrepared := False;
end;

function TThingyTable.Decode(var Source): TThingyItem;
var
 PT: ^TThingyTable; I: Integer; W: Word absolute Source;
 B: Byte absolute Source;
begin
 Result := NIL;
 if FDecodePrepared then
 begin
  if FThingyLen > 0 then
  begin
   PT := Addr(FThingy[0]);
   for I := 0 to FThingyLen - 1 do
   begin
    with PT^ do
    begin
     Result := GetValue(Source, FMaxCodeLen);
     if Result <> NIL then Exit;
    end;
    Inc(PT);
   end;
  end;
  if Assigned(FWords) then Result := FWords^[W];
  if Result <> NIL then Exit;
  if Assigned(FBytes) then Result := FBytes^[B];
  if Result <> NIL then Exit;
 end Else for I := FMaxCodeLen downto 1 do
 begin
  Result := GetValue(Source, I);
  if Result <> NIL then Exit;
 end;
end;

procedure TThingyTable.PrepareForEncode;
var
 I, L, Cnt: Integer; N: TNode; WEmpty: Boolean; P: ^TThingyItem;
 Item1, Item: TThingyItem; TBL: TThingyTable; Temp: array of TThingyTable;
begin
 CleanUp;
 New(FWords);
 FillChar(FWords^[0], 65536 shl 2, 0);
 Cnt := FMaxValueLen - 1;
 if FMaxValueLen >= 1 then
 begin
  SetLength(Temp, Cnt);
  for I := 0 to Cnt - 1 do
  begin
   Temp[I] := TThingyTable.Create;
   Temp[I].FMaxValueLen := FMaxValueLen - I;
  end;
 end;
 WEmpty := True;
 N := RootNode;
 while N <> NIL do
 begin
  Item1 := N as TThingyItem;
  with Item1 do
  begin
   L := Length(FValue);
   if L = 1 then
   begin
    P := Addr(FWords^[Word(FValue[1])]);
    if P^ = NIL then P^ := Item1;
    WEmpty := False;
   end Else
   if (L > 0) and (L <= FMaxValueLen) and (Length(Temp) > 0) then
   begin
    TBL := Temp[FMaxValueLen - L];
    Item := TThingyItem.Create(TBL);
    SetLength(Item.FCode, L);
    System.Move(FCode[0], Item.FCode[0], L);
    Item.FValue := FValue;
    TBL.AddCreated(Item);
   end;
  end;
  N := N.Next;
 end;
 if WEmpty then
 begin
  Dispose(FWords);
  FWords := NIL;
 end;
 for I := 0 to Cnt - 1 do if Temp[I].Count > 0 then
 begin
  L := Length(FThingy);
  SetLength(FThingy, L + 1);
  FThingy[L] := Temp[I];
 end;
 FThingyLen := Length(FThingy);
 SetLength(Temp, 0);
 Finalize(Temp);
 FEncodePrepared := True;
end;

function TThingyTable.Encode(var Source): TThingyItem;
var
 PT: ^TThingyTable; I: Integer; W: Word absolute Source;
begin
 Result := NIL;
 if FEncodePrepared then
 begin
  if FThingyLen > 0 then
  begin
   PT := Addr(FThingy[0]);
   for I := 0 to FThingyLen - 1 do
   begin
    with PT^ do
    begin
     Result := GetString(Source, FMaxValueLen);
     if Result <> NIL then Exit;
    end;
    Inc(PT);
   end;
  end;
  if Assigned(FWords) then Result := FWords^[W];
  if Result <> NIL then Exit;
 end Else for I := FMaxValueLen downto 1 do
 begin
  Result := GetString(Source, I);
  if Result <> NIL then Exit;
 end;
end;

procedure TThingyTable.Initialize;
begin
 NodeClass := TThingyItem;
end;

procedure TThingyItem.Assign(Source: TNode);
var
 L: Integer;
begin
 inherited;
 with Source as TThingyItem do
 begin
  Self.FValue := FValue;
  L := Length(FCode);
  SetLength(Self.FCode, L);
  if L > 0 then System.Move(FCode[0], Self.FCode[0], L);
 end;
end;

procedure TThingyTable.AssignAdd(Source: TNode);
var
 Item: TThingyItem;
begin
 Item := TThingyItem.Create(Self);
 AddCreated(Item);
 Item.Assign(Source);
end;

procedure TThingyTable.LoadFromStream(Stream: TStream);
var
 List: TTntStringList;
 I: Integer;
 N: TThingyItem;
 LineBreak: Boolean;
 S, Z: WideString;
begin
 List := TTntStringList.Create;
 List.LoadFromStream(Stream);
 LineBreak := False;
 for I := 0 to List.Count - 1 do
 begin
  S := List[I];
  Z := WideUpperCase(S);
  if Z = '@LINEBREAKERS' then LineBreak := True Else
  if Z = '@NORMALCHARS' then LineBreak := False Else
  begin
   if LineBreak then S := S + #13#10;
   N := TThingyItem.Create(Self);
   if N.SetString(S) then
    AddCreated(N) Else
    N.Free;
  end;
 end;
 List.Free;
end;

procedure TThingyTable.SaveToStream(Stream: TStream);
var
 List: TTntStringList;
 N: TNode;
 I: TThingyItem;
 S: WideString;
begin
 List := TTntStringList.Create;
 N := RootNode;
 while N <> NIL do
 begin
  I := N as TThingyItem;
  if (I <> NIL) then
  begin
   S := I.GetString;
   if S[Length(S)] <> #10 then List.Add(S);
  end;
  N := N.Next;
 end;
 List.Add('@LineBreakers');
 N := RootNode;
 while N <> NIL do
 begin
  I := N as TThingyItem;
  if (I <> NIL) then
  begin
   S := I.GetString;
   if S[Length(S)] = #10 then
   begin
    SetLength(S, Length(S) - 1);
    if S[Length(S)] = #13 then
     SetLength(S, Length(S) - 1);
    List.Add(S);
   end;
  end;
  N := N.Next;
 end;
 List.SaveToStream(Stream);
 List.Free;
end;

function TThingyTable.GetVal(const Code: array of Byte): TThingyItem;
var
 N: ^TThingyItem; I: Integer;
begin
 Result := NIL;
 N := RootLink;
 for I := 0 to Count - 1 do
 begin
  if (N^ <> NIL) and (Length(Code) = Length(N^.FCode)) and
     CompareMem(Pointer(N^.FCode), @Code, Length(Code)) then
  begin
   Result := N^;
   Exit;
  end;
 end;
end;

end.
