unit TilesEx;

interface

uses
 Windows, SysUtils, Classes, Variants, MyClasses, NodeLst, MyUtils, HexUnit, BitmapEx;

type
 TAttrArray = array of Variant;
 TTileItem = class(TNode)
  private
    FTileData: Pointer;
    FTileIndex: Integer;
    FFirstColor: Byte;
    FAttributes: TAttrArray;
    FBufferIndex: Integer;
    function GetTileSize: Integer;
    function GetNamedAttr(const Name: WideString): Variant;
    procedure SetNamedAttr(const Name: WideString; const Value: Variant);
    procedure SetTileIndex(Value: Integer);
    function GetTileHeight: Integer;
    function GetTileWidth: Integer;
  protected
    procedure Initialize; override;
    procedure JustAdded; override;
  public
    property Width: Integer read GetTileWidth;
    property Height: Integer read GetTileHeight;
    property BufferIndex: Integer read FBufferIndex;
    property Attributes: TAttrArray read FAttributes;
    property NamedAttr[const Name: WideString]: Variant read GetNamedAttr write SetNamedAttr;
    property TileSize: Integer read GetTileSize;
    property TileData: Pointer read FTileData write FTileData;
    property TileIndex: Integer read FTileIndex write SetTileIndex;
    property FirstColor: Byte read FFirstColor write FFirstColor;

    procedure Load(Src: TBitmapContainer; X, Y: Integer; ADrawFlags: Integer = 0); overload;
    procedure Load(Src: TBitmapContainer; X, Y: Integer; Cvt: TBitmapConverter); overload;
    procedure Draw(Dest: TBitmapContainer; X, Y: Integer; ADrawFlags: Integer = 0); overload;
    procedure Draw(Dest: TBitmapContainer; X, Y: Integer; Cvt: TBitmapConverter); overload;

    procedure Assign(Source: TNode); override;
    procedure AssignTileData(Source: TTileItem);
    procedure AssignAttributes(Source: TTileItem);
    destructor Destroy; override;
 end;
       {
 PEmptyBlock = ^TEmptyBlock;
 TEmptyBlock = packed record
  ebStart: Integer;
  ebEnd: Integer;
 end;  }

// TEmptyBlocks = array of TEmptyBlock;

 PTileConvertRec = ^TTileConvertRec;
 TTileConvertRec = record
  tcvtLoad: TBitmapConverter;
  tcvtDraw: array[Boolean, Boolean] of TBitmapConverter;
 end;

 TCvtRecList = array of TTileConvertRec;

 TTileSet = class(TSectionedList)
  private
    FEmptyTile: TTileItem;
    FLastIndex: Integer;
  //  FEmptyIndex: Integer;
//    FEmptyBlocks: TEmptyBlocks;
//    FCurrentBlock: PEmptyBlock;
    FFastList: array of TTileItem;
    FTileBitmap: TBitmapContainer;
    FDrawFlags: Word;
    FOptimizationFlags: Word;
    FAttrDefList: array of TAttrDef;
    FEmptyTileIndex: Integer;
    FMaxIndex: Integer;
    FTileBuffer: Pointer;
    FTileBufferSize: Integer;
    FKeepBuffer: Boolean;
    FHaveEmpty: Boolean;
    FNextFrameAttr: Integer;
    FFrameDelayAttr: Integer;
    FFrameRelative: Boolean;
    procedure MakeFastList;

    function GetTilesCount: Integer;
    function GetIndexed(Index: Integer): TTileItem;
    function GetAttrCount: Integer;
    procedure SetAttrCount(Value: Integer);
    function GetAttrDef(Index: Integer): PAttrDef;
    function GetTransparentColor: LongWord;
    procedure SetTransparentColor(Value: LongWord);
    function GetTileHeight: Integer;
    function GetTileSize: Integer;
    function GetTileWidth: Integer;
    function GetBitCount: Integer;
    function GetTileItem(Index: Integer): TTileItem;
    procedure SetFastList(Value: Boolean);
    procedure SetLastIndex(Value: Integer);
    procedure SetTilesCount(Value: Integer);
    function GetEmptyTileIndex: Integer;
    procedure SetMaxIndex(Value: Integer);
    function GetFastList: Boolean;
    function GetHaveEmpty: Boolean;
  protected
    procedure SetUnited(Value: Boolean); override;  
    procedure Initialize; override;
    procedure AssignAdd(Source: TNode); override;
    procedure ClearData; override;

    function CheckHeaderSize(var Size: LongInt): Boolean; override;
    function ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean; override;
    procedure ReadData(Stream: TStream; var Header: TSectionHeader); override;
    procedure WriteData(Stream: TStream; var Header: TSectionHeader); override;
    procedure FillHeader(var Header: TSectionHeader); override;
    function CalculateChecksum: TChecksum; override;
    function CalculateDataSize: LongInt; override;
    procedure ReadTilesInternal(var Rec: TProgressRec; AIndex, ACount: Integer);
    procedure WriteTilesInternal(var Rec: TProgressRec; AIndex, ACount: Integer);
  public
    property NextFrameAttr: Integer read FNextFrameAttr write FNextFrameAttr;
    property FrameRelative: Boolean read FFrameRelative write FFrameRelative;
    property FrameDelayAttr: Integer read FFrameDelayAttr write FFrameDelayAttr;
    property TileBuffer: Pointer read FTileBuffer;
    property TileBufferSize: Integer read FTileBufferSize;
    property KeepBuffer: Boolean read FKeepBuffer write FKeepBuffer;
    property MaxIndex: Integer read FMaxIndex write SetMaxIndex;
    property EmptyTileIndex: Integer read GetEmptyTileIndex;
    property TransparentColor: LongWord read GetTransparentColor
                                       write SetTransparentColor;
    property DrawFlags: Word read FDrawFlags write FDrawFlags;
    property AttrCount: Integer read GetAttrCount write SetAttrCount;
    property TilesCount: Integer read GetTilesCount write SetTilesCount;
    property OptimizationFlags: Word read FOptimizationFlags write FOptimizationFlags;
    property TileWidth: Integer read GetTileWidth;
    property TileHeight: Integer read GetTileHeight;
    property TileSize: Integer read GetTileSize;
    property TileBitmap: TBitmapContainer read FTileBitmap;
    property BitCount: Integer read GetBitCount;
    property LastIndex: Integer read FLastIndex write SetLastIndex;
    property EmptyTile: TTileItem read FEmptyTile;
  //  property EmptyBlocks: TEmptyBlocks read FEmptyBlocks;
    property FastList: Boolean read GetFastList write SetFastList;
    property HaveEmpty: Boolean read GetHaveEmpty;

    property AttrDefs[Index: Integer]: PAttrDef read GetAttrDef;
    property Indexed[Index: Integer]: TTileItem read GetIndexed;
    property Items[Index: Integer]: TTileItem read GetTileItem;

    function FindAttrByName(const Name: WideString): Integer;
    function FindByData(const Source): TTileItem;

    procedure InitEmptyTile;    
    procedure DeleteAttr(Index: Integer);
    procedure MoveAttr(OldIndex, NewIndex: Integer);
    function AddTile: TTileItem; overload;
    function AddTile(const AttrList: array of Variant): TTileItem; overload;   
    function AddFixed(Index: Integer): TTileItem;
    function AddOptimized(const Source): TTileItem; overload;
    function AddOptimized(const Source; const Attr: array of Variant;
                          var XFlip, YFlip: Boolean): TTileItem; overload;
//    function FindWhen(AttrIndex: Integer; const AValue: Variant): TTileItem;

    procedure TileFlip(const Source; var Dest; X, Y: Boolean);
    procedure TileLoad(Src: TBitmapContainer; var Dest; X, Y: Integer; ADrawFlags: Integer = 0); overload;
    procedure TileLoad(Src: TBitmapContainer; var Dest; X, Y: Integer; Cvt: TBitmapConverter); overload;
    procedure TileDraw(const Source; Dest: TBitmapContainer; X, Y: Integer; ADrawFlags: Integer = 0); overload;
    procedure TileDraw(const Source; Dest: TBitmapContainer; X, Y: Integer; Cvt: TBitmapConverter); overload;
{    procedure ResetEmptyBlock;
    procedure AddEmptyBlock(StartIndex, EndIndex: Integer);}
    function FindEmptyIndex: Integer;
{    procedure DeleteEmptyBlock(Index: Integer);
    procedure SetEmptyBlocksLen(Len: Integer);}

    procedure FillConverter(Cvt: TBitmapConverter;
             const Flags: array of Word; ADrawFlags: Word = 0);
    procedure FillConvertRec(var CvtRec: TTileConvertRec;
                             const Flags: array of Word; ADrawFlags: Word = 0);

    procedure ReadFromStream(Stream: TStream; AIndex, ACount: Integer; AOptimize: Boolean);
    procedure WriteToStream(Stream: TStream; AIndex, ACount: Integer; AUseEmptyTiles: Boolean);

    procedure AssignAttrDefs(Source: TTileSet);
    procedure AssignTileFormat(Source: TTileSet);
    procedure Assign(Source: TNode); override;
    procedure ConvertFrom(Source: TTileSet);

    procedure SwitchFormat(const Flags: array of Word;
                           ATileWidth: Integer = -1;
                           ATileHeight: Integer = -1;
                           ColFmt: TColorFormat = nil;
                           ColTbl: Pointer = nil);
    procedure Reallocate(const Flags: array of Word;
                         ATileWidth: Integer = -1;
                         ATileHeight: Integer = -1;
                         ColFmt: TColorFormat = nil;
                         ColTbl: Pointer = nil);
    procedure ClearTileCache;

    destructor Destroy; override;
    procedure Changed; override;    
 end;

 TTileSetList = class(TSectionedList)
  private
    function GetSet(X: Integer): TTileSet;
  protected
    procedure Initialize; override;
  public
    procedure FillCvtRecList(var List: TCvtRecList;
                const Flags: array of Word; ADrawFlags: Word = 0);
    property Sets[X: Integer]: TTileSet read GetSet; default;
 end;

 ETileSetError = class(Exception);

const
 OPT_FIND_MIRRORING = 1;
 OPT_CHECK_COLORS = 1 shl 1;
 OPT_CHECK_EMPTY = 1 shl 2;

 TSET_SIGN =  Ord('T') or
             (Ord('S') shl 8) or
             (Ord('E') shl 16) or
             (Ord('T') shl 24);
 TLST_SIGN =  Ord('T') or
             (Ord('L') shl 8) or
             (Ord('S') shl 16) or
             (Ord('T') shl 24);
  

procedure ConvertRecInit(var CvtRec: TTileConvertRec);
procedure ConvertRecFree(var CvtRec: TTileConvertRec);
procedure CvtRecListFree(var List: TCvtRecList);

implementation


type
 TMainHeader = packed record
  mhSignature: LongWord;
  mhHeaderSize: LongInt;
 end;

 TTileSetHeaderBase = packed Record
  tlhCount:            LongInt;
  tlhLastIndex:        LongInt;
  tlhAttrCount:        LongInt;
  tlhWidth:            Word;
  tlhHeight:           Word;
  tlhReserved:         LongInt;
  tlhTransparentColor: LongWord;
  tlhTileDrawFlags:    Word;
  tlhSetFlags:         Word;
 end;

 TTileSetHeaderV1 = packed record
  bh: TTileSetHeaderBase;
  tlhEmptyFirstColor:      Byte;
  tlhTileImageFormat:      TImageFormat;
  tlhTileFormatFlagsCount: Byte;
  tlhNameLength:           Byte;
 end;

const
 TSHF_IMG_FMT_RGB = 1;
 TSHF_INDEX_INFO = 1 shl 1;
 TSHF_COLOR_INFO = 1 shl 2;
 TSHF_FRAME_RELATIVE = 1 shl 3;

type
 TTileSetHeaderV2 = packed record
  bh: TTileSetHeaderBase;
  tlhEmptyFirstColor:      Byte;
  tlhTilesFirstColor:      Byte;
  tlhSpecialFlags:         Byte;
  tlhTileFormatFlagsCount: Byte;
  tlhNameLength:           Word;
 end;

 TTileSetHeaderV3 = packed record
  V2: TTileSetHeaderV2;
  tlhFrameNextAttr: LongInt;
  tlhFrameDelayAttr: LongInt;
 end;

 TTileSetHeader = TTileSetHeaderV3;

 TTileSetListHeader = packed record
  tslCount: LongInt;
 end;

procedure TileSetFormatError;
begin
 raise ETileSetError.Create('Tile set data format error');
end;

procedure ConvertRecInit(var CvtRec: TTileConvertRec);
begin
 CvtRec.tcvtLoad := TBitmapConverter.Create;
 CvtRec.tcvtDraw[False, False] := TBitmapConverter.Create;
 CvtRec.tcvtDraw[False,  True] := TBitmapConverter.Create;
 CvtRec.tcvtDraw[ True, False] := TBitmapConverter.Create;
 CvtRec.tcvtDraw[ True,  True] := TBitmapConverter.Create;
end;

procedure ConvertRecFree(var CvtRec: TTileConvertRec);
begin
 CvtRec.tcvtLoad.Free;
 CvtRec.tcvtDraw[False, False].Free;
 CvtRec.tcvtDraw[False,  True].Free;
 CvtRec.tcvtDraw[ True, False].Free;
 CvtRec.tcvtDraw[ True,  True].Free;
end;

procedure CvtRecListFree(var List: TCvtRecList);
var
 I: Integer;
begin
 for I := 0 to Length(List) - 1 do
  ConvertRecFree(List[I]);
 Finalize(List);
end;

{ TTileItem }

procedure TTileItem.Assign(Source: TNode);
begin
 inherited;
 AssignTileData(TTileItem(Source));
 AssignAttributes(TTileItem(Source));
end;

procedure TTileItem.AssignAttributes(Source: TTileItem);
var
 I: Integer;
begin
 if FAttributes = nil then
  SetLength(FAttributes, Length(TTileSet(Owner).FAttrDefList));
 if Source <> nil then
 begin
  for I := 0 to Min(Length(FAttributes), Length(Source.FAttributes)) - 1 do
   FAttributes[I] := Source.FAttributes[I];
 end else
  for I := 0 to Length(FAttributes) - 1 do
   FAttributes[I] := Unassigned;
end;

procedure TTileItem.AssignTileData(Source: TTileItem);
var
 DstBmp: TBitmapContainer;
 SrcBmp: TBitmapContainer;
begin
 DstBmp := TTileSet(Owner).FTileBitmap;
 if FTileData = nil then
  GetMem(FTileData, DstBmp.ImageSize);
 with TTileItem(Source) do
 begin
  SrcBmp := TTileSet(Owner).FTileBitmap;

  if not DstBmp.SameAs(SrcBmp) then
  begin
   SrcBmp.ImageData := FTileData;
   DstBmp.ImageData := Self.FTileData;
   SrcBmp.Draw(DstBmp, 0, 0);
  end else
   Move(FTileData^, Self.FTileData^, TileSize);

  Self.FFirstColor := FFirstColor;
 end;
end; 

destructor TTileItem.Destroy;
begin
 inherited;
 if FBufferIndex < 0 then
  FreeMem(FTileData);
end;

procedure TTileItem.Draw(Dest: TBitmapContainer; X, Y: Integer;
  Cvt: TBitmapConverter);
begin
 with TTileSet(Owner).FTileBitmap do
 begin
  ImageData := FTileData;
  Cvt.SrcInfo.PixelModifier := FFirstColor;
  Draw(Dest, X, Y, Cvt);
 end;
end;

procedure TTileItem.Draw(Dest: TBitmapContainer; X, Y,
  ADrawFlags: Integer);
begin
 with TTileSet(Owner).FTileBitmap do
 begin
  ImageData := FTileData;
  ADrawFlags := ADrawFlags or TTileSet(Owner).FDrawFlags; 
  if ADrawFlags and DRAW_PIXEL_MODIFY <> 0 then
   Draw(Dest, X, Y, (ADrawFlags and not 255) or FFirstColor) else
   Draw(Dest, X, Y, ADrawFlags and not 255);
 end;
end;

function TTileItem.GetNamedAttr(const Name: WideString): Variant;
var
 I: Integer;
begin
 I := (Owner as TTileSet).FindAttrByName(Name);
 if I >= 0 then
  Result := FAttributes[I] else
  Result := Unassigned;
end;

function TTileItem.GetTileHeight: Integer;
begin
 with Owner as TTileSet do
  Result := FTileBitmap.Height;
end;

function TTileItem.GetTileSize: Integer;
begin
 with Owner as TTileSet do
  Result := FTileBitmap.ImageSize;
end;

function TTileItem.GetTileWidth: Integer;
begin
 with Owner as TTileSet do
  Result := FTileBitmap.Width;
end;

procedure TTileItem.Initialize;
begin
 FAssignableClass := TTileItem;
end;

procedure TTileItem.JustAdded;
begin
 with FOwner as TTileSet do
 begin
  Inc(FLastIndex);
  FTileIndex := FLastIndex;
  FastList := False;
  GetMem(FTileData, FTileBitmap.ImageSize);
  SetLength(FAttributes, Length(FAttrDefList));
  FBufferIndex := -1;
 end;
 inherited;
end;

procedure TTileItem.Load(Src: TBitmapContainer; X, Y: Integer;
  Cvt: TBitmapConverter);
begin
 with TTileSet(Owner) do
 begin
  FTileBitmap.ImageData := FTileData;
  Src.Draw(FTileBitmap, -X, -Y, Cvt);
 end;
end;

procedure TTileItem.Load(Src: TBitmapContainer; X, Y, ADrawFlags: Integer);
begin
 with TTileSet(Owner) do
 begin
  FTileBitmap.ImageData := FTileData;
  Src.Draw(FTileBitmap, -X, -Y, ADrawFlags);
 end;
end;

procedure TTileItem.SetNamedAttr(const Name: WideString; const Value: Variant);
var
 I: Integer;
begin
 I := (Owner as TTileSet).FindAttrByName(Name);
 if I >= 0 then
  FAttributes[I] := Value else
  raise EVariantError.Create(Format('Unable to assign value to non-existent property ''%s''.', [Name]));
end;

{ TTileSet }
                {
procedure TTileSet.AddEmptyBlock(StartIndex, EndIndex: Integer);
var
 L: Integer;
begin
 L := Length(FEmptyBlocks);
 SetLength(FEmptyBlocks, L + 1);
 with FEmptyBlocks[L] do
 begin
  ebStart := StartIndex;
  ebEnd := EndIndex;
 end;
 ResetEmptyBlock;
end;         }

function TTileSet.AddFixed(Index: Integer): TTileItem;
var
 Idx: Integer;
begin
 Result := Indexed[Index];
 if Result = EmptyTile then
 begin
  Idx := FLastIndex;
  Result := AddTile;
  Result.FTileIndex := Index;
  if Index > Idx then
   Idx := Index;

  FLastIndex := Idx;
 end;
end;

function TTileSet.AddOptimized(const Source; const Attr: array of Variant;
                               var XFlip, YFlip: Boolean): TTileItem;
var
 Buf: Pointer;
 PB: PByte;
 VarPtr: PVariant;
 Found, Empty: Boolean;
 Idx, J, X, B, E,  SZ, Cnt, ACnt: Integer;
begin
 SZ := FTileBitmap.ImageSize;
 Empty := CompareMem(@Source, FEmptyTile.TileData, SZ);

 if not Empty and (FOptimizationFlags and OPT_FIND_MIRRORING <> 0) then
  GetMem(Buf, SZ * 3) else
  Buf := nil;
 try
  if not Empty and (FOptimizationFlags and OPT_FIND_MIRRORING <> 0) then
  begin
   PB := Buf;
   TileFlip(Source, PB^, False, True);
   Inc(PB, SZ);
   TileFlip(Source, PB^, True, False);
   Inc(PB, SZ);
   TileFlip(Source, PB^, True, True);
  end;
  Idx := FindEmptyIndex;

  FastList := False;
  X := Ord((FOptimizationFlags and OPT_CHECK_COLORS <> 0) and (Length(Attr) >= 1));
  ACnt := Length(Attr) - X;
  Result := RootNode as TTileItem;
  while Result <> nil do
  begin
   with Result do
   begin
    Found := (X = 0) or (FFirstColor = Attr[0]);
    Cnt := AttrCount;
    if (ACnt >= Cnt) then
    begin
     B := 0;
     E := Cnt - 1;
     if ACnt - 1 = Cnt then
     begin
      VarPtr := Addr(Attr[ACnt]);
      if VarIsNumeric(VarPtr^) then
       E := VarPtr^ - 1;
     end else
     if ACnt - 2 = Cnt then
     begin
      VarPtr := Addr(Attr[ACnt - 1]);
      if VarIsNumeric(VarPtr^) then
       B := VarPtr^;
      VarPtr := Addr(Attr[ACnt]);
      if VarIsNumeric(VarPtr^) then
       E := VarPtr^;
     end;
     for J := B to E do
      if not CompareVariants(Attributes[J], Attr[J + X]) then
      begin
       Found := False;
       Break;
      end;
    end;
    if Found then
    begin
     if Empty and (FOptimizationFlags and OPT_CHECK_EMPTY <> 0) and
        (ACnt = 0) and ((X = 0) or (FEmptyTile.FFirstColor = Attr[0])) then
     begin
      Result := FEmptyTile;     
      XFlip := False;
      YFlip := False;
      Exit;
     end;

     if CompareMem(@Source, TileData, SZ) then
     begin
      XFlip := False;
      YFlip := False;
      Exit;
     end;

     if FOptimizationFlags and OPT_FIND_MIRRORING <> 0 then
     begin
      PB := Buf;
      if CompareMem(PB, TileData, SZ) then
      begin
       XFlip := False;
       YFlip := True;
       Exit;
      end;

      Inc(PB, SZ);
      if CompareMem(PB, TileData, SZ) then
      begin
       XFlip := True;
       YFlip := False;
       Exit;
      end;

      Inc(PB, SZ);
      if CompareMem(PB, TileData, SZ) then
      begin
       XFlip := True;
       YFlip := True;
       Exit;
      end;
     end;
    end;
   end;
   Result := Result.Next as TTileItem;
  end;
  if Empty and (FOptimizationFlags and OPT_CHECK_EMPTY <> 0) and
     (ACnt = 0) and ((X = 0) or (FEmptyTile.FFirstColor = Attr[0])) then
  begin
   Result := FEmptyTile;
   XFlip := False;
   YFlip := False;
   Exit;
  end;
  Result := AddTile;
  Move(Source, Result.TileData^, SZ);
  if X <> 0 then
   Result.FFirstColor := Attr[0];
  Cnt := AttrCount;
  if (ACnt >= Cnt) then
   for J := 0 to Cnt - 1 do
    Result.Attributes[J] := Attr[J + X];
  XFlip := False;
  YFlip := False;
  Result.FTileIndex := Idx;
  if Idx > FLastIndex then
   FLastIndex := Idx;
 { if FCurrentBlock <> nil then
  with FCurrentBlock^ do
  begin
   if FEmptyIndex < ebStart then FEmptyIndex := ebStart else
   if FEmptyIndex > ebEnd then
   begin
    Inc(FCurrentBlock);
    if Cardinal(FCurrentBlock) <=
       Cardinal(Addr(FEmptyBlocks[Length(FEmptyBlocks) - 1])) then
     FEmptyIndex := FCurrentBlock.ebStart else
    begin
     FEmptyIndex := Count - 1;
     FCurrentBlock := NIL;
    end;
   end;
   Result.FTileIndex := FEmptyIndex;
   if FLastIndex < FEmptyIndex then FLastIndex := FEmptyIndex;
   Inc(FEmptyIndex);
  end;  }
 finally
  FreeMem(Buf);
 end;
end;

function TTileSet.AddOptimized(const Source): TTileItem;
var
 Idx, SZ: Integer;
begin
 SZ := FTileBitmap.ImageSize;
 if (FOptimizationFlags and OPT_CHECK_EMPTY <> 0) and
          CompareMem(@Source, FEmptyTile.TileData, SZ) then
 begin
  Result := FEmptyTile;
  Exit;
 end;
 Idx := FindEmptyIndex;
 FastList := False;
 Result := RootNode as TTileItem;
 while Result <> nil do
 begin
  if CompareMem(@Source, Result.TileData, SZ) then
   Exit;
  Result := Result.Next as TTileItem; 
 end;

 Result := AddTile;
 Move(Source, Result.TileData^, SZ);

 Result.FTileIndex := Idx;
 Dec(FLastIndex);
 if FLastIndex < Result.FTileIndex then
  FLastIndex := Result.FTileIndex;
end;

function TTileSet.AddTile: TTileItem;
begin
 Result := AddNode as TTileItem;
end;

function TTileSet.AddTile(const AttrList: array of Variant): TTileItem;
var
 I: Integer;
begin
 Result := AddNode as TTileItem;
 with Result do
 for I := 0 to Min(Length(AttrList), Length(FAttributes)) - 1 do
  FAttributes[I] := AttrList[I];
end;

procedure TTileSet.Assign(Source: TNode);
var
 Src: TTileSet absolute Source;
begin
 if (Source <> Self) and (Source is TTileSet) then
 begin
  Clear;
  AssignTileFormat(Src);
 end;
 inherited;

 FNextFrameAttr := Src.FNextFrameAttr;
 FFrameDelayAttr := Src.FFrameDelayAttr;
 FFrameRelative := Src.FFrameRelative;
 FLastIndex := Src.FLastIndex;
// FEmptyTile.FirstColor := Src.FEmptyTile.FirstColor;
 FEmptyTile.Assign(Src.FEmptyTile);
end;

procedure TTileSet.AssignAdd(Source: TNode);
begin
 with AddTile do
 begin
  Assign(Source);
  TileIndex := TTileItem(Source).TileIndex;
 end;
end;

procedure TTileSet.AssignAttrDefs(Source: TTileSet);
begin
 if Source <> nil then
 begin
  AttrCount := Source.AttrCount;
  AttrDefsCopy(Source.FAttrDefList, FAttrDefList);
 end else
  AttrCount := 0;
end;

procedure TTileSet.AssignTileFormat(Source: TTileSet);
begin
 if Source <> nil then
 begin
  FOptimizationFlags := Source.FOptimizationFlags;
  AssignAttrDefs(Source);
  TransparentColor := Source.TransparentColor;
  FDrawFlags := Source.FDrawFlags;
  Reallocate(Source.FTileBitmap.FlagsList,
             Source.FTileBitmap.Width,
             Source.FTileBitmap.Height,
             Source.FTileBitmap.ColorFormat,
             Source.FTileBitmap.ColorTable);
 end else
  raise ETileSetError.Create('Cannot assign tile format if source tile set is undefined');
end;

function TTileSet.CalculateChecksum: TChecksum;
var
 I, SZ: Integer;
begin
 Result := CalcChecksum(Pointer(FName)^,
            Min(Length(FName), High(Word)) shl 1) +
           CalcChecksum(FTileBitmap.CompositeFlags[0],
           Length(FTileBitmap.CompositeFlags) shl 1);
 I := FTileBitmap.ImageFlags;
 Inc(Result, CalcChecksum(I, 2));

 SZ := TileSize;
 for I := 0 to Count - 1 do
  Inc(Result, CalcChecksum((Nodes[I] as TTileItem).FTileData^, SZ));
end;

function TTileSet.CalculateDataSize: LongInt;
var
 I, J: Integer;
 Cl: Byte;
begin
 ClearTileCache; //70934
 Result := TileSize * (Count + 1) + Min(Length(FName), High(Word)) shl 1 +
           2 + Length(FTileBitmap.CompositeFlags) shl 1 {+
           Length(FEmptyBlocks) * SizeOf(TEmptyBlock)};
    //110132
 if FTileBitmap.ImageFormat <> ifIndexed then
  Inc(Result, SizeOf(TColorFormatRec));

 for I := 0 to Length(FAttrDefList) - 1 do
  Inc(Result, AttrDefGetStoreSize(FAttrDefList[I]));
        //110483

 if Count > 0 then
 begin
  for I := 0 to TilesCount - 1 do
   if Indexed[I].FTileIndex <> I then
   begin
    Inc(Result, Count * 4);
    Break;
   end;
      //113919
  Cl := (RootNode as TTileItem).FFirstColor;
  for I := 1 to Count - 1 do
   if Items[I].FFirstColor <> Cl then
   begin
    Inc(Result, Count);
    Break;
   end;
    //114778
 end;

 with FEmptyTile do
  for I := 0 to Length(FAttributes) - 1 do
   Inc(Result, VariantGetStoreSize(FAttributes[I]));
     // 114798

 for I := 0 to Count - 1 do
  with Nodes[I] as TTileItem do
   for J := 0 to Length(FAttributes) - 1 do
    Inc(Result, VariantGetStoreSize(FAttributes[J]));
  //214492
end;

procedure TTileSet.Changed;
begin
 inherited;
 FastList := False;
end;

function TTileSet.CheckHeaderSize(var Size: LongInt): Boolean;
begin
 Result := (Size = SizeOf(TTileSetHeaderV1)) or
           (Size = SizeOf(TTileSetHeaderV2)) or
           inherited CheckHeaderSize(Size);
end;

procedure TTileSet.ClearData;
begin
 InitEmptyTile;
 FLastIndex := -1;
 FastList := False;
end;

procedure TTileSet.ClearTileCache;
var
 N, Nxt: TNode;
 TN: TTileItem absolute N;
begin
 N := RootNode;
 while N <> nil do
 begin
  if not (N is TTileItem) or
    (TN.FTileIndex < 0) or
    (TN.FTileIndex > FLastIndex) then
  begin
   Nxt := N.Next;
   DetachNode(N);
   N.Free;
   N := Nxt;
  end else
   N := N.Next;
 end;
 United := True;
end;

procedure TTileSet.ConvertFrom(Source: TTileSet);
var
 Src, Dst: TTileItem;
 I: Integer;
begin
 if Source <> nil then
 begin
  Src := Source.RootNode as TTileItem;
  Dst := RootNode as TTileItem;
  for I := 0 to Max(Count, Source.Count) - 1 do
  begin
   if Dst = nil then
    FTileBitmap.ImageData := AddTile.FTileData else
    FTileBitmap.ImageData := Dst.FTileData;
   FTileBitmap.FillData(FTileBitmap.TransparentColor);
   Source.FTileBitmap.ImageData := Src.FTileData;
   Source.FTileBitmap.Draw(FTileBitmap, 0, 0);
   Src := Src.Next as TTileItem;
   if Src = nil then Break;
   Dst := Dst.Next as TTileItem;
  end;
 end else
  raise ETileSetError.Create('ConvertFrom is cannot be done if source tile set is undefined');
end;

procedure TTileSet.DeleteAttr(Index: Integer);
 procedure DelAttr(Index: Integer; var List: TAttrArray);
 var
  L: Integer;
 begin
  SetLength(List, Length(FAttrDefList) + 1);
  L := Length(List) - 1;
  if (Index >= 0) and (Index < L) then
  begin
   Finalize(List[Index]);
   Move(Addr(List[Index + 1])^, Addr(List[Index])^,
             (L - Index) * SizeOf(Variant));
   FillChar(List[L], SizeOf(Variant), 0);
  end;
  SetLength(List, L);
 end;
var
 I, L: Integer;
begin
 L := Length(FAttrDefList) - 1;
 if Index = L then SetAttrCount(L) else
 if (Index >= 0) and (Index < L) then
 begin
  Finalize(FAttrDefList[Index]);
  Move(FAttrDefList[Index + 1], FAttrDefList[Index],
      (L - Index) * SizeOf(TAttrDef));
  FillChar(FAttrDefList[L], SizeOf(TAttrDef), 0);

  SetLength(FAttrDefList, L);

  DelAttr(Index, FEmptyTile.FAttributes);
  for I := 0 to Count - 1 do
   DelAttr(Index, (Nodes[I] as TTileItem).FAttributes);
 end;
end;
      {
procedure TTileSet.DeleteEmptyBlock(Index: Integer);
var
 L: Integer;
begin
 if Index >= 0 then
 begin
  L := Length(FEmptyBlocks) - 1;
  if Index = L then SetLength(FEmptyBlocks, L) else
  if Index < L then
  begin
   Move(FEmptyBlocks[Index + 1], FEmptyBlocks[Index],
      (L - Index) * SizeOf(TEmptyBlock));
   SetLength(FEmptyBlocks, L);
  end else Exit;
  ResetEmptyBlock;
 end;
end;    }

destructor TTileSet.Destroy;
begin
 inherited;
 FreeMem(FTileBuffer);
 FEmptyTile.Free;
 FTileBitmap.ColorFormat.Free;
 FTileBitmap.Free;
end;

procedure TTileSet.FillConverter(Cvt: TBitmapConverter;
  const Flags: array of Word; ADrawFlags: Word);
var
 BmpFlags: TCompositeFlags;
begin
 BmpFlags := FTileBitmap.FlagsList;
 Cvt.Reset(BmpFlags, Flags, ADrawFlags or FDrawFlags);
end;

procedure TTileSet.FillConvertRec(var CvtRec: TTileConvertRec;
                    const Flags: array of Word; ADrawFlags: Word = 0);
var
 BmpFlags: TCompositeFlags;
 DrawFlg: Integer;
begin
 BmpFlags := FTileBitmap.FlagsList;
 DrawFlg := FDrawFlags or ADrawFlags;
 CvtRec.tcvtLoad.Reset(Flags, BmpFlags, DrawFlg and DRAW_PIXEL_MODIFY);
 CvtRec.tcvtDraw[False, False].Reset(BmpFlags, Flags, DrawFlg);
 CvtRec.tcvtDraw[False,  True].Reset(BmpFlags, Flags, DrawFlg or DRAW_Y_FLIP);
 CvtRec.tcvtDraw[ True, False].Reset(BmpFlags, Flags, DrawFlg or DRAW_X_FLIP);
 CvtRec.tcvtDraw[ True,  True].Reset(BmpFlags, Flags, DrawFlg or DRAW_XY_FLIP);
end;

procedure TTileSet.FillHeader(var Header: TSectionHeader);
var
 I: Integer;
begin
 inherited;
 with TTileSetHeader(Addr(Header.dh)^), V2 do
 begin
  bh.tlhCount := Count;
  bh.tlhLastIndex := FLastIndex;
  bh.tlhWidth := FTileBitmap.Width;
  bh.tlhHeight := FTileBitmap.Height;
  bh.tlhAttrCount := AttrCount;
//  bh.tlhEmptyBlocksCount := Length(FEmptyBlocks);
  bh.tlhReserved := 0;
  bh.tlhTransparentColor := FTileBitmap.TransparentColor;
  bh.tlhTileDrawFlags := FDrawFlags;
  tlhSpecialFlags := Ord(FTileBitmap.ImageFormat <> ifIndexed);
  if FFrameRelative then
   tlhSpecialFlags := tlhSpecialFlags or TSHF_FRAME_RELATIVE;
  tlhFrameNextAttr := FNextFrameAttr;
  tlhFrameDelayAttr := FFrameDelayAttr;
  if Count > 0 then
  begin
   for I := 0 to TilesCount - 1 do
    if Indexed[I].FTileIndex <> I then
    begin
     tlhSpecialFlags := tlhSpecialFlags or TSHF_INDEX_INFO;
     Break;
    end;

   tlhTilesFirstColor := (RootNode as TTileItem).FFirstColor;
   for I := 1 to Count - 1 do
    if Items[I].FFirstColor <> tlhTilesFirstColor then
    begin
     tlhSpecialFlags := tlhSpecialFlags or TSHF_COLOR_INFO;
     Break;
    end;
  end;
  tlhEmptyFirstColor := FEmptyTile.FFirstColor;
  bh.tlhSetFlags := FOptimizationFlags;
  tlhTileFormatFlagsCount := Length(FTileBitmap.CompositeFlags) + 1;
  tlhNameLength := Min(Length(FName), High(Word));
 end;
end;

function TTileSet.FindAttrByName(const Name: WideString): Integer;
var
 I: Integer;
begin
 Result := -1;
 for I := 0 to Length(FAttrDefList) - 1 do
  if FAttrDefList[I].adName = Name then
  begin
   Result := I;
   Exit;
  end;
end;

function TTileSet.FindByData(const Source): TTileItem;
begin
 Result := RootNode as TTileItem;
 while Result <> nil do
 begin
  if CompareMem(@Source, Result.TileData, TileSize) then Exit;
  Result := Result.Next as TTileItem;
 end;
end;

function TTileSet.FindEmptyIndex: Integer;
var
 I: Integer;
begin
 if HaveEmpty then
  for I := 0 to FLastIndex do
   if Indexed[I] = FEmptyTile then
   begin
    Result := I;
    Exit;
   end;
 Result := Count;

{ for I := 0 to Length(FEmptyBlocks) - 1 do with FEmptyBlocks[I] do
 if (Value >= ebStart) and (Value <= ebEnd) then
 begin
  Result := I;
  Exit;
 end;
 Result := -1;}
end;

function TTileSet.GetAttrCount: Integer;
begin
 Result := Length(FAttrDefList);
end;

function TTileSet.GetAttrDef(Index: Integer): PAttrDef;
begin
 if (Index >= 0) and (Index < Length(FAttrDefList)) then
  Result := Addr(FAttrDefList[Index]) else
  Result := nil;
end;

function TTileSet.GetBitCount: Integer;
begin
 Result := FTileBitmap.BitsCount;
end;

function TTileSet.GetEmptyTileIndex: Integer;
var
 Item: TTileItem;
 X: Integer;
begin
 if FEmptyTileIndex < 0 then
 begin
  FEmptyTileIndex := 0;
  for X := 0 to Count - 1 do
   with Nodes[X] as TTileItem do
    if FEmptyTileIndex = FTileIndex then
     Inc(FEmptyTileIndex);
  if FEmptyTileIndex > FMaxIndex then
  begin
   Item := FindByData(EmptyTile.FTileData^);
   if (Item <> nil) and (FEmptyTileIndex <= FMaxIndex) then
    FEmptyTileIndex := Item.FTileIndex else
    FEmptyTileIndex := 0;
  end;
 end;
 Result := FEmptyTileIndex;
end;

function TTileSet.GetFastList: Boolean;
begin
 Result := FFastList <> nil;
end;

function TTileSet.GetHaveEmpty: Boolean;
begin
 MakeFastList;
 Result := FHaveEmpty;
end;

function TTileSet.GetIndexed(Index: Integer): TTileItem;
begin
 if (Index >= 0) and (Index <= FLastIndex) then
 begin
  FastList := True;
  Result := FFastList[Index];
 end else if Index >= 0 then
  Result := FEmptyTile else
  Result := nil;
end;

function TTileSet.GetTileHeight: Integer;
begin
 Result := FTileBitmap.Height;
end;

function TTileSet.GetTileItem(Index: Integer): TTileItem;
begin
 Result := TTileItem(Nodes[Index]);
end;

function TTileSet.GetTilesCount: Integer;
begin
 Result := FLastIndex + 1;
end;

function TTileSet.GetTileSize: Integer;
begin
 Result := FTileBitmap.ImageSize;
end;

function TTileSet.GetTileWidth: Integer;
begin
 Result := FTileBitmap.Width;
end;

function TTileSet.GetTransparentColor: LongWord;
begin
 Result := FTileBitmap.TransparentColor;
end;

procedure TTileSet.InitEmptyTile;
begin
 ReallocMem(FEmptyTile.FTileData, FTileBitmap.ImageSize);
 FTileBitmap.ImageData := FEmptyTile.FTileData;
 FTileBitmap.FillData(FTileBitmap.TransparentColor);
 FEmptyTile.FTileIndex := -1;
 FEmptyTile.Owner := Self;
end;

procedure TTileSet.Initialize;
begin
 inherited;
 FFrameRelative := False;
 FNextFrameAttr := -1;
 FFrameDelayAttr := -1;
 FEmptyTileIndex := -1;
 FLastIndex := -1;
// FEmptyIndex := -1;
 FAssignableClass := TTileSet;
 FNodeClass := TTileItem;
 FEmptyTile := TTileItem.Create;
 FTileBitmap := TBitmapContainer.Create;
 FTileBitmap.ColorFormat := TColorFormat.Create; 
 InitEmptyTile;
 FDrawFlags := DRAW_PIXEL_MODIFY;
 FSignature := TSET_SIGN;
 FHeaderSize := SizeOf(TTileSetHeader);
 FMaxNameLength := High(Word);
end;

procedure TTileSet.MakeFastList;
var
 N: TNode;
 I: Integer;
begin
 if FFastList = nil then
 begin
  SetLength(FFastList, FLastIndex + 1);
  Fill32(Cardinal(FEmptyTile), FFastList[0], FLastIndex + 1);
  N := RootNode;
  while N <> nil do
  begin
   with N as TTileItem do
    if (FTileIndex >= 0) and (FTileIndex <= FLastIndex) then
     FFastList[FTileIndex] := TTileItem(N);
   N := N.Next;
  end;
  FHaveEmpty := Count <= FLastIndex;
  if not FHaveEmpty then
  begin
   for I := 0 to FLastIndex do
    if FFastList[I] = FEmptyTile then
    begin
     FHaveEmpty := True;
     Exit;
    end;
  end;
 end;
end;

procedure TTileSet.MoveAttr(OldIndex, NewIndex: Integer);
procedure  MovAttr(OldIndex, NewIndex: Integer; var Attrs: TAttrArray);
var
 Temp: Variant;
 I: Integer;
begin
 SetLength(Attrs, Length(FAttrDefList));
 if (OldIndex + 1 = NewIndex) or
    (OldIndex - 1 = NewIndex) then
 begin
  Move(Attrs[NewIndex], Temp, SizeOf(Variant));
  Move(Attrs[OldIndex], Attrs[NewIndex], SizeOf(Variant));
  Move(Temp, Attrs[OldIndex], SizeOf(Variant));
  FillChar(Temp, SizeOf(Variant), 0);
 end else
 begin
  if OldIndex < NewIndex then
  begin
   I := NewIndex;
   NewIndex := I;
   OldIndex := NewIndex;
  end;
  Temp := Attrs[OldIndex];
  Finalize(Attrs[OldIndex]);

  Move(Attrs[NewIndex], Attrs[NewIndex + 1],
       (OldIndex - NewIndex) *  SizeOf(Variant));
  FillChar(Attrs[NewIndex], SizeOf(Variant), 0);
  Attrs[NewIndex] := Temp;
 end;
end;
var
 AttrTemp: TAttrDef;
 I, L: Integer;
begin
 L := Length(FAttrDefList);
 if (OldIndex <> NewIndex) and
    (OldIndex >= 0) and (OldIndex < L) then
 begin
  if NewIndex < 0 then
   NewIndex := 0 else 
  if NewIndex >= L then
   NewIndex := L - 1;

  if (OldIndex + 1 = NewIndex) or
     (OldIndex - 1 = NewIndex) then
  begin
   Move(FAttrDefList[NewIndex], AttrTemp, SizeOf(TAttrDef));
   Move(FAttrDefList[OldIndex], FAttrDefList[NewIndex], SizeOf(TAttrDef));
   Move(AttrTemp, FAttrDefList[OldIndex], SizeOf(TAttrDef));
   FillChar(AttrTemp, SizeOf(TAttrDef), 0);
  end else
  begin
   if OldIndex < NewIndex then
   begin
    I := NewIndex;
    NewIndex := I;
    OldIndex := NewIndex;
   end;
   AttrTemp := FAttrDefList[OldIndex];
   Finalize(FAttrDefList[OldIndex]);

   Move(FAttrDefList[NewIndex], FAttrDefList[NewIndex + 1],
        (OldIndex - NewIndex) *  SizeOf(TAttrDef));
   FillChar(FAttrDefList[NewIndex], SizeOf(TAttrDef), 0);
   FAttrDefList[NewIndex] := AttrTemp;
  end;

  MovAttr(OldIndex, NewIndex, FEmptyTile.FAttributes);
  for I := 0 to Count - 1 do
   MovAttr(OldIndex, NewIndex, (Nodes[I] as TTileItem).FAttributes);
 end;
end;

procedure TTileSet.ReadData(Stream: TStream; var Header: TSectionHeader);
var
 Base: ^TTileSetHeaderBase;
 V1: ^TTileSetHeaderV1 absolute Base;
 V2: ^TTileSetHeaderV2 absolute Base;
 V3: ^TTileSetHeaderV3 absolute Base;
 Flags: array of Word;

 I, J: Integer;
 ColInfo: array of Byte;
 IndexInfo: array of LongInt;
 CFmt: TColorFormatRec;
 SavePos: Int64;
 Rec: TProgressRec;
begin
 Base := Addr(Header.dh);
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_READ_DATA;

 Read(Rec, Pointer(FName)^, Length(FName) shl 1);

 System.Initialize(Flags);
 case Header.shHeaderSize of
  SizeOf(TTileSetHeaderV1): SetLength(Flags, V1.tlhTileFormatFlagsCount);
  SizeOf(TTileSetHeaderV2),
  SizeOf(TTileSetHeaderV3): SetLength(Flags, V2.tlhTileFormatFlagsCount);
 end;

 if Header.shHeaderSize = SizeOf(TTileSetHeaderV3) then
 begin
  NextFrameAttr := V3.tlhFrameNextAttr;
  FrameDelayAttr := V3.tlhFrameDelayAttr;
  FrameRelative := V2.tlhSpecialFlags and TSHF_FRAME_RELATIVE <> 0;
 end;

 Read(Rec, Pointer(Flags)^, Length(Flags) shl 1);

 if FTileBitmap.ImageFormat <> ifIndexed then
 begin
  Read(Rec, CFmt, SizeOf(CFmt));

  with FTileBitmap.ColorFormat do
  begin
   SetFormat(CFmt.colRedMask, CFmt.colGreenMask,
             CFmt.colBlueMask, CFmt.colAlphaMask);
   UsedBits := CFmt.colBits;
   ColorSize := CFmt.colSize;
   Flags := CFmt.colFlags;
  end;
 end;

// Read(Rec, FEmptyBlocks[0], Length(FEmptyBlocks) * SizeOf(TEmptyBlock));

 SavePos := Stream.Position;
 for I := 0 to Length(FAttrDefList) - 1 do
  AttrDefLoadFromStream(FAttrDefList[I], Stream);
 Rec.Value := Stream.Position - SavePos;
 Progress(Self, Rec);

 FTileBitmap.Width := 0;
 FTileBitmap.Height := 0;
 SwitchFormat(Flags, Base.tlhWidth, Base.tlhHeight);
 DrawFlags := Base.tlhTileDrawFlags;

 Read(Rec, FEmptyTile.FTileData^, FTileBitmap.ImageSize);

 ReadTilesInternal(Rec, 0, Base.tlhCount);

 FLastIndex := Base.tlhLastIndex;

 SetLength(IndexInfo, Count);
 SetLength(ColInfo, Count);
 case Header.shHeaderSize of
  SizeOf(TTileSetHeaderV1):
  begin
   Read(Rec, IndexInfo[0], Count shl 2);
   Read(Rec, ColInfo[0], Count);
  end;
  SizeOf(TTileSetHeaderV2),
  SizeOf(TTileSetHeaderV3):
  begin
   if V2.tlhSpecialFlags and TSHF_INDEX_INFO <> 0 then
    Read(Rec, IndexInfo[0], Count shl 2) else
   for J := 0 to Count - 1 do
    IndexInfo[J] := J;

   if V2.tlhSpecialFlags and TSHF_COLOR_INFO <> 0 then
    Read(Rec, ColInfo[0], Count) else
    FillChar(ColInfo[0], Count, V2.tlhTilesFirstColor);
  end;
 end;

 SavePos := Stream.Position;
 with FEmptyTile do
 begin
  FTileIndex := -1;
  for J := 0 to Length(FAttributes) - 1 do
   FAttributes[J] := VariantLoadFromStream(Stream);
 end;
 Rec.Value := Stream.Position - SavePos;
 Progress(Self, Rec);

 SavePos := Stream.Position;
 for I := 0 to Count - 1 do
  with Nodes[I] as TTileItem do
  begin
   FTileIndex := IndexInfo[I];
   if FTileIndex > FLastIndex then
    FLastIndex := FTileIndex;
   FFirstColor := ColInfo[I];
   for J := 0 to Length(FAttributes) - 1 do
    FAttributes[J] := VariantLoadFromStream(Stream);
  end;
 Rec.Value := Stream.Position - SavePos;
 Progress(Self, Rec);
end;

procedure TTileSet.ReadFromStream(Stream: TStream; AIndex, ACount: Integer;
  AOptimize: Boolean);
var
 I, SZ: Integer;
 Buf: Pointer;
 N: TTileItem;
begin
 SZ := FTileBitmap.ImageSize;
 if AOptimize then
 begin
  I := AIndex;
  Inc(AIndex, ACount - 1);
  FLastIndex := Max(FLastIndex, AIndex);
  GetMem(Buf, SZ);
  try
   for I := I to AIndex do
   begin
    Stream.ReadBuffer(Buf^, SZ);
    if CompareMem(Buf, FEmptyTile.FTileData, SZ) then
     Remove(Indexed[I]) else
     Move(Buf^, AddFixed(I).FTileData^, SZ);
   end;
  finally
   FreeMem(Buf);
  end;
 end else
 begin
  if Count > AIndex then
  begin
   N := Items[AIndex];
   while (N <> nil) and (ACount > 0) do
   begin
    Stream.ReadBuffer(N.FTileData^, SZ);
    N := N.Next as TTileItem;
    Dec(ACount);
   end;
  end else
   Count := AIndex;

  while (ACount > 0) do
  begin
   Stream.ReadBuffer(AddTile.FTileData^, SZ);
   Dec(ACount);
  end;
 end;
end;

function TTileSet.ReadHeader(Stream: TStream; var Header: TSectionHeader): Boolean;
var
 Base: ^TTileSetHeaderBase;
 V1: ^TTileSetHeaderV1 absolute Base;
 V2: ^TTileSetHeaderV2 absolute Base;
begin
 Result := inherited ReadHeader(Stream, Header);
 if Result then
 begin
  Base := Addr(Header.dh);
  FOptimizationFlags := Base.tlhSetFlags;
  FTileBitmap.TransparentColor := Base.tlhTransparentColor;
//  SetEmptyBlocksLen(Base.tlhEmptyBlocksCount);
  AttrCount := Base.tlhAttrCount;
  case Header.shHeaderSize of
   SizeOf(TTileSetHeaderV1):
   begin
    SetLength(FName, V1.tlhNameLength);
    FEmptyTile.FFirstColor := V1.tlhEmptyFirstColor;
    FTileBitmap.ImageFormat := V1.tlhTileImageFormat;
   end;
   SizeOf(TTileSetHeaderV2),
   SizeOf(TTileSetHeaderV3):
   begin
    SetLength(FName, V2.tlhNameLength);
    FEmptyTile.FFirstColor := V2.tlhEmptyFirstColor;
    if V2.tlhSpecialFlags and TSHF_IMG_FMT_RGB <> 0 then
     FTileBitmap.ImageFormat := ifRGB else
     FTileBitmap.ImageFormat := ifIndexed;
   end;
  end;
 end;
end;

procedure TTileSet.ReadTilesInternal(var Rec: TProgressRec; AIndex,
  ACount: Integer);
var
 Sz, Cnt: Integer;
begin
 Sz := FTileBitmap.ImageSize;
 Cnt := 32768 div Sz;
 while ACount > 0 do
 begin
  if Cnt > ACount then
   Cnt := ACount;
  ReadFromStream(Rec.Stream, AIndex, Cnt, False);
  Inc(AIndex, Cnt);
  Dec(ACount, Cnt);
  Rec.Value := Sz * Cnt;
  Progress(Self, Rec);
 end;
end;

procedure TTileSet.Reallocate(const Flags: array of Word;
                              ATileWidth: Integer = -1;
                              ATileHeight: Integer = -1;
                              ColFmt: TColorFormat = nil;
                              ColTbl: Pointer = nil);
var
 SZ: Integer;
 SrcFlags: TCompositeFlags;
 Temp: TBitmapContainer;
 Item: TTileItem;
 Ptr: Pointer;
 Cvt: TBitmapConverter;
// OldKeepBuffer: Boolean;
begin
 if ATileWidth < 0 then ATileWidth := FTileBitmap.Width;
 if ATileHeight < 0 then ATileHeight := FTileBitmap.Height;
 //if ColFmt = nil then ColFmt := FTileBitmap.ColorFormat;
 if ColTbl = nil then ColTbl := FTileBitmap.ColorTable;

 if (ATileWidth = FTileBitmap.Width) and
    (ATileHeight = FTileBitmap.Height) and
    (ColTbl = FTileBitmap.ColorTable) and
    (High(Flags) = Length(FTileBitmap.CompositeFlags)) and
    (Flags[0] = FTileBitmap.ImageFlags) and
    CompareMem(Addr(Flags[1]), Pointer(FTileBitmap.CompositeFlags), High(Flags) shl 1) and
    ((ColFmt = nil) or ColFmt.SameAs(FTileBitmap.ColorFormat)) then
  Exit;
{
 if (ATileWidth = FTileBitmap.Width) and
    (ATileHeight = FTileBitmap.Height) and
    (Length(Flags) = 0) and
    (((Length(Flags) = 0) and (FTileBitmap.ImageFlags = 0) and
      (Length(FTileBitmap.CompositeFlags) = 0)) or
    ((Length(Flags) > 0) and (Flags[0] = FTileBitmap.ImageFlags) and
    (Length(FTileBitmap.CompositeFlags) = Length(Flags) - 1))) and
    CompareMem(Addr(Flags[1]), Pointer(FTileBitmap.CompositeFlags),
               (Length(Flags) - 1) shl 1) and
    ((ADrawFlags < 0) or (ADrawFlags = FDrawFlags)) and
    ((ColFmt = nil) or FTileBitmap.ColorFormat.SameAs(ColFmt)) and
    ((ColTbl = nil) or (ColTbl = FTileBitmap.ColorTable)) then
    Exit;                }
 {OldKeepBuffer := FKeepBuffer;
 try
  FKeepBuffer := False;
  United := False;
 finally
  FKeepBuffer := OldKeepBuffer;
 end;   }
 Temp := TBitmapContainer.Create;
 try
  Temp.ColorFormat := TColorFormat.Create;
  try
   Temp.ColorFormat.Assign(FTileBitmap.ColorFormat);
   Temp.ColorTable := FTileBitmap.ColorTable;
   Temp.Width := FTileBitmap.Width;
   Temp.Height := FTileBitmap.Height;
   Temp.WidthRemainder := FTileBitmap.WidthRemainder;
   SrcFlags := FTileBitmap.FlagsList;   
   Temp.SetFlags(SrcFlags);
   Temp.ImageFormat := FTileBitmap.ImageFormat;

   if ColFmt <> nil then
    FTileBitmap.ColorFormat.Assign(ColFmt);
   if ColTbl = Pointer(-1) then
    FTileBitmap.ImageFormat := ifRGB else
   if ColTbl <> nil then
   begin
    FTileBitmap.ColorTable := ColTbl;
    FTileBitmap.ImageFormat := ifIndexed;
   end;
   if ((FlagsGetBitsCount(Flags) > 8) and
      (FTileBitmap.ColorFormat.ColorSize > 1)) then
    FTileBitmap.ImageFormat := ifRGB;

   FTileBitmap.Width := ATileWidth;
   FTileBitmap.WidthRemainder := 0;
   FTileBitmap.Height := ATileHeight;
   FTileBitmap.Compressed := False;

   if Length(Flags) > 0 then
    FTileBitmap.SetFlags(Flags);
   SZ := FTileBitmap.ImageSize;
 //  Ptr := FEmptyTile.FTileData;
//   FEmptyTile.FTileData := nil;
   InitEmptyTile;
//   FillChar(FEmpyTile.FTileData^, SZ, 0);
   if (Temp.Width > 0) and (Temp.Height > 0) then
   begin
    Cvt := TBitmapConverter.Create(SrcFlags, FTileBitmap.FlagsList);
    try
   //  FTileBitmap.ImageData := FEmptyTile.FTileData;
   //  Temp.ImageData := Ptr;
   //  Temp.Draw(FTileBitmap, 0, 0, Cvt);
   //  FreeMem(Ptr);
     Item := RootNode as TTileItem;
     while Item <> nil do
     begin
      Ptr := Item.FTileData;
      GetMem(Item.FTileData, SZ);
      FTileBitmap.ImageData := Item.FTileData;
      FTileBitmap.FillData(FTileBitmap.TransparentColor);
      Temp.ImageData := Ptr;
      Temp.Draw(FTileBitmap, 0, 0, Cvt);
      if Item.FBufferIndex < 0 then
       FreeMem(Ptr) else
       Item.FBufferIndex := -1;
      Item := Item.Next as TTileItem;
     end;
    finally
     Cvt.Free;
    end;
   end else
   begin
    Item := RootNode as TTileItem;
    while Item <> nil do
    begin
     if Item.FBufferIndex >= 0 then
     begin
      Item.FTileData := nil;
      Item.FBufferIndex := -1;
     end;
     ReallocMem(Item.FTileData, SZ);
     FTileBitmap.ImageData := Item.FTileData;
     FTileBitmap.FillData(FTileBitmap.TransparentColor);
     Item := Item.Next as TTileItem;
    end;
   end;
   FreeMem(FTileBuffer);
   FTileBuffer := nil;
   FTileBufferSize := 0;
   inherited United := False;
  finally
   Temp.ColorFormat.Free;
  end;
 finally
  Temp.Free;
 end;
end;
      {
procedure TTileSet.ResetEmptyBlock;
begin
 FEmptyIndex := -1;
 FCurrentBlock := Pointer(FEmptyBlocks);
end;   }

procedure TTileSet.SetAttrCount(Value: Integer);
var
 I, Old: Integer;
begin
 if Length(FAttrDefList) <> Value then
 begin
  Old := Length(FAttrDefList);
  SetLength(FAttrDefList, Value);
  for I := Old to Value - 1 do
   InitIntegerAttr(AttrDefs[I]^, 'NEW_ATTR', Low(Int64), High(Int64));

  SetLength(FEmptyTile.FAttributes, Value);
  for I := 0 to Count - 1 do
   SetLength((Nodes[I] as TTileItem).FAttributes, Value);
 end;
end;
       {
procedure TTileSet.SetEmptyBlocksLen(Len: Integer);
begin
 SetLength(FEmptyBlocks, Len);
 ResetEmptyBlock;
end;     }

procedure TTileSet.SetFastList(Value: Boolean);
begin
 if not Value then
  Finalize(FFastList) else
  MakeFastList;
end;

procedure TTileSet.SetLastIndex(Value: Integer);
begin
 if FLastIndex <> Value then
 begin
  FLastIndex := Value;
  FastList := False;
 end;
end;

procedure TTileSet.SetMaxIndex(Value: Integer);
begin
 if Value <> FMaxIndex then
 begin
  FMaxIndex := Value;
  FEmptyTileIndex := -1;
 end;
end;

procedure TTileSet.SetTilesCount(Value: Integer);
begin
 Dec(Value);
 if FLastIndex <> Value then
 begin
  FLastIndex := Value;
  FastList := False;
 end;
end;

procedure TTileSet.SetTransparentColor(Value: LongWord);
begin
 FTileBitmap.TransparentColor := Value;
 InitEmptyTile;
end;

procedure TTileSet.SetUnited(Value: Boolean);
var
 P: PByte;
 Sz: Integer;
 N: TNode;
begin
 if not Value then
 begin
  if (FTileBuffer <> nil) and not FKeepBuffer then
  begin
   Sz := FTileBitmap.ImageSize;
   N := RootNode;
   while N <> nil do
   begin
    with N as TTileItem do
    begin
     if FBufferIndex >= 0 then
     begin
      GetMem(FTileData, Sz);
      if FTileBuffer <> nil then
       Move(PByteArray(FTileBuffer)[FBufferIndex * Sz], FTileData^, Sz);
      FBufferIndex := -1;
     end;
    end;
    N := N.Next;
   end;
   FreeMem(FTileBuffer);
   FTileBuffer := nil;
  end;
  inherited;
 end else
 if Value and not United then
 begin
  if FKeepBuffer then
  begin
   FKeepBuffer := False;
   SetUnited(False);
   FKeepBuffer := True;
  end;
  inherited SetUnited(True);
  Sz := FTileBitmap.ImageSize;
  if FKeepBuffer then
   FTileBufferSize := Max(Sz * Count, FTileBufferSize) else
   FTileBufferSize := Sz * Count;
  ReallocMem(FTileBuffer, FTileBufferSize);
  if FTileBuffer <> nil then
  begin
   P := FTileBuffer;
   N := RootNode;
   while N <> nil do
   begin
    with N as TTileItem do
    begin
     Move(FTileData^, P^, Sz);
     if FBufferIndex < 0 then
      FreeMem(FTileData);
     FBufferIndex := Index;
     FTileData := P;
     Inc(P, Sz);
    end;
    N := N.Next;
   end;
  end;
 end;
end;

procedure TTileSet.SwitchFormat(const Flags: array of Word; ATileWidth,
  ATileHeight: Integer; ColFmt: TColorFormat; ColTbl: Pointer);
var
 TileSz: Integer;
 N: TNode;
 P: PByte;
begin
 United := True;

 if ATileWidth >= 0 then
 begin
  FTileBitmap.Width := ATileWidth;
  FTileBitmap.WidthRemainder := 0;
 end;

 if ATileHeight >= 0 then
  FTileBitmap.Height := ATileHeight;

 if ColFmt <> nil then
  FTileBitmap.ColorFormat.Assign(ColFmt);

 if ColTbl = Pointer(-1) then
  FTileBitmap.ImageFormat := ifRGB else
 begin
  FTileBitmap.ImageFormat := ifIndexed;
  if ColTbl <> nil then
   FTileBitmap.ColorTable := ColTbl;
 end;

 if Length(Flags) > 0 then
  FTileBitmap.SetFlags(Flags);

 TileSz := FTileBitmap.ImageSize;
 InitEmptyTile;
{ FreeMem(FEmptyTile.FTileData);
 GetMem(EmptyTile.FTileData, TileSz);
 FillChar(EmptyTile.FTileData^, TileSz, 0);}
 FTileBufferSize := Max(TileSz * Count, FTileBufferSize);
 ReallocMem(FTileBuffer, FTileBufferSize);


 P := FTileBuffer;
 N := RootNode;
 while N <> nil do
 begin
  with N as TTileItem do
  begin
   if FBufferIndex < 0 then
    FreeMem(FTileData);
   FBufferIndex := FIndex;
   FTileData := P;
   Inc(P, TileSz);
  end;
  N := N.Next;
 end;
end;

procedure TTileSet.TileDraw(const Source; Dest: TBitmapContainer; X,
  Y: Integer; ADrawFlags: Integer);
begin
 FTileBitmap.ImageData := @Source;
 FTileBitmap.Draw(Dest, X, Y, ADrawFlags or FDrawFlags);
end;

procedure TTileSet.TileDraw(const Source; Dest: TBitmapContainer; X,
  Y: Integer; Cvt: TBitmapConverter);
begin
 FTileBitmap.ImageData := @Source;
 FTileBitmap.Draw(Dest, X, Y, Cvt);
end;

procedure TTileSet.TileFlip(const Source; var Dest; X, Y: Boolean);
var
 Bmp: TBitmapContainer;
begin
 Bmp := TBitmapContainer.Create;
 try
  Bmp.Width := FTileBitmap.Width;
  Bmp.WidthRemainder := FTileBitmap.WidthRemainder;
  Bmp.Height := FTileBitmap.Height;
  Bmp.ImageFormat := FTileBitmap.ImageFormat;
  Bmp.Compressed := False;
  Bmp.FlagsList := FTileBitmap.FlagsList;
  Bmp.Left := 0;
  Bmp.Top := 0;
  Bmp.ImageData := @Dest;
  FTileBitmap.ImageData := @Source;
  FTileBitmap.Draw(Bmp, 0,0, DRAW_NO_CONVERT or
                          (Ord(X <> False) shl DRAW_FLIPS_SHIFT) or
                          (Ord(Y <> False) shl (DRAW_FLIPS_SHIFT + 1)));
 finally
  Bmp.Free;
 end;
end;

procedure TTileSet.TileLoad(Src: TBitmapContainer; var Dest; X, Y, ADrawFlags: Integer);
begin
 FTileBitmap.ImageData := @Dest;
 Src.Draw(FTileBitmap, -X, -Y, ADrawFlags);
end;

procedure TTileSet.TileLoad(Src: TBitmapContainer; var Dest; X, Y: Integer;
  Cvt: TBitmapConverter);
begin
 FTileBitmap.ImageData := @Dest;
 Src.Draw(FTileBitmap, -X, -Y, Cvt);
end;

procedure TTileSet.WriteData(Stream: TStream; var Header: TSectionHeader);
var
 I, J: Integer;
 tsh: ^TTileSetHeader;
 Flags: TCompositeFlags;
 IndexInfo: array of LongInt;
 ColInfo: array of Byte;
 CFmt: TColorFormatRec;
 Rec: TProgressRec;
 SavePos{, Save}: Int64;
begin
 Flags := FTileBitmap.FlagsList;

 tsh := Addr(Header.dh);
 Rec.Stream := Stream;
 Rec.Header := @Header;
 Rec.ProgressID := SPID_WRITE_DATA;

 //Save := Stream.Position;

 Write(Rec, Pointer(FName)^, tsh.V2.tlhNameLength shl 1);

 Write(Rec, Flags[0], tsh.V2.tlhTileFormatFlagsCount shl 1);


 if FTileBitmap.ImageFormat <> ifIndexed then
 begin
  with FTileBitmap.ColorFormat do
  begin
   CFmt.colRedMask := RedMask;
   CFmt.colGreenMask := GreenMask;
   CFmt.colBlueMask := BlueMask;
   CFmt.colAlphaMask := AlphaMask;
   CFmt.colBits := UsedBits;
   CFmt.colSize := ColorSize;
   CFmt.colFlags := Flags;
   CFmt.colReserved1 := 0;
  end;

  Write(Rec, CFmt, SizeOf(CFmt));
 end;

 
// Write(Rec, FEmptyBlocks[0], Length(FEmptyBlocks) * SizeOf(TEmptyBlock));

 SavePos := Stream.Position;
 for I := 0 to tsh.V2.bh.tlhAttrCount - 1 do
  AttrDefSaveToStream(FAttrDefList[I], Stream);
 Rec.Value := Stream.Position - SavePos; //368
 Progress(Self, Rec);

 Write(Rec, FEmptyTile.FTileData^, TileBitmap.ImageSize);

 WriteTilesInternal(Rec, 0, Count);


 if tsh.V2.tlhSpecialFlags and TSHF_INDEX_INFO <> 0 then
 begin
  SetLength(IndexInfo, Count);
  for I := 0 to Count - 1 do
   IndexInfo[I] := Items[I].FTileIndex;
  Write(Rec, IndexInfo[0], Count shl 2);
 end;

 if tsh.V2.tlhSpecialFlags and TSHF_COLOR_INFO <> 0 then
 begin
  SetLength(ColInfo, Count);
  for I := 0 to Count - 1 do
   ColInfo[I] := Items[I].FFirstColor;
  Write(Rec, ColInfo[0], Length(ColInfo));
 end;

 SavePos := Stream.Position;
 with FEmptyTile do
  for J := 0 to Length(FAttributes) - 1 do
   VariantSaveToStream(FAttributes[J], Stream);
 Rec.Value := Stream.Position - SavePos; // 20, 118441
 Progress(Self, Rec);

 SavePos := Stream.Position;
 for I := 0 to Count - 1 do
  with Nodes[I] as TTileItem do
   for J := 0 to Length(FAttributes) - 1 do
    VariantSaveToStream(FAttributes[J], Stream);
 Rec.Value := Stream.Position - SavePos; // 99644, 218085
 Progress(Self, Rec);

 //if Stream.Position - Save <> 0 then;
end;

procedure TTileSet.WriteTilesInternal(var Rec: TProgressRec; AIndex, ACount: Integer);
var
 Sz, Cnt: Integer;
begin
 Sz := FTileBitmap.ImageSize;
 Cnt := 32768 div Sz;
 while ACount > 0 do
 begin
  if Cnt > ACount then
   Cnt := ACount;
  WriteToStream(Rec.Stream, AIndex, Cnt, False);
  Inc(AIndex, Cnt);
  Dec(ACount, Cnt);
  Rec.Value := Sz * Cnt;
  Progress(Self, Rec);
 end;
end;

procedure TTileSet.WriteToStream(Stream: TStream; AIndex, ACount: Integer; AUseEmptyTiles: Boolean);
var
 I, SZ: Integer;
 N: TTileItem;
begin
 SZ := TileSize;
 if not AUseEmptyTiles then
 begin
  N := Items[AIndex];
  while (N <> nil) and (ACount > 0) do
  begin
   Stream.WriteBuffer(N.FTileData^, SZ);
   N := N.Next as TTileItem;
   Dec(ACount);
  end;
 end else
 begin
  for I := AIndex to AIndex + Min(FLastIndex, ACount - 1) do
  begin
   Stream.WriteBuffer(Indexed[I].FTileData^, SZ);
   Dec(ACount);
  end;
 end;
 while (ACount > 0) do
 begin
  Stream.WriteBuffer(FEmptyTile.FTileData^, SZ);
  Dec(ACount);
 end;
end;

procedure TTileItem.SetTileIndex(Value: Integer);
begin
 if Self <> (Owner as TTileSet).FEmptyTile then
  FTileIndex := Value else
  FTileIndex := -1;
end;

{ TTileSetList }

procedure TTileSetList.FillCvtRecList(var List: TCvtRecList;
           const Flags: array of Word; ADrawFlags: Word = 0);
var
 I: Integer;
begin
 CvtRecListFree(List);
 SetLength(List, Count);
 for I := 0 to Count - 1 do
 begin
  ConvertRecInit(List[I]);
  Sets[I].FillConvertRec(List[I], Flags, ADrawFlags);
 end;
end;

function TTileSetList.GetSet(X: Integer): TTileSet;
begin
 Result := Nodes[X] as TTileSet;
end;

procedure TTileSetList.Initialize;
begin
 inherited;
 FAssignableClass := TTileSetList;
 FNodeClass := TTileSet;
 FSignature := TLST_SIGN;
end;

end.
