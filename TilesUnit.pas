unit TilesUnit;

(*   Copyright!  *)
(* Made by Jinni *)
(*  Do not sell! *)

interface

uses SysUtils, Classes, NodeLst, HexUnit, MyClasses, BitmapEx;

type
 TTile2bit = Array[0..7, 0..1] of Byte;
 TTile4bit_SNES = array[0..1] of TTile2bit;
 TTile2bit_NES = Array[0..1, 0..7] of Byte;
 TTile4bit = Array[0..7, 0..3] of Byte;
 TTile8bit = Array[0..7, 0..7] of Byte;
 T16x16tile4 = array [0..15, 0..7] of Byte;
 T16x16tile = array [0..15, 0..15] of Byte;
 PLine = ^TLine;
 TLine = array [0..15] of Byte;
 TLineRec = packed record
  Lo: Int64;
  Hi: Int64;
 end;

 TTileItem = class(TNode)
  private
    FTilePointer: Pointer;
    FTileIndex: Integer;
    FFirstColor: Byte;
  protected
    function GetTileSize: Integer; virtual;
    function GetWidth: Integer; virtual;
    function GetHeight: Integer; virtual;
    function GetBitCount: Integer; virtual;
    procedure Initialize; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); virtual; abstract;    
  public
    property TileIndex: Integer read FTileIndex write FTileIndex;
    property TileSize: Integer read GetTileSize;
    property TileData: Pointer read FTilePointer write FTilePointer;
    property FirstColor: Byte read FFirstColor write FFirstColor;
    property Width: Integer read GetWidth;
    property Height: Integer read GetHeight;
    property BitCount: Integer read GetBitCount;

    procedure Flip(X, Y: Boolean; var Dest); virtual;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); virtual;
    procedure BufLoad(const Source; RowStride: Integer); virtual;
    procedure BufDraw4bpp(var Dest; Odd: Boolean; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); virtual;
    procedure BufLoad4bpp(const Source; Odd: Boolean; RowStride: Integer); virtual;

    procedure Assign(Source: TNode); override;
 end;

 PTileCvtRec = ^TTileCvtRec;
 TTileCvtRec = record
  tcvtBmp: TBitmapContainer;
  tcvtFrom4bpp: TBitmapConverter;
  tcvtFrom8bpp: TBitmapConverter;
  tcvtTo4bpp: array [Boolean, Boolean] of TBitmapConverter;
  tcvtTo8bpp: array [Boolean, Boolean] of TBitmapConverter;
 end;

 TCustomizableTile = class(TTileItem)
  private
    FCvtPtr: PTileCvtRec;
    FBitmap: TBitmapContainer;
    function GetColorTable: Pointer;
    procedure SetColorTable(Value: Pointer);
    function GetColorFormat: TColorFormat;
    procedure SetColorFormat(Value: TColorFormat);
    function GetImageFormat: TImageFormat;
    procedure SetImageFormat(Value: TImageFormat);
    function GetTransparentColor: LongWord;
    procedure SetTransparentColor(Value: LongWord);
  protected
    function GetTileSize: Integer; override;
    function GetWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetBitCount: Integer; override;
    procedure Initialize; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;    
  public
    FTileAttr: array of Int64;
    property ColorFormat: TColorFormat read GetColorFormat write SetColorFormat;
    property ColorTable: Pointer read GetColorTable write SetColorTable;
    property ImageFormat: TImageFormat read GetImageFormat write SetImageFormat;
    property TransparentColor: LongWord read GetTransparentColor write SetTransparentColor;

    procedure Reallocate(AWidth, AHeight: Integer; const Flags: array of Word;
                       ColFmt: TColorFormat; ColTbl: Pointer; ImgFmt: TImageFormat);

    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
    procedure BufDraw4bpp(var Dest; Odd: Boolean; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad4bpp(const Source; Odd: Boolean; RowStride: Integer); override;

    procedure Assign(Source: TNode); override;
    destructor Destroy; override;
 end;

 TTileItems = array of TTileItem;
 TTileClass = class of TTileItem;

 TTileHolder2BPP_NES = class(TTileItem)
  protected
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile2BPP_NES = class(TTileHolder2BPP_NES)
  private
    FData: TTile2bit_NES;
  protected
    procedure Initialize; override;
  public
    property Data: TTile2bit_NES read FData write FData;
 end;

 TTileHolder4BPP = class(TTileItem)
  protected
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile4BPP = class(TTileHolder4BPP)
  private
    FData: TTile4bit;
  protected
    procedure Initialize; override;
  public
    property Data: TTile4bit read FData write FData;
 end;

 TTileHolder4BPP_MSX = class(TTileItem)
  protected
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
    procedure BufDraw4bpp(var Dest; Odd: Boolean; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad4bpp(const Source; Odd: Boolean; RowStride: Integer);
              override;
 end;

 TTile4BPP_MSX = class(TTileHolder4BPP_MSX)
  private
    FData: TTile4bit;
  protected
    procedure Initialize; override;
  public
    property Data: TTile4bit read FData write FData;
 end;

 TTileHolder4BPP_SNES = class(TTileItem)
  protected
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile4BPP_SNES = class(TTileHolder4BPP_SNES)
  private
    FData: TTile4bit_SNES;
  protected
    procedure Initialize; override;
  public
    property Data: TTile4bit_SNES read FData write FData;
 end;

 TTileHolder8BPP = class(TTileItem)
  protected
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile8BPP = class(TTileHolder8BPP)
  private
    FData: TTile8bit;
  protected
    procedure Initialize; override;
  public
    property Data: TTile8bit read FData write FData;
 end;

 TTile4bpp16x16_Holder = class(TTileItem)
  protected
    function GetWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile4bpp16x16 = class(TTile4bpp16x16_Holder)
  private
    FData: T16x16tile4;
  protected
    procedure Initialize; override;
  public
    property Data: T16x16tile4 read FData write FData;
 end;

 TTileHolder16x16 = class(TTileItem)
  protected
    function GetWidth: Integer; override;
    function GetHeight: Integer; override;
    function GetTileSize: Integer; override;
    function GetBitCount: Integer; override;
    procedure FillHeader(var Header; var Flags: TCompositeFlags); override;
  public
    procedure Flip(X, Y: Boolean; var Dest); override;
    procedure BufDraw(var Dest; RowStride: Integer;
           XFlip: Boolean = False; YFlip: Boolean = False); override;
    procedure BufLoad(const Source; RowStride: Integer); override;
 end;

 TTile16x16 = class(TTileHolder16x16)
  private
    FData: T16x16tile;
  protected
    procedure Initialize; override;
  public
    property Data: T16x16tile read FData write FData;
 end;

 PEmptyBlock = ^TEmptyBlock;
 TEmptyBlock = packed record
  ebStart: Integer;
  ebEnd: Integer;
 end;

 TEmptyBlocks = array of TEmptyBlock;

 TCustomTiles = class(TStreamedList)
  private
    FTileSetName: WideString;
    FEmptyTile: TTileItem;
    FTransparentColor: LongWord;
    procedure SetTransparentColor(Value: LongWord);
  protected
    FTileSize,
    FTileWidth,
    FTileHeight,
    FTileBPP: Integer;
    FMaxIndex: Integer;
    FTileClassAssign: Boolean;
    procedure Initialize; override;
    procedure SetNodeClass(Value: TNodeClass); override;
    procedure AssignAdd(Source: TNode); override;
    procedure AssignNodeClass(Source: TNodeList); override;
    function GetTilesCount: Integer; virtual;
    function GetTileItem(Index: Integer): TTileItem; virtual;
    procedure AfterChange; virtual;
    procedure ClearData; override;
    procedure InitTileClassInfo(const Header; const Flags: array of Word); virtual;
 public
    property TileSetName: WideString read FTileSetName write FTileSetName;
    property TransparentColor: LongWord read FTransparentColor write SetTransparentColor;
    property MaxIndex: Integer read FMaxIndex write FMaxIndex;
    property EmptyTile: TTileItem read FEmptyTile;
    property TileSize: Integer read FTileSize;
    property TileWidth: Integer read FTileWidth;
    property TileHeight: Integer read FTileHeight;
    property TileBPP: Integer read FTileBPP;
    property TilesCount: Integer read GetTilesCount;
    property List[Index: Integer]: TTileItem read GetTileItem;
    property TileClassAssign: Boolean read FTileClassAssign
                                     write FTileClassAssign;

    constructor Create(TileClass: TTileClass);

    function AddTile: TTileItem; virtual;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;
    procedure AddFromStream(const Stream: TStream; ACount: Integer);
    procedure AddBuffer(Buffer: Pointer; ACount: Integer;
     AClass: TTileClass = nil);
    destructor Destroy; override;
 end;

 TTileSetList = class(TStreamedList)
  private
    function GetSet(X: Integer): TCustomTiles;
  protected
    procedure Initialize; override;
  public
    property Sets[X: Integer]: TCustomTiles read GetSet; default;
    function AddSet(const AName: WideString): TCustomTiles;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;
 end;

 TTileList = class(TCustomTiles)
  private
    FOptimization: Boolean;
    FLastIndex: Integer;
    FEmptyBlocks: TEmptyBlocks;
    FCurrentBlock: PEmptyBlock;
    FFastTiles: TTileItems;
    FFast: Boolean;
    function GetTile(Index: Integer): TTileItem;
  protected
    procedure Initialize; override;
    procedure ClearData; override;
    procedure AssignAdd(Source: TNode); override;
    function GetTilesCount: Integer; override;
    function GetTileItem(Index: Integer): TTileItem; override;
    procedure AfterChange; override;
  public
    property Optimization: Boolean read FOptimization write FOptimization;
    property EmptyBlocks: TEmptyBlocks read FEmptyBLocks write FEmptyBLocks;
    property FastTiles: TTileItems read FFastTiles;
    property Tiles[Index: Integer]: TTileItem read GetTile;

    function AddFixed(Index: Integer): TTileItem;
    function AddOrGet(const Source; var XFlip, YFlip: Boolean): TTileItem;
    constructor Create(TileClass: TTileClass; Optimization: Boolean);
    procedure ResetEmptyBlock;
    procedure AddEmptyBlock(StartIndex, EndIndex: Integer);
    procedure AppendFromStream(const Stream: TStream; Count: Integer;
                               StartIndex: Integer = 0);
    procedure SaveToStream(Stream: TStream); override;                               
    procedure LoadFromStream(Stream: TStream); override;
    procedure MakeArray;
    procedure Assign(Source: TNode); override;
    function FindEmptyIndex(Value: Integer): Integer;
    procedure DeleteEmptyBlock(Index: Integer);
    procedure SetEmptyBlocksLen(Len: Integer);
 end;

 TAttrType = (atDecimal, atHexadecimal);
 TAttrDef = record
  adName: WideString;
  adType: TAttrType;
  adMin: Int64;
  adMax: Int64;
 end;

 TCustomizableTileList = class(TTileList)
  private
    FTileFormatFlags: TCompositeFlags;
    FColorFormat: TColorFormat;
    FColorTable: Pointer;
    FTileImageFormat: TImageFormat;
    FCvtRec: TTileCvtRec;
    FDrawFlags: Word;
    procedure SetAttrCount(Value: Integer);
    function GetAttrCount: Integer;
  protected
    procedure SetNodeClass(Value: TNodeClass); override;
    procedure Initialize; override;
    procedure AfterChange; override;
  public
    FAttrDefList: array of TAttrDef;
    property DrawFlags: Word read FDrawFlags;
    property AttrCount: Integer read GetAttrCount write SetAttrCount;
    property ColorFormat: TColorFormat read FColorFormat;
    property ColorTable: Pointer read FColorTable write FColorTable;
    property TileImageFormat: TImageFormat read FTileImageFormat write FTileImageFormat;
    constructor Create(ATileWidth, ATileHeight: Integer;
        const Flags: array of Word; DrawFlags: Word = 0);
    procedure Reallocate(ATileWidth, ATileHeight: Integer;
        const Flags: array of Word; DrawFlags: Word = 0);
    function AddTile: TTileItem; override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure SaveToStream(Stream: TStream); override;    
    destructor Destroy; override;
 end;

 ETileListError = class(Exception);

const
 MSX_Shifts: array[0..7] of Byte =
 (1 shl 2, 0 shl 2, 3 shl 2, 2 shl 2,
  5 shl 2, 4 shl 2, 7 shl 2, 6 shl 2);

implementation

const
 CHR_SIGN  = Ord('C') or
            (Ord('H') shl 8) or
            (Ord('R') shl 16) or
            (Ord('_') shl 24);

type            
 TMainHeader = packed record
  mhSignature: LongWord;
  mhHeaderSize: LongInt;
 end;

 TTileListHeader = packed record
  tlhCount: LongInt;
  tlhWidth: Integer;
  tlhHeight: Integer;
  tlhTileImageFormat: TImageFormat;
  tlhTileDrawFlags: Word;
  tlhTileFormatFlagsCount: Byte;
  tlhNameLength: Byte;
 end;

procedure TileListFormatError;
begin
 raise ETileListError.Create('Tile list data format error');
end;


{ TTileItem }

procedure TTileItem.Assign(Source: TNode);
var
 Tile: Pointer;
 W, H: Integer;
begin
 inherited;
 with Source as TTileItem do
 if ClassType <> Self.ClassType then
 begin
  W := Max(Width, Self.Width);
  H := Max(Height, Self.Height);
  Self.FFirstColor := FFirstColor;
  GetMem(Tile, W * H);
  try
   BufDraw(Tile^, W);
   Self.FTileIndex := FTileIndex;
   Self.BufLoad(Tile^, W);
  finally
   FreeMem(Tile);
  end;
 end else
  Move(FTilePointer^, Self.FTilePointer^, TileSize);
end;

procedure TTileItem.BufDraw(var Dest; RowStride: Integer; XFlip,
  YFlip: Boolean);
begin
//do nothing
end;

procedure TTileItem.BufDraw4bpp(var Dest; Odd: Boolean;
  RowStride: Integer; XFlip, YFlip: Boolean);
begin
//do nothing
end;

procedure TTileItem.BufLoad(const Source; RowStride: Integer);
begin
//do nothing
end;

procedure TTileItem.BufLoad4bpp(const Source; Odd: Boolean;
  RowStride: Integer);
begin
 //do nothing
end;

procedure TTileItem.Flip(X, Y: Boolean; var Dest);
begin
 //do nothing
end;

function TTileItem.GetBitCount: Integer;
begin
 Result := 0;
end;

function TTileItem.GetHeight: Integer;
begin
 Result := 8;
end;

function TTileItem.GetTileSize: Integer;
begin
 Result := 0;
end;

function TTileItem.GetWidth: Integer;
begin
 Result := 8;
end;

procedure TTileItem.Initialize;
begin
 FAssignableClass := TTileItem;
end;

{ TTileHolder2BPP_NES }

procedure TTileHolder2BPP_NES.BufDraw(var Dest; RowStride: Integer;
  XFlip, YFlip: Boolean);
var
 XX, YY: Integer;
 Src1, Src2: Byte;
 Dst, P: PByte;
 FData: ^TTile2bit_NES;
begin
 Dst := @Dest;
 FData := TileData;
 if not XFlip then Inc(Dst, 7);
 for YY := 0 to 7 do
 begin
  P := Dst;
  if YFlip then
  begin
   Src1 := FData[0, 7 - YY];
   Src2 := FData[1, 7 - YY];
  end else
  begin
   Src1 := FData[0, YY];
   Src2 := FData[1, YY];
  end;
  if not XFlip then for XX := 0 to 7 do
  begin
   P^ := FFirstColor + ((Src1 and 1) or ((Src2 and 1) shl 1));
   Src1 := Src1 shr 1;
   Src2 := Src2 shr 1;
   Dec(P);
  end else for XX := 0 to 7 do
  begin
   P^ := FFirstColor + ((Src1 and 1) or ((Src2 and 1) shl 1));
   Src1 := Src1 shr 1;
   Src2 := Src2 shr 1;
   Inc(P);
  end;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder2BPP_NES.BufLoad(const Source; RowStride: Integer);
var
 XX, YY: Integer;
 Clr: Byte;
 Dst1, Dst2: PByte;
 Src, P: PByte;
 FData: ^TTile2bit_NES;
begin
 Src := @Source;
 FData := TileData;
 Dst1 := Addr(FData[0, 0]);
 Dst2 := Addr(FData[1, 0]);
 for YY := 0 to 7 do
 begin
  P := Src;
  Dst1^ := 0;
  Dst2^ := 0;
  for XX := 7 downto 0 do
  begin
   Clr := P^ - FFirstColor;
   Dst1^ := Dst1^ or ((Clr and 1) shl XX);
   Dst2^ := Dst2^ or (((Clr shr 1) and 1) shl XX);
   Inc(P);
  end;
  Inc(Dst1);
  Inc(Dst2);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder2BPP_NES.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 8;
 H.tlhHeight := 8;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := (2 - 1) or IMG_PLANAR;
end;

procedure TTileHolder2BPP_NES.Flip(X, Y: Boolean; var Dest);
var
 Tile: TTile2bit_NES absolute Dest;
 XX, YY, I: Integer; Src, Dst: Byte;
 FData: ^TTile2bit_NES;
begin
 FData := TileData;
 if X then
 begin
  for YY := 0 to 7 do
  begin
   for I := 0 to 1 do
   begin
    Src := FData[I, YY];
    Dst := 0;
    for XX := 0 to 7 do
    begin
     Dst := Dst or ((Src and 1) shl XX);
     Src := Src shr 1;
    end;
    if Y then
     Tile[I, 7 - YY] := Dst else
     Tile[I, YY] := Dst;
   end;
  end;
 end else if Y then
 begin
  for YY := 0 to 7 do
  begin
   PWord(Addr(Tile[0, YY]))^ :=
   PWord(Addr(FData[0, 7 - YY]))^;
  end;
 end;
end;

function TTileHolder2BPP_NES.GetBitCount: Integer;
begin
 Result := 2;
end;

function TTileHolder2BPP_NES.GetTileSize: Integer;
begin
 Result := SizeOf(TTile2bit_NES);
end;

{ TTile2BPP_NES }

procedure TTile2BPP_NES.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TTileHolder4BPP }

procedure TTileHolder4BPP.BufDraw(var Dest; RowStride: Integer; XFlip,
  YFlip: Boolean);
var
 XX, YY: Integer;
 Src: Cardinal;
 Dst, P: PByte;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Dst := @Dest;
 if XFlip then Inc(Dst, 7);
 for YY := 0 to 7 do
 begin
  P := Dst;
  if YFlip then
   Src := PCardinal(Addr(FData[7 - YY, 0]))^ else
   Src := PCardinal(Addr(FData[YY, 0]))^;
  if XFlip then for XX := 0 to 7 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Dec(P);
  end else for XX := 0 to 7 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Inc(P);
  end;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder4BPP.BufLoad(const Source; RowStride: Integer);
var
 XX, YY: Integer;
 Dst: PCardinal;
 Src, P: PByte;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Src := @Source;
 Dst := Addr(FData[0, 0]);
 for YY := 0 to 7 do
 begin
  P := Src;
  Dst^ := 0;
  for XX := 0 to 7 do
  begin
   Dst^ := Dst^ or ((Byte(P^ - FFirstColor) and 15) shl (XX shl 2));
   Inc(P);
  end;
  Inc(Dst);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder4BPP.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 8;
 H.tlhHeight := 8;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := (4 - 1) or IMG_BITS_REVERSE;
end;


procedure TTileHolder4BPP.Flip(X, Y: Boolean; var Dest);
var
 Tile: TTile4bit absolute Dest;
 XX, YY: Integer;
 Src, Dst: Cardinal;
 FData: ^TTile4bit;
begin
 FData := TileData;
 if X then
 begin
  for YY := 0 to 7 do
  begin
   Src := PCardinal(Addr(FData[YY, 0]))^;
   Dst := 0;
   XX := 7 shl 2;
   while XX >= 0 do
   begin
    Dst := Dst or ((Src and 15) shl XX);
    Src := Src shr 4;
    Dec(XX, 4);
   end;
   if Y then
    PCardinal(Addr(Tile[7 - YY, 0]))^ := Dst else
    PCardinal(Addr(Tile[YY, 0]))^ := Dst;
  end;
 end else if Y then
 begin
  for YY := 0 to 7 do
  begin
   PCardinal(Addr(Tile[YY, 0]))^ :=
   PCardinal(Addr(FData[7 - YY, 0]))^;
  end;
 end;
end;

function TTileHolder4BPP.GetBitCount: Integer;
begin
 Result := 4;
end;

function TTileHolder4BPP.GetTileSize: Integer;
begin
 Result := SizeOf(TTile4bit);
end;

{ TTile4BPP }

procedure TTile4BPP.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TTileHolder4BPP_MSX }

procedure TTileHolder4BPP_MSX.BufDraw(var Dest; RowStride: Integer;
  XFlip, YFlip: Boolean);
var
 XX, YY: Integer;
 Src: Cardinal;
 Dst, P: PByte;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Dst := @Dest;
 if XFlip then
  Inc(Dst, 6) else
  Inc(Dst);
 for YY := 0 to 7 do
 begin
  P := Dst;
  if YFlip then
   Src := PCardinal(Addr(FData[7 - YY, 0]))^ else
   Src := PCardinal(Addr(FData[YY, 0]))^;
  if XFlip then for XX := 0 to 3 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Inc(P);
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Dec(P, 3);
  end else for XX := 0 to 3 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Dec(P);
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Inc(P, 3);
  end;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder4BPP_MSX.BufDraw4bpp(var Dest; Odd: Boolean;
  RowStride: Integer; XFlip, YFlip: Boolean);
var
 XX, YY: Integer;
 Src: Cardinal;
 Dst, P: PByte;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Dst := @Dest;
 if XFlip then
  Inc(Dst, 3);
 if not Odd then
 for YY := 0 to 7 do
 begin
  P := Dst;
  if YFlip then
   Src := PCardinal(Addr(FData[7 - YY, 0]))^ else
   Src := PCardinal(Addr(FData[YY, 0]))^;
  if XFlip then for XX := 0 to 3 do
  begin
   P^ := (Src and 15) shl 4;
   Src := Src shr 4;
   P^ := P^ or (Src and 15);
   Src := Src shr 4;
   Dec(P);
  end else for XX := 0 to 3 do
  begin
   P^ := Byte(Src);
   Src := Src shr 8;
   Inc(P);
  end;
  Inc(Dst, RowStride);
 end else
 for YY := 0 to 7 do
 begin
  P := Dst;
  if YFlip then
   Src := PCardinal(Addr(FData[7 - YY, 0]))^ else
   Src := PCardinal(Addr(FData[YY, 0]))^;
  if XFlip then for XX := 0 to 3 do
  begin
   P^ := P^ or (Src and $F0);
   Dec(P);                      //01 23 45 67
   P^ := Src and $0F;
   Src := Src shr 8;            //07 65 43 21 00
  end else for XX := 0 to 3 do
  begin
   P^ := P^ or ((Src shr 4) and $0F);
   Inc(P);                      //00 12 34 56
   P^ := Byte(Src) shl 4;
   Src := Src shr 8;
  end;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder4BPP_MSX.BufLoad(const Source; RowStride: Integer);
var
 XX, YY: Integer;
 Dst: PCardinal;
 Src, P: PByte;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Src := @Source;
 Dst := Addr(FData[0, 0]);
 for YY := 0 to 7 do
 begin
  P := Src;
  Dst^ := 0;
  for XX := 0 to 7 do
  begin
   Dst^ := Dst^ or ((Byte(P^ - FFirstColor) and 15) shl MSX_Shifts[XX]);
   Inc(P);
  end;
  Inc(Dst);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder4BPP_MSX.BufLoad4bpp(const Source; Odd: Boolean;
  RowStride: Integer);
var
 YY: Integer;
 Dst: PCardinal;
 P, Src: PByte; SrcD: PCardinal absolute Src;
 FData: ^TTile4bit;
begin
 FData := TileData;
 Src := @Source;
 Dst := Addr(FData[0, 0]);
 if not Odd then
 for YY := 0 to 7 do
 begin
  Dst^ := SrcD^;
  Inc(Dst);
  Inc(Src, RowStride);
 end else
 for YY := 0 to 7 do
 begin
  Dst^ := SrcD^ shl 4;
  P := Src;
  Inc(P, 4);
  Dst^ := Dst^ or (P^ shr 4);
  Inc(Dst);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder4BPP_MSX.FillHeader(var Header;
  var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 8;
 H.tlhHeight := 8;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := 4 - 1;
end;

procedure TTileHolder4BPP_MSX.Flip(X, Y: Boolean; var Dest);
var
 Tile: TTile4bit absolute Dest;
 XX, YY: Integer;
 Src, Dst: Cardinal;
 FData: ^TTile4bit;
begin
 FData := TileData;
 if X then
 begin
  for YY := 0 to 7 do
  begin
   Src := PCardinal(Addr(FData[YY, 0]))^;
   Dst := 0;
   for XX := 7 downto 0 do
   begin
    Dst := Dst or ((Src and 15) shl MSX_Shifts[XX]);
    Src := Src shr 4;
   end;
   if Y then
    PCardinal(Addr(Tile[7 - YY, 0]))^ := Dst else
    PCardinal(Addr(Tile[YY, 0]))^ := Dst;
  end;
 end else if Y then
 begin
  for YY := 0 to 7 do
  begin
   PCardinal(Addr(Tile[YY, 0]))^ :=
   PCardinal(Addr(FData[7 - YY, 0]))^;
  end;
 end;
end;

function TTileHolder4BPP_MSX.GetBitCount: Integer;
begin
 Result := 4;
end;

function TTileHolder4BPP_MSX.GetTileSize: Integer;
begin
 Result := SizeOf(TTile4bit);
end;

{ TTile4BPP_MSX }

procedure TTile4BPP_MSX.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TTileHolder8BPP }

procedure TTileHolder8BPP.BufDraw(var Dest; RowStride: Integer; XFlip,
  YFlip: Boolean);
var
 XX, YY: Integer;
 Dst, P, Src: PByte;
 Src64: PInt64 absolute Src;
 Dst64: PInt64 absolute Dst;
 FData: ^TTile8bit;
begin
 FData := TileData;
 Dst := @Dest;
 if XFlip then Inc(Dst, 7);
 for YY := 0 to 7 do
 begin
  if YFlip then
   Src := Addr(FData[7 - YY, 0]) else
   Src := Addr(FData[YY, 0]);
  if XFlip then
  begin
   P := Dst;
   for XX := 0 to 7 do
   begin
    P^ := Src^;
    Inc(Src);
    Dec(P);
   end;
  end else Dst64^ := Src64^;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder8BPP.BufLoad(const Source; RowStride: Integer);
var
 YY: Integer;
 Dst: PInt64; Src: PByte;
 Src64: PInt64 absolute Src;
 FData: ^TTile8bit;
begin
 FData := TileData;
 Src := @Source;
 Dst := Addr(FData[0, 0]); 
 for YY := 0 to 7 do
 begin
  Dst^ := Src64^;
  Inc(Dst);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder8BPP.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 8;
 H.tlhHeight := 8;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := 8 - 1;
end;

procedure TTileHolder8BPP.Flip(X, Y: Boolean; var Dest);
var
 Tile: TTile8bit absolute Dest;
 YY: Integer; Src: Int64;
 FData: ^TTile8bit;
begin
 FData := TileData;
 if X then
 begin
  for YY := 0 to 7 do
  begin
   Src := SwapInt64(PInt64(Addr(FData[YY, 0]))^);
   if Y then
    PInt64(Addr(Tile[7 - YY, 0]))^ := Src else
    PInt64(Addr(Tile[YY, 0]))^ := Src;
  end;
 end else if Y then
 begin
  for YY := 0 to 7 do
  begin
   PInt64(Addr(Tile[YY, 0]))^ :=
   PInt64(Addr(FData[7 - YY, 0]))^;
  end;
 end;
end;

function TTileHolder8BPP.GetBitCount: Integer;
begin
 Result := 8;
end;

function TTileHolder8BPP.GetTileSize: Integer;
begin
 Result := SizeOf(TTile8bit);
end;

{ TTile8BPP }

procedure TTile8BPP.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TTileList }

procedure TTileList.AddEmptyBlock(StartIndex, EndIndex: Integer);
var L: Integer;
begin
 L := Length(FEmptyBlocks);
 SetLength(FEmptyBlocks, L + 1);
 with FEmptyBlocks[L] do
 begin
  ebStart := StartIndex;
  ebEnd := EndIndex;
 end;
 ResetEmptyBlock;
end;

function TTileList.AddFixed(Index: Integer): TTileItem;
begin
 Result := Tiles[Index];
 if Result = NIL then
 begin
  Result := AddTile;
  Result.FTileIndex := Index;
  if FMaxIndex < Index then FMaxIndex := Index;
  AfterChange;
 end;
end;

function TTileList.AddOrGet(const Source; var XFlip,
  YFlip: Boolean): TTileItem;
var
 Buf: Pointer;
 I, SZ: Integer;
 Link: ^TTileItem;
begin
 Buf := NIL;
 FFast := False;
 Link := RootLink;
 for I := 0 to Count - 1 do
 begin
  Result := Link^;
  with Result do
  begin
   SZ := TileSize;
   if CompareMem(@Source, TileData, SZ) then
   begin
    XFlip := False;
    YFlip := False;
    Exit;
   end;
   if FOptimization then
   begin
    ReallocMem(Buf, SZ);
    Flip(False, True, Buf^);
    if CompareMem(@Source, Buf, SZ) then
    begin
     XFlip := False;
     YFlip := True;
     Exit;
    end;
    Flip(True, False, Buf^);
    if CompareMem(@Source, Buf, SZ) then
    begin
     XFlip := True;
     YFlip := False;
     Exit;
    end;
    Flip(True, True, Buf^);
    if CompareMem(@Source, Buf, SZ) then
    begin
     XFlip := True;
     YFlip := True;
     Exit;
    end;
   end;
  end;
  Inc(Link);
 end;
 Result := AddTile;
 Move(Source, Result.TileData^, FEmptyTile.TileSize);
 XFlip := False;
 YFlip := False;
 if FCurrentBlock = NIL then with Result do
 begin
  FTileIndex := Count - 1;
  if FMaxIndex < FTileIndex then FMaxIndex := FTileIndex;
 end else
 with FCurrentBlock^ do
 begin
  if FLastIndex < ebStart then FLastIndex := ebStart else
  if FLastIndex > ebEnd then
  begin
   Inc(FCurrentBlock);
   if Cardinal(FCurrentBlock) <=
      Cardinal(Addr(FEmptyBlocks[Length(FEmptyBlocks) - 1])) then
    FLastIndex := FCurrentBlock.ebStart else
   begin
    FLastIndex := Count - 1;
    FCurrentBlock := NIL;
   end;
  end;
  Result.FTileIndex := FLastIndex;
  if FMaxIndex < FLastIndex then FMaxIndex := FLastIndex;
  Inc(FLastIndex);
 end;
end;

procedure TTileList.AfterChange;
begin
 FFast := False;
end;

procedure TTileList.AppendFromStream(const Stream: TStream; Count,
  StartIndex: Integer);
var
 I: Integer;
begin
 if StartIndex < 0 then StartIndex := 0;
 for I := 0 to Count - 1 do with AddTile do
 begin
  FTileIndex := StartIndex;
  Inc(StartIndex);
  Stream.ReadBuffer(TileData^, TileSize);
 end;
 if StartIndex - 1 > FMaxIndex then FMaxIndex := StartIndex - 1;
 AfterChange;
end;

procedure TTileList.Assign(Source: TNode);
var
 I: Integer;
 Block: PEmptyBlock;
 Src: TTileList absolute Source; 
begin
 if Source is TTileList then
 begin
  SetLength(FEmptyBlocks, Length(Src.FEmptyBlocks));
  Move(Src.FEmptyBlocks[0], FEmptyBlocks[0],
       Length(FEmptyBlocks) * SizeOf(TEmptyBlock));
  inherited;
  FOptimization := Src.FOptimization;
  FLastIndex := Src.FLastIndex;
  FMaxIndex := Src.FMaxIndex;
  Block := Pointer(FEmptyBlocks);
  with Src do if FCurrentBlock <> NIL then
  begin
   for I := 0 to Length(FEmptyBlocks) - 1 do
   begin
    if CompareMem(FCurrentBlock, Block, SizeOf(TEmptyBlock)) then
    with Self do
    begin
     FCurrentBlock := Block;
     Break;
    end;
    Inc(Block);
   end;
  end else Self.FCurrentBlock := NIL;
  FFast := False;
 end else
 begin
  Finalize(FEmptyBlocks);
  FCurrentBlock := nil;
  inherited;
 end;
end;

procedure TTileList.AssignAdd(Source: TNode);
begin
 if FindEmptyIndex((Source as TTileItem).FTileIndex) < 0 then
  inherited;
end;

procedure TTileList.ClearData;
begin
 inherited;
 FLastIndex := -1;
 FFast := False;
 FCurrentBlock := Pointer(FEmptyBlocks);
 Finalize(FFastTiles);
end;

constructor TTileList.Create(TileClass: TTileClass; Optimization: Boolean);
begin
 inherited Create(TileClass);
 FOptimization := Optimization;
end;

procedure TTileList.DeleteEmptyBlock(Index: Integer);
var
 L: Integer;
begin
 if Index >= 0 then
 begin
  L := Length(FEmptyBlocks);
  if Index = L - 1 then SetLength(FEmptyBlocks, L - 1) else
  if Index < L - 1 then
  begin
   Move(FEmptyBlocks[Index + 1], FEmptyBlocks[Index],
      ((L - Index) - 1) * SizeOf(TEmptyBlock));
   SetLength(FEmptyBlocks, L - 1);
  end else Exit;
  ResetEmptyBlock;
 end;
end;

function TTileList.FindEmptyIndex(Value: Integer): Integer;
var
 I: Integer;
begin
 for I := 0 to Length(FEmptyBlocks) - 1 do with FEmptyBlocks[I] do
 if (Value >= ebStart) and (Value <= ebEnd) then
 begin
  Result := I;
  Exit;
 end;
 Result := -1;
end;

function TTileList.GetTile(Index: Integer): TTileItem;
var
 Link: ^TTileItem;
 I: Integer;
begin
 Link := RootLink;
 for I := 0 to Count - 1 do
 begin
  Result := Link^;
  if Index = Result.FTileIndex then Exit;
  Inc(Link);
 end;
 Result := NIL;
end;

function TTileList.GetTileItem(Index: Integer): TTileItem;
begin
 if not FFast then MakeArray;
 if Index >= Length(FFastTiles) then
  Result := FEmptyTile else
  Result := FFastTiles[Index];
end;

function TTileList.GetTilesCount: Integer;
begin
 if not FFast then MakeArray;
 Result := Length(FFastTiles);
end;

procedure TTileList.Initialize;
begin
 inherited;
 FLastIndex := -1;
end;

procedure TTileList.LoadFromStream(Stream: TStream);
begin
 inherited;
 // TODO: finish me!!!
end;

procedure TTileList.MakeArray;
var
 Tile: TTileItem; I: Integer;
begin
 FFast := True;
 SetLength(FFastTiles, FMaxIndex + 1);
 for I := 0 to FMaxIndex do
 begin
  Tile := Tiles[I];
  if Tile = NIL then
   FFastTiles[I] := FEmptyTile else
   FFastTiles[I] := Tile;
 end;
end;

procedure TTileList.ResetEmptyBlock;
begin
 FLastIndex := -1;
 FCurrentBlock := Pointer(FEmptyBlocks);
end;

procedure TTileList.SaveToStream(Stream: TStream);
begin
  inherited;
 // TODO: finish me!!!
end;

procedure TTileList.SetEmptyBlocksLen(Len: Integer);
begin
 SetLength(FEmptyBlocks, Len);
 ResetEmptyBlock;
end;

{ TTileHolder4BPP_SNES }

procedure TTileHolder4BPP_SNES.BufDraw(var Dest; RowStride: Integer;
  XFlip, YFlip: Boolean);
begin
  inherited;

end;

procedure TTileHolder4BPP_SNES.BufLoad(const Source; RowStride: Integer);
{var
 XX, YY: Integer;
 Clr: Byte;
 Dst1, Dst2: PByte;
 Src, P: PByte;
 FData: ^TTile4bit_SNES;   }
begin
{ Src := @Source;
 FData := TileData;
 Dst1 := Addr(FData[0, 0]);
 Dst2 := Addr(FData[1, 0]);
 for YY := 0 to 7 do
 begin
  P := Src;
  Dst1^ := 0;
  Dst2^ := 0;
  for XX := 7 downto 0 do
  begin
   Clr := P^ - FFirstColor;
   Dst1^ := Dst1^ or ((Clr and 1) shl XX);
   Dst2^ := Dst2^ or (((Clr shr 1) and 1) shl XX);
   Inc(P);
  end;
  Inc(Dst1);
  Inc(Dst2);
  Inc(Src, RowStride);
 end;    }
end;

procedure TTileHolder4BPP_SNES.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 8;
 H.tlhHeight := 8;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 2;
 SetLength(Flags, 2);
 Flags[0] := (2 - 1) or IMG_PLANAR or IMG_PLANE_VECTOR;
 Flags[1] := (2 - 1) or IMG_PLANAR or IMG_PLANE_VECTOR;
end;

procedure TTileHolder4BPP_SNES.Flip(X, Y: Boolean; var Dest);
var
 Tile: TTile4bit_SNES absolute Dest;
 I, J, XX, YY: Integer;
 Src, Dst: Byte;
 FData: ^TTile4bit_SNES;
begin
 FData := TileData;
 if X then
 begin
  for I := 0 to 1 do
  for YY := 0 to 7 do
  begin
   for J := 0 to 1 do
   begin
    Src := PByte(Addr(FData[I][YY, J]))^;
    Dst := 0;
    for XX := 7 downto 0 do
    begin
     Dst := Dst or ((Src and 1) shl XX);
     Src := Src shr 1;
    end;
    if Y then
     PByte(Addr(Tile[I][7 - YY, J]))^ := Dst else
     PByte(Addr(Tile[I][YY, J]))^ := Dst;
   end;
  end;
 end else if Y then
 begin
  for I := 0 to 1 do
  for YY := 0 to 7 do
  begin
   PWord(Addr(Tile[I][YY, 0]))^ :=
   PWord(Addr(FData[I][7 - YY, 0]))^;
  end;
 end;
end;

function TTileHolder4BPP_SNES.GetBitCount: Integer;
begin
 Result := 4;
end;

function TTileHolder4BPP_SNES.GetTileSize: Integer;
begin
 Result := SizeOf(TTile4bit_SNES);
end;

{ TTile4BPP_SNES }

procedure TTile4BPP_SNES.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TCustomTiles }

procedure TCustomTiles.AddBuffer(Buffer: Pointer; ACount: Integer;
  AClass: TTileClass);
var
 I: Integer;
 PB: PByte;
begin
 PB := Buffer;
 if AClass <> nil then
  NodeClass := AClass;
 for I := 0 to ACount - 1 do
 with AddTile do
 begin
  FTileIndex := Index;
  FTilePointer := PB;
  Inc(PB, TileSize);
 end;
 AfterChange;
end;

procedure TCustomTiles.AddFromStream(const Stream: TStream; ACount: Integer);
var
 I: Integer;
begin
 for I := 0 to ACount - 1 do
  Stream.ReadBuffer(AddTile.TileData^, TileSize);
end;

function TCustomTiles.AddTile: TTileItem;
begin
 Result := AddNode as TTileItem;
 Inc(FMaxIndex);
 Result.FTileIndex := FMaxIndex;
 AfterChange;
end;

procedure TCustomTiles.AfterChange;
begin
// do nothing
end;

procedure TCustomTiles.AssignAdd(Source: TNode);
begin
 AddTile.Assign(Source);
end;

procedure TCustomTiles.AssignNodeClass(Source: TNodeList);
begin
 if FTileClassAssign then
  inherited;
end;

procedure TCustomTiles.ClearData;
begin
 FMaxIndex := -1;
end;

constructor TCustomTiles.Create(TileClass: TTileClass);
begin
 inherited Create;
 NodeClass := TileClass;
end;

destructor TCustomTiles.Destroy;
begin
 FEmptyTile.Free;
 inherited;
end;

function TCustomTiles.GetTileItem(Index: Integer): TTileItem;
begin
 Result := Nodes[Index] as TTileItem;
end;

function TCustomTiles.GetTilesCount: Integer;
begin
 Result := Count;
end;

procedure TCustomTiles.Initialize;
begin
 NodeClass := TTileItem;
 FAssignableClass := TTileList;
 FTileClassAssign := True;
 FMaxIndex := -1;
end;

procedure TCustomTiles.InitTileClassInfo(const Header; const Flags: array of Word);
var
 H: TTileListHeader absolute Header;
begin
 if (H.tlhTileImageFormat <> ifIndexed) or
    (H.tlhTileDrawFlags <> 0) then
  TileListFormatError;
 case Flags[0] of
  4-1: if (Length(Flags) > 1) or
          (H.tlhWidth <> H.tlhHeight) then TileListFormatError Else
       case H.tlhWidth of
         8:  NodeClass := TTile4bpp_MSX;
        16:  NodeClass := TTile4bpp16x16;
        else TileListFormatError;
       end;
  //------------------------//
  8-1: if (Length(Flags) > 1) or
          (H.tlhWidth <> H.tlhHeight) then TileListFormatError Else
       case H.tlhWidth of
         8:  NodeClass := TTile8BPP;
        16:  NodeClass := TTile16x16;
        else TileListFormatError;
       end;
  //------------------------//
  (4-1) or IMG_BITS_REVERSE:
  if (Length(Flags) > 1) or
     (H.tlhWidth <> 8) or
     (H.tlhHeight <> 8) then
   TileListFormatError else
   NodeClass := TTile4BPP;
  //------------------------//
  (2-1) or IMG_PLANAR:
  if (Length(Flags) > 1) or
     (H.tlhWidth <> 8) or
     (H.tlhHeight <> 8) then
   TileListFormatError else
   NodeClass := TTile2BPP_NES;
  //------------------------//
  (2-1) or IMG_PLANAR or IMG_PLANE_VECTOR:
  if (Length(Flags) <> 2) or
     (H.tlhWidth <> 8) or
     (H.tlhHeight <> 8) or
     (Flags[1] <> (2-1) or IMG_PLANAR or IMG_PLANE_VECTOR) then
   TileListFormatError else
   NodeClass := TTile4BPP_SNES;
  //------------------------//   
  else // not supported
   TileListFormatError;
 end;
end;

procedure TCustomTiles.LoadFromStream(Stream: TStream);
var
 I: Integer;
 MainHeader: TMainHeader;
 Header: TTileListHeader;
 Flags: array of Word;
 ColInfo: array of Byte; 
begin
 Clear;
 Stream.ReadBuffer(MainHeader, SizeOf(TMainHeader));
 if (MainHeader.mhSignature <> CHR_SIGN) or
    (MainHeader.mhHeaderSize <> SizeOf(TTileListHeader)) then
  TileListFormatError;
 Stream.ReadBuffer(Header, SizeOf(TTileListHeader));
 SetLength(FTileSetName, Header.tlhNameLength);
 Stream.ReadBuffer(Pointer(FTileSetName)^, Length(FTileSetName) shl 1);
 SetLength(Flags, Header.tlhTileFormatFlagsCount);
 Stream.ReadBuffer(Pointer(Flags)^, Length(Flags) shl 1);
 InitTileClassInfo(Header, Flags);
 AddFromStream(Stream, Header.tlhCount);
 SetLength(ColInfo, TilesCount);
 Stream.ReadBuffer(Pointer(ColInfo)^, Length(ColInfo));
 for I := 0 to Length(ColInfo) - 1 do
  List[I].FFirstColor := ColInfo[I];
end;

procedure TCustomTiles.SaveToStream(Stream: TStream);
var
 I: Integer;
 MainHeader: TMainHeader;
 Header: TTileListHeader;
 Flags: TCompositeFlags;
 ColInfo: array of Byte;
begin
 MainHeader.mhSignature := CHR_SIGN;
 MainHeader.mhHeaderSize := SizeOf(TTileListHeader);
 Stream.WriteBuffer(MainHeader, SizeOf(TMainHeader));
 Header.tlhCount := TilesCount;
 FEmptyTile.FillHeader(Header, Flags);
 Header.tlhNameLength := Min(Length(FTileSetName), 255);
 Stream.WriteBuffer(Header, SizeOf(TTileListHeader));
 Stream.WriteBuffer(Pointer(FTileSetName)^, Header.tlhNameLength shl 1);
 Stream.WriteBuffer(Pointer(Flags)^, Header.tlhTileFormatFlagsCount shl 1);
 for I := 0 to TilesCount - 1 do
  Stream.WriteBuffer(List[I].TileData^, TileSize);
 SetLength(ColInfo, TilesCount);
 for I := 0 to Length(ColInfo) - 1 do
  ColInfo[I] := List[I].FFirstColor;
 Stream.WriteBuffer(Pointer(ColInfo)^, Length(ColInfo));
end;

procedure TCustomTiles.SetNodeClass(Value: TNodeClass);
begin
 FEmptyTile.Free;
 FEmptyTile := Value.Create as TTileItem;
 FEmptyTile.Owner := Self;
 FTileSize := FEmptyTile.TileSize;
 FTileWidth := FEmptyTile.Width;
 FTileHeight := FEmptyTile.Height;
 FTileBPP := FEmptyTile.BitCount;
 inherited;
end;

procedure TCustomTiles.SetTransparentColor(Value: LongWord);
begin
 if FTransparentColor <> Value then
 begin
  FTransparentColor := Value;
  AfterChange;
 end;
end;

{ TTileHolder16x16 }

procedure TTileHolder16x16.BufDraw(var Dest; RowStride: Integer; XFlip,
  YFlip: Boolean);
var
 XX, YY: Integer;
 Dst, P, Src: PByte;
 SrcLine: PLine absolute Src;
 DstLine: PLine absolute Dst;
 FData: ^T16x16tile;
begin
 FData := TileData;
 Dst := @Dest;
 if XFlip then Inc(Dst, 15);
 for YY := 0 to 15 do
 begin
  if YFlip then
   Src := Addr(FData[15 - YY, 0]) else
   Src := Addr(FData[YY, 0]);
  if XFlip then
  begin
   P := Dst;
   for XX := 0 to 15 do
   begin
    P^ := Src^;
    Inc(Src);
    Dec(P);
   end;
  end else DstLine^ := SrcLine^;
  Inc(Dst, RowStride);
 end;
end;

procedure TTileHolder16x16.BufLoad(const Source; RowStride: Integer);
var
 YY: Integer;
 DstLine: PLine; Src: PByte;
 SrcLine: PLine absolute Src;
 Data: ^T16x16tile;
begin
 Data := TileData;
 Src := @Source;
 DstLine := Addr(Data[0, 0]);
 for YY := 0 to 15 do
 begin
  DstLine^ := SrcLine^;
  Inc(DstLine);
  Inc(Src, RowStride);
 end;
end;

procedure TTileHolder16x16.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 16;
 H.tlhHeight := 16;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := 8 - 1;
end;

procedure TTileHolder16x16.Flip(X, Y: Boolean; var Dest);
var
 Tile: T16x16tile absolute Dest;
 YY: Integer;
 Src64: PInt64;
 SrcLine: TLine;
 SrcLineRec: TLineRec absolute SrcLine;
 Data: ^T16x16tile;
begin
 Data := TileData;
 if X then
 begin
  for YY := 0 to 15 do
  begin
   Src64 := Addr(Data[YY, 0]);
   SrcLineRec.Hi := SwapInt64(Src64^);
   Inc(Src64);
   SrcLineRec.Lo := SwapInt64(Src64^);
   if Y then
    PLine(Addr(Tile[15 - YY, 0]))^ := SrcLine else
    PLine(Addr(Tile[YY, 0]))^ := SrcLine;
  end;
 end else if Y then
  for YY := 0 to 15 do
   PLine(Addr(Tile[YY, 0]))^ := PLine(Addr(Data[15 - YY, 0]))^;
end;

function TTileHolder16x16.GetBitCount: Integer;
begin
 Result := 8;
end;

function TTileHolder16x16.GetHeight: Integer;
begin
 Result := 16;
end;

function TTileHolder16x16.GetTileSize: Integer;
begin
 Result := SizeOf(T16x16tile);
end;

function TTileHolder16x16.GetWidth: Integer;
begin
 Result := 16;
end;

{ TTile16x16 }

procedure TTile16x16.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TTile4bpp16x16_Holder }

procedure TTile4bpp16x16_Holder.BufDraw(var Dest; RowStride: Integer;
  XFlip, YFlip: Boolean);
var
 XX, YY: Integer;
 Src: Int64;
 Dst, P: PByte;
 Data: ^T16x16Tile4;
begin
 Data := TileData;
 Dst := @Dest;
 if XFlip then
  Inc(Dst, 14) else
  Inc(Dst);
 for YY := 0 to 15 do
 begin
  P := Dst;
  if YFlip then
   Src := PInt64(Addr(Data[15 - YY, 0]))^ else
   Src := PInt64(Addr(Data[YY, 0]))^;
  if XFlip then for XX := 0 to 7 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Inc(P);
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Dec(P, 3);
  end else for XX := 0 to 7 do
  begin
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Dec(P);
   P^ := FFirstColor + (Src and 15);
   Src := Src shr 4;
   Inc(P, 3);
  end;
  Inc(Dst, RowStride);
 end;
end;

procedure TTile4bpp16x16_Holder.BufLoad(const Source; RowStride: Integer);
var
 XX, YY: Integer;
 Dst: PByte;
 Src, P: PByte;
begin
 Src := @Source;
 Dst := TileData;
 for YY := 0 to 15 do
 begin
  P := Src;
  for XX := 0 to 7 do
  begin
   Dst^ := (P^ - FFirstColor) shl 4;
   FFirstColor := Max(FFirstColor, P^ and $F0);
   Inc(P);
   Dst^ := Dst^ or ((P^ - FFirstColor) and 15);
   FFirstColor := Max(FFirstColor, P^ and $F0);
   Inc(P);
   Inc(Dst);
  end;
  Inc(Src, RowStride);
 end;
end;

procedure TTile4bpp16x16_Holder.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
begin
 H.tlhWidth := 16;
 H.tlhHeight := 16;
 H.tlhTileImageFormat := ifIndexed;
 H.tlhTileDrawFlags := 0;
 H.tlhTileFormatFlagsCount := 1;
 SetLength(Flags, 1);
 Flags[0] := 4 - 1;
end;


procedure TTile4bpp16x16_Holder.Flip(X, Y: Boolean; var Dest);
var
 Tile: T16x16Tile4 absolute Dest;
 XX, YY: Integer;
 Data: ^TTile4bit;
 Src: PByte;
 Dst: PByte;
begin
 if X then
 begin
  Src := TileData;
  for YY := 0 to 15 do
  begin
   if Y then
    Dst := Addr(Tile[15 - YY, 7]) else
    Dst := Addr(Tile[YY, 7]);
   for XX := 0 to 7 do
   begin
    Dst^ := Src^ shl 4;
    Dst^ := Dst^ or (Src^ and 15);
    Inc(Src);
    Dec(Dst);
   end;
  end;
 end else
 if Y then
 begin
  Data := TileData;
  for YY := 0 to 15 do
   PInt64(Addr(Tile[YY, 0]))^ := PInt64(Addr(Data[15 - YY, 0]))^;
 end;
end;

function TTile4bpp16x16_Holder.GetBitCount: Integer;
begin
 Result := 4;
end;

function TTile4bpp16x16_Holder.GetHeight: Integer;
begin
 Result := 16;
end;

function TTile4bpp16x16_Holder.GetTileSize: Integer;
begin
 Result := SizeOf(T16x16tile4);
end;

function TTile4bpp16x16_Holder.GetWidth: Integer;
begin
 Result := 16;
end;

{ TTile4bpp16x16 }

procedure TTile4bpp16x16.Initialize;
begin
 FTilePointer := @FData;
 inherited;
end;

{ TCustomizableTile }

procedure TCustomizableTile.Assign(Source: TNode);
begin
 if Source is TCustomizableTile then
 with TCustomizableTile(Source) do
 begin
  FBitmap.Draw(Self.FBitmap, 0, 0);
  Move(Pointer(FTileAttr)^, Pointer(Self.FTileAttr)^,
       Min(Length(Self.FTileAttr), Length(FTileAttr)) * 8);
 end else
  inherited;
end;

procedure TCustomizableTile.BufDraw(var Dest; RowStride: Integer; XFlip,
  YFlip: Boolean);
var
 Bmp: TBitmapContainer;
 Cvt: TBitmapConverter;
begin
 Bmp := FCvtPtr.tcvtBmp;
 Bmp.ImageFlags := (8 - 1) or (Ord(RowStride < 0) shl (IMG_FLIPS_SHIFT + 1)); 
 Bmp.Width := Abs(RowStride);
 Bmp.WidthRemainder := 0;
 Bmp.Height := FBitmap.Height;
 Bmp.ImageFormat := ifIndexed;
 Bmp.Compressed := False;
 Finalize(Bmp.CompositeFlags);
 if RowStride < 0 then
  Bmp.ImageData := Pointer(Integer(@Dest) + (Bmp.Height - 1) * RowStride) else
  Bmp.ImageData := @Dest;
 Bmp.Left := 0;
 Bmp.Top := 0;
 Cvt := FCvtPtr.tcvtTo8bpp[YFlip, XFlip];
 Cvt.DstInfo.Flags := Bmp.ImageFlags; 
 Cvt.SrcInfo.PixelModifier := FFirstColor;
 FBitmap.Draw(Bmp, 0, 0, Cvt);
end;

procedure TCustomizableTile.BufDraw4bpp(var Dest; Odd: Boolean;
  RowStride: Integer; XFlip, YFlip: Boolean);
var
 Bmp: TBitmapContainer;
 Cvt: TBitmapConverter;
begin
 Bmp := FCvtPtr.tcvtBmp;
 Bmp.ImageFlags := (4 - 1) or (Ord(RowStride < 0) shl (IMG_FLIPS_SHIFT + 1));
 Bmp.Width := Abs(RowStride) shl 1;
 Bmp.WidthRemainder := 0;
 Bmp.Height := FBitmap.Height;
 Bmp.ImageFormat := ifIndexed;
 Bmp.Compressed := False;
 if RowStride < 0 then
  Bmp.ImageData := Pointer(Integer(@Dest) + (Bmp.Height - 1) * RowStride) else
  Bmp.ImageData := @Dest;
 Bmp.Left := 0;
 Bmp.Top := 0;
 Finalize(Bmp.CompositeFlags);
 Cvt := FCvtPtr.tcvtTo4bpp[YFlip, XFlip];
 Cvt.DstInfo.Flags := Bmp.ImageFlags;
 Cvt.SrcInfo.PixelModifier := FFirstColor;
 FBitmap.Draw(Bmp, Ord(Odd), 0, Cvt);
end;

procedure TCustomizableTile.BufLoad(const Source; RowStride: Integer);
var
 Bmp: TBitmapContainer;
 Cvt: TBitmapConverter; 
begin
 Bmp := FCvtPtr.tcvtBmp;
 Bmp.ImageFlags := (8 - 1) or (Ord(RowStride < 0) shl (IMG_FLIPS_SHIFT + 1));
 Bmp.Width := Abs(RowStride);
 Bmp.WidthRemainder := 0;
 Bmp.Height := FBitmap.Height;
 Bmp.ImageFormat := ifIndexed;
 Bmp.Compressed := False;
 if RowStride < 0 then
  Bmp.ImageData := Pointer(Integer(@Source) + (Bmp.Height - 1) * RowStride) else
  Bmp.ImageData := @Source;
 Bmp.Left := 0;
 Bmp.Top := 0;
 Finalize(Bmp.CompositeFlags);
 Cvt := FCvtPtr.tcvtFrom8bpp;
 Cvt.SrcInfo.Flags := Bmp.ImageFlags;
 Cvt.SrcInfo.PixelModifier := -FFirstColor;
 Bmp.Draw(FBitmap, 0, 0, Cvt);
end;

procedure TCustomizableTile.BufLoad4bpp(const Source; Odd: Boolean;
  RowStride: Integer);
var
 Bmp: TBitmapContainer;
 Cvt: TBitmapConverter;
begin
 Bmp := FCvtPtr.tcvtBmp;
 Bmp.ImageFlags := (4 - 1) or (Ord(RowStride < 0) shl (IMG_FLIPS_SHIFT + 1));
 Bmp.Width := Abs(RowStride) shl 1;
 Bmp.WidthRemainder := 0;
 Bmp.Height := FBitmap.Height;
 Bmp.ImageFormat := ifIndexed;
 Bmp.Compressed := False;
 if RowStride < 0 then
  Bmp.ImageData := Pointer(Integer(@Source) + (Bmp.Height - 1) * RowStride) else
  Bmp.ImageData := @Source;
 Bmp.Left := -Ord(Odd);
 Bmp.Top := 0;
 Finalize(Bmp.CompositeFlags);
 Cvt := FCvtPtr.tcvtFrom4bpp;
 Cvt.SrcInfo.Flags := Bmp.ImageFlags;
 Cvt.SrcInfo.PixelModifier := -FFirstColor;
 Bmp.Draw(FBitmap, 0, 0, Cvt);
end;

destructor TCustomizableTile.Destroy;
var
 Ptr: Pointer;
begin
 Finalize(FTileAttr);
 Ptr := FBitmap.ImageData;
 FreeMem(Ptr);
 FBitmap.Free;
 inherited;
end;

procedure TCustomizableTile.FillHeader(var Header; var Flags: TCompositeFlags);
var
 H: TTileListHeader absolute Header;
 Cnt: Integer;
begin
 H.tlhWidth := FBitmap.Width;
 H.tlhHeight := FBitmap.Height;
 H.tlhTileImageFormat := FBitmap.ImageFormat;
 H.tlhTileDrawFlags := (Owner as TCustomizableTileList).FDrawFlags;
 Cnt := 1 + Length(FBitmap.CompositeFlags);
 H.tlhTileFormatFlagsCount := Cnt;
 SetLength(Flags, Cnt);
 Flags[0] := FBitmap.ImageFlags;
 Move(Pointer(FBitmap.CompositeFlags)^, Flags[1], Cnt - 1);
end;

procedure TCustomizableTile.Flip(X, Y: Boolean; var Dest);
var
 Bmp: TBitmapContainer;
begin
 Bmp := FCvtPtr.tcvtBmp;
 Bmp.Width := FBitmap.Width;
 Bmp.WidthRemainder := FBitmap.WidthRemainder;
 Bmp.Height := FBitmap.Height;
 Bmp.ImageFormat := FBitmap.ImageFormat;
 Bmp.Compressed := False;
 Bmp.FlagsList := FBitmap.FlagsList;
 Bmp.Left := 0;
 Bmp.Top := 0;
 Bmp.ImageData := @Dest;
 FBitmap.Draw(Bmp, 0, 0, DRAW_NO_CONVERT or (Ord(X) shl DRAW_FLIPS_SHIFT) or
                         (Ord(Y) shl (DRAW_FLIPS_SHIFT + 1)));
end;

function TCustomizableTile.GetBitCount: Integer;
begin
 Result := FBitmap.BitsCount;
end;

function TCustomizableTile.GetColorFormat: TColorFormat;
begin
 Result := FBitmap.ColorFormat;
end;

function TCustomizableTile.GetColorTable: Pointer;
begin
 Result := FBitmap.ColorTable;
end;

function TCustomizableTile.GetHeight: Integer;
begin
 Result := FBitmap.Height;
end;

function TCustomizableTile.GetImageFormat: TImageFormat;
begin
 Result := FBitmap.ImageFormat;
end;

function TCustomizableTile.GetTileSize: Integer;
begin
 Result := FBitmap.ImageSize;
end;

function TCustomizableTile.GetTransparentColor: LongWord;
begin
 Result := FBitmap.TransparentColor;
end;

function TCustomizableTile.GetWidth: Integer;
begin
 Result := FBitmap.Width;
end;

procedure TCustomizableTile.Initialize;
begin
 inherited;
 FBitmap := TBitmapContainer.Create;
end;

procedure TCustomizableTile.Reallocate(AWidth, AHeight: Integer;
  const Flags: array of Word; ColFmt: TColorFormat; ColTbl: Pointer;
  ImgFmt: TImageFormat);
var
 Ptr: Pointer;
 ImgSz: Integer;
begin
 if (FBitmap.Width <> 0) and
    (FBitmap.Height <> 0) then
 begin
  with TBitmapContainer.Create do
  try
   Width := FBitmap.Width;
   WidthRemainder := FBitmap.WidthRemainder;
   Height := FBitmap.Height;
   FlagsList := FBitmap.FlagsList;
   ColorFormat := FBitmap.ColorFormat;
   ColorTable := FBitmap.ColorTable;
   ImageFormat := FBitmap.ImageFormat;
   ImageData := FBitmap.ImageData;
   Compressed := FBitmap.Compressed;
   CompressedSize := FBitmap.CompressedSize;

   FBitmap.Width := AWidth;
   FBitmap.WidthRemainder := 0;
   FBitmap.Height := AHeight;
   FBitmap.ImageFormat := ImgFmt;
   FBitmap.SetFlags(Flags);
   FBitmap.ColorFormat := ColFmt;
   FBitmap.ColorTable := ColTbl;
   FBitmap.Compressed := False;
   GetMem(FTilePointer, FBitmap.ImageSize);
   FBitmap.ImageData := FTilePointer;
   Draw(FBitmap, 0, 0);
   Ptr := ImageData;
   FreeMem(Ptr);   
  finally
   Free;
  end;
 end else
 begin
  FBitmap.Width := AWidth;
  FBitmap.WidthRemainder := 0;
  FBitmap.Height := AHeight;
  FBitmap.ImageFormat := ImgFmt;
  FBitmap.SetFlags(Flags);
  FBitmap.ColorFormat := ColFmt;
  FBitmap.ColorTable := ColTbl;
  FBitmap.Compressed := False;
  FTilePointer := FBitmap.ImageData;
  ImgSz := FBitmap.ImageSize;
  ReallocMem(FTilePointer, ImgSz);
  FillChar(FTilePointer^, ImgSz, 0);
  FBitmap.ImageData := FTilePointer;
 end;
end;

procedure TCustomizableTile.SetColorFormat(Value: TColorFormat);
begin
 FBitmap.ColorFormat := Value;
end;

procedure TCustomizableTile.SetColorTable(Value: Pointer);
begin
 FBitmap.ColorTable := Value;
end;

procedure TCustomizableTile.SetImageFormat(Value: TImageFormat);
begin
 FBitmap.ImageFormat := Value;
end;

procedure TCustomizableTile.SetTransparentColor(Value: LongWord);
begin
 FBitmap.TransparentColor := Value;
end;

{ TCustomizableTileList }

function TCustomizableTileList.AddTile: TTileItem;
begin
 Result := inherited AddTile;
 with TCustomizableTile(Result) do
 begin
  FCvtPtr := @FCvtRec;
  Reallocate(FTileWidth, FTileHeight,
              FTileFormatFlags, FColorFormat, FColorTable, FTileImageFormat);
  SetLength(FTileAttr, Length(FAttrDefList));
  TransparentColor := Self.FTransparentColor;
 end;
end;

procedure TCustomizableTileList.AfterChange;
begin
 inherited;
 TCustomizableTile(FEmptyTile).TransparentColor := FTransparentColor;
end;

constructor TCustomizableTileList.Create(ATileWidth, ATileHeight: Integer;
  const Flags: array of Word; DrawFlags: Word);
begin
 Reallocate(ATileWidth, ATileHeight, Flags, DrawFlags);
 inherited Create(TCustomizableTile, False);
end;

destructor TCustomizableTileList.Destroy;
begin
 FCvtRec.tcvtFrom4bpp.Free;
 FCvtRec.tcvtFrom8bpp.Free;
 FCvtRec.tcvtTo4bpp[False, False].Free;
 FCvtRec.tcvtTo4bpp[False, True].Free;
 FCvtRec.tcvtTo4bpp[True, False].Free;
 FCvtRec.tcvtTo4bpp[True, True].Free;
 FCvtRec.tcvtTo8bpp[False, False].Free;
 FCvtRec.tcvtTo8bpp[False, True].Free;
 FCvtRec.tcvtTo8bpp[True, False].Free;
 FCvtRec.tcvtTo8bpp[True, True].Free;
 FColorFormat.Free;
 FCvtRec.tcvtBmp.Free;
 inherited;
end;

function TCustomizableTileList.GetAttrCount: Integer;
begin
 Result := Length(FAttrDefList);
end;

procedure TCustomizableTileList.Initialize;
begin
 FCvtRec.tcvtBmp := TBitmapContainer.Create;
 FColorFormat := TColorFormat.Create;
 inherited;
 FTileClassAssign := False; 
end;

procedure TCustomizableTileList.LoadFromStream(Stream: TStream);
var
 I, Cnt: Integer;
begin
 inherited;
 Stream.ReadBuffer(Cnt, 4);
 AttrCount := Cnt;
 for I := 0 to Cnt - 1 do
  with FAttrDefList[I] do
  begin
   Stream.ReadBuffer(adMin, 8);
   Stream.ReadBuffer(adMax, 8);
   Stream.ReadBuffer(adType, 1);
   Stream.ReadBuffer(Cnt, 1);
   Stream.ReadBuffer(Pointer(adName)^, Byte(Cnt) shl 1);
  end;
 Cnt := AttrCount shl 3;
 for I := 0 to TilesCount - 1 do
  Stream.ReadBuffer(Pointer(TCustomizableTile(List[I]).FTileAttr)^, Cnt);
end;

procedure TCustomizableTileList.Reallocate(ATileWidth,
  ATileHeight: Integer; const Flags: array of Word; DrawFlags: Word);
begin
 FTileWidth := ATileWidth;
 FTileHeight := ATileHeight;
 FDrawFlags := DrawFlags;
 SetLength(FTileFormatFlags, Length(Flags));
 Move(Flags[0], FTileFormatFlags[0], Length(Flags) shl 1);
 FCvtRec.tcvtFrom4bpp.Free;
 FCvtRec.tcvtFrom4bpp := TBitmapConverter.Create([4 - 1], FTileFormatFlags,
                       DRAW_PIXEL_MODIFY);
 FCvtRec.tcvtFrom8bpp.Free;
 FCvtRec.tcvtFrom8bpp := TBitmapConverter.Create([8 - 1], FTileFormatFlags,
                       DRAW_PIXEL_MODIFY);
 FCvtRec.tcvtTo4bpp[False, False].Free;
 FCvtRec.tcvtTo4bpp[False, False] := TBitmapConverter.Create(FTileFormatFlags,
         [4 - 1], DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo4bpp[False,  True].Free;
 FCvtRec.tcvtTo4bpp[False,  True] := TBitmapConverter.Create(FTileFormatFlags,
         [4 - 1], DRAW_X_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo4bpp[True,  False].Free;
 FCvtRec.tcvtTo4bpp[True,  False] := TBitmapConverter.Create(FTileFormatFlags,
         [4 - 1], DRAW_Y_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo4bpp[True,   True].Free;
 FCvtRec.tcvtTo4bpp[True,   True] := TBitmapConverter.Create(FTileFormatFlags,
         [4 - 1], DRAW_X_FLIP or DRAW_Y_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo8bpp[False, False].Free;
 FCvtRec.tcvtTo8bpp[False, False] := TBitmapConverter.Create(FTileFormatFlags,
         [8 - 1], DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo8bpp[False,  True].Free;
 FCvtRec.tcvtTo8bpp[False,  True] := TBitmapConverter.Create(FTileFormatFlags,
         [8 - 1], DRAW_X_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo8bpp[True,  False].Free;
 FCvtRec.tcvtTo8bpp[True,  False] := TBitmapConverter.Create(FTileFormatFlags,
         [8 - 1], DRAW_Y_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 FCvtRec.tcvtTo8bpp[True,   True].Free;
 FCvtRec.tcvtTo8bpp[True,   True] := TBitmapConverter.Create(FTileFormatFlags,
         [8 - 1], DRAW_X_FLIP or DRAW_Y_FLIP or DRAW_PIXEL_MODIFY or DrawFlags);
 if Assigned(FEmptyTile) then SetNodeClass(NodeClass);
end;

procedure TCustomizableTileList.SaveToStream(Stream: TStream);
var
 I, Cnt: Integer;
begin
 inherited;
 for I := 0 to AttrCount - 1 do
  with FAttrDefList[I] do
  begin
   Cnt := Min(Length(adName), 255);
   Stream.WriteBuffer(adMin, 8);
   Stream.WriteBuffer(adMax, 8);
   Stream.WriteBuffer(adType, 1);
   Stream.WriteBuffer(Cnt, 1);
   Stream.WriteBuffer(Pointer(adName)^, Cnt shl 1);
  end;
 Cnt := AttrCount shl 3;
 for I := 0 to TilesCount - 1 do
  Stream.WriteBuffer(Pointer(TCustomizableTile(List[I]).FTileAttr)^, Cnt);
end;

procedure TCustomizableTileList.SetAttrCount(Value: Integer);
var
 I: Integer;
begin
 SetLength(FAttrDefList, Value);
 SetLength(TCustomizableTile(FEmptyTile).FTileAttr, Value);
 for I := 0 to Count - 1 do
  SetLength(TCustomizableTile(Tiles[I]).FTileAttr, Value);
end;

procedure TCustomizableTileList.SetNodeClass(Value: TNodeClass);
var
 W, H: Integer;
begin
 W := FTileWidth;
 H := FTileHeight;
 FEmptyTile.Free;
 FEmptyTile := Value.Create as TTileItem;
 if FEmptyTile is TCustomizableTile then
  inherited SetNodeClass(Value) else
  inherited SetNodeClass(TCustomizableTile);
 FTileWidth := W;
 FTileHeight := H;
 TCustomizableTile(FEmptyTile).FCvtPtr := @FCvtRec;
 TCustomizableTile(FEmptyTile).Reallocate(FTileWidth, FTileHeight,
                FTileFormatFlags, FColorFormat, FColorTable, FTileImageFormat);
 FTileSize := FEmptyTile.GetTileSize;
 FTileBPP := FEmptyTile.GetBitCount;
 SetLength(TCustomizableTile(FEmptyTile).FTileAttr, Length(FAttrDefList));
end;

{ TTileSetList }

function TTileSetList.AddSet(const AName: WideString): TCustomTiles;
begin
 Result := AddNode as TCustomTiles;
 Result.FTileSetName := AName;
end;

function TTileSetList.GetSet(X: Integer): TCustomTiles;
begin
 Result := Nodes[X] as TCustomTiles;
end;

procedure TTileSetList.Initialize;
begin
 FAssignableClass := TTileSetList;
 NodeClass := TCustomTiles;
end;

procedure TTileSetList.LoadFromStream(Stream: TStream);
begin
  inherited;
   // TODO: finish me!!!
end;

procedure TTileSetList.SaveToStream(Stream: TStream);
begin
  inherited;
   // TODO: finish me!!!
end;

end.
