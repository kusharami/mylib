unit UnicodeUtils;

interface

uses
  SysUtils,
  Classes
{$IFNDEF FORCED_UNICODE}
  , TntSysUtils
  , TntClasses
  , TntSystem
{$ELSE}

{$ENDIF}
  , FormatW
;

{$IFDEF FORCED_UNICODE}
  type
    WideException = Exception;
    TWideStrings = TStrings;
    TWideStringList = class(TStringList)
    public
      procedure LoadFromFile(const FileName: WideString); virtual;
      procedure LoadFromStream(Stream: TStream); override;
      procedure LoadFromStream_BOM(Stream: TStream; WithBOM: Boolean); virtual;
      procedure SaveToStream(Stream: TStream); override;
      procedure SaveToStream_BOM(Stream: TStream; WithBOM: Boolean); virtual;
    end;
    TWideFileStream = TFileStream;
{$ELSE}
  type
    RawByteString = AnsiString;
    TWideStrings = TTntStrings;
    TWideStringList = TTntStringList;
    TWideFileStream = TTntFileStream;
{$ENDIF}

implementation

{$IFDEF FORCED_UNICODE}

{ TWideStringList }

procedure TWideStringList.LoadFromFile(const FileName: WideString);
begin
  inherited LoadFromFile(FileName);
end;

procedure TWideStringList.LoadFromStream(Stream: TStream);
begin
  LoadFromStream_BOM(Stream, True);
end;

procedure TWideStringList.LoadFromStream_BOM(Stream: TStream; WithBOM: Boolean);
begin
  inherited LoadFromStream(Stream);
end;

procedure TWideStringList.SaveToStream(Stream: TStream);
begin
  SaveToStream_BOM(Stream, True);
end;

procedure TWideStringList.SaveToStream_BOM(Stream: TStream; WithBOM: Boolean);
begin
  inherited SaveToStream(Stream);
end;

{$ENDIF}

end.
