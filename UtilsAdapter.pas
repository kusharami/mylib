unit UtilsAdapter;

interface

uses
 SysUtils, Classes, TntClasses, NodeLst, LunaGFX, ZLibEx, MyClasses, Dialogs,
 PropContainer, PlugAdapters, PlugInterface, MyClassesEx, BitmapEx;

type
 TUtilsAdapter = class(TInterfacedObject, IUtilsInterface)
  private
    function CreateMemoryStream: IMemoryStream; stdcall;
    function CreateFileStream(FileName: PAnsiChar;
      Mode: Integer): IStream64; stdcall;
    function CreateFileStreamW(FileName: PWideChar;
      Mode: Integer): IStream64; stdcall;
    function CreateLunaGFX: ILunaGFX; stdcall;
    function CreateLunaGFXSet: ILunaGFXSet; stdcall;    
    function CreateLunaGFXList: ILunaGFXList; stdcall;
    function CreateProperty: IPropertyListItem; stdcall;
    function CreatePropertyList: IPropertyList; stdcall;
    function CreateStringList: IStringList; stdcall;
    function CreateWideStringList: IWideStringList; stdcall;
    function CreateColorFormat(Data: PChannel;
      Count: Integer): IColorFormat; stdcall;
    function CreateZLibCompressor(Level: Integer;
      const Dest: IReadWrite): IStream32; stdcall;
    function CreateZLibDecompressor(const Source: IReadWrite): IStream32;
      stdcall;
    function CreateIniFile: IIniFile; stdcall;  
 end;

 TPropertyAdapter = class(TInterfacedObject, IPropertyListItem)
  private
    FSource: TPropertyListItem;
    FFreeAfterUse: Boolean;
  public
    function NameGet: PWideChar; stdcall;
    procedure NameSet(Value: PWideChar); stdcall;
    function ValueTypeGet: TValueType; stdcall;
    procedure ValueTypeSet(Value: TValueType); stdcall;
    function ValueStrGet: PWideChar; stdcall;
    procedure ValueStrSet(Value: PWideChar); stdcall;
    function DefValStrGet: PWideChar; stdcall;
    procedure DefValStrSet(Value: PWideChar); stdcall;
    function ValueGet: Int64; stdcall;
    procedure ValueSet(const Value: Int64); stdcall;
    function MinGet: Int64; stdcall;
    procedure MinSet(const Value: Int64); stdcall;
    function MaxGet: Int64; stdcall;
    procedure MaxSet(const Value: Int64); stdcall;
    function HexDigitsGet: Integer; stdcall;
    procedure HexDigitsSet(Value: Integer); stdcall;
    function ReadOnlyGet: LongBool; stdcall;
    procedure ReadOnlySet(Value: LongBool); stdcall;
    function ParamsGet: Integer; stdcall;
    procedure ParamsSet(Value: Integer); stdcall;
    function TagGet: Integer stdcall;
    procedure TagSet(Value: Integer); stdcall;
    function DataGet: Pointer; stdcall;
    function DataSizeGet: Integer; stdcall;
    procedure DataSizeSet(Value: Integer); stdcall;
    function GetPickListIndex: Integer; stdcall;
    procedure SetPickListIndex(Value: Integer); stdcall;
    function GetPickList: IWideStringList; stdcall;
    function SubPropsGet: IPropertyList; stdcall;
    function GetDataStream: IStream32; stdcall;
 //public
    property Name: PWideChar read NameGet write NameSet;
    property ValueType: TValueType read ValueTypeGet write ValueTypeSet;
    property ValueStr: PWideChar read ValueStrGet write ValueStrSet;
    property DefaultStr: PWideChar read DefValStrGet write DefValStrSet;
    property Value: Int64 read ValueGet write ValueSet;
    property ValueMin: Int64 read MinGet write MinSet;
    property ValueMax: Int64 read MaxGet write MaxSet;
    property HexDigits: Integer read HexDigitsGet write HexDigitsSet;
    property ReadOnly: LongBool read ReadOnlyGet write ReadOnlySet;
    property Parameters: Integer read ParamsGet write ParamsSet;
    property Tag: Integer read TagGet write TagSet;
    property DataStream: IStream32 read GetDataStream;
    property Data: Pointer read DataGet;
    property DataSize: Integer read DataSizeGet write DataSizeSet;
    property PickListIndex: Integer read GetPickListIndex
                                   write SetPickListIndex;
    property PickList: IWideStringList read GetPickList;
    property SubProperties: IPropertyList read SubPropsGet;
    
    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;

    constructor Create(Source: TPropertyListItem; FreeAfterUse: Boolean = False);
    destructor Destroy; override;
 end;

 TPropertyListAdapter = class(TInterfacedObject, IPropertyList)
  private
    FSource: TPropertyList;
    FFreeAfterUse: Boolean;
  public
    function PropertyGet(Index: Integer): IPropertyListItem; stdcall;
 //public
    function AddProperty: IPropertyListItem; stdcall;
    procedure RemoveProperty(const Prop: IPropertyListItem); stdcall;
    function BooleanAdd(Name: PWideChar;
                       Value: LongBool): IPropertyListItem; stdcall;
    function DecimalAdd(Name: PWideChar;
                      const Value, Min, Max: Int64): IPropertyListItem; stdcall;
    function HexadecimalAdd(Name: PWideChar;
                            const Value, Min, Max: Int64;
                            Digits: Integer): IPropertyListItem; stdcall;
    function StringAdd(Name, Value: PWideChar;
                      ReadOnly: LongBool): IPropertyListItem; stdcall;
    function PickListAdd(Name, Value, List: PWideChar;
                        Fixed: LongBool = True): IPropertyListItem; stdcall;
    function FilePickAdd(Name, FileName, Filter, DefaultExt,
     InitialDir: PWideChar; Save: LongBool = False; Options: Integer =
     FO_HideReadOnly or FO_EnableSizing): IPropertyListItem; stdcall;
    function DataAdd(Name: PWideChar): IPropertyListItem; stdcall;
 //properties
    property Properties[Index: Integer]: IPropertyListItem read PropertyGet;

    function GetPtr: Pointer; stdcall;
    procedure FreeObject; stdcall;


    function GetCount: Integer; stdcall;
    procedure Clear; stdcall;

    procedure LoadFromStream(const Stream: IReadWrite); stdcall;
    procedure SaveToStream(const Stream: IReadWrite); stdcall;

    constructor Create(Source: TPropertyList; FreeAfterUse: Boolean = False);
    destructor Destroy; override;
 end;

implementation

function TUtilsAdapter.CreateColorFormat(Data: PChannel;
  Count: Integer): IColorFormat;
begin
 Result := TColorFormatAdapter.Create(TColorFormat.Create(GetColorFormatStr(Data, Count)), True);
end;

function TUtilsAdapter.CreateFileStream(FileName: PAnsiChar;
  Mode: Integer): IStream64;
begin
 Result := TStreamAdapter.Create(TFileStream.Create(FileName, Mode), True);
end;

function TUtilsAdapter.CreateFileStreamW(FileName: PWideChar;
  Mode: Integer): IStream64;
begin
 Result := TStreamAdapter.Create(TTntFileStream.Create(FileName, Mode), True);
end;

function TUtilsAdapter.CreateIniFile: IIniFile;
begin
 Result := TIniFileAdapter.Create(TStreamIniFileW.Create, False, True);
end;

function TUtilsAdapter.CreateLunaGFX: ILunaGFX;
begin
 Result := TLunaGFXAdapter.Create(TLunaGFX.Create, True);
end;

function TUtilsAdapter.CreateLunaGFXList: ILunaGFXList;
begin
 Result := TLunaGFXListAdapter.Create(TLunaGFXList.Create, True);
end;

function TUtilsAdapter.CreateLunaGFXSet: ILunaGFXSet;
begin
 Result := TLunaGFXSetAdapter.Create(TLunaGFXSet.Create, True);
end;

function TUtilsAdapter.CreateMemoryStream: IMemoryStream;
begin
 Result := TMemoryStreamAdapter.Create(TMemStream.Create, True);
end;

function TUtilsAdapter.CreateProperty: IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(TPropertyListItem.Create, True);
end;

function TUtilsAdapter.CreatePropertyList: IPropertyList;
begin
 Result := TPropertyListAdapter.Create(TPropertyList.Create, True);
end;

function TUtilsAdapter.CreateStringList: IStringList;
begin
 Result := TStringsAdapter.Create(TStringList.Create, True);
end;

function TUtilsAdapter.CreateWideStringList: IWideStringList;
begin
 Result := TStringsAdapter.Create(TTntStringList.Create, True);
end;

function TUtilsAdapter.CreateZLibCompressor(Level: Integer;
  const Dest: IReadWrite): IStream32;
begin
 Result := TStreamAdapter.Create(
  TZCompressionStream.Create(TStream(Dest.GetPtr),
              TZCompressionLevel(Level)), True);
end;

function TUtilsAdapter.CreateZLibDecompressor(
  const Source: IReadWrite): IStream32;
begin
 Result := TStreamAdapter.Create(
  TZDecompressionStream.Create(TStream(Source.GetPtr)));
end;

{ TPropertyAdapter }

constructor TPropertyAdapter.Create(Source: TPropertyListItem;
  FreeAfterUse: Boolean);
begin
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

function TPropertyAdapter.DataGet: Pointer;
begin
 Result := TPropertyListItem(FSource).DataStream.Memory;
end;

function TPropertyAdapter.DataSizeGet: Integer;
begin
 Result := TPropertyListItem(FSource).DataStream.Size;
end;

procedure TPropertyAdapter.DataSizeSet(Value: Integer);
begin
 TPropertyListItem(FSource).DataStream.Size := Value;
end;

function TPropertyAdapter.DefValStrGet: PWideChar;
begin
 Result := Pointer(TPropertyListItem(FSource).DefaultStr);
end;

procedure TPropertyAdapter.DefValStrSet(Value: PWideChar);
begin
 TPropertyListItem(FSource).DefaultStr := Value;
end;

destructor TPropertyAdapter.Destroy;
begin
 if FFreeAfterUse then
  FSource.Free;
 inherited;
end;

procedure TPropertyAdapter.FreeObject;
begin
 FreeAndNIL(FSource);
end;

function TPropertyAdapter.GetDataStream: IStream32;
begin
 Result := TStreamAdapter.Create(TPropertyListItem(FSource).DataStream);
end;

function TPropertyAdapter.GetPickList: IWideStringList;
begin
 Result := TStringsAdapter.Create(TPropertyListItem(FSource).PickList);
end;

function TPropertyAdapter.GetPickListIndex: Integer;
begin
  Result := TPropertyListItem(FSource).PickListIndex;
end;

function TPropertyAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TPropertyAdapter.HexDigitsGet: Integer;
begin
 Result := TPropertyListItem(FSource).HexDigits;
end;

procedure TPropertyAdapter.HexDigitsSet(Value: Integer);
begin
 FSource.HexDigits := Value;
end;

function TPropertyAdapter.MaxGet: Int64;
begin
 Result := FSource.ValueMax;
end;

procedure TPropertyAdapter.MaxSet(const Value: Int64);
begin
 FSource.ValueMax := Value;
end;

function TPropertyAdapter.MinGet: Int64;
begin
 Result := FSource.ValueMin;
end;

procedure TPropertyAdapter.MinSet(const Value: Int64);
begin
 FSource.ValueMin := Value;
end;

function TPropertyAdapter.NameGet: PWideChar;
begin
 Result := Pointer(TPropertyListItem(FSource).Name);
end;

procedure TPropertyAdapter.NameSet(Value: PWideChar);
begin
 FSource.Name := Value;
end;

function TPropertyAdapter.ParamsGet: Integer;
begin
 Result := FSource.Parameters;
end;

procedure TPropertyAdapter.ParamsSet(Value: Integer);
begin
 FSource.Parameters := Value;
end;

function TPropertyAdapter.ReadOnlyGet: LongBool;
begin
 Result := FSource.ReadOnly;
end;

procedure TPropertyAdapter.ReadOnlySet(Value: LongBool);
begin
 FSource.ReadOnly := Value;
end;

procedure TPropertyAdapter.SetPickListIndex(Value: Integer);
begin
 TPropertyListItem(FSource).PickListIndex := Value;
end;

function TPropertyAdapter.SubPropsGet: IPropertyList;
begin
 Result := TPropertyListAdapter.Create(TPropertyListItem(FSource).SubProperties);
end;

function TPropertyAdapter.TagGet: Integer;
begin
 Result := FSource.UserTag;
end;

procedure TPropertyAdapter.TagSet(Value: Integer);
begin
 FSource.UserTag := Value;
end;

function TPropertyAdapter.ValueGet: Int64;
begin
 Result := FSource.Value;
end;

procedure TPropertyAdapter.ValueSet(const Value: Int64);
begin
 FSource.Value := Value;
end;

function TPropertyAdapter.ValueStrGet: PWideChar;
begin
 Result := Pointer(FSource.ValueStr);
end;

procedure TPropertyAdapter.ValueStrSet(Value: PWideChar);
begin
 FSource.ValueStr := Value;
end;

function TPropertyAdapter.ValueTypeGet: TValueType;
begin
 Result := TValueType(FSource.ValueType);
end;

procedure TPropertyAdapter.ValueTypeSet(Value: TValueType);
begin
 FSource.ValueType := PropContainer.TValueType(Value);
end;

{ TPropertyListAdapter }

function TPropertyListAdapter.AddProperty: IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(TPropertyList(FSource).AddProperty);
end;

function TPropertyListAdapter.BooleanAdd(Name: PWideChar;
  Value: LongBool): IPropertyListItem;
begin
  Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddBoolean(Name, Value));
end;

procedure TPropertyListAdapter.Clear;
begin
 FSource.Clear;
end;

constructor TPropertyListAdapter.Create(Source: TPropertyList;
  FreeAfterUse: Boolean);
begin
 FSource := Source;
 FFreeAfterUse := FreeAfterUse;
end;

function TPropertyListAdapter.DataAdd(Name: PWideChar): IPropertyListItem;
begin
  Result := TPropertyAdapter.Create(TPropertyList(FSource).AddData(Name));
end;

function TPropertyListAdapter.DecimalAdd(Name: PWideChar; const Value, Min,
  Max: Int64): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddDecimal(Name, Value, Min, Max));
end;

destructor TPropertyListAdapter.Destroy;
begin
 if FFreeAfterUse then
  FSource.Free;
 inherited;
end;

function TPropertyListAdapter.FilePickAdd(Name, FileName, Filter,
  DefaultExt, InitialDir: PWideChar; Save: LongBool;
  Options: Integer): IPropertyListItem;
var
 Temp: TOpenOptions;
 I: TOpenOption;
begin
 Temp := [];
 for I := Low(TOpenOption) to High(TOpenOption) do
 begin
  if Options and 1 <> 0 then Include(Temp, I);
  Options := Options shr 1;
 end;
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddFilePick(Name, FileName, Filter,
                           DefaultExt, InitialDir, Save, Temp), False);
end;

procedure TPropertyListAdapter.FreeObject;
begin
 FreeAndNil(FSource);
end;

function TPropertyListAdapter.GetCount: Integer;
begin
 Result := FSource.Count;
end;

function TPropertyListAdapter.GetPtr: Pointer;
begin
 Result := Pointer(FSource);
end;

function TPropertyListAdapter.HexadecimalAdd(Name: PWideChar; const Value,
  Min, Max: Int64; Digits: Integer): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddHexadecimal(Name, Value, Min, Max, Digits));
end;

procedure TPropertyListAdapter.LoadFromStream(const Stream: IReadWrite);
begin
 FSource.LoadFromStream(TStream(Stream.GetPtr));
end;

function TPropertyListAdapter.PickListAdd(Name, Value, List: PWideChar;
  Fixed: LongBool): IPropertyListItem;
begin
  Result := TPropertyAdapter.Create(FSource.AddPickList(Name,
                                     Value, List, Fixed));
end;

function TPropertyListAdapter.PropertyGet(
  Index: Integer): IPropertyListItem;
var
 Prop: TPropertyListItem;
begin
 Prop := TPropertyList(FSource).Properties[Index];
 if Prop <> nil then
  Result := TPropertyAdapter.Create(Prop) else
  Result := nil;
end;

procedure TPropertyListAdapter.RemoveProperty(
  const Prop: IPropertyListItem);
begin
 if Prop <> nil then
  FSource.Remove(TNode(Prop.GetPtr));
end;

procedure TPropertyListAdapter.SaveToStream(const Stream: IReadWrite);
begin
 FSource.SaveToStream(TStream(Stream.GetPtr));
end;

function TPropertyListAdapter.StringAdd(Name, Value: PWideChar;
  ReadOnly: LongBool): IPropertyListItem;
begin
 Result := TPropertyAdapter.Create(
   TPropertyList(FSource).AddString(Name, Value, not ReadOnly));
end;

end.
