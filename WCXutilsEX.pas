unit WcxUtilsEx;

interface // Plugin interface for Total Commander 7.5+

uses SysUtils, WcxHeadEx, Archiver, MyUtils;

function AcceptParameters(hArcData: HANDLE; Str: PWideChar): Integer;
 stdcall; export;
 
function OpenArchive(var ArchiveData: TOpenArchiveData): HANDLE;
 stdcall; export;

function OpenArchiveW(var ArchiveData: TOpenArchiveDataW): HANDLE;
 stdcall; export;

function ReadHeader(hArcData: HANDLE; var HeaderData: THeaderData): Integer;
 stdcall; export;

function ReadHeaderEx(hArcData: HANDLE; var HeaderData: THeaderDataEx): Integer;
 stdcall; export;

function ReadHeaderExW(hArcData: HANDLE; var HeaderData: THeaderDataExW): Integer;
 stdcall; export;

function ProcessFile(hArcData: HANDLE; Operation: Integer;
                     DestPath: PAnsiChar; DestName: PAnsiChar): Integer;
                     stdcall; export;

function ProcessFileW(hArcData: HANDLE; Operation: Integer;
          DestPath: PWideChar; DestName: PWideChar): Integer; stdcall; export;

function CloseArchive(hArcData: HANDLE): Integer; stdcall; export;

procedure SetChangeVolProc(hArcData: HANDLE; ChangeVolPrc: TChangeVolProc);
 stdcall; export;

procedure SetChangeVolProcW(hArcData: HANDLE; ChangeVolPrc: TChangeVolProcW);
 stdcall; export;

procedure SetProcessDataProc(hArcData: HANDLE;
           ProcessDataPrc: TProcessDataProc); stdcall; export;
           
procedure SetProcessDataProcW(hArcData: HANDLE;
           ProcessDataPrc: TProcessDataProcW); stdcall; export;

function GetPackerCaps: Integer; stdcall; export;

function PackFiles(PackedFile, SubPath, SrcPath, AddList: PAnsiChar;
                   Flags: Integer): Integer; stdcall; export;

function PackFilesW(PackedFile, SubPath, SrcPath, AddList: PWideChar;
                   Flags: Integer): Integer; stdcall; export;

function DeleteFiles(PackedFile, DeleteList: PAnsiChar): Integer; stdcall; export;
function DeleteFilesW(PackedFile, DeleteList: PWideChar): Integer; stdcall; export;

var
 ArchiveClass: TArchiveClass = TCustomArchive;
 PackerCaps: Integer =
 PK_CAPS_NEW or
 PK_CAPS_MODIFY or
 PK_CAPS_MULTIPLE or
 PK_CAPS_DELETE or
 PK_CAPS_OPTIONS or
 PK_CAPS_SEARCHTEXT or
 PK_CAPS_BY_CONTENT;

implementation

uses NodeLst;

var
 ChangeVolProc: TChangeVolProc;
 ChangeVolProcW: TChangeVolProcW;
 ProcessDataProc: TProcessDataProc;
 ProcessDataProcW: TProcessDataProcW;

type
 TArchiveEvent = class
  private
    FArchive: TCustomArchive;
    FCurrentItem: TArchiveItem;
  published
    procedure ArchiveProgress(Sender: TObject; Item: TArchiveItem;
         const BytesCopied: Int64; var ProgressResult: Integer);
 end;

procedure TArchiveEvent.ArchiveProgress(Sender: TObject; Item: TArchiveItem;
                                        const BytesCopied: Int64;
                                        var ProgressResult: Integer);
begin
 if Addr(ProcessDataProcW) <> nil then
 begin
  if Item = nil then
   ProgressResult := E_ERROR else with Item do
  if ProcessDataProcW(PWideChar(WideFormat('%s (%d/%d)',
    [FileName, TotalBytesCopied, NewFileSize])), BytesCopied) = 0 then
   ProgressResult := E_EABORTED;
 end else
 if Addr(ProcessDataProc) <> NIL then
 begin
  if Item = NIl then
   ProgressResult := E_ERROR Else with Item do
  if ProcessDataProc(PAnsiChar(Format('%s (%d/%d)',
    [AnsiFileName, TotalBytesCopied, NewFileSize])), BytesCopied) = 0 then
   ProgressResult := E_EABORTED;
 end;
end;

function AcceptParameters(hArcData: HANDLE; Str: PWideChar): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
begin
 Result := E_OK;
 if (AH <> NIL) and (AH.FArchive <> NIL) then
  AH.FArchive.ApplyParameters else
  AcceptedParameters := Str;
end;

function OpenArchive(var ArchiveData: TOpenArchiveData): HANDLE; stdcall;
var
 Archive: TCustomArchive;
 AH: TArchiveEvent absolute Result;
begin
 Archive := ArchiveClass.Create(ArchiveData.ArcName);
 try
  Archive.LoadArchive;
  ArchiveData.OpenResult := Archive.LastError;
  AH := TArchiveEvent.Create;
  with AH do
  begin
   FArchive := Archive;
   FArchive.OnProgress := ArchiveProgress;
   FCurrentItem := NIL;
  end;
  ArchiveData.OpenMode := PK_OM_EXTRACT;
 except
  ArchiveData.OpenResult := Archive.LastError;
  Result := 0;
  Archive.Free;
 end;
end;

function OpenArchiveW(var ArchiveData: TOpenArchiveDataW): HANDLE; stdcall;
var
 Archive: TCustomArchive;
 AH: TArchiveEvent absolute Result;
begin
 Archive := ArchiveClass.Create(ArchiveData.ArcName);
 try
  Archive.LoadArchive;
  ArchiveData.OpenResult := E_OK;
  AH := TArchiveEvent.Create;
  with AH do
  begin
   FArchive := Archive;
   FArchive.OnProgress := ArchiveProgress;
   FCurrentItem := NIL;
  end;
  ArchiveData.OpenMode := PK_OM_EXTRACT;
 except
  ArchiveData.OpenResult := E_UNKNOWN_FORMAT;
  Result := 0;
  Archive.Free;
 end;
end;

function ReadHeader(hArcData: HANDLE; var HeaderData: THeaderData): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
 L: Integer; S: String;
begin
 if (AH <> NIL) and (AH.FArchive <> NIL) then with AH do
 begin
  Result := 0;
  if FCurrentItem = NIL then
   FCurrentItem := FArchive.RootNode as TArchiveItem Else
   FCurrentItem := FCurrentItem.Next as TArchiveItem;
  if FCurrentItem <> NIL then with FCurrentItem do
  begin
   S := FileName;
   L := Length(S);
   if L > 259 then L := 259;
   FillChar(HeaderData, SizeOf(THeaderData), 0);
   Move(Pointer(S)^, HeaderData.FileName[0], L);
   S := AH.FArchive.ArchiveName;
   L := Length(S);
   if L > 259 then L := 259;
   Move(Pointer(S)^, HeaderData.ArcName[0], L);
   HeaderData.FileTime := FileTime;
   HeaderData.UnpSize  := FileSize;
   HeaderData.PackSize := PackSize;
   HeaderData.FileAttr := FileAttr;
   HeaderData.Flags := Flags;
  end Else Result := E_END_ARCHIVE;
 end Else Result := E_EOPEN;
end;

function ReadHeaderEx(hArcData: HANDLE; var HeaderData: THeaderDataEx): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
 L: Integer; S: String;
begin
 if (AH <> NIL) and (AH.FArchive <> NIL) then with AH do
 begin
  Result := 0;
  if FCurrentItem = NIL then
   FCurrentItem := FArchive.RootNode as TArchiveItem Else
   FCurrentItem := FCurrentItem.Next as TArchiveItem;
  if FCurrentItem <> NIL then with FCurrentItem do
  begin
   S := FileName;
   L := Length(S);
   if L > 1023 then L := 1023;
   FillChar(HeaderData, SizeOf(HeaderData), 0);
   Move(Pointer(S)^, HeaderData.FileName[0], L);
   S := AH.FArchive.ArchiveName;
   L := Length(S);
   if L > 1023 then L := 1023;
   Move(Pointer(S)^, HeaderData.ArcName[0], L);
   HeaderData.FileTime := FileTime;
   with Int64Rec(Addr(FileSize)^) do
   begin
    HeaderData.UnpSize := Lo;
    HeaderData.UnpSizeHigh := Hi;
   end;
   with Int64Rec(Addr(PackSize)^) do
   begin
    HeaderData.PackSize := Lo;
    HeaderData.PackSizeHigh := Hi;
   end;
   HeaderData.FileAttr := FileAttr;
   HeaderData.Flags := Flags;
  end Else Result := E_END_ARCHIVE;
 end Else Result := E_EOPEN;
end;

function ReadHeaderExW(hArcData: HANDLE; var HeaderData: THeaderDataExW): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
 L: Integer;
begin
 if (AH <> NIL) and (AH.FArchive <> NIL) then with AH do
 begin
  Result := 0;
  if FCurrentItem = NIL then
   FCurrentItem := FArchive.RootNode as TArchiveItem Else
   FCurrentItem := FCurrentItem.Next as TArchiveItem;
  if FCurrentItem <> NIL then with FCurrentItem do
  begin
   L := Length(FileName);
   if L > 1023 then L := 1023;
   FillChar(HeaderData, SizeOf(HeaderData), 0);
   Move(Pointer(FileName)^, HeaderData.FileName[0], L shl 1);
   L := Length(AH.FArchive.ArchiveName);
   if L > 1023 then L := 1023;
   Move(Pointer(AH.FArchive.ArchiveName)^, HeaderData.ArcName[0], L shl 1);
   HeaderData.FileTime := FileTime;
   with Int64Rec(Addr(FileSize)^) do
   begin
    HeaderData.UnpSize := Lo;
    HeaderData.UnpSizeHigh := Hi;
   end;
   with Int64Rec(Addr(PackSize)^) do
   begin
    HeaderData.PackSize := Lo;
    HeaderData.PackSizeHigh := Hi;
   end;
   HeaderData.FileAttr := FileAttr;
   HeaderData.Flags := Flags;
  end Else Result := E_END_ARCHIVE;
 end Else Result := E_EOPEN;
end;

function ProcessFile(hArcData: HANDLE; Operation: Integer;
                     DestPath: PAnsiChar; DestName: PAnsiChar): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
 FName: String;
begin
 if (AH <> NIL) and (AH.FArchive <> NIL) then with AH do
 begin
  if FCurrentItem <> NIL then
  begin
   case Operation of
    PK_SKIP,
    PK_TEST: Result := E_OK;
    PK_EXTRACT:
    begin
     FName := DestPath;
     FName := FName + DestName;
     Result := FArchive.ExtractFile(FCurrentItem, FName);
    end;
    else Result := E_NOT_SUPPORTED;
   end;
  end else Result := E_ERROR;
 end else Result := E_ERROR;
end;

function ProcessFileW(hArcData: HANDLE; Operation: Integer;
                     DestPath: PWideChar; DestName: PWideChar): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
 FName: WideString;
begin
 if (AH <> NIL) and (AH.FArchive <> NIL) then with AH do
 begin
  if FCurrentItem <> NIL then
  begin
   case Operation of
    PK_SKIP,
    PK_TEST: Result := E_OK;
    PK_EXTRACT:
    begin
     FName := DestPath;
     FName := FName + DestName;
     Result := FArchive.ExtractFile(FCurrentItem, FName);
    end;
    Else Result := E_NOT_SUPPORTED;
   end;
  end Else Result := E_ERROR;
 end Else Result := E_ERROR;
end;

function CloseArchive(hArcData: HANDLE): Integer; stdcall;
var
 AH: TArchiveEvent absolute hArcData;
begin
 if AH <> NIL then
 begin
  AH.FArchive.Free;
  AH.Free;
  Result := 0;
 end Else Result := E_ECLOSE;
end;

procedure SetChangeVolProc(hArcData: HANDLE; ChangeVolPrc: TChangeVolProc); stdcall;
begin
 @ChangeVolProc := Addr(ChangeVolPrc);
end;

procedure SetChangeVolProcW(hArcData: HANDLE; ChangeVolPrc: TChangeVolProcW); stdcall;
begin
 @ChangeVolProcW := Addr(ChangeVolPrc);
end;

procedure SetProcessDataProc(hArcData: HANDLE; ProcessDataPrc: TProcessDataProc); stdcall;
begin
 @ProcessDataProc := Addr(ProcessDataPrc);
end;

procedure SetProcessDataProcW(hArcData: HANDLE; ProcessDataPrc: TProcessDataProcW); stdcall;
begin
 @ProcessDataProcW := Addr(ProcessDataPrc);
end;

function GetPackerCaps: Integer; stdcall;
begin
 Result := PackerCaps;
end;

function PackFiles(PackedFile, SubPath, SrcPath, AddList: PAnsiChar;
                   Flags: Integer): Integer; stdcall;
var
 Archive: TCustomArchive;

 function AddFileList(SourcePath, DestPath: String;
                      FileList: PAnsiChar; Flgs: Integer): Integer;
 var
  S, SS: String; Item: TArchiveItem; SR: TSearchRec;
  P: PAnsiChar; Adder: TLoadFromFile; ch_flag: Boolean;
  LdFlg, X: Integer;
 begin
  if (SourcePath <> '') and (SourcePath[Length(SourcePath)] <> '\') then
   SourcePath := SourcePath + '\';
  if (DestPath <> '') and (DestPath[Length(DestPath)] <> '\') then
   DestPath := DestPath + '\';
  P := Pointer(FileList);
  ch_flag := False;
  with Archive do While P^ <> #0 do
  begin
   SS := P; Inc(P, StrLen(P) + 1);
   LdFlg := 0;
   X := Pos('::', SS);
   if X > 0 then
   begin
    if TryStrToInt(PAnsiChar(Addr(SS[X + 2])), LdFlg) then
     LdFlg := LdFlg or LOAD_FLAG_SET;
    SetLength(SS, X - 1);
   end;
   if (SS <> '') and (SS[Length(SS)] = '\') then
   begin
    if Flgs and PK_PACK_SAVE_PATHS = 0 then Continue;
    SetLength(SS, Length(SS) - 1);
   end;
   if Flgs and PK_PACK_SAVE_PATHS = 0 then
    S := DestPath + ExtractFileName(SS) Else
    S := DestPath + SS;
   SS := SourcePath + SS;
   Item := Files[S];
   if FindFirst(SS, faAnyFile, SR) = 0 then
   begin
    if SR.Attr and faDirectory = faDirectory then
    begin
     if (Item = NIL) and CanAdd then
     begin
      Item := AddItem;
      Item.FileName := S;
      Item.FileAttr := faDirectory;
      if Flgs and PK_PACK_MOVE_FILES <> 0 then
      begin
       Adder := LoadFromFileClass.Create(Item, True);
       Adder.LoadFlags := LdFlg;
       Adder.SourceFileName := SS;
       Item.Modifier := Adder;
      end;
      ch_flag := True;
     end;
    end Else if SR.Attr and faVolumeID = 0 then
    begin
     if (Item = NIL) and CanAdd then
     begin
      Item := AddItem;
      Item.FileName := S;
     end;
     if Item <> NIL then
     begin
      Adder := LoadFromFileClass.Create(Item, Flgs and PK_PACK_MOVE_FILES <> 0);
      Adder.LoadFlags := LdFlg;
      Adder.SourceFileName := SS;
      Item.Modifier := Adder;
      ch_flag := True;
     end;
    end;
    FindClose(SR);
   end Else
   begin
    if (Item = NIL) and CanAdd then//Make dir
    begin
     Item := AddItem;
     Item.FileName := S;
     Item.FileAttr := faDirectory;
     ch_flag := True;
    end;
   end;
  end;
  Result := E_OK;
  if ch_flag then
  with Archive do
  begin
   Updated := False;
   try
    UpdateFile;
   except
    Result := E_EWRITE;
   end;
  end;
 end; //AddFileList
(* PackFiles *)
var
 Event: TArchiveEvent;
begin
 case CheckFileForWrite(PackedFile) of
  cfCanWrite:
  begin
   Archive := ArchiveClass.Create(PackedFile);
   try
    Archive.LoadArchive;
   except
    Result := E_UNKNOWN_FORMAT;
    Archive.Free;
    Exit;
   end;
  end;
  cfNotExist: if PackerCaps and PK_CAPS_NEW <> 0 then
  begin
   Archive := ArchiveClass.Create(PackedFile);
  end else
  begin
   Result := E_NOT_SUPPORTED;
   Exit;
  end;
  else
  begin
   Result := E_EWRITE;
   Exit;
  end;
 end;
 Event := TArchiveEvent.Create;
 try
  Event.FArchive := Archive;
  Event.FCurrentItem := NIL;
  Archive.OnProgress := Event.ArchiveProgress;
  Result := AddFileList(SrcPath, SubPath, AddList, Flags);
 finally
  Event.Free;
 end;
 Archive.Free;
end; //PackFiles

function PackFilesW(PackedFile, SubPath, SrcPath, AddList: PWideChar;
                   Flags: Integer): Integer; stdcall;
var
 Archive: TCustomArchive;

 function AddFileList(SourcePath, DestPath: WideString;
                      FileList: PWideChar; Flgs: Integer): Integer;
 var
  S, SS: WideString; Item: TArchiveItem; SR: TSearchRecW;
  P: PWideChar; Adder: TLoadFromFile; ch_flag: Boolean;
  LdFlg, X: Integer;
 begin
  if (SourcePath <> '') and (SourcePath[Length(SourcePath)] <> '\') then
   SourcePath := SourcePath + '\';
  if (DestPath <> '') and (DestPath[Length(DestPath)] <> '\') then
   DestPath := DestPath + '\';
  P := Pointer(FileList);
  ch_flag := False;
  with Archive do While P^ <> #0 do
  begin
   SS := P; Inc(P, WideStrLen(P) + 1);
   LdFlg := 0;
   X := Pos('::', SS);
   if X > 0 then
   begin
    if TryStrToInt(PWideChar(Addr(SS[X + 2])), LdFlg) then
     LdFlg := LdFlg or LOAD_FLAG_SET;
    SetLength(SS, X - 1);
   end;
   if (SS <> '') and (SS[Length(SS)] = '\') then
   begin
    if Flgs and PK_PACK_SAVE_PATHS = 0 then Continue;
    SetLength(SS, Length(SS) - 1);
   end;
   if Flgs and PK_PACK_SAVE_PATHS = 0 then
    S := DestPath + WideExtractFileName(SS) Else
    S := DestPath + SS;
   SS := SourcePath + SS;
   Item := Files[S];
   if WideFindFirst(SS, faAnyFile, SR) = 0 then
   begin
    if SR.Attr and faDirectory = faDirectory then
    begin
     if (Item = NIL) and CanAdd then
     begin
      Item := AddItem;
      Item.FileName := S;
      Item.FileAttr := faDirectory;
      if Flgs and PK_PACK_MOVE_FILES <> 0 then
      begin
       Adder := LoadFromFileClass.Create(Item, True);
       Adder.SourceFileName := SS;
       Item.Modifier := Adder;
      end;
      ch_flag := True;
     end;
    end Else if SR.Attr and faVolumeID = 0 then
    begin
     if (Item = NIL) and CanAdd then
     begin
      Item := AddItem;
      Item.FileName := S;
     end;
     if Item <> NIL then
     begin
      Adder := LoadFromFileClass.Create(Item, Flgs and PK_PACK_MOVE_FILES <> 0);
      Adder.LoadFlags := LdFlg;
      Adder.SourceFileName := SS;
      Item.Modifier := Adder;
      ch_flag := True;
     end;
    end;
    WideFindClose(SR);
   end Else
   begin
    if (Item = NIL) and CanAdd then//Make dir
    begin
     Item := AddItem;
     Item.FileName := S;
     Item.FileAttr := faDirectory;
     ch_flag := True;
    end;
   end;
  end;
  Result := E_OK;
  if ch_flag then
  with Archive do
  begin
   Updated := False;
   try
    UpdateFile;
   except
    Result := E_EWRITE;
   end;
  end;
 end; //AddFileList
(* PackFiles *)
var
 Event: TArchiveEvent;
begin
 case CheckFileForWrite(PackedFile) of
  cfCanWrite:
  begin
   Archive := ArchiveClass.Create(PackedFile);
   try
    Archive.LoadArchive;
   except
    Result := E_UNKNOWN_FORMAT;
    Archive.Free;
    Exit;
   end;
  end;
  cfNotExist: if PackerCaps and PK_CAPS_NEW <> 0 then
  begin
   Archive := ArchiveClass.Create(PackedFile);
  end else
  begin
   Result := E_NOT_SUPPORTED;
   Exit;
  end;
  else
  begin
   Result := E_EWRITE;
   Exit;
  end;
 end;
 Event := TArchiveEvent.Create;
 try
  Event.FArchive := Archive;
  Event.FCurrentItem := NIL;
  Archive.OnProgress := Event.ArchiveProgress;
  Result := AddFileList(SrcPath, SubPath, AddList, Flags);
 finally
  Event.Free;
 end;
 Archive.Free;
end; //PackFilesW

function DeleteFiles(PackedFile, DeleteList: PAnsiChar): Integer; stdcall;
var
 Archive: TCustomArchive;

 function DeleteFileList(FileList: PAnsiChar): Integer;
 var
  P: PAnsiChar; S: WideString; ch_flag: Boolean;
  SR: TArchiveSearchRec;
 begin
  P := Pointer(FileList);
  ch_flag := False;
  with Archive do While P^ <> #0 do
  begin
   S := P; Inc(P, StrLen(P) + 1);
   if ArcFindFirst(S, faAnyFile, SR) then
   repeat
    ch_flag := ArcDeleteFile(SR.Item);
   until not ArcFindNext(SR);
 {  if (Length(S) > 4) and
      (S[Length(S)] = '*') and
      (S[Length(S) - 1] = '.') and
      (S[Length(S) - 2] = '*') and
      (S[Length(S) - 3] = '\') then
   begin
    SetLength(S, Length(S) - 4);
    N := Files[S];
    if N <> NIL then
    begin
     if N.FileAttr and faDirectory <> 0 then
      ch_flag := ArcDeleteFile(N);
     repeat
      N := FolderFiles[S];
      if N = NIL then Break;
      ch_flag := ArcDeleteFile(N);
     until False;
    end;
   end Else
   begin
    N := Files[S];
    ch_flag := ArcDeleteFile(N);
   end;     }
  end;
  Result := E_OK;
  if ch_flag then
  with Archive do
  begin
   Updated := False;
   try
    UpdateFile;
   except
    Result := E_EWRITE;
   end;
  end;
 end; //DeleteFileList
(* DeleteFiles *)
var
 Event: TArchiveEvent;
begin
 if PackerCaps and PK_CAPS_DELETE = 0 then
 begin
  Result := E_NOT_SUPPORTED;
  Exit;
 end;
 if CheckFileForWrite(PackedFile) = cfCanWrite then
 begin
  try
   Archive := ArchiveClass.Create(PackedFile);
   try
    Archive.LoadArchive;
    if Archive.CanDelete then
    begin
     Event := TArchiveEvent.Create;
     try
      Event.FArchive := Archive;
      Event.FCurrentItem := NIL;
      Archive.OnProgress := Event.ArchiveProgress;
      Result := DeleteFileList(DeleteList);
     finally
      Event.Free;
     end;
    end else Result := E_NOT_SUPPORTED;
   finally
    Archive.Free;
   end;
  except
   Result := E_UNKNOWN_FORMAT;
  end;
 end Else Result := E_EWRITE;
end; //DeleteFiles

function DeleteFilesW(PackedFile, DeleteList: PWideChar): Integer; stdcall;
var
 Archive: TCustomArchive;

 function DeleteFileList(FileList: PWideChar): Integer;
 var
  P: PWideChar; S: WideString; ch_flag: Boolean;
  SR: TArchiveSearchRec;
 begin
  P := Pointer(FileList);
  ch_flag := False;
  with Archive do while P^ <> #0 do
  begin
   S := P; Inc(P, WideStrLen(P) + 1);
   if ArcFindFirst(S, faAnyFile, SR) then
   repeat
    ch_flag := ArcDeleteFile(SR.Item);
   until not ArcFindNext(SR);
   {if (Length(S) > 4) and
      (S[Length(S)] = '*') and
      (S[Length(S) - 1] = '.') and
      (S[Length(S) - 2] = '*') and
      (S[Length(S) - 3] = '\') then
   begin
    SetLength(S, Length(S) - 4);
    N := Files[S];
    if N <> NIL then
    begin
     if N.FileAttr and faDirectory <> 0 then
      ch_flag := ArcDeleteFile(N);
     repeat
      N := FolderFiles[S];
      if N = NIL then Break;
      ch_flag := ArcDeleteFile(N);
     until False;
    end;
   end Else
   begin
    N := Files[S];
    ch_flag := ArcDeleteFile(N);
   end;  }
  end;
  Result := E_OK;
  if ch_flag then
  with Archive do
  begin
   Updated := False;
   try
    UpdateFile;
   except
    Result := E_EWRITE;
   end;
  end;
 end; //DeleteFileList
(* DeleteFiles *)
var
 Event: TArchiveEvent;
begin
 if PackerCaps and PK_CAPS_DELETE = 0 then
 begin
  Result := E_NOT_SUPPORTED;
  Exit;
 end;
 if CheckFileForWrite(PackedFile) = cfCanWrite then
 begin
  try
   Archive := ArchiveClass.Create(PackedFile);
   try
    Archive.LoadArchive;
    if Archive.CanDelete then
    begin
     Event := TArchiveEvent.Create;
     try
      Event.FArchive := Archive;
      Event.FCurrentItem := NIL;
      Archive.OnProgress := Event.ArchiveProgress;
      Result := DeleteFileList(DeleteList);
     finally
      Event.Free;
     end;
    end else Result := E_NOT_SUPPORTED;
   finally
    Archive.Free;
   end;
  except
   Result := E_UNKNOWN_FORMAT;
  end;
 end Else Result := E_EWRITE;
end; //DeleteFilesW

end.
