unit wcxutils;

interface

uses wcxhead, basearc;

Function OpenArchive(Var ArchiveData: TOpenArchiveData): HANDLE; stdcall; export;
Function ReadHeader(hArcData: HANDLE; Var HeaderData: THeaderData): Integer; stdcall; export;
Function ReadHeaderEx(hArcData: HANDLE; Var HeaderData: THeaderDataEx): Integer; stdcall; export;
Function ProcessFile(hArcData: HANDLE; Operation: Integer;
                     DestPath: PChar; DestName: PChar): Integer; stdcall; export;
Function CloseArchive(hArcData: HANDLE): Integer; stdcall; export;
Procedure SetChangeVolProc(hArcData: HANDLE; ChangeVolPrc: TChangeVolProc); stdcall; export;
Procedure SetProcessDataProc(hArcData: HANDLE; ProcessDataPrc: TProcessDataProc); stdcall; export;
Function GetPackerCaps: Integer; stdcall; export;
Function PackFiles(PackedFile, SubPath, SrcPath, AddList: PChar; Flags: Integer): Integer; stdcall; export;
Function DeleteFiles(PackedFile, DeleteList: PChar): Integer; stdcall; export;

Type
 TArchiveClass = class of TCustomArchive;

Var
 ArchiveClass: TArchiveClass;
 ArchiveItemClass: TArchiveItemClass;
 Caps: Integer;

implementation

Function OpenArchive(Var ArchiveData: TOpenArchiveData): HANDLE; stdcall;
Var Archive: TCustomArchive;
begin
 Archive := ArchiveClass.Create(ArchiveData.ArcName, ArchiveItemClass, amOpen);
 ArchiveData.OpenResult := Archive.ArcResult;
 If ArchiveData.OpenResult = 0 then
 begin
  ArchiveData.OpenMode := PK_OM_EXTRACT;
  Result := Integer(Archive);
 end Else
 begin
  Result := 0;
  Archive.Free;
 end;
end;

Function ReadHeader(hArcData: HANDLE; Var HeaderData: THeaderData): Integer; stdcall;
Var Archive: TCustomArchive absolute hArcData; N: TArchiveItem; L: Integer;
begin
 If Archive <> NIL then
 begin
  Result := 0;
  N := Archive.ReadHeader;
  If N <> NIL then With N do
  begin
   L := Length(FileName);
   If L > 259 then L := 259;
   FillChar(HeaderData, SizeOf(THeaderData), 0);
   Move(N.FileName[1], HeaderData.FileName[0], L);
   HeaderData.FileTime := FileTime;
   HeaderData.PackSize := PackedSize;
   HeaderData.UnpSize := FileSize;
   If Directory then
    HeaderData.FileAttr := $10 Else
    HeaderData.FileAttr := $20;
  end Else Result := E_END_ARCHIVE;
 end Else Result := E_EOPEN;
end;

Function ReadHeaderEx(hArcData: HANDLE; Var HeaderData: THeaderDataEx): Integer; stdcall;
Var Archive: TCustomArchive absolute hArcData; N: TArchiveItem; L: Integer;
begin
 If Archive <> NIL then
 begin
  Result := 0;
  N := Archive.ReadHeader;
  If N <> NIL then With N do
  begin
   L := Length(FileName);
   If L > 1023 then L := 1023;
   FillChar(HeaderData, SizeOf(THeaderDataEx), 0);
   Move(N.FileName[1], HeaderData.FileName[0], L);
   HeaderData.FileTime := FileTime;
   HeaderData.PackSize := PackedSize;
   HeaderData.UnpSize := FileSize;
   If Directory then
    HeaderData.FileAttr := $10 Else
    HeaderData.FileAttr := $20;
  end Else Result := E_END_ARCHIVE;
 end Else Result := E_EOPEN;
end;

Function ProcessFile(hArcData: HANDLE; Operation: Integer;
                     DestPath: PChar; DestName: PChar): Integer; stdcall;
Var Archive: TCustomArchive absolute hArcData;
begin
 If Archive <> NIL then
  Result := Archive.ProcessFile(Operation, DestPath, DestName) Else
  Result := E_BAD_DATA;
end;

Function CloseArchive(hArcData: HANDLE): Integer; stdcall;
Var Archive: TCustomArchive absolute hArcData;
begin
 If Archive <> NIL then
 begin
  Archive.Free;
  Result := 0;
 end Else Result := E_ECLOSE;
end;

Procedure SetChangeVolProc(hArcData: HANDLE; ChangeVolPrc: TChangeVolProc); stdcall;
begin
 @ChangeVolProc := Addr(ChangeVolPrc);
end;

Procedure SetProcessDataProc(hArcData: HANDLE; ProcessDataPrc: TProcessDataProc); stdcall;
begin
 @ProcessDataProc := Addr(ProcessDataPrc);
end;

Function GetPackerCaps: Integer; stdcall;
begin
 Result := Caps;
end;

Function PackFiles(PackedFile, SubPath, SrcPath, AddList: PChar; Flags: Integer): Integer; stdcall;
Var Archive: TCustomArchive;
begin
 Case CheckFile(PackedFile) of
  1:
  begin
   Archive := ArchiveClass.Create(PackedFile, ArchiveItemClass, amOpen);
   If Archive.ArcResult <> 0 then
   begin
    Archive.Free;
    Result := Archive.ArcResult;
    Exit;
   end;
  end;
  0: If Caps and PK_CAPS_NEW = PK_CAPS_NEW then
      Archive := ArchiveClass.Create(PackedFile, ArchiveItemClass) Else
     begin
      Result := E_NOT_SUPPORTED;
      Exit;
     end;
  Else Archive := NIL;
 end;
 If Archive <> NIL then
 begin
  Archive.AddFiles(SrcPath, SubPath, AddList, Flags);
  Result := Archive.UpdateFile;
  Archive.Free;
 end Else Result := E_EWRITE;
end;

Function DeleteFiles(PackedFile, DeleteList: PChar): Integer; stdcall;
Var Archive: TCustomArchive;
begin
 If Caps and PK_CAPS_DELETE <> PK_CAPS_DELETE then
 begin
  Result := E_NOT_SUPPORTED;
  Exit;
 end;
 If CheckFile(PackedFile) = 1 then
 begin
  Archive := ArchiveClass.Create(PackedFile, ArchiveItemClass, amOpen);
  If Archive.ArcResult <> 0 then
  begin
   Archive.Free;
   Archive := NIL;
  end;
 end Else Archive := NIL;
 If Archive <> NIL then
 begin
  Archive.DeleteFiles(DeleteList);
  Result := Archive.UpdateFile;
  Archive.Free;
 end Else Result := E_EWRITE;
end;

end.
