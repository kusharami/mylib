program JavaClassTexter;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, TntClasses, MyUtils,
  javaclass;

Var
 Java: TJavaClassEUC; List: TTntStringList; SR: TJavaStringSearchRec;

begin
 Java := TJavaClassEUC.Create;
 If Java.LoadFromFile('ST0210_eng.class') then
 begin
  List := TTntStringList.Create;
  If Java.FindFirstString(SR) then
  Repeat
   List.Add('@' + IntToStr(SR.PoolIndex));
   List.Add(Java.WideStrings[SR.PoolIndex]);
   List.Add('');
  Until not Java.FindNextString(SR);
  List.SaveToFile('ST0210.txt');
  List.Free;
  Java.WideStrings[244] := '    "����������� �������"'#10;
  Java.SaveToFile('ST0210.class');
 end;
 Java.Free;
end.
