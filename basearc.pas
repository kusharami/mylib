unit basearc;

interface

Uses wcxhead, nodelst, classes;

Type
 TArchiveItem = Class
 private
  FFileName: String;
  FDirectory: Boolean;
  FCompression: Cardinal;
  FPosition: Cardinal;
  FFileSize: Cardinal;
  FPackedSize: Cardinal;
  FExternal: Boolean;
  FSource: String;
  FDeleteAfterUse: Boolean;
  FFileTime: Cardinal;
 public
  Constructor Create(Const FileName: String; IsDir: Boolean = False); virtual;
  Destructor Destroy; override;
  Property FileName: String read FFileName write FFileName;
  Property Directory: Boolean read FDirectory write FDirectory;
  Property Position: Cardinal read FPosition write FPosition;
  Property FileSize: Cardinal read FFileSize write FFileSize;
  Property PackedSize: Cardinal read FPackedSize write FPackedSize;
  Property ExternalFile: Boolean read FExternal write FExternal;
  Property SourceFile: String read FSource write FSource;
  Property FileTime: Cardinal read FFileTime write FFileTime;
  Property Compression: Cardinal read FCompression write FCompression;
  Property DeleteAfterUse: Boolean read FDeleteAfterUse write FDeleteAfterUse;
 end;
 TArchiveItemClass = class of TArchiveItem;
 TArchiveCreateMode = (amCreate, amOpen);
 TCustomArchive = Class(TNodeList)
 private
  FArchiveName: String;
  FCurrentFile: PNodeRec;
  FArchiveTime: Integer;
  FArcResult: Integer;
  FCompress: Cardinal;
  FItemClass: TArchiveItemClass;
  FArcCompression: Boolean;
  FLoaded: Boolean;
  Function GetByName(FName: String): PNodeRec;
  Function GetByPath(Path: String): PNodeRec;
 public
  function LoadArchive: Integer; virtual;
  Constructor Create(Const FileName: String;
              ItemClass: TArchiveItemClass;
              CreateMode: TArchiveCreateMode = amCreate); virtual;
  Destructor Destroy; override;
  Function AddItem(Const FileName: String): TArchiveItem;
  Procedure AddFiles(SrcPath, SubPath: String; FileList: PChar; Flags: Integer);
  Procedure DeleteFiles(FileList: PChar);
  Function UpdateFile: Integer; virtual; abstract;
  Procedure ReadInit;
  Function ReadHeader: TArchiveItem;
  Procedure NextItem;
  Function TestItem: Integer; virtual; abstract;
  Function ExtractItem(DestPath, DestName: PChar): Integer; virtual; abstract;
  Function ProcessFile(Operation: Integer; DestPath, DestName: PChar): Integer;
  property ArcCompression: Boolean read FArcCompression write FArcCompression;
  property Compress: Cardinal read FCompress write FCompress;
  property ArcResult: Integer read FArcResult write FArcResult;
  property ArchiveName: String read FArchiveName write FArchiveName;
  property ArchiveTime: Integer read FArchiveTime write FArchiveTime;
  property CurrentFile: PNodeRec read FCurrentFile write FCurrentFile;
  Property Names[Name: String]: PNodeRec read GetByName;
  Property Dirs[Path: String]: PNodeRec read GetByPath;

  property ItemClass: TArchiveItemClass read FItemClass write FItemClass;
  property Loaded: Boolean read FLoaded;
    procedure DeleteOriginalFiles;
 end;

Function CheckFile(Const FileName: String): Integer;
Function CopyData(Const FileName: String; FileSize: Integer; Input, Output: TStream): Integer;
Procedure Redot(Const Dir: String; Var S: String);
function MyProcessDataPrc(FileName: PChar; Size: LongInt): LongInt; stdcall;

Var
 CopyBuffer: Pointer;
 ChangeVolProc: TChangeVolProc;
 ProcessDataProc: TProcessDataProc;

implementation

Uses SysUtils;

Procedure Redot(Const Dir: String; Var S: String);
Var U, D: String; J, I, K: Integer;
begin
 If Dir = '' then Exit;
 If Pos('.\', S) = 1 then Delete(S, 1, 2); 
 If Pos('\', S) < 1 then
 begin
  D := Dir;
  If D[Length(D)] <> '\' then D := D + '\';
  S := D + S;
  Exit;
 end;
 U := S;
 I := 0;
 While Pos('..\', U) > 0 do
 begin
  Delete(U, Pos('..\', U), 3);
  Inc(I);
 end;
 D := Dir;
 If D[Length(D)] <> '\' then D := D + '\';
 For J := 1 to I do
 begin
  K := Length(D) - 1;
  While (K > 0) and (D[K] <> '\') do Dec(k);
  Delete(D, K + 1, Length(D) - (K + 1) + 1);
 end;
 If I > 0 then S := D + U;
end;

Function TCustomArchive.GetByName(FName: String): PNodeRec;
Var N: PNodeRec;
begin
 N := Root; Result := NIL;
 FName := UpperCase(FName);
 While N <> NIL do With N^, Node as TArchiveItem do
 begin
  If FName = UpperCase(FileName) then
  begin
   Result := N;
   Exit;
  end;
  N := Next;
 end;
end;

Function TCustomArchive.GetByPath(Path: String): PNodeRec;
Var N: PNodeRec;
begin
 N := Root; Result := NIL;
 Path := UpperCase(Path);
 While N <> NIL do With N^, Node as TArchiveItem do
 begin
  If Pos(Path, UpperCase(FileName)) = 1 then
  begin
   Result := N;
   Exit;
  end;
  N := Next;
 end;
end;

Constructor TCustomArchive.Create(Const FileName: String;
              ItemClass: TArchiveItemClass;
              CreateMode: TArchiveCreateMode = amCreate);
begin
 inherited Create;
 FArchiveName := FileName;
 FCurrentFile := NIL;
 FItemClass := ItemClass;
 FArcCompression := True;
 If CreateMode = amOpen then
  FArcResult := LoadArchive Else
  FArcResult := 0;
end;

Destructor TCustomArchive.Destroy;
begin
 FArchiveName := '';
 inherited Destroy;
end;

Procedure TCustomArchive.AddFiles(SrcPath, SubPath: String; FileList: PChar;
  Flags: Integer);
Var
 S, SS: String; N: PNodeRec; FileInfo: TArchiveItem;
 SR: TSearchRec; P: PChar;
begin
 If (SrcPath <> '') and (SrcPath[Length(SrcPath)] <> '\') then
  SrcPath := SrcPath + '\';
 If (SubPath <> '') and (SubPath[Length(SubPath)] <> '\') then
  SubPath := SubPath + '\';
 P := Addr(FileList^);
 While P^ <> #0 do
 begin
  SS := P; Inc(P, StrLen(P) + 1);
  If (SS <> '') and (SS[Length(SS)] = '\') then
  begin
   If Flags and PK_PACK_SAVE_PATHS = 0 then Continue;
   SetLength(SS, Length(SS) - 1);
  end;
  If Flags and PK_PACK_SAVE_PATHS = 0 then
   S := SubPath + ExtractFileName(SS) Else
   S := SubPath + SS;
  SS := SrcPath + SS;
  N := Names[S];
  If N <> NIL then
  begin
   FileInfo := N^.Node as TArchiveItem;
   If FileInfo.Directory then Continue;
  end Else FileInfo := NIL;
  If FindFirst(SS, faAnyFile, SR) = 0 then
  begin
   If SR.Attr and faDirectory = faDirectory then
   begin
    If FileInfo = NIL then
    begin
     FileInfo := AddItem(S);
     FileInfo.Directory := True;
     FileInfo.FileTime := FArchiveTime;
     If Flags and PK_PACK_MOVE_FILES <> 0 then
     begin
      FileInfo.DeleteAfterUse := True;
      FileInfo.SourceFile := SS;
     end;
    end;
   end Else
   begin
    If N = NIL then
    begin
     FileInfo := AddItem(S);
     FileInfo.Compression := Compress;
     FileInfo.FileTime := FArchiveTime;
    end;
    FileInfo.Directory := False;
    If ArcCompression then FileInfo.Compression := Compress;
    FileInfo.ExternalFile := True;
    FileInfo.SourceFile := SS;
    If Flags and PK_PACK_MOVE_FILES <> 0 then
     FileInfo.DeleteAfterUse := True;
   end;
   FindClose(SR);
  end Else
  begin
   If FileInfo = NIL then //Make dir
   begin
    FileInfo := AddItem(S);
    FileInfo.Directory := True;
    FileInfo.FileTime := ArchiveTime;
   end;
  end;
 end;
end;

Procedure TCustomArchive.DeleteFiles(FileList: PChar);
Var P: PChar; S: String; N: PNodeRec;
begin
 P := Addr(FileList^);
 While P^ <> #0 do
 begin
  S := P; Inc(P, StrLen(P) + 1);
  If (Length(S) > 4) and
     (S[Length(S)] = '*') and
     (S[Length(S) - 1] = '.') and
     (S[Length(S) - 2] = '*') and
     (S[Length(S) - 3] = '\') then
  begin
   System.Delete(S, Length(S) - 3, 4);
   Repeat
    N := Dirs[S];
    If N <> NIL then Remove(N);
   Until N = NIL;
  end Else
  begin
   N := Names[S];
   If N <> NIL then Remove(N);
  end;
 end;
end;

Procedure TCustomArchive.ReadInit;
begin
 FCurrentFile := Root;
end;

Function TCustomArchive.ReadHeader: TArchiveItem;
begin
 Result := NIL;
 If FCurrentFile = NIL then
 begin
  FCurrentFile := Root;
  Exit;
 end Else With FCurrentFile^ do If Node <> NIL then
  Result := Node as TArchiveItem;
end;

Procedure TCustomArchive.NextItem;
begin
 If FCurrentFile <> NIL then FCurrentFile := FCurrentFile^.Next;
end;

Constructor TArchiveItem.Create(Const FileName: String; IsDir: Boolean = False);
begin
 FFileName := FileName;
 FDirectory := IsDir;
 FExternal := False;
 FSource := '';
end;

Destructor TArchiveItem.Destroy;
begin
 Finalize(FFileName);
 Finalize(FSource);
 inherited Destroy;
end;

Function TCustomArchive.AddItem(Const FileName: String): TArchiveItem;
Var N: PNodeRec;
begin
 N := Add;
 If N <> NIL then With N^ do
 begin
  Result := FItemClass.Create(FileName);
  Node := Result;
 end Else Result := NIL;
end;

Function CheckFile(Const FileName: String): Integer;
Var SR: TSearchRec;
begin
 Result := 0;
 If FindFirst(FileName, faAnyFile, SR) = 0 then
 begin
  With SR do
  If (Attr and 1 <> 1) and (Attr and 8 <> 8) and
     (Attr and 16 <> 16) and (Attr and 64 <> 64) then
   Result := 1 Else
   Result := 2;
  FindClose(SR);
 end;
end;

Function TCustomArchive.ProcessFile(Operation: Integer; DestPath, DestName: PChar): Integer;
begin
 Result := 0;
 Case Operation of
  PK_SKIP: NextItem;
  PK_TEST:
  begin
   Result := TestItem;
   NextItem;
  end;
  PK_EXTRACT:
  begin
   Result := ExtractItem(DestPath, DestName);
   NextItem;
  end;
 end;
end;

function MyProcessDataPrc(FileName: PChar; Size: LongInt): LongInt; stdcall;
begin
 Result := -1;
end;

Function CopyData(Const FileName: String; FileSize: Integer; Input, Output: TStream): Integer;
Var FSZ, SZ: Integer;
begin
 Result := 0;
 FSZ := FileSize;
 SZ := 0;
 While FSZ > 0 do
 begin
  If FSZ >= 65536 then
  begin
   Input.Read(CopyBuffer^, 65536);
   Inc(Result, Output.Write(CopyBuffer^, 65536));
   Inc(SZ, 65536);
   Dec(FSZ, 65536);
   If ProcessDataProc(PChar(FileName), SZ) = 0 then
   begin
    Result := -1;
    Break;
   end;
  end Else
  begin
   Input.Read(CopyBuffer^, FSZ);
   Inc(Result, Output.Write(CopyBuffer^, FSZ));
   SZ := FileSize;
   FSZ := 0;
   ProcessDataProc(PChar(FileName), SZ);
  end;
 end;
end;

function TCustomArchive.LoadArchive: Integer;
begin
 Result := 0;
 FLoaded := True;
end;

procedure TCustomArchive.DeleteOriginalFiles;
Var N: PNodeRec; Item: TArchiveItem;
begin
 N := Root;
 While N <> NIL do With N^ do
 begin
  Item := Node as TArchiveItem;
  If Item <> NIL then With Item do
  If FDeleteAfterUse and not FDirectory and FileExists(FSource) then
  begin
   DeleteFile(FSource);
   FDeleteAfterUse := False;
   FSource := '';
   FExternal := False;
  end;
  N := Next;
 end;
 N := Root;
 While N <> NIL do With N^ do
 begin
  Item := Node as TArchiveItem;
  If Item <> NIL then With Item do
  If FDeleteAfterUse and FDirectory and DirectoryExists(FSource) then
  begin
   RemoveDir(FSource);
   FDeleteAfterUse := False;
   FSource := '';
   FExternal := False;
  end;
  N := Next;
 end;
end;

initialization
 GetMem(CopyBuffer, 65536);
 @ProcessDataProc := Addr(MyProcessDataPrc);
finalization
 FreeMem(CopyBuffer, 65536);
end.
