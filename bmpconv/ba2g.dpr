program ba2g;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  DIB;

Var PIC: TDIB; I: Integer;
begin
 PIC := TDIB.Create;
 PIC.LoadFromFile(ParamStr(1));
 If PIC.BitCount in [4, 8] then
 begin
  With PIC do For I := 0 to 255 do With ColorTable[I] do
  begin
   If rgbReserved >= 128 then
   begin
    rgbRed := 255;
    rgbGreen := 255;
    rgbBlue := 255;
   end Else
   begin
    rgbRed := rgbReserved shl 1;
    rgbGreen := rgbReserved shl 1;
    rgbBlue := rgbReserved shl 1;
    rgbReserved := 0;
   end;
  end;
  PIC.UpdatePalette;
  If ParamCount > 1 then
   PIC.SaveToFile(ParamStr(2)) Else
   PIC.SaveToFile(ParamStr(1));   
 end;
 PIC.Free;
end.
