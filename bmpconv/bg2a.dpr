program bg2a;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  DIB;

Var PIC: TDIB; I: Integer;
begin
 PIC := TDIB.Create;
 PIC.LoadFromFile(ParamStr(1));
 If PIC.BitCount in [4, 8] then
 begin
  With PIC do For I := 0 to 255 do With ColorTable[I] do
  begin
   rgbReserved := rgbRed;
   rgbRed := 255;
   rgbGreen := 255;
   rgbBlue := 255;
  end;
  PIC.UpdatePalette;
  If ParamCount > 1 then
   PIC.SaveToFile(ParamStr(2)) Else
   PIC.SaveToFile(ParamStr(1));   
 end;
 PIC.Free;
end.

