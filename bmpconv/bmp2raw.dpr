program bmp2raw;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes,
  bmpimg, BitmapEx;

const
 FL_HEADER_SHIFT = 16;
 FL_HEADER_MASK = 3;
 FL_FORMAT_SHIFT = FL_HEADER_SHIFT + 2;
 FL_FORMAT_MASK = 3;

var
 Pic: TCustomImage;
 FExt, Path, SavePath: String;
 SR: TSearchRec; Flags, X: Integer; //Y, L: Integer; P: PByte;
begin
 Path := ExtractFilePath(ParamStr(1));
 If (Path <> '') and (Path[Length(Path)] <> '\') then Path := Path + '\';
 If ParamCount >= 2 then
  Flags := StrToIntDef(ParamStr(2), 0) Else
  Flags := 0;
 if ParamCount >= 3 then
  SavePath := ParamStr(3) else
  SavePath := Path;
 If ParamCount >= 4 then
  FExt := ParamStr(4) Else
  FExt := '.raw';
 If FindFirst(ParamStr(1), $20, SR) = 0 then
 begin
  Repeat
   Write('Extracting raw data: "' + SR.Name + '"... ');
   Pic := TCustomImage.Create(TBitmapFile);
   try
    Pic.LoadFromFile(Path + SR.Name);
    if Word(Flags) <> 0 then
     Pic.Reallocate(Pic.Width, Pic.Height,
       TImageFormat((Flags shr FL_FORMAT_SHIFT) and 3), [Word(Flags)]);
    with TFileStream.Create(ChangeFileExt(SR.Name, FExt), fmCreate) do
    try
     case (Flags shr FL_HEADER_SHIFT) and FL_HEADER_MASK of
      1:
      begin
       X := Pic.Width;
       WriteBuffer(X, 1);
       X := Pic.Height;
       WriteBuffer(X, 1);
      end;
      2:
      begin
       X := Pic.Width;
       WriteBuffer(X, 2);
       X := Pic.Height;
       WriteBuffer(X, 2);
      end;
      3:
      begin
       X := Pic.Width;
       WriteBuffer(X, 4);
       X := Pic.Height;
       WriteBuffer(X, 4);
      end;
     end;
     WriteBuffer(Pic.ImageData^, Pic.ImageSize);
    finally
     Free;
    end;
   finally
    Pic.Free;
   end;
   Writeln('Done.');
  Until FindNext(SR) <> 0;
  FindClose(SR);
 end;
end.
