program bmp2tm2;

{$APPTYPE CONSOLE}

uses
  SysUtils, DIB;

Const
 TIM2Sign = $324D4954;

Type
 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;
 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;

Var Pic, Pic2: TDIB; Header: TTIM2Header; F: File; B: Byte; FExt, Path: String;
    Layer: TLayerHeader; W, H: Integer; Y: Integer; SR: TSearchRec;
    Shift: Boolean;
begin
 Path := ExtractFilePath(ParamStr(1));
 If ParamCount >= 2 then
  Shift := ParamStr(2) = '1' Else
  Shift := False;
 If ParamCount >= 3 then
  FExt := ParamStr(3) Else
  FExt := '.tm2';
 If (Path <> '') and (Path[Length(Path)] <> '\') then Path := Path + '\';
 If FindFirst(ParamStr(1), $20, SR) = 0 then
 begin
  Repeat
   Write('Converting: "' + SR.Name + '"... ');
   Pic := TDIB.Create;
   Pic.LoadFromFile(Path + SR.Name);
   If Pic.BitCount = 4 then
   begin
    Pic2 := TDIB.Create;
    Pic2.PixelFormat := Pic.PixelFormat;
    Pic2.BitCount := 8;
    Pic2.Width := Pic.Width;
    Pic2.Height := Pic.Height;
    Pic2.ColorTable := Pic.ColorTable;
    Pic2.UpdatePalette;
    For Y := 0 to Pic.Height - 1 do
     For W := 0 to Pic.Width - 1 do
      Pic2.Pixels[W, Y] := Pic.Pixels[W, Y];
    Pic.Free;
    Pic := Pic2;
   end;
   If Pic.BitCount = 8 then
   begin
    Header.thSignTag := TIM2Sign;
    Header.thFormatTag := 3;
    Header.thAlign128 := False;
    Header.thLayersCount := 1;
    Header.thReserved1 := 0;
    Header.thReserved2 := 0;
    W := Pic.Width;
    H := Pic.Height;
    AssignFile(F, ChangeFileExt(Path + SR.Name, FExt));
    Rewrite(F, 1);
    BlockWrite(F, Header, SizeOf(Header));
    FillChar(Layer, SizeOf(Layer), 0);
    Layer.lhImageSize := W * H;
    Layer.lhPaletteSize := 1024;
    Layer.lhLayerSize := SizeOf(Layer) + Layer.lhImageSize + 1024;
    Layer.lhHeaderSize := SizeOf(Layer);
    Layer.lhColorsUsed := 256;
    Layer.lh256 := 256;
    Layer.lhControlByte := $83;
    Layer.lhFormat := 5;
    Layer.lhWidth := W;
    Layer.lhHeight := H;
    BlockWrite(F, Layer, SizeOf(Layer));
    With Pic do For Y := 0 to H - 1 do
     BlockWrite(F, ScanLine[Y]^, W);
    With Pic do For Y := 0 to 255 do With ColorTable[Y] do
    begin
     B := rgbRed;
     rgbRed := rgbBlue;
     rgbBlue := B;
     If Shift then
     begin
      If rgbReserved = 255 then
       rgbReserved := $80 Else
       rgbReserved := rgbReserved shr 1;
     end;
    end;
    BlockWrite(F, Pic.ColorTable, 1024);
    CloseFile(F);
   end;
   Pic.Free;
   Writeln('Done.');
  Until FindNext(SR) <> 0;
  FindClose(SR);
 end;
end.
