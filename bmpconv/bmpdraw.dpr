program bmpdraw;

{$APPTYPE CONSOLE}

uses
  Windows,
  SysUtils,
  BitmapEx,
  tim2 in '..\tim2.pas';

function RDTSC: Int64;
asm
 rdtsc
end;

Function CompMem(P1, P2: PChar; Length: Integer): Boolean;
Begin
 Result := True;
 If Length > 0 Then
  While Length <> 0 Do
  Begin
   If P1^ <> P2^ Then
   Begin
    Result := False;
    Exit;
   End;
   Inc(P1);
   Inc(P2);
   Dec(Length);
  End;
End;

function CompareMem(P1, P2: Pointer; Length: Integer): Boolean; assembler;
asm
        PUSH    ESI
        PUSH    EDI
        MOV     ESI,P1
        MOV     EDI,P2
        MOV     EDX,ECX
        XOR     EAX,EAX
        AND     EDX,3
        SAR     ECX,2
        JS      @@1     // Negative Length implies identity.
        jz      @@3
        REPE    CMPSD
        JNE     @@2
@@3:
        MOV     ECX,EDX
        REPE    CMPSB
        JNE     @@2
@@1:    INC     EAX
@@2:    POP     EDI
        POP     ESI
end;

const
 XXX = LongWord(255 shl 24);
 TestImg: array [0..63+4] of Byte =
 ($10,$00,$00,$00, // Scanline 0
  $17,$00,$00,$00, // Scanline 1
  $20,$00,$00,$00, // Scanline 2
  $24,$00,$00,$00, // Scanline 3
  $05,$07,$FF,$FE,$FD,$FC,$FB,$FA,$F9, $00,$00,
  $10,$09,$F8,$F7,$F6,$F5,$F4,$F3,$F2,$F1,$F0, $00,$00,
  $00,$04,$E0,$E1,$E2,$E3, $00,$00,
  $10,$10,$C0,$C1,$C2,$C3,$C4,$C5,$C6,$C7,$C8,$C9,$CA,$CB,$CC,$CD,$CE,$CF, $00,$00);

var
 Spr: TCustomImage;
 Pic: TCustomImage;
// a1, a2: array [Byte] of Char;
// cf: TColorFormat;
// J, X: Integer; X1, X2: Int64;
// Y: TRGBQuad;  // Threshold: array[0..3] of Word = (28, 151, 77, 0);
begin
(* Spr := TCustomImage.Create(TTIM2_File);
 try
  Spr.LoadFromFile(ParamStr(1));
  Spr.Compress;
  Spr.ImageFileClass := TBitmapExFile;
  Spr.SaveToFile(ChangeFileExt(ParamStr(1), '.ali'));
{  Spr.Left := 2;
//  Spr.Top := -1;
  Spr.Width := 32;
  Spr.Height := 4;
  Spr.ImageData := @TestImg;
  Spr.ImageFlags := IMG_COMPRESSED or (8 - 1);
  Spr.ImageFormat := ifColor8bit;
  Spr.ColorFormat := GrayScaleFormat;
  Spr.SaveToFile('testfile.bmp');}
 finally
  Spr.Free;
 end;   *)
{ asm
  mov eax,edx
 end;
 X1 := RDTSC;
 for J := 1 to 100 do
 if CompMem(@a1, @a2, 2) then;
 X2 := RDTSC;
 writeln(x2 - x1);
 X1 := RDTSC;
 for J := 1 to 100 do 
 if CompareMem(@a1, @a2, 2) then;
 X2 := RDTSC;
 writeln(x2 - x1);
{ cf := TColorFormat.Create('RGBA2222*');
 try
  Y.rgbRed := 24;
  Y.rgbGreen := 149;
  Y.rgbBlue := 211;
  Y.rgbReserved := 255;
  X1 := RDTSC;
  X := cf.FromRGBQuad(LongWord(Y));
  LongWord(Y) := cf.ValueToRGBQuad(X);
  X2 := RDTSC;
  if X <> 0 then;
  writeln(x2 - x1);
 finally
  cf.Free;
 end; }
 try
  Spr := TCustomImage.Create(TBitmapExFile);
  try
   Spr.LoadFromFile(ParamStr(1));
   Pic := TCustomImage.Create(TBitmapFile);
   try
    Pic.LoadFromFile(ParamStr(2));
    Spr.Draw(Pic, StrToIntDef(ParamStr(4),
    (Pic.Width - Spr.Width) div 2), StrToIntDef(ParamStr(5),
    (Pic.Height - Spr.Height) div 2),
             DRAW_ALPHA_BLEND);
    Pic.SaveToFile(ParamStr(3));
   finally
    Pic.Free;
   end;
  finally
   Spr.Free;
  end;
 except
  on E: Exception do
  begin
   Writeln(E.Message);
  end;
 end;
end.
