program fixdif;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes;

Var
 LastToken: String = '';
 LastChars: String = '';

Function GetToken(Var P: PChar; Var Line, Cursor: Integer; Up: Boolean = True): String;
Var D: PChar;
begin
 Result := '';
 LastChars := '';
 If P = NIL then Exit;
 Case P^ of
  #0: Exit;
  '(', ')', '{', '}', '=', '+', '-', '*', '#', ',', ';', '^', '@', '.', ':', '<', '>', '?', '!':
  begin
   Inc(Cursor);
   Result := P^;
   Inc(P);
  end;
  Else
  begin
   If P^ = #13 then
   begin
    LastChars := LastChars + #13;
    Inc(P);
   end;
   If P^ = #10 then
   begin
    LastChars := LastChars + #10;
    Inc(Line);
    Inc(P);
    Cursor := 0;
   end;
   While P^ in [' ', #9, #13, #10] do
   begin
    Inc(Cursor);
    LastChars := LastChars + P^;
    Inc(P);
   end;
   If P^ in ['(', ')', '{', '}', '=', '+', '#', '-', '*', ',', ';', '^', '@', '.', ':', '<', '>', '?', '!'] then
   begin
    Inc(Cursor);
    Result := P^;
    Inc(P);
   end Else If P^ = '/' then
   begin
    LastChars := LastChars + P^;
    Inc(Cursor);
    Inc(P);
    Case P^ of
     '/':
     begin
      While not (P^ in [#0, #13, #10]) do
      begin
       LastChars := LastChars + P^;
       Inc(Cursor);
       Inc(P);
      end;
      Result := GetToken(P, Line, Cursor, Up);
     end;
     '*':
     begin
      LastChars := LastChars + P^;     
      Inc(P);
      Repeat
       If P^ = '*' then
       begin
        Inc(Cursor);
        LastChars := LastChars + P^;
        Inc(P);
        If P^ = '/' then
        begin
         Inc(Cursor);
         LastChars := LastChars + P^;
         Inc(P);
         Result := GetToken(P, Line, Cursor, Up);
         Break;
        end;
       end;
       If P^ = #13 then
       begin
        LastChars := LastChars + P^;
        Inc(P);
       end;
       If P^ = #10 then
       begin
        LastChars := LastChars + P^;
        Inc(Line);
        Inc(P);
        Cursor := 0;
       end Else
       begin
        LastChars := LastChars + P^;
        Inc(Cursor);
        Inc(P);
       end;
      Until False;
     end;
     Else Result := '/';
    end;
   end Else If P^ = '"' then
   begin
    Repeat
     If P^ = '\' then
     begin
      D := Addr(P^);
      Inc(D);
      If D^ = '"' then
      begin
       Result := Result + '\';
       Inc(Cursor);
       Inc(P);
      end;
     end;
     Result := Result + P^;
     Inc(Cursor);
     Inc(P);
    Until P^ in ['"', #0, #13, #10];
    If P^ = '"' then
    begin
     Result := Result + '"';
     Inc(Cursor);
     Inc(P);
    end;
   end Else If P^ <> #0 then
   begin
    Repeat
     If Up then
      Result := Result + UpCase(P^) Else
      Result := Result + P^;
     Inc(Cursor);
     Inc(P);
    Until P^ in [#0..#32, '(', ')', '{', '}', '=', '#', '+', '-', '*', ',', ';', '^', '@', '.', ':', '?', '!', '<', '>', '"'];
   end;
  end;
 end;
 LastToken := Result;
end;


Var List: TStringList; S, S2, Min: String; I, J, K, Cnt, A, B, X: Integer; P: PChar;
 SR: TSearchRec;
begin
 List := TStringList.Create;
 If FindFirst(ParamStr(1), faArchive, SR) = 0 then
 begin
  Repeat
  List.LoadFromFile(SR.Name);
  A := 0; B := 0;
  S := List[0];
  If S = 'header' then
  begin
   X := StrToInt(List[5]);
   Cnt := StrToInt(List[7]);
   I := 8;
   For J := 1 to Cnt do
   begin
    S := List[I] + #0;
    S2 := '';
    P := Addr(S[1]);
    LastChars := '';
    For K := 1 to 9 do If GetToken(P, A, B) <> '' then
    begin
     If LastToken = '-' then
     begin
      Min := LastChars;
      GetToken(P, A, B);
      LastToken := '-' + LastToken;
      LastChars := Min;
     end;
     S2 := S2 + LastChars;
     If K = 5 then
     begin
      A := StrToInt(LastToken);
      A := -A;
      Inc(A, X);
      S2 := S2 + IntToStr(A);
     end Else S2 := S2 + LastToken;
    end;
    List[I] := S2;
    Inc(I);
   end;
  end;
  List.SaveToFile(ChangeFileExt(SR.Name, '.dif'));
  Until FindNext(SR) <> 0;
  FindClose(SR);
 end;
 List.Free;
end.

