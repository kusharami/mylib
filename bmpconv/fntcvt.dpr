program fntcvt;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  BmpImg;

var
 Source: TStream;
 CW, CH, IH, Cnt, X, BitmapSize, ChOnLine: Integer;
 Bitmap, FontData: Pointer;
begin
 try
  Source := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyWrite);
  try
   Source.Seek(StrToIntDef(ParamStr(3), 0), soFromBeginning);
   CW := StrToIntDef(ParamStr(4), 0);
   CH := StrToIntDef(ParamStr(5), 0);
   Cnt := StrToIntDef(ParamStr(6), 0);
   ChOnLine := StrToIntDef(ParamStr(7), 16);
   X := ((CW + 7) div 8) * CH * Cnt;
   GetMem(FontData, X);
   try
    Source.ReadBuffer(FontData^, X);
    IH := (Cnt + (ChOnLine - 1)) div ChOnLine;
    BitmapSize := GetBitmapSize(ChOnLine * CW, IH * CH, 1);
    GetMem(Bitmap, BitmapSize);
    try
     FontToBMP(FontData^, Bitmap^, CW, CH, ChOnLine, IH, Cnt);
     with TFileStream.Create(ParamStr(2), fmCreate) do
     try
      WriteBuffer(Bitmap^, BitmapSize);
     finally
      Free;
     end;
     Writeln('Converted.');
    finally
     FreeMem(Bitmap);
    end;
   finally
    FreeMem(FontData);
   end;
  finally
   Source.Free;
  end;
 except
  on E: Exception do Writeln(E.Message);
 end;
end.
