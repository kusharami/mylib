program strconv;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  TntClasses;

Type
 TPtrRec = Packed Record
  ID: Cardinal;
  Ptr: Integer;
 end;

Var
 F: TFileStream; List: TTntStringList; WS: WideString; WC: WideChar;
 I, FileID, Cnt, SPos: Integer; PtrList: Array of TPtrRec;
begin
 F := TFileStream.Create(ParamStr(1), fmOpenRead);
 List := TTntStringList.Create;
 F.Read(FileID, 4);
 If FileID = 2 then
 begin
  F.Read(Cnt, 4);
  SetLength(PtrList, Cnt);
  F.Read(PtrList[0], Cnt * 8);
  SPos := F.Position;
  For I := 0 to Cnt - 1 do With PtrList[I] do
  begin
   List.Add('[' + IntToStr(ID) + ']');
   F.Position := SPos + Ptr * 2;
   WS := '';
   While True do
   begin
    F.Read(WC, 2);
    If WC = #0 then Break;
    If WC = #1 then
    begin
     F.Read(WC, 2);
     WS := WS + '<c=' + IntToStr(Word(WC)) + '>';
    end Else
     WS := WS + WC;
   end;
   List.Add(WS);
   List.Add('');
  end;
 end;
 List.SaveToFile(ParamStr(2));
 List.Free;
 F.Free;
end.
 