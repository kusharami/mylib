program tm2tobap;

{$APPTYPE CONSOLE}

uses
  SysUtils;

Const
 TIM2Sign = $324D4954;
 BAPSign = $706162;

Type
 TBapHeader = Packed Record
  Signature: Integer;
  PalettesCount: Integer;
  ColorsCount: Integer;
  Zero: Integer;
 end;
 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;
 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;

Var Header: TTIM2Header; F, F1: File; B: Byte; BB: ^Byte;
    Layer: TLayerHeader; W, H: Integer; Y, R: Integer;
    Pal: Array[Byte] of Integer;
    Path, FExt: String; SR: TSearchRec; Bap: TBapHeader;
begin
 Path := ExtractFilePath(ParamStr(1));
 If ParamCount >= 2 then
  FExt := ParamStr(2) Else
  FExt := '.bap';
 If (Path <> '') and (Path[Length(Path)] <> '\') then Path := Path + '\';
 If FindFirst(ParamStr(1), $20, SR) = 0 then
 begin
  Repeat
   Write('Extracting palette from: "' + SR.Name + '"... ');
   AssignFile(F, Path + SR.Name);
   Reset(F, 1);
   BlockRead(F, Header, SizeOf(Header), R);
   If (Header.thSignTag = TIM2Sign) and
      (Header.thFormatTag in [3, 4]) and
      (Header.thLayersCount >= 1) then
   begin
    BlockRead(F, Layer, SizeOf(Layer), R);
    If (Layer.lhControlByte = $83) and (Layer.lhFormat = 5) and
       (Layer.lhPaletteSize = 1024) then
    begin
     AssignFile(F1, ChangeFileExt(Path + SR.Name, FExt));
     Rewrite(F1, 1);
     BAP.Signature := BAPSign;
     BAP.PalettesCount := 1;
     BAP.ColorsCount := 256;
     BAP.Zero := 0;
     Seek(F, FilePos(F) + Integer(Layer.lhWidth) * Integer(Layer.lhHeight));
     BlockRead(F, Pal, 1024, R);
     BlockWrite(F1, BAP, SizeOf(BAP));
     BlockWrite(F1, Pal, 1024);
     CloseFile(F1);
    end;
   end;
   CloseFile(F);
   Writeln('Done.');
  Until FindNext(SR) <> 0;
  FindClose(SR);
 end;
end.
