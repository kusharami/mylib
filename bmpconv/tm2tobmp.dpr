program tm2tobmp;

{$APPTYPE CONSOLE}

uses
  SysUtils, DIB;

Const
 TIM2Sign = $324D4954;

Type
 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;
 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;

Var Pic: TDIB; Header: TTIM2Header; F: File; B: Byte; BB: ^Byte;
    Layer: TLayerHeader; W, H: Integer; Y, R: Integer;
    Path, FExT: String; Shift: Boolean; SR: TSearchRec;
begin
 Path := ExtractFilePath(ParamStr(1));
 If ParamCount >= 2 then
  Shift := ParamStr(2) = '1' Else
  Shift := False;
 If ParamCount >= 3 then
  FExt := ParamStr(3) Else
  FExt := '.bmp';
 If (Path <> '') and (Path[Length(Path)] <> '\') then Path := Path + '\';
 If FindFirst(ParamStr(1), $20, SR) = 0 then
 begin
  Repeat
   Write('Converting: "' + SR.Name + '"... ');
   AssignFile(F, Path + SR.Name);
   Reset(F, 1);
   BlockRead(F, Header, SizeOf(Header), R);
   If (Header.thSignTag = TIM2Sign) and
      (Header.thFormatTag in [3, 4]) and
      (Header.thLayersCount >= 1) then
   begin
    BlockRead(F, Layer, SizeOf(Layer), R);
    If (Layer.lhControlByte = $83) and (Layer.lhFormat = 5) and
       (Layer.lhPaletteSize = 1024) then
    begin
     Pic := TDIB.Create;
     Pic.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
     Pic.BitCount := 8;
     W := Layer.lhWidth;
     H := Layer.lhHeight;
     Pic.Width := W;
     Pic.Height := H;
     With Pic do For Y := 0 to H - 1 do
      BlockRead(F, ScanLine[Y]^, W, R);
     BlockRead(F, Pic.ColorTable, 1024, R);
     With Pic do For Y := 0 to 255 do With ColorTable[Y] do
     begin
      B := rgbRed;
      rgbRed := rgbBlue;
      rgbBlue := B;
      If Shift then
      begin
       If rgbReserved = 128 then
        rgbReserved := 255 Else
        rgbReserved := rgbReserved shl 1;
      end;
     end;
     Pic.UpdatePalette;
     Pic.SaveToFile(ChangeFileExt(Path + SR.Name, FExt));
     Pic.Free;
    end Else
    If (Layer.lhControlByte = $03) and (Layer.lhFormat = 4) and
       (Layer.lhPaletteSize = 64) then
    begin
     Pic := TDIB.Create;
     Pic.PixelFormat := MakeDIBPixelFormat(8, 8, 8);
     Pic.BitCount := 4;
     W := Layer.lhWidth;
     H := Layer.lhHeight;
     Pic.Width := W;
     Pic.Height := H;
     With Pic do For Y := 0 to H - 1 do
     begin
      BlockRead(F, ScanLine[Y]^, WidthBytes, R);
      BB := ScanLine[Y];
      For R := 1 to WidthBytes do
      begin
       B := BB^;
       BB^ := (B shr 4) or ((B and 15) shl 4);
       Inc(BB);
      end;
     end;
     BlockRead(F, Pic.ColorTable, 64, R);
     With Pic do For Y := 0 to 15 do With ColorTable[Y] do
     begin
      B := rgbRed;
      rgbRed := rgbBlue;
      rgbBlue := B;
      If Shift then
      begin
       If rgbReserved = 128 then
        rgbReserved := 255 Else
        rgbReserved := rgbReserved shl 1;
      end;
     end;
     Pic.UpdatePalette;
     Pic.SaveToFile(ChangeFileExt(Path + SR.Name, FExt));
     Pic.Free;
    end
   end;
   CloseFile(F);
   Writeln('Done.');
  Until FindNext(SR) <> 0;
  FindClose(SR);
 end;
end.
