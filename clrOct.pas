unit clrOct;

interface

uses
  Windows, Sysutils, Classes;

Type
  PPNode = ^PNode;
  PNode = ^TNode;
  TNode = packed record
    IsLeaf: Boolean;
    PixelCount: Integer;
    RedSum: Integer;
    GreenSum: Integer;
    BlueSum: Integer;
    AlphaSum: Integer;
    Children: array [0..7] of PNode;
    Next: PNode;
  end;
  TColorQuantizer = class
  private
    FTree: PNode;
    FLeafCount: Integer;
    FReducibleNodes: array [0..8] of PNode;
    FMaxColors: Integer;
    FOutputMaxColors: Integer;
    FColorBits: Integer;
    procedure AddColor(var Node: PNode; Color: TRGBQuad; ColorBits,
      Level: Integer; var LeafCount: Integer; RNodes: PPNode);
    function CreateNode(Level, ColorBits: Integer; var LeafCount: Integer;
      RNodes: PPNode): PNode;
    procedure DeleteTree(var Tree: PNode);
    function GetColorBits: Integer;
    function GetColorCount: Integer;
    function GetMaxColors: Integer;    
    procedure GetPaletteColors(Tree: PNode; Palette: PRGBQuad;
      var Index: Integer; Sum: PInteger);
    procedure ReduceTree(ColorBits: Integer; var LeafCount: Integer;
      RNodes: PPNode);
  public
    property ColorBits: Integer
        read GetColorBits;
    property MaxColors: Integer
        read GetMaxColors;
    property ColorsCount: Integer
        read GetColorCount;

    procedure Clear;
    constructor Create; overload;
    constructor Create(MaxColors, ColorBits: Integer); overload;
    destructor Destroy; override;
    procedure Reset(MaxColors, ColorBits: Integer);
    procedure ProcessBuffer(const Buffer; Count: Integer);
    procedure SetColorTable(var Palette);
  end;

implementation

procedure TColorQuantizer.AddColor(var Node: PNode; Color: TRGBQuad; ColorBits,
  Level: Integer; var LeafCount: Integer; RNodes: PPNode);
var Shift, ML: Integer;
begin
 // If the node doesn't exist, create it.
 if Node = NIL then Node := CreateNode(Level, ColorBits, LeafCount, RNodes);
 with Node^, Color do
 if IsLeaf then // Update color information if it's a leaf node.
 begin
  Inc(PixelCount);
  Inc(RedSum, rgbRed);
  Inc(GreenSum, rgbGreen);
  Inc(BlueSum, rgbBlue);
  Inc(AlphaSum, rgbReserved);
 end else // Recurse a level deeper if the node is not a leaf.
 begin
  Shift := 7 - Level; ML := 1 shl Shift;
  AddColor(Node.Children[(((rgbRed and ML) shr Shift) shl 2) or
                       (((rgbGreen and ML) shr Shift) shl 1) or
                         ((rgbBlue and ML) shr Shift)], Color,
                         ColorBits, Level + 1, LeafCount, RNodes);
 end;
end;

procedure TColorQuantizer.Clear;
begin
 DeleteTree(FTree);
end;

constructor TColorQuantizer.Create;
begin
 FColorBits := 8;
 FTree := NIL;
 FLeafCount := 0;
 FillChar(FReducibleNodes, SizeOf(FReducibleNodes), 0);
 FMaxColors := 256;
 FOutputMaxColors := 256;
end;

constructor TColorQuantizer.Create(MaxColors, ColorBits: Integer);
begin
 if ColorBits < 8 then
  FColorBits := ColorBits Else
  FColorBits := 8;
 FTree := NIL;
 FLeafCount := 0;
 FillChar(FReducibleNodes, SizeOf(FReducibleNodes), 0);
 FMaxColors := MaxColors;
 FOutputMaxColors := MaxColors;
 if FMaxColors < 16 then FMaxColors := 16;
end;

function TColorQuantizer.CreateNode(Level, ColorBits: Integer;
  var LeafCount: Integer; RNodes: PPNode): PNode;
begin
 New(Result);
 With Result^ do
 begin
  IsLeaf := Level = ColorBits;
  if IsLeaf then Inc(LeafCount) else
  begin
   Inc(RNodes, Level);
   Next := RNodes^;
   RNodes^ := Result;
  end;
 end;
end;

procedure TColorQuantizer.DeleteTree(var Tree: PNode);
var I: Integer;
begin
 if Tree = NIL then Exit;
 With Tree^ do for I := 0 to 7 do
  DeleteTree(Children[I]);
 Dispose(Tree);
 Tree := NIL;
end;

destructor TColorQuantizer.Destroy;
begin
 DeleteTree(FTree);
 inherited;
end;

function TColorQuantizer.GetColorBits: Integer;
begin
 Result := FColorBits;
end;

function TColorQuantizer.GetColorCount: Integer;
begin
 Result := FLeafCount;
end;

function TColorQuantizer.GetMaxColors: Integer;
begin
 Result := FOutputMaxColors;
end;

procedure TColorQuantizer.GetPaletteColors(Tree: PNode; Palette: PRGBQuad;
  var Index: Integer; Sum: PInteger);
var I: Integer;
begin
 if (Tree <> NIL) and (Palette <> NIL) then
 begin
  with Tree^ do if IsLeaf then with Palette^ do
  begin
   rgbRed := RedSum div PixelCount;
   rgbGreen := GreenSum div PixelCount;
   rgbBlue := BlueSum div PixelCount;
   rgbReserved := AlphaSum div PixelCount;
   if Sum <> NIL then
   begin
    Inc(Sum, Index);
    Sum^ := PixelCount;
   end;
   Inc(Index);
  end else for I := 0 to 7 do
   GetPaletteColors(Children[I], Palette, Index, Sum);
 end;
end;

procedure TColorQuantizer.Reset(MaxColors, ColorBits: Integer);
begin
 if ColorBits < 8 then
  FColorBits := ColorBits Else
  FColorBits := 8;
 DeleteTree(FTree);
 FLeafCount := 0;
 FillChar(FReducibleNodes, SizeOf(FReducibleNodes), 0);
 FMaxColors := MaxColors;
 FOutputMaxColors := MaxColors;
 if FMaxColors < 16 then FMaxColors := 16;
end;

procedure TColorQuantizer.ProcessBuffer(const Buffer; Count: Integer);
var I: Integer; P: PRGBQuad;
begin
 P := @Buffer;
 for I := 0 to Count - 1 do
 begin
  AddColor(FTree, P^, FColorBits, 0, FLeafCount, Addr(FReducibleNodes));
  while FLeadCount > FMaxColors do
   ReduceTree(FColorBits, FLeafCount, Addr(FReducibleNodes));
  Inc(P);
 end;
end;

procedure TColorQuantizer.ReduceTree(ColorBits: Integer;
  var LeafCount: Integer; RNodes: PPNode);
var I: Integer; P: PPNode; Node, Child: PNode; R, G, B, A, ChildCount: Integer;
begin
 // Find the deepest level containing at least one reducible node.
 I := ColorBits - 1;
 P := RNodes; Inc(P, I);
 while (I > 0) and (P^ = NIL) do
 begin
  Dec(I);
  Dec(P);
 end;
 // Reduce the node most recently added to the list at level I.
 Node := P^;
 P^ := Node.Next;
 R := 0; G := 0; B := 0; A := 0; ChildCount := 0;
 with Node^ do
 begin
  for I := 0 to 7 do
  begin
   Child := Children[I];
   if Child <> NIL then
   begin
    with Child^ do
    begin
     Inc(R, RedSum);
     Inc(G, GreenSum);
     Inc(B, BlueSum);
     Inc(A, AlphaSum);
     Inc(Node.PixelCount, PixelCount);
    end;
    Dispose(Child);
    Children[I] := NIL;
    Inc(ChildCount);
   end;
  end;
  IsLeaf := True;
  RedSum := R;
  GreenSum := G;
  BlueSum := B;
  AlphaSum := A;
 end;
 Dec(LeafCount, ChildCount - 1);
end;

procedure TColorQuantizer.SetColorTable(var Palette);
var
 PRGB: PRGBQuad; Index: Integer;
 Sum: array [0..15] of Integer;
 Temp: array [0..15] of TRGBQuad;
 J, K, NR, NG, NB, NA, NS, A, B: Integer;
begin
 Index := 0;
 if FOutputMaxColors < 16 then
 begin
  GetPaletteColors(FTree, Addr(Temp), Index, Addr(Sum));
  if FLeafCount > FOutputMaxColors then
  begin
   PRGB := Addr(Palette);
   for J := 0 to FOutputMaxColors - 1 do
   begin
    A := (J * FLeafCount) div FOutputMaxColors;
    B := ((J + 1) * FLeafCount) div FOutputMaxColors;
    NR := 0; NG := 0; NB := 0; NA := 0; NS := 0;
    for K := A to B - 1 do with Temp[K] do
    begin
     Inc(NR, rgbRed * Sum[K]);
     Inc(NG, rgbGreen * Sum[K]);
     Inc(NB, rgbBlue * Sum[K]);
     Inc(NA, rgbReserved * Sum[K]);
     Inc(NS, Sum[K]);
    end;
    With PRGB^ do
    begin
     rgbRed := NR div NS;
     rgbGreen := NG div NS;
     rgbBlue := NB div NS;
     rgbReserved := NA div NS;
    end;
    Inc(PRGB);
   end;
  end else Move(Temp, Palette, FLeafCount * SizeOf(TRGBQuad));
 end else GetPaletteColors(FTree, Addr(Palette), Index, NIL);
end;

end.
