program cpConvert;

{$APPTYPE CONSOLE}

Uses SysUtils;

{
Type
 TNode = Packed Record
  Index: Word;
  Len: Word;
 end;
 TCharRec = Packed Record
  Code: Word;
  Str: String;
 end;

Var SR: TSearchRec; FExt: String;

Const
 ABCArray: Array[$00..$9c] of Char =
 ' !#$%&''()*+,-./0123456789:;<=>?'+
 'ABCDEFGHIJKLMNOPQRSTUVWXYZ[]_' +
 'abcdefghijklmnopqrstuvwxyz{}{���'+ //$5A
 '�����Ũ��������������������������'+
 '��������������������������������';
 MAXCHAR = 257;
 CharRecArray: Array[0..MAXCHAR] of TCharRec =
 (
  (Code: $FF02; Str: ' '),
  (Code: $FF40; Str: '�'),
  (Code: $FF41; Str: '�'),
  (Code: $FF42; Str: '�'),
  (Code: $FF43; Str: '�'),
  (Code: $FF44; Str: '�'),
  (Code: $FF45; Str: '�'),
  (Code: $FF46; Str: '�'),
  (Code: $FF47; Str: '�'),
  (Code: $FF48; Str: '�'),
  (Code: $FF49; Str: '�'),
  (Code: $FF4A; Str: '�'),
  (Code: $FF4B; Str: '�'),
  (Code: $FF4C; Str: '�'),
  (Code: $FF4D; Str: '�'),
  (Code: $FF4E; Str: '�'),
  (Code: $FF4F; Str: '�'),
  (Code: $FF50; Str: '�'),
  (Code: $FF51; Str: '�'),
  (Code: $FF52; Str: '�'),
  (Code: $FF53; Str: '�'),
  (Code: $FF54; Str: '�'),
  (Code: $FF55; Str: '�'),
  (Code: $FF56; Str: '�'),
  (Code: $FF57; Str: '�'),
  (Code: $FF58; Str: '�'),
  (Code: $FF59; Str: '�'),
  (Code: $FF5A; Str: '�'),
  (Code: $FF5B; Str: '�'),
  (Code: $FF5C; Str: '�'),
  (Code: $FF5D; Str: '�'),
  (Code: $FF5E; Str: '�'),
  (Code: $FF5F; Str: '�'),
  (Code: $FF60; Str: '�'),
  (Code: $FF61; Str: '�'),
  (Code: $FF62; Str: '�'),
  (Code: $FF63; Str: '�'),
  (Code: $FF64; Str: '�'),
  (Code: $FF65; Str: '�'),
  (Code: $FF66; Str: '�'),
  (Code: $FF67; Str: '�'),
  (Code: $FF68; Str: '�'),
  (Code: $FF69; Str: '�'),
  (Code: $FF6A; Str: '�'),
  (Code: $FF6B; Str: '�'),
  (Code: $FF6C; Str: '�'),
  (Code: $FF6D; Str: '�'),
  (Code: $FF6E; Str: '�'),
  (Code: $FF6F; Str: '�'),
  (Code: $FF70; Str: '�'),
  (Code: $FF71; Str: '�'),
  (Code: $FF72; Str: '�'),
  (Code: $FF73; Str: '�'),
  (Code: $FF74; Str: '�'),
  (Code: $FF75; Str: '�'),
  (Code: $FF76; Str: '�'),
  (Code: $FF77; Str: '�'),
  (Code: $FF78; Str: '�'),
  (Code: $FF79; Str: '�'),
  (Code: $FF7A; Str: '�'),
  (Code: $FF7B; Str: '�'),
  (Code: $FF7C; Str: '�'),
  (Code: $FF7D; Str: '�'),
  (Code: $FF7E; Str: '�'),
  (Code: $FF7F; Str: '�'),
  (Code: $FF3F; Str: '�'),
  (Code: $FF3E; Str: '�'),
  (Code: $FF21; Str: '"'),
  (Code: $FF22; Str: '�'),
  (Code: $FF23; Str: '�'),
  (Code: $FF3A; Str: '�'),
  (Code: $FD00; Str: '[select]'),
  (Code: $FD01; Str: '[start]'),
  (Code: $FD02; Str: '[l1]'),
  (Code: $FD03; Str: '[r1]'),
  (Code: $FD04; Str: '[l2]'),
  (Code: $FD05; Str: '[r2]'),
  (Code: $FD06; Str: '(C)'),
  (Code: $FD07; Str: '(A)'),
  (Code: $FD08; Str: '(X)'),
  (Code: $FD09; Str: '(#)'),
  (Code: $FD0D; Str: '[!]'),
  (Code: $FD0E; Str: '[heart]'),
  (Code: $FD0F; Str: '[clef]'),
  (Code: $FD10; Str: '[daze]'),
  (Code: $FD11; Str: '[sita]'),
  (Code: $FD12; Str: '[Ex:]'),
  (Code: $FD13; Str: '[G]'),
  (Code: $FD13; Str: '<gold>'),
  (Code: $FD14; Str: '[E]'),
  (Code: $FD15; Str: '[km]'),
  (Code: $FD16; Str: '[lv_]'),
  (Code: $FD17; Str: '[star]'),
  (Code: $FD18; Str: '[button]'),
  (Code: $FD19; Str: '[C]'),
  (Code: $FD19; Str: '<coin>'),
  (Code: $FD1A; Str: '[(!)]'),
  (Code: $FD1B; Str: '[||>>]'),
  (Code: $FD22; Str: '*'),
  (Code: $FD24; Str: '[cross]'),
  (Code: $FD25; Str: '[|>]'),
  (Code: $FD26; Str: '[hp]'),
  (Code: $FD27; Str: '[mp]'),
  (Code: $FD28; Str: '[lv]'),
  (Code: $FF04; Str: '<se_low>'),
  (Code: $FF05; Str: '<se_def>'),
  (Code: $FF06; Str: '<se_high>'),
  (Code: $FF07; Str: '<close>'),
  (Code: $FF08; Str: '<fraction>'),
  (Code: $FF09; Str: '<border>'),
  (Code: $FF0A; Str: '</fraction>'),
  (Code: $FF0B; Str: '<set_x='),
  (Code: $FF0C; Str: '<def>'),
  (Code: $FF0D; Str: '<red>'),
  (Code: $FF0E; Str: '<blue>'),
  (Code: $FF0F; Str: '<green>'),
  (Code: $FF10; Str: '<gray>'),
  (Code: $FF14; Str: '<yellow>'),
  (Code: $FF1A; Str: '<brown>'),
  (Code: $FF16; Str: '<pinchi>'),
  (Code: $FF17; Str: '<hinshi>'),
  (Code: $FF18; Str: '<dead>'),
  (Code: $FF11; Str: '<wait_s>'),
  (Code: $FF12; Str: '<wait_m>'),
  (Code: $FF13; Str: '<wait_l>'),
  (Code: $FF15; Str: '<space_13>'),
  (Code: $FF19; Str: '<Cap>'),
  (Code: $FF01; Str: '<end>'),
  (Code: $FF24; Str: '<6>'),
  (Code: $FF25; Str: '<9>'),
  (Code: $FF26; Str: '<1>'),
  (Code: $FF21; Str: '<66>'),
  (Code: $FF22; Str: '<99>'),
  (Code: $FF23; Str: '<11>'),
  (Code: $FF80; Str: '<IF_SING val_1>'),
  (Code: $FF81; Str: '<ELSE_NOT_SING>'),
  (Code: $FF82; Str: '<ENDIF_SING>'),
  (Code: $FF83; Str: '<IF_SOLO>'),
  (Code: $FF84; Str: '<ELSE_NOT_SOLO>'),
  (Code: $FF85; Str: '<ENDIF_SOLO>'),
  (Code: $FF86; Str: '<IF_MALE>'),
  (Code: $FF87; Str: '<ELSE_NOT_MALE>'),
  (Code: $FF88; Str: '<ENDIF_MALE>'),
  (Code: $FF8D; Str: '<IF_character_1_MALE>'),
  (Code: $FF8E; Str: '<ELSE_NOT_character_1_MALE>'),
  (Code: $FF8F; Str: '<ENDIF_character_1_MALE>'),
  (Code: $FF89; Str: '<IF_character_2_MALE>'),
  (Code: $FF8A; Str: '<ELSE_NOT_character_2_MALE>'),
  (Code: $FF8B; Str: '<ENDIF_character_2_MALE>'),
  (Code: $FFA1; Str: '<NUM_I_NAME_1, val_1>'),
  (Code: $FF8C; Str: '<IF_SING val_2>'),
  (Code: $FF1D; Str: '<...>'),
  (Code: $FF1E; Str: '<->'),
  (Code: $FF3C; Str: '<plus>'),
  (Code: $FAFA; Str: '<hero>'),
  (Code: $FF1C; Str: '<:>'),
  (Code: $FF3D; Str: '<equal>'),
  (Code: $FF20; Str: '<-->'),
  (Code: $FF27; Str: '<l_arrow>'),
  (Code: $FF28; Str: '<r_arrow>'),
  (Code: $FF29; Str: '<u_arrow>'),
  (Code: $FF2A; Str: '<d_arrow>'),
  (Code: $FF2B; Str: '<2way_arrow>'),
  (Code: $FF2C; Str: '<doub_arrow>'),
  (Code: $FF2D; Str: '<star>'),
  (Code: $FF2E; Str: '<blackstar>'),
  (Code: $FF2F; Str: '<snow>'),
  (Code: $FF30; Str: '<tilde>'),
  (Code: $FF31; Str: '<music>'),
  (Code: $FF32; Str: '<music2>'),
  (Code: $FF33; Str: '<circle>'),
  (Code: $FF34; Str: '<triangle>'),
  (Code: $FF35; Str: '<blktriangle>'),
  (Code: $FF36; Str: '<invtriangle>'),
  (Code: $FF37; Str: '<invblktriangle>'),
  (Code: $FF38; Str: '<square>'),
  (Code: $FF39; Str: '<blksquare>'),
  (Code: $FF3A; Str: '<maru_R>'),
  (Code: $FF3B; Str: '<TM>'),
  (Code: $FFA2; Str: '<endash>'),
  (Code: $FBFE; Str: '<string_1>'),
  (Code: $FBFD; Str: '<string_2>'),
  (Code: $FBFC; Str: '<string_3>'),
  (Code: $FBFB; Str: '<string_4>'),
  (Code: $FBF2; Str: '<string_5>'),
  (Code: $FBF1; Str: '<string_6>'),
  (Code: $FBF0; Str: '<string_7>'),
  (Code: $FBEF; Str: '<string_8>'),
  (Code: $FBEE; Str: '<string_9>'),
  (Code: $FBED; Str: '<string_10>'),
  (Code: $FBEC; Str: '<string_11>'),
  (Code: $FBEB; Str: '<string_12>'),
  (Code: $FBEA; Str: '<string_13>'),
  (Code: $FBE9; Str: '<string_14>'),
  (Code: $FBE8; Str: '<string_15>'),
  //2C 00 1B FF 52 00 FA FB 04 F9 F8 FA F9 FA 
  (Code: $FBE7; Str: '<string_16>'),
  (Code: $FBFF; Str: '<val_0>'),
  (Code: $FBFA; Str: '<val_1>'),
  (Code: $FBF9; Str: '<val_2>'),
  (Code: $FBF8; Str: '<val_3>'),
  (Code: $FBF7; Str: '<val_4>'),
  (Code: $FBF6; Str: '<val_5>'),
  (Code: $FBF5; Str: '<val_6>'),
  (Code: $FBF4; Str: '<val_7>'),
  (Code: $FBF3; Str: '<val_8>'),
  (Code: $FBE6; Str: '<val_9>'),
  (Code: $FBE5; Str: '<val_10>'),
  (Code: $FBE4; Str: '<val_11>'),
  (Code: $FBE3; Str: '<val_12>'),
  (Code: $FBE2; Str: '<val_13>'),
  (Code: $FBE1; Str: '<val_14>'),
  (Code: $FBE0; Str: '<val_15>'),
  (Code: $FBDF; Str: '<val_16>'),
  (Code: $FBDE; Str: '<val_17>'),
  (Code: $FBDD; Str: '<val_18>'),
  (Code: $FBDC; Str: '<val_19>'),
  (Code: $FBDB; Str: '<val_20>'),
  (Code: $FAF9; Str: '<item_1>'),
  (Code: $FAF8; Str: '<item_2>'),
  (Code: $FAF7; Str: '<item_3>'),
  (Code: $FAF6; Str: '<item_4>'),
  (Code: $FAF5; Str: '<item_5>'),
  (Code: $FAF4; Str: '<item_6>'),
  (Code: $FAF3; Str: '<item_7>'),
  (Code: $FAF2; Str: '<item_8>'),
  (Code: $FAF1; Str: '<item_9>'),
  (Code: $FAF0; Str: '<item_10>'),
  (Code: $FAEF; Str: '<item_11>'),
  (Code: $FAEE; Str: '<item_12>'),
  (Code: $FAED; Str: '<item_13>'),
  (Code: $FAEC; Str: '<item_14>'),
  (Code: $FAEB; Str: '<item_15>'),
  (Code: $FAEA; Str: '<item_16>'),
  (Code: $FAE9; Str: '<INDEF_ART_SGL_I_NAME_1>'),
  (Code: $FAE8; Str: '<DEF_ART_SGL_I_NAME_1>'),
  (Code: $FAE7; Str: '<SGL_I_NAME_1>'),
  (Code: $FAE6; Str: '<PLR_I_NAME_1>'),
  (Code: $FAE5; Str: '<INDEF_ART_SGL_I_NAME_2>'),
  (Code: $FAE4; Str: '<DEF_ART_SGL_I_NAME_2>'),
  (Code: $FAE3; Str: '<SGL_I_NAME_2>'),
  (Code: $FAE2; Str: '<PLR_I_NAME_2>'),
  (Code: $FAE1; Str: '<INDEF_ART_SGL_I_NAME_3>'),
  (Code: $FAE0; Str: '<DEF_ART_SGL_I_NAME_3>'),
  (Code: $FADF; Str: '<SGL_I_NAME_3>'),
  (Code: $FADE; Str: '<PLR_I_NAME_3>'),
  (Code: $FADD; Str: '<INDEF_ART_SGL_I_NAME_4>'),
  (Code: $FADC; Str: '<DEF_ART_SGL_I_NAME_4>'),
  (Code: $FADB; Str: '<SGL_I_NAME_4>'),
  (Code: $FADA; Str: '<PLR_I_NAME_4>'),
  (Code: $FAD9; Str: '<INDEF_ART_SGL_I_NAME_5>'),
  (Code: $FAD8; Str: '<DEF_ART_SGL_I_NAME_5>'),
  (Code: $FAD7; Str: '<SGL_I_NAME_5>'),
  (Code: $FAD6; Str: '<PLR_I_NAME_5>'),
  (Code: $FAFB; Str: '<character_1>'),
  (Code: $FAFC; Str: '<character_2>'),
  (Code: $FAFD; Str: '<character_3>'),
  (Code: $FD23; Str: '<_>'),
  (Code: $FF1B; Str: '<|>')       //257
 );

Type
 PItem = ^TItem;
 TItem = Packed Record
  Index: Integer;
  Text: WideString;
  Next: PItem;
 end;
 TCodedList = Class
 private
  FRoot, FCur: PItem;
  FCount: Integer;
  Function Get(Index: Integer): PItem;
 public
  Constructor Create;
  Destructor Destroy; override;
  Function Add: PItem;
  Procedure Clear;
  Procedure Remove(Item: PItem);
  property Root: PItem read FRoot;
  property Cur: PItem read FCur;
  property Count: Integer read FCount;
  Property Items[Index: Integer]: PItem read Get;
 end;

Function TCodedList.Get(Index: Integer): PItem;
Var I: Integer; N: PItem;
begin
 I := 0; N := FRoot; Result := NIL;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Constructor TCodedList.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TCodedList.Destroy;
begin
 Clear;
 Inherited Destroy;
end;

Function TCodedList.Add: PItem;
begin
 New(Result);
 If FRoot = NIL then FRoot := Result Else FCur^.Next := Result;
 FCur := Result;
 FillChar(Result^, SizeOf(TItem), 0);
 Result^.Text := '';
 Inc(FCount);
end;

Procedure TCodedList.Clear;
Var N: PItem;
begin
 While FRoot <> NIL do
 begin
  With FRoot^ do
  begin
   N := Next;
   Text := '';
  end;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

Procedure TCodedList.Remove(Item: PItem);
Var P, PR: PItem;
begin
 P := FRoot; PR := NIL;
 While P <> NIL do
 begin
  If P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 If P = NIL then Exit;
 With P^ do
 begin
  If FRoot = P then
  begin
   If FRoot = FCur then FCur := Next;
   FRoot := Next;
  end Else If Next = NIL then
  begin
   PR^.Next := NIL;
   FCur := PR;
  end Else PR^.Next := P^.Next;
  WIth P^ do Text := '';
  Dispose(P);
  Dec(FCount);
 end;
end;

Var BINMODE: Boolean;

Function SIndex(const S: String): Integer;
Var I: Integer;
begin
 If S <> '' then
 begin
  If (not BINMODE) or (Length(S) > 1) then
  begin
   For I := 0 to MAXCHAR do With CharRecArray[I] do
   If S = Str then
   begin
    Result := Code;
    Exit;
   end;
  end;
  For I := 0 to $9C do If S = ABCArray[I] then
  begin
   Result := I;
   Exit;
  end;
 end;
 Result := -1;
end;

Function ConvertString(const S: String): WideString;
Var I, C, J, II: Integer; P: ^Char; CH, CC: Char; SS: String;
begin
 BINMODE := False;
 Result := '';
 If S <> '' then
 begin
  I := 0; C := Length(S);
  P := Addr(S[1]);
  While I < C do
  begin
   CH := P^;
   Inc(I);
   Inc(Integer(P));
   Case CH of
    '\': If I < C then
    begin
     If P^ = 'n' then
     begin
      Inc(I);
      Inc(Integer(P));
      Result := Result + WideChar($FF03);
      If I >= C then Exit;
     end;
    end;
    '<', '[', '(':
    begin
     CC := CH;
     Case CC of
      '<': CH := '>';
      '[': CH := ']';
      '(': CH := ')';
     end;
     J := I; SS := '';
     Repeat
      SS := SS + S[J];
      Inc(J);
      If J > C then Break;
      If (S[J] = '=') and (SS = '<space') then
      begin
       Inc(J);
       If J > C then Break;
       SS := '';
       Repeat
        SS := SS + S[J];
        If S[J] = '>' then Break;
        Inc(J);
        If J > C then Break;
       Until False;
       If (SS <>'') and (SS[Length(SS)] = '>') then
       begin
        Delete(SS, Length(SS), 1);
        Val(SS, J, II);
        If II = 0 then
        begin
         II := -2;
         Break;
        end;
       end;
      end Else If S[J] = CH then
      begin
       SS := SS + CH;
       Break;
      end;
     Until False;
     If II = -2 then
     begin
      Inc(I, Length(SS) + 7);
      Inc(Integer(P), Length(SS) + 7);
      Result := Result + WideChar($F900 + Byte(J));
      II := -1;
     end Else If SS = '<bin>' then
     begin
      BINMODE := True;
      Inc(I, Length(SS) - 1);
      Inc(Integer(P), Length(SS) - 1);
     end Else
     begin
      II := SIndex(SS);
      If II >= 0 then
      begin
       Inc(I, Length(SS) - 1);
       Inc(Integer(P), Length(SS) - 1);
       Result := Result + WideChar(II);
      end Else
      begin
       II := SIndex(CC);
       If II >= 0 then Result := Result + WideChar(II);
      end;
     end;
    end;
    Else
    begin
     If CH = ':' then BINMODE := False;
     II := SIndex(CH);
     If II >= 0 then Result := Result + WideChar(II);
    end;
   end;
  end;
  Result := Result + WideChar($FF00);
 end Else Result := WideChar($FF00);
end;

Function Encode(FileName: String): Boolean;
Var
 FileStream: TFileStream; StringList: TStringList; E: Integer;
 CodedList: TCodedList; WS: WideString; S: String; I, J: Integer;
 Nodes: Array of TNode;
begin
 try
  FileStream := TFileStream.Create(ChangeFileExt(FileName, FExt), fmCreate);
  StringList := TStringList.Create;
  try
   StringList.LoadFromFile(FileName);
   CodedList := TCodedList.Create;
   I := 0;
   While I < StringList.Count do
   begin
    S := Trim(StringList.Strings[I]);
    If (S <> '') and (S[1] = '@') then
    begin
     Delete(S, 1, 1);
     Val(S, J, E);
     If E = 0 then
     begin
      Inc(I);
      WS := '';
      If I >= StringList.Count then With CodedList.Add^ do
      begin
       Index := J;
       Text := #$FF00;
      end Else
      Repeat
       S := StringList.Strings[I];
       Inc(I);
       If not ((Length(S) >= 2) and (S[1] = '/') and (S[2] = '/')) then
        WS := WS + ConvertString(S);
       If I < StringList.Count then
       begin
        S := Trim(StringList.Strings[I]);
       end;
       If (I >= StringList.Count) or ((S <> '') and (S[1] = '@')) then
       begin
        With CodedList.Add^ do
        begin
         Index := J;
         Text := #$FF00 + WS;
         If WS[Length(WS)] = #$FF00 then
          SetLength(Text, Length(Text) - 2);
        end;
        Break;
       end;
      Until False;
     end;
    end;
   end;
   SetLength(Nodes, CodedList.Count);
   FileStream.Write(CodedList.FCount, 4);
   With CodedList do For I := 0 to Count - 1 do With Items[I]^ do
   begin
    Nodes[I].Index := Index;
    If I = 0 then
     Nodes[I].Len := Count + 3 Else
     Nodes[I].Len := J;
    J := Length(Text) + 1;
   end;
   FileStream.Write(Nodes[0], CodedList.Count * SizeOf(TNode));
   J := $FF01;
   FileStream.Write(J, 2);
   With CodedList do For I := 0 to Count - 1 do With Items[I]^ do
   begin
    FileStream.Write(Text[1], Length(Text) shl 1);
    FileStream.Write(J, 2);
   end;
   CodedList.Free;
   Result := True;
  except
   on E: Exception do Result := False;
  end;
  StringList.Free;
  FileStream.Free;
 except
  on E: Exception do Result := False;
 end;
end;

Function GetString(C: Word): String;
Var I: Integer;
begin
 If C <= $9C then
 begin
  Result := ABCArray[C];
  Exit;
 end Else For I := 0 to MAXCHAR do With CharRecArray[I] do
 If C = Code then
 begin
  Result := Str;
  Exit;
 end;
 Result := '<hex_' + IntToHex(C, 4) + '>';
end;

Function Decode(FileName: String): Boolean;
Var
 FileStream: TFileStream; StringList: TStringList;
 Data: Array of TNode; W: Word;
 DataCount, R, P, I: Integer; S: String;
begin
 try
  FileStream := TFileStream.Create(FileName, fmOpenRead);
  StringList := TStringList.Create;
  R := FileStream.Read(DataCount, 4);
  DataCount := DataCount and $FFFF;
  Result := True;
  If R = 4 then
  begin
   try
    If DataCount > 0 then
    begin
     SetLength(Data, DataCount);
     FileStream.Read(Data[0], DataCount * SizeOf(TNode));
     S := '';
     P := DataCount;
     For I := 0 to DataCount - 1 do With Data[I] do
     begin
      Inc(P, Len);
      FileStream.Position := P * 2;
      S := S + '@' + IntToStr(Index);
      Repeat
       R := FileStream.Read(W, 2);
       If R <> 2 then Break;
       If (W >= $F900) and (W < $FA00) then S := S + Format('<space=%d>', [Byte(W)]) Else
       Case W of
        $FF00: S := S + #13#10;
        $FF01: S := S + #13#10#13#10;
        $FF03: S := S + '\n'#13#10;
        Else S := S + GetString(W);
       end;
      Until (W = $FF01) or (W = 0);
     end;
    end;
    StringList.Text := S;
    StringList.SaveToFile(ChangeFileExt(FileName, FExt));
   except
    on E: Exception do Result := False;
   end;
  end Else Result := False;
  StringList.Free;
  FileStream.Free;
 except
  on E: Exception do Result := False;
 end;
end;
}
function MultiByteToWideChar(CodePage, Flags: Integer; MBStr: PChar;
  MBCount: Integer; WCStr: PWideChar; WCCount: Integer): Integer; stdcall;
  external 'kernel32.dll' name 'MultiByteToWideChar';

function WideCharToMultiByte(CodePage, Flags: Integer; WCStr: PWideChar;
  WCCount: Integer; MBStr: PChar; MBCount: Integer; DefaultChar: PChar;
  UsedDefaultChar: Pointer): Integer; stdcall;
  external 'kernel32' name 'WideCharToMultiByte';

function GetLocaleInfo(Locale: Longint; LCType: Longint; lpLCData: PChar; cchData: Integer): Integer; stdcall;
  external 'kernel32.dll' name 'GetLocaleInfoA';

function GetThreadLocale: Longint; stdcall;
  external 'kernel32.dll' name 'GetThreadLocale';

function LCIDToCodePage(ALcid: LongWord): Integer;
const
  CP_ACP = 0;                                // system default code page
  LOCALE_IDEFAULTANSICODEPAGE = $00001004;   // default ansi code page
var
  ResultCode: Integer;
  Buffer: array [0..6] of Char;
begin
  GetLocaleInfo(ALcid, LOCALE_IDEFAULTANSICODEPAGE, Buffer, SizeOf(Buffer));
  Val(Buffer, Result, ResultCode);
  if ResultCode <> 0 then
    Result := CP_ACP;
end;

Function SwapWord(Value: Word): Word;
asm
 bswap eax
 shr eax, 16
end;

Procedure SwapEndian(Var S: WideString);
Var P: ^Word; L, I: Integer;
begin
 L := Length(S);
 I := 0;
 P := Addr(S[1]);
 While I < L do
 begin
  P^ := SwapWord(P^);
  Inc(I);
  Inc(P);
 end;
end;

Var
 FExt, S: String; SCP, DCP: Integer; W: Word; R: Integer;
 WS: WideString; F: File; SR: TSearchRec;
begin
 If ParamCount >= 3 then
 begin
  If ParamCount > 3 then
   FExt := ParamStr(4) Else
   FExt := '.txt';
  try
   SCP := StrToInt(ParamStr(2));
  except
   SCP := LCIDToCodePage(GetThreadLocale);
  end;
  try
   DCP := StrToInt(ParamStr(3));
  except
   DCP := 1200;
  end;
  If SCP = DCP then Exit;
  If FindFirst(ParamStr(1), $20, SR) = 0 then
  begin
   Repeat
    Writeln('Converting: ' + SR.Name);
    AssignFile(F, SR.Name);
    try
     Reset(F, 1);
    except
     Writeln('Error opening file.');
     Continue;
    end;
    WS := '';
    If (SCP = 1200) or (SCP = 1201) then
    begin
     BlockRead(F, W, 2, R);
     If (W = $FFFE) or (W = $FEFF) then
     begin
      If W = $FEFF then
       SCP := 1200 Else
       SCP := 1201;
      SetLength(WS, (FileSize(F) - 2) shr 1);
      BlockRead(F, WS[1], FileSize(F) - 2, R);
      CloseFile(F);
     end Else
     begin
      Writeln('Invalid source codepage.');
      CloseFile(F);
      Continue;
     end;
    end Else
    begin
     SetLength(S, FileSize(F));
     BlockRead(F, S[1], FileSize(F), R);
     SetLength(WS, R shl 2);
     SetLength(WS,
     MultiByteToWideChar(SCP, 0, Addr(S[1]), R, Addr(WS[1]), R shl 2));
     CloseFile(F);
    end;
    If ((DCP = 1201) and (SCP = 1200)) or
       ((DCP = 1200) and (SCP = 1201)) then SwapEndian(WS) Else
    begin
     AssignFile(F, ChangeFileExt(SR.Name, FExt));
     try
      Rewrite(F, 1);
     except
      Writeln('Error saving file.');
      Continue;
     end;
     If (DCP = 1200) or (DCP = 1201) then
     begin
      If DCP = 1200 then
       W := $FEFF Else
       W := $FFFE;
      BlockWrite(F, W, 2, R);
      BlockWrite(F, WS[1], Length(WS) shl 1, R);
     end Else
     begin
      R := Length(WS);
      SetLength(S, R shl 2);
      SetLength(S, WideCharToMultiByte(
      DCP, 0, Addr(WS[1]), R, Addr(S[1]), R shl 2, NIL, NIL));
      BlockWrite(F, S[1], Length(S), R);
     end;
     CloseFile(F);
    end;
   Until FindNext(SR) <> 0;
   FindClose(SR);
  end;
 end Else
 begin
  Writeln('Text files codepage converter.');
  Writeln;
  Writeln('USAGE: cpConvert filelist srccodepage destcodepage [newextension]');
 end;
end.
