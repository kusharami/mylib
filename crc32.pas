unit crc32;

interface

const
 DEFAULT_POLYNOMIAL = $04c11db7;

type
 TCRC32 = class
  private
   FCRC: LongWord;
   FPatch32: array[0..3] of Byte;
   FPatch64: array[0..7] of Byte;
   FTable: array[Byte] of LongWord;
   FReverse: array[Byte] of Byte;
   function Reflect(Value: LongWord; Count: Integer): LongWord;
  public
   property CRC: LongWord read FCRC write FCRC;

   procedure AppendByte(Value: Byte);
   procedure AppendBuffer(Buffer: Pointer; Size: Integer);
   function FindReverse32(desiredCRC: LongWord): PByte;
   function FindReverse64(desiredCRC: LongWord): PByte;

   procedure InitTables(Polynomial: LongWord);

   constructor Create(Polynomial: LongWord = DEFAULT_POLYNOMIAL);
   destructor Destroy; override;
 end;

implementation

{ TCRC32 }

procedure TCRC32.AppendBuffer(Buffer: Pointer; Size: Integer);
begin

end;

procedure TCRC32.AppendByte(Value: Byte);
begin

end;

constructor TCRC32.Create(Polynomial: LongWord);
begin
 InitTables(Polynomial);
end;

destructor TCRC32.Destroy;
begin

  inherited;
end;

function TCRC32.FindReverse32(desiredCRC: LongWord): PByte;
begin

end;

function TCRC32.FindReverse64(desiredCRC: LongWord): PByte;
begin

end;

procedure TCRC32.InitTables(Polynomial: LongWord);
var
 I, J: Integer;
 Value: Integer;
begin
 for I := 0 to 255 do
 begin
  Value := Reflect(I, 8) shl 24;
  for J := 0 to 7 do
  begin
   if Value < 0 then
    Value := (Value shl 1) xor Polynomial else
    Inc(Value, Value);
  end;
  FTable[I] := Reflect(Value, 32);
 end;
end;

function TCRC32.Reflect(Value: LongWord; Count: Integer): LongWord;
begin

end;

end.
