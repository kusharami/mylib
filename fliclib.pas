unit fliclib;

interface

uses
 Windows, SysUtils;

const
 ID_FLI        = $AF11; // For FLI files
 ID_FLC        = $AF12; // For standard FLC files.
 ID_FLC_EX1    = $AF44; // Upgraded to use colour depth other than 8.
 ID_FLC_EX2    = $AF30; // Upgraded to use Huffman or BWT compression.
 ID_FLC_EX3    = $AF31; // Upgraded to use "frame shift" compression. 
 CEL_DATA      = 3;     // registration and transparency
 COLOR_256     = 4;     // 256-level colour palette
 DELTA_FLC     = 7;
 FLI_SS2       = 7;     // delta image, word oriented RLE
 COLOR_64      = 11;    // 64-level colour palette
 DELTA_FLI     = 12;
 FLI_LC        = 12;    // delta image, byte oriented RLE
 BLACK         = 13;    // full black frame (rare)
 BYTE_RUN      = 15;
 FLI_BRUN      = 15;    // full image, byte oriented RLE
 FLI_COPY      = 16;    // uncompressed image (rare)
 PSTAMP        = 18;    // postage stamp (icon of the first frame)
 DTA_BRUN      = 25;    // full image, pixel oriented RLE
 DTA_COPY      = 26;    // uncompressed image
 DTA_LC        = 27;    // delta image, pixel oriented RLE
 LABEL_        = 31;    // frame label
 BMP_MASK      = 32;    // bitmap mask
 MLEV_MASK     = 33;    // multilevel mask
 SEGMENT       = 34;    // segment information
 KEY_IMAGE     = 35;    // key image, similar to BYTE_RUN / DTA_BRUN
 KEY_PAL       = 36;    // key palette, similar to COLOR_256
 REGION        = 37;    // region of frame differences
 WAVE          = 38;    // digitized audio
 USERSTRING    = 39;    // general purpose user data
 RGN_MASK      = 40;    // region mask
 LABELEX       = 41;    // extended frame label (includes symbolic name)
 SHIFT         = 42;    // scanline delta shifts (compression)
 PATHMAP       = 43;    // path map (segment transitions)
 PREFIX_TYPE   = $F100; // prefix chunk
 SCRIPT_CHUNK  = $F1E0; // embedded "pawn" script
 FRAME_TYPE    = $F1FA; // frame chunk
 SEGMENT_TABLE = $F1FB; // segment table chunk
 HUFFMAN_TABLE = $F1FC; // huffman table chunk

type
 TChunk = packed record
  fcSize: Cardinal;
  fcType: Word;
 end;

 TFLI_Header = packed record
  fhFrames:    Word;     // Number of frames in first segment
  fhWidth:     Word;     // FLIC width in pixels
  fhHeight:    Word;     // FLIC height in pixels
  fhDepth:     Word;     // Bits per pixel (usually 8)
  fhFlags:     Word;     // Set to zero or to three
  fhSpeed:     Cardinal; // Delay between frames
  fhReserved1: Word;     // Set to zero
  fhReserved2: array [0..23] of Byte; // Set to zero
  fhReserved3: array [0..39] of Byte; // Set to zero
 end;

 TFLC_Header = packed record
  fhFrames:    Word;     // Number of frames in first segment
  fhWidth:     Word;     // FLIC width in pixels
  fhHeight:    Word;     // FLIC height in pixels
  fhDepth:     Word;     // Bits per pixel (usually 8)
  fhFlags:     Word;     // Set to zero or to three
  fhSpeed:     Cardinal; // Delay between frames
  fhReserved1: Word;     // Set to zero
  fhCreated:   Cardinal; // Date of FLIC creation (FLC only)
  fhCreator:   Cardinal; // Serial number or compiler id (FLC only)
  fhUpdated:   Cardinal; // Date of FLIC update (FLC only)
  fhUpdater:   Cardinal; // Serial number (FLC only), see creator
  fhAspectDx:  Word;     // Width of square rectangle (FLC only)
  fhAspectDy:  Word;     // Height of square rectangle (FLC only)
  fhReserved2: array [0..23] of Byte; // Set to zero
  fhOFrame1:   Cardinal; // Offset to frame 1 (FLC only)
  fhOFrame2:   Cardinal; // Offset to frame 2 (FLC only)
  fhReserved3: array [0..39] of Byte; // Set to zero
 end;

 TEGI_Header = packed record
  ehFrames:      Word;     // Number of frames in first segment
  ehWidth:       Word;     // FLIC width in pixels
  ehHeight:      Word;     // FLIC height in pixels
  ehDepth:       Word;     // Bits per pixel (usually 8)
  ehFlags:       Word;     // Set to zero or to three
  ehSpeed:       Cardinal; // Delay between frames
  ehReserved1:   Word;     // Set to zero //
  ehCreated:     Cardinal; // Date of FLIC creation (FLC only)
  ehCreator:     Cardinal; // Serial number or compiler id (FLC only)
  ehUpdated:     Cardinal; // Date of FLIC update (FLC only)
  ehUpdater:     Cardinal; // Serial number (FLC only), see creator
  ehAspectDx:    Word;     // Width of square rectangle (FLC only)
  ehAspectDy:    Word;     // Height of square rectangle (FLC only)
  ehExtFlags:    Word;     // EGI: flags for specific EGI extensions
  ehKeyFrames:   Word;     // EGI: key-image frequency
  ehTotalFrames: Word;     // EGI: total number of frames (segments)
  ehReqMemory:   Cardinal; // EGI: maximum chunk size (uncompressed)
  ehMaxRegions:  Word;     // EGI: max. number of regions in a CHK_REGION chunk
  ehTranspNum:   Word;     // EGI: number of transparent levels
  ehReserved2:   array [0..23] of Byte; // Set to zero
  ehOFrame1:     Cardinal;     // Offset to frame 1 (FLC only)
  ehOFrame2:     Cardinal;     // Offset to frame 2 (FLC only)
  ehReserved3:   array [0..39] of Byte; // Set to zero
 end;

implementation

end.
