unit gba;

interface

uses SysUtils, Classes, MyClasses, Compression;

const
 (* LZ77 constants *)
 RING_SHIFT = 12;
 RING_SIZE = 1 shl RING_SHIFT;
 RING_MAX = RING_SIZE - 1;

 MATCH_LIMIT = 18;
 THRESHOLD = 2;
 LZ77_SIGN = $10;
 US_LZ_READFLAGS = US_CUSTOM;
 US_LZ_COPY = US_CUSTOM + 1;
 US_LZ_DECOMPRESS = US_CUSTOM + 2;
 US_LZ_SUBDECOMPR = US_CUSTOM + 3;
 (* Huffman constants *)
 HFMN_4BIT = $24;
 HFMN_8BIT = $28;
 US_HF_TREESTEP = US_CUSTOM;
 US_HF_READBITS = US_CUSTOM + 1;
 US_HF_PROCEED = US_CUSTOM + 2;
 US_HF_WRITE4BIT = US_CUSTOM + 3;
 US_HF_WRITE8BIT = US_CUSTOM + 4;
 (* RLE constants *)
 RLE_SIGN = $30;
 MAX_CHAIN = 127 + 3;
 US_RL_READFLAGS = US_CUSTOM;
 US_RL_COPY = US_CUSTOM + 1;
 US_RL_READFILLER = US_CUSTOM + 2;
 US_RL_FILL = US_CUSTOM + 3;

type
 (* Global types *)
 TUInt24 = packed record
  A: Byte;
  B: Byte;
  C: Byte;
 end;

 TGBA_ComprHeader = packed record
  gchSignature: Byte; //$10 - LZ77; $24, $28 - Huffman; $30 - RLE
  gchSize: TUInt24;
 end;

 TCustomGBAUnpackStream = class(TCustomUnpackStream)
  protected
    FHeader: TGBA_ComprHeader;
    function CheckHeader: Boolean; virtual; abstract;
    function ReadHeader: Integer; override;
 end;

 (* LZ77 compression utils *)

 TLZ77CmprMode = (lzNormal, lzVRAM_Safe);

 TLZ77CmprData = packed record
  FlagsPtr: PByte;
  RingPos: Integer;
  WindowSize: Integer;
  SearchPos: Integer;
  LastRingPos: Integer;
  FlagIndex: ShortInt;
  SkipFirstByte: Boolean;
  SearchData: WordRec;
  FlagsPtrOffset: Integer;
 end;

 TLZ77_PackStream = class(TCustomPackStreamML)
  private
    FComprData: TLZ77CmprData;
    FVRAM_Safe: Integer;
    FRingBuf: array [0..RING_MAX] of Byte;
    FLinks: array[0..RING_MAX] of SmallInt;
    FLastLetter: array[Byte] of SmallInt;
  protected
    function WriteHeader: Integer; override;
    procedure CompressInit;
    function FillMatchLength: Boolean; override;
    function SearchState: Boolean; override;
    function WriteState: Boolean; override;
  public
    constructor Create(Dest: TStream; Mode: TLZ77CmprMode = lzNormal);
    procedure Reset; override;
 end;

 TLZ77DcmprData = packed record
  Flags: Byte;
  RingPos: Integer;
  LastCycle: Integer;
  case Integer of
   US_LZ_DECOMPRESS: (SkipFlagsRead: Boolean;
                      LastFlags: Word);
   US_LZ_SUBDECOMPR: (LastSubCycle: Integer;
                      LastLength: Integer;
                      LastOffset: Integer);
 end;

 TLZ77_UnpackStream = class(TCustomGBAUnpackStream)
  private
    FDecomprData: TLZ77DcmprData;
    FRingBuf: array[0..RING_MAX] of Byte;
  protected
    function DecompressStep: Boolean; override;
    procedure SetPos(Offset: Integer); override;    
    function CheckHeader: Boolean; override;
  public
    procedure Reset; override;
 end;

 (* Huffman compression utils *)

 THfmnCmprMode = (hf4bit, hf8bit);
 THfmnCmprModeEx = (hfe4bit, hfe8bit, hfeAutoDetect);

 THuffmanPackStream = class(TCustomPackStream)
  private
    FMode: THfmnCmprModeEx;
    FFreqList4: array of Cardinal;
    FFreqList8: array of Cardinal;
    FSignature: Byte;
  protected
    function Compress: Integer; override;
    procedure FinishCompression; override;
    function WriteHeader: Integer; override;
  public
    constructor Create(Dest: TStream; Mode: THfmnCmprModeEx = hfeAutoDetect);
    procedure Reset; override;
 end;

 THfmnDcmprData = packed record
  CurrentNode: Byte;
  WriteByte: Byte;
  FirstHalf: Boolean;
  Data: Cardinal;
  Mask: Cardinal;
  TreePosition: Integer;
  BitsRead: Integer;
 end;

 THuffmanUnpackStream = class(TCustomGBAUnpackStream)
  private
    FDecomprData: THfmnDcmprData;
    FTree: array of Byte;
  protected
    function CheckHeader: Boolean; override;
    function DecompressStep: Boolean; override;
  public
    procedure Reset; override;
 end;

 (* RLE compression utils *)

 TRLE_CmprData = packed record
  FlagPtr: PByte;
  FlagPtrOffset: Integer;
  DefaultFlag: Byte;
 end;

 TRLE_PackStream = class(TCustomPackStreamML)
  private
    FComprData: TRLE_CmprData;
  protected
    function WriteHeader: Integer; override;
    procedure CompressInit;
    function SearchState: Boolean; override;
  public
    constructor Create(Dest: TStream);
    procedure Reset; override;
 end;

 TRLE_DcmprData = packed record
  RemainDecompress: Integer;
  FillByte: Byte;
 end;

 TRLE_UnpackStream = class(TCustomGBAUnpackStream)
  private
    FDecomprData: TRLE_DcmprData;
  protected
    function CheckHeader: Boolean; override;
    function DecompressStep: Boolean; override;
  public
    procedure Reset; override;
 end;

 (* LZH compression utils *)

 TLZH_PackStream = class(TLZ77_PackStream)
  public
    constructor Create(Dest: TStream;
      LZMode: TLZ77CmprMode = lzNormal;
      HfmnMode: THfmnCmprModeEx = hfeAutoDetect);
    destructor Destroy; override;
 end;

 TLZH_UnpackStream = class(TLZ77_UnpackStream)
  private
    FSource: TStream;
  public
    procedure Reset; override;
    destructor Destroy; override;
 end;

function GBA_DetectCompression(Stream: TStream): TCustomUnpackStreamClass; 
(* LZ77 compression utils *)
function GBA_LZ77_Check(const Source): Integer;
function GBA_LZ77_Compress(const Source; var Dest;
                SrcLen: Integer; Mode: TLZ77CmprMode): Integer;
function GBA_LZ77_Decompress(const Source; var Dest): Integer;
(* Huffman compression utils *)
function GBA_HuffmanCheck(const Source): Integer;
function GBA_HuffmanCompress(const Source; var Dest; SrcLen: Integer;
                             Mode: THfmnCmprMode): Integer;
function GBA_HuffmanDecompress(const Source; var Dest): Integer;
(* RLE compression utils *)
function GBA_RLE_Check(const Source): Integer;
function GBA_RLE_Compress(const Source; var Dest; SrcLen: Integer): Integer;
function GBA_RLE_Decompress(const Source; var Dest): Integer;

Function GetR(V: Word): Byte;
Function GetG(V: Word): Byte;
Function GetB(V: Word): Byte;
Function Color2GBA(Color: Cardinal): Word;
Function GBA2Color(Color: Word): Cardinal;
Procedure GetGBApalette(Var Source, Dest; Count: Integer);
Procedure SetGBApalette(Var Source, Dest; Count: Integer);

const
 SHeaderIsInvalid: PChar = 'Header is invalid';
 SUnableToCreateTree: PChar = 'Unable to create GBA-compatible Huffman tree';

implementation

Procedure GetGBApalette(Var Source, Dest; Count: Integer);
Var SP: PWord; DP: PCardinal;
begin
 SP := Addr(Source);
 DP := Addr(Dest);
 While Count > 0 do
 begin
  DP^ := GBA2Color(SP^);
  Inc(DP); Inc(SP);
  Dec(Count);
 end;
end;

Procedure SetGBApalette(Var Source, Dest; Count: Integer);
Var SP: PCardinal; DP: PWord; 
begin
 SP := Addr(Source);
 DP := Addr(Dest);
 While Count > 0 do
 begin
  DP^ := Color2GBA(SP^);
  Inc(DP); Inc(SP);
  Dec(Count);
 end;
end;

Function GetR(V: Word): Byte;
begin
 Result := (V and $1F) shl 3;
end;

Function GetG(V: Word): Byte;
begin
 Result := ((V and $3FF) shr 5) shl 3;
end;

Function GetB(V: Word): Byte;
begin
 Result := ((V and $7FFF) shr 10) shl 3;
end;

Function Color2GBA(Color: Cardinal): Word;
Type
 TBGRZ = Packed Record R, G, B, Z: Byte end;
begin
 With TBGRZ(Color) do Result := (B shr 3) shl 10 + (G shr 3) shl 5 + R shr 3;
end;

Function GBA2Color(Color: Word): Cardinal;
begin
 Result := GetB(Color) shl 16 + GetG(Color) shl 8 + GetR(Color);
end;

function GBA_DetectCompression(Stream: TStream): TCustomUnpackStreamClass;
var
 Header: TGBA_ComprHeader;
 SavePos: Int64;
begin
 Result := NIL;
 try
  SavePos := Stream.Position;
  if Stream.Read(Header, SizeOf(Header)) = SizeOf(Header) then
  case Header.gchSignature of
   LZ77_SIGN: Result := TLZ77_UnpackStream;
   RLE_SIGN: Result := TRLE_UnpackStream;
   HFMN_4BIT,
   HFMN_8BIT:
   begin
    Stream.Seek(-SizeOf(Header), soFromCurrent);
    with THuffmanUnpackStream.Create(Stream) do
    try

     if (Read(Header, SizeOf(Header)) = SizeOf(Header)) and
        (Header.gchSignature = LZ77_SIGN) then
      Result := TLZH_UnpackStream else
      Result := THuffmanUnpackStream;
    finally
     Free;
    end;
   end;
  end;
  Stream.Position := SavePos;
 except
  //do nothing
 end;
end;

(* LZ77 functions *)

function GBA_LZ77_Check(const Source): Integer;
var
 Data: TGBA_ComprHeader absolute Source;
begin
 if (@Source <> NIL) and (Data.gchSignature = LZ77_SIGN) then
  Result := Integer(Data) shr 8 else
  Result := -1;
end;

function GBA_LZ77_Compress(const Source; var Dest;
          SrcLen: Integer; Mode: TLZ77CmprMode): Integer;
var
 I, J, LastRingPos: Integer;
 Dst, Src: PByte;
 DstD: PCardinal absolute Dst;
 FlagIndex: Integer;
 FlagsPtr: PByte;
 WindowSize, VSafe, RingPos: Integer;
 MatchLength: Integer;
 SearchPos: Integer;
 SrcSave: PByte;
 W: WordRec;
 RingBuf: array[0..RING_MAX] of Byte;
 Links: array[0..RING_MAX] of SmallInt;
 LastLetter: array[Byte] of SmallInt;
begin
 Src := @Source;
 Dst := @Dest;
 VSafe := Integer(Mode = lzVRAM_Safe) and 1;
 Inc(SrcLen, SrcLen and VSafe);
 if (Src <> NIL) and (Dst <> NIL) and (SrcLen >= 0) and (SrcLen <= $FFFFFF) then
 begin
  DstD^ := LZ77_SIGN or (Cardinal(SrcLen) shl 8);
  Inc(DstD);
  RingPos := 0;
  WindowSize := 0;
  Result := 4;
  LastRingPos := 0;
  FlagIndex := 7;
  for I := 0 to RING_MAX do Links[I] := I;
  FillChar(LastLetter, SizeOf(LastLetter), $FF);
  while SrcLen > 0 do
  begin
   if WindowSize = 0 then MatchLength := 1 else
   begin
    if SrcLen < MATCH_LIMIT then
     MatchLength := SrcLen else
     MatchLength := MATCH_LIMIT;
   end;
   SearchPos := (RingPos - WindowSize) - 1;
   SrcSave := Src;
   Inc(SrcSave, LastRingPos);
   for I := LastRingPos to MatchLength - 1 do
   begin
    RingBuf[(RingPos + I) and RING_MAX] := SrcSave^;
    Inc(SrcSave);
   end;
   asm
        push    ebx
        mov     ebx,[MatchLength]
        mov     [LastRingPos],ebx
        xor     eax,eax
        mov     [MatchLength],eax
        cmp     ebx,THRESHOLD
        jle     @@Finish
        mov     ecx,[WindowSize]
        sub     ecx,[VSafe]
        cmp     ecx,0
        jle     @@Finish
        mov     eax,[Src]
        mov     bh,byte ptr [eax]
        movzx   edx,bh
        push    esi
        push    edi
        mov     eax,[SearchPos]
        add     eax,ecx
        and     eax,RING_MAX
        lea     edi,[RingBuf + eax]
        mov     dx,word ptr [LastLetter + edx * 2]
        cmp     dx,$FFFF
        je      @@ExitLoop 
        cmp     edx,eax
        je      @@Skip
        mov     esi,eax
        sub     esi,edx
        and     esi,RING_MAX
        mov     edi,ecx
        sub     edi,esi
        cmp     edi,0
        jle     @@SearchPreLoop
   @@Search:
        mov     ecx,edi
        mov     eax,edx
        lea     edi,[RingBuf + eax]
        jmp     @@Skip
        @@SearchPreLoop:
                lea     edi,[RingBuf + eax]
                cmp     byte ptr [edi],bh
                je      @@Skip
                dec     eax
                and     eax,RING_MAX
        loop    @@SearchPreLoop
        pop     edi
        pop     esi
        jmp     @@Finish
        @@SearchLoop:
                lea     edi,[RingBuf + eax]
                cmp     byte ptr [edi],bh
                jne     @@Continue
                mov     word ptr [Links + edx * 2],ax
        @@Skip:
                movzx   edx,bl
                push    ecx
                mov     ecx,eax
                add     ecx,edx
                dec     ecx
                and     ecx,RING_MAX
                mov     esi,[Src]
                cmp     ecx,eax
                jb      @@DoubleCompare
                mov     ecx,edx
                repe    cmpsb
                je      @@Found
        @@NotFound:
                sub     edx,ecx
                dec     edx
                cmp     edx,[MatchLength]
                jle     @@Proceed
                mov     [MatchLength],edx
                mov     [SearchPos],eax
         @@Proceed:
                mov     edx,eax
                pop     ecx
                movzx   edi,word ptr [Links + eax * 2]
                cmp     edi,eax
                jne     @@Boost
        @@Continue:
                dec     eax
                and     eax,RING_MAX
        loop    @@SearchLoop
        jmp     @@ExitLoop
        @@Boost:
                mov     esi,eax
                sub     esi,edi
                and     esi,RING_MAX
                sub     ecx,esi
                cmp     ecx,0
                jle     @@ExitLoop
                inc     ecx
                mov     eax,edi
                lea     edi,[RingBuf + eax]
        loop    @@Skip
   @@ExitLoop:
        pop     edi
        pop     esi
        jmp     @@Finish
        @@NotFound2:
                mov     edx,[Src]
                sub     esi,edx
                dec     esi
                cmp     esi,[MatchLength]
                jle     @@Proceed
                mov     [MatchLength],esi
                mov     [SearchPos],eax
        jmp     @@Proceed
        @@DoubleCompare:
                push    ecx
                mov     ecx,RING_SIZE
                sub     ecx,eax
                repe    cmpsb
                pop     ecx
                jne     @@NotFound2
                inc     ecx
                lea     edi,[RingBuf]
                repe    cmpsb
                jne     @@NotFound
   @@Found:
        pop     ecx
        mov     [SearchPos],eax
        movzx   ebx,bl
        mov     [MatchLength],ebx
        pop     edi
        pop     esi
    @@Finish:
        pop     ebx
   end;
   if FlagIndex = 7 then
   begin
    FlagsPtr := Dst;
    FlagsPtr^ := 0;
    Inc(Dst);
    Inc(Result);
   end;
   SrcSave := Src;
   if MatchLength > THRESHOLD then
   begin
    Word(W) := ((MatchLength - (THRESHOLD + 1)) shl 12) or
         ((RingPos - SearchPos - 1) and RING_MAX);
    Dst^ := W.Hi;
    Inc(Dst);
    Dst^ := W.Lo;
    Inc(Dst);
    Inc(Result, 2);
    FlagsPtr^ := FlagsPtr^ or (1 shl FlagIndex);
    Inc(Src, MatchLength);
    Dec(SrcLen, MatchLength);
    Dec(FlagIndex);
   end else
   begin
    MatchLength := 1;
    Dst^ := Src^;
    Inc(Dst);
    Inc(Result);
    Inc(Src);
    Dec(SrcLen);
    Dec(FlagIndex);
   end;
   for I := 0 to MatchLength - 1 do
   begin
    J := (RingPos + I) and RING_MAX;
    if LastLetter[SrcSave^] > 0 then
     Links[J] := LastLetter[SrcSave^] else
     Links[J] := J;
    LastLetter[SrcSave^] := J;
    Inc(SrcSave);
   end;
   RingPos := (RingPos + MatchLength) and RING_MAX;
   Dec(LastRingPos, MatchLength);
   if FlagIndex < 0 then FlagIndex := 7;
   if WindowSize < RING_SIZE - MATCH_LIMIT then
   begin
    Inc(WindowSize, MatchLength);
    if WindowSize > RING_SIZE - MATCH_LIMIT then
     WindowSize := RING_SIZE - MATCH_LIMIT;
   end;
  end;
  if Result and 3 > 0 then Result := Result or 3 + 1;
 end else Result := 0;
end;

function GBA_LZ77_Decompress(const Source; var Dest): Integer;
var
 Src, Dst: PByte;
 I, Lng, Offset: Integer;
 D: Byte; Data: Word;
 UncompSize: Integer;
begin
 Src := @Source;
 Dst := @Dest;
 Result := 0;
 if (Src = NIL) or (Dst = NIL) or (Src^ <> LZ77_SIGN) then Exit;
 Result := Integer(Pointer(Src)^) shr 8;
 UncompSize := Result;
 Inc(Src, 4);
 while UncompSize > 0 do
 begin
  D := Src^;
  Inc(Src);
  if D <> 0 then
  begin
   for I := 0 to 7 do
   begin
    if D and $80 <> 0 then
    begin
     Data := Src^ shl 8;
     Inc(Src);
     Data := Data or Src^;
     Inc(Src);
     Lng := (Data shr 12) + 3;
     Offset := Data and $FFF;
     Dec(UncompSize, Lng);
     if UncompSize < 0 then Inc(Lng, UncompSize);
     Move(Pointer(Integer(Dst) - Offset - 1)^, Dst^, Lng);
     Inc(Dst, Lng);
     if UncompSize <= 0 then Exit;
    end else
    begin
     Dst^ := Src^;
     Inc(Dst);
     Inc(Src);
     Dec(UncompSize);
     If UncompSize = 0 then Exit;
    end;
    D := D shl 1;
   end;
  end else
  begin
   Dec(UncompSize, 8);
   if UncompSize < 0 then
    Lng := 8 + UncompSize else
    Lng := 8;
   Move(Src^, Dst^, Lng);
   Inc(Src, 8);
   Inc(Dst, 8);
  end;
 end;
end;

(* Local Huffman functions *)

function FillTree(var Dest; Node: PHuffTreeNode): Boolean;
var
 Tree: TByteArray absolute Dest;
 Offset, Index: Integer;
begin
 if Node <> NIL then with Node^ do
 begin
  if Position < 0 then
   Index := 0 else
   Index := 1 + Position * 2 + Integer(Direction = diRight);
  if (Left <> NIL) and (Right <> NIL) then
  begin
   Offset := (Left.Position - 1) - Position;
   if Offset > $3F then
   begin
    Result := False;
    Exit;
   end;
   Tree[Index] := Offset or (Byte(Left.Value >= 0) shl 7) or
                        (Byte(Right.Value >= 0) shl 6);
   Result := FillTree(Tree, Left) and FillTree(Tree, Right);
  end else
  begin
   Tree[Index] := Value;
   Result := True;
  end;
 end else Result := False;
end;

(* Global Huffman functions *)

function GBA_HuffmanCheck(const Source): Integer;
var
 Data: TGBA_ComprHeader absolute Source;
begin
 if (@Source <> NIL) and (Data.gchSignature in [HFMN_4BIT, HFMN_8BIT]) then
  Result := Integer(Data) shr 8 else
  Result := -1;
end;

function GBA_HuffmanCompress(const Source; var Dest; SrcLen: Integer;
                             Mode: THfmnCmprMode): Integer;
var
 Tree: THuffmanTree;
 Freqs: array of Cardinal;
 Src, Dst: PByte;
 Data: PCardinal absolute Dst;
 I, TreeSize: Integer;
 DataMask: Cardinal;
procedure WriteBitStream(Value: Byte);
var
 Mask: Cardinal;
begin
 with Tree.BitStreams[Value] do
 begin
  Mask := 1 shl (Size - 1);
  while Mask > 0 do
  begin
   if Bits and Mask <> 0 then
    Data^ := Data^ or DataMask;
   Mask := Mask shr 1;
   DataMask := DataMask shr 1;
   if DataMask = 0 then
   begin
    Inc(Data);
    Data^ := 0;
    DataMask := $80000000;
   end;
  end;
 end;
end;
begin
 Src := @Source;
 Dst := @Dest;
 Result := 0;
 if (Src <> NIL) and (Dst <> NIL) and (SrcLen >= 0) and (SrcLen <= $FFFFFF) then
 begin
  Data^ := (HFMN_4BIT + Cardinal(Mode = hf8bit) * 4) or (Cardinal(SrcLen) shl 8);
  Inc(Data);
  if SrcLen = 0 then
  begin
   Result := 4;
   Exit;
  end;
  case Mode of
   hf4bit:
   begin
    SetLength(Freqs, 16);
    FillChar(Freqs[0], 16 * SizeOf(Cardinal), 0);
    for I := 0 to SrcLen - 1 do
    begin
     Inc(Freqs[Src^ and 15]);
     Inc(Freqs[Src^ shr 4]);
     Inc(Src);
    end;
   end;
   hf8bit:
   begin
    SetLength(Freqs, 256);
    FillChar(Freqs[0], 256 * SizeOf(Cardinal), 0);
    for I := 0 to SrcLen - 1 do
    begin
     Inc(Freqs[Src^]);
     Inc(Src);
    end;
   end;
   else Exit;
  end;
  Tree := THuffmanTree.Create(Freqs);
  try
   TreeSize := Length(Tree.Branches) * 2 + 2;
   if TreeSize and 3 > 0 then TreeSize := TreeSize or 3 + 1;
   I := (TreeSize shr 1) - 1;
   if I in [0..255] then
   begin
    Dst^ := I;
    Inc(Dst);
    Dec(TreeSize);
    FillChar(Dst^, TreeSize, 0);
    if FillTree(Dst^, Tree.Trunk) then
    begin
     Inc(Dst, TreeSize);
     DataMask := $80000000;
     Data^ := 0;
     Src := @Source;
     case Mode of
      hf4bit: for I := 0 to SrcLen - 1 do
      begin
       WriteBitStream(Src^ and 15);
       WriteBitStream(Src^ shr 4);
       Inc(Src);
      end;
      hf8bit: for I := 0 to SrcLen - 1 do
      begin
       WriteBitStream(Src^);
       Inc(Src);
      end;
     end;
     if DataMask > 0 then Inc(Data);
     Result := Integer(Dst) - Integer(@Dest);
    end;
   end;
  finally
   Tree.Free;
  end;
 end;
end;

function GBA_HuffmanDecompress(const Source; var Dest): Integer;
var
 Src, Dst: PByte;
 Data: PCardinal absolute Src;
 Tree: PByteArray;
 CurrentNode, WriteByte: Byte;
 TreeSize, TreePos: Integer;
 UncompSize: Integer;
 Mask: Cardinal;
 WriteData, FirstHalf: Boolean;
 Mode: THfmnCmprMode;
begin
 Src := @Source;
 Dst := @Dest;
 Result := 0;
 if (Src = NIL) or (Dst = NIL) then Exit;
 if Src^ = HFMN_4BIT then Mode := hf4bit else
 if Src^ = HFMN_8BIT then Mode := hf8bit else Exit;
 Result := Integer(Pointer(Src)^) shr 8;
 if Result = 0 then Exit;
 UncompSize := Result;
 Inc(Src, 4);
 TreeSize := (Src^ + 1) shl 1;
 Tree := Pointer(Cardinal(Src) + 1);
 Inc(Src, TreeSize);
 CurrentNode := Tree[0];
 TreePos := 0;
 FirstHalf := True;
 Mask := $80000000;
 WriteByte := 0;
 while UncompSize > 0 do
 begin
  if TreePos = 0 then
   Inc(TreePos) else
   Inc(TreePos, (((Integer(CurrentNode) and $3F) + 1) shl 1));
  if Data^ and Mask = 0 then
  begin
   WriteData := CurrentNode and $80 <> 0;
   CurrentNode := Tree[TreePos];
  end else
  begin
   WriteData := CurrentNode and $40 <> 0;
   CurrentNode := Tree[TreePos + 1];
  end;
  Mask := Mask shr 1;
  if Mask = 0 then
  begin
   Mask := $80000000;
   Inc(Data);
  end;
  if WriteData then
  begin
   case Mode of
    hf4bit: if FirstHalf then
    begin
     WriteByte := CurrentNode;
     FirstHalf := False;
    end else
    begin
     Dst^ := WriteByte or (CurrentNode shl 4);
     Inc(Dst);
     Dec(UncompSize);
     FirstHalf := True;
    end;
    hf8bit:
    begin
     Dst^ := CurrentNode;
     Inc(Dst);
     Dec(UncompSize);
    end;
   end;
   TreePos := 0;
   CurrentNode := Tree[0];
  end;
 end;
end;

(* RLE functions *)

function GBA_RLE_Check(const Source): Integer;
var
 Data: TGBA_ComprHeader absolute Source;
begin
 if (@Source <> NIL) and (Data.gchSignature = RLE_SIGN) then
  Result := Integer(Data) shr 8 else
  Result := -1;
end;

function GBA_RLE_Compress(const Source; var Dest; SrcLen: Integer): Integer;
var
 Src, Dst, FlagPtr: PByte; B: Byte;
 Data: PCardinal absolute Dst;
begin
 Src := @Source;
 Dst := @Dest;
 if (Src <> NIL) and (Dst <> NIL) and (SrcLen >= 0) and (SrcLen <= $FFFFFF) then
 begin
  Data^ := RLE_SIGN or (Cardinal(SrcLen) shl 8);
  Inc(Data);
  if SrcLen > 0 then
  begin
   B := 255;
   FlagPtr := @B;
   asm
        push    edi
   @@Loop:
        mov     eax,dword ptr [SrcLen]
        mov     ecx,MAX_CHAIN
        cmp     ecx,eax
        jle     @@Lower
        mov     ecx,eax
    @@Lower:
        mov     edx,ecx
        dec     ecx
        mov     edi,[Src]
        mov     al,byte ptr [edi]
        inc     edi
        repe    scasb
        je      @@SkipCalc
        inc     ecx
        sub     edx,ecx
    @@SkipCalc:
        cmp     edx,THRESHOLD
        ja      @@SetChain
        inc     dword ptr [Src]
        dec     dword ptr [SrcLen]
        mov     edi,[FlagPtr]
        cmp     byte ptr [edi],127
        jb      @@Proceed
        mov     edi,[Dst]
        inc     dword ptr [Dst]
        mov     [FlagPtr],edi
        mov     byte ptr [edi],255
    @@Proceed:
        inc     byte ptr [edi]
        mov     edi,[Dst]
        mov     [edi],al
        inc     dword ptr [Dst]
        jmp     @@Continue
    @@SetChain:
        add     dword ptr [Src],edx
        sub     dword ptr [SrcLen],edx
        sub     edx,3
        or      dl,128
        mov     edi,[Dst]
        add     dword ptr [Dst],2
        mov     byte ptr [edi],dl
        mov     byte ptr [edi + 1],al
        lea     edi,[B]
        mov     [FlagPtr],edi
    @@Continue:
        cmp     dword ptr [SrcLen],0
        jg      @@Loop
        pop     edi
   end;
  end;
  Result := Integer(Dst) - Integer(@Dest);
 end else Result := 0;
end;

function GBA_RLE_Decompress(const Source; var Dest): Integer;
var
 Src, Dst: PByte;
 Lng: Integer;
 D: Byte;
 UncompSize: Integer;
begin
 Src := @Source;
 Dst := @Dest;
 if (Src <> NIL) and (Dst <> NIL) and (Src^ = RLE_SIGN) then
 begin
  Result := Integer(Pointer(Src)^) shr 8;
  UncompSize := Result;
  Inc(Src, 4);
  while UncompSize > 0 do
  begin
   D := Src^;
   Inc(Src);
   Lng := D and $7F;
   if D and $80 = 0 then
   begin
    Inc(Lng);
    Dec(UncompSize, Lng);
    if UncompSize < 0 then
     Inc(Lng, UncompSize);
    Move(Src^, Dst^, Lng);
    Inc(Src, Lng);
    Inc(Dst, Lng);
   end else
   begin
    Inc(Lng, 3);
    Dec(UncompSize, Lng);
    if UncompSize < 0 then
     Inc(Lng, UncompSize);
    FillChar(Dst^, Lng, Src^);
    Inc(Src);
    Inc(Dst, Lng);
   end;
  end;
 end else Result := 0;
end;

(* TCustomGBAUnpackStream *)

function TCustomGBAUnpackStream.ReadHeader: Integer;
begin
 Result := FStream.Read(FHeader, SizeOf(TGBA_ComprHeader));
 if (Result <> SizeOf(TGBA_ComprHeader)) or not CheckHeader then
  Result := -1 else
  FOutputSize := Integer(FHeader) shr 8;
end;

(* TLZ77_PackStream *)

procedure TLZ77_PackStream.CompressInit;
begin
 with FComprData do
 begin
  if FlagsPtrOffset >= 0 then
  begin
   FlagsPtr := FBufStream.Memory;
   Inc(FlagsPtr, FlagsPtrOffset);
  end;
 end;
end;

constructor TLZ77_PackStream.Create(Dest: TStream; Mode: TLZ77CmprMode);
begin
 FVRAM_Safe := Integer(Mode = lzVRAM_Safe) and 1;
 inherited Create(Dest, MATCH_LIMIT, $FFFFFF);
end;

function TLZ77_PackStream.FillMatchLength: Boolean;
begin
 Result := (FComprData.WindowSize = 0) and (FRemainIn >= 1);
 if Result then FMatchLength := 1;
end;

procedure TLZ77_PackStream.Reset;
var
 I: Integer;
begin
 inherited;
 FillChar(FComprData, SizeOf(TLZ77CmprData), 0);
 FComprData.FlagIndex := 7;
 FComprData.FlagsPtrOffset := -1;
 for I := 0 to RING_MAX do FLinks[I] := I;
 FillChar(FLastLetter, SizeOf(FLastLetter), $FF);
end;

function TLZ77_PackStream.SearchState: Boolean;
var
 PS, ML, WS, I: Integer;
 Src: PByte;
begin
 with FComprData do
 begin
  Src := FSrcPtr;
  Inc(Src, LastRingPos);
  for I := LastRingPos to FMatchLength - 1 do
  begin
   FRingBuf[(RingPos + I) and RING_MAX] := Src^;
   Inc(Src);
  end;
  LastRingPos := FMatchLength;
  PS := (RingPos - WindowSize) - 1;
  ML := FMatchLength;
  WS := WindowSize - FVRAM_Safe;
  Src := FSrcPtr;
  asm
        push    ebx
        mov     ebx,[ML]
        xor     eax,eax
        mov     [ML],eax
        cmp     ebx,THRESHOLD
        jle     @@Finish
        mov     ecx,[WS]
        cmp     ecx,0
        jle     @@Finish
        mov     eax,[Src]
        mov     bh,byte ptr [eax]
        movzx   edx,bh
        push    esi
        push    edi
        mov     eax,[PS]
        add     eax,ecx
        and     eax,RING_MAX
        mov     esi,[Self]
        lea     edi,[esi + offset TLZ77_PackStream.FRingBuf + eax]
        mov     dx,word ptr [esi + offset TLZ77_PackStream.FLastLetter + edx * 2]
        cmp     dx,$FFFF
        je      @@ExitLoop
        cmp     edx,eax
        je      @@Skip
        mov     esi,eax
        sub     esi,edx
        and     esi,RING_MAX
        mov     edi,ecx
        sub     edi,esi
        cmp     edi,0
        jle     @@SearchInit
        mov     ecx,edi
        mov     eax,edx
        mov     esi,[Self]
        lea     edi,[esi + offset TLZ77_PackStream.FRingBuf + eax]
        jmp     @@Skip
   @@SearchInit:
        mov     esi,[Self]
        @@SearchPreLoop:
                lea     edi,[esi + offset TLZ77_PackStream.FRingBuf + eax]
                cmp     byte ptr [edi],bh
                je      @@Skip
                dec     eax
                and     eax,RING_MAX
        loop    @@SearchPreLoop
        pop     edi
        pop     esi
        jmp     @@Finish
        @@SearchLoop:
                mov     esi,[Self]
                lea     edi,[esi + offset TLZ77_PackStream.FRingBuf + eax]
                cmp     byte ptr [edi],bh
                jne     @@Continue
                mov     word ptr [esi + offset TLZ77_PackStream.FLinks + edx * 2],ax
        @@Skip:
                movzx   edx,bl
                push    ecx
                mov     ecx,eax
                add     ecx,edx
                dec     ecx
                and     ecx,RING_MAX
                mov     esi,[Src]
                cmp     ecx,eax
                jb      @@DoubleCompare
                mov     ecx,edx
                repe    cmpsb
                je      @@Found
        @@NotFound:
                sub     edx,ecx
                dec     edx
                cmp     edx,[ML]
                jle     @@Proceed
                mov     [ML],edx
                mov     [PS],eax
         @@Proceed:
                mov     edx,eax
                pop     ecx
                mov     edi,[Self]
                movzx   edi,word ptr [edi + offset TLZ77_PackStream.FLinks + eax * 2]
                cmp     edi,eax
                jne     @@Boost
        @@Continue:
                dec     eax
                and     eax,RING_MAX
        loop    @@SearchLoop
        jmp     @@ExitLoop
        @@Boost:
                mov     esi,eax
                sub     esi,edi
                and     esi,RING_MAX
                sub     ecx,esi
                cmp     ecx,0
                jle     @@ExitLoop
                inc     ecx
                mov     eax,edi
                mov     edi,[Self]
                lea     edi,[edi + offset TLZ77_PackStream.FRingBuf + eax]
        loop    @@Skip
   @@ExitLoop:
        pop     edi
        pop     esi
        jmp     @@Finish
        @@NotFound2:
                mov     edx,[Src]
                sub     esi,edx
                dec     esi
                cmp     esi,[ML]
                jle     @@Proceed
                mov     [ML],esi
                mov     [PS],eax
        jmp     @@Proceed
        @@DoubleCompare:
                push    ecx
                mov     ecx,RING_SIZE
                sub     ecx,eax
                repe    cmpsb
                pop     ecx
                jne     @@NotFound2
                inc     ecx
                mov     edi,[Self]
                lea     edi,[edi + offset TLZ77_PackStream.FRingBuf]
                repe    cmpsb
                jne     @@NotFound
   @@Found:
        pop     ecx
        mov     [PS],eax
        movzx   ebx,bl
        mov     [ML],ebx
        pop     edi
        pop     esi
    @@Finish:
        pop     ebx
  end;
  FMatchLength := ML;
  SearchPos := PS;
  if FlagIndex = 7 then
  begin
   FlagsPtrOffset := FTotalOut;
   FlagsPtr := FOutput;
   FlagsPtr^ := 0;
   Inc(FOutput);
   Inc(FTotalOut);
   Dec(FRemainOut);
  end;
 end;
 FState := PS_ML_WRITE;
 Result := True;
end;

function TLZ77_PackStream.WriteHeader: Integer;
var
 Header: Cardinal;
begin
 Header := LZ77_SIGN or (Cardinal(FTotalIn) shl 8);
 Result := FStream.Write(Header, 4);
end;

function TLZ77_PackStream.WriteState: Boolean;
var
 Src: PByte;
 I, L: Integer;
begin
 Result := True;
 with FComprData do
 begin
  Src := FSrcPtr;
  if FMatchLength > THRESHOLD then
  begin
   if not SkipFirstByte then
   begin
    Word(SearchData) := ((FMatchLength - (THRESHOLD + 1)) shl 12) or
            ((RingPos - SearchPos - 1) and RING_MAX);
    FOutput^ := SearchData.Hi;
    Inc(FOutput);
    Inc(FTotalOut);
    Dec(FRemainOut);
    if FRemainOut = 0 then
    begin
     SkipFirstByte := True;
     Exit;
    end;
   end else SkipFirstByte := False;
   FOutput^ := SearchData.Lo;
   Inc(FOutput);
   Inc(FTotalOut);
   Dec(FRemainOut);
   FlagsPtr^ := FlagsPtr^ or (1 shl FlagIndex);
   Inc(FSrcPtr, FMatchLength);
   Dec(FRemainInsert, FMatchLength);
   Dec(FlagIndex);
  end else
  begin
   FMatchLength := 1;
   FOutput^ := FSrcPtr^;
   Inc(FSrcPtr);
   Inc(FOutput);
   Inc(FTotalOut);
   Dec(FRemainOut);
   Dec(FRemainInsert);
   Dec(FlagIndex);
  end;
  for I := 0 to FMatchLength - 1 do
  begin
   L := (RingPos + I) and RING_MAX;
   if FLastLetter[Src^] > 0 then
    FLinks[L] := FLastLetter[Src^] else
    FLinks[L] := L;
   FLastLetter[Src^] := L;
   Inc(Src);
  end;
  RingPos := (RingPos + FMatchLength) and RING_MAX;
  Dec(LastRingPos, FMatchLength);
  if FlagIndex < 0 then FlagIndex := 7;
  if WindowSize < RING_SIZE - MATCH_LIMIT then
  begin
   Inc(WindowSize, FMatchLength);
   if WindowSize > RING_SIZE - MATCH_LIMIT then
    WindowSize := RING_SIZE - MATCH_LIMIT;
  end;
 end;
 FState := PS_ML_INIT;
end;

(* TLZ77_UnpackStream *)

function TLZ77_UnpackStream.CheckHeader: Boolean;
begin
 Result := FHeader.gchSignature = LZ77_SIGN;
end;

function TLZ77_UnpackStream.DecompressStep: Boolean;
var
 I, J, L, Ofs: Integer;
 B: Byte; Data: Word;
begin
 Result := False;
 with FDecomprData do
 begin
  case FState of
   US_LZ_READFLAGS: (* reading flags byte*)
   begin
    if FRemainIn <= 0 then Exit;
    Flags := FInput^;
    Inc(FInput);
    Inc(FTotalIn);
    Dec(FRemainIn);
    if Flags = 0 then
    (* if all 8 bits are zero then we just copy 8 bytes*)
     FState := US_LZ_COPY else
    (* else decompress *)
     FState := US_LZ_DECOMPRESS;
    LastCycle := 0;
   end;
   US_LZ_COPY: (* simple data copy *)
   begin
    (* setting default next state *)
    FState := US_LZ_READFLAGS;
    (* starting from cycle when previous pass had stopped *)
    for I := LastCycle to 7 do
    begin
     B := FInput^;
     FOutput^ := B;
     FRingBuf[RingPos] := B;
     RingPos := (RingPos + 1) and RING_MAX;
     Inc(FInput);
     Inc(FOutput);
     Inc(FTotalIn);
     Inc(FTotalOut);
     Dec(FRemainIn);
     Dec(FTotalRemain);
     Dec(FRemainOut);
     if FTotalRemain = 0 then
     begin (* if all bytes readen then stop *)
      FState := US_FINISHED;
      Exit;
     end;
     if (FRemainOut = 0) or (FRemainIn = 0) then // if the specified count of
     begin                                       // bytes readen or we reached
      if I < 7 then                              // the end of input buffer then
      begin                                      // save next cycle index if it
       LastCycle := I + 1;                       // is necessary and stop
       FState := US_LZ_COPY;
      end;
      Exit;
     end;
    end;
   end;
   US_LZ_DECOMPRESS: (* decompression *)
   begin
    if FRemainIn <= 0 then Exit;
    (* setting default next state *)
    FState := US_LZ_READFLAGS;
    (* starting from cycle when previous pass had stopped *)
    for I := LastCycle to 7 do
    begin
     (* if current bit is zero then just copy one byte *)
     if Flags and $80 = 0 then
     begin
      B := FInput^;
      FOutput^ := B;
      FRingBuf[RingPos] := B;
      RingPos := (RingPos + 1) and RING_MAX;
      Inc(FInput);
      Inc(FOutput);
      Inc(FTotalIn);
      Inc(FTotalOut);
      Dec(FRemainIn);
      Dec(FTotalRemain);
      Dec(FRemainOut);
     end else
     begin
      if SkipFlagsRead then    // if the first byte of length/offset flags
      begin                    // was readen in previous process then just
       Data := LastFlags;      // load it in variable
       SkipFlagsRead := False;
      end else                 // otherwise read first byte of
      begin                    // length/offset from input buffer
       Data := FInput^ shl 8;
       Inc(FInput);
       Inc(FTotalIn);
       Dec(FRemainIn);
       if FRemainIn = 0 then   // if we reached the end of input buffer
       begin                   // then save current cycle index,
        LastCycle := I;        // save readen data and stop
        SkipFlagsRead := True;
        LastFlags := Data;
        FState := US_LZ_DECOMPRESS;
        Exit;
       end;
      end;
      Data := Data or FInput^; // read next length/offset flags byte
      Inc(FInput);
      Inc(FTotalIn);
      Dec(FRemainIn);
      L := (Data shr 12) + THRESHOLD; // extracting length from flag bits
      Ofs := Data and RING_MAX; // extracting data offset in ring buffer
      (* copy from ring buffer *)
      for J := 0 to L do
      begin
       B := FRingBuf[(RingPos - Ofs - 1) and RING_MAX];
       FOutput^ := B;
       FRingBuf[RingPos] := B;
       RingPos := (RingPos + 1) and RING_MAX;
       Inc(FOutput);
       Inc(FTotalOut);
       Dec(FTotalRemain);
       Dec(FRemainOut);
       (* if all bytes readen then stop *)
       if FTotalRemain = 0 then Break;
       if FRemainOut = 0 then
       begin                     // if the specified count of bytes readen
        if J < L then            // then save next cycle index of copy from
        begin                    // ring buffer loop, save count of bytes to
         LastSubCycle := J + 1;  // copy and ring offset
         LastLength := L;
         LastOffset := Ofs;
         FState := US_LZ_SUBDECOMPR;
        end;
        Break;
       end;
      end;
     end;
     Flags := Flags shl 1; // setting cursor to the next flags bit
     if FTotalRemain = 0 then
     begin (* if all bytes readen then stop *)
      FState := US_FINISHED;
      Exit;
     end;
     if (FRemainOut = 0) or (FRemainIn = 0) then // the same as for dsSimpleCopy
     begin
      if I < 7 then
      begin
       LastCycle := I + 1;
       if FState = US_LZ_READFLAGS then
        FState := US_LZ_DECOMPRESS;
      end;
      Exit;
     end;
    end;
   end;
   US_LZ_SUBDECOMPR: (* copy from ring buffer loop *)
   begin
    (* setting default next state *)
    if LastCycle > 0 then
     FState := US_LZ_DECOMPRESS else
     FState := US_LZ_READFLAGS;
    Ofs := LastOffset;
    (* starting from the cycle when previous pass had stopped *)
    for I := LastSubCycle to LastLength do
    begin
     B := FRingBuf[(RingPos - Ofs - 1) and RING_MAX];
     FOutput^ := B;
     FRingBuf[RingPos] := B;
     RingPos := (RingPos + 1) and RING_MAX;
     Inc(FOutput);
     Inc(FTotalOut);
     Dec(FTotalRemain);
     Dec(FRemainOut);
     if FTotalRemain = 0 then     // same as before
     begin
      FState := US_FINISHED;
      Exit;
     end;
     if FRemainOut = 0 then // same as before
     begin
      if I < LastLength then
      begin
       LastSubCycle := I + 1;
       FState := US_LZ_SUBDECOMPR;
      end;
      Exit;
     end;
    end;
    if FState <> US_LZ_SUBDECOMPR then // if next state is dsReadFlagsByte
    begin                         // then clear dsSubDecompress info
     LastSubCycle := 0;
     LastLength := 0;
     LastOffset := 0;
    end;
   end;
   else Exit; (* decompression finished *)
  end;
 end;
 Result := True;
end;

procedure TLZ77_UnpackStream.Reset;
begin
 inherited;
 FillChar(FDecomprData, SizeOf(TLZ77DcmprData), 0);
 if FTotalRemain = 0 then FState := US_FINISHED else
 begin
  FDecomprData.RingPos := 0;
  FState := US_LZ_READFLAGS;
  FillChar(FRingBuf, SizeOf(FRingBuf), $E5);
 end;
end;

procedure TLZ77_UnpackStream.SetPos(Offset: Integer);
var
 Remain, L: Integer;
begin
 Remain := Offset - FTotalOut;
 with FDecomprData do
 begin
  L := RING_SIZE - RingPos;
  while Remain > 0 do
  begin
   if L > Remain then L := Remain;
   ReadBuffer(FRingBuf[RingPos], L);
   RingPos := (RingPos + L) and RING_MAX;
   Dec(Remain, L);
   L := RING_SIZE;
  end;
 end;
end;

(* THuffmanPackStream *)

function THuffmanPackStream.Compress: Integer;
var
 I: Integer;
begin
 Result := FRemainIn;
 Dec(FRemainOut, Result);
 if FRemainOut < 0 then
 begin
  Inc(Result, FRemainOut);
  FRemainOut := 0;
 end;
 Move(FInput^, FOutput^, Result);
 case FMode of
  hfe4bit: for I := 0 to Result - 1 do
  begin
   Inc(FFreqList4[FInput^ and 15]);
   Inc(FFreqList4[FInput^ shr 4]);
   Inc(FInput);
  end;
  hfe8bit: for I := 0 to Result - 1 do
  begin
   Inc(FFreqList8[FInput^]);
   Inc(FInput);
  end;
  else for I := 0 to Result - 1 do
  begin
   Inc(FFreqList4[FInput^ and 15]);
   Inc(FFreqList4[FInput^ shr 4]);
   Inc(FFreqList8[FInput^]);
   Inc(FInput);
  end;
 end;
 Inc(FOutput, Result);
 Inc(FTotalIn, Result);
 Dec(FRemainIn, Result);
end;

constructor THuffmanPackStream.Create(Dest: TStream; Mode: THfmnCmprModeEx);
begin
 FMode := Mode;
 case Mode of
  hfe4bit: SetLength(FFreqList4, 16);
  hfe8bit: SetLength(FFreqList8, 256);
  else
  begin
   FMode := hfeAutoDetect;
   SetLength(FFreqList4, 16);
   SetLength(FFreqList8, 256);
  end;
 end;
 inherited Create(Dest, $FFFFFF);
end;

procedure THuffmanPackStream.FinishCompression;
var
 Dst: PByte;
 Data: PCardinal absolute Dst;
 DataMask: Cardinal;
 Buffer: TMemoryStream; 
procedure WriteBitStream(Bits: Cardinal; Size: Integer);
var
 Mask: Cardinal;
begin
 Mask := 1 shl (Size - 1);
 while Mask > 0 do
 begin
  if Bits and Mask <> 0 then
   Data^ := Data^ or DataMask;
  Mask := Mask shr 1;
  DataMask := DataMask shr 1;
  if DataMask = 0 then
  begin
   Inc(Data);
   Data^ := 0;
   DataMask := $80000000;
  end;
 end;
end;
procedure WriteHuffman(Tree: THuffmanTree; TreeSize, BufSize: Integer);
var
 I: Integer;
 Src: PByte;
 BinTree: array of Byte;
begin
 I := (TreeSize shr 1) - 1;
 if I in [0..255] then
 begin
  SetLength(BinTree, TreeSize);
  FillChar(BinTree[0], TreeSize, 0);
  BinTree[0] := I;
  if FillTree(BinTree[1], Tree.Trunk) then
  begin
   Buffer.Size := BufSize;
   Dst := Buffer.Memory;
   Move(BinTree[0], Dst^, TreeSize);
   Inc(Dst, TreeSize);
   DataMask := $80000000;
   Data^ := 0;
   Src := FBufStream.Memory;
   if FSignature = HFMN_4BIT then for I := 0 to FTotalIn - 1 do
   begin
    with Tree.BitStreams[Src^ and 15] do WriteBitStream(Bits, Size);
    with Tree.BitStreams[Src^ shr 04] do WriteBitStream(Bits, Size);
    Inc(Src);
   end else for I := 0 to FTotalIn - 1 do
   begin
    with Tree.BitStreams[Src^] do WriteBitStream(Bits, Size);
    Inc(Src);
   end;
   Exit;
  end;
 end;
 raise ECompressionError.Create(SUnableToCreateTree);
end;
var
 Tree4, Tree8: THuffmanTree;
 Tree: THuffmanTree absolute Tree4;
 TreeSize4, TreeSize8, SZ4, SZ8: Integer;
 SZ: Integer absolute SZ4;
 TreeSize: Integer absolute TreeSize4;
begin
 if FTotalIn <= 0 then Exit;
 Buffer := TMemoryStream.Create;
 try
  case FMode of
   hfe4bit, hfe8bit:
   begin
    if FMode = hfe4bit then
    begin
     FSignature := HFMN_4BIT;
     Tree := THuffmanTree.Create(FFreqList4);
    end else
    begin
     FSignature := HFMN_8BIT;
     Tree := THuffmanTree.Create(FFreqList8);
    end;
    try
     TreeSize := Length(Tree.Branches) * 2 + 2;
     if TreeSize and 3 > 0 then TreeSize := TreeSize or 3 + 1;
     SZ := Tree.TotalBitsCount shr 3 + TreeSize +
           Integer(Tree.TotalBitsCount and 7 > 0);
     if SZ and 3 > 0 then SZ := SZ or 3 + 1;
     WriteHuffman(Tree, TreeSize, SZ);
    finally
     Tree4.Free;
    end;
   end;
   else
   begin
    Tree4 := THuffmanTree.Create(FFreqList4);
    try
     Tree8 := THuffmanTree.Create(FFreqList8);
     try
      TreeSize4 := Length(Tree4.Branches) * 2 + 2;
      TreeSize8 := Length(Tree8.Branches) * 2 + 2;
      if TreeSize4 and 3 > 0 then TreeSize4 := TreeSize4 or 3 + 1;
      if TreeSize8 and 3 > 0 then TreeSize8 := TreeSize8 or 3 + 1;
      SZ4 := Tree4.TotalBitsCount shr 3 + TreeSize4 +
             Integer(Tree4.TotalBitsCount and 7 > 0);
      if SZ4 and 3 > 0 then SZ4 := SZ4 or 3 + 1;
      SZ8 := Tree8.TotalBitsCount shr 3 + TreeSize8 +
             Integer(Tree8.TotalBitsCount and 7 > 0);
      if SZ8 and 3 > 0 then SZ8 := SZ8 or 3 + 1;
      if (SZ8 < SZ4) and ((TreeSize8 shr 1) - 1 in [0..255]) then
      try
       FSignature := HFMN_8BIT;
       WriteHuffman(Tree8, TreeSize8, SZ8);
      except
       FSignature := HFMN_4BIT;
       WriteHuffman(Tree4, TreeSize4, SZ4);
      end else
      begin
       FSignature := HFMN_4BIT;
       WriteHuffman(Tree4, TreeSize4, SZ4);
      end;
     finally
      Tree8.Free;
     end;
    finally
     Tree4.Free;
    end;
   end;
  end;
  FBufStream.Clear;
  FTotalOut := Buffer.Size;
  if FTotalOut > 0 then FBufStream.Write(Buffer.Memory^, FTotalOut);
 finally
  Buffer.Free;
 end;
end;

procedure THuffmanPackStream.Reset;
begin
 inherited;
 FillChar(FFreqList4[0], Length(FFreqList4) * 4, 0);
 FillChar(FFreqList8[0], Length(FFreqList8) * 4, 0);
 FState := PS_CUSTOM;
end;

function THuffmanPackStream.WriteHeader: Integer;
var
 Header: Cardinal;
begin
 Header := FSignature or (Cardinal(FTotalIn) shl 8);
 Result := FStream.Write(Header, 4);
end;

(* THuffmanUnpackStream *)

function THuffmanUnpackStream.CheckHeader: Boolean;
begin
 Result := FHeader.gchSignature in [HFMN_4BIT, HFMN_8BIT];
end;

function THuffmanUnpackStream.DecompressStep: Boolean;
var
 WriteData: Boolean;
begin
 Result := False;
 with FDecomprData do
 begin
  case FState of
   US_HF_TREESTEP:
   begin
    if Mask = 0 then
    begin
     Mask := $80000000;
     BitsRead := 0;
     Data := 0;
     FState := US_HF_READBITS;
    end else FState := US_HF_PROCEED;
    if TreePosition = 0 then
    begin
     CurrentNode := FTree[0];
     Inc(TreePosition);
    end else Inc(TreePosition, (((Integer(CurrentNode) and $3F) + 1) shl 1));
   end;
   US_HF_READBITS:
   begin
    if FRemainIn <= 0 then Exit;
    while (BitsRead < 32) and (FRemainIn > 0) do
    begin
     Data := Data or (FInput^ shl BitsRead);
     Inc(BitsRead, 8);
     Inc(FInput);
     Inc(FTotalIn);
     Dec(FRemainIn);
    end;
    if BitsRead = 32 then FState := US_HF_PROCEED;
   end;
   US_HF_PROCEED:
   begin
    if Data and Mask = 0 then
    begin
     WriteData := CurrentNode and $80 <> 0;
     CurrentNode := FTree[TreePosition];
    end else
    begin
     WriteData := CurrentNode and $40 <> 0;
     CurrentNode := FTree[TreePosition + 1];
    end;
    Mask := Mask shr 1;
    if WriteData then
    begin
     case FHeader.gchSignature of
      HFMN_4BIT: FState := US_HF_WRITE4BIT;
      HFMN_8BIT: FState := US_HF_WRITE8BIT;
      else
      begin
       FState := US_ERROR;
       Exit;
      end;
     end;
     TreePosition := 0;
    end else FState := US_HF_TREESTEP;
   end;
   US_HF_WRITE4BIT:
   begin
    if FirstHalf then
    begin
     WriteByte := CurrentNode;
     FirstHalf := False;
    end else
    begin
     FOutput^ := WriteByte or (CurrentNode shl 4);
     Inc(FOutput);
     Inc(FTotalOut);
     Dec(FTotalRemain);
     Dec(FRemainOut);
     FirstHalf := True;
    end;
    FState := US_HF_TREESTEP;
   end;
   US_HF_WRITE8BIT:
   begin
    FOutput^ := CurrentNode;
    Inc(FOutput);
    Inc(FTotalOut);
    Dec(FTotalRemain);
    Dec(FRemainOut);
    FState := US_HF_TREESTEP;
   end;
   else Exit; (* decompression finished or error *)
  end;
 end;
 Result := True;
end;

procedure THuffmanUnpackStream.Reset;
var
 TreeSize: Integer;
begin
 inherited;
 FillChar(FDecomprData, SizeOf(FDecomprData), 0);
 if FTotalRemain = 0 then FState := US_FINISHED else
 begin
  FStream.ReadBuffer(TreeSize, 1);
  TreeSize := (TreeSize and $FF) shl 1 + 1;
  SetLength(FTree, TreeSize);
  FStream.ReadBuffer(FTree[0], TreeSize);
  Inc(FStreamPosition, TreeSize + 1);
  Inc(FTotalIn, TreeSize + 1);
  FState := US_HF_TREESTEP;
  FDecomprData.FirstHalf := True;
 end;
end;

(* TRLE_PackStream *)

procedure TRLE_PackStream.CompressInit;
begin
 with FComprData do
 begin
  if FlagPtrOffset >= 0 then
  begin
   FlagPtr := FBufStream.Memory;
   Inc(FlagPtr, FlagPtrOffset);
  end;
 end;
end;

constructor TRLE_PackStream.Create(Dest: TStream);
begin
 inherited Create(Dest, MAX_CHAIN, $FFFFFF);
end;

procedure TRLE_PackStream.Reset;
begin
 inherited;
 FillChar(FComprData, SizeOf(FComprData), 0);
 with FComprData do
 begin
  DefaultFlag := 255;
  FlagPtr := @DefaultFlag;
  FlagPtrOffset := -1;
 end;
end;

function TRLE_PackStream.SearchState: Boolean;
var
 ML, SrcLen: Integer;
 Src, Dst, FP: PByte;
begin
 with FComprData do
 begin
  if FRemainOut <= 1 then
  begin
   ML := FBufStream.Size;
   FBufStream.Size := ML + PackBufSize;
   FOutput := FBufStream.Memory;
   Inc(FOutput, ML - FRemainOut);
   FRemainOut := PackBufSize + FRemainOut;
   CompressInit;
  end;
  Src := FSrcPtr;
  SrcLen := FRemainInsert;
  FP := FlagPtr;
  Dst := FOutput;
  asm
        push    edi
        mov     ecx,[SrcLen]
        mov     edx,ecx
        dec     ecx
        mov     edi,[Src]
        mov     al,byte ptr [edi]
        inc     edi
        repe    scasb
        je      @@SkipCalc
        inc     ecx
        sub     edx,ecx
      @@SkipCalc:
        cmp     edx,THRESHOLD
        ja      @@SetChain
        inc     dword ptr [Src]
        dec     dword ptr [SrcLen]
        mov     edi,[FP]
        cmp     byte ptr [edi],127
        jb      @@Proceed
        mov     edi,[Dst]
        inc     dword ptr [Dst]
        mov     [FP],edi
        mov     byte ptr [edi],255
      @@Proceed:
        inc     byte ptr [edi]
        mov     edi,[Dst]
        mov     [edi],al
        inc     dword ptr [Dst]
        jmp     @@Continue
      @@SetChain:
        add     dword ptr [Src],edx
        sub     dword ptr [SrcLen],edx
        sub     edx,3
        or      dl,128
        mov     edi,[Dst]
        add     dword ptr [Dst],2
        mov     byte ptr [edi],dl
        mov     byte ptr [edi + 1],al
        xor     eax,eax
        mov     [FP],eax
      @@Continue:
       pop      edi
  end;
  if FP = FOutput then FlagPtrOffset := FTotalOut else
  if FP = NIL then
  begin
   FP := @DefaultFlag;
   FlagPtrOffset := -1;
  end;
  ML := Integer(Dst) - Integer(FOutput);
  Inc(FTotalOut, ML);
  Dec(FRemainOut, ML);
  FOutput := Dst;
  FlagPtr := FP;
  FSrcPtr := Src;
  FRemainInsert := SrcLen;
 end;
 FState := PS_ML_INIT;
 Result := FRemainIn > 0;
end;

function TRLE_PackStream.WriteHeader: Integer;
var
 Header: Cardinal;
begin
 Header := RLE_SIGN or (Cardinal(FTotalIn) shl 8);
 Result := FStream.Write(Header, 4);
end;

(* TRLE_UnpackStream *)

function TRLE_UnpackStream.CheckHeader: Boolean;
begin
 Result := FHeader.gchSignature = RLE_SIGN;
end;

function TRLE_UnpackStream.DecompressStep: Boolean;
var
 L: Integer;
 B: Byte;
begin
 Result := False;
 with FDecomprData do
 begin
  case FState of
   US_RL_READFLAGS: (* reading flags byte*)
   begin
    if FRemainIn <= 0 then Exit;
    B := FInput^;
    Inc(FInput);
    Inc(FTotalIn);
    Dec(FRemainIn);
    RemainDecompress := (B and $7F) + 1;
    if B and $80 = 0 then FState := US_RL_COPY else
    begin
     Inc(RemainDecompress, 2);
     FState := US_RL_READFILLER;
    end;
   end;
   US_RL_COPY:
   begin
    if FRemainIn <= 0 then Exit;
    if FRemainOut > RemainDecompress then
     L := RemainDecompress else
     L := FRemainOut;
    if L > FRemainIn then L := FRemainIn;
    if L > FTotalRemain then L := FTotalRemain;
    Move(FInput^, FOutput^, L);
    Dec(RemainDecompress, L);
    Inc(FInput, L);
    Inc(FOutput, L);
    Inc(FTotalIn, L);
    Inc(FTotalOut, L);
    Dec(FRemainIn, L);
    Dec(FRemainOut, L);
    Dec(FTotalRemain, L);
    if FTotalRemain = 0 then
    begin
     FState := US_FINISHED;
     Exit;
    end;
    if RemainDecompress = 0 then FState := US_RL_READFLAGS;
   end;
   US_RL_READFILLER:
   begin
    if FRemainIn <= 0 then Exit;
    FillByte := FInput^;
    Inc(FInput);
    Inc(FTotalIn);
    Dec(FRemainIn);
    FState := US_RL_FILL;
   end;
   US_RL_FILL:
   begin
    if FRemainOut > RemainDecompress then
     L := RemainDecompress else
     L := FRemainOut;
    if L > FTotalRemain then L := FTotalRemain;
    FillChar(FOutput^, L, FillByte);
    Dec(RemainDecompress, L);
    Inc(FOutput, L);
    Inc(FTotalOut, L);
    Dec(FRemainOut, L);
    Dec(FTotalRemain, L);
    if FTotalRemain = 0 then
    begin
     FState := US_FINISHED;
     Exit;
    end;
    if RemainDecompress = 0 then FState := US_RL_READFLAGS;
   end;
   else Exit; (* decompression finished or error *)
  end;
 end;
 Result := True;
end;

procedure TRLE_UnpackStream.Reset;
begin
 inherited;
 FillChar(FDecomprData, SizeOf(FDecomprData), 0);
 if FTotalRemain = 0 then
  FState := US_FINISHED else
  FState := US_RL_READFLAGS;
end;

(* TLZH_PackStream *)

constructor TLZH_PackStream.Create(Dest: TStream; LZMode: TLZ77CmprMode;
  HfmnMode: THfmnCmprModeEx);
begin
 inherited Create(THuffmanPackStream.Create(Dest, HfmnMode), LZMode);
end;

destructor TLZH_PackStream.Destroy;
begin
 inherited;
 if (FState = PS_ERROR) and (FStream is TCustomPackStream) then
  TCustomPackStream(FStream).State := PS_ERROR;
 FStream.Free;
end;

(* TLZH_UnpackStream *)

destructor TLZH_UnpackStream.Destroy;
begin
 FSource.Free;
 inherited;
end;

procedure TLZH_UnpackStream.Reset;
begin
 if FSource = nil then
 begin
  FSource := THuffmanUnpackStream.Create(FSource);
  SetStream(FSource);
 end;
 inherited;
end;

end.
