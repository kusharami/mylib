unit JavaClass;

interface

uses SysUtils, Classes, Windows, HexUnit, MyUtils;

type
 PConstantPoolInfo = ^TConstantPoolInfo;
 TConstantPoolInfo = packed record
  cpTag: Byte;
  cpInfo: array of Byte;
 end;

 TAttributeInfo = packed record
  aiAttributeNameIndex: Word;
  aiAttributeLength: Integer;
  aiInfo: array of Byte;
 end;

 TAttributeArray = array of TAttributeInfo;

 TFieldInfo = packed record
  fiAccessFlags: Word;
  fiNameIndex: Word;
  fiDescriptorIndex: Word;
  fiAttributesCount: Word;
  fiAttributes: TAttributeArray;
 end;

 TJavaClassStructure = packed record
  jcMagic: Cardinal; //CAFEBABE
  jcMinorVersion: Word;
  jcMajorVersion: Word;
  jcConstantPoolCount: Word;
  jcConstantPool: array of TConstantPoolInfo;
  jcAccessFlags: Word;
  jcThisClass: Word;
  jcSuperClass: Word;
  jcInterfacesCount: Word;
  jcInterfaces: array of Word;
  jcFieldsCount: Word;
  jcFields: array of TFieldInfo;
  jcMethodsCount: Word;
  jcMethods: array of TFieldInfo;
  jcAttributesCount: Word;
  jcAttributes: TAttributeArray;
 end;

 TJavaException = packed record
  jeStartPC: Word;
  jeEndPC: Word;
  jeHandlerPC: Word;
  jeCatchType: Word;
 end;

 TExceptionsAttribute = packed record
 	eaCount: Word;
  eaTable: array of Word;
 end;

 TInnerClassInfo = packed record
  icInnerInfoIndex: Word;
  icOuterInfoIndex: Word;
  icNameIndex: Word;
  icAccessFlags: Word;
 end;

 TInnerClassesAttribute = packed record
  icCount: Word;
  icClasses: array of TInnerClassInfo;
 end;

 TLineNumberTableAttribute = packed record
  ltCount: Word;
  ltTable: array of packed record
            StartPC: Word;
            LineNumber: Word
           end;
 end;

 TLocalVariableTableAttribute = packed record
  lvCount: Word;
  lvTable: array of packed record
            StartPC: Word;
    	      Len: Word;
    	      NameIndex: Word;
    	      DescriptorIndex: Word;
    	      Index: Word;
           end;
 end;

 TCodeAttribute = packed record
  caMaxStack: Word;
  caMaxLocals: Word;
  caCodeLength: Integer;
  caCode: array of Byte;
  caExceptionsCount: Word;
  caExceptions: array of TJavaException;
  caAttributesCount: Word;
  caAttributes: TAttributeArray;
 end;

 TMBStringInfo = packed record
  msLen: Word;
  msText: AnsiString;
 end;

 TJavaStringSearchRec = packed record
  Index: Word;
  PoolInfo: PConstantPoolInfo;
  PoolIndex: Word;
 end;

 TJavaClass = class
  private
    FMaxCharSize: Cardinal;
    FCodePage: Cardinal;
    FFlags: Cardinal;
    FMainData: TJavaClassStructure;
    function GetString(PoolIndex: Integer): AnsiString;
    function GetWideString(PoolIndex: Integer): WideString;
    procedure SetString(PoolIndex: Integer; const Value: AnsiString);
    procedure SetWideString(PoolIndex: Integer; const Value: WideString);
    function GetStringConst(PoolIndex: Integer): AnsiString;
    function GetWideStringConst(PoolIndex: Integer): WideString;
    procedure SetStringConst(PoolIndex: Integer; const Value: AnsiString);
    procedure SetWideStringConst(PoolIndex: Integer; const Value: WideString);
    function GetConstantsCount: Integer;
    procedure SetCodePage(Value: Cardinal);
  protected
    property MainData: TJavaClassStructure
        read FMainData
       write FMainData;
    function StringEncode(const S: WideString): AnsiString; virtual;
    function StringDecode(const S: AnsiString): WideString; virtual;
  public
    property CodePage: Cardinal
        read FCodePage
       write SetCodePage;

    property Flags: Cardinal
        read FFlags
       write FFlags;

    property ConstantsCount: Integer
        read GetConstantsCount;

    property Strings[PoolIndex: Integer]: AnsiString
        read GetString
       write SetString;

    property WideStrings[PoolIndex: Integer]: WideString
        read GetWideString
       write SetWideString;

    property ConstStrings[PoolIndex: Integer]: AnsiString
        read GetStringConst
       write SetStringConst;

    property ConstWideString[PoolIndex: Integer]: WideString
        read GetWideStringConst
       write SetWideStringConst;

    constructor Create;
    destructor Destroy; override;
    procedure Clear; virtual;
    function FindFirstString(var SR: TJavaStringSearchRec): Boolean;
    function FindNextString(var SR: TJavaStringSearchRec): Boolean;
    procedure LoadFromStream(const Stream: TStream); virtual;
    procedure SaveToStream(const Stream: TStream); virtual;
    procedure LoadFromFile(const FileName: AnsiString);
    procedure SaveToFile(const FileName: AnsiString);
 end;

 TJavaClassEUC = class(TJavaClass)
  protected
    function StringEncode(const S: WideString): AnsiString; override;
    function StringDecode(const S: AnsiString): WideString; override;
 end;

 EJavaClassException = class(Exception)
  private
    FErrorCode: Integer;
  public
    property ErrorCode: Integer
        read FErrorCode
       write FErrorCode;

    constructor Create(const Msg: String; ErrorCode: Integer);
 end;

const
 CONSTANT_Class = 7;
 CONSTANT_Fieldref = 9;
 CONSTANT_Methodref = 10;
 CONSTANT_InterfaceMethodref = 11;
 CONSTANT_String = 8;
 CONSTANT_Integer = 3;
 CONSTANT_Float = 4;
 CONSTANT_Long = 5;
 CONSTANT_Double = 6;
 CONSTANT_NameAndType = 12;
 CONSTANT_Utf8 = 1;
 //*******Java Class Flags*****//
 JC_NULL_STR = 1; //Strings with null-terminator

implementation

//------ TJavaClass ------//

procedure TJavaClass.Clear;
 procedure FinalizeAttributes(var List: TAttributeArray);
 var
  I: Integer;
  S: AnsiString;
 begin
  for I := 0 to Length(List) - 1 do with List[I] do
  begin
   S := UpperCase(Strings[SwapWord(aiAttributeNameIndex)]);
   if S = 'CODE' then with TCodeAttribute(Pointer(aiInfo)^) do
   begin
    Finalize(caCode);
    Finalize(caExceptions);
    FinalizeAttributes(caAttributes);
   end else if S = 'EXCEPTIONS' then
   with TExceptionsAttribute(Pointer(aiInfo)^) do Finalize(eaTable) else
   if S = 'INNERCLASSES' then
   with TInnerClassesAttribute(Pointer(aiInfo)^) do Finalize(icClasses) else
   if S = 'LINENUMBERTABLE' then
   with TLineNumberTableAttribute(Pointer(aiInfo)^) do Finalize(ltTable) else
   if S = 'LOCALVARIABLETABLE' then
   with TLocalVariableTableAttribute(Pointer(aiInfo)^) do Finalize(lvTable);
   Finalize(aiInfo);
  end;
  Finalize(List);
 end; //FinalizeAttributes
(* TJavaClass.Clear *)
var
 I: Integer;
begin
 with FMainData do
 begin
  FinalizeAttributes(jcAttributes);
  for I := 0 to Length(jcMethods) - 1 do
   FinalizeAttributes(jcMethods[I].fiAttributes);
  for I := 0 to Length(jcFields) - 1 do
   FinalizeAttributes(jcFields[I].fiAttributes);
  for I := 0 to Length(jcConstantPool) - 1 do with jcConstantPool[I] do
  begin
   if (cpTag = CONSTANT_Utf8) and (Length(cpInfo) >= SizeOf(TMBStringInfo)) then
    Finalize(TMBStringInfo(Pointer(cpInfo)^).msText);
   Finalize(cpInfo);
  end;
  Finalize(jcConstantPool);
  Finalize(jcInterfaces);
  Finalize(jcFields);
  Finalize(jcMethods);
  FillChar(FMainData, SizeOf(TJavaClassStructure), 0);
  jcMagic := $BEBAFECA;
 end;
end;

constructor TJavaClass.Create;
begin
 FillChar(FMainData, SizeOf(TJavaClassStructure), 0);
 FMainData.jcMagic := $BEBAFECA;
 FFlags := 0;
 CodePage := CP_UTF8;
end;

destructor TJavaClass.Destroy;
begin
 Clear;
 inherited;
end;

function TJavaClass.FindFirstString(var SR: TJavaStringSearchRec): Boolean;
var
 P: PConstantPoolInfo;
 I: Integer;
begin
 FillChar(SR, SizeOf(SR), 0);
 with FMainData do
 begin
  P := Pointer(jcConstantPool);
  for I := 0 to Length(jcConstantPool) - 1 do
  begin
   with P^ do if cpTag = CONSTANT_String then
   begin
    SR.Index := I;
    SR.PoolInfo := P;
    SR.PoolIndex := SwapWord(Word(Pointer(cpInfo)^));
    Result := True;
    Exit;
   end;
   Inc(P);
  end;
 end;
 Result := False;
end;

function TJavaClass.FindNextString(var SR: TJavaStringSearchRec): Boolean;
var
 P: PConstantPoolInfo;
 I, J: Integer;
begin
 with FMainData do
 begin
  J := SR.Index + 1;
  P := SR.PoolInfo; Inc(P);
  for I := J to Length(jcConstantPool) - 1 do
  begin
   with P^ do if cpTag = CONSTANT_String then
   begin
    SR.Index := I;
    SR.PoolInfo := P;
    SR.PoolIndex := SwapWord(Word(Pointer(cpInfo)^));
    Result := True;
    Exit;
   end;
   Inc(P);
  end;
 end;
 Result := False;
end;

function TJavaClass.GetConstantsCount: Integer;
begin
 Result := SwapWord(FMainData.jcConstantPoolCount) - 1;
end;

function TJavaClass.GetString(PoolIndex: Integer): AnsiString;
var
 Cnt: Integer;
begin
 Result := '';
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_Utf8 then
  begin
   if FFlags and JC_NULL_STR = 0 then
    Result := TMBStringInfo(Pointer(cpInfo)^).msText else
    Result := PChar(Pointer(TMBStringInfo(Pointer(cpInfo)^).msText));
  end;
 end;
end;

function TJavaClass.GetStringConst(PoolIndex: Integer): AnsiString;
var
 Cnt: Integer;
begin
 Result := '';
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_String then
  Result := Strings[SwapWord(Word(Pointer(cpInfo)^))];
 end;
end;

function TJavaClass.GetWideString(PoolIndex: Integer): WideString;
var
 Cnt: Integer;
begin
 Result := '';
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_Utf8 then
  begin
   if FFlags and JC_NULL_STR = 0 then
    Result := StringDecode(TMBStringInfo(Pointer(cpInfo)^).msText) else
    Result := StringDecode(PChar(Pointer(TMBStringInfo(Pointer(cpInfo)^).msText)));
  end;
 end;
end;

function TJavaClass.GetWideStringConst(PoolIndex: Integer): WideString;
var
 Cnt: Integer;
begin
 Result := '';
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_String then
  Result := WideStrings[SwapWord(Word(Pointer(cpInfo)^))];
 end;
end;

procedure TJavaClass.LoadFromFile(const FileName: AnsiString);
var
 Stream: TFileStream;
begin
 Stream := TFileStream.Create(FileName, fmOpenRead);
 try
  LoadFromStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TJavaClass.LoadFromStream(const Stream: TStream);
var
 Temp: TJavaClassStructure;

 function LoadAttributes(Count: Integer; var Dest: TAttributeArray): Integer;
 var
  I, L: Integer;
  S: AnsiString;
 label
  LoadError;
 begin
  Result := 0;
  SetLength(Dest, Count);
  for I := 0 to Count - 1 do with Dest[I] do
  begin
   Stream.Read(aiAttributeNameIndex, 2);
   Stream.Read(aiAttributeLength, 4);
   try
    with Temp.jcConstantPool[SwapWord(aiAttributeNameIndex) - 1] do
    begin
     if cpTag <> 1 then
     begin
      LoadError:
      Result := -1;
      Exit;
     end;
     with TMBStringInfo(Pointer(cpInfo)^) do
     begin
      if (FFlags and JC_NULL_STR = 0) and (msText <> '') and
         (msText[Length(msText)] = #0) then
       FFlags := FFlags or JC_NULL_STR;  
      S := UpperCase(PChar(Pointer(msText)));
      if (S = 'CONSTANTVALUE') or (S = 'SOURCEFILE') then
      begin
       Inc(Result, 2);
       SetLength(aiInfo, 2);
       Stream.Read(aiInfo[0], 2);
      end else if S = 'CODE' then
      begin
       Inc(Result, SizeOf(TCodeAttribute) - 12);
       SetLength(aiInfo, SizeOf(TCodeAttribute));
       with TCodeAttribute(Pointer(aiInfo)^) do
       begin
        Stream.Read(caMaxStack, 2);
        Stream.Read(caMaxLocals, 2);
        Stream.Read(caCodeLength, 4);
        L := SwapInt(caCodeLength);
        SetLength(caCode, L);
        Stream.Read(caCode[0], L);
        Stream.Read(caExceptionsCount, 2);
        L := SwapWord(caExceptionsCount);
        SetLength(caExceptions, L);
        Stream.Read(caExceptions[0], L * SizeOf(TJavaException));
        Stream.Read(caAttributesCount, 2);
        L := LoadAttributes(SwapWord(caAttributesCount), caAttributes);
        if L >= 0 then Inc(Result, L) else Goto LoadError;
       end;
      end else if S = 'EXCEPTIONS' then
      begin
       Inc(Result, 2);
       SetLength(aiInfo, SizeOf(TExceptionsAttribute));
       with TExceptionsAttribute(Pointer(aiInfo)^) do
       begin
        Stream.Read(eaCount, 2);
        L := SwapWord(eaCount);
        SetLength(eaTable, L);
        Inc(Result, 2 + Stream.Read(eaTable[0], L shl 1));
       end;
      end else if S = 'INNERCLASSES' then
      begin
       Inc(Result, 2);
       SetLength(aiInfo, SizeOf(TInnerClassesAttribute));
       with TInnerClassesAttribute(Pointer(aiInfo)^) do
       begin
        Stream.Read(icCount, 2);
        L := SwapWord(icCount);
        SetLength(icClasses, L);
        Inc(Result, 2 + Stream.Read(icClasses[0], L * SizeOf(TInnerClassInfo)));
       end;
      end else if S = 'LINENUMBERTABLE' then
      begin
       Inc(Result, 2);
       SetLength(aiInfo, SizeOf(TLineNumberTableAttribute));
       with TLineNumberTableAttribute(Pointer(aiInfo)^) do
       begin
        Stream.Read(ltCount, 2);
        L := SwapWord(ltCount);
        SetLength(ltTable, L);
        Inc(Result, 2 + Stream.Read(ltTable[0], L shl 2));
       end;
      end else if S = 'LOCALVARIABLETABLE' then
      begin
       Inc(Result, 2);
       SetLength(aiInfo, SizeOf(TLocalVariableTableAttribute));
       with TLocalVariableTableAttribute(Pointer(aiInfo)^) do
       begin
        Stream.Read(lvCount, 2);
        L := SwapWord(lvCount);
        SetLength(lvTable, L);
        Inc(Result, 2 + Stream.Read(lvTable[0], L * 10));
       end;
      end else SetLength(aiInfo, 0);
     end;
    end;
   except
    Result := -1;
    Exit;
   end;
  end;
 end; //LoadAttributes
{TJavaClass.LoadFromStream}
var
 Error: Boolean;
 ErrorCode: Integer;
 I, L: Integer;
begin
 if Assigned(Stream) then
 begin
  ErrorCode := 1;
  FillChar(Temp, SizeOf(Temp), 0);
  with Temp do
  begin
   try
    Stream.Read(jcMagic, 4);
    if jcMagic = $BEBAFECA then
    begin
     Error := False;
     Stream.Read(jcMinorVersion, 2);
     Stream.Read(jcMajorVersion, 2);
     Stream.Read(jcConstantPoolCount, 2);
     L := SwapWord(jcConstantPoolCount) - 1;
     if L >= 0 then
     begin
      SetLength(jcConstantPool, L);
      for I := 0 to L - 1 do with jcConstantPool[I] do
      begin
       Stream.Read(cpTag, 1);
       Case cpTag of
        CONSTANT_Class,
        CONSTANT_String: SetLength(cpInfo, 2);
        CONSTANT_Fieldref,
        CONSTANT_Methodref,
        CONSTANT_InterfaceMethodref,
        CONSTANT_Integer,
        CONSTANT_Float,
        CONSTANT_NameAndType: SetLength(cpInfo, 4);
        CONSTANT_Long,
        CONSTANT_Double: SetLength(cpInfo, 8);
        CONSTANT_Utf8: SetLength(cpInfo, 6);
       end;
       if cpTag = CONSTANT_Utf8 then with TMBStringInfo(Pointer(cpInfo)^) do
       begin
        Stream.Read(msLen, 2);
        L := SwapWord(msLen);
        SetLength(msText, L);
        Stream.Read(msText[1], L);
       end else Stream.Read(cpInfo[0], Length(cpInfo));
      end;
     end;
     Stream.Read(jcAccessFlags, 2);
     Stream.Read(jcThisClass, 2);
     Stream.Read(jcSuperClass, 2);
     Stream.Read(jcInterfacesCount, 2);
     L := SwapWord(jcInterfacesCount);
     SetLength(jcInterfaces, L);
     Stream.Read(jcInterfaces[0], L shl 1);
     Stream.Read(jcFieldsCount, 2);
     L := SwapWord(jcFieldsCount);
     SetLength(jcFields, L);
     for I := 0 to L - 1 do with jcFields[I] do
     begin
      Stream.Read(fiAccessFlags, 2);
      Stream.Read(fiNameIndex, 2);
      Stream.Read(fiDescriptorIndex, 2);
      Stream.Read(fiAttributesCount, 2);
      Error := LoadAttributes(SwapWord(fiAttributesCount), fiAttributes) < 0;
      if Error then Break;
     end;
     Stream.Read(jcMethodsCount, 2);
     L := SwapWord(jcMethodsCount);
     SetLength(jcMethods, L);
     if not Error then
     for I := 0 to L - 1 do with jcMethods[I] do
     begin
      Stream.Read(fiAccessFlags, 2);
      Stream.Read(fiNameIndex, 2);
      Stream.Read(fiDescriptorIndex, 2);
      Stream.Read(fiAttributesCount, 2);
      Error := LoadAttributes(SwapWord(fiAttributesCount), fiAttributes) < 0;
      if Error then Break;
     end;
     Stream.Read(jcAttributesCount, 2);
     if not Error and
       (LoadAttributes(SwapWord(jcAttributesCount), jcAttributes) >= 0) then
     begin
      Clear;
      Move(Temp, FMainData, SizeOf(TJavaClassStructure));
      FillChar(Temp, SizeOf(TJavaClassStructure), 0);
      ErrorCode := 0;
     end;
    end;
   except
    ErrorCode := 2;
   end;
  end;
 end else ErrorCode := 666;
 if ErrorCode <> 0 then
 begin
  raise EJavaClassException.Create(Format('%s.LoadFromStream error #%d',
                  [ClassName, ErrorCode]), ErrorCode);
 end;
end;

function TJavaClass.StringDecode(const S: AnsiString): WideString;
begin
 Result := CodePageStringDecode(FCodePage, S);
end;

procedure TJavaClass.SaveToFile(const FileName: AnsiString);
var
 Stream: TFileStream;
begin
 Stream := TFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream);
 finally
  Stream.Free;
 end;
end;

procedure TJavaClass.SaveToStream(const Stream: TStream);
 procedure WriteAttributes(const List: TAttributeArray);
 var
  I: Integer;
  S: AnsiString;
 begin
  for I := 0 to Length(List) - 1 do with List[I] do
  begin
   Stream.Write(aiAttributeNameIndex, 2);
   Stream.Write(aiAttributeLength, 4);
   S := UpperCase(Strings[SwapWord(aiAttributeNameIndex)]);
   if S = 'CODE' then with TCodeAttribute(Pointer(aiInfo)^) do
   begin
    Stream.Write(caMaxStack, 2);
    Stream.Write(caMaxLocals, 2);
    Stream.Write(caCodeLength, 4);
    Stream.Write(caCode[0], SwapInt(caCodeLength));
    Stream.Write(caExceptionsCount, 2);
    Stream.Write(caExceptions[0], SwapWord(caExceptionsCount) *
                                  SizeOf(TJavaException));
    Stream.Write(caAttributesCount, 2);
    WriteAttributes(caAttributes);
   end else if S = 'EXCEPTIONS' then
   with TExceptionsAttribute(Pointer(aiInfo)^) do
   begin
    Stream.Write(eaCount, 2);
    Stream.Write(eaTable[0], SwapWord(eaCount) shl 1);
   end else if S = 'INNERCLASSES' then
   with TInnerClassesAttribute(Pointer(aiInfo)^) do
   begin
    Stream.Write(icCount, 2);
    Stream.Write(icClasses[0], SwapWord(icCount) * SizeOf(TInnerClassInfo));
   end else if S = 'LINENUMBERTABLE' then
   with TLineNumberTableAttribute(Pointer(aiInfo)^) do
   begin
    Stream.Write(ltCount, 2);
    Stream.Write(ltTable[0], SwapWord(ltCount) shl 2);
   end else if S = 'LOCALVARIABLETABLE' then
   with TLocalVariableTableAttribute(Pointer(aiInfo)^) do
   begin
    Stream.Write(lvCount, 2);
    Stream.Write(lvTable[0], SwapWord(lvCount) * 10);
   end else Stream.Write(aiInfo[0], Length(aiInfo));
  end;
 end; //WriteAttributes;
{TJavaClass.SaveToStream}
var
 I: Integer;
begin
 if Assigned(Stream) then with FMainData do
 begin
  Stream.Write(jcMagic, 4);
  Stream.Write(jcMinorVersion, 2);
  Stream.Write(jcMajorVersion, 2);
  Stream.Write(jcConstantPoolCount, 2);
  for I := 0 to Length(jcConstantPool) - 1 do with jcConstantPool[I] do
  begin
   Stream.Write(cpTag, 1);
   if cpTag = CONSTANT_Utf8 then with TMBStringInfo(Pointer(cpInfo)^) do
   begin
    Stream.Write(msLen, 2);
    Stream.Write(msText[1], SwapWord(msLen));
   end else Stream.Write(cpInfo[0], Length(cpInfo));
  end;
  Stream.Write(jcAccessFlags, 2);
  Stream.Write(jcThisClass, 2);
  Stream.Write(jcSuperClass, 2);
  Stream.Write(jcInterfacesCount, 2);
  Stream.Write(jcInterfaces[0], SwapWord(jcInterfacesCount) shl 1);
  Stream.Write(jcFieldsCount, 2);
  for I := 0 to SwapWord(jcFieldsCount) - 1 do with jcFields[I] do
  begin
   Stream.Write(fiAccessFlags, 2);
   Stream.Write(fiNameIndex, 2);
   Stream.Write(fiDescriptorIndex, 2);
   Stream.Write(fiAttributesCount, 2);
   WriteAttributes(fiAttributes);
  end;
  Stream.Write(jcMethodsCount, 2);
  for I := 0 to SwapWord(jcMethodsCount) - 1 do with jcMethods[I] do
  begin
   Stream.Write(fiAccessFlags, 2);
   Stream.Write(fiNameIndex, 2);
   Stream.Write(fiDescriptorIndex, 2);
   Stream.Write(fiAttributesCount, 2);
   WriteAttributes(fiAttributes);
  end;
  Stream.Write(jcAttributesCount, 2);
  WriteAttributes(jcAttributes);
 end else
  raise EJavaClassException.Create(Format('%s.SaveToStream error #%d',
        [ClassName, 666]), 666);
end;

procedure TJavaClass.SetCodePage(Value: Cardinal);
var
 Info: TCPInfo;
begin
 FCodePage := Value;
 if GetCPInfo(Value, Info) then
  FMaxCharSize := Info.MaxCharSize else
  FMaxCharSize := 0;
end;

procedure TJavaClass.SetString(PoolIndex: Integer; const Value: AnsiString);
var
 Cnt: Integer;
begin
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_Utf8 then
  with TMBStringInfo(Pointer(cpInfo)^) do
  begin
   Cnt := Length(Value);
   if FFlags and JC_NULL_STR = 0 then
   begin
    if Cnt > 65535 then Cnt := 65535;
    msLen := Cnt;
   end else
   begin
    if Cnt > 65534 then Cnt := 65534;
    msLen := Cnt + 1;
   end;
   SetLength(msText, msLen);
   Move(Pointer(Value)^, Pointer(msText)^, Cnt);
   if FFlags and JC_NULL_STR <> 0 then msText[msLen] := #0;
   msLen := SwapWord(msLen);
  end;
 end;
end;

procedure TJavaClass.SetStringConst(PoolIndex: Integer; const Value: AnsiString);
var
 Cnt: Integer;
begin
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_String then
  Strings[SwapWord(Word(Pointer(cpInfo)^))] := Value;
 end;
end;

procedure TJavaClass.SetWideString(PoolIndex: Integer; const Value: WideString);
var
 Cnt, MSZ: Integer;
begin
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_Utf8 then
  with TMBStringInfo(Pointer(cpInfo)^) do
  begin
   msText := StringEncode(Value);
   Cnt := Length(msText);
   MSZ := 65534 + Byte(FFlags and JC_NULL_STR = 0);
   if Cnt > MSZ then
   begin
    Cnt := MSZ;
    SetLength(msText, Cnt)
   end;
   if FFlags and JC_NULL_STR <> 0 then
   begin
    msLen := SwapWord(Cnt + 1);
    msText := msText + #0;
   end else msLen := SwapWord(Cnt);
  end;
 end;
end;

procedure TJavaClass.SetWideStringConst(PoolIndex: Integer;
                                        const Value: WideString);
var
 Cnt: Integer;
begin
 with FMainData do
 begin
  Cnt := SwapWord(jcConstantPoolCount);
  if (PoolIndex > 0) and (PoolIndex < Cnt) then
  with jcConstantPool[PoolIndex - 1] do if cpTag = CONSTANT_String then
  WideStrings[SwapWord(Word(Pointer(cpInfo)^))] := Value;
 end;
end;

function TJavaClass.StringEncode(const S: WideString): AnsiString;
begin
 Result := CodePageStringEncode(FCodePage, FMaxCharSize, S);
end;

//------ TJavaClassEUC ------//

function TJavaClassEUC.StringDecode(const S: AnsiString): WideString;
begin
 Result := EUCJPtoUTF16(S);
end;

function TJavaClassEUC.StringEncode(const S: WideString): AnsiString;
begin
 Result := UTF16toEUCJP(S);
end;

constructor EJavaClassException.Create(const Msg: String; ErrorCode: Integer);
begin
 inherited Create(Msg);
 FErrorCode := ErrorCode;
end;

end.
