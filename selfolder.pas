unit selfolder;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, ComCtrls, ShellCtrls;

type
  TSelectFolderDialog = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    ShellTreeView: TShellTreeView;
    PathEdit: TEdit;
    SetButton: TButton;
    Procedure SetPath(Const Path: String);
    Function GetPath: String;
    procedure ShellTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure SetButtonClick(Sender: TObject);
   public
    Function Execute: Boolean;
    property Path: String read GetPath write SetPath;
  end;

var
  SelectFolderDialog: TSelectFolderDialog;

implementation

{$R *.DFM}

Procedure TSelectFolderDialog.SetPath(Const Path: String);
begin
 ShellTreeView.Path := Path;
end;

Function TSelectFolderDialog.GetPath: String;
begin
 Result := ShellTreeView.Path;
end;

Function TSelectFolderDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TSelectFolderDialog.ShellTreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
 PathEdit.Text := Path;
end;

procedure TSelectFolderDialog.SetButtonClick(Sender: TObject);
begin
 If DirectoryExists(PathEdit.Text) then Path := PathEdit.Text;
end;

end.
