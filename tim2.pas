unit tim2;

interface

uses SysUtils, Classes, BitmapEx;

type
 TTIM2_File = class(TCustomImageFile)
  public
   procedure LoadFromStream(Stream: TStream); override;
   procedure SaveToStream(Stream: TStream); override;
 end;

implementation

Const
 TIM2Sign = $324D4954;

Type
 TTIM2Header = Packed Record
  thSignTag: Integer;
  thFormatTag: Byte;
  thAlign128: Boolean;
  thLayersCount: Word;
  thReserved1: Integer;
  thReserved2: Integer;
 end;
 TLayerHeader = Packed Record
  lhLayerSize: Integer;
  lhPaletteSize: Integer;
  lhImageSize: Integer;
  lhHeaderSize: Word;
  lhColorsUsed: Word;
  lh256: Word;
  lhControlByte: Byte;
  lhFormat: Byte;
  lhWidth: Word;
  lhHeight: Word;
  lhTEX0: Array[0..7] of Byte;
  lhTEX1: Array[0..7] of Byte;
  lhTEXA: Array[0..3] of Byte;
  lhTEXCLUT: Array[0..3] of Byte;
 end;
 TRGBA = Packed Record
  R: Byte;
  G: Byte;
  B: Byte;
  A: Byte;
 end;
 TPalBlock = Array[0..7] of Cardinal;
 TPalette32 = Array[0..31] of TPalBlock;

{ TTIM2_File }

procedure TTIM2_File.LoadFromStream(Stream: TStream);
var
 Pixels: Pointer;
 Header: TTIM2Header;
 Layer: TLayerHeader;
 W, H, R: Integer;
 AColorTable: Pointer;
 PalBlock: TPalBlock;
 Pal: ^TPalette32;
begin
 with FImageInfo do
 begin
  Stream.ReadBuffer(Header, SizeOf(Header));
  if not ((Header.thSignTag = TIM2Sign) and
      (Header.thFormatTag in [3, 4]) and
      (Header.thLayersCount >= 1)) then
   FileFormatError;
  Stream.ReadBuffer(Layer, SizeOf(Layer));
  if not ((Layer.lhControlByte in [3, $83]) and (Layer.lhFormat = 5) and
     (Layer.lhPaletteSize = 1024)) then
   FileFormatError;
  W := Layer.lhWidth;
  H := Layer.lhHeight;
  Pixels := nil;
  AColorTable := nil;
  try
   GetMem(Pixels, W * H);
   GetMem(AColorTable, 1024);
   Stream.ReadBuffer(Pixels^, W * H);
   Stream.ReadBuffer(AColorTable^, 1024);
   if Layer.lhControlByte = 3 then
   begin
    Pal := AColorTable;
    R := 1;
    repeat
     Move(Pal[R], PalBlock, SizeOf(TPalBlock));
     Move(Pal[R + 1], Pal[R], SizeOf(TPalBlock));
     Move(PalBlock, Pal[R + 1], SizeOf(TPalBlock));
     Inc(R, 4);
    until R > 29;
   end;
   Width := W;
   Height := H;
   ImageFormat := ifIndexed;
   Finalize(CompositeFlags);
   ImageFlags := 7;
   FInternalClrFmt.SetFormat('RGBA8888');
   if FInternalImage <> nil then FreeMem(FInternalImage);
   FInternalImage := Pixels;
   if FInternalPalette <> nil then FreeMem(FInternalPalette);
   FInternalPalette := AColorTable;
   ImageData := Pixels;
   ColorTable := AColorTable;
   ColorFormat := FInternalClrFmt;
  except
   if AColorTable <> nil then FreeMem(AColorTable);
   if Pixels <> nil then FreeMem(Pixels);
  end;
 end;
{ BlockRead(F, Header, SizeOf(Header), R);
   If (Header.thSignTag = TIM2Sign) and
      (Header.thFormatTag in [3, 4]) and
      (Header.thLayersCount >= 1) then
   begin
    BlockRead(F, Layer, SizeOf(Layer), R);
    If (Layer.lhControlByte in [3, $83]) and (Layer.lhFormat = 5) and
       (Layer.lhPaletteSize = 1024) then
    begin
     W := Layer.lhWidth;
     H := Layer.lhHeight;
     GetMem(P8, W * H);
     BlockRead(F, P8^, W * H, R);
     BlockRead(F, ColorTable, 1024, R);
     RGB := Addr(ColorTable);
     For R := 0 to 255 do
     begin
      With RGB^ do
      begin
       If A >= $7F then A := $FF Else A := A shl 1;
       C := R;
       R := B;
       B := C;
      end;
      Inc(RGB);
     end;
     If Layer.lhControlByte = 3 then
     begin
      Pal := Addr(ColorTable[0]);
      R := 1;
      Repeat
       Move(Pal^[R], PalBlock, SizeOf(TPalBlock));
       Move(Pal^[R + 1], Pal^[R], SizeOf(TPalBlock));
       Move(PalBlock, Pal^[R + 1], SizeOf(TPalBlock));
       Inc(R, 4);
      Until R > 29;
     end;
     GetMem(Pixels, W * 4 * H);
     BB := P8;
     RGB := Pixels;
     For R := 0 to W * H - 1 do
     begin
      RGB^ := ColorTable[BB^];
      Inc(BB);
      Inc(RGB);
     end;
     SaveTGA(ChangeFileExt(Path + SR.Name, FExt), W, H, Pixels, True);
     FreeMem(Pixels);
     FreeMem(P8);
     Writeln('Done.');
    end Else
    If (Layer.lhControlByte in [$03, $83]) and (Layer.lhFormat = 4) and
       (Layer.lhPaletteSize = 64) then
    begin
     W := Layer.lhWidth;
     H := Layer.lhHeight;
     GetMem(P8, (W shr 1) * H);
     BlockRead(F, P8^, (W shr 1) * H, R);
     BlockRead(F, ColorTable, 64, R);
     RGB := Addr(ColorTable);
     For R := 0 to 255 do
     begin
      With RGB^ do
      begin
       If A >= $7F then A := $FF Else A := A shl 1;
       C := R;
       R := B;
       B := C;
      end;
      Inc(RGB);
     end;
     GetMem(Pixels, W * 4 * H);
     BB := P8;
     RGB := Pixels;
     For R := 0 to ((W * H) shr 1) - 1 do
     begin
      RGB^ := ColorTable[BB^ and 15];
      Inc(RGB);
      RGB^ := ColorTable[BB^ shr 4];
      Inc(RGB);
      Inc(BB);
     end;
}
end;

procedure TTIM2_File.SaveToStream(Stream: TStream);
begin
//
end;

end.
