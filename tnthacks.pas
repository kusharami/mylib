unit tnthacks;

interface

uses
 Windows, TntWindows, SysUtils, TntSysUtils, Classes, Menus, TntMenus, Graphics;

type
 TUnicodeMainMenu = class(TTntMainMenu)
 end;

 TUnicodePopupMenu = class(TTntPopupMenu)
 end;

 TUnicodeMenuItem = class(TTntMenuItem)
  private
   function GetFBitmap: TBitmap;
  protected
   property FBitmap: TBitmap read GetFBitmap;

   procedure MeasureItem(ACanvas: TCanvas; var Width, Height: Integer); override;
 end;

implementation

uses ImgList;

type
 TMenuItemHack = class(TComponent)
  protected
    FCaption: string;
    FHandle: HMENU;
    FChecked: Boolean;
    FEnabled: Boolean;
    FDefault: Boolean;
    FAutoHotkeys: TMenuItemAutoFlag;
    FAutoLineReduction: TMenuItemAutoFlag;
    FRadioItem: Boolean;
    FVisible: Boolean;
    FGroupIndex: Byte;
    FImageIndex: TImageIndex;
    FActionLink: TMenuActionLink;
    FBreak: TMenuBreak;
    FBitmap: TBitmap;
 end;

{ TUnicodeMenuItem }

function TUnicodeMenuItem.GetFBitmap: TBitmap;
begin
 Result := TMenuItemHack(Self).FBitmap;
end;

procedure TUnicodeMenuItem.MeasureItem(ACanvas: TCanvas; var Width,
  Height: Integer);
const
 Alignments: array[TPopupAlignment] of Word = (DT_LEFT, DT_RIGHT, DT_CENTER);
var
 Alignment: TPopupAlignment;
 ImageList: TCustomImageList;
 ParentMenu: TMenu;
 DrawGlyph: Boolean;
 TopLevel: Boolean;
 DrawStyle: Integer;
 Text: string;
 R: TRect;

 procedure GetMenuSize;
 var
  NonClientMetrics: TNonClientMetricsW;
 begin
  NonClientMetrics.cbSize := sizeof(NonClientMetrics);
  if SystemParametersInfoW(SPI_GETNONCLIENTMETRICS, 0, @NonClientMetrics, 0) then
  begin
   Width := NonClientMetrics.iMenuWidth;
   Height := NonClientMetrics.iMenuHeight;
  end;
 end;

begin
 if (not Win32PlatformIsUnicode) or IsLine then
 begin
  inherited;
  Exit;
 end;
 if GetParentComponent is TMainMenu then
 begin
  TopLevel := True;
  GetMenuSize;
 end else TopLevel := False;
 ParentMenu := GetParentMenu;
 ImageList := GetImageList;
 if Caption = cLineCaption then
 begin
  Height := 5;
  Width := -2;
  DrawGlyph := False;
 end else if Assigned(ImageList) and ((ImageIndex > -1) or not TopLevel) then
 begin
  Width := ImageList.Width;
  if not TopLevel then Height := ImageList.Height;
  DrawGlyph := True;
 end else if Assigned(FBitmap) and not FBitmap.Empty then
 begin
  Width := 16;
  if not TopLevel then Height := 16;
  DrawGlyph := True;
 end else
 begin
  Width := -7;
  DrawGlyph := False;
 end;
 if DrawGlyph and not TopLevel then Inc(Width, 15);
 if not TopLevel then Inc(Height, 3);
 FillChar(R, SizeOf(R), 0);
 if ParentMenu is TMenu then
  Alignment := paLeft else
 if ParentMenu is TPopupMenu then
  Alignment := TPopupMenu(ParentMenu).Alignment else
  Alignment := paLeft;
 if ShortCut <> 0 then
  Text := Concat(Caption, ShortCutToText(ShortCut)) else
  Text := Caption;
 DrawStyle := Alignments[Alignment] or DT_EXPANDTABS or DT_SINGLELINE or
    DT_NOCLIP or DT_CALCRECT;
 DoDrawText(ACanvas, Text, R, False, DrawStyle);
 Inc(Width, R.Right - R.Left + 7);
 if Assigned(OnMeasureItem) then OnMeasureItem(Self, ACanvas, Width, Height);
end;

end.
