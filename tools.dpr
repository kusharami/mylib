program tools;

{$APPTYPE CONSOLE}

uses
  SysUtils, TntClasses, UtilsAdapter,
  NodeLst, PropUtils,
  PlugInterface, LunaGFX,
  PropContainer in 'PropContainer.pas';

var List: TTntStringList;

begin
 List := TTntStringList.Create;
 List.Add('Test1');
 List.Add('Test2');
 Writeln(List.AnsiStrings[0]);
 List.Free;
  { TODO -oUser -cConsole Main : Insert code here }
end.
