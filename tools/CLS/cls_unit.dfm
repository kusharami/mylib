object MainForm: TMainForm
  Left = 364
  Top = 169
  Width = 910
  Height = 747
  Caption = 'Crazy List Shuffler'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 679
    Width = 902
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 902
    Height = 679
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    OnChange = PageControlChange
    object TabSheet1: TTabSheet
      Caption = 'Text'
      object CharList: TTntMemo
        Left = 0
        Top = 41
        Width = 894
        Height = 610
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Meiryo'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object ButtonsBar1: TTntPanel
        Left = 0
        Top = 0
        Width = 894
        Height = 41
        Align = alTop
        TabOrder = 1
        object ShuffleButton: TButton
          Left = 8
          Top = 8
          Width = 113
          Height = 25
          Caption = 'Char Shuffle'
          TabOrder = 0
          OnClick = ShuffleButtonClick
        end
        object LineShuffleButton: TButton
          Left = 128
          Top = 8
          Width = 113
          Height = 25
          Caption = 'Line Shuffle'
          TabOrder = 1
          OnClick = LineShuffleButtonClick
        end
        object CharCntBtn: TButton
          Left = 248
          Top = 8
          Width = 113
          Height = 25
          Caption = 'Count Characters'
          TabOrder = 2
          OnClick = CharCntBtnClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Char'
      ImageIndex = 1
      object Splitter: TSplitter
        Left = 170
        Top = 41
        Width = 6
        Height = 610
      end
      object CharPanel: TPanel
        Left = 0
        Top = 41
        Width = 170
        Height = 610
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        OnResize = CharPanelResize
        object CharLabel: TTntLabel
          Left = 0
          Top = 0
          Width = 170
          Height = 610
          Align = alClient
          AutoSize = False
          Caption = #23383
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -160
          Font.Name = 'Meiryo'
          Font.Style = []
          ParentFont = False
        end
      end
      object ButtonsBar2: TTntPanel
        Left = 0
        Top = 0
        Width = 894
        Height = 41
        Align = alTop
        TabOrder = 1
        object IndexEdit: TSpinEdit
          Left = 8
          Top = 8
          Width = 113
          Height = 25
          MaxValue = 1
          MinValue = 1
          TabOrder = 0
          Value = 1
          OnChange = IndexEditChange
        end
      end
    end
  end
  object MainMenu: TTntMainMenu
    Left = 512
    object File1: TTntMenuItem
      Caption = 'File'
      object Exit1: TTntMenuItem
        Caption = 'Exit'
        OnClick = Exit1Click
      end
    end
    object Edit1: TTntMenuItem
      Caption = 'Edit'
      object SelectAll1: TTntMenuItem
        Caption = 'Select All'
        ShortCut = 16449
        OnClick = SelectAll1Click
      end
    end
    object Options1: TTntMenuItem
      Caption = 'Options'
      object Font1: TTntMenuItem
        Caption = 'Font...'
        OnClick = Font1Click
      end
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Meiryo'
    Font.Style = []
    Left = 456
  end
  object Timer: TTimer
    Interval = 10
    OnTimer = TimerTimer
    Left = 400
  end
end
