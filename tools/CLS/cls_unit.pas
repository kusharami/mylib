unit cls_unit;

interface

uses
  XPMan, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TntExtCtrls, TntStdCtrls, Menus, TntMenus, TntClasses,
  ComCtrls, Spin;

type
  TMainForm = class(TForm)
    MainMenu: TTntMainMenu;
    File1: TTntMenuItem;
    Exit1: TTntMenuItem;
    CharList: TTntMemo;
    Options1: TTntMenuItem;
    Font1: TTntMenuItem;
    ButtonsBar1: TTntPanel;
    ShuffleButton: TButton;
    FontDialog: TFontDialog;
    LineShuffleButton: TButton;
    Edit1: TTntMenuItem;
    SelectAll1: TTntMenuItem;
    StatusBar: TStatusBar;
    Timer: TTimer;
    PageControl: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CharLabel: TTntLabel;
    CharPanel: TPanel;
    Splitter: TSplitter;
    ButtonsBar2: TTntPanel;
    IndexEdit: TSpinEdit;
    CharCntBtn: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ShuffleButtonClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure Font1Click(Sender: TObject);
    procedure LineShuffleButtonClick(Sender: TObject);
    procedure SelectAll1Click(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure CharPanelResize(Sender: TObject);
    procedure IndexEditChange(Sender: TObject);
    procedure CharCntBtnClick(Sender: TObject);
  private
    CurrentChar: Integer;
    CharsTotal: Integer;
    Chars: WideString;
    procedure UpdateStatusBar;
    procedure UpdateChar;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
 CharList.Font := FontDialog.Font;
 CharLabel.Font := FontDialog.Font;
 CharPanelResize(nil);
end;

procedure TMainForm.Exit1Click(Sender: TObject);
begin
 Close;
end;

procedure TMainForm.Font1Click(Sender: TObject);
begin
 if FontDialog.Execute then
 begin
  CharList.Font := FontDialog.Font;
  CharLabel.Font := FontDialog.Font;
  CharPanelResize(nil);
 end;
end;

procedure TMainForm.ShuffleButtonClick(Sender: TObject);
var
 Src, Dst: WideString;
 L, X: Integer;
 P: PWideChar;
begin
 Src := '';
 for X := 0 to CharList.Lines.Count - 1 do
  Src := Src + CharList.Lines[X];
 L := Length(Src);
 SetLength(Dst, L);
 Randomize;
 P := Pointer(Dst);
 while L > 0 do
 begin
  X := Random(L) + 1;
  P^ := Src[X];
  Inc(P);
  if X <> L then
   Src[X] := Src[L];
  Dec(L);
 end;
 CharList.Lines.Text := Dst;
end;

procedure TMainForm.LineShuffleButtonClick(Sender: TObject);
var
 Src, Dst: TTntStringList;
 L, X: Integer;
begin
 Src := TTntStringList.Create;
 try
  Src.Text := CharList.Text;
  L := Src.Count;
  Dst := TTntStringList.Create;
  try
   Randomize;
   while L > 0 do
   begin
    X := Random(L);
    Dst.Add(Src.Strings[X]);
    Dec(L);
    if X <> L then
     Src.Strings[X] := Src.Strings[L];
   end;
   CharList.Lines.Assign(Dst);
  finally
   Dst.Free;
  end;
 finally
  Src.Free;
 end;
end;

procedure TMainForm.SelectAll1Click(Sender: TObject);
begin
 CharList.SelectAll;
end;

procedure TMainForm.UpdateStatusBar;
begin
 if PageControl.ActivePageIndex = 0 then
 with CharList, CaretPos do
 begin
  StatusBar.Panels[0].Text := Format('X: %d/%d', [X, Length(Lines[Y])]);
  StatusBar.Panels[1].Text := Format('Y: %d/%d', [Y, Lines.Count]);
// end else
// begin
//  StatusBar.Panels[0].Text := Format('Current: %d', [CurrentChar + 1]);
//  StatusBar.Panels[1].Text := Format('Total: %d', [CharsTotal]);
 end;
end;

procedure TMainForm.TimerTimer(Sender: TObject);
begin
 UpdateStatusBar;
end;

procedure TMainForm.PageControlChange(Sender: TObject);
var
 X: Integer;
begin
 SelectAll1.Enabled := PageControl.ActivePageIndex = 0;
 if PageControl.ActivePageIndex = 1 then
 begin
  Chars := '';
  for X := 0 to CharList.Lines.Count - 1 do
   Chars := Chars + CharList.Lines[X];
  CharsTotal := Length(Chars);
  StatusBar.Panels[1].Text := Format('Total: %d', [CharsTotal]);
  if CurrentChar < 0 then
   CurrentChar := 0;
  if CurrentChar >= CharsTotal then
   CurrentChar := CharsTotal - 1;
  IndexEdit.Enabled := CharsTotal > 0;
  IndexEdit.MaxValue := CharsTotal;
  IndexEdit.MinValue := 1;
  IndexEdit.OnChange := nil;
  IndexEdit.Value := CurrentChar + 1;
  IndexEdit.OnChange := IndexEditChange;
  UpdateChar;
 end;
end;

procedure TMainForm.CharPanelResize(Sender: TObject);
begin
 CharLabel.Font.Height := -(CharLabel.Width - 10);
end;

procedure TMainForm.UpdateChar;
begin
 if CurrentChar >= 0 then
  CharLabel.Caption := Chars[CurrentChar + 1];
 StatusBar.Panels[0].Text := Format('Current: %d', [CurrentChar + 1]);  
end;

procedure TMainForm.IndexEditChange(Sender: TObject);
begin
 try
  CurrentChar := IndexEdit.Value - 1;
 except
  CurrentChar := -1;
 end;
 UpdateChar;
end;

procedure TMainForm.CharCntBtnClick(Sender: TObject);
type
 PWCArray = ^TWCArray;
 TWCArray = array[WideChar] of Integer;
var
 Str: WideString;
 Freq: PWCArray;
 P: PWideChar;
 Count, I: Integer;
begin
 Str := CharList.Text;
 P := Pointer(Str);
 GetMem(Freq, 65536 * 4);
 try
  FillChar(Freq^, 65536 * 4, 0);
  Count := 0;
  while P^ <> #0 do
  begin
   if (P^ >= #32) and (Freq[P^] = 0) then
    Inc(Count);
   Inc(Freq[P^]);
   Inc(P);
  end;
  SetLength(Str, Count);
  P := Pointer(Str);
  for I := 32 to 65535 do
  if Freq[WideChar(I)] > 0 then
  begin
   P^ := WideChar(I);
   Inc(P);
  end;
  CharList.Text := Str;
 finally
  FreeMem(Freq);
 end;
end;

end.
