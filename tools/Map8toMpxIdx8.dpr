program ConvertMapToMpxIdx;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes;

Var
 Stream: TMemoryStream;
 MpxArray: Array of Byte;
 MapArray: Array of Byte;
 Width, WW: Integer;
 Height, HH: Integer;
 X, Y: Integer;
 MPX, MAP: ^Byte;

Type
 TIdxItem = Array[0..3] of Byte;
 PIdxRec = ^TIdxRec;
 TIdxRec = Record
  Data: TIdxItem;
  Next: PIdxRec;
 end;
 TIdx = Class
 private
  FRoot, FCur: PIdxRec;
  FCount: Integer;
  Function Get(Index: Integer): PIdxRec;
 public
  Constructor Create;
  Destructor Destroy; override;
  Function Add: PIdxRec;
  Function Find(Var X; Var I: Integer): PIdxRec;
  Procedure Clear;
  Property Items[Index: Integer]: PIdxRec read Get;
 end;

Function TIdx.Get(Index: Integer): PIdxRec;
Var I: Integer; N: PIdxRec;
begin
 I := 0; N := FRoot; Result := NIL;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Constructor TIdx.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TIdx.Destroy;
begin
 Clear;
 Inherited Destroy;
end;

Function TIdx.Add: PIdxRec;
begin
 New(Result);
 If FRoot = NIL then FRoot := Result Else FCur^.Next := Result;
 FCur := Result;
 FillChar(Result^, SizeOf(TIdxRec), 0);
 Inc(FCount);
end;

Function TIdx.Find(Var X; Var I: Integer): PIdxRec;
Var N: PIdxRec;
begin
 N := FRoot; I := 0;
 While N <> NIL do With N^ do
 begin
  If CompareMem(Addr(X), Addr(Data), SizeOf(TIdxItem)) then
  begin
   Result := N;
   Exit;
  end;
  N := Next;
  Inc(I);
 end;
 I := -1;
 Result := NIL;
end;

Procedure TIdx.Clear;
Var N: PIdxRec;
begin
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

Var
 Idx: TIdx; FT: PIdxRec; JIndex: Integer;
 IdxItem: TIdxItem;
 YY, XX: Integer;

begin
 If ParamCount >= 3 then
 begin
  Stream := TMemoryStream.Create;
  Stream.LoadFromFile(ParamStr(1));
  Width := 0; Height := 0;
  Stream.Read(Width, 1);
  WW := Width shr 1;
  Stream.Read(Height, 1);
  HH := Height shr 1;
  SetLength(MapArray, Width * Height);
  Stream.Read(MapArray[0], Width * Height);
  SetLength(MpxArray, WW * HH);
  MPX := Addr(MpxArray[0]);
  Idx := TIdx.Create;
  if ParamCount > 3 then for X := 0 to 3 do
   FillChar(Idx.Add.Data, 4, X);
  With Idx do
  begin
   For Y := 0 to HH - 1 do
    For X := 0 to WW - 1 do
    begin
     FillChar(IdxItem, SizeOf(TIdxItem), 0);
     For YY := 0 to 1 do
     begin
      MAP := Addr(MapArray[(Y shl 1 + YY) * Width + X shl 1]);
      For XX := 0 to 1 do
      begin
       IdxItem[YY * 2 + XX] := MAP^;
       Inc(MAP);
      end;
     end;
     FT := Find(IdxItem, JIndex);
     If FT = NIL then With Add^ do
     begin
      Data := IdxItem;
      MPX^ := FCount - 1;
     end Else MPX^ := JIndex;
     Inc(MPX);
    end;
   Stream.Clear;
   For XX := 0 to FCount - 1 do
    Stream.Write(Items[XX]^.Data, SizeOf(TIdxItem));
   Stream.SaveToFile(ParamStr(3));
   Stream.Clear;
   Stream.Write(MpxArray[0], Length(MpxArray));
   Stream.SaveToFile(ParamStr(2));
  end;
  Idx.Free;
  Stream.Free;
 end;
end.

