program MpxIdx8toMap8;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes;

Type
 TBigTile = Packed Record
  X1: Byte;
  X2: Byte;
  X3: Byte;
  X4: Byte;
 end;

Var
 Stream: TMemoryStream;
 MpxArray: Array of Byte;
 IdxArray: Array of TBigTile;
 MapArray: Array of Byte;
 Width, WW: Integer;
 Height, HH: Integer;
 X, Y: Integer;
 MPX: ^Byte;
 MAP: ^Byte;

begin
 If ParamCount = 5 then
 begin
  Stream := TMemoryStream.Create;
  Stream.LoadFromFile(ParamStr(1));
  SetLength(MpxArray, Stream.Size);
  Stream.Read(MpxArray[0], Length(MpxArray));
  Stream.Clear;
  Stream.LoadFromFile(ParamStr(2));
  SetLength(IdxArray, Stream.Size div SizeOf(TBigTile));
  Stream.Read(IdxArray[0], Length(IdxArray) * SizeOf(TBigTile));
  Width := StrToInt(ParamStr(3));
  WW := Width shr 1;
  Height := StrToInt(ParamStr(4));
  HH := Height shr 1;
  SetLength(MapArray, Width * Height);
  Stream.Clear;
  MPX := Addr(MpxArray[0]);
  For Y := 0 to HH - 1 do
   For X := 0 to WW - 1 do
   begin
    With IdxArray[MPX^] do
    begin
     MAP := Addr(MapArray[(Y shl 1) * Width + X shl 1]);
     MAP^ := X1;
     Inc(MAP);
     MAP^ := X2;
     Inc(MAP, Width);
     MAP^ := X4;
     Dec(MAP);
     MAP^ := X3;
    end; 
    Inc(MPX);
   end;
  Stream.Write(Width, 1);
  Stream.Write(Height, 1);
  Stream.Write(MapArray[0], Length(MapArray));
  Stream.SaveToFile(ParamStr(5));
  Stream.Free;
 end Else
 begin
  Writeln('MpxIdx8toMap8 [MPXFILE] [IDXFILE] [WIDTH] [HEIGHT] [MAPFILE]');
 end;
end.
