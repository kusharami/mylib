program WCXer;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Windows,
  Classes,
  WCXheadEX,
  MyUtils,
  MyClasses;

const
 Info: AnsiString =
 'Packer/Unpacker for WCX total commander plugins v1.0'#13#10+
 #13#10+
 'USAGE:'#13#10+
 ' WCXer plugin.wcx -option archive.ext [-suboption argslist]'#13#10+
  #13#10+
 'OPTIONS:'#13#10+
 '-l:'#9#9#9'List all files in archive'#13#10+
 '-e:'#9#9#9'Extract files from archive'#13#10+
 '-a:'#9#9#9'Add files to archive'#13#10+
 '-r:'#9#9#9'Remove files from archive'#13#10+
 #13#10+
 'SUB OPTIONS:'#13#10+
 '-f file1,...,fileN:'#9'Specify the file list'#13#10+
 '-l filename.txt:'#9'Specify the file list in a text file [filename.txt]'#13#10+
 '-s path:'#9#9'Source path'#13#10+
 '-d path:'#9#9'Destination path'#13#10+
 '-n:'#9#9#9'Extract/add without sub directories'#13#10+
 '-b:'#9#9#9'Search files in subdirectories'#13#10+
 '-a "params":'#9#9'Specify parameter list for the plugin';

 SFuncNotFound: AnsiString = 'Function not found: %s';
 SExtracting: AnsiString = 'Extracting: ';
 SAdding: AnsiString = 'Adding: ';
 SDeleting: AnsiString = 'Deleting: ';

var
 OpenArchive: function(var ArchiveData: TOpenArchiveData): HANDLE; stdcall;
 OpenArchiveW: function(var ArchiveData: TOpenArchiveDataW): HANDLE; stdcall;
 ReadHeader: function(hArcData: HANDLE; var HeaderData: THeaderData): Integer; stdcall;
 ReadHeaderEx: function(hArcData: HANDLE; var HeaderData: THeaderDataEx): Integer; stdcall;
 ReadHeaderExW: function(hArcData: HANDLE; var HeaderData: THeaderDataExW): Integer; stdcall;
 ProcessFile: function(hArcData: HANDLE; Operation: Integer;
                     DestPath: PAnsiChar; DestName: PAnsiChar): Integer; stdcall;
 ProcessFileW: function(hArcData: HANDLE; Operation: Integer;
               DestPath: PWideChar; DestName: PWideChar): Integer; stdcall;
 CloseArchive: function(hArcData: HANDLE): Integer; stdcall;
 SetChangeVolProc: procedure(hArcData: HANDLE; ChangeVolPrc: TChangeVolProc); stdcall;
 SetChangeVolProcW: procedure(hArcData: HANDLE; ChangeVolPrc: TChangeVolProcW); stdcall;
 SetProcessDataProc: procedure(hArcData: HANDLE; ProcessDataPrc: TProcessDataProc); stdcall;
 SetProcessDataProcW: procedure(hArcData: HANDLE; ProcessDataPrc: TProcessDataProcW); stdcall;
 GetPackerCaps: function: Integer; stdcall;
 PackFiles: function(PackedFile, SubPath, SrcPath, AddList: PAnsiChar; Flags: Integer): Integer; stdcall;
 PackFilesW: function(PackedFile, SubPath, SrcPath, AddList: PWideChar; Flags: Integer): Integer; stdcall;
 DeleteFiles: function(PackedFile, DeleteList: PAnsiChar): Integer; stdcall;
 DeleteFilesW: function(PackedFile, DeleteList: PWideChar): Integer; stdcall;
 AcceptParameters: function(hArcData: HANDLE; Str: PWideChar): Integer; stdcall;

function ChangeVolume(ArcName: PAnsiChar; Mode: LongInt): LongInt; stdcall;
var
 S: AnsiString;
begin
 case Mode of
  PK_VOL_ASK:
  begin
   Writeln(ArcName, ': if you agree to change the volume enter ''y''');
   Readln(S);
   LongBool(Result) := S = 'y';
  end;
  PK_VOL_NOTIFY:
  begin
   Writeln(ArcName, ': unpacking the next volume');
   Result := 1;
  end;
  else Result := 0;
 end;
end;

function ChangeVolumeW(ArcName: PWideChar; Mode: LongInt): LongInt; stdcall;
var
 S: AnsiString;
begin
 case Mode of
  PK_VOL_ASK:
  begin
   Writeln(ArcName, ': if you agree to change the volume enter ''y''');
   Readln(S);
   LongBool(Result) := S = 'y';
  end;
  PK_VOL_NOTIFY:
  begin
   Writeln(ArcName, ': unpacking the next volume');
   Result := 1;
  end;
  else Result := 0;
 end;
end;

var
 LastFileName: AnsiString;
 Action: AnsiString;

function ProcessData(FileName: PAnsiChar; Size: LongInt): LongInt; stdcall;
begin
 if FileName <> LastFileName then
 begin
  LastFileName := FileName;
  if FileName <> nil then
   Writeln(Action, LastFileName);
 end;
 Result := 1;
end;

var
 LastFileNameW: WideString;

function ProcessDataW(FileName: PWideChar; Size: LongInt): LongInt; stdcall;
begin
 if FileName <> LastFileNameW then
 begin
  LastFileNameW := FileName;
  if FileName <> nil then
   Writeln(Action, LastFileNameW);
 end;
 Result := 1;
end;

function Test(Error: Integer): Integer;
var
 Msg: AnsiString;
begin
 Result := Error;
 case Error of
  E_OK,
  E_END_ARCHIVE: Msg := '';
  E_NO_MEMORY: Msg := 'Not enough memory';
  E_BAD_DATA: Msg := 'CRC error in the data of the currently unpacked file';
  E_BAD_ARCHIVE: Msg := 'The archive as a whole is bad, e.g. damaged headers';
  E_UNKNOWN_FORMAT: Msg := 'Archive format unknown';
  E_EOPEN: Msg := 'Cannot open existing file';
  E_ECREATE: Msg := 'Cannot create file';
  E_ECLOSE: Msg := 'Error closing file';
  E_EREAD: Msg := 'Error reading from file';
  E_EWRITE: Msg := 'Error writing to file';
  E_SMALL_BUF: Msg := 'Buffer too small';
  E_EABORTED: Msg := 'Function aborted by user';
  E_NO_FILES: Msg := 'No files found';
  E_TOO_MANY_FILES: Msg := 'Too many files to pack';
  E_NOT_SUPPORTED: Msg := 'Not supported';
  else Msg := 'Unknown error code ' + IntToStr(Error);
 end;
 if Msg <> '' then raise Exception.Create(Msg);
end;

procedure SetProcessProcs(A: HANDLE; const Act: AnsiString);
begin
 Action := Act;
 if @SetProcessDataProcW <> nil then
  SetProcessDataProcW(A, @ProcessDataW);
 if @SetProcessDataProc <> nil then
  SetProcessDataProc(A, @ProcessData);
 if @SetChangeVolProcW <> nil then
  SetChangeVolProcW(A, @ChangeVolumeW);
 if @SetChangeVolProc <> nil then
  SetChangeVolProc(A, @ChangeVolume);
end;

function _ProcessFile(A: HANDLE; Operation: Integer;
                     const DestPath, DestName: WideString): Integer;
procedure CheckDirs;
var
 FullPath: WideString;
begin
 FullPath := DestPath + DestName;
 if not MakeDirs(FullPath) then
  raise Exception.Create('Cannot create directories for ''' + FullPath + '''');
end;
var
 ADP, ADN: AnsiString;
begin
 if @ProcessFileW <> nil then
 begin
  if Operation = PK_EXTRACT then CheckDirs;
  Result := ProcessFileW(A, Operation, Pointer(DestPath), Pointer(DestName));
 end else
 if @ProcessFile <> nil then
 begin
  if Operation = PK_EXTRACT then CheckDirs;
  ADP := DestPath;
  ADN := DestName;
  Result := ProcessFile(A, Operation, Pointer(ADP), Pointer(ADN));
 end else
  raise Exception.Create(Format(SFuncNotFound, ['ProcessFile|Ex']));
end;

procedure ExtrAllLoop(A: HANDLE; const DestPath: WideString; NoSubDirs: Boolean);
var
 HeaderDataExW: THeaderDataExW;
 HeaderDataEx: THeaderDataEx absolute HeaderDataExW;
 HeaderData: THeaderData absolute HeaderDataEx;
 DestName: WideString;
begin
 FillChar(HeaderDataExW, SizeOf(THeaderDataExW), 0);
 if @ReadHeaderExW <> nil then
 while Test(ReadHeaderExW(A, HeaderDataExW)) <> E_END_ARCHIVE do
 begin
  if NoSubDirs then
   DestName := WideExtractFileName(PWideChar(Addr(HeaderDataExW.FileName))) else
   DestName := PWideChar(Addr(HeaderDataExW.FileName));
  Test(_ProcessFile(A, PK_EXTRACT, DestPath, DestName));
 end else
 if @ReadHeaderEx <> nil then
 while Test(ReadHeaderEx(A, HeaderDataEx)) <> E_END_ARCHIVE do
 begin
  if NoSubDirs then
   DestName := ExtractFileName(PAnsiChar(Addr(HeaderDataEx.FileName))) else
   DestName := PAnsiChar(Addr(HeaderDataEx.FileName));
  Test(_ProcessFile(A, PK_EXTRACT, DestPath, DestName));
 end else
 if @ReadHeader <> nil then
 while Test(ReadHeader(A, HeaderData)) <> E_END_ARCHIVE do
 begin
  if NoSubDirs then
   DestName := ExtractFileName(PAnsiChar(Addr(HeaderData.FileName))) else
   DestName := PAnsiChar(Addr(HeaderData.FileName));
  Test(_ProcessFile(A, PK_EXTRACT, DestPath, DestName));
 end else
  raise Exception.Create(Format(SFuncNotFound, ['ReadHeader|Ex|W']));
end;

function FindW(const List: array of WideString; const FName: WideString; SubSearch: Boolean): Boolean;
var
 I, FL, L: Integer;
 FN, Full, Mask: WideString;
begin
 if SubSearch then
 begin
  for I := 0 to Length(List) - 1 do
   if WideMatchesMask(FName, List[I]) then
   begin
    Result := True;
    Exit;
   end;
 end else
 begin
  Full := WideUpperCase(FName);
  FN := WideExtractFileName(Full);
  FL := Length(Full) - Length(FN);
  for I := 0 to Length(List) - 1 do
  begin
   Mask := WideUpperCase(WideExtractFileName(List[I]));
   if WideMatchesMask(FN, Mask, True) then
   begin
    L := Length(List[I]) - Length(Mask);
    Mask := WideUpperCase(List[I]);
    if (L = FL) and
    ((L = 0) or CompareMem(Pointer(Full), Pointer(Mask), L shl 1)) then
    begin
     Result := True;
     Exit;
    end;
   end;
  end;
 end;
 Result := False;
end;

procedure ExtractOrSkip(A: HANDLE; const DestPath, DestName: WideString;
            NoSubDirs, SubSearch: Boolean; const List: array of WideString);
begin
 if FindW(List, DestName, SubSearch) then
 begin
  if NoSubDirs then
   Test(_ProcessFile(A, PK_EXTRACT, DestPath, WideExtractFileName(DestName))) else
   Test(_ProcessFile(A, PK_EXTRACT, DestPath, DestName))
 end else
  Test(_ProcessFile(A, PK_SKIP, '', ''));
end;

procedure ExtractLoop(A: HANDLE; const DestPath: WideString; NoSubDirs: Boolean;
                      SubSearch: Boolean; const List: array of WideString);
var
 HeaderDataExW: THeaderDataExW;
 HeaderDataEx: THeaderDataEx absolute HeaderDataExW;
 HeaderData: THeaderData absolute HeaderDataEx;
begin
 FillChar(HeaderDataExW, SizeOf(THeaderDataExW), 0);
 if @ReadHeaderExW <> nil then
 begin
  while Test(ReadHeaderExW(A, HeaderDataExW)) <> E_END_ARCHIVE do
   ExtractOrSkip(A, DestPath, PWideChar(Addr(HeaderDataExW.FileName)),
    NoSubDirs, SubSearch, List);
 end else
 if @ReadHeaderEx <> nil then
 begin
  while Test(ReadHeaderEx(A, HeaderDataEx)) <> E_END_ARCHIVE do
   ExtractOrSkip(A, DestPath, PAnsiChar(Addr(HeaderDataEx.FileName)),
    NoSubDirs, SubSearch, List);
 end else
 if @ReadHeader <> nil then
 begin
  while Test(ReadHeader(A, HeaderData)) <> E_END_ARCHIVE do
   ExtractOrSkip(A, DestPath, PAnsiChar(Addr(HeaderData.FileName)),
    NoSubDirs, SubSearch, List);
 end else
  raise Exception.Create(Format(SFuncNotFound, ['ReadHeader|Ex|W']));
end;

procedure ListLoop(A: HANDLE);
const
 FmtW: WideString = 's:%d'#9'p:%d'#9'f:0x%4.4x'#9'%s';
 FmtA: AnsiString = 's:%d'#9'p:%d'#9'f:0x%4.4x'#9'%s';
var
 HeaderDataExW: THeaderDataExW;
 HeaderDataEx: THeaderDataEx absolute HeaderDataExW;
 HeaderData: THeaderData absolute HeaderDataEx;
begin
 FillChar(HeaderDataExW, SizeOf(THeaderDataExW), 0);
 if @ReadHeaderExW <> nil then
 while Test(ReadHeaderExW(A, HeaderDataExW)) <> E_END_ARCHIVE do
 begin
  with HeaderDataExW do
  Writeln(WideFormat(FmtW, [Int64(Addr(UnpSize)^), Int64(Addr(PackSize)^),
                            Flags, PWideChar(@FileName)]));
  Test(_ProcessFile(A, PK_SKIP, '', ''));
 end else
 if @ReadHeaderEx <> nil then
 while Test(ReadHeaderEx(A, HeaderDataEx)) <> E_END_ARCHIVE do
 begin
  with HeaderDataEx do
  Writeln(Format(FmtA, [Int64(Addr(UnpSize)^), Int64(Addr(PackSize)^),
                        Flags, PAnsiChar(@FileName)]));
  Test(_ProcessFile(A, PK_SKIP, '', ''));
 end else
 if @ReadHeader <> nil then
 while Test(ReadHeader(A, HeaderData)) = E_END_ARCHIVE do
 begin
  with HeaderData do
  Writeln(Format(FmtA, [UnpSize, PackSize, Flags, PAnsiChar(@FileName)]));
  Test(_ProcessFile(A, PK_SKIP, '', ''));
 end else
  raise Exception.Create(Format(SFuncNotFound, ['ReadHeader|Ex|W']));
end;

function _OpenArchive(Mode: Integer; AName: PAnsiChar; WName: PWideChar): HANDLE;
var
 ArcDataW: TOpenArchiveDataW;
 ArcData: TOpenArchiveData absolute ArcDataW;
begin
 FillChar(ArcDataW, SizeOf(TOpenArchiveDataW), 0);
 if @OpenArchiveW <> nil then
 begin
  ArcDataW.ArcName := WName;
  ArcDataW.OpenMode := Mode;
  Result := OpenArchiveW(ArcDataW);
  Test(ArcDataW.OpenResult);
 end else
 if @OpenArchive <> nil then
 begin
  ArcData.ArcName := AName;
  ArcData.OpenMode := Mode;
  Result := OpenArchive(ArcData);
  Test(ArcData.OpenResult);  
 end else
  raise Exception.Create(Format(SFuncNotFound, ['OpenArchive|W']));
end;

var
 H: THandle;
 A: HANDLE;
 List: array of WideString;
 ArcNameW, SrcPath, DestPath, Option, LibName: WideString;
 ArcName: AnsiString;
 NoSubDirs, SubSearch: Boolean;
 SubParams: WideString;

procedure LoadFileList(const FileName: WideString);
var
 H: THandle;
 S: TStream;
 Cnt, L: Integer;
 Str: WideString;
 P, PSave: PWideChar;

begin
 H := CreateFileW(PWideChar(FileName), GENERIC_READ,
                  FILE_SHARE_READ, nil, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL, 0);
 if H <> INVALID_HANDLE_VALUE then
 try
  S := THandleStream.Create(H);
  try
   Str := ReadCodePagedString(S, CP_WideBOM or CP_MbcsBOM, Cnt);
   P := Pointer(Str);
   repeat
    PSave := P;
    while (P^ <> #13) and (P^ <> #10) and (P^ <> #0) do
    begin
     if P^ = '/' then P^ := '\';
     Inc(P);
    end;
    Cnt := P - PSave;
    case P^ of
     #10:
     begin
      Inc(P);
      if P^ = #13 then Inc(P);
     end;
     #13:
     begin
      Inc(P);
      if P^ = #10 then Inc(P);
     end;
     #$0A0D,
     #$0D0A: Inc(P);
    end;
    L := Length(List);
    SetLength(List, L + 1);
    SetLength(List[L], Cnt);
    Move(PSave^, Pointer(List[L])^, Cnt shl 1);
   until P^ = #0;
  finally
   S.Free;
  end;
 finally
  FileClose(H);
 end;
end;

function ParseParam(Count: Integer): Boolean;
var
 I, L: Integer; Opt: Integer;
 S: WideString;
begin
 if Count >= 3 then
 begin
  LibName := FixedFileName(WideParamStr(1), '\', ['/']);
  Option := WideParamStr(2);
  ArcNameW := FixedFileName(WideParamStr(3), '\', ['/']);
  SrcPath := '';
  DestPath := '';
  SubParams := '';
  NoSubDirs := False;
  SubSearch := False;
  ArcName := ArcNameW;
  Opt := -1;
  for I := 4 to Count do
  begin
   S := WideParamStr(I);
   if (Length(S) = 2) and (S[1] = '-') then
   case UpCase(Char(S[2])) of
    'F':
    begin
     Opt := 0;
     Continue;
    end;
    'S':
    begin
     Opt := 1;
     Continue;
    end;
    'D':
    begin
     Opt := 2;
     Continue;
    end;
    'L':
    begin
     Opt := 3;
     Continue;
    end;
    'A':
    begin
     Opt := 4;
     Continue;
    end;
    'N':
    begin
     NoSubDirs := True;
     Opt := -1;
     Continue;
    end;
    'B':
    begin
     SubSearch := True;
     Opt := -1;
     Continue;
    end;
   end;
   case Opt of
    0:
    begin
     L := Length(List);
     SetLength(List, L + 1);
     List[L] := FixedFileName(S, '\', ['/']);
    end;
    1:
    begin
     SrcPath := WideIncludeTrailingPathDelimiter(FixedFileName(S, '\', ['/']));
     Opt := -1;
    end;
    2:
    begin
     DestPath := WideIncludeTrailingPathDelimiter(FixedFileName(S, '\', ['/']));
     Opt := -1;
    end;
    3:
    begin
     LoadFileList(FixedFileName(S, '\', ['/']));
     Opt := -1;
    end;
    4:
    begin
     SubParams := S;
     Opt := -1;
    end;
    else raise Exception.Create('Sub option expected');
   end;
  end;
  Result := Length(List) > 0;
 end else
  Result := False;
end;

function WideFileExists(const FileName: WideString): Boolean;
var
 FindData: TWIN32FindDataW;
 Handle: THandle;
begin
 Result := False;
 Handle := FindFirstFileW(Pointer(FileName), FindData);
 if Handle <> INVALID_HANDLE_VALUE then
 begin
  FindClose(Handle);
  if (FindData.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0 then
   Result := True;
 end;
end;

function FillListBufW: Pointer;
var
 Size, I: Integer;
 P: PByte; PW: PWord absolute P;
begin
 Size := 2;
 for I := 0 to Length(List) - 1 do
  Inc(Size, (Length(List[I]) shl 1) + 2);
 GetMem(Result, Size);
 P := Result;
 for I := 0 to Length(List) - 1 do
 begin
  Size := (Length(List[I]) shl 1) + 2;
  Move(Pointer(List[I])^, P^, Size);
  Inc(P, Size);
 end;
 PW^ := 0;
end;

function FillListBufA: Pointer;
var
 Size, I: Integer;
 P: PByte;
 A: AnsiString;
begin
 Size := 1;
 for I := 0 to Length(List) - 1 do
  Inc(Size, Length(List[I]) + 1);
 GetMem(Result, Size);
 P := Result;
 for I := 0 to Length(List) - 1 do
 begin
  A := List[I];
  Size := Length(A) + 1;
  Move(Pointer(A)^, P^, Size);
  Inc(P, Size);
 end;
 P^ := 0;
end;

var
 Flags: Integer;
 Buf: Pointer;
 S1, S2: AnsiString;
begin
 try
  if ParseParam(ParamCount) then
  begin
   H := LoadLibraryW(Pointer(LibName));
   if H <> 0 then
   try
    @OpenArchive := GetProcAddress(H, 'OpenArchive');
    @OpenArchiveW := GetProcAddress(H, 'OpenArchiveW');
    @ReadHeader := GetProcAddress(H, 'ReadHeader');
    @ReadHeaderEx := GetProcAddress(H, 'ReadHeaderEx');
    @ReadHeaderExW := GetProcAddress(H, 'ReadHeaderExW');
    @ProcessFile := GetProcAddress(H, 'ProcessFile');
    @ProcessFileW := GetProcAddress(H, 'ProcessFileW');
    @CloseArchive := GetProcAddress(H, 'CloseArchive');
    @SetChangeVolProc := GetProcAddress(H, 'SetChangeVolProc');
    @SetChangeVolProcW := GetProcAddress(H, 'SetChangeVolProcW');
    @SetProcessDataProc := GetProcAddress(H, 'SetProcessDataProc');
    @SetProcessDataProcW := GetProcAddress(H, 'SetProcessDataProcW');
    @GetPackerCaps := GetProcAddress(H, 'GetPackerCaps');
    @PackFiles := GetProcAddress(H, 'PackFiles');
    @PackFilesW := GetProcAddress(H, 'PackFilesW');
    @DeleteFiles := GetProcAddress(H, 'DeleteFiles');
    @DeleteFilesW := GetProcAddress(H, 'DeleteFilesW');
    @AcceptParameters := GetProcAddress(H, 'AcceptParameters');
    if @CloseArchive = nil then
     raise Exception.Create(Format(SFuncNotFound, ['CloseArchive']));
    if @GetPackerCaps = nil then
     raise Exception.Create(Format(SFuncNotFound, ['GetPackerCaps']));
    if (Option[1] = '-') and (Length(Option) = 2) then
    case UpCase(Char(Option[2])) of
     'L':
     begin
      A := _OpenArchive(PK_OM_LIST, Pointer(ArcName), Pointer(ArcNameW));
      try
       if @AcceptParameters <> nil then
        AcceptParameters(A, Pointer(SubParams));
       ListLoop(A);
      finally
       Test(CloseArchive(A));
      end;
     end;
     'E':
     begin
      A := _OpenArchive(PK_OM_EXTRACT, Pointer(ArcName), Pointer(ArcNameW));
      try
       if @AcceptParameters <> nil then
        AcceptParameters(A, Pointer(SubParams));
       SetProcessProcs(A, SExtracting);
       if SubSearch and ((List[0] = '*') or (List[0] = '*.*')) then
        ExtrAllLoop(A, DestPath, NoSubDirs) else
        ExtractLoop(A, DestPath, NoSubDirs, SubSearch, List);
      finally
       Test(CloseArchive(A));
      end;
     end;
     'A':
     begin
      if @AcceptParameters <> nil then
       AcceptParameters(0, Pointer(SubParams));
      SetProcessProcs(0, SAdding);
      if WideFileExists(ArcName) then
       Flags := PK_CAPS_MODIFY else
       Flags := PK_CAPS_NEW;
      if Length(List) > 1 then
       Flags := Flags or PK_CAPS_MULTIPLE;
      if GetPackerCaps and Flags <> 0 then
      begin
       if NoSubDirs then
        Flags := 0 else
        Flags := PK_PACK_SAVE_PATHS;
       if @PackFilesW <> nil then
       begin
        Buf := FillListBufW;
        try
         PackFilesW(Pointer(ArcNameW), Pointer(DestPath), Pointer(SrcPath), Buf, Flags);
        finally
         FreeMem(Buf);
        end;
       end else
       if @PackFiles <> nil then
       begin
        Buf := FillListBufA;
        try
         S1 := DestPath; // Unicode to ansi
         S2 := SrcPath;  // Unicode to ansi
         PackFiles(Pointer(ArcName), Pointer(S1), Pointer(S2), Buf, Flags);
        finally
         FreeMem(Buf);
        end;
       end else
        raise Exception.Create(Format(SFuncNotFound, ['PackFiles|W']));
      end else
       raise Exception.Create('Operation is not supported by this plugin');
     end;
     'R':
     begin
      if @AcceptParameters <> nil then
       AcceptParameters(0, Pointer(SubParams));     
      SetProcessProcs(0, SDeleting);
      if GetPackerCaps and PK_CAPS_DELETE <> 0 then
      begin
       if @DeleteFilesW <> nil then
       begin
        Buf := FillListBufW;
        try
         DeleteFilesW(Pointer(ArcNameW), Buf);
        finally
         FreeMem(Buf);
        end;
       end else
       if @PackFiles <> nil then
       begin
        Buf := FillListBufA;
        try
         DeleteFiles(Pointer(ArcName), Buf);
        finally
         FreeMem(Buf);
        end;
       end else
        raise Exception.Create(Format(SFuncNotFound, ['PackFiles|W']));
      end else
       raise Exception.Create('Operation is not supported by this plugin');
     end;
     else Writeln(Info);
    end else Writeln(Info);
   finally
    FreeLibrary(H);
   end else raise Exception.Create('Cannot load plugin');
  end else Writeln(Info);
 except
  on E: Exception do
  begin
   Writeln('Error: ', E.Message);
   Readln;
  end;
 end;
end.
