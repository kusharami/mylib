library alpack;

uses
  SysUtils,
  Windows,
  Classes,
  WCXheadEX,
  WCXutilsEX,
  u_alp_main;

{$E .wcx}

{$R *.res}


procedure ConfigurePacker(Parent: HWND; DLLInstance: HINST); stdcall;
begin
 // do nothing
end;

function CanYouHandleThisFile(FileName: PChar): Boolean; stdcall;
begin
 Result := CheckALPack(FileName);
end;

function CanYouHandleThisFileW(FileName: PWideChar): Boolean; stdcall;
begin
 Result := CheckALPack(FileName);
end;

exports
 OpenArchive,
 OpenArchiveW,
 ReadHeader,
 ReadHeaderEx,
 ReadHeaderExW,
 ProcessFile,
 ProcessFileW,
 CloseArchive,
 SetChangeVolProc,
 SetChangeVolProcW,
 SetProcessDataProc,
 SetProcessDataProcW,
 CanYouHandleThisFile,
 CanYouHandleThisFileW,
 GetPackerCaps,
 PackFiles,
 PackFilesW,
 DeleteFiles,
 DeleteFilesW,
 ConfigurePacker,
 AcceptParameters;

begin
 ArchiveClass := TALPackFile;
 PackerCaps :=
 PK_CAPS_NEW or
 PK_CAPS_MULTIPLE or
 PK_CAPS_MODIFY or
 PK_CAPS_DELETE or
// PK_CAPS_OPTIONS or
 PK_CAPS_SEARCHTEXT or
 PK_CAPS_BY_CONTENT or
// PK_CAPS_HIDE or
 0;
end.
 