program alzpack;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  HexUnit,
  Classes,
  ALZSS;

var
 Source, Dest: TStream;
 Mode: Integer;
begin
 if ParamCount > 2 then
 begin
  Mode := StrToIntDef(ParamStr(3), 12);
 end else Mode := 12;
 try
  Source := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyWrite);
  try
   Dest := TFileStream.Create(ParamStr(2), fmCreate);
   try
    with TALZSS_PackStream.Create(Dest, Mode) do
    try
     CopyFrom(Source, Source.Size);
     Finalize;
     Writeln('Original size: ', Source.Size);
     Writeln('Compressed size: ', Dest.Size);
     Writeln(Format('Compression rate: %.2f%%', [CompressionRate]));
    finally
     Free;
    end;
   finally
    Dest.Free;
   end;
  finally
   Source.Free;
  end;
 except
  on E: Exception do Writeln(E.Message);
 end;
end.

