program bin2pas;

{$APPTYPE CONSOLE}

uses
  SysUtils;

Const
 CTypes: Array[0..2] of String = ('Byte', 'Word', 'Integer');
 CTSize: Array[0..2] of Integer = (1, 2, 4);

Var
 F: File; T: TextFile;
 Bin, Bo: Pointer; J, I, SZ, aType, SZ2, ColWidth: Integer;
begin
 AssignFile(F, ParamStr(1));
 Reset(F, 1);
 SZ := FileSize(F);
 GetMem(Bin, SZ);
 BlockRead(F, Bin^, SZ);
 CloseFile(F);
 AssignFile(T, ParamStr(2));
 Rewrite(T);
 Val(ParamStr(4), aType, J);
 If J <> 0 then aType := 0;
 If aType > 2 then aType := 0;
 If aType < 0 then aType := 0;
 Writeln(T, 'Const ' + ParamStr(3) + ': Array[0..' + IntToStr(SZ - 1) + '] of ' + CTypes[aType] + ' = (');
 J := 0; Bo := Bin;
 SZ2 := SZ div CTSize[aType];
 Case aType of
  0, 1: ColWidth := 16;
  Else ColWidth := 8;
 end; 
 For I := 1 to SZ2 do
 begin
  Case aType of
   0: If I < SZ2 then
       Write(T, '$' + IntToHex(Byte(Bo^), 2) + ', ') Else
       Write(T, '$' + IntToHex(Byte(Bo^), 2));
   1: If I < SZ2 then
       Write(T, '$' + IntToHex(Word(Bo^), 4) + ', ') Else
       Write(T, '$' + IntToHex(Word(Bo^), 4));
   2: If I < SZ2 then
       Write(T, '$' + IntToHex(Integer(Bo^), 8) + ', ') Else
       Write(T, '$' + IntToHex(Integer(Bo^), 8));
  end;
  Inc(J);
  If J = ColWidth then
  begin
   J := 0;
   Writeln(T);
  end;
  If I = SZ2 then Writeln(T, ');');
  Inc(Integer(Bo), CTSize[aType]);
 end;
 CloseFile(T);
 FreeMem(Bin);
end.
