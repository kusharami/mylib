program crypto;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes,
  xcrypt in '..\xcrypt.pas';

var
 Buffer: Pointer;
 NewSize: Integer;
 Mode, Path, OutPath, Ext, OutName: String;
 c: Char;
 Key: Int64;
 SR: TSearchRec;
begin
 try
  mode := ParamStr(1);
  if (Length(Mode) = 1) then
   c := UpCase(Mode[1]) else
  if (Length(Mode) = 2) and (Mode[1] in ['-', '/', '\']) then
   c := UpCase(Mode[2]) else
   c := #0;

  if not (c in ['D', 'E']) then
  begin
   Writeln('-e files [output_path] [key]: encrypt');
   Writeln('-d files [output_path] [key]: decrypt');
   Exit;
  end;

  Path := ExtractFilePath(ParamStr(2));

  if (Path <> '') and (Path[Length(Path)] <> '\') then
   Path := Path + '\';

  if ParamCount >= 3 then
   OutPath := ParamStr(3) else
   OutPath := Path;

  Key := $0123456789ABCDEF;
  if ParamCount >= 4 then
   Key := StrToInt64Def(ParamStr(4), Key);

  if ParamCount < 5 then
  begin
   case c of
    'D': Ext := '.out';
    else Ext := '.enc';
   end;
  end else
   Ext := ParamStr(5);

  Buffer := nil;
  if FindFirst(ParamStr(2), $20, SR) = 0 then
  try
   repeat
    Write('Processing: "' + SR.Name + '"... ');
    with TFileStream.Create(Path + SR.Name, fmOpenRead or fmShareDenyWrite) do
    try
     ReallocMem(Buffer, Size);
     Read(Buffer^, Size);
     case c of
      'E':
      begin
       NewSize := xcrypt.GetEncryptSize(Size);
       ReallocMem(Buffer, NewSize);
       xcrypt.Encrypt(Key, Size, Buffer, Buffer);
      end;
      else
      begin
       NewSize := xcrypt.Decrypt(Key, Size, Buffer, Buffer);
       if NewSize < 0 then
       begin
        Writeln('Error!');
        Continue;
       end;
       ReallocMem(Buffer, NewSize);
      end;
     end;
     OutName := OutPath + ChangeFileExt(SR.Name, Ext);
     with TFileStream.Create(OutName, fmCreate) do
     try
      WriteBuffer(Buffer^, NewSize);
      Writeln('Done!');
     finally
      Free;
     end;
    finally
     Free;
    end;
   until FindNext(SR) <> 0;
  finally
   FindClose(SR);
   FreeMem(Buffer);
  end;
 except
  on E: Exception do Writeln(E.Message);
 end;
end.
