function GBA_LZ77_Compress(const Source; var Dest;
          SrcLen: Integer; Mode: TLZ77CmprMode): Integer;
var
 I, J, LastRingPos: Integer;
 Dst, Src: PByte;
 DstD: PCardinal absolute Dst;
 FlagIndex: Integer;
 FlagsPtr: PByte;
 WindowSize, VSafe, RingPos: Integer;
 MatchLength: Integer;
 SearchPos: Integer;
 SrcSave: PByte;
 W: WordRec;
 RingBuf: array[0..RING_SIZE - 1] of Byte;
 Links: array[0..RING_SIZE - 1] of SmallInt;
 LastLetter: array[Byte] of SmallInt;
begin
 Src := @Source;
 Dst := @Dest;
 VSafe := Integer(Mode = lzVRAM_Safe) and 1;
 Inc(SrcLen, SrcLen and VSafe);
 if (Src <> NIL) and (Dst <> NIL) and (SrcLen >= 0) and (SrcLen <= $FFFFFF) then
 begin
  DstD^ := LZ77_SIGN or (Cardinal(SrcLen) shl 8);
  Inc(DstD);
  RingPos := 0;
  WindowSize := 0;
  Result := 4;
  LastRingPos := 0;
  FlagIndex := 7;
  for I := 0 to RING_SIZE - 1 do Links[I] := I;
  FillChar(LastLetter, SizeOf(LastLetter), $FF);
  while SrcLen > 0 do
  begin
   if WindowSize = 0 then MatchLength := 1 else
   begin
    if SrcLen < MATCH_LIMIT then
     MatchLength := SrcLen else
     MatchLength := MATCH_LIMIT;
   end;
   SearchPos := (RingPos - WindowSize) - 1;
   SrcSave := Src;
   Inc(SrcSave, LastRingPos);
   for I := LastRingPos to MatchLength - 1 do
   begin
    RingBuf[(RingPos + I) and (RING_SIZE - 1)] := SrcSave^;
    Inc(SrcSave);
   end;
   asm
        push    ebx
        mov     ebx,[MatchLength]
        mov     [LastRingPos],ebx
        xor     eax,eax
        mov     [MatchLength],eax
        cmp     ebx,THRESHOLD
        jle     @@Finish
        mov     ecx,[WindowSize]
        sub     ecx,[VSafe]
        cmp     ecx,0
        jle     @@Finish
        mov     eax,[Src]
        mov     bh,byte ptr [eax]
        movzx   edx,bh
        push    esi
        push    edi
        mov     eax,[SearchPos]
        add     eax,ecx
        and     eax,RING_SIZE - 1
        lea     edi,[RingBuf + eax]
        mov     dx,word ptr [LastLetter + edx * 2]
        cmp     dx,$FFFF
        je      @@ExitLoop 
        cmp     edx,eax
        je      @@Skip
        mov     esi,eax
        sub     esi,edx
        and     esi,RING_SIZE - 1
        mov     edi,ecx
        sub     edi,esi
        cmp     edi,0
        jle     @@SearchPreLoop
   @@Search:
        mov     ecx,edi
        mov     eax,edx
        lea     edi,[RingBuf + eax]
        jmp     @@Skip
        @@SearchPreLoop:
                lea     edi,[RingBuf + eax]
                cmp     byte ptr [edi],bh
                je      @@Skip
                dec     eax
                and     eax,RING_SIZE - 1
        loop    @@SearchPreLoop
        pop     edi
        pop     esi
        jmp     @@Finish
        @@SearchLoop:
                lea     edi,[RingBuf + eax]
                cmp     byte ptr [edi],bh
                jne     @@Continue
                mov     word ptr [Links + edx * 2],ax
        @@Skip:
                movzx   edx,bl
                push    ecx
                mov     ecx,eax
                add     ecx,edx
                dec     ecx
                and     ecx,RING_SIZE - 1
                mov     esi,[Src]
                cmp     ecx,eax
                jb      @@DoubleCompare
                mov     ecx,edx
                repe    cmpsb
                je      @@Found
        @@NotFound:
                sub     edx,ecx
                dec     edx
                cmp     edx,[MatchLength]
                jle     @@Proceed
                mov     [MatchLength],edx
                mov     [SearchPos],eax
         @@Proceed:
                mov     edx,eax
                pop     ecx
                movzx   edi,word ptr [Links + eax * 2]
                cmp     edi,eax
                jne     @@Boost
        @@Continue:
                dec     eax
                and     eax,RING_SIZE - 1
        loop    @@SearchLoop
        jmp     @@ExitLoop
        @@Boost:
                mov     esi,eax
                sub     esi,edi
                and     esi,RING_SIZE - 1
                sub     ecx,esi
                cmp     ecx,0
                jle     @@ExitLoop
                inc     ecx
                mov     eax,edi
                lea     edi,[RingBuf + eax]
        loop    @@Skip
   @@ExitLoop:
        pop     edi
        pop     esi
        jmp     @@Finish
        @@NotFound2:
                mov     edx,[Src]
                sub     esi,edx
                dec     esi
                cmp     esi,[MatchLength]
                jle     @@Proceed
                mov     [MatchLength],esi
                mov     [SearchPos],eax
        jmp     @@Proceed
        @@DoubleCompare:
                push    ecx
                mov     ecx,RING_SIZE
                sub     ecx,eax
                repe    cmpsb
                pop     ecx
                jne     @@NotFound2
                inc     ecx
                lea     edi,[RingBuf]
                repe    cmpsb
                jne     @@NotFound
   @@Found:
        pop     ecx
        mov     [SearchPos],eax
        movzx   ebx,bl
        mov     [MatchLength],ebx
        pop     edi
        pop     esi
    @@Finish:
        pop     ebx
   end;
   if FlagIndex = 7 then
   begin
    FlagsPtr := Dst;
    FlagsPtr^ := 0;
    Inc(Dst);
    Inc(Result);
   end;
   SrcSave := Src;
   if MatchLength > THRESHOLD then
   begin
    Word(W) := ((MatchLength - (THRESHOLD + 1)) shl 12) or
         ((RingPos - SearchPos - 1) and (RING_SIZE - 1));
    Dst^ := W.Hi;
    Inc(Dst);
    Dst^ := W.Lo;
    Inc(Dst);
    Inc(Result, 2);
    FlagsPtr^ := FlagsPtr^ or (1 shl FlagIndex);
    Inc(Src, MatchLength);
    Dec(SrcLen, MatchLength);
    Dec(FlagIndex);
   end else
   begin
    MatchLength := 1;
    Dst^ := Src^;
    Inc(Dst);
    Inc(Result);
    Inc(Src);
    Dec(SrcLen);
    Dec(FlagIndex);
   end;
   for I := 0 to MatchLength - 1 do
   begin
    J := (RingPos + I) and (RING_SIZE - 1);
    Links[J] := J;
    LastLetter[SrcSave^] := J;
    Inc(SrcSave);
   end;
   RingPos := (RingPos + MatchLength) and (RING_SIZE - 1);
   Dec(LastRingPos, MatchLength);
   if FlagIndex < 0 then FlagIndex := 7;
   if WindowSize < RING_SIZE - MATCH_LIMIT then
   begin
    Inc(WindowSize, MatchLength);
    if WindowSize > RING_SIZE - MATCH_LIMIT then
     WindowSize := RING_SIZE - MATCH_LIMIT;
   end;
  end;
  if Result and 3 > 0 then Result := Result and not 3 + 4;
 end else Result := 0;
end;
