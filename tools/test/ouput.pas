unit gba_lz77;

interface

uses SysUtils, Classes, MyClasses;

const
 RING_SIZE = 4096;
 MATCH_LIMIT = 18;
 THRESHOLD = 2;
 NULL = RING_SIZE;

type
 TUInt24 = packed record
  A: Byte;
  B: Byte;
  C: Byte;
 end;

 TGBA_LZ77_Header = packed record
  glhSignature: Byte; //$10
  glhSize: TUInt24;
 end;

 PLZ77_Tree = ^TLZ77_Tree;
 TLZ77_Tree = object
  public
    VRAM_Safe: LongBool;
    MatchPosition: Integer;
    MatchLength: Integer;
    LSON: array[0..RING_SIZE] of SmallInt;
    RSON: array[0..RING_SIZE + 256] of SmallInt;
    DAD: array[0..RING_SIZE] of SmallInt;
    TextBuf: array[0..RING_SIZE + MATCH_LIMIT - 2] of Byte;
    constructor Create(AVRAM_Safe: Boolean);
    procedure InsertNode(Node: Integer);
    procedure DeleteNode(Node: Integer);
    destructor Destroy;
 end;

 TDecompState = (dsCheckFlagsByte, dsSimpleCopy, dsDecompress,
                 dsSubDecompress, dsFinished);

 TDecompData = packed record
  State: TDecompState;
  SrcBits: Byte;
  RingPos: Integer;
  LastCycle: Integer;
  case TDecompState of
   dsDecompress:    (SkipFlagsRead: Boolean;
                     LastFlags: Word);
   dsSubDecompress: (LastSubCycle: Integer;
                     LastLength: Integer;
                     LastOffset: Integer);
 end;

 TGBA_LZ77_Decompressor = class(TCustomPackStream)
  private
    FDecompData: TDecompData;
    FRingBuf: array [0..RING_SIZE - 1] of Byte;
    FHeader: TGBA_LZ77_Header;
    function Decompress: Integer;
  protected
    procedure SetSize(NewSize: Integer); override;
    function GetSize: Int64; override;      
    procedure Reset; override;
  public
    property CompressedSize: Integer read FTotalIn;
    property OnProgress;

    constructor Create(Source: TStream);
    destructor Destroy; override;
    function Read(var Buffer; Count: Integer): Longint; override;
    function Write(const Buffer; Count: Integer): Longint; override;
    function Seek(Offset: Integer; Origin: Word): Longint; override;
 end;

function GetDecompressedSize(const Source): Integer;
function Compress(const Source; var Dest;
                       SrcLen: Integer; VRAM_Safe: Boolean): Integer;
function Decompress(const Source; var Dest): Integer;


implementation

function Compress(const Source; var Dest;
          SrcLen: Integer; VRAM_Safe: Boolean): Integer;
var
 I, C, Len, R, S: Integer;
 LastMatchLength: Integer;
 FlagPtr: PByte;
 Mask: Byte;
 Dst, Src: PByte;
 DstW: PWord absolute Dst;
 DstD: PCardinal absolute Dst;
 Tree: PLZ77_Tree;
begin
 Src := @Source;
 Dst := @Dest;
 if (Src <> NIL) and (Dst <> NIL) and
    (SrcLen >= 0) and (SrcLen <= $FFFFFF) then
 begin
  DstD^ := $10 + Cardinal(SrcLen) shl 8;
  Inc(DstD);
  Result := 4;
  S := 0;
  R := RING_SIZE - MATCH_LIMIT;
  Mask := 0;
  FlagPtr := NIL;
  New(Tree, Create(VRAM_Safe));
  with Tree^ do
  begin
   FillChar(TextBuf[0], R, $E5);
   for Len := 0 to MATCH_LIMIT - 1 do
   begin
    if SrcLen = 0 then Break;
    TextBuf[R + Len] := Src^;
    Inc(Src);
    Dec(SrcLen);
   end;
   for I := 1 to MATCH_LIMIT do
    InsertNode(R - I);
   InsertNode(R);
   repeat
    if MatchLength > Len then MatchLength := Len;
    if Mask = 0 then
    begin
     FlagPtr := Dst;
     Dst^ := 0;
     Inc(Dst);
     Inc(Result);
     Mask := 128;
    end;
    if MatchLength <= THRESHOLD then
    begin
     MatchLength := 1;
     Dst^ := TextBuf[R];
     Inc(Dst);
     Inc(Result);
    end Else
    begin
     FlagPtr^ := FlagPtr^ or mask;
     Dst^ := (((R - MatchPosition - 1) shr 8) and 15) or
	      ((MatchLength - (THRESHOLD + 1)) shl 4);
     Inc(Dst);
     Dst^ := (R - MatchPosition - 1) and 255;
     Inc(Dst);
     Inc(Result, 2);
    end;
    Mask := Mask shr 1;
    LastMatchLength := MatchLength;
    for I := 0 to LastMatchLength - 1 do
    begin
     if SrcLen <= 0 then Break;
     DeleteNode(S);
     C := Src^;
     Inc(Src);
     TextBuf[S] := C;
     Dec(SrcLen);
     if S < MATCH_LIMIT - 1 then
      TextBuf[S + RING_SIZE] := C;
     S := (S + 1) and (RING_SIZE - 1);
     R := (R + 1) and (RING_SIZE - 1);
     InsertNode(R);
    end;
    while I < LastMatchLength do
    begin
     DeleteNode(S);
     S := (S + 1) and (RING_SIZE - 1);
     R := (R + 1) and (RING_SIZE - 1);
     Dec(Len);
     if Len > 0 then InsertNode(R);
     Inc(I);
    end;
   until Len <= 0;
  end;
  Dispose(Tree, Destroy);
  while Result and 3 > 0 do
  begin
   Dst^ := 0;
   Inc(Dst);
   Inc(Result);
  end;
 end else Result := 0;
end;

function GetDecompressedSize(const Source): Integer;
var
 Data: TGBA_LZ77_Header absolute Source;
begin
 if (@Source <> NIL) and (Data.glhSignature = $10) then
  Result := Integer(Data) shr 8 else
  Result := 0;
end;

function Decompress(const Source; var Dest): Integer;
var
 Src, Dst: PByte;
 I, Lng, Offset: Integer;
 D: Byte; Data: Word;
 UncompSize: Integer;
begin
 Src := @Source;
 Dst := @Dest;
 Result := -1;
 if (Src^ <> $10) or (Src = NIL) or (Dst = NIL) then Exit;
 Result := Integer(Pointer(Src)^) shr 8;
 UncompSize := Result;
 Inc(Src, 4);
 while UncompSize > 0 do
 begin
  D := Src^; Inc(Src);
  if D <> 0 then
  begin
   for I := 0 to 7 do
   begin
    if D and $80 <> 0 then
    begin
     Data := Src^ shl 8;
     Inc(Src);
     Data := Data or Src^;
     Inc(Src);
     Lng := (Data shr 12) + 3;
     Offset := Data and $FFF;
     Dec(UncompSize, Lng);
     if UncompSize < 0 then Inc(Lng, UncompSize);
     Move(Pointer(Integer(Dst) - Offset - 1)^, Dst^, Lng);
     Inc(Dst, Lng);
     if UncompSize <= 0 then Exit;
    end else
    begin
     Dst^ := Src^;
     Inc(Dst);
     Inc(Src);
     Dec(UncompSize);
     If UncompSize = 0 then Exit;
    end;
    D := D shl 1;
   end;
  end else
  begin
   Dec(UncompSize, 8);
   if UncompSize < 0 then
    Lng := 8 + UncompSize else
    Lng := 8;
   Move(Src^, Dst^, Lng);
   Inc(Src, 8);
   Inc(Dst, 8);
  end;
 end;
end;

constructor TLZ77_Tree.Create(AVRAM_Safe: Boolean);
//{$IFDEF PUREPASCAL}
{
var
 I: Integer;
 W: PSmallInt;
begin
 VRAM_Safe := AVRAM_Safe;
 MatchPosition := 0;
 MatchLength := 0;
 I := RING_SIZE + 1;
 W := Addr(RSON[I]);
 While I <= RING_SIZE + 256 do
 begin
  W^ := NULL;
  Inc(W);
  Inc(I);
 end;
 I := 0;
 W := Addr(DAD[0]);
 While I < RING_SIZE do
 begin
  W^ := NULL;
  Inc(W);
  Inc(I);
 end;
end;}
//{$ELSE}
asm
 mov    edx,eax
 and    ecx,1
 mov    dword ptr [edx + offset VRAM_Safe],ecx
 xor    ecx,ecx
 mov    [edx + offset MatchPosition],ecx
 mov    [edx + offset MatchLength],ecx
 push   edi
 lea    edi,[edx + offset RSON + (RING_SIZE + 1) * 2]
 mov    eax,NULL
 mov    ecx,255
 rep    stosw
 lea    edi,[edx + offset DAD]
 mov    ecx,eax
 rep    stosw
 pop    edi
 mov    eax,edx
end;
//{$ENDIF}

procedure TLZ77_Tree.DeleteNode(Node: Integer);
var
 Q: Integer;
begin
 if DAD[Node] = NULL then Exit;
 if RSON[Node] = NULL then Q := LSON[Node] else
 if LSON[Node] = NULL then Q := RSON[Node] else
 begin
  Q := LSON[Node];
  if RSON[Q] <> NULL then
  begin
   repeat Q := RSON[Q] until RSON[Q] = NULL;
   RSON[DAD[Q]] := LSON[Q];
   DAD[LSON[Q]] := DAD[Q];
   LSON[Q] := LSON[Node];
   DAD[LSON[Node]] := Q;
  end;
  RSON[Q] := RSON[Node];
  DAD[RSON[Node]] := Q;
 end;
 DAD[Q] := DAD[Node];
 if RSON[DAD[Node]] = Node then
  RSON[DAD[Node]] := Q else
  LSON[DAD[Node]] := Q;
 DAD[Node] := NULL;
end;

destructor TLZ77_Tree.Destroy;
begin
//do nothing
end;

procedure TLZ77_Tree.InsertNode(Node: Integer);
type
 TByteArray = array[Byte] of Byte;
var
 I, P, CMP: Integer;
 Key: ^TByteArray;
begin
 CMP := 1;
 Key := Addr(TextBuf[Node]);
 P := RING_SIZE + 1 + Key^[0];
 RSON[Node] := NULL;
 LSON[Node] := NULL;
 MatchLength := 0;
 while True do
 begin
  If CMP >= 0 then
  begin
   if RSON[P] <> NULL then P := RSON[P] else
   begin
    RSON[P] := Node;
    DAD[Node] := P;
    Exit;
   end;
  end else
  begin
   if LSON[P] <> NULL then P := LSON[P] else
   begin
    LSON[P] := Node;
    DAD[Node] := P;
    Exit;
   end;
  end;
  for I := 1 to MATCH_LIMIT - 1 do
  begin
   CMP := Key^[I] - TextBuf[P + I];
   if CMP <> 0 then Break;
  end;
  if (I > MatchLength) and not (VRAM_Safe and
     ((Node - P) and (RING_SIZE - 1) <= 1)) then
  begin
   MatchPosition := P;
   MatchLength := I;
   if MatchLength >= MATCH_LIMIT then Break;
  end;
 end;
 DAD[Node] := DAD[p];
 LSON[Node] := LSON[p];
 RSON[Node] := RSON[p];
 DAD[LSON[p]] := Node;
 DAD[RSON[p]] := Node;
 if RSON[DAD[P]] = P then
  RSON[DAD[P]] := Node else
  LSON[DAD[P]] := Node;
 DAD[P] := NULL;
end;

constructor TGBA_LZ77_Decompressor.Create(Source: TStream);
begin
 inherited;
 FCompression := False;
end;

function TGBA_LZ77_Decompressor.Decompress: Integer;
var
 Remain, I, J, L, Ofs: Integer;
 B: Byte; Data: Word;
label
 LExit;
begin
 Remain := FRemainOut;
 with FDecompData do
 repeat
  case State of
   dsCheckFlagsByte:
   begin
    SrcBits := FInput^;
    Inc(FInput);
    Inc(FTotalIn);
    Dec(FRemainIn);
    if SrcBits = 0 then
     State := dsSimpleCopy else
     State := dsDecompress;
    if FRemainIn = 0 then Break;
   end;
   dsSimpleCopy:
   begin
    for I := LastCycle to 7 do
    begin
     B := FInput^;
     FOutput^ := B;
     FRingBuf[RingPos] := B;
     RingPos := (RingPos + 1) and (RING_SIZE - 1);
     Inc(FInput);
     Inc(FOutput);
     Inc(FTotalIn);
     Inc(FTotalOut);
     Dec(FRemainIn);
     Dec(FTotalRemain);
     Dec(Remain);
     if FTotalRemain = 0 then
     begin
      State := dsFinished;
      Break;
     end;
     if (I < 7) and ((Remain = 0) or (FRemainIn = 0)) then
     begin
      LastCycle := I + 1;
      goto LExit;
     end;
    end;
    LastCycle := 0;
    State := dsCheckFlagsByte;
   end;
   dsDecompress:
   begin
    for I := LastCycle to 7 do
    begin
     if SrcBits and $80 = 0 then
     begin
      if SkipFlagsRead then
      begin
       Data := LastFlags;
       SkipFlagsRead := False;
      end else
      begin
       Data := FInput^ shl 8;
       Inc(FInput);
       Inc(FTotalIn);
       Dec(FRemainIn);
       if FRemainIn = 0 then
       begin
        LastCycle := I;
        SkipFlagsRead := True;
        LastFlags := Data;
        goto LExit;
       end;
      end;
      Data := Data or FInput^;
      Inc(FInput);
      Inc(FTotalIn);
      Dec(FRemainIn);
      L := (Data shr 12) + THRESHOLD;
      Ofs := Data and (RING_SIZE - 1);
      for J := 0 to L do
      begin
       B := FRingBuf[(RingPos - Ofs - 1) and (RING_SIZE - 1)];
       FOutput^ := B;
       FRingBuf[RingPos] := B;
       RingPos := (RingPos + 1) and (RING_SIZE - 1);
       Inc(FTotalOut);
       Dec(FTotalRemain);
       Dec(Remain);
       if FTotalRemain = 0 then Break;
       if (J < L) and (Remain = 0) then
       begin
        LastSubCycle := J + 1;
        LastLength := L;
        LastOffset := Ofs;
        State := dsSubDecompress;
        Break;
       end;
      end;
     end else
     begin
      B := FInput^;
      FOutput^ := B;
      FRingBuf[RingPos] := B;
      RingPos := (RingPos + 1) and (RING_SIZE - 1);
      Inc(FInput);
      Inc(FOutput);
      Inc(FTotalIn);
      Inc(FTotalOut);
      Dec(FRemainIn);
      Dec(FTotalRemain);
      Dec(Remain);
     end;
     SrcBits := SrcBits shl 1;
     if FTotalRemain = 0 then
     begin
      State := dsFinished;
      goto LExit;
     end;
     if (I < 7) and ((Remain = 0) or (FRemainIn = 0)) then
     begin
      LastCycle := I + 1;
      goto LExit;
     end;
    end;
    LastCycle := 0;
    State := dsCheckFlagsByte;
   end;
   dsSubDecompress:
   begin
    Ofs := LastOffset;
    for I := LastSubCycle to LastLength do
    begin
     B := FRingBuf[(RingPos - Ofs - 1) and (RING_SIZE - 1)];
     FOutput^ := B;
     FRingBuf[RingPos] := B;
     RingPos := (RingPos + 1) and (RING_SIZE - 1);
     Inc(FTotalOut);
     Dec(FTotalRemain);
     Dec(Remain);
     if FTotalRemain = 0 then
     begin
      State := dsFinished;
      Break;
     end;
     if (I < LastLength) and (Remain = 0) then
     begin
      LastSubCycle := I + 1;
      goto LExit;
     end;
    end;
    if LastCycle > 0 then
     State := dsDecompress else
     State := dsCheckFlagsByte;
    LastSubCycle := 0;
    LastLength := 0;
    LastOffset := 0;
   end;
   dsFinished:
   begin
    Result := 0;
    Exit;
   end;
  end;
 until False;
LExit:
 Result := FRemainOut - Remain;
 FRemainOut := Remain;
end;

destructor TGBA_LZ77_Decompressor.Destroy;
begin
 inherited;
end;

function TGBA_LZ77_Decompressor.GetSize: Int64;
begin
 Result := Integer(FHeader) shr 8;
end;

function TGBA_LZ77_Decompressor.Read(var Buffer; Count: Integer): Longint;
begin
 FOutput := @Buffer;
 FRemainOut := Count;
 FStream.Position := FStreamPosition;
 Result := 0;
 while FRemainOut > 0 do
 begin
  FRemainIn := FStream.Read(FBuffer, PackBufSize);
  if FRemainIn = 0 then
  begin
   Result := Count - FRemainOut;
   Exit;
  end;
  Inc(FStreamPosition, FRemainIn);
  FInput := @FBuffer;
  Inc(Result, Decompress);
  Progress(Self);
 end;
end;

procedure TGBA_LZ77_Decompressor.Reset;
begin
 FStream.Position := FStartPosition;
 FStream.ReadBuffer(FHeader, SizeOf(TGBA_LZ77_Header));
 if FHeader.glhSignature <> $10 then
  raise EDecompressionError.Create('Header is invalid');
 FStreamPosition := FStartPosition + SizeOf(TGBA_LZ77_Header);  
 FTotalRemain := Integer(FHeader) shr 8;
 FTotalIn := 0;
 FTotalOut := 0;
 if FTotalRemain = 0 then FDecompData.State := dsFinished else
 begin
  FDecompData.RingPos := RING_SIZE - MATCH_LIMIT;
  FDecompData.State := dsCheckFlagsByte;
  FillChar(FRingBuf, SizeOf(FRingBuf), $E5);
 end;
end;

function TGBA_LZ77_Decompressor.Seek(Offset: Integer; Origin: Word): Longint;
var
 Remain, L: Integer;
begin
 L := Integer(FHeader) shr 8;
 case Origin of
  soFromBeginning: Result := Offset;
  soFromCurrent: Result := FTotalOut + Offset;
  soFromEnd: Result := L + Offset;
  else Result := 0;
 end;
 if Result = 0 then (* Restarting decompression *) Reset else
 if Result > L then raise EDecompressionError.Create('Invalid stream operation') else
 if Result > FTotalOut then
 begin
  Remain := FTotalOut - Result;
  with FDecompData do
  begin
   L := RING_SIZE - RingPos;
   while Remain > 0 do
   begin
    if L > Remain then L := Remain;   
    ReadBuffer(FRingBuf[RingPos], L);
    Dec(Remain, L);
    L := RING_SIZE;
   end;
  end;
 end;
end;

procedure TGBA_LZ77_Decompressor.SetSize(NewSize: Integer);
begin
 raise EDecompressionError.Create('Invalid stream operation');
end;

function TGBA_LZ77_Decompressor.Write(const Buffer; Count: Integer): Longint;
begin
 raise EDecompressionError.Create('Invalid stream operation');
end;

end.
