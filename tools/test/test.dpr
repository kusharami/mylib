program test;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes, DIB,
  gba,
  GbaUnit, MyClasses;
var
 DS, Input: TStream; Pic: TDIB;
// Mem, Buf: TMemoryStream;
 //Freqs: array of Cardinal;
 //Tree: THuffmanTree;
 //I: Integer;
// Input, Output: TFileStream; CS: TCustomPackStream;
begin
{ Input := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyWrite);
 try
  Output := TFileStream.Create(ParamStr(2), fmCreate);
  try
   CS := TRLE_PackStream.Create(Output);
   try
    CS.CopyFrom(Input, Input.Size);
   finally
    CS.Free;
   end;
  except
   Output.Free;
   DeleteFile(ParamStr(2));
   raise;
  end;
  Output.Free;
 finally
  Input.Free;
 end; }
{ Mem := TMemoryStream.Create;
 try
  Mem.LoadFromFile(ParamStr(1));
  Buf := TMemoryStream.Create;
  try
   Buf.Size := Mem.Size * 2;
   Buf.Size := GBA_LZ77_Compress(Mem.Memory^, Buf.Memory^, Mem.Size, lzNormal);
//   Buf.Size := Mem.Size * 2;
//   Buf.Size := GBA_HuffmanCompress(Mem.Memory^, Buf.Memory^, Mem.Size, hf8bit);
//   Buf.Size := Mem.Size * 2;
//   Buf.Size := GBA_RLE_Compress(Mem.Memory^, Buf.Memory^, Mem.Size);
//   Buf.Size := GBA_RLE_Check(Mem.Memory^);
//   GBA_RLE_Decompress(Mem.Memory^, Buf.Memory^);
   Buf.SaveToFile(ParamStr(2));
  finally
   Buf.Free;
  end;
 finally
  Mem.Free;
 end;  }
 Input := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyWrite);
 try
  DS := TLZH_UnpackStream.Create(Input);
  try
   Pic := TDIB.Create;
   try
    Pic.LoadFromStream(DS);
    Pic.SaveToFile(ParamStr(2));
   finally
    Pic.Free;
   end;
  finally
   DS.Free;
  end;
 finally
  Input.Free;
 end;
end.

