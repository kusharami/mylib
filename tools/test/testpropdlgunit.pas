unit testpropdlgunit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PropDialog, PropContainer, StdCtrls, TilesUnit, MyClassesEx;

type
  TForm1 = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

var
 List: TPropertyList;
begin
 List := TPropertyList.Create;
 List.AddString('TestStr', 'Value');
 List.AddHexadecimal('TestHex', 55, Low(Byte), High(Byte), 2);
 List.AddDecimal('TestDec', 666, 0, High(Integer));
 if PropertyDialog('Test', List) then
 begin
 end;
 List.Free;
end;


end.
