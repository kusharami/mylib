program unzlib;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes, ZLibEx;

var
 F: TFileStream;
 D: TStream;
 I: Integer;
begin
 for I := $20 to $100 do
 try
  F := TFileStream.Create(ParamStr(1), fmOpenRead or fmShareDenyWrite);
  try
   F.Position := I;
   Write(I, ':');
   D := TZDecompressionStream.Create(F);
   try
    with TFileStream.Create(ParamStr(2), fmCreate) do
    try
     CopyFrom(D, $10);
     Writeln('Success!!');
    finally
     Free;
    end;
   finally
    D.Free;
   end;
  finally
   F.Free;
  end;
 except
  on E: Exception do Writeln(E.Message);
 end;
end.
