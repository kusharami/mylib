unit u_ahp_main;

interface

uses
 Windows, SysUtils, HexUnit, WcxHeadEx, MyUtils, Classes,
 Archiver, MyClasses, Compression, ALZSS;

type
 TAHPackFile = class(TCustomArchive)
  private
   FPakFileName: WideString;
   FNamesPrefix: AnsiString;
   TreeBuffer: array of WordRec;
   LeftBuffer: array of Byte;
   RightBuffer: array of Byte;
  protected
   procedure Initialize; override;
   function GetDataFileName: WideString; override;
   function Load(Source: TStream): Integer; override;
   function Save(Source, Dest: TStream): Integer; override;
  public
   MasterKey: Word;
   ExtraFields: array of LongInt;
   function InitStream: TStream; override;
   procedure ApplyParameters; override;
 end;

 TAHPackItem = class(TArchiveItem)
  public
    function CopyData(Input, Output: TStream; Size: Int64): Integer; override;
    function Decompress(Input, Output: TStream): Integer; override;
    function Compress(Input, Output: TStream): Integer; override;
 end;

const
 AHFNameSet: TSysCharSet = ['_', '0'..'9', 'A'..'Z', 'a'..'z'];
 LstHeader: AnsiString = '*AH PACK file list'#13#10;
 PrefixHeader: AnsiString = '*Prefix: ';
 ID_AH = Ord('A') or (Ord('H') shl 8);
 MODE_LZSS = 1 shl 3;
 MASTER_KEY = 0;
 MASTER_KEY32 = MASTER_KEY or (MASTER_KEY shl 16);
 BEEF = $EFBE;
 AHF_BIG_ENDIAN = 1 shl 3;
 AHF_FLAGS_FIELD = 1 shl 2;
 AHF_AUTO_LIST_FMT = 1 shl 15;
 AHF_NO_LZSS = 1 shl 14;
 AHF_FORCE_LZSS = 1 shl 13;
 DEFAULT_FLAGS = (12 shl 8) or AHF_AUTO_LIST_FMT;
 LIST_FMT_MASK = 3;
 LIST_FMT_3232 = 0; // Offset32, Size32
 LIST_FMT_1616 = 1; // Offset16, Size16
 LIST_FMT_0824 = 2; // Size8, Offset24
 LIST_FMT_3216 = 3; // Offset32 Table, Size16 Table

type
 TAH_MainHeader = packed record
  mhID: Word;   // AH
  mhKey: Word;  // Each next word xored with (mhKey xor $BEEF)
  mhFlags: Byte;
  mhExtraFields: Byte; // specify how much 32-bit extra fields
  // First extra field always points to file flag table: 1 byte for each file
  mhCount: Word;
 end;

 TAH_PackHeader = packed record // Xored with (mhKey xor $BEEF)
  phID: Word; // AH
  phNodeCnt: Word;
  phTotalBits: LongWord;
 end;

 TAH_FileHeader = packed record // Xored with (mhKey xor $BEEF)
  fhBitOffset: LongWord;
  fhFileSize: LongWord;
 end;

function CheckAHPack(const FileName: WideString): Boolean;

implementation

function CheckAHPack(const FileName: WideString): Boolean;
var
 Str: AnsiString;
 H: THandle;
begin
 Result := False;
 try
  H := CreateFileW(PWideChar(FileName), GENERIC_READ,
                   FILE_SHARE_READ, nil, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL, 0);
  if H <> INVALID_HANDLE_VALUE then
  try
   with THandleStream.Create(H) do
   try
    SetLength(Str, Length(LstHeader));
    ReadBuffer(Pointer(Str)^, Length(LstHeader));
    Result := Str = LstHeader;
   finally
    Free;
   end;
  finally
   FileClose(H);
  end;
 except
 end;
end;

function CutStrSlice(Chop: AnsiString; var Str: AnsiString): AnsiString;
var
 Len: Integer;
begin
 Len := Pos(Chop, Str) - 1;
 if Len >= 0 then
 begin
  Result := Trim(Copy(Str, 1, Len));
  Delete(Str, 1, Len + Length(Chop));
 end else
 begin
  Result := Str;
  Str := '';
 end;
end;

function CutStrSliceW(Chop: WideString; var Str: WideString): WideString;
var
 Len: Integer;
begin
 Len := Pos(Chop, Str) - 1;
 if Len >= 0 then
 begin
  Result := Trim(Copy(Str, 1, Len));
  Delete(Str, 1, Len + Length(Chop));
 end else
 begin
  Result := Str;
  Str := '';
 end;
end;

{ TAHPackFile }

function TAHPackFile.GetDataFileName: WideString;
begin
 Result := FPakFileName;
end;

procedure TAHPackFile.Initialize;
begin
 MasterKey := MASTER_KEY;
 Flags := DEFAULT_FLAGS;
 inherited;
 FPakFileName := WideChangeFileExt(ArchiveName, '.ahp');
 ItemClass := TAHPackItem;
 FNamesPrefix := 'fi';
end;

function TAHPackFile.InitStream: TStream;
var
 List: TAnsiStringList;
 Str: AnsiString;
 Stream: TStream;
begin
 try
  List := TAnsiStringList.Create;
  try
   FPakFileName := ArchiveName;
   Stream := inherited InitStream;
   FPakFileName := '';
   if Stream <> nil then
   try
    SetLength(Str, Length(LstHeader));
    Stream.ReadBuffer(Pointer(Str)^, Length(LstHeader));
    Stream.Position := 0;
    if Str = LstHeader then
    begin
     List.LoadFromStream(Stream);
     if List.Count > 1 then
     begin
      Str := Trim(List[1]);
      if Str[1] = '*' then
      begin
       Delete(Str, 1, 1);
       FPakFileName := ExpandFileNameEx(WideExtractFilePath(ArchiveName), Str);
       if not FileExists(FPakFileName) then
        FPakFileName := '';
      end;
     end;
    end;
   finally
    DoneStream(Stream);
   end;
   if FPakFileName = '' then
    FPakFileName := WideChangeFileExt(ArchiveName, '.ahp');
   Result := inherited InitStream;
  finally
   List.Free;
  end;
 except
  Result := nil;
 end;
end;

function TAHPackFile.Load(Source: TStream): Integer;
var
 PtrLst: array of TAH_FileHeader;
function LoadPtrList(Key: Word; Cnt, Flags: Integer): Integer;
var
 Lst: array of LongWord;
 ShortLst: array of Word;
 I, X: Integer; Key32: LongWord;
begin
 Key32 := Key or (Key shl 16);
 SetLength(PtrLst, Cnt);
 case Flags and LIST_FMT_MASK of
  LIST_FMT_3232:
  begin
   Result := Cnt * SizeOf(TAH_FileHeader);
   if Source.Read(PtrLst[0], Result) <> Result then
    Result := -1 else
   if Flags and AHF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    fhBitOffset := SwapLong(fhBitOffset xor Key32);
    fhFileSize := SwapLong(fhFileSize xor Key32);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    fhBitOffset := fhBitOffset xor Key32;
    fhFileSize := fhFileSize xor Key32;
   end;
  end;
  LIST_FMT_1616:
  begin
   SetLength(Lst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   if Flags and AHF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    fhBitOffset := SwapWord(Lo xor Key);
    fhFileSize := SwapWord(Hi xor Key);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    fhBitOffset := Lo xor Key;
    fhFileSize := Hi xor Key;
   end;
  end;
  LIST_FMT_0824:
  begin
   SetLength(Lst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   if Flags and AHF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    X := SwapLong(Lst[I] xor Key32);
    fhBitOffset := X shr 8;
    fhFileSize := Byte(X);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    X := Lst[I] xor Key32;
    fhBitOffset := X shr 8;
    fhFileSize := Byte(X);
   end;
  end;
  else // LIST_FMT_3216:
  begin
   SetLength(Lst, Cnt);
   SetLength(ShortLst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   begin
    Result := Cnt * 2;
    if Source.Read(ShortLst[0], Result) <> Result then
     Result := -1 else
    begin
     Inc(Result, Cnt * 4);
     if Flags and AHF_BIG_ENDIAN <> 0 then
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhBitOffset := SwapLong(Lst[I] xor Key32);
      fhFileSize := SwapWord(ShortLst[I] xor Key);
     end else
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhBitOffset := Lst[I] xor Key32;
      fhFileSize := ShortLst[I] xor Key;
     end;
    end;
   end
  end;
 end;
end;
var
 I, X, Cnt, L, K32, TreeSize, FlagsPtr: Integer;
 Str, Slice: AnsiString;
 FilesLst: array of AnsiString;
 List: TAnsiStringList;
 MH: TAH_MainHeader;
 PH: TAH_PackHeader;
 H: THandle; Test: TMemStream;
 FlagsLst: array of Byte;
label
 Error;
begin //Load
 Result := E_UNKNOWN_FORMAT;
 List := TAnsiStringList.Create;
 try
  try
   H := CreateFileW(PWideChar(ArchiveName), GENERIC_READ,
                    FILE_SHARE_READ, nil, OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL, 0);
   if H <> INVALID_HANDLE_VALUE then
   try
    with THandleStream.Create(H) do
    try
     SetLength(Str, Length(LstHeader));
     ReadBuffer(Pointer(Str)^, Length(LstHeader));
    finally
     Free;
    end;
   finally
    FileClose(H);
   end;
   if Str = LstHeader then
   begin
    List.LoadFromFile(ArchiveName);
    Result := E_OK;
   end;
  except
   Result := E_EOPEN;
  end;
  if (Result = E_OK) and (List.Count >= 1) then
  begin
   Cnt := 0;
   Slice := List[2];
   if (Length(Slice) > Length(PrefixHeader)) and
      CompareMem(Pointer(Slice), Pointer(PrefixHeader), Length(PrefixHeader)) then
   begin
    X := 3;
    FNamesPrefix := Copy(Slice, Length(PrefixHeader) + 1, MaxInt);
   end else X := 2;
   for I := X to List.Count - 1 do
   begin
    Slice := List[I];
    if (Slice = '') or (Slice[1] = '*') then Continue;
    Str := CutStrSlice('|', Slice); // Remove comment block
    Slice := CutStrSlice('=', Str); // Slice = file name
    Str := Trim(Str); // Str = file index string
    L := Length(FNamesPrefix);
    if (Str <> '') and (Length(Slice) > L) and
     CompareMem(Pointer(Slice), Pointer(FNamesPrefix), L) then
    begin
     Delete(Slice, 1, L);
     L := StrToIntDef(Str, -1); // Get file index
     if (L >= 0) and (L < 32768) then
     begin
      if L >= Cnt then
      begin
       Cnt := L + 1;
       SetLength(FilesLst, Cnt);
      end;
     end else goto Error;
     X := LastDelimiter('_', Slice);
     if X > 1 then Slice[X] := '.'; // Set extension delimiter
     FilesLst[L] := LowerCase(Slice); // Save file name
    end else
    begin
     Error:
     Result := E_UNKNOWN_FORMAT;
     Break;
    end;
   end;
   if Result = E_OK then
   begin
    with MH do
    if (Source.Read(MH, SizeOf(TAH_MainHeader)) = SizeOf(TAH_MainHeader)) and
       (mhID = ID_AH) then
    begin
     mhKey := mhKey xor BEEF;
     MasterKey := mhKey;
     K32 := MasterKey or (MasterKey shl 16);
     Word(Addr(mhFlags)^) := Word(Addr(mhFlags)^) xor mhKey;
     if mhFlags and AHF_BIG_ENDIAN <> 0 then
      mhCount := SwapWord(mhCount xor mhKey) else
      mhCount := mhCount xor mhKey;
     if mhCount = Cnt then
     begin
      Flags := DEFAULT_FLAGS or mhFlags;
      SetLength(FlagsLst, Cnt);
      if mhFlags and AHF_FLAGS_FIELD <> 0 then
      begin
       Source.Read(FlagsPtr, 4);
       if mhFlags and AHF_BIG_ENDIAN <> 0 then
        FlagsPtr := SwapInt(FlagsPtr xor K32) else
        FlagsPtr := FlagsPtr xor K32;
      end else FlagsPtr := 0;
      SetLength(ExtraFields, mhExtraFields);
      Source.Read(ExtraFields[0], mhExtraFields shl 2);
      if mhFlags and AHF_BIG_ENDIAN <> 0 then
      for X := 0 to mhExtraFields - 1 do
       ExtraFields[X] := SwapLong(ExtraFields[X] xor K32) else
      for X := 0 to mhExtraFields - 1 do
       ExtraFields[X] := ExtraFields[X] xor K32;
      X := LoadPtrList(mhKey, Cnt, mhFlags); //Cnt * SizeOf(TAH_FileHeader);
      if X >= 0 then
      begin
       Source.Read(PH, SizeOf(TAH_PackHeader));
       with PH do
       begin
        phID := phID xor mhKey;
        if mhFlags and AHF_BIG_ENDIAN <> 0 then
         phNodeCnt := SwapWord(phNodeCnt xor mhKey) else
         phNodeCnt := phNodeCnt xor mhKey;
        SetLength(TreeBuffer, phNodeCnt);
        Source.Read(TreeBuffer[0], phNodeCnt shl 1);
        TreeSize := (phNodeCnt + 7) shr 3;
        SetLength(LeftBuffer, TreeSize);
        Source.Read(LeftBuffer[0], TreeSize);
        SetLength(RightBuffer, TreeSize);
        Source.Read(RightBuffer[0], TreeSize);
        if mhFlags and AHF_FLAGS_FIELD <> 0 then
        begin
         Source.Position := FlagsPtr;
         Source.Read(FlagsLst[0], Cnt);
        end;
        TreeSize := (TreeSize shl 1) + (phNodeCnt shl 1);
        if phID = ID_AH then
        begin
         Test := TMemStream.Create;
         try
          for I := 0 to Cnt - 1 do with AddItem do
          begin
           FileName := FilesLst[I];
           FileAttr := faArchive;
           with PtrLst[I] do
           begin
            Position := fhBitOffset shr 3 +
                        LongWord(SizeOf(TAH_MainHeader) +
                        Ord(mhFlags and AHF_FLAGS_FIELD <> 0) * 4 +
                        mhExtraFields shl 2 + X +
                        SizeOf(TAH_PackHeader) + TreeSize);
            Flags := (fhBitOffset and 7) or (FlagsLst[I] shl 3);
            FileSize := fhFileSize;
           end;
           if (FileName = '') or (FileSize < 0) then
           begin
            Result := E_BAD_ARCHIVE;
            Break;
           end;
           if Flags and MODE_LZSS <> 0 then
           begin
            PackSize := 4;
            Flags := Flags xor MODE_LZSS;
            Source.Position := Position;
            Test.Seek(0, soFromBeginning);
            Result := Decompress(Source, Test);
            if Result = E_OK then
            begin
             Flags := Flags or MODE_LZSS;            
             PackSize := FileSize;
             FileSize := PLongWord(Test.Memory)^ shr 8;
            end;
           end else
            PackSize := FileSize;
          end;
         finally
          Test.Free;
         end;
        end else Result := E_UNKNOWN_FORMAT;
       end;
      end else Result := E_EREAD;
     end else Result := E_BAD_ARCHIVE;
    end else Result := E_EREAD;
   end;
  end else if Result = E_OK then Result := E_UNKNOWN_FORMAT;
 finally
  List.Free;
 end;
end;

function TAHPackFile.Save(Source, Dest: TStream): Integer;
var
 Tree: THuffmanTree;
 Data: PByte;
 DataMask: Byte;

 function WriteBitStream(Value: Byte): Integer;
 var
  Mask: Cardinal;
 begin
  with Tree.BitStreams[Value] do
  begin
   Result := Size;
   Mask := 1 shl (Size - 1);
   while Mask > 0 do
   begin
    if DataMask = 0 then
    begin
     Inc(Data);
     Data^ := 0;
     DataMask := 1;
    end;
    if Bits and Mask <> 0 then
     Data^ := Data^ or DataMask;
    Mask := Mask shr 1;
    DataMask := DataMask shl 1;
   end;
  end;
 end;

type
 PWordRecArray = ^TWordRecArray;
 TWordRecArray = array[Byte] of WordRec;

var
 TreeBuf: PWordRecArray;
 LinkBufferL: PByteArray;
 LinkBufferR: PByteArray;

function FillTree(Node: PHuffTreeNode): Boolean;
var
 P: PByte;
begin
 if Node <> NIL then with Node^ do
 begin
  if (Left <> NIL) and (Right <> NIL) then
  begin
   if Position >= 0 then
   case Direction of
    diLeft:  TreeBuf[Position].Lo := Left.Position;
    diRight: TreeBuf[Position].Hi := Right.Position;
   end;
   Result := FillTree(Left) and FillTree(Right);
  end else
  begin
   if Position >= 0 then
   begin
    case Direction of
     diLeft:
     begin
      TreeBuf[Position].Lo := Value;
      P := Addr(LinkBufferL[Position shr 3]);
     end;
     diRight:
     begin
      TreeBuf[Position].Hi := Value;
      P := Addr(LinkBufferR[Position shr 3]);
     end;
    end;
    P^ := P^ or (1 shl (Position and 7));
   end;
   Result := True;
  end;
 end else Result := False;
end;

var
 PtrLst: array of TAH_FileHeader;
 MasterKey32, MaxFSize: LongWord;
 I, X, NSZ, Cnt, LSZ, DstSize: Integer;
 List: TAnsiStringList;
 MH: TAH_MainHeader;
 PB: PByte;
 BitOffset: LongWord;
 Buf: TMemStream;
 Dst: Pointer;
 FlagsLst: array of Byte;
 FreqList: array[Byte] of Cardinal;
begin //Save
 MasterKey32 := MasterKey or (MasterKey shl 16);
 Result := E_EWRITE;
 try
  Buf := TMemStream.Create;
  try
  // X := 0;
   Cnt := Min(65535, Count);
   SetLength(FlagsLst, Cnt);
   for I := 0 to Cnt - 1 do
    with FileItems[I] do
    begin
     Result := ProcessFile(Source, Buf);
     if Result <> E_OK then Exit;
     FlagsLst[I] := Modifier.NewFlags shr 3;
   //  Inc(X, Modifier.NewPackSize);
    end;
   FillChar(FreqList, SizeOf(FreqList), 0);
   PB := Buf.Memory;
   for I := 1 to Buf.Size do
   begin
    Inc(FreqList[PB^]);
    Inc(PB);
   end;
   SetLength(PtrLst, Cnt);
   Tree := THuffmanTree.Create(FreqList);
   with Tree do
   try
    NSZ := Length(Branches) shl 1;
    LSZ := (Length(Branches) + 7) shr 3;
    MaxFSize := 0;
    DstSize := SizeOf(TAH_MainHeader) + TotalBytesCount + NSZ + LSZ * 2;
    GetMem(Dst, DstSize);
    try
     with TAH_PackHeader(Dst^) do
     begin
      phID        := ID_AH xor MasterKey;
      TreeBuf     := Pointer(Integer(Dst) + SizeOf(TAH_MainHeader));
      LinkBufferL := Pointer(Integer(TreeBuf) + NSZ);
      LinkBufferR := Pointer(Integer(LinkBufferL) + LSZ);
      Data        := Pointer(Integer(LinkBufferR) + LSZ);
      FillChar(LinkBufferL^, LSZ * 2, 0);
      BitOffset := 0;      
      if Trunk <> nil then
      begin
       if FillTree(Trunk) then
       begin
        Data^ := 0;
        DataMask := 1;
        PB := Buf.Memory;
        for X := 0 to Cnt - 1 do with FileItems[X], PtrLst[X] do
        begin
         fhBitOffset := BitOffset;
         Modifier.NewFlags := Modifier.NewFlags or (BitOffset and 7);
         fhFileSize := Modifier.NewPackSize;
         if fhFileSize > MaxFSize then MaxFSize := fhFileSize;
         for I := 1 to fhFileSize do
         begin
          Inc(BitOffset, WriteBitStream(PB^));
          Inc(PB);
         end;
        end;
        SetLength(TreeBuffer, Length(Branches));
        Move(TreeBuf^, TreeBuffer[0], NSZ);
        SetLength(LeftBuffer, LSZ);
        Move(LinkBufferL^, LeftBuffer[0], LSZ);
        SetLength(RightBuffer, LSZ);
        Move(LinkBufferR^, RightBuffer[0], LSZ);
       end else
        raise Exception.Create('Huffman tree error.');
      end;
      if Flags and AHF_BIG_ENDIAN <> 0 then
      begin
       phTotalBits := SwapLong(BitOffset) xor MasterKey32;
       phNodeCnt   := SwapWord(Length(Branches)) xor MasterKey;
      end else
      begin
       phTotalBits := BitOffset xor MasterKey32;
       phNodeCnt   := Length(Branches) xor MasterKey;
      end;
     end;
     MH.mhID := ID_AH;
     MH.mhKey := MasterKey xor BEEF;
     if Flags and AHF_AUTO_LIST_FMT <> 0 then
     begin
      if Cnt > 0 then
      BitOffset := PtrLst[Cnt - 1].fhBitOffset;
      if BitOffset > $FFFFFF then
      begin
       if MaxFSize > $FFFF then
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3216;
      end else
      if BitOffset > $FFFF then
      begin
       if MaxFSize > $FFFF then
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
       if MaxFSize > $FF then
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3216 else
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_0824;
      end else
      if MaxFSize > $FFFF then
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
        Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_1616;
     end;
     if (Flags and AHF_NO_LZSS <> 0) or ZeroMemCheck(Pointer(FlagsLst), Cnt) then
     begin
      Flags := Flags and not AHF_FLAGS_FIELD;
      X := 0;
     end else
     begin
      Flags := Flags or AHF_FLAGS_FIELD;
      X := SizeOf(MH) + 4 + Length(ExtraFields) * 4 + DstSize;
      case Flags and LIST_FMT_MASK of
       LIST_FMT_3232: Inc(X, Cnt shl 3);
       LIST_FMT_1616,
       LIST_FMT_0824: Inc(X, Cnt shl 2);
       LIST_FMT_3216: Inc(X, Cnt * 6);
      end;
      if Flags and AHF_BIG_ENDIAN <> 0 then
       X := SwapInt(X) xor Integer(MasterKey32) else
       X := X xor Integer(MasterKey32);
     end;
     MH.mhFlags := Flags;
     MH.mhExtraFields := Length(ExtraFields);
     Word(Addr(MH.mhFlags)^) :=
      Word(Addr(MH.mhFlags)^) xor MasterKey;
     if Flags and AHF_BIG_ENDIAN <> 0 then
      MH.mhCount := SwapWord(Cnt) xor MasterKey else
      MH.mhCount := Cnt xor MasterKey;
     Dest.WriteBuffer(MH, SizeOf(MH));
     if Flags and AHF_FLAGS_FIELD <> 0 then
      Dest.WriteBuffer(X, 4);
     if Flags and AHF_BIG_ENDIAN <> 0 then
     for I := 0 to Byte(Length(ExtraFields)) - 1 do
     begin
      X := SwapLong(ExtraFields[I]) xor MasterKey32;
      Dest.Write(X, 4);
     end else
     for I := 0 to Byte(Length(ExtraFields)) - 1 do
     begin
      X := ExtraFields[I] xor Integer(MasterKey32);
      Dest.Write(X, 4);
     end;
     case Flags and LIST_FMT_MASK of
      LIST_FMT_3232:
      begin
       if Flags and AHF_BIG_ENDIAN <> 0 then
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhBitOffset := SwapLong(fhBitOffset) xor MasterKey32;
        fhFileSize := SwapLong(fhFileSize) xor MasterKey32;
       end else
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhBitOffset := fhBitOffset xor MasterKey32;
        fhFileSize := fhFileSize xor MasterKey32;
       end;
       Dest.WriteBuffer(PtrLst[0], SizeOf(TAH_FileHeader) * Cnt);
      end;
      LIST_FMT_1616:
      if Flags and AHF_BIG_ENDIAN <> 0 then
      for I := 0 to Cnt - 1 do with PtrLst[I] do
      begin
       fhBitOffset := (SwapWord(fhBitOffset) or
            (SwapWord(Min(65535, fhFileSize)) shl 16)) xor MasterKey32;
       Dest.Write(fhBitOffset, 4);
      end else
      for I := 0 to Cnt - 1 do with PtrLst[I] do
      begin
       fhBitOffset := (fhBitOffset or Cardinal(Min(65535, fhFileSize) shl 16)) xor
       MasterKey32;
       Dest.Write(fhBitOffset, 4);
      end;
      LIST_FMT_0824:
      if Flags and AHF_BIG_ENDIAN <> 0 then
      for I := 0 to Cnt - 1 do with PtrLst[I] do
      begin
       fhBitOffset := SwapLong(Cardinal(Min(255, fhFileSize)) or
        (fhBitOffset shl 8)) xor MasterKey32;
       Dest.Write(fhBitOffset, 4);
      end else
      for I := 0 to Cnt - 1 do with PtrLst[I] do
      begin
       fhBitOffset := (Cardinal(Min(255, fhFileSize)) or
        (fhBitOffset shl 8)) xor MasterKey32;
       Dest.Write(fhBitOffset, 4);
      end;
      else // LIST_FMT_3216:
      if Flags and AHF_BIG_ENDIAN <> 0 then
      begin
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhBitOffset := SwapLong(fhBitOffset) xor MasterKey32;
        Dest.Write(fhBitOffset, 4);
       end;
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhFileSize := SwapWord(Min(65535, fhFileSize)) xor MasterKey;
        Dest.Write(fhFileSize, 2);
       end;
      end else
      begin
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhBitOffset := fhBitOffset xor MasterKey32;
        Dest.Write(fhBitOffset, 4);
       end;
       for I := 0 to Cnt - 1 do with PtrLst[I] do
       begin
        fhFileSize := Min(65535, fhFileSize) xor MasterKey;
        Dest.Write(fhFileSize, 2);
       end;
      end;
     end;
     Dest.WriteBuffer(Dst^, DstSize);
     if Flags and AHF_FLAGS_FIELD <> 0 then
      Dest.WriteBuffer(FlagsLst[0], Cnt);
    finally
     FreeMem(Dst);
    end;
   finally
    Free;
   end;
   List := TAnsiStringList.Create;
   try
    List.Add(Copy(LstHeader, 1, Length(LstHeader) - 2));
    List.Add('*' + ExtractRelativePath(ArchiveName, FPakFileName));
    List.Add(PrefixHeader + FNamesPrefix);
    List.Add('******************');
    for I := 0 to Cnt - 1 do
    with FileItems[I] do
     List.Add(Format('%s%s = %d', [FNamesPrefix,
      FixedFileName(WideUpperCase(FileName), '_', AHFNameSet, True), I]));
    List.SaveToFile(ArchiveName);
   finally
    List.Free;
   end;
   List := TAnsiStringList.Create;
   try
    for I := 0 to Cnt - 1 do
    with FileItems[I] do
     List.Add(Format('#define %s%s'#9'%d', [FNamesPrefix,
      FixedFileName(WideUpperCase(FileName), '_', AHFNameSet, True), I]));
    List.SaveToFile(WideChangeFileExt(ArchiveName, '.h'));
   finally
    List.Free;
   end;
   Result := E_OK;
  finally
   Buf.Free;
  end;
 except
 end;
end;

procedure TAHPackFile.ApplyParameters;
var
 O, S: WideString;
 Cnt, I: Integer;
begin
 S := CustomParameters;
 while S <> '' do
 begin
  O := CutStrSliceW(' ', S);
  if (Length(O) = 2) and (O[1] = '-') then
  case O[2] of
   'B': Flags := Flags or AHF_BIG_ENDIAN;
   'e':
   begin
    I := StrToIntDef(CutStrSliceW(' ', S), 0);
    if (I >= 0) and (I < Length(ExtraFields)) then
     ExtraFields[I] := StrToIntDef(CutStrSliceW(' ', S), ExtraFields[I]) else
     CutStrSliceW(' ', S);
   end;
   'f':
   begin
    I := StrToIntDef(CutStrSliceW(' ', S), 0) - 1;
    case I of
     LIST_FMT_3232, LIST_FMT_1616,
     LIST_FMT_0824, LIST_FMT_3216: Flags :=
      ((Flags and not LIST_FMT_MASK) or Byte(I)) and not AHF_AUTO_LIST_FMT;
     else Flags := Flags or AHF_AUTO_LIST_FMT;
    end;
   end;
   'k': MasterKey := StrToIntDef(CutStrSliceW(' ', S), MasterKey);
   'L': Flags := Flags and not AHF_BIG_ENDIAN; // little endian
   'p': FNamesPrefix := CutStrSliceW(' ', S);
   'r': // setting ringshift for LZSS compression
   if TryStrToInt(CutStrSliceW(' ', S), I) then
   begin
    if I and 16 <> 0 then
     Flags := (Flags and not (AHF_NO_LZSS or (15 shl 8))) or
          AHF_FORCE_LZSS or ((Cardinal(I) and 15) shl 8) else
    if I < 0 then
     Flags := Flags or AHF_NO_LZSS else
     Flags := (Flags and not (AHF_FORCE_LZSS or AHF_NO_LZSS or (15 shl 8)))
              or ((Cardinal(I) and 15) shl 8);
   end;
   'E':
   begin
    Cnt := StrToIntDef(CutStrSliceW(' ', S), 0);
    SetLength(ExtraFields, Cnt);
    for I := 0 to Cnt - 1 do
     ExtraFields[I] := StrToIntDef(CutStrSliceW(' ', S), ExtraFields[I]);
   end;
  end;
 end;
end;

{ TAHPackItem }

function TAHPackItem.Compress(Input, Output: TStream): Integer;
var
 Pack: TALZSS_PackStream;
 Buf: TMemStream;
 SavePos: Int64;
 ItemFlags: Integer;
begin
 if Assigned(Modifier) and (Modifier.ClassType <> TFileModifier) then
  ItemFlags := Modifier.LoadFlags else
 if Flags and MODE_LZSS <> 0 then
  ItemFlags := 0 else
  ItemFlags := -1;
 with TAHPackFile(Owner) do
 begin
  if (Flags and AHF_NO_LZSS <> 0) or (ItemFlags = -1) then
  begin
   Result := inherited CopyData(Input, Output, Modifier.NewFileSize);
   Modifier.NewPackSize := Modifier.NewFileSize;
   Modifier.NewFlags := 0;
  end else
  if (Flags and AHF_FORCE_LZSS <> 0) or
    ((ItemFlags < 0) and (Byte(ItemFlags) > 16)) then
  begin
   if ItemFlags < 0 then
    Pack := TALZSS_PackStream.Create(Output, ItemFlags and 15) else
    Pack := TALZSS_PackStream.Create(Output, (Flags shr 8) and 15);
   try
    Result := inherited CopyData(Input, Pack, Modifier.NewFileSize);
    if Result = E_OK then
    begin
     Pack.Finalize;
     Modifier.NewPackSize := Pack.CompressedSize;
     Modifier.NewFlags := MODE_LZSS;
    end;
   finally
    Pack.Free;
   end;
  end else
  begin
   SavePos := Input.Position;
   Buf := TMemStream.Create;
   try
    if ItemFlags < 0 then
     Pack := TALZSS_PackStream.Create(Buf, ItemFlags and 15) else
     Pack := TALZSS_PackStream.Create(Buf, (Flags shr 8) and 15);
    try
     Result := inherited CopyData(Input, Pack, Modifier.NewFileSize);
     if Result = E_OK then
     begin
      Pack.Finalize;
      if Pack.CompressedSize >= Modifier.NewFileSize then
      begin
       Input.Position := SavePos;
       Result := inherited CopyData(Input, Output, Modifier.NewFileSize);
       Modifier.NewPackSize := Modifier.NewFileSize;
       Modifier.NewFlags := 0;
      end else
      begin
       Buf.Seek(0, soFromBeginning);
       Result := inherited CopyData(Buf, Output, Pack.CompressedSize);
       Modifier.NewPackSize := Pack.CompressedSize;
       Modifier.NewFlags := MODE_LZSS;
      end;
     end;
    finally
     Pack.Free;
    end;
   finally
    Buf.Free;
   end;
  end;
 end;
end;

function TAHPackItem.CopyData(Input, Output: TStream;
  Size: Int64): Integer;  // called when copy from original file
var
 CopyBuf: TMemStream;
begin
 CopyBuf := TMemStream.Create;
 try
  Result := Decompress(Input, CopyBuf);
  if Result = E_OK then
  begin
   CopyBuf.Seek(0, soFromBeginning);
   Result := Compress(CopyBuf, Output);
  end;
 finally
  CopyBuf.Free;
 end;
end;

function TAHPackItem.Decompress(Input, Output: TStream): Integer;
var
 BitCounter, PackedBits: Byte; Finish: Boolean;
 I: Integer; TreeOffset: Integer; Pack, Dst: TStream;
const
 Mask: array[0..7] of Byte =
 (1 shl 0,
  1 shl 1,
  1 shl 2,
  1 shl 3,
  1 shl 4,
  1 shl 5,
  1 shl 6,
  1 shl 7);
begin
 try
  if Flags and MODE_LZSS <> 0 then
   Dst := TMemStream.Create else
   Dst := Output;
  try
   BitCounter := Flags and 7;
   Input.Read(PackedBits, 1);
   PackedBits := PackedBits shr BitCounter;
   BitCounter := 8 - BitCounter;
   TotalBytesCopied := 0;
   with TAHPackFile(Owner) do
   begin
    for I := 1 to PackSize do
    begin
     TreeOffset := 0;
     repeat
      if BitCounter = 0 then
      begin
       Input.Read(PackedBits, 1);
       BitCounter := 8;
      end;
      if PackedBits and 1 = 0 then
      begin
       Finish := (LeftBuffer[TreeOffset shr 3] and Mask[TreeOffset and 7]) <> 0;
       TreeOffset := TreeBuffer[TreeOffset].Lo;
      end else
      begin
       Finish := (RightBuffer[TreeOffset shr 3] and Mask[TreeOffset and 7]) <> 0;
       TreeOffset := TreeBuffer[TreeOffset].Hi;
      end;
      PackedBits := PackedBits shr 1;
      Dec(BitCounter);
     until Finish;
     Dst.Write(TreeOffset, 1);
    end;
    Result := DoProgress(Self, FileSize);
   end;
   if Dst <> Output then
   begin
    Dst.Seek(0, soFromBeginning);
    Pack := TALZSS_UnpackStream.Create(Dst);
    try
     Result := inherited CopyData(Pack, Output, Pack.Size);
    finally
     Pack.Free;
    end;
   end;
  finally
   if Dst <> Output then Dst.Free;
  end;
 except
  Result := E_ERROR;
 end;
end;

end.
