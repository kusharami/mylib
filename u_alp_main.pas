unit u_alp_main;

interface

uses
 Windows, SysUtils, HexUnit, WcxHeadEx, MyUtils, Classes,
 Archiver, MyClasses, Compression, ALZSS;

type
 TALPackFile = class(TCustomArchive)
  private
   FPakFileName: WideString;
   MasterKey: Word;
   ExtraFields: array of LongInt;
  protected
   procedure Initialize; override;
   function GetDataFileName: WideString; override;
   function Load(Source: TStream): Integer; override;
   function Save(Source, Dest: TStream): Integer; override;
  public
   FNamesPrefix: AnsiString;  
   function InitStream: TStream; override;
   procedure ApplyParameters; override;
 end;

 TALPackItem = class(TArchiveItem)
  public
    function Decompress(Input, Output: TStream): Integer; override;
    function Compress(Input, Output: TStream): Integer; override;
 end;

const
 AHFNameSet: TSysCharSet = ['_', '0'..'9', 'A'..'Z', 'a'..'z'];
 LstHeader: AnsiString = '*AL PACK file list'#13#10;
 PrefixHeader: AnsiString = '*Prefix: ';
 ID_AL = Ord('A') or (Ord('L') shl 8);
 MODE_LZSS = 1 shl 0;
 MASTER_KEY = 0;
 MASTER_KEY32 = MASTER_KEY or (MASTER_KEY shl 16);
 BEEF = $EFBE;
 ALF_BIG_ENDIAN = 1 shl 3;
 ALF_FLAGS_FIELD = 1 shl 2;
 ALF_AUTO_LIST_FMT = 1 shl 15;
 ALF_NO_LZSS = 1 shl 14;
 ALF_FORCE_LZSS = 1 shl 13;
 DEFAULT_FLAGS = (12 shl 8) or ALF_AUTO_LIST_FMT;
 LIST_FMT_MASK = 3;
 LIST_FMT_3232 = 0; // Offset32, Size32
 LIST_FMT_1616 = 1; // Offset16, Size16
 LIST_FMT_0824 = 2; // Size8, Offset24
 LIST_FMT_3216 = 3; // Offset32 Table, Size16 Table
 ALIGN_SHIFT = 4;
 ALIGN_MASK = 3 shl ALIGN_SHIFT;
 NO_ALIGN = 0 shl ALIGN_SHIFT;
 ALIGN_2  = 1 shl ALIGN_SHIFT;
 ALIGN_4  = 2 shl ALIGN_SHIFT;
 ALIGN_8  = 3 shl ALIGN_SHIFT;

type
 TAL_MainHeader = packed record
  mhID: Word;   // AL
  mhKey: Word;  // Each next word xored with (mhKey xor $BEEF)
  mhFlags: Byte;
  mhExtraFields: Byte; // specify how much 32-bit extra fields
  // First extra field always points to file flag table: 1 byte for each file
  mhCount: Word;
 end;

 TAL_FileHeader = packed record // Xored with (mhKey xor $BEEF)
  fhOffset: LongWord;
  fhFileSize: LongWord;
 end;

function CheckALPack(const FileName: WideString): Boolean;

implementation

function CheckALPack(const FileName: WideString): Boolean;
var
 Str: AnsiString;
 H: THandle;
begin
 Result := False;
 try
  H := CreateFileW(PWideChar(FileName), GENERIC_READ,
                   FILE_SHARE_READ, nil, OPEN_EXISTING,
                   FILE_ATTRIBUTE_NORMAL, 0);
  if H <> INVALID_HANDLE_VALUE then
  try
   with THandleStream.Create(H) do
   try
    SetLength(Str, Length(LstHeader));
    ReadBuffer(Pointer(Str)^, Length(LstHeader));
    Result := Str = LstHeader;
   finally
    Free;
   end;
  finally
   FileClose(H);
  end;
 except
 end;
end;

function CutStrSlice(Chop: AnsiString; var Str: AnsiString): AnsiString;
var
 Len: Integer;
begin
 Len := Pos(Chop, Str) - 1;
 if Len >= 0 then
 begin
  Result := Trim(Copy(Str, 1, Len));
  Delete(Str, 1, Len + Length(Chop));
 end else
 begin
  Result := Str;
  Str := '';
 end;
end;

function CutStrSliceW(Chop: WideString; var Str: WideString): WideString;
var
 Len: Integer;
begin
 Len := Pos(Chop, Str) - 1;
 if Len >= 0 then
 begin
  Result := Trim(Copy(Str, 1, Len));
  Delete(Str, 1, Len + Length(Chop));
 end else
 begin
  Result := Str;
  Str := '';
 end;
end;

{ TALPackFile }

function TALPackFile.GetDataFileName: WideString;
begin
 Result := FPakFileName;
end;

procedure TALPackFile.Initialize;
begin
 MasterKey := MASTER_KEY;
 Flags := DEFAULT_FLAGS;
 inherited;
 FPakFileName := WideChangeFileExt(ArchiveName, '.alp');
 ItemClass := TALPackItem;
 FNamesPrefix := 'fi';
end;

function TALPackFile.InitStream: TStream;
var
 List: TAnsiStringList;
 Str: AnsiString;
 Stream: TStream;
begin
 try
  List := TAnsiStringList.Create;
  try
   FPakFileName := ArchiveName;
   Stream := inherited InitStream;
   FPakFileName := '';
   if Stream <> nil then
   try
    SetLength(Str, Length(LstHeader));
    Stream.ReadBuffer(Pointer(Str)^, Length(LstHeader));
    Stream.Position := 0;
    if Str = LstHeader then
    begin
     List.LoadFromStream(Stream);
     if List.Count > 1 then
     begin
      Str := Trim(List[1]);
      if Str[1] = '*' then
      begin
       Delete(Str, 1, 1);
       FPakFileName := ExpandFileNameEx(WideExtractFilePath(ArchiveName), Str);
       if not FileExists(FPakFileName) then
        FPakFileName := '';
      end;
     end;
    end;
   finally
    DoneStream(Stream);
   end;
   if FPakFileName = '' then
    FPakFileName := WideChangeFileExt(ArchiveName, '.alp');
   Result := inherited InitStream;
  finally
   List.Free;
  end;
 except
  Result := nil;
 end;
end;

function TALPackFile.Load(Source: TStream): Integer;
var
 PtrLst: array of TAL_FileHeader;
function LoadPtrList(Key: Word; Cnt, Flags: Integer): Integer;
var
 Lst: array of LongWord;
 ShortLst: array of Word;
 I, X: Integer; Key32: LongWord;
begin
 Key32 := Key or (Key shl 16);
 SetLength(PtrLst, Cnt);
 case Flags and LIST_FMT_MASK of
  LIST_FMT_3232:
  begin
   Result := Cnt * SizeOf(TAL_FileHeader);
   if Source.Read(PtrLst[0], Result) <> Result then
    Result := -1 else
   if Flags and ALF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    fhOffset := SwapLong(fhOffset xor Key32);
    fhFileSize := SwapLong(fhFileSize xor Key32);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    fhOffset := fhOffset xor Key32;
    fhFileSize := fhFileSize xor Key32;
   end;
  end;
  LIST_FMT_1616:
  begin
   SetLength(Lst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   if Flags and ALF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    fhOffset := SwapWord(Lo xor Key);
    fhFileSize := SwapWord(Hi xor Key);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    fhOffset := Lo xor Key;
    fhFileSize := Hi xor Key;
   end;
  end;
  LIST_FMT_0824:
  begin
   SetLength(Lst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   if Flags and ALF_BIG_ENDIAN <> 0 then
   for I := 0 to Cnt - 1 do with PtrLst[I], LongRec(Lst[I]) do
   begin
    X := SwapLong(Lst[I] xor Key32);
    fhOffset := X shr 8;
    fhFileSize := Byte(X);
   end else
   for I := 0 to Cnt - 1 do with PtrLst[I] do
   begin
    X := Lst[I] xor Key32;
    fhOffset := X shr 8;
    fhFileSize := Byte(X);
   end;
  end;
  else // LIST_FMT_3216:
  begin
   SetLength(Lst, Cnt);
   SetLength(ShortLst, Cnt);
   Result := Cnt * 4;
   if Source.Read(Lst[0], Result) <> Result then
    Result := -1 else
   begin
    Result := Cnt * 2;
    if Source.Read(ShortLst[0], Result) <> Result then
     Result := -1 else
    begin
     Inc(Result, Cnt * 4);
     if Flags and ALF_BIG_ENDIAN <> 0 then
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := SwapLong(Lst[I] xor Key32);
      fhFileSize := SwapWord(ShortLst[I] xor Key);
     end else
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := Lst[I] xor Key32;
      fhFileSize := ShortLst[I] xor Key;
     end;
    end;
   end
  end;
 end;
end;
var
 I, X, Cnt, L, K32, FlagsPtr: Integer;
 Str, Slice: AnsiString;
 FilesLst: array of AnsiString;
 List: TAnsiStringList;
 MH: TAL_MainHeader;
 H: THandle;
 FlagsLst: array of Byte;
 Start: Int64;
label
 Error;
begin //Load
 Result := E_UNKNOWN_FORMAT;
 List := TAnsiStringList.Create;
 try
  try
   H := CreateFileW(PWideChar(ArchiveName), GENERIC_READ,
                    FILE_SHARE_READ, nil, OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL, 0);
   if H <> INVALID_HANDLE_VALUE then
   try
    with THandleStream.Create(H) do
    try
     SetLength(Str, Length(LstHeader));
     ReadBuffer(Pointer(Str)^, Length(LstHeader));
    finally
     Free;
    end;
   finally
    FileClose(H);
   end;
   if Str = LstHeader then
   begin
    List.LoadFromFile(ArchiveName);
    Result := E_OK;
   end;
  except
   Result := E_EOPEN;
  end;
  if (Result = E_OK) and (List.Count >= 1) then
  begin
   Cnt := 0;
   Slice := List[2];
   if (Length(Slice) > Length(PrefixHeader)) and
      CompareMem(Pointer(Slice), Pointer(PrefixHeader), Length(PrefixHeader)) then
   begin
    X := 3;
    FNamesPrefix := Copy(Slice, Length(PrefixHeader) + 1, MaxInt);
   end else X := 2;
   for I := X to List.Count - 1 do
   begin
    Slice := List[I];
    if (Slice = '') or (Slice[1] = '*') then Continue;
    Str := CutStrSlice('|', Slice); // Remove comment block
    Slice := CutStrSlice('=', Str); // Slice = file name
    Str := Trim(Str); // Str = file index string
    L := Length(FNamesPrefix);
    if (Str <> '') and (Length(Slice) > L) and
     CompareMem(Pointer(Slice), Pointer(FNamesPrefix), L) then
    begin
     Delete(Slice, 1, L);
     L := StrToIntDef(Str, -1); // Get file index
     if (L >= 0) and (L <= 65535) then
     begin
      if L >= Cnt then
      begin
       Cnt := L + 1;
       SetLength(FilesLst, Cnt);
      end;
     end else goto Error;
     X := LastDelimiter('_', Slice);
     if X > 1 then Slice[X] := '.'; // Set extension delimiter
     FilesLst[L] := LowerCase(Slice); // Save file name
    end else
    begin
     Error:
     Result := E_UNKNOWN_FORMAT;
     Break;
    end;
   end;
   if Result = E_OK then
   begin
    with MH do
    if (Source.Read(MH, SizeOf(TAL_MainHeader)) = SizeOf(TAL_MainHeader)) and
       (mhID = ID_AL) then
    begin
     mhKey := mhKey xor BEEF;
     MasterKey := mhKey;
     K32 := MasterKey or (MasterKey shl 16);
     Word(Addr(mhFlags)^) := Word(Addr(mhFlags)^) xor mhKey;
     if mhFlags and ALF_BIG_ENDIAN <> 0 then
      mhCount := SwapWord(mhCount xor mhKey) else
      mhCount := mhCount xor mhKey;
     if mhCount = Cnt then
     begin
      Flags := DEFAULT_FLAGS or mhFlags;
      SetLength(FlagsLst, Cnt);
      if mhFlags and ALF_FLAGS_FIELD <> 0 then
      begin
       Source.Read(FlagsPtr, 4);
       if mhFlags and ALF_BIG_ENDIAN <> 0 then
        FlagsPtr := SwapInt(FlagsPtr xor K32) else
        FlagsPtr := FlagsPtr xor K32;
      end else FlagsPtr := 0;
      SetLength(ExtraFields, mhExtraFields);
      Source.Read(ExtraFields[0], mhExtraFields shl 2);
      if mhFlags and ALF_BIG_ENDIAN <> 0 then
      for X := 0 to mhExtraFields - 1 do
       ExtraFields[X] := SwapLong(ExtraFields[X] xor K32) else
      for X := 0 to mhExtraFields - 1 do
       ExtraFields[X] := ExtraFields[X] xor K32;
      X := LoadPtrList(mhKey, Cnt, mhFlags); //Cnt * SizeOf(TAL_FileHeader);
      if X >= 0 then
      begin
       Start := Source.Position;
       if mhFlags and ALF_FLAGS_FIELD <> 0 then
       begin
        Source.Position := FlagsPtr;
        Source.Read(FlagsLst[0], Cnt);
       end;
       for I := 0 to Cnt - 1 do with AddItem do
       begin
        FileName := FilesLst[I];
        FileAttr := faArchive;
        with PtrLst[I] do
        begin
         Position := fhOffset + Start;
         Flags := FlagsLst[I];
         if Flags and MODE_LZSS <> 0 then
         begin
          Source.Position := Position + 1;
          X := 0;
          Source.Read(X, 3);
          FileSize := X;
          PackSize := fhFileSize;
         end else
         begin
          FileSize := fhFileSize;
          PackSize := fhFileSize;
         end;
        end;
        if (FileName = '') or (FileSize < 0) then
        begin
         Result := E_BAD_ARCHIVE;
         Break;
        end;
       end;
      end else Result := E_UNKNOWN_FORMAT;
     end else Result := E_UNKNOWN_FORMAT
    end else Result := E_UNKNOWN_FORMAT;
   end;
  end else if Result = E_OK then Result := E_UNKNOWN_FORMAT;
 finally
  List.Free;
 end;
end;

function TALPackFile.Save(Source, Dest: TStream): Integer;

var
 PtrLst: array of TAL_FileHeader;
 MasterKey32, MaxFSize: LongWord;
 I, J, X, Cnt, AlignMax, AlignMask: Integer;
 List: TAnsiStringList;
 MH: TAL_MainHeader;
 Offset: LongWord;
 Buf: TMemStream;
 FlagsLst: array of Byte;
begin //Save
 MasterKey32 := MasterKey or (MasterKey shl 16);
 Result := E_EWRITE;
 try
  Buf := TMemStream.Create;
  try
   X := 0;
   MaxFSize := 0;
   Cnt := Min(65535, Count);
   SetLength(FlagsLst, Cnt);
   SetLength(PtrLst, Cnt);
   AlignMax := (1 shl ((Flags and ALIGN_MASK) shr ALIGN_SHIFT)) - 1;
   AlignMask := not AlignMax;
   for I := 0 to Cnt - 1 do
    with FileItems[I], PtrLst[I] do
    begin
     fhOffset := X;
     Result := ProcessFile(Source, Buf);
     if Result <> E_OK then Exit;
     FlagsLst[I] := Modifier.NewFlags;
     fhFileSize := Modifier.NewPackSize;
     if fhFileSize > MaxFSize then
      MaxFSize := fhFileSize;
     Inc(X, fhFileSize);
     J := X;
     X := (X + AlignMax) and AlignMask;
     Buf.Seek(X - J, soFromCurrent);
    end;
   MH.mhID := ID_AL;
   MH.mhKey := MasterKey xor BEEF;
   if Flags and ALF_AUTO_LIST_FMT <> 0 then
   begin
    if Cnt > 0 then
     Offset := PtrLst[Cnt - 1].fhOffset else
     Offset := 0;
    if Offset > $FFFFFF then
    begin
     if MaxFSize > $FFFF then
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3216;
    end else
    if Offset > $FFFF then
    begin
     if MaxFSize > $FFFF then
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
     if MaxFSize > $FF then
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3216 else
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_0824;
    end else
    if MaxFSize > $FFFF then
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_3232 else
      Flags := (Flags and not LIST_FMT_MASK ) or LIST_FMT_1616;
   end;
   if (Flags and ALF_NO_LZSS <> 0) or ZeroMemCheck(Pointer(FlagsLst), Cnt) then
   begin
    Flags := Flags and not ALF_FLAGS_FIELD;
    X := 0;
   end else
   begin
    Flags := Flags or ALF_FLAGS_FIELD;
    X := SizeOf(MH) + 4 + Length(ExtraFields) * 4;
    case Flags and LIST_FMT_MASK of
     LIST_FMT_3232: Inc(X, Cnt shl 3);
     LIST_FMT_1616,
     LIST_FMT_0824: Inc(X, Cnt shl 2);
     LIST_FMT_3216: Inc(X, Cnt * 6);
    end;
    Inc(X, Buf.Size);
    if Flags and ALF_BIG_ENDIAN <> 0 then
     X := SwapInt(X) xor Integer(MasterKey32) else
     X := X xor Integer(MasterKey32);
   end;
   MH.mhFlags := Flags;
   MH.mhExtraFields := Length(ExtraFields);
   Word(Addr(MH.mhFlags)^) :=
    Word(Addr(MH.mhFlags)^) xor MasterKey;
   if Flags and ALF_BIG_ENDIAN <> 0 then
    MH.mhCount := SwapWord(Cnt) xor MasterKey else
    MH.mhCount := Cnt xor MasterKey;
   Dest.WriteBuffer(MH, SizeOf(MH));
   if Flags and ALF_FLAGS_FIELD <> 0 then
    Dest.WriteBuffer(X, 4);
   if Flags and ALF_BIG_ENDIAN <> 0 then
   for I := 0 to Byte(Length(ExtraFields)) - 1 do
   begin
    X := SwapLong(ExtraFields[I]) xor MasterKey32;
    Dest.Write(X, 4);
   end else
   for I := 0 to Byte(Length(ExtraFields)) - 1 do
   begin
    X := ExtraFields[I] xor Integer(MasterKey32);
    Dest.Write(X, 4);
   end;
   case Flags and LIST_FMT_MASK of
    LIST_FMT_3232:
    begin
     if Flags and ALF_BIG_ENDIAN <> 0 then
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := SwapLong(fhOffset) xor MasterKey32;
      fhFileSize := SwapLong(fhFileSize) xor MasterKey32;
     end else
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := fhOffset xor MasterKey32;
      fhFileSize := fhFileSize xor MasterKey32;
     end;
     Dest.WriteBuffer(PtrLst[0], SizeOf(TAL_FileHeader) * Cnt);
    end;
    LIST_FMT_1616:
    if Flags and ALF_BIG_ENDIAN <> 0 then
    for I := 0 to Cnt - 1 do with PtrLst[I] do
    begin
     fhOffset := (SwapWord(fhOffset) or
          (SwapWord(Min(65535, fhFileSize)) shl 16)) xor MasterKey32;
     Dest.Write(fhOffset, 4);
    end else
    for I := 0 to Cnt - 1 do with PtrLst[I] do
    begin
     fhOffset := (fhOffset or Cardinal(Min(65535, fhFileSize) shl 16)) xor
     MasterKey32;
     Dest.Write(fhOffset, 4);
    end;
    LIST_FMT_0824:
    if Flags and ALF_BIG_ENDIAN <> 0 then
    for I := 0 to Cnt - 1 do with PtrLst[I] do
    begin
     fhOffset := SwapLong(Cardinal(Min(255, fhFileSize)) or
      (fhOffset shl 8)) xor MasterKey32;
     Dest.Write(fhOffset, 4);
    end else
    for I := 0 to Cnt - 1 do with PtrLst[I] do
    begin
     fhOffset := (Cardinal(Min(255, fhFileSize)) or
      (fhOffset shl 8)) xor MasterKey32;
     Dest.Write(fhOffset, 4);
    end;
    else // LIST_FMT_3216:
    if Flags and ALF_BIG_ENDIAN <> 0 then
    begin
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := SwapLong(fhOffset) xor MasterKey32;
      Dest.Write(fhOffset, 4);
     end;
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhFileSize := SwapWord(Min(65535, fhFileSize)) xor MasterKey;
      Dest.Write(fhFileSize, 2);
     end;
    end else
    begin
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhOffset := fhOffset xor MasterKey32;
      Dest.Write(fhOffset, 4);
     end;
     for I := 0 to Cnt - 1 do with PtrLst[I] do
     begin
      fhFileSize := Min(65535, fhFileSize) xor MasterKey;
      Dest.Write(fhFileSize, 2);
     end;
    end;
   end;
   Dest.WriteBuffer(Buf.Memory^, Buf.Size);
   if Flags and ALF_FLAGS_FIELD <> 0 then
    Dest.WriteBuffer(FlagsLst[0], Cnt);
   List := TAnsiStringList.Create;
   try
    List.Add(Copy(LstHeader, 1, Length(LstHeader) - 2));
    List.Add('*' + ExtractRelativePath(ArchiveName, FPakFileName));
    List.Add(PrefixHeader + FNamesPrefix);
    List.Add('******************');
    for I := 0 to Cnt - 1 do
    with FileItems[I] do
     List.Add(Format('%s%s = %d', [FNamesPrefix,
      FixedFileName(WideUpperCase(FileName), '_', AHFNameSet, True), I]));
    List.SaveToFile(ArchiveName);
   finally
    List.Free;
   end;
   List := TAnsiStringList.Create;
   try
    for I := 0 to Cnt - 1 do
    with FileItems[I] do
     List.Add(Format('#define %s%s'#9'%d', [FNamesPrefix,
      FixedFileName(WideUpperCase(FileName), '_', AHFNameSet, True), I]));
    List.SaveToFile(WideChangeFileExt(ArchiveName, '.h'));
   finally
    List.Free;
   end;
   Result := E_OK;
  finally
   Buf.Free;
  end;
 except
 end;
end;

procedure TALPackFile.ApplyParameters;
var
 O, S: WideString;
 Cnt, I: Integer;
begin
 S := CustomParameters;
 while S <> '' do
 begin
  O := CutStrSliceW(' ', S);
  if (Length(O) = 2) and (O[1] = '-') then
  case O[2] of
   'a':
   begin
    Flags := Flags and not ALIGN_MASK;
    case StrToIntDef(CutStrSliceW(' ', S), 0) of
     8: Flags := Flags or ALIGN_8;
     4: Flags := Flags or ALIGN_4;
     2: Flags := Flags or ALIGN_2;
    end;
   end;
   'B': Flags := Flags or ALF_BIG_ENDIAN;
   'e':
   begin
    I := StrToIntDef(CutStrSliceW(' ', S), 0);
    if (I >= 0) and (I < Length(ExtraFields)) then
     ExtraFields[I] := StrToIntDef(CutStrSliceW(' ', S), ExtraFields[I]) else
     CutStrSliceW(' ', S);
   end;
   'f':
   begin
    I := StrToIntDef(CutStrSliceW(' ', S), 0) - 1;
    case I of
     LIST_FMT_3232, LIST_FMT_1616,
     LIST_FMT_0824, LIST_FMT_3216: Flags :=
      ((Flags and not LIST_FMT_MASK) or Byte(I)) and not ALF_AUTO_LIST_FMT;
     else Flags := Flags or ALF_AUTO_LIST_FMT;
    end;
   end;
   'k': MasterKey := StrToIntDef(CutStrSliceW(' ', S), MasterKey);
   'L': Flags := Flags and not ALF_BIG_ENDIAN; // little endian
   'p': FNamesPrefix := CutStrSliceW(' ', S);
   'r': // setting ringshift for LZSS compression
   if TryStrToInt(CutStrSliceW(' ', S), I) then
   begin
    if I and 16 <> 0 then
     Flags := (Flags and not (ALF_NO_LZSS or (15 shl 8))) or
          ALF_FORCE_LZSS or ((Cardinal(I) and 15) shl 8) else
    if I < 0 then
     Flags := Flags or ALF_NO_LZSS else
     Flags := (Flags and not (ALF_FORCE_LZSS or ALF_NO_LZSS or (15 shl 8)))
              or ((Cardinal(I) and 15) shl 8);
   end;
   'E':
   begin
    Cnt := StrToIntDef(CutStrSliceW(' ', S), 0);
    SetLength(ExtraFields, Cnt);
    for I := 0 to Cnt - 1 do
     ExtraFields[I] := StrToIntDef(CutStrSliceW(' ', S), ExtraFields[I]);
   end;
  end;
 end;
end;

{ TALPackItem }

function TALPackItem.Compress(Input, Output: TStream): Integer;
var
 Pack: TALZSS_PackStream;
 Buf: TMemStream;
 SavePos: Int64;
 ItemFlags: Integer;
begin
 if Assigned(Modifier) and (Modifier.ClassType <> TFileModifier) then
  ItemFlags := Modifier.LoadFlags else
 if Flags and MODE_LZSS <> 0 then
  ItemFlags := 0 else
  ItemFlags := -1;
 with TALPackFile(Owner) do
 begin
  if (Flags and ALF_NO_LZSS <> 0) or (ItemFlags = -1) then
  begin
   Result := CopyData(Input, Output, Modifier.NewFileSize);
   Modifier.NewPackSize := Modifier.NewFileSize;
   Modifier.NewFlags := 0;
  end else
  if (Flags and ALF_FORCE_LZSS <> 0) or
    ((ItemFlags < 0) and (Byte(ItemFlags) > 16)) then
  begin
   if ItemFlags < 0 then
    Pack := TALZSS_PackStream.Create(Output, ItemFlags and 15) else
    Pack := TALZSS_PackStream.Create(Output, (Flags shr 8) and 15);
   try
    Result := CopyData(Input, Pack, Modifier.NewFileSize);
    if Result = E_OK then
    begin
     Pack.Finalize;
     Modifier.NewPackSize := Pack.CompressedSize;
     Modifier.NewFlags := MODE_LZSS;
    end;
   finally
    Pack.Free;
   end;
  end else
  begin
   SavePos := Input.Position;
   Buf := TMemStream.Create;
   try
    if ItemFlags < 0 then
     Pack := TALZSS_PackStream.Create(Buf, ItemFlags and 15) else
     Pack := TALZSS_PackStream.Create(Buf, (Flags shr 8) and 15);
    try
     Result := CopyData(Input, Pack, Modifier.NewFileSize);
     if Result = E_OK then
     begin
      Pack.Finalize;
      if Pack.CompressedSize >= Modifier.NewFileSize then
      begin
       Input.Position := SavePos;
       Result := CopyData(Input, Output, Modifier.NewFileSize);
       Modifier.NewPackSize := Modifier.NewFileSize;
       Modifier.NewFlags := 0;
      end else
      begin
       Buf.Seek(0, soFromBeginning);
       Result := CopyData(Buf, Output, Pack.CompressedSize);
       Modifier.NewPackSize := Pack.CompressedSize;
       Modifier.NewFlags := MODE_LZSS;
      end;
     end;
    finally
     Pack.Free;
    end;
   finally
    Buf.Free;
   end;
  end;
 end;
end;


function TALPackItem.Decompress(Input, Output: TStream): Integer;
var
 Pack: TStream;
begin
 if Flags and MODE_LZSS <> 0 then
 try
  Pack := TALZSS_UnpackStream.Create(Input);
  try
   Result := CopyData(Pack, Output, Pack.Size);
  finally
   Pack.Free;
  end;
 except
  Result := E_ERROR;
 end else
  Result := CopyData(Input, Output, FileSize);
end;


end.
