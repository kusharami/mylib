unit xcrypt;

interface

uses SysUtils;

type
 TBlockHeader = packed record
  Size: LongInt;
  CRC: LongWord;
 end;

type
 TPattern = array[0..7] of Char;

function Decrypt(Pattern: Int64; Size: Integer;
                     Source, Dest: Pointer): Integer;
procedure Encrypt(Pattern: Int64; Size: Integer;
                     Source, Dest: Pointer);
function GetEncryptSize(Size: Integer): Integer;

implementation

const
 CRPAT = $A1652347;

const sdbytetable1: array[0..255] of Byte =
(
 $78, $79, $7A, $7B, $7C, $7D, $7E, $7F, $80, $81, $82, $83, $84, $85, $86, $87,
 $88, $89, $8A, $8B, $8C, $8D, $8E, $8F, $90, $91, $92, $93, $94, $95, $96, $97,
 $98, $99, $9A, $9B, $9C, $9D, $9E, $9F, $A0, $A1, $A2, $A3, $A4, $A5, $A6, $A7,
 $A8, $A9, $AA, $AB, $AC, $AD, $AE, $AF, $B0, $B1, $B2, $B3, $B4, $B5, $B6, $B7,
 $B8, $B9, $BA, $BB, $BC, $BD, $BE, $BF, $C0, $C1, $C2, $C3, $C4, $C5, $C6, $C7,
 $C8, $C9, $CA, $CB, $CC, $CD, $CE, $CF, $D0, $D1, $D2, $D3, $D4, $D5, $D6, $D7,
 $D8, $D9, $DA, $DB, $DC, $DD, $DE, $DF, $E0, $E1, $E2, $E3, $E4, $E5, $E6, $E7,
 $E8, $E9, $EA, $EB, $EC, $ED, $EE, $EF, $F0, $F1, $F2, $F3, $F4, $F5, $F6, $F7,
 $F8, $F9, $FA, $FB, $FC, $FD, $FE, $FF, $00, $01, $02, $03, $04, $05, $06, $07,
 $08, $09, $0A, $0B, $0C, $0D, $0E, $0F, $10, $11, $12, $13, $14, $15, $16, $17,
 $18, $19, $1A, $1B, $1C, $1D, $1E, $1F, $20, $21, $22, $23, $24, $25, $26, $27,
 $28, $29, $2A, $2B, $2C, $2D, $2E, $2F, $30, $31, $32, $33, $34, $35, $36, $37,
 $38, $39, $3A, $3B, $3C, $3D, $3E, $3F, $40, $41, $42, $43, $44, $45, $46, $47,
 $48, $49, $4A, $4B, $4C, $4D, $4E, $4F, $50, $51, $52, $53, $54, $55, $56, $57,
 $58, $59, $5A, $5B, $5C, $5D, $5E, $5F, $60, $61, $62, $63, $64, $65, $66, $67,
 $68, $69, $6A, $6B, $6C, $6D, $6E, $6F, $70, $71, $72, $73, $74, $75, $76, $77
);

const sdbytetable2: array[0..255] of Byte =
(
 $88, $89, $8A, $8B, $8C, $8D, $8E, $8F, $90, $91, $92, $93, $94, $95, $96, $97,
 $98, $99, $9A, $9B, $9C, $9D, $9E, $9F, $A0, $A1, $A2, $A3, $A4, $A5, $A6, $A7,
 $A8, $A9, $AA, $AB, $AC, $AD, $AE, $AF, $B0, $B1, $B2, $B3, $B4, $B5, $B6, $B7,
 $B8, $B9, $BA, $BB, $BC, $BD, $BE, $BF, $C0, $C1, $C2, $C3, $C4, $C5, $C6, $C7,
 $C8, $C9, $CA, $CB, $CC, $CD, $CE, $CF, $D0, $D1, $D2, $D3, $D4, $D5, $D6, $D7,
 $D8, $D9, $DA, $DB, $DC, $DD, $DE, $DF, $E0, $E1, $E2, $E3, $E4, $E5, $E6, $E7,
 $E8, $E9, $EA, $EB, $EC, $ED, $EE, $EF, $F0, $F1, $F2, $F3, $F4, $F5, $F6, $F7,
 $F8, $F9, $FA, $FB, $FC, $FD, $FE, $FF, $00, $01, $02, $03, $04, $05, $06, $07,
 $08, $09, $0A, $0B, $0C, $0D, $0E, $0F, $10, $11, $12, $13, $14, $15, $16, $17,
 $18, $19, $1A, $1B, $1C, $1D, $1E, $1F, $20, $21, $22, $23, $24, $25, $26, $27,
 $28, $29, $2A, $2B, $2C, $2D, $2E, $2F, $30, $31, $32, $33, $34, $35, $36, $37,
 $38, $39, $3A, $3B, $3C, $3D, $3E, $3F, $40, $41, $42, $43, $44, $45, $46, $47,
 $48, $49, $4A, $4B, $4C, $4D, $4E, $4F, $50, $51, $52, $53, $54, $55, $56, $57,
 $58, $59, $5A, $5B, $5C, $5D, $5E, $5F, $60, $61, $62, $63, $64, $65, $66, $67,
 $68, $69, $6A, $6B, $6C, $6D, $6E, $6F, $70, $71, $72, $73, $74, $75, $76, $77,
 $78, $79, $7A, $7B, $7C, $7D, $7E, $7F, $80, $81, $82, $83, $84, $85, $86, $87
);

type
 u64 = 0..9223372036854775807;
 s64 = Int64;
 u32 = Cardinal;
 s32 = Integer;
 u16 = Word;
 s16 = SmallInt;
 u8  = Byte;
 s8  = ShortInt;
 GPR_reg = packed record
  case Byte of
   0: (UD: array[0..1]  of u64);
   1: (SD: array[0..1]  of s64);
   2: (UL: array[0..3]  of u32);
   3: (SL: array[0..3]  of s32);
   4: (US: array[0..7]  of u16);
   5: (SS: array[0..7]  of s16);
   6: (UC: array[0..15] of u8 );
   7: (SC: array[0..15] of s8 );
 end;
 GPRRegs = packed record
  case Boolean of
   False: (n: packed record
            case Byte of
             0: (r0: GPR_reg; //0
                 at: GPR_reg; //1
                 v0: GPR_reg; //2
                 v1: GPR_reg; //3
                 a0: GPR_reg; //4
                 a1: GPR_reg; //5
                 a2: GPR_reg; //6
                 a3: GPR_reg; //7
                 t0: GPR_reg; //8
                 t1: GPR_reg; //9
                 t2: GPR_reg; //10
                 t3: GPR_reg; //11
                 t4: GPR_reg; //12
                 t5: GPR_reg; //13
                 t6: GPR_reg; //14
                 t7: GPR_reg; //15
                 s0: GPR_reg; //16
                 s1: GPR_reg; //17
                 s2: GPR_reg; //18
                 s3: GPR_reg; //19
                 s4: GPR_reg; //20
                 s5: GPR_reg; //21
                 s6: GPR_reg; //22
                 s7: GPR_reg; //23
                 t8: GPR_reg; //24
                 t9: GPR_reg; //25
                 k0: GPR_reg; //26
                 k1: GPR_reg; //27
                 gp: GPR_reg; //28
                 sp: GPR_reg; //29
                 fp: GPR_reg; //30
                 ra: GPR_reg); //31
             1: (r0x: Int64; //0
                 r0y: Int64;
                 atx: Int64; //1
                 aty: Int64;
                 v0x: Int64; //2
                 v0y: Int64;
                 v1x: Int64; //3
                 v1y: Int64;
                 a0x: Int64; //4
                 a0y: Int64;
                 a1x: Int64; //5
                 a1y: Int64;
                 a2x: Int64; //6
                 a2y: Int64;
                 a3x: Int64; //7
                 a3y: Int64;
                 t0x: Int64; //8
                 t0y: Int64;
                 t1x: Int64; //9
                 t1y: Int64;
                 t2x: Int64; //10
                 t2y: Int64;
                 t3x: Int64; //11
                 t3y: Int64;
                 t4x: Int64; //12
                 t4y: Int64;
                 t5x: Int64; //13
                 t5y: Int64;
                 t6x: Int64; //14
                 t6y: Int64;
                 t7x: Int64; //15
                 t7y: Int64;
                 s0x: Int64; //16
                 s0y: Int64;
                 s1x: Int64; //17
                 s1y: Int64;
                 s2x: Int64; //18
                 s2y: Int64;
                 s3x: Int64; //19
                 s3y: Int64;
                 s4x: Int64; //20
                 s4y: Int64;
                 s5x: Int64; //21
                 s5y: Int64;
                 s6x: Int64; //22
                 s6y: Int64;
                 s7x: Int64; //23
                 s7y: Int64;
                 t8x: Int64; //24
                 t8y: Int64;
                 t9x: Int64; //25
                 t9y: Int64;
                 k0x: Int64; //26
                 k0y: Int64;
                 k1x: Int64; //27
                 k1y: Int64;
                 gpx: Int64; //28
                 gpy: Int64;
                 spx: Int64; //29
                 spy: Int64;
                 fpx: Int64; //30
                 fpy: Int64;
                 rax: Int64;
                 ray: Int64;); //31
           end);
   True: (r: array[0..31] of GPR_reg);
 end;

 (*
     a1x := s3x; // src       
     a2x := s0x; // dst       
     a0x := s1x; // pat
 *)
procedure DecryptBlockStep1(var GPR: GPRRegs); //FirstPass
begin
 with GPR.n do
 begin
  t1x := a0x;                                   //	daddu		t1, a0, zero		# 0041b148:0080482d
  v1x := Integer(Addr(sdbytetable1[0]));        //	lui		v1, $0048		# 0041b14c:3c030048	v1=$00480000
//  v0x := SrcIndex;                            //	lw		v0, $0100(t1)		# 0041b150:8d220100
  v0x := Integer(Pointer(Integer(t1x) + $100)^);
  t4x := v1x;                                   //	addiu		t4, v1, $dc10		# 0041b154:246cdc10	t4=$0047dc10
  a3x := 0;                                     //	daddu		a3, zero, zero		# 0041b158:0000382d
  v0x := Cardinal(v0x) shr 3;                   //	srl		v0, v0, 3		# 0041b15c:000210c2
  v0x := v0x xor $45;                           //	xori		v0, v0, $0045		# 0041b160:38420045
  t3x := v0x and $FF;                           //	andi		t3, v0, $00ff		# 0041b164:304b00ff
  v0x := Integer(a1x) + Integer(a3x);           //	addu		v0, a1, a3		# 0041b168:00a71021
                                                //	nop					# 0041b16c:00000000
//__0041b170:
  repeat
   v1x := Integer(a2x) + Integer(a3x);           //	addu		v1, a2, a3		# 0041b170:00c71821
   a0x := PByte(v0x)^;                  //	lbu		a0, $0000(v0)		# 0041b174:90440000
   t2x := Integer(a3x) + 1;                      //	addiu		t2, a3, $0001		# 0041b178:24ea0001	t2=$00000001
   t0x := v1x;                                   //	daddu		t0, v1, zero		# 0041b17c:0060402d
   a3x := 0;                                     //	daddu		a3, zero, zero		# 0041b180:0000382d
   v0x := t3x xor a0x;                           //	xor		v0, t3, a0		# 0041b184:01641026
   PByte(v1x)^ := Byte(v0x);                     //	sb		v0, $0000(v1)		# 0041b188:a0620000
   t3x := a0x;                                   //	daddu		t3, a0, zero		# 0041b18c:0080582d
 //__0041b190:
   repeat                                        //  v0x := SrcIndex; //	lbu		v0, $0100(t1)		# 0041b190:91220100
    v0x := PByte(Integer(t1x) + $100)^;
    v1x := PByte(t0x)^;                           //	lbu		v1, $0000(t0)		# 0041b194:91030000
    v0x := v0x shr 3;                             //	dsrl		v0, v0, 3		# 0041b198:000210fa
    v0x := Cardinal(v0x) shl 3;                   //	sll		v0, v0, 3		# 0041b19c:000210c0
    v1x := Integer(v1x) + Integer(t4x);           //	addu		v1, v1, t4		# 0041b1a0:006c1821
    v0x := Integer(t1x) + Integer(v0x);           //	addu		v0, t1, v0		# 0041b1a4:01221021
    a0x := PByte(v1x)^;                           //	lbu		a0, $0000(v1)		# 0041b1a8:90640000
    v0x := Integer(v0x) + Integer(a3x);           //	addu		v0, v0, a3		# 0041b1ac:00471021
    v1x := PByte(v0x)^;                           //	lbu		v1, $0000(v0)		# 0041b1b0:90430000
    a3x := Integer(a3x) + 1;                      //	addiu		a3, a3, $0001		# 0041b1b4:24e70001	a3=$00000001
    v0x := Byte(a3x < 8);                         //	sltiu		v0, a3, $0008		# 0041b1b8:2ce20008
    a0x := Integer(a0x) - Integer(v1x);           //	subu		a0, a0, v1		# 0041b1bc:00832023
    PByte(t0x)^ := Byte(a0x);                     //	sb		a0, $0000(t0)		# 0041b1c4:a1040000
   until v0x = 0;                                 //	bne		v0, zero, $0041b190	# 0041b1c0:1440fff3	^ __0041b190

   a3x := t2x;                                    //	daddu		a3, t2, zero		# 0041b1c8:0140382d	a3=$00000001
   v0x := Integer(a1x) + Integer(a3x);            //	addu		v0, a1, a3		# 0041b1d4:00a71021
                                                  //	sltiu		v0, a3, $0008		# 0041b1cc:2ce20008
  until a3x >= 8;                                 //	bne		v0, zero, $0041b170	# 0041b1d0:1440ffe7	^ __0041b170
//	jr		ra			# 0041b1d8:03e00008
 end;
end;

procedure DecryptBlockStep2(var GPR: GPRRegs);    // FNC_0041B0D8; //SecondPass
begin
 with GPR.n do
 begin
//  v0x := SrcIndex;                              //	lw		v0, $0100(a0)		# 0041b0d8:8c820100
  v0x := Integer(Pointer(Integer(a0x) + $100)^);  //	ori		t0, zero, $a165		# 0041b0dc:3408a165	t0=$0000a165
  t0x := CRPAT;                                   //	dsll		t0, t0, 16		# 0041b0e0:00084438
                                                  //	ori		t0, t0, $2347		# 0041b0e4:35082347	t0=$0000a367
  t1x := Cardinal(Pointer(Integer(a0x) + $100)^); //  t1x := SrcIndex; //	lwu		t1, $0100(a0)		# 0041b0e8:9c890100
  a3x := PInt64(a1x)^;                            //	ld		a3, $0000(a1)		# 0041b0ec:dca70000
  v0x := v0x and $F8;                             //	andi		v0, v0, $00f8		# 0041b0f0:304200f8
  a0x := Integer(a0x) + Integer(v0x);             //	addu		a0, a0, v0		# 0041b0f4:00822021
  a1x := t1x shl 10;                              //	dsll		a1, t1, 10		# 0041b0f8:00092ab8
  PInt64(a2x)^ := a3x;                            //	sd		a3, $0000(a2)		# 0041b0fc:fcc70000
  v1x := t1x shl 20;                              //	dsll		v1, t1, 20		# 0041b100:00091d38
  v1x := v1x or a1x;                              //	or		v1, v1, a1		# 0041b104:00651825
  v0x := t1x shl 30;                              //	dsll		v0, t1, 30		# 0041b108:000917b8
  a1x := PInt64(a0x)^;                            //	ld		a1, $0000(a0)		# 0041b10c:dc850000
  v0x := t1x or v0x;                              //	or		v0, t1, v0		# 0041b110:01221025
  v0x := v0x or v1x;                              //	or		v0, v0, v1		# 0041b114:00431025
  t1x := v0x + t0x;                               //	daddu		t1, v0, t0		# 0041b118:0048482d
  a3x := a3x - a1x;                               //	dsubu		a3, a3, a1		# 0041b11c:00e5382f
  a3x := a3x xor t1x;                             //	xor		a3, a3, t1		# 0041b120:00e93826
  PInt64(a2x)^ := a3x;                            //	sd		a3, $0000(a2)		# 0041b124:fcc70000
  v0x := PInt64(a0x)^;                            //	ld		v0, $0000(a0)		# 0041b128:dc820000
  a3x := a3x xor v0x;                             //	xor		a3, a3, v0		# 0041b12c:00e23826
  v1x := a3x shr 32;                              //	dsrl32		v1, a3, 0		# 0041b130:0007183e
  a3x := a3x shl 32;                              //	dsll32		a3, a3, 0		# 0041b134:0007383c
  a3x := a3x or v1x;                              //	or		a3, a3, v1		# 0041b138:00e33825
//	jr		ra			# 0041b13c:03e00008
  PInt64(a2x)^ := a3x; //sd a3,$0000(a2)
 end;
end;

procedure FillPattern(var GPR: GPRRegs); //FillPattern
begin           // a0x = buffer
 with GPR.n do  // a1x = pattern
 begin
  v1x := PInt64(a1x)^;                            //	ld		v1, $0000(a1)		# 0041ad08:dca30000
  a3x := a0x;                                     //	daddu		a3, a0, zero		# 0041ad0c:0080382d
  t0x := Integer(a3x) + 8;                        //	addiu		t0, a3, $0008		# 0041ad10:24e80008
  a2x := Integer(a3x) + 1;                        //	addiu		a2, a3, $0001		# 0041ad14:24e60001
  v0x := v1x shr 32;                              //	dsra32		v0, v1, 0		# 0041ad18:0003103f
  v1x := v1x shl 32;                              //	dsll32		v1, v1, 0		# 0041ad1c:0003183c
  v1x := v1x shr 32;                              //	dsra32		v1, v1, 0		# 0041ad20:0003183f
  a0x := Cardinal(v0x) shl 16;                    //	sll		a0, v0, 16		# 0041ad24:00022400
  a1x := Cardinal(v1x) shr 24;                    //	srl		a1, v1, 24		# 0041ad28:00032e02
  v0x := Cardinal(v0x) shr 16;                    //	srl		v0, v0, 16		# 0041ad2c:00021402
  v1x := Cardinal(v1x) shl 8;                     //	sll		v1, v1, 8		# 0041ad30:00031a00
  v0x := v0x or a0x;                              //	or		v0, v0, a0		# 0041ad34:00441025
  v1x := v1x or a1x;                              //	or		v1, v1, a1		# 0041ad38:00651825
  v0x := v0x shl 32;                              //	dsll32		v0, v0, 0		# 0041ad3c:0002103c
  v0x := v0x shr 32;                              //	dsrl32		v0, v0, 0		# 0041ad40:0002103e
  v1x := v1x shl 32;                              //	dsll32		v1, v1, 0		# 0041ad44:0003183c
  v1x := v1x or v0x;                              //	or		v1, v1, v0		# 0041ad48:00621825
  PInt64(a3x)^ := v1x;                            //	sd		v1, $0000(a3)		# 0041ad4c:fce30000
  v0x := PByte(a3x)^;                             //	lbu		v0, $0000(a3)		# 0041ad50:90e20000
  v0x := Integer(v0x) + $45;                      //	addiu		v0, v0, $0045		# 0041ad54:24420045
  PByte(a3x)^ := Byte(v0x);                       //	sb		v0, $0000(a3)		# 0041ad58:a0e20000
  repeat
   v0x := PByte(a2x)^;                            //	lbu		v0, $0000(a2)		# 0041ad5c:90c20000
   v1x := PByte(a2x - 1)^;                        //	lbu		v1, $ffff(a2)		# 0041ad60:90c3ffff
   v0x := Integer(v0x) + $D4;                     //	addiu		v0, v0, $00d4		# 0041ad64:244200d4
   v0x := Integer(v1x) + Integer(v0x);            //	addu		v0, v1, v0		# 0041ad68:00621021
   v1x := Cardinal(v1x) shl 2;                    //	sll		v1, v1, 2		# 0041ad6c:00031880
   v0x := v0x xor v1x;                            //	xor		v0, v0, v1		# 0041ad70:00431026
   v0x := v0x xor $45;                            //	xori		v0, v0, $0045		# 0041ad74:38420045
   PByte(a2x)^ := Byte(v0x);                      //	sb		v0, $0000(a2)		# 0041ad78:a0c20000
   a2x := Integer(a2x) + 1;                       //	addiu		a2, a2, $0001		# 0041ad7c:24c60001
  until a2x >= t0x;                               //	sltu		v0, a2, t0		# 0041ad80:00c8102b
                                                  //	bnel		v0, zero, $0041ad60	# 0041ad84:5440fff6	^ __0041ad60
  a2x := 1;                                       //	addiu		a2, zero, $0001		# 0041ad8c:24060001	a2=$00000001
  a1x := Integer(a3x) + 8;                        //	addiu		a1, a3, $0008		# 0041ad90:24e50008
  repeat
   v0x := PInt64(a1x - 8)^;                       //	ld		v0, $fff8(a1)		# 0041ad98:dca2fff8
   a2x := Integer(a2x + 1);                       //	addiu		a2, a2, $0001		# 0041ad9c:24c60001	a2=$00000002
   a0x := Byte(a2x < $20);                        //	sltiu		a0, a2, $0020		# 0041ada0:2cc40020
   v1x := v0x shl 2;                              //	dsll		v1, v0, 2		# 0041ada4:000218b8
   v1x := v1x + v0x;                              //	daddu		v1, v1, v0		# 0041ada8:0062182d
   PInt64(a1x)^ := v1x;                           //	sd		v1, $0000(a1)		# 0041adac:fca30000
   a1x := Integer(a1x) + 8;                       //	addiu		a1, a1, $0008		# 0041adb4:24a50008
  until a0x = 0;                                  //	bne		a0, zero, $0041ad98	# 0041adb0:1480fff9	^ __0041ad98
  Integer(Pointer(a3x + $104)^) := 0;             //	sw		zero, $0104(a3)		# 0041adb8:ace00104
  Integer(Pointer(a3x + $100)^) := 0;             //	sw		zero, $0100(a3)		# 0041adc0:ace00100
 end;                                             //	jr		ra			# 0041adbc:03e00008
end;

(*
   a0x := s1x; // pat_buffer   
   a1x := s4x; // source       
   a2x := s2x; // dest         
   a3x := s0x; // size
*)
procedure DecryptBlock(var GPR: GPRRegs); //DecryptBlock
var
 Stack: array[0..4] of Int64;
begin                         //	addiu		sp, sp, $ffa0		# 0041b1e0:27bdffa0
 with GPR.n do                 //	sd		ra, $0050(sp)		# 0041b1f8:ffbf0050
 begin                        //	lui		v0, $ffff		# 0041b1e4:3c02ffff	v0=$ffff0000
  Stack[2] := s2x;            //	sd		s2, $0020(sp)		# 0041b1e8:ffb20020
  v1x := a1x; // source       //	daddu		v1, a1, zero		# 0041b1ec:00a0182d
  Stack[1] := s1x;            //	sd		s1, $0010(sp)		# 0041b1f0:ffb10010
  s2x := Cardinal(a3x) shr 3; //	srl		s2, a3, 3		# 0041b1f4:000790c2
            // size / 8
  s1x := a0x; // pat_buffer   //	daddu		s1, a0, zero		# 0041b1fc:0080882d
  Stack[4] := s4x;            //	sd		s4, $0040(sp)		# 0041b200:ffb40040
  a1x := a2x; // dest         //	daddu		a1, a2, zero		# 0041b204:00c0282d
  Stack[3] := s3x;            //	sd		s3, $0030(sp)		# 0041b208:ffb30030
  s2x := Integer(s2x) - 1;    //	addiu		s2, s2, $ffff		# 0041b20c:2652ffff
  v0x := -1;                  //	ori		v0, v0, $ffff		# 0041b210:3442ffff	v0=$ffffffff
  Stack[0] := s0x;            //	sd		s0, $0000(sp)		# 0041b218:ffb00000
  if s2x <> v0x then          //	beq		s2, v0, $0041b288	# 0041b214:1242001c	v __0041b288
  begin                       //	lui		s4, $ffff		# 0041b21c:3c14ffff	s4=$ffff0000
   s0x := a1x; // dest        //	daddu		s0, a1, zero		# 0041b220:00a0802d
   s3x := v1x; // source      //	daddu		s3, v1, zero		# 0041b224:0060982d
  // s4x := -1;                 //	ori		s4, s4, $ffff		# 0041b228:3694ffff	s4=$ffffffff
   repeat                     //	nop					# 0041b22c:00000000
    a1x := s3x; // src        //	daddu		a1, s3, zero		# 0041b230:0260282d
    a2x := s0x; // dst        //	daddu		a2, s0, zero		# 0041b234:0200302d
    a0x := s1x; // pat        //	daddu		a0, s1, zero		# 0041b238:0220202d
    s3x := Integer(s3x) + 8;  //	jal		$0041b148		# 0041b23c:0c106c52	^ FNC_0041b148
    DecryptBlockStep1(GPR);   //	addiu		s3, s3, $0008		# 0041b240:26730008
    s2x := Integer(s2x) - 1;  //	addiu		s2, s2, $ffff		# 0041b244:2652ffff
    a1x := s0x; // dst        //	daddu		a1, s0, zero		# 0041b248:0200282d
    a2x := s0x; // dst        //	daddu		a2, s0, zero		# 0041b24c:0200302d
    a0x := s1x; // pat        //	jal		$0041b0d8		# 0041b250:0c106c36	^ FNC_0041b0d8
    DecryptBlockStep2(GPR);   //	daddu		a0, s1, zero		# 0041b254:0220202d
    v0x := Integer(Pointer(Integer(s1x) + $100)^);          //	lw		v0, $0100(s1)		# 0041b258:8e220100
    a0x := Integer(Pointer(Integer(s1x) + $104)^);          //	lw		a0, $0104(s1)		# 0041b25c:8e240104
    v0x := Integer(v0x) + 8;                                //	addiu		v0, v0, $0008		# 0041b260:24420008
    Integer(Pointer(Integer(s1x) + $100)^) := Integer(v0x); //	sw		v0, $0100(s1)		# 0041b264:ae220100
    v1x := PByte(s0x)^;                                     //	lbu		v1, $0000(s0)		# 0041b268:92030000
    a0x := a0x + v1x;                                       //	addu		a0, a0, v1		# 0041b26c:00832021
    Integer(Pointer(Integer(s1x) + $104)^) := Integer(a0x); //	sw		a0, $0104(s1)		# 0041b270:ae240104
    v0x := PByte(Integer(s0x) + 4)^;                        //	lbu		v0, $0004(s0)		# 0041b274:92020004
    s0x := Integer(s0x) + 8;                                //	addiu		s0, s0, $0008		# 0041b278:26100008
    a0x := a0x + v0x;                                       //	addu		a0, a0, v0		# 0041b27c:00822021
    Integer(Pointer(Integer(s1x) + $104)^) := Integer(a0x); //	sw		a0, $0104(s1)		# 0041b284:ae240104
   until s2x < 0;             //	bne		s2, s4, $0041b230	# 0041b280:1654ffeb	^ __0041b230
  end;   // size ==           //	ld		ra, $0050(sp)		# 0041b288:dfbf0050
  s4x := Stack[4];            //	ld		s4, $0040(sp)		# 0041b28c:dfb40040
  s3x := Stack[3];            //	ld		s3, $0030(sp)		# 0041b290:dfb30030
  s2x := Stack[2];            //	ld		s2, $0020(sp)		# 0041b294:dfb20020
  s1x := Stack[1];            //	ld		s1, $0010(sp)		# 0041b298:dfb10010
  s0x := Stack[0];            //	ld		s0, $0000(sp)		# 0041b29c:dfb00000
 end;
end;

(*
   a1x := s1x; // Source             //	daddu		a1, s1, zero		# 0041b3ec:0220282d
   a2x := s2x; // Dest               //	daddu		a2, s2, zero		# 0041b3f0:0240302d
   a3x := s0x; // Size               //	daddu		a3, s0, zero		# 0041b3f4:0200382d
   a0x := Integer(Addr(Buffer[0]));   //	jal		$0041b2a8		# 0041b3f8:0c106caa	^ DecryptSt
   DecryptProc(GPR);
*)
procedure DecryptProc(var GPR: GPRRegs); //DecryptPass;
var
 Stack: array[0..4] of Int64;
begin
 with GPR.n do                           //	addiu		sp, sp, $ffa0		# 0041b2a8:27bdffa0
 begin                                   //	sd		ra, $0050(sp)		# 0041b2c8:ffbf0050
  Stack[3] := s3x;                       //	sd		s3, $0030(sp)		# 0041b2ac:ffb30030
  Stack[4] := s4x;                       //	sd		s4, $0040(sp)		# 0041b2b0:ffb40040
  s3x := a3x;                            //	daddu		s3, a3, zero		# 0041b2b4:00e0982d
  Stack[2] := s2x;                       //	sd		s2, $0020(sp)		# 0041b2b8:ffb20020
  s4x := a1x;                            //	daddu		s4, a1, zero		# 0041b2bc:00a0a02d
  Stack[1] := s1x;                       //	sd		s1, $0010(sp)		# 0041b2c0:ffb10010
  s2x := a2x;                            //	daddu		s2, a2, zero		# 0041b2c4:00c0902d
  s1x := a0x;                            //	daddu		s1, a0, zero		# 0041b2cc:0080882d
  // v0x := Byte(s3x < 8);               //	sltiu		v0, s3, $0008		# 0041b2d0:2e620008
  Stack[0] := s0x;                       //	sd		s0, $0000(sp)		# 0041b2d8:ffb00000
  if s3x >= 8 then                       //	beq		v0, zero, $0041b2e4	# 0041b2d4:10400003	v __0041b2e4
  begin
   s0x := Integer(s3x) - 8;              //	addiu		s0, s3, $fff8		# 0041b2e4:2670fff8
   a0x := s1x; // pat_buffer             //	daddu		a0, s1, zero		# 0041b2e8:0220202d
   a1x := s4x; // source                 //	daddu		a1, s4, zero		# 0041b2ec:0280282d
   a2x := s2x; // dest                   //	daddu		a2, s2, zero		# 0041b2f0:0240302d
   a3x := s0x; // size                   //	jal		$0041b1e0		# 0041b2f4:0c106c78	^ DecryptBlock
   DecryptBlock(GPR);                    //	daddu		a3, s0, zero		# 0041b2f8:0200382d
   a0x := s1x;                           //	daddu		a0, s1, zero		# 0041b2fc:0220202d
   a1x := s4x + s0x;                     //	addu		a1, s4, s0		# 0041b300:02902821
   s1x := Integer(Pointer(s1x + $104)^); //	lw		s1, $0104(s1)		# 0041b304:8e310104
   a2x := s2x + s0x;                     //	addu		a2, s2, s0		# 0041b308:02503021
   a3x := 8;                             //	jal		$0041b1e0		# 0041b30c:0c106c78	^ DecryptBlock
   DecryptBlock(GPR);                    //	addiu		a3, zero, $0008		# 0041b310:24070008	a3=$00000008
   v1x := s3x + s2x;                     //	addu		v1, s3, s2		# 0041b314:02721821
   a1x := -1;                            //	addiu		a1, zero, $ffff		# 0041b318:2405ffff	a1=$ffffffff
   a0x := Integer(Pointer(v1x - 4)^);    //	lw		a0, $fffc(v1)		# 0041b31c:8c64fffc
   v0x := Integer(Pointer(v1x - 8)^);    //	lw		v0, $fff8(v1)		# 0041b320:8c62fff8
   s1x := s1x xor a0x;                   //	xor		s1, s1, a0		# 0041b324:02248826
   if s1x <> 0 then
    v0x := a1x;                          //	movn		v0, a1, s1		# 0041b328:00b1100b
  end else                               //	beq		zero, zero, $0041b32c	# 0041b2dc:10000013	v __0041b32c
   v0x := -1;                            //	addiu		v0, zero, $ffff		# 0041b2e0:2402ffff	v0=$ffffffff
                                         //	ld		ra, $0050(sp)		# 0041b32c:dfbf0050
  s4x := Stack[4];                       //	ld		s4, $0040(sp)		# 0041b330:dfb40040
  s3x := Stack[3];                       //	ld		s3, $0030(sp)		# 0041b334:dfb30030
  s2x := Stack[2];                       //	ld		s2, $0020(sp)		# 0041b338:dfb20020
  s1x := Stack[1];                       //	ld		s1, $0010(sp)		# 0041b33c:dfb10010
  s0x := Stack[0];                       //	ld		s0, $0000(sp)		# 0041b340:dfb00000
 end;                                    //	jr		ra			# 0041b344:03e00008
end;                                     //	addiu		sp, sp, $0060		# 0041b348:27bd0060

(*
   a0x := Integer(Addr(Pattern));
   a1x := Integer(Source);
   a2x := Integer(Dest);
   a3x := Size;
*)
procedure DataDecrypt(var GPR: GPRRegs); //DataDecrypt
var
 Buffer: array[0..$10F] of Byte;
 Stack: array[0..2] of Int64;
begin
 with GPR.n do                       //	addiu		sp, sp, $feb0		# 0041b3a8:27bdfeb0
 begin                               //	sd		ra, $0140(sp)		# 0041b3c0:ffbf0140
  Stack[0] := s0x;                   //	sd		s0, $0110(sp)		# 0041b3ac:ffb00110
  s0x := a3x;                        //	daddu		s0, a3, zero		# 0041b3b0:00e0802d
  Stack[2] := s2x;                   //	sd		s2, $0130(sp)		# 0041b3b4:ffb20130
  Stack[1] := s1x;                   //	sd		s1, $0120(sp)		# 0041b3b8:ffb10120
  v0x := Cardinal(s0x) shr 3;        //	srl		v0, s0, 3		# 0041b3bc:001010c2
  s1x := a1x;                        //	daddu		s1, a1, zero		# 0041b3c4:00a0882d
  v0x := Cardinal(v0x) shl 3;        //	sll		v0, v0, 3		# 0041b3c8:000210c0
  s2x := a2x;                        //	daddu		s2, a2, zero		# 0041b3d0:00c0902d
  if v0x <> 0 then                   //	beq		v0, zero, $0041b3dc	# 0041b3cc:10400003	v __0041b3dc
   a1x := a0x;                       //	daddu		a1, a0, zero		# 0041b3d8:0080282
  if (s0x <> 0) and (v0x = s0x) then //	beq		v0, s0, $0041b3e4	# 0041b3d4:10500003	v __0041b3e4
  begin
   a0x := Integer(Addr(Buffer[0]));  //	jal		$0041b0d0		# 0041b3e4:0c106c34	^ FNC_0041b0d0
   FillPattern(GPR);                 //	daddu		a0, sp, zero		# 0041b3e8:03a0202d
   a1x := s1x; // Source             //	daddu		a1, s1, zero		# 0041b3ec:0220282d
   a2x := s2x; // Dest               //	daddu		a2, s2, zero		# 0041b3f0:0240302d
   a3x := s0x; // Size               //	daddu		a3, s0, zero		# 0041b3f4:0200382d
   a0x := Integer(Addr(Buffer[0]));   //	jal		$0041b2a8		# 0041b3f8:0c106caa	^ DecryptSt
   DecryptProc(GPR);              //	daddu		a0, sp, zero		# 0041b3fc:03a0202d
  end else                           //	beq		zero, zero, $0041b400	# 0041b3dc:10000008	v __0041b400
   v0x := -1;                        //	addiu		v0, zero, $ffff		# 0041b3e0:2402ffff	v0=$ffffffff
                                     //	ld		ra, $0140(sp)		# 0041b400:dfbf0140
  s2x := Stack[2];                   //	ld		s2, $0130(sp)		# 0041b404:dfb20130
  s1x := Stack[1];                   //	ld		s1, $0120(sp)		# 0041b408:dfb10120
  s0x := Stack[0];                   //	ld		s0, $0110(sp)		# 0041b40c:dfb00110
 end;                                //	jr		ra			# 0041b410:03e00008
end;                                 //	addiu		sp, sp, $0150		# 0041b414:27bd0150

type
 TPatternArray = array[0..32] of Int64;

function EncryptBlock(var Source, Dest; Index: Integer;
                      const PatArray: TPatternArray; Size: Integer): Integer;
type
 TRec64 = packed record
  A: Integer;
  B: Integer;
 end;
Var
 S, D: PInt64;
 SX: ^TRec64 absolute S;
 DX: ^TRec64 absolute D;
 I, X, Y: Integer;
 SB, DB, PB: PByte;
 t3: Byte;
 ZX, Pattern, PatX: Int64;
begin
 Result := 0;
 if Size < 8 then Exit;
 Size := Size shr 3;
 Index := Index shr 3;
 S := Addr(Source);
 D := Addr(Dest);
 PatX := CRPAT;
 for I := 0 to Size - 1 do
 begin
  DB := Pointer(S);
  Inc(Result, DB^);
  Inc(DB, 4);
  Inc(Result, DB^);
  X := SX.B;
  Y := SX.A;
  DX.A := X;
  DX.B := Y;
  ZX := (I + Index) shl 3;
  Pattern := PatArray[(I + Index) and $1F];
  D^ := Pattern + ((D^ xor Pattern) xor
        (PatX + (ZX or (ZX shl 10) or (ZX shl 20) or (ZX shl 30))));
  SB := Pointer(S);
  DB := Pointer(D);
  t3 := (I + Index) xor $45;
  For Y := 0 to 7 do
  begin
   PB := Addr(Pattern);
   Inc(PB, 7);
   for X := 0 to 7 do
   begin
    DB^ := sdbytetable2[Byte(PB^ + DB^)];
    Dec(PB);
   end;
   DB^ := DB^ xor t3;
   t3 := DB^;
   Inc(SB);
   Inc(DB);
  end;
  Inc(S);
  Inc(D);
 end;
end;

function Decrypt(Pattern: Int64; Size: Integer;
                     Source, Dest: Pointer): Integer;
var GPR: GPRRegs;
begin
 if Size >= SizeOf(TBlockHeader) then
 begin
  Size := (Size + 7) and not 7;
  with GPR.n do
  begin
   a0x := Integer(Addr(Pattern));
   a1x := Integer(Source);
   a2x := Integer(Dest);
   a3x := Size;
   DataDecrypt(GPR);
   Result := v0x;
  end;
 end else
  Result := -1;
end;

procedure Encrypt(Pattern: Int64; Size: Integer;
                 Source, Dest: Pointer);
var
 Block: ^TBlockHeader;
 PatArray: TPatternArray;
 GPR: GPRRegs;
 XSize: Integer;
begin
 if Size > 0 then
 begin
  XSize := (Size + 7) and not 7;
  with GPR.n do
  begin
   a0x := Integer(Addr(PatArray[0]));
   a1x := Integer(Addr(Pattern));
   FillPattern(GPR);
   Block := Pointer(Integer(Dest) + XSize);
   Block.Size := Size;
   Block.CRC := EncryptBlock(Source^, Dest^, 0, PatArray, XSize);
   EncryptBlock(Block^, Block^, XSize, PatArray, 8);
  end;
 end else
 begin
  Block := Dest;
  Block.Size := 0;
  Block.CRC := 0;
 end;
end;

function GetEncryptSize(Size: Integer): Integer;
begin
 Result := (Size + 7) and not 7 + SizeOf(TBlockHeader);
end;

//$5454545400002E00

end.

